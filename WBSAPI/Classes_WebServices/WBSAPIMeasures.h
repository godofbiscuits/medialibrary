// Very limited example class to parse the measure groups and 
// initialize a few variable from them.

@interface WBSAPIMeasures : NSObject {
    float last_weight;
    float last_fat;
    float last_fatpct;
    float last_height;

    float target_weight;
    float target_fat;


}

@property (nonatomic, readwrite)     float last_weight;
@property (nonatomic, readwrite)     float last_fat;
@property (nonatomic, readwrite)     float last_fatpct;
@property (nonatomic, readwrite)     float last_height;

@property (nonatomic, readwrite)     float target_weight;
@property (nonatomic, readwrite)     float target_fat;

-(BOOL) parseMeasures:(NSDictionary *)body;


@end
