// -*- objc -*-

#include <CommonCrypto/CommonDigest.h>  // For MD5 hash

#import <CFNetwork/CFHTTPMessage.h>
#import <SystemConfiguration/SCNetworkReachability.h>

#import "WBSAPI.h"

#  define BASE_HTTP "http://wbsapi.withings.net/"






// update status if 'err' is handled, else keep it.
void get_real_error_status(NSError *err, int *status)
{
	// error codes: http://developer.apple.com/iphone/library/documentation/Cocoa/Reference/Foundation/Miscellaneous/Foundation_Constants/Reference/reference.html#//apple_ref/doc/c_ref/NSURLErrorTimedOut
	if ([err.domain isEqualToString:NSURLErrorDomain] ){
		if ((err.code <= -1000) &&(err.code >= -2000))
			*status = err.code;
	}
}



#define CHECK_NSError_RETURN_STATUS(err) do { if (err) {status = JSONTOOLS_GETURL_NETWORK_ERROR; get_real_error_status(err, &status); return NO;} } while (0)


// return a static pointer. Make a copy if you want to keep the result after other calls to this function.
// Not thread safe.
char *md5_hash_to_hex (char *Bin )
{
  unsigned short i;
  unsigned char j;
  static char Hex[33];

  for (i = 0; i < 16; i++)
    {
      j = (Bin[i] >> 4) & 0xf;
      if (j <= 9)
        Hex[i * 2] = (j + '0');
      else
        Hex[i * 2] = (j + 'a' - 10);
      j = Bin[i] & 0xf;
      if (j <= 9)
        Hex[i * 2 + 1] = (j + '0');
      else
        Hex[i * 2 + 1] = (j + 'a' - 10);
    };
  Hex[32] = '\0';
  return(Hex);
}





@implementation WBSAPI


@synthesize status;

@synthesize account_email;
@synthesize account_password;

@synthesize user_id;
@synthesize user_publickey;



-(NSString *) escapeJSONforHTTP:(NSString *)string
{
	NSString *escaped = string;
	
	escaped = [escaped stringByReplacingOccurrencesOfString:@"%" withString:@"%25"];
	escaped = [escaped stringByReplacingOccurrencesOfString:@"\"" withString:@"%22"];
	escaped = [escaped stringByReplacingOccurrencesOfString:@"{" withString:@"%7B"];
	escaped = [escaped stringByReplacingOccurrencesOfString:@"}" withString:@"%7D"];
	escaped = [escaped stringByReplacingOccurrencesOfString:@"[" withString:@"%5B"];
	escaped = [escaped stringByReplacingOccurrencesOfString:@"]" withString:@"%5D"];

	return (escaped);
}




// Transforms the JSON text representation to an internal NSDictionary structure.
-(NSDictionary *) getBody:(id)repr withStatus:(int *)status_p
{
	SBJSON *json;
	NSError *error = nil;

	json = [[SBJSON alloc] init];
	id obj = [json objectWithString:repr allowScalar:NO error:&error];
	[json release];
	
	if (error || !obj){
		*status_p = JSONTOOLS_JSON_PARSE_ERROR;
		return nil;
	}

	if (![obj isKindOfClass:[NSDictionary class]]){
		NSLog(@"'obj' not dictionnary");
		*status_p = JSONTOOLS_NO_ROOT_DICTIONNARY;
		return nil;
	}

	NSDictionary *main_dict = (NSDictionary *)obj;
	id status_o =  [main_dict objectForKey:@"status"];
	id body_o =  [main_dict objectForKey:@"body"];

	if (![status_o isKindOfClass:[NSNumber class]]) {
		NSLog(@"'status' not number");
		*status_p = JSONTOOLS_NO_STATUS;
		return nil;
	}

	*status_p = [((NSNumber *)status_o) intValue];
	if (!body_o)
		return nil; // 'body' is optionnal, (when status indicates an error or when nothing needs to be returned, like un "once?probe")
	// When 'body' is present, it MUST be a json Object ( Dictionnary )
	if (![body_o isKindOfClass:[NSDictionary class]]) {
		NSLog(@"'body' not a dictionnary");
		*status_p = JSONTOOLS_NO_BODY_ELT;
		return nil;
	}
	NSDictionary *body = (NSDictionary *)body_o;


	return body;
}


// useGzip should be used only when the answer is large and will benefit from compression
// (measures are a good candidate).
-(id)getHTMLForURL:(NSString *)url_req gzip:(BOOL) useGzip error:(NSError **)nserror
{
	NSURL *baseURL = [NSURL URLWithString:@BASE_HTTP];
	NSMutableURLRequest *nsrequest;
	NSURLResponse *nsresponse;

	nsrequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url_req relativeToURL:baseURL]];
	if (useGzip) {
		[nsrequest setValue:@"gzip" forHTTPHeaderField:@"Accept-Encoding"];
		[nsrequest addValue:@"deflate" forHTTPHeaderField:@"Accept-Encoding"];
	} else {
		// gzip-encoding is the default mode of UrlRequest. Have to explicitely disable it.
		[nsrequest setValue:@"" forHTTPHeaderField:@"Accept-Encoding"];
	}


	[nsrequest setTimeoutInterval:8.0f];

	NSData *data = [NSURLConnection sendSynchronousRequest:nsrequest returningResponse:&nsresponse error:nserror];

	
							  
	if (data == nil){
		if (nserror)
			NSLog(@"sendSynchronousRequest failed: %@", [*nserror description]);
		else
			NSLog(@"sendSynchronousRequest failed: (NULL error)");
		return nil;
	}

	NSString *repr = [[NSString alloc ] initWithData:data encoding:NSUTF8StringEncoding];
	[repr autorelease];
	return repr;
	
}



#pragma mark -



-(NSString *) getOnce
{
	id repr;
	NSDictionary *body;
	NSError *nserror = nil;
	NSString *once;

	repr = [self getHTMLForURL:@"once?action=get" gzip:NO error:&nserror];
	CHECK_NSError_RETURN_STATUS(nserror);
	body = [self getBody:repr withStatus:&status];

	if (!body || status != 0){
		return nil;
	}

	GET_DICT_KEY_TYPE_VARIABLE_status_or_goto(body, @"once", NSString, once, status, return_NO);

	return once;

 return_NO:
	return nil;

}



-(NSArray *) getUsersListFromAccount
{

	id repr;
	NSDictionary *body;
	NSString *request;
	NSError *nserror = nil;


	char  hashResult[33];

	char *hashed_pwd;

	if (account_email == nil || account_password == nil) {
		NSLog(@"account_email or account_password missing");
		status = JSONTOOLS_API_ERROR;
		return nil;
	}

	const char *pwd_c = [account_password UTF8String];
	if (pwd_c == NULL) {
		NSLog(@"missing password");
		status = JSONTOOLS_API_ERROR;
		return nil;
	}



	
	NSString *once = [self getOnce];
	if (!once)
		return nil;



    CC_MD5((unsigned char*)pwd_c, strlen(pwd_c), (unsigned char*)hashResult);
    hashed_pwd = md5_hash_to_hex(hashResult);


	NSString *challenge_to_hash = [NSString stringWithFormat:@"%@:%s:%@", account_email, hashed_pwd, once];
	const char *challenge_c = [challenge_to_hash UTF8String];

	CC_MD5(challenge_c, strlen(challenge_c), (unsigned char*)hashResult);
	NSString *hashed_challenge = [NSString stringWithFormat:@"%s", md5_hash_to_hex(hashResult) ];

	request = [NSString stringWithFormat:@"account?action=getuserslist&email=%@&hash=%@",
						account_email, hashed_challenge];
	repr = [self getHTMLForURL:request gzip:NO error:&nserror];
	CHECK_NSError_RETURN_STATUS(nserror);
	body = [self getBody:repr withStatus:&status];

	if (status != JSONTOOLS_OK)
		return nil;


	NSArray *users;
	GET_DICT_KEY_TYPE_VARIABLE_status_or_goto(body,@"users",NSArray, users, status, return_NO);
	if ([users count] < 1){
		//NSLog(@"userslist: 'users' array empty");
		return [[[NSArray alloc] init] autorelease];
	}

	NSMutableArray *parsed_users = [[NSMutableArray alloc] init];
	[parsed_users autorelease];

	int i;
	for (i=0; i < [users count]; i++){
		id user_i_o = [users objectAtIndex:i];
		if (![user_i_o isKindOfClass:[NSDictionary class]]) {
			//NSLog(@"userslist: user #%d not a dict", i);
			status = JSONTOOLS_MANDATORY_NOT_FOUND;
			return nil;
		}

		WBSAPIUser *user = [[[WBSAPIUser alloc] init] autorelease];
		NSDictionary *user_i = (NSDictionary *)user_i_o;
		GET_DICT_KEY_integer_VARIABLE_status_or_goto(user_i, @"id", user.user_id, status, return_NO);
		GET_DICT_KEY_TYPE_VARIABLE_status_or_goto(user_i, @"firstname", NSString, user.firstname, status, return_NO);
		GET_DICT_KEY_TYPE_VARIABLE_status_or_goto(user_i, @"lastname",  NSString, user.lastname, status, return_NO);
		GET_DICT_KEY_TYPE_VARIABLE_status_or_goto(user_i, @"shortname", NSString, user.shortname, status, return_NO);

		GET_DICT_KEY_integer_VARIABLE_status_or_goto(user_i, @"gender", user.gender, status, return_NO);
		GET_DICT_KEY_integer_VARIABLE_status_or_goto(user_i, @"fatmethod", user.fatmethod, status, return_NO);
		GET_DICT_KEY_unixtime_VARIABLE_status_or_goto(user_i, @"birthdate", user.birthdate, status, return_NO);
		GET_DICT_KEY_bool_VARIABLE_status_or_goto(user_i, @"ispublic", user.ispublic, status, return_NO);
		GET_DICT_KEY_TYPE_VARIABLE_status_or_goto(user_i, @"publickey", NSString, user.publickey, status, return_NO);


		[parsed_users addObject:user];
	}

	return parsed_users;

 return_NO:  // any parse error branch here.
	return nil; // self.status is already set by the GET_DICT_... macro.

}






-(WBSAPIUser *) getUserInfo
{

	if (user_id == 0 || user_publickey == nil) {
		NSLog(@"user_id or user_publickey missing");
		status = JSONTOOLS_API_ERROR;
		return nil;
	}


	id repr;
	NSDictionary *body;
	NSString *request;
	NSError *nserror = nil;

	request = [NSString stringWithFormat:@"user?action=getbyuserid&userid=%d&publickey=%@",
						user_id, user_publickey];
	repr = [self getHTMLForURL:request gzip:NO error:&nserror];
	CHECK_NSError_RETURN_STATUS(nserror);
	body = [self getBody:repr withStatus:&status];
	if (status != 0){
		return nil;
	}

	// The user information is not directly inside 'body', it's in the unique element of the 'users' array
	// (like in account?getuserslist)

	NSArray *users;
	GET_DICT_KEY_TYPE_VARIABLE_status_or_goto(body, @"users",NSArray, users, status, return_NO);
	if ([users count] < 1){
		status = JSONTOOLS_MANDATORY_EMPTY;
		NSLog(@"getbyuserid: 'users' array empty");
		return nil;
	}

	id user_i_o = [users objectAtIndex:0];
	if (![user_i_o isKindOfClass:[NSDictionary class]]) {
		NSLog(@"getbyuserid: user #0 not a dict");
		status = JSONTOOLS_MANDATORY_NOT_FOUND;
		return nil;
	}

	WBSAPIUser *user = [[[WBSAPIUser alloc] init] autorelease];
	NSDictionary *user_i = (NSDictionary *)user_i_o;




	GET_DICT_KEY_integer_VARIABLE_status_or_goto(user_i, @"id", user.user_id, status, return_NO);
	GET_DICT_KEY_TYPE_VARIABLE_status_or_goto(user_i, @"firstname", NSString, user.firstname, status, return_NO);
	GET_DICT_KEY_TYPE_VARIABLE_status_or_goto(user_i, @"lastname",  NSString, user.lastname, status, return_NO);
	GET_DICT_KEY_TYPE_VARIABLE_status_or_goto(user_i, @"shortname", NSString, user.shortname, status, return_NO);

	GET_DICT_KEY_integer_VARIABLE_status_or_goto(user_i, @"gender", user.gender, status, return_NO);
	GET_DICT_KEY_integer_VARIABLE_status_or_goto(user_i, @"fatmethod", user.fatmethod, status, return_NO);
	GET_DICT_KEY_unixtime_VARIABLE_status_or_goto(user_i, @"birthdate", user.birthdate, status, return_NO);
	user.ispublic = YES;
	user.publickey = user_publickey;


	return user;

 return_NO:
	return nil;


}



#pragma mark -





-(NSDictionary *) getUserMeasuresWithCategory:(int)category
{
	if (user_id == 0 || user_publickey == nil) {
		NSLog(@"user_id or user_publickey missing");
		status = JSONTOOLS_API_ERROR;
		return nil;
	}


	id repr;
	NSDictionary *body;
	NSString *request;
	NSError *nserror = nil;

	// You will probably want to have other methods to better specify the category types,
	// the measure types, and the date range.
	// This is just a sample application.


	request = [NSString stringWithFormat:@"measure?action=getmeas&userid=%d&publickey=%@&category=%d",
						user_id, user_publickey, category];

	repr = [self getHTMLForURL:request gzip:YES error:&nserror];
	CHECK_NSError_RETURN_STATUS(nserror);
	body = [self getBody:repr withStatus:&status];


	if (status != 0){
		NSLog(@"Error measure/getmeas: status = %d", status);
		return nil;
	}

	return body;
	
}





/* ********************************************** */
#pragma mark -


/* ********************************************** */

-(WBSAPI *) init
{
	self = [super init];
	if (!self)
		return nil;

	return self;
}

-(void) dealloc
{

	[super dealloc];
}

@end

/*
 * Local Variables:
 * c-basic-offset:4
 * indent-tabs-mode:t
 * tab-width:4
 * End:
 */
