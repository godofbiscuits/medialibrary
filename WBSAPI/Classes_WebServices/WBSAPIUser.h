
// Storage of a "user" in the Withings BodyScale API

@interface WBSAPIUser : NSObject {
	int        user_id;
	NSString  *firstname;
	NSString  *lastname;
	NSString  *shortname;
	int        gender;
	int        fatmethod;
	NSDate    *birthdate;
	
	// in account / getuserslist
	BOOL      ispublic;
	NSString *publickey;

}

@property (readwrite, nonatomic        ) 	int        user_id;
@property (readwrite, nonatomic, retain) 	NSString  *firstname;
@property (readwrite, nonatomic, retain) 	NSString  *lastname;
@property (readwrite, nonatomic, retain) 	NSString  *shortname;
@property (readwrite, nonatomic        )	int        gender;
@property (readwrite, nonatomic        ) 	int        fatmethod;
@property (readwrite, nonatomic, retain) 	NSDate    *birthdate;


@property (readwrite, nonatomic        ) 	BOOL       ispublic;
@property (readwrite, nonatomic, retain) 	NSString  *publickey;


@end

