
#import "WBSAPIMeasures.h"
#import "WBSAPI.h" // for the JSON parsing Macro





@implementation WBSAPIMeasures


@synthesize last_weight;
@synthesize last_fat;
@synthesize last_fatpct;
@synthesize last_height;

@synthesize target_weight;
@synthesize target_fat;



/*
     Dict{body}{measuregrps}[] =>    {attrib}, {date},...  {measures}[] => {type}, {value}, {unit}
     {value, unit} => float

 */


/*
 *  Your application will probably use a custom parseMeasures method that directly fills your data structures.
 */

-(BOOL) parseMeasures:(NSDictionary *)body
{
	int dummy_status; // for the macros...
	NSArray *msgrp;

	GET_DICT_KEY_TYPE_VARIABLE_status_or_goto(body, @"measuregrps", NSArray, msgrp, dummy_status, return_nil);

	id group_o;
	NSEnumerator * enumerator =  [msgrp reverseObjectEnumerator];  // reverse because the webservice order is most recent first.


	NSLog(@"Measure groups = {");

	while ( (group_o = [enumerator nextObject]) ){
		if (![group_o isKindOfClass:[NSDictionary class]])
			continue;
		NSDictionary *group = (NSDictionary *)group_o;

		int grpid;
		int attrib;
		int unixdate;
		NSDate *macdate;
		int category;
		NSArray *measures;
		

		GET_DICT_KEY_TYPE_VARIABLE_status_or_goto(group, @"measures", NSArray, measures, dummy_status, return_nil);
		GET_DICT_KEY_integer_VARIABLE_status_or_goto(group, @"grpid", grpid, dummy_status, return_nil);
		GET_DICT_KEY_integer_VARIABLE_status_or_goto(group, @"attrib", attrib, dummy_status, return_nil);

		// Choose your preferred date representation:
		GET_DICT_KEY_integer_VARIABLE_status_or_goto(group, @"date", unixdate, dummy_status, return_nil);
		CFAbsoluteTime cftimestamp = unixdate - kCFAbsoluteTimeIntervalSince1970;
		GET_DICT_KEY_unixtime_VARIABLE_status_or_goto(group, @"date", macdate, dummy_status, return_nil);

		GET_DICT_KEY_integer_VARIABLE_status_or_goto(group, @"category", category, dummy_status, return_nil);

		//BOOL dubious = ((attrib & MEASURE_ATTRIB_DUBIOUS) != 0);
		//BOOL manual = ((attrib & MEASURE_ATTRIB_MANUAL) != 0);


		
		NSEnumerator *m_enum =  [measures objectEnumerator];
		id measure_elt_o;
		float weight = 0.0f;
		float fatfree = 0.0f;
		float fat = 0.0f;
		float fatpct = 0.0f;
		float height = 0.0f;
		while ( (measure_elt_o = [m_enum nextObject]) ){
			if (![measure_elt_o isKindOfClass:[NSDictionary class]])
				continue;
			NSDictionary *measure_elt = (NSDictionary *)measure_elt_o;

			int type, value, unit;

			GET_DICT_KEY_integer_VARIABLE_status_or_goto(measure_elt, @"type", type, dummy_status, return_nil);
			GET_DICT_KEY_integer_VARIABLE_status_or_goto(measure_elt, @"value", value, dummy_status, return_nil);
			GET_DICT_KEY_integer_VARIABLE_status_or_goto(measure_elt, @"unit", unit, dummy_status, return_nil);

			float fvalue = value * powf (10, unit);


			switch (type){
			case WS_TYPE_WEIGHT:
			    weight=fvalue;
			    if (category == WS_CATEGORY_MEASURE)
				self.last_weight = fvalue;
			    else if (category == WS_CATEGORY_TARGET)
				self.target_weight = fvalue;
			    break;
			case WS_TYPE_HEIGHT:
			    height=fvalue;
			    if (category == WS_CATEGORY_MEASURE)
				self.last_height = fvalue;
			    break;
			case WS_TYPE_FATFREE_MASS:
			    fatfree=fvalue;
			    break;
			case WS_TYPE_FAT_MASS:
			    fat=fvalue;
			    if (category == WS_CATEGORY_MEASURE)
				self.last_fat = fvalue;
			    else if (category == WS_CATEGORY_TARGET)
				self.target_fat = fvalue;
			    break;
			case WS_TYPE_FAT_PCT:
			    fatpct=fvalue;
			    if (category == WS_CATEGORY_MEASURE)
				self.last_fatpct = fvalue;
			    break;
			default:
			    NSLog(@"Unknown measure type %d", type);
			}
		}

/*
  Log all measure in console...
  
		NSLog(@"  #%d [%@] : %@ = { W = %.02f kg, H = %.02f m, fat mass = %.02f kg  = %.02f%% , fatfree = %.02f kg }",
			  grpid,
		      [macdate description],
		      (category == WS_CATEGORY_MEASURE ? @"Measure" : @"Target"),
		      weight, height, fat, fatpct, fatfree);
*/

	}
	NSLog(@"}");
	
	return YES;
 return_nil:
	return NO;
}

@end
