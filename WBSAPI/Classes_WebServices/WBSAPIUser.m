#import "WBSAPIUser.h"

@implementation WBSAPIUser
@synthesize  user_id;
@synthesize  firstname;
@synthesize  lastname;
@synthesize  shortname;

@synthesize  gender;
@synthesize  fatmethod;
@synthesize  birthdate;

@synthesize  ispublic;
@synthesize  publickey;

-(void) dealloc
{
	self.firstname = nil;
	self.lastname  = nil;
	self.shortname  = nil;
	self.birthdate = nil;
	self.publickey = nil;

	[super dealloc];
}

@end
