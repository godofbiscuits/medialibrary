// -*- objc -*-

#import "JSON.h"

#import "WBSAPIUser.h"




#define WS_CATEGORY_MEASURE 1
#define WS_CATEGORY_TARGET  2

#define WS_TYPE_WEIGHT 1
#define WS_TYPE_HEIGHT   4 
#define WS_TYPE_FATFREE_MASS 5
#define WS_TYPE_FAT_PCT  6
#define WS_TYPE_FAT_MASS 8



typedef enum {

	// status code > 0 are directly the "status" of the webservice.


	GEN_WS_STATUSCODE_AUTHFAILED_ID				= 100,
	GEN_WS_STATUSCODE_AUTHFAILED_USER_ID		= 250,


	GEN_WS_STATUSCODE_UNSPECIFIED_ID			= 2555,


	JSONTOOLS_OK  = 0,

	// status code < 0 are internal error to this WBSAPI implementation or parse error:


	JSONTOOLS_JSON_PARSE_ERROR = -1, // reply was not JSON text
	JSONTOOLS_NO_ROOT_DICTIONNARY = -2, // root object was not an "object" (=nsdictionnary)
	JSONTOOLS_NO_STATUS = -3, // no integer "status" element in root json
	JSONTOOLS_NO_BODY_ELT = -4, // no dictionnary "body" in root json
	
	JSONTOOLS_MANDATORY_NOT_FOUND = -10,  // a mandatory element (for this webservice) was not found, or is of wrong type
	JSONTOOLS_MANDATORY_EMPTY = -11,  // a mandatory array was found, but is empty

	JSONTOOLS_API_ERROR = -97,  // WBSAPI object was not set-up correctly for this method

	JSONTOOLS_GETURL_NETWORK_ERROR = -100  // generic low-level error, if unknown cause. Else use NSURLErrorDomain:
	// errors between de -1000 à -2000 (inclusive) are directly the codes from NSURLErrorDomain 
	// http://developer.apple.com/iphone/library/documentation/Cocoa/Reference/Foundation/Miscellaneous/Foundation_Constants/Reference/reference.html#//apple_ref/doc/uid/TP40003793-CH3g-SW40

} e_jsontools_error; 



char *md5_hash_to_hex (char *Bin );


// Extracting the elements from the parsed JSON NSDictionnary structure, and checking for errors,
// can be tedious.
// Here are several C Macro for easier code. Error handling is done by doing a "goto label;"
// this way most of the code is a sequential call of macros instead of doing if..else.. each time.

#define GET_DICT_KEY_TYPE_VARIABLE_status_or_goto(_Dict,_Key,_Type,_Variable, _Status, _Label) \
	do {																	\
		id _v = [_Dict objectForKey:_Key];				\
		if (![_v isKindOfClass:[_Type class]]) {		\
			NSLog(@"'" #_Key "' not a '" #_Type "'.");	\
			_Status = JSONTOOLS_MANDATORY_NOT_FOUND;    \
			goto _Label;								\
		}												\
		_Variable = (_Type *)_v;						\
	} while (0)


#define GET_DICT_KEY_TYPE_VARIABLE_status_or_goto_nowarning(_Dict,_Key,_Type,_Variable, _Status, _Label) \
	do {																	\
		id _v = [_Dict objectForKey:_Key];				\
		if (![_v isKindOfClass:[_Type class]]) {		\
			_Status = JSONTOOLS_MANDATORY_NOT_FOUND;    \
			goto _Label;								\
		}												\
		_Variable = (_Type *)_v;						\
	} while (0)


#define GET_DICT_KEY_TYPE_VARIABLE(_Dict,_Key,_Type,_Variable) \
	do {												\
		id _v = [_Dict objectForKey:_Key];				\
		if (_v && [_v isKindOfClass:[_Type class]]) 	\
			_Variable = (_Type *)_v;					\
	} while (0)

#define GET_DICT_KEY_array0of_TYPE_VARIABLE_status_or_goto(_Dict,_Key,_Type,_Variable, _Status, _Label) \
	do {																	   \
		NSArray *_a;                                                           \
		GET_DICT_KEY_TYPE_VARIABLE_status_or_goto(_Dict,_Key,NSArray, _a, _Status, _Label); \
		if ([_a count] < 1){                    \
			NSLog(@"'" #_Key "' is an empty array.");	\
			_Status = JSONTOOLS_MANDATORY_EMPTY; \
			goto _Label;                          \
		}                                       \
		id _v = [_a objectAtIndex:0];           \
		if (![_v isKindOfClass:[_Type class]]) {		\
			NSLog(@"'" #_Key "' not an array of '" #_Type "'.");	\
			_Status = JSONTOOLS_MANDATORY_NOT_FOUND;                 \
			goto _Label;								\
		}												\
		_Variable = (_Type *)_v;						\
	} while (0)
			
#define GET_DICT_KEY_integer_VARIABLE_status_or_goto(_Dict,_Key,_Variable, _Status, _Label)	\
	do {																\
		id _v = [_Dict objectForKey:_Key];				\
		if (![_v isKindOfClass:[NSNumber class]]) {		\
			NSLog(@"'" #_Key "' not a NSNumber .");		\
			_Status = JSONTOOLS_MANDATORY_NOT_FOUND;                               \
			goto _Label;								\
		}												\
		_Variable =  [((NSNumber *)_v) intValue];		\
	} while (0)

#define GET_DICT_KEY_integer_VARIABLE_status_or_goto_nowarning(_Dict,_Key,_Variable, _Status, _Label)	\
	do {																\
		id _v = [_Dict objectForKey:_Key];				\
		if (![_v isKindOfClass:[NSNumber class]]) {		\
			_Status = JSONTOOLS_MANDATORY_NOT_FOUND;    \
			goto _Label;								\
		}												\
		_Variable =  [((NSNumber *)_v) intValue];		\
	} while (0)

#define GET_DICT_KEY_bool_VARIABLE_status_or_goto(_Dict,_Key,_Variable, _Status, _Label)	\
	do {																\
		id _v = [_Dict objectForKey:_Key];				\
		if (![_v isKindOfClass:[NSNumber class]]) {		\
			_Status = JSONTOOLS_MANDATORY_NOT_FOUND;    \
			goto _Label;								\
		} else											\
			_Variable =  [((NSNumber *)_v) boolValue];	\
	} while (0)

#define GET_DICT_KEY_float_VARIABLE_status_or_goto(_Dict,_Key,_Variable, _Status, _Label)	\
	do {																\
		id _v = [_Dict objectForKey:_Key];				\
		if (![_v isKindOfClass:[NSNumber class]]) {		\
			NSLog(@"'" #_Key "' not a NSNumber .");		\
			_Status = JSONTOOLS_MANDATORY_NOT_FOUND;                               \
			goto _Label;								\
		}												\
		_Variable =  [((NSNumber *)_v) floatValue];		\
	} while (0)

// Parses and convert an Unix time (epoch) integer to an NSDate object.
// If you just want to directly get the integer value without conversion,
// use the GET_DICT_KEY_integer macro instead.
#define GET_DICT_KEY_unixtime_VARIABLE_status_or_goto(_Dict,_Key,_Variable, _Status, _Label)	\
	do {																\
		id _v = [_Dict objectForKey:_Key];				\
		if (![_v isKindOfClass:[NSNumber class]]) {		\
			NSLog(@"'" #_Key "' not a NSNumber .");		\
			_Status = JSONTOOLS_MANDATORY_NOT_FOUND;                               \
			goto _Label;								\
		}												\
		long  _unixtime =  [((NSNumber *)_v) longValue];				 \
		_Variable = [NSDate dateWithTimeIntervalSince1970:_unixtime];    \
	} while (0)

#define GET_DICT_KEY_unixtime_VARIABLE_status_or_goto_nowarning(_Dict,_Key,_Variable, _Status, _Label)	\
	do {																\
		id _v = [_Dict objectForKey:_Key];				\
		if (![_v isKindOfClass:[NSNumber class]]) {		\
			_Status = JSONTOOLS_MANDATORY_NOT_FOUND;                               \
			goto _Label;								\
		}												\
		long  _unixtime =  [((NSNumber *)_v) longValue];				 \
		_Variable = [NSDate dateWithTimeIntervalSince1970:_unixtime];    \
	} while (0)







@interface WBSAPI : NSObject {

	int status;
 
	NSString *account_email;
	NSString *account_password;

	int       user_id;
	NSString *user_publickey;

}

@property (nonatomic, readonly) int status;

@property (nonatomic, readwrite, retain) NSString *account_email;
@property (nonatomic, readwrite, retain) NSString *account_password;

@property (nonatomic, readwrite        ) int       user_id;
@property (nonatomic, readwrite, retain) NSString *user_publickey;





// call with account_email and account_password correctly set. 
// returns an autoreleased array of WBSAPIUser *, or nil on error. Check 'status' for error.
-(NSArray *) getUsersListFromAccount;

// call with user_id and user_publickey correctly set.
// returns an autoreleased 'user' object, or nil + status.
-(WBSAPIUser *) getUserInfo;

// call with user_id and user_publickey correctly set.
// returns the parsed body{} of the json reply; caller is responsible to interpret the content.
-(NSDictionary *) getUserMeasuresWithCategory:(int)category;




@end


/*
 * Local Variables: 
 * c-basic-offset:4
 * indent-tabs-mode:t
 * tab-width:4
 * End:
 */
