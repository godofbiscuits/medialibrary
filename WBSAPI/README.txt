

This is an example implementation of the Withings BodyScale API (WBSAPI) for an iPhone application.

This application can get the user list from an account+password, the user informations
from the user-id + publickey, and the measures from a user-id + publickey.


The following webservices are implemented and called : 
-  once?action=get
-  account?action=getuserslist 
-  user?action=getbyuserid 
-  measure?action=getmeas  , for actual measures and for user-defined targets.

The other webservices are not implemented in this example, but they can easily
be derived from the sample code in Classes_WebServices/WBSAPI.m .

The JSON-coded "measures" are then parsed to get the latest measure of each type
(weight, height, fat mass, fat %...) and the last target, in WBSAPIMeasures.m


Results are displayed in simple UITableView screens.



To compile : 
- first edit the file Classes/RootViewController.m and modify the 'viewDidLoad' method
  to replace the default account/passwords by your test account
  (You can also type them at run time).
- To test on a device, modify the Info.plist and change CFBundleIdentifier to your own company,
  and set your code signing identity in the project Build Settings.



This example uses the JSON-framework to parse JSON responses from the webservices.
A copy has been made in the JSON/ directory and added to the resources of the project to make
it ready to compile.

You can get the latest official version at:  http://code.google.com/p/json-framework/



More information about the Withings BodyScale API : http://www.withings.com/en/api/bodyscale

