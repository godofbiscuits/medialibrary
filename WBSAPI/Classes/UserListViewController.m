//
//  UserListViewController.m
//  wbsapi
//
//  Created by Dev on 11/01/10.
//  Copyright WiThings 2010. All rights reserved.
//

#import "UserListViewController.h"
#import "UserInfoViewController.h"
#import "AlertHelper.h"


@implementation UserListViewController

@synthesize email;
@synthesize password;




-(void) backgroundNetworkTerminated
{
	[self.tableView reloadData];
	if (wbsapi.status != 0) 
		[AlertHelper displayWebServiceErrorAlertView:wbsapi.status];
}

// Call the webservice from a background thread to not block the UI.
- (void) backgroundNetwork
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];

	NSArray *list =  [wbsapi getUsersListFromAccount];
	if (list) {
		[userList release];
		userList = [list retain];
	}
	[self performSelectorOnMainThread:@selector(backgroundNetworkTerminated)
		  withObject:nil waitUntilDone:YES];
	
	[pool release];

}


- (void)viewDidAppear:(BOOL)animated
{

	self.navigationItem.title = @"User list";


	if (!wbsapi) {
		wbsapi = [[WBSAPI alloc] init];
	}
	wbsapi.account_email = self.email;
	wbsapi.account_password = self.password;
	
	[NSThread detachNewThreadSelector:@selector(backgroundNetwork)
			  toTarget:self withObject:nil];
	// TODO: Display a UIActivityIndicator when the thread takes time

	[super viewDidAppear:animated];
}

/*
  - (void)viewWillDisappear:(BOOL)animated {
  [super viewWillDisappear:animated];
  }
*/
/*
  - (void)viewDidDisappear:(BOOL)animated {
  [super viewDidDisappear:animated];
  }
*/

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
// Return YES for supported orientations.
return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [userList count];
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    UITableViewCell *cell;
    int row = indexPath.row;
	WBSAPIUser *user = (WBSAPIUser *)[userList objectAtIndex:row];


	static NSString *CellIdentifier = @"Cell-user";
	cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if (cell == nil) {
		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
	}
	cell.textLabel.text = [NSString stringWithFormat:@"%@ %@ (%@)",
									user.firstname, user.lastname, user.shortname];
    

	if (user.ispublic)
		cell.accessoryView = nil;
	else
		cell.accessoryView =  [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"user_locked.png"]] autorelease];

	// If (!user.ispublic), you may want to call the "user / update" webservice to make it public.



    return cell;
}




- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	WBSAPIUser *user = (WBSAPIUser *)[userList objectAtIndex:indexPath.row];

	UserInfoViewController *ui = [[UserInfoViewController alloc] init];
	ui.user_id = user.user_id;
	ui.user_publickey = user.publickey;

	[[self navigationController] pushViewController:ui animated:YES];
	[ui release];

}


- (void)dealloc {
	self.email = nil;
	self.password = nil;
	[wbsapi release];
	[userList release];

    [super dealloc];
}


@end


/*
 * Local Variables: 
 * c-basic-offset:4
 * indent-tabs-mode:t
 * tab-width:4
 * End:
 */
