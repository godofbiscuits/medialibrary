//
//  UserInfoViewController.h
//  wbsapi
//
//  Created by Dev on 11/01/10.
//  Copyright WiThings 2010. All rights reserved.
//

#import "WBSAPIUser.h"
#import "WBSAPIMeasures.h"
#import "WBSAPI.h"

@interface UserInfoViewController : UITableViewController {
    int user_id;
    NSString *user_publickey;

    WBSAPIUser *user;
    WBSAPIMeasures *measures;

    WBSAPI  *wbsapi;
}

@property (nonatomic) int user_id;
@property (nonatomic,retain) NSString *user_publickey;

@end
