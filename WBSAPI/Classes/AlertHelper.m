
#import <UIKit/UIKit.h>

#import "AlertHelper.h"

#import "WBSAPI.h"

@implementation AlertHelper

+(void) displayWebServiceErrorAlertView:(int) status
{
	if (status == JSONTOOLS_OK)
		return;


	// unknown error
	NSString *title = [NSString stringWithFormat:@"ERROR %d", status];
	NSString *message = [NSString stringWithFormat:@"ERROR CODE %d", status];

	// explicit status from the webservices
	if (status > 0){
		title = @"Webservice error";
		switch (status){
		case GEN_WS_STATUSCODE_AUTHFAILED_ID:
			message = @"Authentication failed for account : wrong email/password";
			break;

		case GEN_WS_STATUSCODE_AUTHFAILED_USER_ID:
			message = @"Authentication failed for user : wrong user/publickey, or not set to public";
			break;

		case  GEN_WS_STATUSCODE_UNSPECIFIED_ID:
			message = @"Unspecified error";
			break;

		}
	}

	// error code from low-level functions
	else if ((status<=-1000) && (status >= -2000)){

		title = @"Network error";
		switch (status){
		case NSURLErrorBadURL:
			message = @"bad URL";
			break;
		case NSURLErrorTimedOut:
			message = @"Network timeout";
			break;
		case NSURLErrorCannotFindHost:
			message = @"Cannot find host";
			break;
		case NSURLErrorDNSLookupFailed:
			message = @"DNS Lookup error";
			break;
		case NSURLErrorNetworkConnectionLost:
			message = @"Connection lost";
			break;
		case NSURLErrorCannotConnectToHost:
			message = @"Cannot connect to host";
			break;
		case NSURLErrorNotConnectedToInternet:
			message = @"Not connected to internet";
			break;
			
		}
	}
		

	// errors from HTTP or JSON decoding
	else if (status < 0){
		title = @"HTTP error";
		switch (status){
		case JSONTOOLS_JSON_PARSE_ERROR:
			message = @"Reply was not a JSON message";
			break;
		case JSONTOOLS_NO_ROOT_DICTIONNARY:
		case JSONTOOLS_NO_STATUS:
		case JSONTOOLS_NO_BODY_ELT:
			message = [NSString stringWithFormat:@"Incorrect JSON message, error %d", status];
			break;

		case JSONTOOLS_MANDATORY_NOT_FOUND:
			message = @"A mandatory JSON element was not found";
			break;
			
		case JSONTOOLS_MANDATORY_EMPTY:
			message = @"A mandatory JSON array was empty";
			break;


		case JSONTOOLS_API_ERROR:
			message = @"Internal error, WBSAPI called with incorrect initialization";
			break;

		case JSONTOOLS_GETURL_NETWORK_ERROR:
			message = @"Generic URL error";
			break;
		}
	}


	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
											  message:message
											  delegate:nil
											  cancelButtonTitle:@"OK"
											  otherButtonTitles:nil
						  ];
	[alert show];
	[alert autorelease];

}


@end
/*
 * Local Variables: 
 * c-basic-offset:4
 * indent-tabs-mode:t
 * tab-width:4
 * End:
 */
