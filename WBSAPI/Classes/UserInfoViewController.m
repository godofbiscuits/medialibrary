//
//  UserInfoViewController.m
//  wbsapi
//
//  Created by Dev on 11/01/10.
//  Copyright WiThings 2010. All rights reserved.
//

#import "UserInfoViewController.h"
#import "AlertHelper.h"



@implementation UserInfoViewController

@synthesize user_id;
@synthesize user_publickey;




-(void) backgroundNetworkTerminated
{
	[self.tableView reloadData];
	if (wbsapi.status != 0) 
		[AlertHelper displayWebServiceErrorAlertView:wbsapi.status];
}

// Call the webservice from a background thread to not block the UI.
- (void) backgroundNetwork
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];

	WBSAPIUser *l_user =  [wbsapi getUserInfo];
	if (l_user != nil) {
		[user release];
		user = [l_user retain];

		[measures release];
		measures = [[WBSAPIMeasures alloc] init];

		NSDictionary *meas_dict;

		// We fetch the actual measures, and then the weight targets,
		// and parse both in the same catch-all structure.
		// You will probably want to do it differently.
		
		meas_dict = [wbsapi getUserMeasuresWithCategory:WS_CATEGORY_MEASURE];
		if (meas_dict != nil) {
			[measures parseMeasures:meas_dict];

			meas_dict = [wbsapi getUserMeasuresWithCategory:WS_CATEGORY_TARGET];
			if (meas_dict != nil) 
				[measures parseMeasures:meas_dict];

			
		}
	}

	[self performSelectorOnMainThread:@selector(backgroundNetworkTerminated)
		  withObject:nil waitUntilDone:YES];
	
	[pool release];

}


- (void)viewDidAppear:(BOOL)animated
{

	self.navigationItem.title = @"User Details";


	if (!wbsapi) {
		wbsapi = [[WBSAPI alloc] init];
	}
	wbsapi.user_id = self.user_id;
	wbsapi.user_publickey = self.user_publickey;
	
	[NSThread detachNewThreadSelector:@selector(backgroundNetwork)
			  toTarget:self withObject:nil];
	// TODO: Display a UIActivityIndicator when the thread takes time

	[super viewDidAppear:animated];
}


- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

	if (!user)
		return 0;
    return 2;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	if (section == 0)
		return 8;
	else
		return 6;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
	if (section == 0)
		return @"User Info";
	else
		return @"Last measures";
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    UITableViewCell *cell;
    int section = indexPath.section; 
    int row = indexPath.row;


	static NSString *CellIdentifier = @"Cell-detail";
	cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if (cell == nil) {
		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
	}

	if (section == 0)
		switch(row) {
			case 0: cell.textLabel.text = @"1stName"; cell.detailTextLabel.text = user.firstname; break;
			case 1: cell.textLabel.text = @"Name";    cell.detailTextLabel.text = user.lastname; break;
			case 2: cell.textLabel.text = @"Short";   cell.detailTextLabel.text = user.shortname; break;
			case 3: cell.textLabel.text = @"ID";      cell.detailTextLabel.text = [NSString stringWithFormat:@"%d", user.user_id]; break;
			case 4: cell.textLabel.text = @"publickey";   cell.detailTextLabel.text = user.publickey; break;
			case 5: cell.textLabel.text = @"gender";  cell.detailTextLabel.text = (user.gender == 0 ? @"Male" : @"Female"); break;
			case 6: cell.textLabel.text = @"Birthdate"; cell.detailTextLabel.text = [user.birthdate description]; break;
			case 7: cell.textLabel.text = @"fatmethod"; cell.detailTextLabel.text = [NSString stringWithFormat:@"%d", user.fatmethod]; break;
		}
	else
		switch(row) {
			case 0: cell.textLabel.text = @"Weight";  cell.detailTextLabel.text = [NSString stringWithFormat:@"%.02f kg", measures.last_weight]; break;
			case 1: cell.textLabel.text = @"Height";  cell.detailTextLabel.text = [NSString stringWithFormat:@"%.02f m", measures.last_height]; break;

			case 2: cell.textLabel.text = @"Fat";  cell.detailTextLabel.text = [NSString stringWithFormat:@"%.02f kg", measures.last_fat]; break;
			case 3: cell.textLabel.text = @"Fat%";  cell.detailTextLabel.text = [NSString stringWithFormat:@"%.02f %%", measures.last_fatpct]; break;


			case 4: cell.textLabel.text = @"Target weight";  cell.detailTextLabel.text = [NSString stringWithFormat:@"%.02f kg", measures.target_weight]; break;
			case 5: cell.textLabel.text = @"Target fat";  cell.detailTextLabel.text = [NSString stringWithFormat:@"%.02f kg", measures.target_fat]; break;
		}
    

    return cell;
}




- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	NSLog(@"do fetch user/getmeasure");
}


- (void)dealloc {
	self.user_publickey = nil;
	[wbsapi release];
	[user release];
	[measures release];

    [super dealloc];
}


@end


/*
 * Local Variables: 
 * c-basic-offset:4
 * indent-tabs-mode:t
 * tab-width:4
 * End:
 */
