//
//  RootViewController.m
//  wbsapi
//
//  Created by Dev on 11/01/10.
//  Copyright WiThings 2010. All rights reserved.
//

#import "RootViewController.h"
#import "UserListViewController.h"
#import "UserInfoViewController.h"


@implementation RootViewController


#pragma mark Alert view delegate

- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex
{
    int section = edit_path.section; 
    int row = edit_path.row;
	NSString *text = edit_text.text;

	if (buttonIndex != 0) { // 
		if (section == 0 && row == 0) {
			[email release];
			email = [text retain];
		}
		if (section == 0 && row == 1) {
			[password release];
			password = [text retain];
		}

		if (section == 1 && row == 0) {
			int int_id = [text intValue];
			if (int_id != 0)
				userid = int_id;
		}
		if (section == 1 && row == 1) {
			[publickey release];
			publickey = [text retain];
		}
	} 

	[self.tableView reloadData];

	[edit_path release];
	edit_path = nil;

	[edit_text resignFirstResponder];
	[edit_text release];
	edit_text = nil;


}


#pragma mark UITableViewController


- (void)viewDidLoad {


	// Note: the default password/keys are NOT valid.
	// You will get authentication errors.
	// Replace the values by your own test account !

//#error  Please replace the default value by your own test account.

	email = [@"jeff@barbose.name" retain];
	password = [@"black8dog" retain];
	userid = 71717;
	publickey = [@"9bcdb5da05d42538" retain];


	self.navigationItem.title = @"WBSAPI example";

	[super viewDidLoad];

}



- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release anything that can be recreated in viewDidLoad or on demand.
	// e.g. self.myOutlet = nil;
}


#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    UITableViewCell *cell;
    int section = indexPath.section; 
    int row = indexPath.row;

	if ([indexPath compare:edit_path] == NSOrderedSame) {
		static NSString *CellIdentifier = @"Cell-root-edit";
    	cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
		if (cell == nil) {
			cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
		}
		[cell.contentView addSubview:edit_text];
		return cell;

	}

    if (row == 2) {
		static NSString *CellIdentifier = @"Cell-root-action";
    	cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
		if (cell == nil) {
			cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
		}
		if (section == 0)
			cell.textLabel.text = @"Get users list";
		else
			cell.textLabel.text = @"Get measures";
		cell.textLabel.textAlignment = UITextAlignmentCenter;
    } else {
		static NSString *CellIdentifier = @"Cell-root-item";
    	cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
		if (cell == nil) {
			cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
		}
		// Configure the cell.
		if (section == 0) { // account
			if (row == 0) {
				cell.textLabel.text = @"account";
				cell.detailTextLabel.text = email;
			} else {
				cell.textLabel.text = @"password";
				cell.detailTextLabel.text = password;
			}
		} else { // user
			if (row == 0) {
				cell.textLabel.text = @"userid";
				cell.detailTextLabel.text = [NSString stringWithFormat:@"%d", userid];
			} else {
				cell.textLabel.text = @"public key";
				cell.detailTextLabel.text = publickey;
			}
		}


    }
    

    return cell;
}



-(void) popEditViewForTableView:(UITableView *)tableView path:(NSIndexPath *)indexPath
{
    int section = indexPath.section; 
    int row = indexPath.row;

	UITableViewCell *cell = [self tableView:tableView cellForRowAtIndexPath:indexPath];

	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:cell.textLabel.text
												message:@"\n" // room for textfield...
												delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Submit", nil];
	UITextField *textField = [[UITextField alloc] initWithFrame: CGRectMake(12.0f, 45.0f, 260.0f, 25.0f)];
	textField.text = cell.detailTextLabel.text;

	textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
	textField.autocorrectionType = UITextAutocorrectionTypeNo;
	if (section == 0) { 
		if (row == 0) { // account email
			textField.keyboardType = UIKeyboardTypeEmailAddress;
		} else { //password
			textField.keyboardType =  UIKeyboardTypeDefault;
		}
	} else {
		if (row == 0) { // userid
			textField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
		} else { // publickey
			textField.keyboardType = UIKeyboardTypeASCIICapable;
		}
	}

	[textField setBackgroundColor: [UIColor whiteColor]];
	[textField setTextAlignment:UITextAlignmentCenter];
	[textField becomeFirstResponder];
	[alert addSubview: textField];

	edit_text = [textField retain];
	edit_path = [indexPath retain];

	// Move above keyboard
	CGAffineTransform t = CGAffineTransformMakeTranslation(0.0f, 130.0f);
	[alert setTransform: t];

	[alert show];
	[alert release];
	[textField release];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.row == 2) {
		if (indexPath.section == 0) {
			UserListViewController *ul = [[UserListViewController alloc] init];
			ul.email = email;
			ul.password = password;

			[[self navigationController] pushViewController:ul animated:YES];
			[ul release];
		} else {
			UserInfoViewController *ui = [[UserInfoViewController alloc] init];
			ui.user_id = userid;
			ui.user_publickey = publickey;

			[[self navigationController] pushViewController:ui animated:YES];
			[ui release];
			
		}
	} else {
		UITableViewCell *cell = [self tableView:tableView cellForRowAtIndexPath:indexPath];
		cell.selected = NO;
		[self popEditViewForTableView:tableView path:indexPath];
	}


		
}


- (void)dealloc {
    [super dealloc];
}


@end


/*
 * Local Variables: 
 * c-basic-offset:4
 * indent-tabs-mode:t
 * tab-width:4
 * End:
 */
