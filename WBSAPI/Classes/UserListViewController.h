//
//  UserListViewController.h
//  wbsapi
//
//  Created by Dev on 11/01/10.
//  Copyright WiThings 2010. All rights reserved.
//

#import "WBSAPIUser.h"
#import "WBSAPI.h"

@interface UserListViewController : UITableViewController {
    NSString *email;
    NSString *password;

    NSArray *userList; // WBSAPIUser
    WBSAPI  *wbsapi;
}

@property (nonatomic,retain) NSString *email;
@property (nonatomic,retain) NSString *password;

@end
