// -*- objc -*-



@interface AlertHelper : NSObject {
}

// Display an AlertView with a text describing the error code (from WBSAPI.h)
+(void) displayWebServiceErrorAlertView:(int) status;

@end

/*
 * Local Variables: 
 * c-basic-offset:4
 * indent-tabs-mode:t
 * tab-width:4
 * End:
 */
