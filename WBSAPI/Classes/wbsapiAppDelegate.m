//
//  wbsapiAppDelegate.m
//  wbsapi
//
//  Created by Dev on 11/01/10.
//  Copyright WiThings 2010. All rights reserved.
//

#import "wbsapiAppDelegate.h"
#import "RootViewController.h"


@implementation wbsapiAppDelegate

@synthesize window;
@synthesize navigationController;


#pragma mark -
#pragma mark Application lifecycle

- (void)applicationDidFinishLaunching:(UIApplication *)application {    
    
    // Override point for customization after app launch    
	
	[window addSubview:[navigationController view]];
    [window makeKeyAndVisible];
}


- (void)applicationWillTerminate:(UIApplication *)application {
	// Save data if appropriate
}


#pragma mark -
#pragma mark Memory management

- (void)dealloc {
	[navigationController release];
	[window release];
	[super dealloc];
}


@end

