//
//  RootViewController.h
//  wbsapi
//
//  Created by Dev on 11/01/10.
//  Copyright WiThings 2010. All rights reserved.
//

@interface RootViewController : UITableViewController <UIAlertViewDelegate> {
    NSString *email;
    NSString *password;
    int       userid;
    NSString *publickey;

    UITextField *edit_text;
    NSIndexPath *edit_path;
}

@end
