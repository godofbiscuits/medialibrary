//
//  StworyViewController.h
//  Stwory
//
//  Created by Jeff Barbose on 1/11/10.
//  Copyright 2010 HowLand Software. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface StworyViewController : UIViewController <UITextViewDelegate>
{
    UITextView* stworyTextView;
    
    UIView* navBarContentView;
    UITextField* wordCountField;

    UILabel* emptyStworyInstructions;
    
    BOOL dirty;

    
    
    
@private
    BOOL keyboardIsShown;
	NSString* stworyBody;
}


@property (nonatomic, retain) IBOutlet UITextView* stworyTextView;
@property (nonatomic, retain) IBOutlet UITextField* wordCountField;

@property (nonatomic, retain) IBOutlet UILabel* emptyStworyInstructions;

@property (nonatomic, getter=wasModified) BOOL dirty;



// IBActions

- (IBAction) beginStwory:(id)sender;




@end
