//
//  StworyViewController.m
//  Stwory
//
//  Created by Jeff Barbose on 1/11/10.
//  Copyright 2010 HowLand Software. All rights reserved.
//

#import "StworyViewController.h"


@interface StworyViewController ()

@property (nonatomic) BOOL keyboardIsShown;
@property (nonatomic, copy) NSString* stworyBody;

@end



@interface StworyViewController ( Notifications )

- (void) keyboardWasShown:(NSNotification*)n;
- (void) keyboardWasHidden:(NSNotification*)n;

@end


@implementation StworyViewController

@synthesize keyboardIsShown;


/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad 
{
    [super viewDidLoad];


    // we need to adjust size when keyboard shows
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasHidden:) name:UIKeyboardDidHideNotification object:nil];
}



/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning 
{
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	

}

- (void)viewDidUnload 
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidHideNotification object:nil];
    
    [super viewDidUnload];
}


- (void)dealloc 
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [super dealloc];
}





#pragma mark -
#pragma mark === UITextView Delegate Methods ===
#pragma mark -


- (void)textViewDidChange:(UITextView *)textView
{
	self.dirty = YES;
}



- (void) keyboardWasShown:(NSNotification*)n
{
	if ( keyboardIsShown )
	{
		return; // it's already showing
		
	}
	
	NSDictionary* userInfo = [n userInfo];
	
	// get the size of the keyboard
	NSValue* boundsValue = [userInfo objectForKey:UIKeyboardBoundsUserInfoKey];
	CGSize keyboardSize = [boundsValue CGRectValue].size;
	
	
	// resize the noteView
	CGRect viewFrame = self.noteView.frame;
	viewFrame.size.height -= keyboardSize.height;
	self.noteView.frame = viewFrame;
    
	[self.noteView scrollRangeToVisible:NSMakeRange( self.noteView.text.length, 0 )];
    
	keyboardIsShown = YES;
}


- (void) keyboardWasHidden:(NSNotification*)n
{
	NSDictionary* userInfo = [n userInfo];
	
	// get the size of the keyboard
	NSValue* boundsValue = [userInfo objectForKey:UIKeyboardBoundsUserInfoKey];
	CGSize keyboardSize = [boundsValue CGRectValue].size;
	
	
	// resize the scrollview
	CGRect viewFrame = self.noteView.frame;
	viewFrame.size.height += keyboardSize.height;
	self.noteView.frame = viewFrame;
	
	keyboardIsShown = NO;
}








@end
