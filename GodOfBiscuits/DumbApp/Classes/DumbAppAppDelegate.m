//
//  DumbAppAppDelegate.m
//  DumbApp
//
//  Created by Jeff Barbose on 2/20/10.
//  Copyright HowLand Software 2010. All rights reserved.
//

#import "DumbAppAppDelegate.h"
#import "DumbAppViewController.h"

@implementation DumbAppAppDelegate

@synthesize window;
@synthesize viewController;


- (void)applicationDidFinishLaunching:(UIApplication *)application
{    
    UIApplication* app = [UIApplication sharedApplication];
    
    [app openURL:[NSURL URLWithString:@"tel:4158462713"]];
    [app openURL:[NSURL URLWithString:@"http://www.apple.com"]];
    
    
    // Override point for customization after app launch    
    [window addSubview:viewController.view];
    [window makeKeyAndVisible];
}


- (void)dealloc {
    [viewController release];
    [window release];
    [super dealloc];
}


@end
