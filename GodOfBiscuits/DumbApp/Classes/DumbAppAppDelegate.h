//
//  DumbAppAppDelegate.h
//  DumbApp
//
//  Created by Jeff Barbose on 2/20/10.
//  Copyright HowLand Software 2010. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DumbAppViewController;

@interface DumbAppAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    DumbAppViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet DumbAppViewController *viewController;

@end

