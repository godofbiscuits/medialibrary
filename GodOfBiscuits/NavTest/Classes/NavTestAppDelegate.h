//
//  NavTestAppDelegate.h
//  NavTest
//
//  Created by Jeff Barbose on 1/11/10.
//  Copyright HowLand Software 2010. All rights reserved.
//

@interface NavTestAppDelegate : NSObject <UIApplicationDelegate> {
    
    UIWindow *window;
    UINavigationController *navigationController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet UINavigationController *navigationController;

@end

