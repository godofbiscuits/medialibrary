//
//  NavTestAppDelegate.m
//  NavTest
//
//  Created by Jeff Barbose on 1/11/10.
//  Copyright HowLand Software 2010. All rights reserved.
//

#import "NavTestAppDelegate.h"
#import "RootViewController.h"


@implementation NavTestAppDelegate

@synthesize window;
@synthesize navigationController;


#pragma mark -
#pragma mark Application lifecycle

- (void)applicationDidFinishLaunching:(UIApplication *)application {    
    
    // Override point for customization after app launch    
	
	[window addSubview:[navigationController view]];
    [window makeKeyAndVisible];
}


- (void)applicationWillTerminate:(UIApplication *)application {
	// Save data if appropriate
}


#pragma mark -
#pragma mark Memory management

- (void)dealloc {
	[navigationController release];
	[window release];
	[super dealloc];
}


@end

