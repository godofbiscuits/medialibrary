//
//  SOTC_CorePlotTestViewController.h
//  SOTC-CorePlotTest
//
//  Created by Jeff Barbose on 2/20/10.
//  Copyright HowLand Software 2010. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CorePlot-CocoaTouch.h"



@interface SOTC_CorePlotTestViewController : UIViewController <CPPlotDataSource>
{
    CPXYGraph* graph;
}

@end

