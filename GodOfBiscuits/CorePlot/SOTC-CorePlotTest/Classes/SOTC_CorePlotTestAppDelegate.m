//
//  SOTC_CorePlotTestAppDelegate.m
//  SOTC-CorePlotTest
//
//  Created by Jeff Barbose on 2/20/10.
//  Copyright HowLand Software 2010. All rights reserved.
//

#import "SOTC_CorePlotTestAppDelegate.h"
#import "SOTC_CorePlotTestViewController.h"

@implementation SOTC_CorePlotTestAppDelegate

@synthesize window;
@synthesize viewController;


- (void)applicationDidFinishLaunching:(UIApplication *)application {    
    
    // Override point for customization after app launch    
    [window addSubview:viewController.view];
    [window makeKeyAndVisible];
}


- (void)dealloc {
    [viewController release];
    [window release];
    [super dealloc];
}


@end
