//
//  SOTC_CorePlotTestAppDelegate.h
//  SOTC-CorePlotTest
//
//  Created by Jeff Barbose on 2/20/10.
//  Copyright HowLand Software 2010. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SOTC_CorePlotTestViewController;

@interface SOTC_CorePlotTestAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    SOTC_CorePlotTestViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet SOTC_CorePlotTestViewController *viewController;

@end

