//
//  Robot.h
//  575
//
//  Created by Jeff Barbose on 2/9/10.
//  Copyright 2010 HowLand Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Robot;


enum {
    robotSearchStrategyBase = -1,
    robotSearchStrategySeasonal,
    robotSearchStrategyCutting,
    robotSearchStrategyRandom
};

typedef NSUInteger RobotSearchStrategy;



@interface Robot : NSObject
{
    NSIndexPath* location;
    UIImage* face;
    UIImage* trailImage;
    NSString* uuid;
    
    NSString* name;
    RobotSearchStrategy searchStrategy;
}

@property (retain) NSIndexPath* location;

@property (nonatomic, retain) UIImage* face;
@property (nonatomic, retain) UIImage* trailImage;
@property (nonatomic, retain) NSString* uuid;
@property (nonatomic, retain) NSString* name;
@property (nonatomic) RobotSearchStrategy searchStrategy;


+ (Robot*) robotWithFace:(UIImage*)faceImage whichLeavesTrail:(UIImage*)imageForTrail;

- (id) initWithFace:(UIImage*)faceImage andTrailImage:(UIImage*)imageForTrail;



@end
