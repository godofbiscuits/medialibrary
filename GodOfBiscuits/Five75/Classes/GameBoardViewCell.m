//
//  GameBoardViewCell.m
//  575
//
//  Created by Jeff Barbose on 2/9/10.
//  Copyright 2010 HowLand Software. All rights reserved.
//

#import "GameBoardViewCell.h"
#import "UIImage+ImageScaling.h"


@implementation GameBoardViewCell

@synthesize dataSource;


- (id)initWithImage:(UIImage *)image cellWidth:(CGFloat)cellWidth
{
    UIImage* resizedImage = [UIImage cgImage:[image CGImage] sizedTo:cellWidth];
    
    if ( self = [super initWithImage:resizedImage] )
    {
        CGRect newFrame = CGRectMake( self.frame.origin.x, self.frame.origin.y, cellWidth, cellWidth );
        
        self.frame = newFrame;
        
        return self;
    }
    else
    {
        return nil;
    }
}


/*
- (void) setImage:(UIImage*)newImage
{
    UIImage* resizedImage = [UIImage cgImage:[newImage CGImage] sizedTo:self.frame.size.width];
    
    [super setImage:resizedImage];
}
*/




- (void)dealloc 
{
    [super dealloc];
}


@end
