//
//  GameBoardViewController.m
//  Five75
//
//  Created by Jeff Barbose on 2/10/10.
//  Copyright HowLand Software 2010. All rights reserved.
//

#import "GameBoardViewController.h"
#import "Robot.h"
#import "Prize.h"
#import "GameBoard.h"
#import "GameBoardViewCell.h"



#define BOARD_CELL_VIEW_SIZE    36.0
#define BOARD_CELL_SPACER_WIDTH 11.0


NSUInteger kFive75NumberOfRows      = 7;
NSUInteger kFive75NumberOfColumns   = 7;



@interface GameBoardViewController ()


- (void) clearGameBoardView;

@end




@interface GameBoardViewController (Layout)

- (void) layoutGameBoardCells;

@end


/*
@interface GameBoardViewController ( ConversionUtilities )

- (NSUInteger) locationToArrayIndex:(NSIndexPath*)location;
- (NSIndexPath*) arrayIndexToLocation:(NSUInteger)arrayIndex;

@end
*/



@implementation GameBoardViewController

@synthesize bannerLabel, gameBoardView, viewCells;
@synthesize swatch1, score1, swatch2, score2;
@synthesize resetButton;
@synthesize gameBoard;


/*
// The designated initializer. Override to perform setup that is required before the view is loaded.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/



    // Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad 
{
    [super viewDidLoad];
    
    UIColor* boardViewBackgroundColor = [UIColor colorWithRed:0.800 green:0.840 blue:0.855 alpha:1.000];
    
    self.gameBoardView.backgroundColor = boardViewBackgroundColor;

        // set up the GameBoard (Model)
    
    Robot* blueBot = [[Robot alloc] initWithFace:[UIImage imageNamed:@"BlueRobotFace.png"] andTrailImage:[UIImage imageNamed:@"BlueRobotTrail.png"]];
    blueBot.name = NSLocalizedString( @"Kigo", @"" );
    blueBot.searchStrategy = robotSearchStrategySeasonal;
    
    Robot* greenBot = [[Robot alloc] initWithFace:[UIImage imageNamed:@"GreenRobotFace.png"] andTrailImage:[UIImage imageNamed:@"GreenRobotTrail.png"]];
    greenBot.name = NSLocalizedString( @"Kireji", @"" );
    greenBot.searchStrategy = robotSearchStrategyCutting;
    
    gameBoard = [[GameBoard alloc] initWithRows:kFive75NumberOfRows andColumns:kFive75NumberOfColumns player1:blueBot player2:greenBot];
    
    
        // set up the  View    
    
    [self layoutGameBoardCells];
    
    
    [blueBot release];
    [greenBot release];
}




/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}



- (void) layoutGameBoardCells
{
    NSMutableArray* tempArray = [[NSMutableArray alloc] initWithCapacity:self.gameBoard.gameBoardCells.count];
    
    UIImage* blankCellImage = [UIImage imageNamed:@"EmptyBoardCellImage.png"];
    GameBoardViewCell* aViewCell = nil;

    CGFloat currentX = 0.0;
    CGFloat currentY = 0.0;
    
    NSUInteger masterIndex = 0;
    
    
    for ( NSUInteger boardRow = 0 ; boardRow < self.gameBoard.rows ; boardRow++ )
    {
        for ( NSUInteger boardColumn = 0 ; boardColumn < self.gameBoard.columns ; boardColumn++ )
        {
            aViewCell = [[GameBoardViewCell alloc] initWithImage:blankCellImage cellWidth:BOARD_CELL_VIEW_SIZE];
            aViewCell.opaque = YES;
            
            
            NSLog( @"laying out:[%f,%f]", currentX, currentY );

            CGRect newFrame = CGRectMake( currentX, currentY, aViewCell.frame.size.width, aViewCell.frame.size.height );
            aViewCell.frame = newFrame;
            
            currentX += ( BOARD_CELL_VIEW_SIZE + BOARD_CELL_SPACER_WIDTH );  // after laying out a cell, the origin for X increases by width of a cell + spacer
            
            aViewCell.dataSource = self;
            
            masterIndex++;
            
            [self.gameBoardView addSubview:aViewCell];

            [tempArray addObject:aViewCell];
                        
            [aViewCell release];
        }
        
            // at the end of laying out a row, the origin for x goes to zero, and Y increases by the height of a cell plus a spacer
        
        currentX = 0.0;
        currentY += ( BOARD_CELL_VIEW_SIZE + BOARD_CELL_SPACER_WIDTH );
    }
    
    viewCells = [[NSArray alloc] initWithArray:tempArray];
    
    [tempArray release];
}





- (void) resetAfterPrizeFound
{    
        // reset to empty board
    [self clearGameBoardView];
    
        // let the model place the objects
    [self.gameBoard resetBoard];

        // we're dirty
    [self.gameBoardView setNeedsDisplay];
    
    
/*    
    // the board reset itself, now place the game objects (2 robots, 1 prize) into the view
    
    NSUInteger robot1Index = [self locationToArrayIndex:self.gameBoard.robot1.location];
    NSUInteger robot2Index = [self locationToArrayIndex:self.gameBoard.robot2.location];
    NSUInteger prizeIndex  = [self locationToArrayIndex:self.gameBoard.prizeLocation];
    
    [self clearGameBoardView];

    [[self.viewCells objectAtIndex:robot1Index] setImage:self.gameBoard.robot1.face];
    [[self.viewCells objectAtIndex:robot2Index] setImage:self.gameBoard.robot2.face];
    [[self.viewCells objectAtIndex:prizeIndex]  setImage:self.gameBoard.prize.prizeImage];
 */
}


- (void) clearGameBoardView
{
    for ( GameBoardViewCell* viewCell in viewCells )
    {
        [viewCell setImage:[UIImage imageNamed:@"EmptyBoardCellImage.png"]];
    }
}







#pragma mark -
#pragma mark === IBActions ===
#pragma mark -

- (IBAction) toggleStartStop:(id)sender
{
    
}


- (IBAction) resetScores:(id)sender
{
    
}



/*
#pragma mark -
#pragma mark === Conversion Utilities ===
#pragma mark -



- (NSUInteger) locationToArrayIndex:(NSIndexPath*)location
{
    NSUInteger indexes[] = {0,0};
    
    [location getIndexes:indexes];
    
    NSUInteger arrayIndex = ( indexes[0] * self.columns ) + ( indexes[1] + 1 );
    
    return arrayIndex;
}




- (NSIndexPath*) arrayIndexToLocation:(NSUInteger)arrayIndex
{
    NSUInteger indexes[] = {0,0};
    
    NSUInteger total = self.gameBoardCells.count;
    
    indexes[0] = total / self.columns;
    indexes[1] = total % self.columns;
    
    NSIndexPath* indexPath = [NSIndexPath indexPathWithIndexes:indexes length:2];
    
    return indexPath;
}

*/



#pragma -
#pragma === GameBoardCellDataSource Methods ===
#pragma -



- (UIImage*) boardViewCell:(GameBoardViewCell*)boardViewCell imageForBoardCell:(GameBoardCell*)boardCell
{
    if ( YES ) //self.occupant != nil )
    {
        return nil; //[self.occupant imageForBoardCell:self];
    }
    else
    {
        return nil;
    }
}


- (NSString*) boardViewCell:(GameBoardViewCell*)boardViewCell occupantIDStringForBoardCell:(GameBoardCell*)boardCell
{
    if ( YES ) //self.occupant != nil )
    {
        return nil; //[self.occupant occupantIDStringForBoardCell:self];
    }
    else
    {
        return nil;
    }
}



@end
