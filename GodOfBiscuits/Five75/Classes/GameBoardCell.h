//
//  GameBoardCell.h
//  575
//
//  Created by Jeff Barbose on 2/10/10.
//  Copyright 2010 HowLand Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GameBoardCellDataSource.h"



@interface GameBoardCell : NSObject 
{
    id occupant;
    
    NSIndexPath* location;
    
    BOOL hasPrize;
    BOOL isOccupied;
}

@property (assign, setter=setOccupant:) id occupant;
@property (nonatomic, retain) NSIndexPath* location;
@property (nonatomic, readonly) BOOL hasPrize;
@property (readonly) BOOL isOccupied;



- (GameBoardCell*) initWithIndexPath:(NSIndexPath*)location;




@end
