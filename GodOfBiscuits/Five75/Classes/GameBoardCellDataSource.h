//
//  GameBoardCellDataSource.h
//  575
//
//  Created by Jeff Barbose on 2/10/10.
//  Copyright 2010 HowLand Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GameBoardCell;
@class GameBoardViewCell;

@protocol GameBoardCellDataSource <NSObject>

@required
- (UIImage*) boardViewCell:(GameBoardViewCell*)boardViewCell imageForBoardCell:(GameBoardCell*)boardCell;
- (NSString*) boardViewCell:(GameBoardViewCell*)boardViewCell occupantIDStringForBoardCell:(GameBoardCell*)boardCell;


@end
