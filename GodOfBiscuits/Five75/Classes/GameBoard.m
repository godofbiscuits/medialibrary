//
//  GameBoard.m
//  575
//
//  Created by Jeff Barbose on 2/9/10.
//  Copyright 2010 HowLand Software. All rights reserved.
//

#import "GameBoard.h"
#import "Five75AppDelegate.h"

#import "Robot.h"
#import "Prize.h"
#import "GameBoardCell.h"


@interface GameBoard ()

@property (nonatomic, retain) NSIndexPath* prizeLocation;

@property (nonatomic, retain) Prize* prize;

@property (nonatomic, retain) Robot* robot1;
@property (nonatomic, retain) Robot* robot2;


@end



@interface GameBoard ( ConversionUtilities )

- (NSUInteger) locationToArrayIndex:(NSIndexPath*)location;
- (NSIndexPath*) arrayIndexToLocation:(NSUInteger)arrayIndex;

@end


@interface GameBoard ( Images )



@end





@implementation GameBoard

@synthesize gameBoardCells;
@synthesize rows, columns;
@synthesize robot1, robot2;
@synthesize prizeLocation, prize;







- (id) initWithRows:(NSUInteger)numRows andColumns:(NSUInteger)numCols player1:(Robot*)bot1 player2:(Robot*)bot2
{
    if ( ( bot1 != nil ) && ( bot2 != nil ) && ( self = [super init] ) )
    {
        
            // assign the robots
        
        robot1 = [bot1 retain];
        robot2 = [bot2 retain];
        
        
            // assign the dimensions and create the array of game board cells
        
        rows = numRows;
        columns = numCols;
        
        NSUInteger cellCount = rows * columns;
        
        NSMutableArray* tempArray = [NSMutableArray arrayWithCapacity:cellCount];
        
        NSUInteger row = 0;
        NSUInteger col = 0;
        NSUInteger boardIndices[] = {0,0};
                
        for ( row = 0 ; row < self.rows ; row++ )
        {
            for ( col = 0 ; col < self.columns; col++ )
            {
                boardIndices[0] = row;
                boardIndices[1] = col;
                
                NSIndexPath* ip = [NSIndexPath indexPathWithIndexes:boardIndices length:2];
                
                GameBoardCell* cell = [[GameBoardCell alloc] initWithIndexPath:ip];
                
                [tempArray addObject:cell];
                
                [cell release];
            }
        }
        
        gameBoardCells = [[NSArray alloc] initWithArray:tempArray];
        
        
            // create the prize object
        
        prize = [[Prize alloc] initWithImage:[UIImage imageNamed:@"PrizeImage.png"]];
        
        
            // don't ever create a game in a weird state
            
        [self resetBoard];
        
        
        return self;
    }
    else
    {
        return nil;
    }
}




- (void) resetBoard
{
    NSUInteger robot1Indices[] = { 0, ( self.columns - 1 ) };
    NSUInteger robot2Indices[] = { ( self.rows - 1 ), 0 };
    
    // reset robot locations
    
    self.robot1.location = [NSIndexPath indexPathWithIndexes:robot1Indices length:2];
    self.robot2.location = [NSIndexPath indexPathWithIndexes:robot2Indices length:2];


    // reset prize location (to a non-occupied cell)
    
    NSUInteger newPrizeIndices[] = {0,0};
    
    newPrizeIndices[0] = arc4random() % self.rows;
    newPrizeIndices[1] = arc4random() % self.columns;
    
    // to avoid visual confusion at the start of a run, don't let the prize occupy a robot initial location
    // no real reason why a prize couldn't be dropped on a robot's starting position.

    // obviously comparing indexPaths for readability, otherwise would just compare actual integer arrays.
    
    self.prizeLocation = [NSIndexPath indexPathWithIndexes:newPrizeIndices length:2];
    
    while ( ( [self.prizeLocation compare:self.robot1.location] == NSOrderedSame ) || ( [self.prizeLocation compare:self.robot1.location] == NSOrderedSame ) )
    {
        newPrizeIndices[0] = arc4random() % self.rows;
        newPrizeIndices[1] = arc4random() % self.columns;
        
        self.prizeLocation = [NSIndexPath indexPathWithIndexes:newPrizeIndices length:2];
    }
}



- (NSArray*) possibleMoves:(Robot*)robot
{
    
    
    
    
    
    return nil;
}





- (NSUInteger) locationToArrayIndex:(NSIndexPath*)location
{
    NSUInteger indexes[] = {0,0};
    
    [location getIndexes:indexes];
    
    NSUInteger arrayIndex = ( indexes[0] * self.columns ) + ( indexes[1] + 1 );
    
    return arrayIndex;
}




- (NSIndexPath*) arrayIndexToLocation:(NSUInteger)arrayIndex
{
    NSUInteger indexes[] = {0,0};

    NSUInteger total = self.gameBoardCells.count;
    
    indexes[0] = total / self.columns;
    indexes[1] = total % self.columns;
    
    NSIndexPath* indexPath = [NSIndexPath indexPathWithIndexes:indexes length:2];
    
    return indexPath;
}








@end
