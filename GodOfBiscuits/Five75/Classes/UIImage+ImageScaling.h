//
//  UIImage+ImageScaling.h
//  gEmpFind
//
//  Created by Jeff Barbose on 26-11-08.
//  Copyright 2009 Jeffrey J Barbose, Inc. All rights reserved.
//



@interface UIImage (ImageScaling) 

	
+ (UIImage*) imageWithCGImage:(CGImageRef)cgImage scaledBy:(CGFloat)scalar;
+ (UIImage*) imageWithData:(NSData*)imageData sizedToMax:(CGFloat)maxImageSize;
+ (UIImage*) imageNamed:(NSString*)imageName scaledBy:(CGFloat)scalar;

+ (UIImage*) cgImage:(CGImageRef)cgImage sizedTo:(CGFloat)newImageHeight;
+ (UIImage*) imageNamed:(NSString*)imageName sizedTo:(CGFloat)newImageHeight;

+ (UIImage*) rotatedImageForExternalUseFromImage:(UIImage*)image;

@end
