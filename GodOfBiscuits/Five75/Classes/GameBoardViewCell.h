//
//  GameBoardViewCell.h
//  575
//
//  Created by Jeff Barbose on 2/9/10.
//  Copyright 2010 HowLand Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GameBoardCellDataSource.h"



@interface GameBoardViewCell : UIImageView 
{
    id<GameBoardCellDataSource> dataSource;
    
    
}

@property (assign) id<GameBoardCellDataSource> dataSource;


- (id)initWithImage:(UIImage *)image cellWidth:(CGFloat)cellWidth;


@end
