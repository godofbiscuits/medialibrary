//
//  Robot.m
//  575
//
//  Created by Jeff Barbose on 2/9/10.
//  Copyright 2010 HowLand Software. All rights reserved.
//

#import "Robot.h"

#import "GameBoardCell.h";






@implementation Robot

@synthesize location;
@synthesize face, trailImage;
@synthesize uuid, name;
@synthesize searchStrategy;



+ (Robot*) robotWithFace:(UIImage*)faceImage whichLeavesTrail:(UIImage*)imageForTrail
{
    Robot* aRobot = [[Robot alloc] initWithFace:faceImage andTrailImage:imageForTrail];
    
    return [aRobot autorelease];
}




- (id) initWithFace:(UIImage*)faceImage andTrailImage:(UIImage*)imageForTrail
{
    if ( self = [super init] )
    {
        CFUUIDRef idRef = CFUUIDCreate( NULL );
        
        uuid = (NSString*)CFUUIDCreateString( NULL, idRef );
        
        CFRelease( idRef );
        
        NSUInteger zeroIndices[] = {0,0};
        
        location = [NSIndexPath indexPathWithIndexes:zeroIndices length:2];   
        
        searchStrategy = robotSearchStrategyRandom;
        
        name = [[uuid substringFromIndex:( uuid.length - 4 )] retain];   // set default name just in case name doesn't get set.  last 3 chars of uuid should be locally unique
        
        face = [faceImage retain];
        trailImage = [imageForTrail retain];
        
        return self;
    }
    else
    {
        return nil;
    }
}





- (void) dealloc
{
    [location release];
    [face release];
    [trailImage release];
    [uuid release];
    [name release];
    
    [super dealloc];
}



@end
