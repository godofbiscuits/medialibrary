//
//  Five75AppDelegate.m
//  Five75
//
//  Created by Jeff Barbose on 2/10/10.
//  Copyright HowLand Software 2010. All rights reserved.
//

#import "Five75AppDelegate.h"
#import "GameBoardViewController.h"



@implementation Five75AppDelegate

@synthesize window;
@synthesize viewController;


+ (Five75AppDelegate*) five75AppDelegate
{
    return (Five75AppDelegate*)( [UIApplication sharedApplication].delegate );
}



- (void)applicationDidFinishLaunching:(UIApplication *)application
{    
    
    
    // Override point for customization after app launch    
    [window addSubview:viewController.view];
    [window makeKeyAndVisible];
}


- (void)dealloc 
{
    [viewController release];
    [window release];
    
    [super dealloc];
}


@end
