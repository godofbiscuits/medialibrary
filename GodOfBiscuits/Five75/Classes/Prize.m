//
//  Prize.m
//  575
//
//  Created by Jeff Barbose on 2/10/10.
//  Copyright 2010 HowLand Software. All rights reserved.
//

#import "Prize.h"

NSString* kPrizeIdString = \
@"Standard Nine it was,\
what lured me with Wond’rous\
Possibilities!";


@implementation Prize

@synthesize prizeImage;

+ (Prize*) prizeWithImage:(UIImage*)imageForPrize
{
    Prize* aPrize = [[Prize alloc] initWithImage:imageForPrize];
    
    return [aPrize autorelease];
}



- (Prize*) initWithImage:(UIImage*)imageForPrize
{
    if ( self = [super init] )
    {
        prizeImage = [imageForPrize retain];
        
        return self;
    }
    else 
    {
        return nil;
    }

}




@end
