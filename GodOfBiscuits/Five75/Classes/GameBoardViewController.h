//
//  GameBoardViewController.h
//  Five75
//
//  Created by Jeff Barbose on 2/10/10.
//  Copyright HowLand Software 2010. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GameBoardCellDataSource.h"



extern NSUInteger kFive75NumberOfRows;
extern NSUInteger kFive75NumberOfColumns;




@class GameBoard;

@interface GameBoardViewController : UIViewController <GameBoardCellDataSource>

{
    UILabel* bannerLabel;
    UIView* gameBoardView;
    NSArray* viewCells;
    
    
    // box scores
    UIImageView* swatch1;
    UILabel* score1;
    UIImageView* swatch2;
    UILabel* score2;
    

    //UIButton* startStopButton;
    UIButton* resetButton;
    
    GameBoard* gameBoard;
}

@property (nonatomic, retain) IBOutlet UILabel* bannerLabel;
@property (nonatomic, retain) IBOutlet UIView* gameBoardView;
@property (nonatomic, retain) NSArray* viewCells;

@property (nonatomic, retain) IBOutlet UIImageView* swatch1;
@property (nonatomic, retain) IBOutlet UILabel* score1;
@property (nonatomic, retain) IBOutlet UIImageView* swatch2;
@property (nonatomic, retain) IBOutlet UILabel* score2;

    // need a ref to this for enabling/disabling
@property (nonatomic, retain) IBOutlet UIButton* resetButton;

@property (nonatomic, retain) GameBoard* gameBoard;






// IBActions

- (IBAction) toggleStartStop:(id)sender;
- (IBAction) resetScores:(id)sender;


@end

