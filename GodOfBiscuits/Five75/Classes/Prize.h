//
//  Prize.h
//  575
//
//  Created by Jeff Barbose on 2/10/10.
//  Copyright 2010 HowLand Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GameBoardCellDataSource.h"

@class Prize;

extern NSString* kPrizeIdString;


@interface Prize : NSObject
{
    UIImage* prizeImage;
}

@property (nonatomic, retain) UIImage* prizeImage;

+ (Prize*) prizeWithImage:(UIImage*)imageForPrize;

- (Prize*) initWithImage:(UIImage*)imageForPrize;


@end
