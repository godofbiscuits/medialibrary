//
//  GameBoardCell.m
//  575
//
//  Created by Jeff Barbose on 2/10/10.
//  Copyright 2010 HowLand Software. All rights reserved.
//

#import "GameBoardCell.h"
#import "Prize.h"
#import "Robot.h"



@interface GameBoardCell ()

@property (nonatomic) BOOL hasPrize;
@property BOOL isOccupied;

@end



@implementation GameBoardCell

@synthesize occupant;
@synthesize location;
@synthesize hasPrize;
@synthesize isOccupied;


- (GameBoardCell*) initWithIndexPath:(NSIndexPath*)location
{
    if ( self = [super init] )
    {
        hasPrize = NO;
        isOccupied = NO;
        occupant = nil;
        
        return self;
    }
    else
    {
        return nil;
    }
}





- (void) setOccupant:(id)newOccupant
{
    occupant = newOccupant;
    
    if ( occupant != nil )
    {
        if ( [occupant isMemberOfClass:[Prize class]] )
        {
            self.hasPrize = YES;
        }
    }
}








@end
