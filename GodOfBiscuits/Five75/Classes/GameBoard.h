//
//  GameBoard.h
//  575
//
//  Created by Jeff Barbose on 2/9/10.
//  Copyright 2010 HowLand Software. All rights reserved.
//

#import <Foundation/Foundation.h>





@class Robot;
@class Prize;




@interface GameBoard : NSObject
{
    NSArray* gameBoardCells;
    
    NSUInteger rows;
    NSUInteger columns;
    
    NSUInteger score1;
    NSUInteger score2;

    
@private
    NSIndexPath* prizeLocation;
    
    Prize* prize;
    
    Robot* robot1;
    Robot* robot2;
}

@property (nonatomic, retain) NSArray* gameBoardCells;
@property (nonatomic, readonly) NSUInteger rows;
@property (nonatomic, readonly) NSUInteger columns;

@property (nonatomic, retain, readonly) Robot* robot1;
@property (nonatomic, retain, readonly) Robot* robot2;
@property (nonatomic, retain, readonly) NSIndexPath* prizeLocation;


- (id) initWithRows:(NSUInteger)numRows andColumns:(NSUInteger)numCols player1:(Robot*)bot1 player2:(Robot*)bot2;


    // once a single run is won, reset the board
- (void) resetBoard;


- (NSArray*) possibleMoves:(Robot*)robot;


@end
