//
//  ViewAppDelegate.m
//  View
//
//  Created by Jeff Barbose on 2/10/10.
//  Copyright HowLand Software 2010. All rights reserved.
//

#import "ViewAppDelegate.h"
#import "ViewViewController.h"

@implementation ViewAppDelegate

@synthesize window;
@synthesize viewController;


- (void)applicationDidFinishLaunching:(UIApplication *)application {    
    
    // Override point for customization after app launch    
    [window addSubview:viewController.view];
    [window makeKeyAndVisible];
}


- (void)dealloc {
    [viewController release];
    [window release];
    [super dealloc];
}


@end
