//
//  ViewAppDelegate.h
//  View
//
//  Created by Jeff Barbose on 2/10/10.
//  Copyright HowLand Software 2010. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewViewController;

@interface ViewAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    ViewViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet ViewViewController *viewController;

@end

