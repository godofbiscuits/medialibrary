<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
 <title>iTiVo Changes</title>
</head>
<body>
<h2>Changes in 1.7.6</h2>
<ul>
<li> switched Installation format from zip file to Disk Image (dmg)</li>
<li> Moved logfile to ~/Library/Logs/iTiVo.log .  Logging now always enabled </li>
<li> Add an advanced preference to control number of attempts per show </li>
<li> New Formats added:
<ul>
<li> ffmpeg mpeg-2 </li>
<li> DivX 5 </li>
</ul>
</li>
<li> Remove copyright section in pytivo 'description' field. </li>
<li> Change copy-restricted icon to brighter red with clearer "c" </li>
<li> bugfix: Do not time out on files larger than 10G</li>
<li> bugfix: Spaces in foldernames were causing issues</li>
<li> Bugfix: Elgato h.264 should now correctly deal with multiple downloads </li>
<li> Bugfix: pyTivo metadata on powerPC machines was broken </li>
</ul>

<h2>Changes in 1.7.5</h2>
<ul>
<li> New option to extract subtitles into .srt file </li>
</ul>

<h2>Changes in 1.7.4</h2>
<ul>
<li> Add option to put computer to sleep after processing queue</li>
</ul>

<h2>Changes in 1.7.3</h2>
<ul>
<li> Display shows that are copyrighted, (shows will not be selectable)</li>
<li> Allow for scheduling times for when queue processing may be automatically initiated </li>
</ul>

<h2>Changes in 1.7.2</h2>
<ul>
<li>emergency fix for a bug in encoder-progress.pl</li>
</ul>

<h2>Changes in 1.7.1</h2>
<ul>
<li>Option to control auto-connect at startup</li>
<li>Fix Applescript bug with prefs Panel</li>
<li>Add support for ffmpeg as encoder choice (requires pyTivoX in /Applications or another working build of ffmpeg to be installed)</li>
<li>Bug fixes with encode formats:
<ul><li>  DVD</li>
  <li>  mpeg-2</li>
</ul>
<li>Add formats:<ul>
<li>  youtube HD</li>
<li>  quicktime superfast (mpeg-1)</li>
<li>  quicktime fast (divx3)</li>
<li>  FFmpeg (for testing).</li>
</ul>
<li>Update to latest versions of:<ul>
<li>  x264</li>
<li>  mencoder </li>
</ul>
</ul>

<h2> Changes in 1.7</h2>
<ul>
<li>Save new user-defined formats</li>
<li>Add support for metadata:<ul>
<li>  pyTivo support</li>
<li>  AtomicParsley support</li>
<li>  .xml from .tivo</li>
</ul>
<li>Store shows in subdirs</li>
<li>Add 6-channel audio to PS3</li>
<li>update x264, mp3lame, mencoder</li>
<li>build with some new flags for powerpc</li>
<li>remove tags that were breaking pytivo</li>
<li>Fix elgato encoder bug</li>
<li>Move files in /tmp to /tmp/iTiVo-<user>/</li>
<li>Do not exit when closing window</li>
<li>More info in tivo space drawer
