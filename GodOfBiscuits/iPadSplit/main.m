//
//  main.m
//  iPadSplit
//
//  Created by Jeff Barbose on 3/19/10.
//  Copyright HowLand Software 2010. All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[]) {
    
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
    int retVal = UIApplicationMain(argc, argv, nil, nil);
    [pool release];
    return retVal;
}
