//
//  TorchMeThereAppDelegate.h
//  TorchMeThere
//
//  Created by Jeff Barbose on 7/23/10.
//  Copyright 2010 HowLand Software. All rights reserved.
//


#import <UIKit/UIKit.h>

@class TorchMeThereViewController;

@interface TorchMeThereAppDelegate : NSObject <UIApplicationDelegate>
{

    UIWindow *window;

    TorchMeThereViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;


@property (nonatomic, retain) IBOutlet TorchMeThereViewController *viewController;

@end

