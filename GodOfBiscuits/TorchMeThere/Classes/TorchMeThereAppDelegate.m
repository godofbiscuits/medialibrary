//
//  TorchMeThereAppDelegate.m
//  TorchMeThere
//
//  Created by Jeff Barbose on 7/23/10.
//  Copyright 2010 HowLand Software. All rights reserved.
//


#import "TorchMeThereAppDelegate.h"

#import "TorchMeThereViewController.h"

@implementation TorchMeThereAppDelegate


@synthesize window;

@synthesize viewController;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions 
{

    // Override point for customization after application launch.
    // Override point for customization after app launch. 
    [window addSubview:viewController.view];
    [window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillTerminate:(UIApplication *)application 
{

    // Save data if appropriate.
}

- (void)dealloc 
{

    [window release];
    [viewController release];
    [super dealloc];
}

@end

