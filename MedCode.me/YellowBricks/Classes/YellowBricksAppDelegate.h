//
//  YellowBricksAppDelegate.h
//  YellowBricks
//
//  Created by Jeff Barbose on 6/7/10.
//  Copyright 2010 HowLand Software. All rights reserved.
//


#import <UIKit/UIKit.h>

@interface YellowBricksAppDelegate : NSObject <UIApplicationDelegate, UITabBarControllerDelegate> {
    UIWindow *window;

    UITabBarController *tabBarController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;


@property (nonatomic, retain) IBOutlet UITabBarController *tabBarController;

@end

