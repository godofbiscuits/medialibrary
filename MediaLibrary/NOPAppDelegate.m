//
//  NOPAppDelegate.m
//  MediaLibrary
//
//  Created by Jeff Barbose on 6/16/13.
//  Copyright (c) 2013 Jeff Barbose. All rights reserved.
//


@import MediaLibrary;

#import "NOPAppDelegate.h"
#import "NOPImageFeed.h"



@interface NOPAppDelegate ()

@property (strong, nonatomic) NOPImageFeed* imageFeed;
@property (strong, nonatomic) MLMediaLibrary* library;


@end


@implementation NOPAppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    self.library = [[MLMediaLibrary alloc] initWithOptions:@{ MLMediaLoadSourceTypesKey: @(MLMediaSourceTypeImage),
                                                              MLMediaLoadIncludeSourcesKey: @[ MLMediaSourceiPhotoIdentifier, MLMediaSourceApertureIdentifier ],
                                                              MLMediaLoadExcludeSourcesKey: @[ MLMediaSourcePhotoBoothIdentifier ] }];
//    self.library = [[MLMediaLibrary alloc] initWithOptions:@{ MLMediaLoadIncludeSourcesKey: @[ MLMediaSourceiPhotoIdentifier, MLMediaSourceApertureIdentifier ] }];
    
    self.imageFeed = [[NOPImageFeed alloc] initWithLibrary:self.library];
    

    

}

@end
