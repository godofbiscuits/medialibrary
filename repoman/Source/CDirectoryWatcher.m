//
//  CDirectoryWatcher.m
//  Repoman
//
//  Created by Jonathan Wight on 01/04/10.
//  Copyright 2010 toxicsoftware.com. All rights reserved.
//

#import "CDirectoryWatcher.h"

static void MyCallback(ConstFSEventStreamRef streamRef, void *clientCallBackInfo, size_t numEvents, void *eventPaths, const FSEventStreamEventFlags eventFlags[], const FSEventStreamEventId eventIds[]);

@interface CDirectoryWatcher ()

@property (readwrite, assign) BOOL started;
@property (readwrite, retain) NSMutableDictionary *blocksForPath;
@property (readwrite, retain) NSArray *paths;
@property (readwrite, assign) FSEventStreamRef stream;
@property (readwrite, assign) FSEventStreamEventId lastEventID;

@end

#pragma mark -

@implementation CDirectoryWatcher

@synthesize started;
@synthesize blocksForPath;
@synthesize paths;
@synthesize stream;
@synthesize lastEventID;

//+ (void)load
//{
//NSAutoreleasePool *thePool = [[NSAutoreleasePool alloc] init];
//
//DirectoryWatcherBlock theBlock = ^(NSString *path, FSEventStreamEventFlags flag, FSEventStreamEventId eventId) {
//	NSLog(@"%@ %d %d", path, eventId, flag);
//	};
//
//[[CDirectoryWatcher instance] addBlock:theBlock forPath:@"/Users/schwa/Desktop"];
//
//[[CDirectoryWatcher instance] start];
//
//[[CDirectoryWatcher instance] addBlock:theBlock forPath:@"/Users/schwa/Pictures"];
//
//[thePool release];
//}

static CDirectoryWatcher *gInstance = NULL;

+ (CDirectoryWatcher *)instance
{
if (gInstance == NULL)
	gInstance = [[self alloc] init];
return(gInstance);
}

- (id)init
{
if ((self = [super init]) != NULL)
	{
	started = YES;
	lastEventID = kFSEventStreamEventIdSinceNow;
	blocksForPath = [[NSMutableDictionary alloc] init];
	}
return(self);
}

- (void)dealloc
{
[self stop];
//
[super dealloc];
}

- (void)addBlock:(DirectoryWatcherBlock)inBlock forPath:(NSString *)inPath
{
@synchronized(self)
	{
	[self.blocksForPath setObject:inBlock forKey:inPath];

	NSArray *thePaths = [self.blocksForPath allKeys];
	if ([self.paths isEqual:thePaths] == NO)
		{
		BOOL theWasStartedFlag = self.started;
		if (theWasStartedFlag)
			[self stop];
		self.paths = thePaths;
		if (theWasStartedFlag)
			[self start];
		}
	}
}

- (void)addBlock:(DirectoryWatcherBlock)inBlock paths:(NSArray *)inPaths identifier:(NSString *)inIdentifier
{
}

- (void)start
{
@synchronized(self)
	{
	if (self.stream == NULL)
		{
		FSEventStreamContext theContext = {
			.version = 0,
			.info = self,
			.retain = NULL,
			.release = NULL,
			};
			
		self.stream = FSEventStreamCreate(kCFAllocatorDefault, MyCallback, &theContext, (CFArrayRef)self.paths, self.lastEventID, 1.0, kFSEventStreamCreateFlagUseCFTypes);

		FSEventStreamScheduleWithRunLoop(self.stream, CFRunLoopGetMain(), kCFRunLoopCommonModes);

		FSEventStreamStart(self.stream);

		NSLog(@"STARTED");
		}

	if (self.started == NO)
		self.started = YES;
	}
}

- (void)stop
{
@synchronized(self)
	{
	if (self.stream != NULL)
		{
		NSLog(@"STOPPING");
		
		FSEventStreamEventId theLastEventID = FSEventStreamGetLatestEventId(self.stream);
		self.lastEventID = theLastEventID;
		
		FSEventStreamStop(self.stream);
		FSEventStreamInvalidate(self.stream);
		FSEventStreamRelease(self.stream);
		self.stream = NULL;
		}
		
	if (self.started == YES)
		self.started = NO;
	}
}

@end

static void MyCallback(ConstFSEventStreamRef streamRef, void *clientCallBackInfo, size_t numEvents, void *eventPaths, const FSEventStreamEventFlags eventFlags[], const FSEventStreamEventId eventIds[])
{
CDirectoryWatcher *self = (CDirectoryWatcher *)clientCallBackInfo;
//
NSArray *theEventPaths = (NSArray *)eventPaths;

NSUInteger N = 0;
for (NSString *theEventPath in theEventPaths)
	{
	FSEventStreamEventFlags theEventFlags = eventFlags[N];
	FSEventStreamEventId theEventID = eventIds[N];

	BOOL theMatch = NO;
	for (NSString *thePath in [[[self.blocksForPath allKeys] copy] autorelease])
		{
		NSString *theStandardizedPath = [[thePath stringByStandardizingPath] stringByResolvingSymlinksInPath];
		
		if ([theEventPath hasPrefix:theStandardizedPath])
			{
			DirectoryWatcherBlock theBlock = [self.blocksForPath objectForKey:thePath];
			theBlock(theEventPath, theEventFlags, theEventID);
			}
		}
	
	++N;
	}
}