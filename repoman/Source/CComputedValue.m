//
//  CComputedValue.m
//  Repoman
//
//  Created by Jonathan Wight on 12/23/09.
//  Copyright 2009 toxicsoftware.com. All rights reserved.
//

#import "CComputedValue.h"

@implementation CComputedValue

@synthesize state;
@synthesize value;
@synthesize computeBlock;
@synthesize executeBlock;

- (id)value
{
if (self.state == ComputedValueState_Computing)
	{
//	NSLog(@"Computing in progress!");
	}
else if (self.state == ComputedValueState_Unknown)
	{
	if (self.computeBlock && self.executeBlock)
		{
		self.state = ComputedValueState_Computing;
//		dispatch_block_t theUpdateBlock = ^{
//			self.state = ComputedValueState_Computing;
//			};
//		dispatch_async(dispatch_get_main_queue(), theUpdateBlock);

		dispatch_block_t theComputeBlock = ^{

			id theNewValue = self.computeBlock();

			dispatch_block_t theUpdateBlock = ^{
				self.value = theNewValue;
				self.state = ComputedValueState_Assigned;
				};
			dispatch_async(dispatch_get_main_queue(), theUpdateBlock);
			};
					
		self.executeBlock(theComputeBlock);
		}
	}

return(value);
}

@end
