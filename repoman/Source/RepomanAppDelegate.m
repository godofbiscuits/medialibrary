//
//  RepomanAppDelegate.m
//  Repoman
//
//  Created by Jonathan Wight on 12/23/09.
//  Copyright 2009 toxicsoftware.com. All rights reserved.
//

#import "RepomanAppDelegate.h"

#import "CProjectWindowController.h"

@implementation RepomanAppDelegate

@synthesize windowController;

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
self.windowController = [[[CProjectWindowController alloc] init] autorelease];
[self.windowController showWindow:NULL];
}

@end
