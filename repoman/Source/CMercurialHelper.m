//
//  CMercurialHelper.m
//  Repoman
//
//  Created by Jonathan Wight on 12/26/09.
//  Copyright 2009 toxicsoftware.com. All rights reserved.
//

#import "CMercurialHelper.h"


static CMercurialHelper *gInstance = NULL;

@implementation CMercurialHelper

+ (CMercurialHelper *)instance
{
if (gInstance == NULL)
	{
	gInstance = [[self alloc] init];
	}
return(gInstance);
}

+ (void)registerHelper:(CMercurialHelper *)inHelper
{
//NSLog(@"%@", inHelper);
gInstance = [inHelper retain];
}

- (BOOL)repositoryHasLocalChanges:(NSURL *)inURL
{
return(NO);
}

- (NSString *)repositoryBranch:(NSURL *)inURL
{
return(NULL);
}

@end
