//
//  CNodeProxy.m
//  Repoman
//
//  Created by Jonathan Wight on 12/24/09.
//  Copyright 2009 toxicsoftware.com. All rights reserved.
//

#import "CNodeProxy.h"

#import "CNode.h"
#import "CDirectory.h"
#import "CProjectWindowController.h"
#import "CKVOBlockNotificationCenter.h"
#include <objc/runtime.h>
#import "CRepository.h"

@interface CNodeProxy ()
@end

#pragma mark -

@implementation CNodeProxy

@synthesize node;
@synthesize controller;

+ (CNodeProxy *)nodeProxyWithController:(CProjectWindowController *)inController node:(CNode *)inNode
{
CNodeProxy *theProxy = objc_getAssociatedObject(inNode, @"nodeProxy");
if (theProxy == NULL)
	theProxy = [[[self alloc] initWithController:inController node:inNode] autorelease];
return(theProxy);
}

- (id)initWithController:(CProjectWindowController *)inController node:(CNode *)inNode
{
controller = inController;
node = [inNode retain];
objc_setAssociatedObject(inNode, @"nodeProxy", self, OBJC_ASSOCIATION_RETAIN);

if ([node isKindOfClass:[CDirectory class]])
	{
	//NSLog(@"OBSERVING");

	KVOBlock theBlock = ^(NSString *keyPath, id object, NSDictionary *change, id identifier) {
		NSPredicate *thePredicate = [change objectForKey:@"new"];
		//NSLog(@"%@ Predicate changed: %@", self.node.fileURL, thePredicate);

		[(id)self willChangeValueForKey:@"children"];
		[(id)self didChangeValueForKey:@"children"];
		};
	[controller addObserver:(id)self handler:theBlock forKeyPath:@"filterPredicate" options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionInitial identifier:NULL];
	}

return(self);
}

- (NSMethodSignature *)methodSignatureForSelector:(SEL)inSelector
{
NSMethodSignature *theMethodSignature = [self.node methodSignatureForSelector:inSelector];
return(theMethodSignature);
}

- (void)forwardInvocation:(NSInvocation *)ioInvocation
{
//if (ioInvocation.selector == @selector(children) || ioInvocation.selector == @selector(valueForKeyPath:))
//	[ioInvocation invokeWithTarget:self];
//else
	[ioInvocation invokeWithTarget:self.node];
}

- (id)valueForKeyPath:(NSString *)inKeyPath
{
if ([inKeyPath isEqualToString:@"children"])
	{
	return([self children]);
	}
else if ([inKeyPath isEqualToString:@"children"])
	{
	return([self children]);
	}
else if ([inKeyPath isEqualToString:@"isLeaf"])
	{
	return([NSNumber numberWithBool:[self isLeaf]]);
	}

return([self.node valueForKeyPath:inKeyPath]);
}

- (NSArray *)children
{
//NSLog(@"proxyChildren!");
NSArray *theChildren = [(CDirectory *)(self.node) children];
NSPredicate *thePredicate = self.controller.filterPredicate;
if (thePredicate)
	theChildren = [theChildren filteredArrayUsingPredicate:thePredicate];
//
NSMutableArray *theProxyChildren = [NSMutableArray array];
for (CNode *theChild in theChildren)
	{
	CNodeProxy *theProxy = [CNodeProxy nodeProxyWithController:self.controller node:theChild];
	[theProxyChildren addObject:theProxy];
	}

return(theProxyChildren);
}

- (NSUInteger)childCount
{
return([self children].count);
}

- (BOOL)isLeaf
{
// Repositories are only leaves when they have no subrepos. Otherwise pretty much everything is not a leaf.
if ([self.node isKindOfClass:[CRepository class]])
	{
	if (((CRepository *)self.node).subrepositories.count > 0)
		return(NO);
	else
		return(YES);
	}
else if ([self.node isKindOfClass:[CDirectory class]])
	return(NO);
return(YES);
}


@end
