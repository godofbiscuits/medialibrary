//
//  CMercurialHelper.h
//  Repoman
//
//  Created by Jonathan Wight on 12/26/09.
//  Copyright 2009 toxicsoftware.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CMercurialHelper : NSValueTransformer

+ (CMercurialHelper *)instance;

- (BOOL)repositoryHasLocalChanges:(NSURL *)inURL;
- (NSString *)repositoryBranch:(NSURL *)inURL;

@end
