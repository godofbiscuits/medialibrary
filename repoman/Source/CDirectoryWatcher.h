//
//  CDirectoryWatcher.h
//  Repoman
//
//  Created by Jonathan Wight on 01/04/10.
//  Copyright 2010 toxicsoftware.com. All rights reserved.
//

#import <Cocoa/Cocoa.h>

typedef void (^DirectoryWatcherBlock)(NSString *path, FSEventStreamEventFlags flag, FSEventStreamEventId eventId);

@interface CDirectoryWatcher : NSObject {
}

@property (readonly, assign) BOOL started;

+ (CDirectoryWatcher *)instance;

- (void)addBlock:(DirectoryWatcherBlock)inBlock forPath:(NSString *)inPath;

- (void)addBlock:(DirectoryWatcherBlock)inBlock paths:(NSArray *)inPaths identifier:(NSString *)inIdentifier;

- (void)start;
- (void)stop;

@end
