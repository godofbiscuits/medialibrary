//
//  CProjectWindowController.h
//  Repoman
//
//  Created by Jonathan Wight on 12/24/09.
//  Copyright 2009 toxicsoftware.com. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class CProjectsViewController;

@interface CProjectWindowController : NSWindowController {

}

@property (readwrite, retain) IBOutlet NSTreeController *treeController;

@property (readwrite, retain) NSURL *rootURL;
@property (readwrite, retain) id rootNodes;
@property (readwrite, retain) NSPredicate *filterPredicate;

@property (readwrite, assign) NSInteger filterSelection;
@property (readwrite, retain) IBOutlet CProjectsViewController *projectsViewController;

- (void)saveBookmarks;
- (void)loadBookmarks;

- (IBAction)addDirectory:(id)inSender;

- (IBAction)openInTerminal:(id)inSender;
- (IBAction)pull:(id)inSender;
- (IBAction)update:(id)inSender;
- (IBAction)addRemove:(id)inSender;
- (IBAction)commit:(id)inSender;
- (IBAction)push:(id)inSender;

@end
