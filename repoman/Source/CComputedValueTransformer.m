//
//  CComputedValueTransformer.m
//  Repoman
//
//  Created by Jonathan Wight on 12/24/09.
//  Copyright 2009 toxicsoftware.com. All rights reserved.
//

#import "CComputedValueTransformer.h"

#import "CComputedValue.h"

@implementation CComputedValueTransformer

+ (void)load
{
NSAutoreleasePool *thePool = [[NSAutoreleasePool alloc] init];
//
[self setValueTransformer:[[[self alloc] init] autorelease] forName:NSStringFromClass(self)];
//
[thePool release];
}

+ (Class)transformedValueClass
{
return([NSString class]);
}

+ (BOOL)allowsReverseTransformation
{
return(NO);
}

- (id)transformedValue:(id)value
{
CComputedValue *theValue = (CComputedValue *)value;
return(theValue.value);
//
//if (theValue.state == ComputedValueState_Computing || ComputedValueState_AssignedButComputing)
//	return(@"Updating");
//else if (theValue.state == ComputedValueState_Assigned)
//	return(theValue.value);
//else
//	return(NULL);
}

@end
