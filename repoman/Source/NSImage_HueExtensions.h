//
//  NSImage_HueExtensions.h
//  Repoman
//
//  Created by Jonathan Wight on 01/04/10.
//  Copyright 2010 toxicsoftware.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSImage (NSImage_HueExtensions)

- (NSImage *)imageWithHue:(CGFloat)inHue;

@end
