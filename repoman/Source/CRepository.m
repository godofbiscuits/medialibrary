//
//  CRepository.m
//  Repoman
//
//  Created by Jonathan Wight on 12/23/09.
//  Copyright 2009 toxicsoftware.com. All rights reserved.
//

#import "CRepository.h"

#import "CComputedValue.h"
#import "NSTask_MercurialExtensions.h"
#import "CSubrepository.h"
#import "CMercurialHelper.h"
#import "CDirectoryWatcher.h"
#import "NSImage_HueExtensions.h"

#import <QuartzCore/QuartzCore.h>

@interface CRepository ()
@end

@implementation CRepository

@synthesize branch;
@synthesize hasChanges;
@synthesize hasOutgoing;
@synthesize hasIncoming;
@synthesize subrepositories;

+ (NSString *)typeName
{
return(@"repository");
}

- (id)init
{
if ((self = [super init]) != NULL)
	{
	static NSOperationQueue *queue = NULL;
	if (queue == NULL)
		{
		queue = [[NSOperationQueue alloc] init];
		queue.maxConcurrentOperationCount = 20;
		}

	ComputedValueExecuteBlock theExecuteBlock = ^(dispatch_block_t inBlock){
		[queue addOperation:[NSBlockOperation blockOperationWithBlock:inBlock]];
		};
	ComputedValueExecuteBlock theHighPriorityExecuteBlock = ^(dispatch_block_t inBlock){
		NSOperation *theOperation = [NSBlockOperation blockOperationWithBlock:inBlock];
		theOperation.queuePriority = NSOperationQueuePriorityHigh;
		[queue addOperation:theOperation];
		};
	
	self.hasChanges = [[[CComputedValue alloc] init] autorelease];
	self.hasChanges.computeBlock = ^{
		[self watch];

		BOOL theOutput = [[CMercurialHelper instance] repositoryHasLocalChanges:self.fileURL];

//		NSTask *theTask = [NSTask mercurialTaskWithRepositoryURL:self.fileURL];
//		NSString *theOutput = [theTask executeWithArguments:[NSArray arrayWithObjects:@"status", NULL]];
		if (theOutput)
			{
			return([NSNumber numberWithBool:YES]);
			}
		else
			{
			return([NSNumber numberWithBool:NO]);
			}
		};
	self.hasChanges.executeBlock = theHighPriorityExecuteBlock;

	self.hasOutgoing = [[[CComputedValue alloc] init] autorelease];
	self.hasOutgoing.computeBlock = ^{
		[self watch];
		NSTask *theTask = [NSTask mercurialTaskWithRepositoryURL:self.fileURL];
		NSString *theOutput = [theTask executeWithArguments:[NSArray arrayWithObjects:@"outgoing", @"-q", NULL]];
		if (theOutput)
			{
			return([NSNumber numberWithBool:YES]);
			}
		else
			{
			return([NSNumber numberWithBool:NO]);
			}
		};
	self.hasOutgoing.executeBlock = theExecuteBlock;

	self.hasIncoming = [[[CComputedValue alloc] init] autorelease];
	self.hasIncoming.computeBlock = ^{
		[self watch];
		NSTask *theTask = [NSTask mercurialTaskWithRepositoryURL:self.fileURL];
		NSString *theOutput = [theTask executeWithArguments:[NSArray arrayWithObjects:@"incoming", @"-q", NULL]];
		if (theOutput)
			{
			return([NSNumber numberWithBool:YES]);
			}
		else
			{
			return([NSNumber numberWithBool:NO]);
			}
		};
	self.hasIncoming.executeBlock = theExecuteBlock;
	}
return(self);
}

- (NSArray *)subrepositories
{
if (subrepositories == NULL)
	{
	NSMutableArray *theSubrepositories = [NSMutableArray array];

	NSURL *theHGSubURL = [self.fileURL URLByAppendingPathComponent:@".hgsub"];

	if ([[NSFileManager defaultManager] fileExistsAtPath:[theHGSubURL path]] == YES)
		{
		NSError *theError = NULL;
		NSString *theHGSubFile = [NSString stringWithContentsOfURL:theHGSubURL encoding:NSUTF8StringEncoding error:&theError];
		
		NSArray *theLines = [theHGSubFile componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]];
		for (NSString *theLine in theLines)
			{
			NSScanner *theScanner = [NSScanner scannerWithString:theLine];
			
			NSString *thePath = NULL;
			if ([theScanner scanUpToString:@"=" intoString:&thePath] == NO)
				continue;
			
			thePath = [thePath stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];

			CSubrepository *theSubrepository = [[[CSubrepository alloc] init] autorelease];
			theSubrepository.parent = self;
			theSubrepository.pathComponent = thePath;

			[theSubrepositories addObject:theSubrepository];
			}
		
		subrepositories = [theSubrepositories copy];
		}
	}
	
return(subrepositories);
}

- (NSArray *)children
{
return(self.subrepositories);
}

- (NSString *)branch
{
if (branch == NULL)
	{
	branch = [[[CMercurialHelper instance] repositoryBranch:self.fileURL] retain];
	[self watch];
	}
return(branch);
}

- (NSImage *)image
{
static NSImage *sImage = NULL;
if (sImage == NULL)
	{
	NSImage *theImage = [super image];
	theImage = [theImage imageWithHue:1.0];
	sImage = [theImage retain];
	}
return(sImage);
}

- (void)watch
{
NSAssert(self.fileURL.path != NULL, @"No path to watch!");

NSLog(@"WATCHING: %@", self.fileURL);

DirectoryWatcherBlock theBlock = ^(NSString *path, FSEventStreamEventFlags flag, FSEventStreamEventId eventId) { NSLog(@"CHANGE: %@", path); };

[[CDirectoryWatcher instance] addBlock:theBlock forPath:self.fileURL.path];
}

@end