//
//  RepomanAppDelegate.h
//  Repoman
//
//  Created by Jonathan Wight on 12/23/09.
//  Copyright 2009 toxicsoftware.com. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class CProjectWindowController;

@interface RepomanAppDelegate : NSObject <NSApplicationDelegate> {
}

@property (readwrite, retain) CProjectWindowController *windowController;

@end
