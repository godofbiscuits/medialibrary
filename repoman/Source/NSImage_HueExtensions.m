//
//  NSImage_HueExtensions.m
//  Repoman
//
//  Created by Jonathan Wight on 01/04/10.
//  Copyright 2010 toxicsoftware.com. All rights reserved.
//

#import "NSImage_HueExtensions.h"

#import <QuartzCore/QuartzCore.h>

@implementation NSImage (NSImage_HueExtensions)

- (NSImage *)imageWithHue:(CGFloat)inHue
{
CGImageRef theCGImage = [self CGImageForProposedRect:NULL context:NULL hints:NULL];
CIImage *theCIImage = [[[CIImage alloc] initWithCGImage:theCGImage] autorelease];
CIFilter *theFilter = [CIFilter filterWithName:@"CIHueAdjust"];
[theFilter setDefaults];
[theFilter setValue:theCIImage forKey:@"inputImage"];
[theFilter setValue:[NSNumber numberWithFloat:inHue] forKey:@"inputAngle"];
theCIImage = [theFilter valueForKey:@"outputImage"];
NSCIImageRep *theImageRep = [NSCIImageRep imageRepWithCIImage:theCIImage];
NSImage *theImage = [[[NSImage alloc] init] autorelease];
[theImage addRepresentation:theImageRep];
return(theImage);
}

@end
