//
//  CSubrepository.m
//  Repoman
//
//  Created by Jonathan Wight on 12/23/09.
//  Copyright 2009 toxicsoftware.com. All rights reserved.
//

#import "CSubrepository.h"

#import "NSImage_HueExtensions.h"

@implementation CSubrepository

+ (NSString *)typeName
{
return(@"subrepo");
}

- (NSImage *)image
{
static NSImage *sImage = NULL;
if (sImage == NULL)
	{
	NSImage *theImage = [super image];
	theImage = [theImage imageWithHue:2.0];
	sImage = [theImage retain];
	}
return(sImage);
}


@end
