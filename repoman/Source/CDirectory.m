//
//  CDirectory.m
//  Repoman
//
//  Created by Jonathan Wight on 12/23/09.
//  Copyright 2009 toxicsoftware.com. All rights reserved.
//

#import "CDirectory.h"

#import "CNodeFactory.h"

@implementation CDirectory

@synthesize children;

+ (NSString *)typeName
{
return(@"directory");
}

- (NSArray *)children
{
if (children == NULL)
	{
	NSMutableArray *theChildren = [NSMutableArray array];
	
	NSError *theError = NULL;

	for (NSString *theName in [[NSFileManager defaultManager] contentsOfDirectoryAtPath:self.fileURL.standardizedURL.path error:&theError])
		{
		NSString *thePath = [self.fileURL.standardizedURL.path stringByAppendingPathComponent:theName];
		
		NSURL *theURL = [NSURL fileURLWithPath:thePath];
		
		CNode *theChild = [[CNodeFactory nodeFactory] nodeForURL:theURL];
		if (theChild)
			{
			theChild.parent = self;
			theChild.pathComponent = theName;

			[theChildren addObject:theChild];
			}
		}
		
	children = [theChildren copy];
	}
return(children);
}

- (NSImage *)image
{
return([NSImage imageNamed:NSImageNameFolder]);
}

@end
