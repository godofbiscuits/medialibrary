import Foundation

from mercurial import hg, ui, commands

CMercurialHelper = Foundation.NSClassFromString('CMercurialHelper')

class PythonMercurialHelper(CMercurialHelper):

	def repositoryHasLocalChanges_(self, inURL):
		try:
			thePath = inURL.absoluteURL().path()
			theUI = ui.ui()
			theRepo = hg.repository(theUI, thePath)
			return hasChanges(theRepo)
		except Exception, e:
			print 'Exception caught', e
			return None

	def repositoryBranch_(self, inURL):
		try:
			thePath = inURL.absoluteURL().path()
			theUI = ui.ui()
			theRepo = hg.repository(theUI, thePath)
			theBranchName = theRepo.dirstate.branch()
#			theBranchName = Foundation.NSString.stringWithString_(theBranchName)
			return theBranchName
		except Exception, e:
			print 'Exception caught', e
			return None

CMercurialHelper.registerHelper_(PythonMercurialHelper.alloc().init())


def hasChanges(repo):
	theStatus = repo.status(None, None, None, False, False, False)
	return True if len([True for x in theStatus if x]) else False
