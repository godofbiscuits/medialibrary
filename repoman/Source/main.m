//
//  main.m
//  Daily
//
//  Created by Jonathan Wight on 2/26/09.
//  Copyright toxicsoftware.com 2009. All rights reserved.
//

#import <Python/Python.h>
#import <Cocoa/Cocoa.h>

static int pymain(int argc, const char *argv[]);

int main(int argc, const char *argv[])
{
int theResult = 0;

NSLog(@"ENABLE_PYOBC: %d", ENABLE_PYOBC);
NSLog(@"OBJC_GC_REQUIRED: %d", OBJC_GC_REQUIRED);

#if ENABLE_PYOBC == 1
NSLog(@"python enabled");
theResult = pymain(argc, argv);
#else
NSLog(@"python disabled");
theResult = NSApplicationMain(argc, argv);
#endif

return(theResult);
}

static int pymain(int argc, const char *argv[])
{
NSAutoreleasePool *theAutoreleasePool = [[NSAutoreleasePool alloc] init];

NSBundle *theMainBundle = [NSBundle mainBundle];
NSArray *thePythonPathArray = [NSArray arrayWithObjects:
	[[theMainBundle sharedSupportPath] stringByAppendingPathComponent:@"PyObjC"],
	[theMainBundle sharedSupportPath],
	[[theMainBundle resourcePath] stringByAppendingPathComponent:@"PyObjC"],
	[theMainBundle resourcePath],
	NULL];

setenv("PYTHONPATH", [[thePythonPathArray componentsJoinedByString:@":"] UTF8String], 1);

NSArray *thePossiblePythonExtensions = [NSArray arrayWithObjects: @"py", @"pyc", @"pyo", NULL];
NSString *theMainPath = NULL;

for (NSString *thePath in thePythonPathArray)
	{
	for (NSString *theExtension in thePossiblePythonExtensions)
		{
		NSString *thePossiblePath = [thePath stringByAppendingPathComponent:[@"main" stringByAppendingPathExtension:theExtension]];
		if ([[NSFileManager defaultManager] fileExistsAtPath:thePossiblePath] == YES)
			{
			theMainPath = thePossiblePath;
			break;
			}
		}
	}

if (!theMainPath)
	{
	[NSException raise: NSInternalInconsistencyException format: @"%s:%d main() Failed to find the Main.{py,pyc,pyo} file in the application wrapper's Resources directory.", __FILE__, __LINE__];
	}

Py_SetProgramName("/usr/bin/python");
Py_Initialize();
PySys_SetArgv(argc, (char **)argv);

const char *theMainPathPtr = [theMainPath UTF8String];
FILE *theMainFile = fopen(theMainPathPtr, "r");
int theResult = PyRun_SimpleFile(theMainFile, (char *)[[theMainPath lastPathComponent] UTF8String]);
if (theResult != 0)
	{
	[NSException raise: NSInternalInconsistencyException format: @"%s:%d main() PyRun_SimpleFile failed with file '%@'.  See console for errors.", __FILE__, __LINE__, theMainPath];
	}

[theAutoreleasePool drain];

return(theResult);
}
