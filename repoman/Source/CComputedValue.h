//
//  CComputedValue.h
//  Repoman
//
//  Created by Jonathan Wight on 12/23/09.
//  Copyright 2009 toxicsoftware.com. All rights reserved.
//

#import <Cocoa/Cocoa.h>

typedef enum {
	ComputedValueState_Unknown,
	ComputedValueState_Computing,
	ComputedValueState_Assigned,	
	ComputedValueState_AssignedButComputing,	
} EComputedValueState;

typedef id (^ComputedValueComputeBlock)(void);
typedef void (^ComputedValueExecuteBlock)(dispatch_block_t inBlock);

@interface CComputedValue : NSObject {
}

@property (readwrite, assign) EComputedValueState state;
@property (readwrite, copy) id value;
@property (readwrite, copy) ComputedValueComputeBlock computeBlock;
@property (readwrite, copy) ComputedValueExecuteBlock executeBlock;

@end
