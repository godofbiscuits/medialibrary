//
//  CProjectWindowController.m
//  Repoman
//
//  Created by Jonathan Wight on 12/24/09.
//  Copyright 2009 toxicsoftware.com. All rights reserved.
//

#import "CProjectWindowController.h"

#import "CProjectsViewController.h"
#import "CKVOBlockNotificationCenter.h"
#import "CNodeProxy.h"
#import "Terminal.h"
#import "CRepository.h"
#import "CNodeFactory.h"

@implementation CProjectWindowController

@synthesize treeController;
@synthesize rootURL;
@synthesize rootNodes;
@synthesize filterPredicate;

@synthesize filterSelection;
@synthesize projectsViewController;

- (NSString *)windowNibName
{
return(NSStringFromClass([self class]));
}

- (id)init
{
if ((self = [super initWithWindow:NULL]) != NULL)
	{
	}
return(self);
}

- (void)windowDidLoad
{
[super windowDidLoad];
//
self.rootNodes = [NSArray array];
[self loadBookmarks];
//
KVOBlock theBlock = ^(NSString *keyPath, id object, NSDictionary *change, id identifier) {
	NSUInteger theTag = [[change objectForKey:@"new"] integerValue];
	if (theTag == 1)
		self.filterPredicate = [NSPredicate predicateWithFormat:@"isRepository == False OR hasChanges.value == 1"];
	else if (theTag == 2)
		self.filterPredicate = [NSPredicate predicateWithFormat:@"isRepository == False OR hasIncoming.value == 1"];
	else if (theTag == 3)
		self.filterPredicate = [NSPredicate predicateWithFormat:@"isRepository == False OR hasOutgoing.value == 1"];
	else
		self.filterPredicate = NULL;
	};
[self addObserver:self handler:theBlock forKeyPath:@"filterSelection" options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionInitial identifier:NULL];

self.projectsViewController.view.frame = ((NSView *)self.window.contentView).bounds;
[self.window.contentView addSubview:self.projectsViewController.view];

self.filterSelection = 0;
}

- (void)saveBookmarks
{
NSError *theError = NULL;

NSMutableArray *theBookmarks = [NSMutableArray array];
for (NSURL *theURL in [self.rootNodes valueForKey:@"fileURL"])
	{
	NSData *theBookmark = [theURL bookmarkDataWithOptions:NSURLBookmarkCreationSuitableForBookmarkFile includingResourceValuesForKeys:NULL relativeToURL:NULL error:&theError];
	[theBookmarks addObject:theBookmark];
	}

[[NSUserDefaults standardUserDefaults] setObject:theBookmarks forKey:@"bookmarks"];
}

- (void)loadBookmarks
{
NSError *theError = NULL;
NSArray *theBookmarks = [[NSUserDefaults standardUserDefaults] objectForKey:@"bookmarks"];
if (theBookmarks)
	{
	NSMutableArray *theRootNodes = [NSMutableArray array];
	for (NSData *theBookmark in theBookmarks)
		{
		BOOL theIsStaleFlag = NO;
		NSURL *theURL = [NSURL URLByResolvingBookmarkData:theBookmark options:0 relativeToURL:NULL bookmarkDataIsStale:&theIsStaleFlag error:&theError];
		CNode *theNode = [[CNodeFactory nodeFactory] nodeForURL:theURL];
		theNode.fileURL = theURL;

		CNodeProxy *theRootDirectoryProxy = [[[CNodeProxy alloc] initWithController:self node:theNode] autorelease];
		[theRootNodes addObject:theRootDirectoryProxy];
		}
	self.rootNodes = theRootNodes;
	}
}

- (IBAction)addDirectory:(id)inSender
{
NSOpenPanel *thePanel = [NSOpenPanel openPanel];
thePanel.canChooseDirectories = YES;
thePanel.canChooseFiles = NO;

id theHandler = ^(NSInteger result) {
	if (result == 1)
		{
		NSMutableArray *theRootNodes = [NSMutableArray arrayWithArray:self.rootNodes];
		for (NSURL *theURL in thePanel.URLs)
			{
			CNode *theNode = [[CNodeFactory nodeFactory] nodeForURL:theURL];
			theNode.fileURL = theURL;

			CNodeProxy *theRootDirectoryProxy = [[[CNodeProxy alloc] initWithController:self node:theNode] autorelease];
			[theRootNodes addObject:theRootDirectoryProxy];
			}
		self.rootNodes = theRootNodes;
		
		[self saveBookmarks];
		}
	};

[thePanel beginWithCompletionHandler:theHandler];
}

- (IBAction)openInTerminal:(id)inSender
{
TerminalApplication *theApplication = [SBApplication applicationWithBundleIdentifier:@"com.apple.terminal"];

for (CNode *theNode in self.treeController.selectedObjects)
	{
	NSString *theScript = [NSString stringWithFormat:@"cd '%@'", theNode.fileURL.absoluteURL.path];
	[theApplication doScript:theScript in:NULL];
	[theApplication activate];
	}
}

- (IBAction)pull:(id)inSender
{
TerminalApplication *theApplication = [SBApplication applicationWithBundleIdentifier:@"com.apple.terminal"];

for (CNode *theNode in self.treeController.selectedObjects)
	{
	if ([theNode isKindOfClass:[CRepository class]])
		{
		NSString *theScript = [NSString stringWithFormat:@"cd '%@' && hg pull", theNode.fileURL.absoluteURL.path];
		[theApplication doScript:theScript in:NULL];
		[theApplication activate];
		}
	}
}

- (IBAction)update:(id)inSender
{
TerminalApplication *theApplication = [SBApplication applicationWithBundleIdentifier:@"com.apple.terminal"];

for (CNode *theNode in self.treeController.selectedObjects)
	{
	if ([theNode isKindOfClass:[CRepository class]])
		{
		NSString *theScript = [NSString stringWithFormat:@"cd '%@' && hg update", theNode.fileURL.absoluteURL.path];
		[theApplication doScript:theScript in:NULL];
		[theApplication activate];
		}
	}
}

- (IBAction)addRemove:(id)inSender
{
TerminalApplication *theApplication = [SBApplication applicationWithBundleIdentifier:@"com.apple.terminal"];

for (CNode *theNode in self.treeController.selectedObjects)
	{
	if ([theNode isKindOfClass:[CRepository class]])
		{
		NSString *theScript = [NSString stringWithFormat:@"cd '%@' && hg addremove -s 80", theNode.fileURL.absoluteURL.path];
		[theApplication doScript:theScript in:NULL];
		[theApplication activate];
		}
	}
}

- (IBAction)commit:(id)inSender
{
TerminalApplication *theApplication = [SBApplication applicationWithBundleIdentifier:@"com.apple.terminal"];

for (CNode *theNode in self.treeController.selectedObjects)
	{
	if ([theNode isKindOfClass:[CRepository class]])
		{
		NSString *theScript = [NSString stringWithFormat:@"cd '%@' && hg commit", theNode.fileURL.absoluteURL.path];
		[theApplication doScript:theScript in:NULL];
		[theApplication activate];
		}
	}
}

- (IBAction)push:(id)inSender
{
TerminalApplication *theApplication = [SBApplication applicationWithBundleIdentifier:@"com.apple.terminal"];

for (CNode *theNode in self.treeController.selectedObjects)
	{
	if ([theNode isKindOfClass:[CRepository class]])
		{
		NSString *theScript = [NSString stringWithFormat:@"cd '%@' && hg push", theNode.fileURL.absoluteURL.path];
		[theApplication doScript:theScript in:NULL];
		[theApplication activate];
		}
	}
}

@end
