//
//  CNodeFactory.h
//  Repoman
//
//  Created by Jonathan Wight on 01/04/10.
//  Copyright 2010 toxicsoftware.com. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class CNode;

@interface CNodeFactory : NSObject {

}

+ (CNodeFactory *)nodeFactory;
- (CNode *)nodeForURL:(NSURL *)inURL;

@end
