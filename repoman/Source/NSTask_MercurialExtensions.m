//
//  NSTask_MercurialExtensions.m
//  Repoman
//
//  Created by Jonathan Wight on 12/24/09.
//  Copyright 2009 toxicsoftware.com. All rights reserved.
//

#import "NSTask_MercurialExtensions.h"


@implementation NSTask (NSTask_MercurialExtensions)

+ (NSTask *)mercurialTaskWithRepositoryURL:(NSURL *)inRepositoryURL
{
NSTask *theTask = [[[NSTask alloc] init] autorelease];
theTask.currentDirectoryPath = inRepositoryURL.path;
theTask.launchPath = @"/Users/schwa/Library/Python/bin/hg";
return(theTask);
}

- (NSString *)executeWithArguments:(NSArray *)inArguments
{
NSString *theShell = [[NSProcessInfo processInfo].environment objectForKey:@"SHELL"];
self.launchPath = theShell;

// TODO -- we need to escape inArguments here for sh

NSMutableArray *theArguments = [NSMutableArray arrayWithObjects:
	@"--login", @"-c", [NSString stringWithFormat:@"hg %@", [inArguments componentsJoinedByString:@" "]], NULL];

NSPipe *theStandardOutput = [NSPipe pipe];
self.arguments = theArguments;
self.standardOutput = theStandardOutput;
[self launch];
[self waitUntilExit];
NSData *theData = [[theStandardOutput fileHandleForReading] readDataToEndOfFile];
if (theData.length > 0)
	{
	NSString *theString = [[[NSString alloc] initWithData:theData encoding:NSUTF8StringEncoding] autorelease];
	return(theString);
	}
else
	{
	return(NULL);
	}
}

@end
