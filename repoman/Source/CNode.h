//
//  CNode.h
//  Repoman
//
//  Created by Jonathan Wight on 12/23/09.
//  Copyright 2009 toxicsoftware.com. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface CNode : NSObject {

}

@property (readwrite, assign) CNode *parent;
@property (readonly, assign) CNode *rootNode;

@property (readonly, copy) NSString *name;
@property (readwrite, copy) NSString *pathComponent;
@property (readwrite, copy) NSURL *fileURL;
@property (readonly, retain) NSImage *image;

+ (NSString *)typeName;

@end
