//
//  NSTask_MercurialExtensions.h
//  Repoman
//
//  Created by Jonathan Wight on 12/24/09.
//  Copyright 2009 toxicsoftware.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSTask (NSTask_MercurialExtensions)

+ (NSTask *)mercurialTaskWithRepositoryURL:(NSURL *)inRepositoryURL;
- (NSString *)executeWithArguments:(NSArray *)inArguments;

@end
