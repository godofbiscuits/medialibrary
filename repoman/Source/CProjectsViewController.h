//
//  CProjectsViewController.h
//  Repoman
//
//  Created by Jonathan Wight on 12/23/09.
//  Copyright 2009 toxicsoftware.com. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface CProjectsViewController : NSViewController {
}

@property (readwrite, retain) IBOutlet NSOutlineView *outlineView;
@property (readwrite, retain) IBOutlet NSTreeController *treeController;

@end
