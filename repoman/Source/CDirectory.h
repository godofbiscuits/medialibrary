//
//  CDirectory.h
//  Repoman
//
//  Created by Jonathan Wight on 12/23/09.
//  Copyright 2009 toxicsoftware.com. All rights reserved.
//

#import "CNode.h"

@interface CDirectory : CNode {
}

@property (readonly, copy) NSArray *children;

@end
