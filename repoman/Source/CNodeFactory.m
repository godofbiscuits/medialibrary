//
//  CNodeFactory.m
//  Repoman
//
//  Created by Jonathan Wight on 01/04/10.
//  Copyright 2010 toxicsoftware.com. All rights reserved.
//

#import "CNodeFactory.h"

#import "CNode.h"
#import "CDirectory.h"
#import "CRepository.h"

@implementation CNodeFactory

+ (CNodeFactory *)nodeFactory
{
return([[[self alloc] init] autorelease]);
}

- (CNode *)nodeForURL:(NSURL *)inURL
{
CNode *theNode = NULL;
NSString *thePath = inURL.path;
NSError *theError = NULL;
NSDictionary *theAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:thePath error:&theError];
if ([theAttributes fileType] == NSFileTypeDirectory)
	{
	BOOL theIsDirectoryFlag = NO;
	if ([[NSFileManager defaultManager] fileExistsAtPath:[thePath stringByAppendingPathComponent:@".hg"] isDirectory:&theIsDirectoryFlag] == YES)
		{
		theNode = [[[CRepository alloc] init] autorelease];
		}
	else
		{
		theNode = [[[CDirectory alloc] init] autorelease];
		}

	theNode.pathComponent = [thePath lastPathComponent];

	}
return(theNode);
}

@end
