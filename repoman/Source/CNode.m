//
//  CNode.m
//  Repoman
//
//  Created by Jonathan Wight on 12/23/09.
//  Copyright 2009 toxicsoftware.com. All rights reserved.
//

#import "CNode.h"

@implementation CNode

@synthesize parent;
@dynamic rootNode;
@dynamic name;
@synthesize pathComponent;
@synthesize fileURL;

+ (NSString *)typeName
{
return(@"node");
}

- (NSString *)description
{
return([NSString stringWithFormat:@"%@ (%@)", [super description], self.name]);
}

- (CNode *)rootNode
{
if (self.parent == NULL)
	return(self);
else
	return(self.parent.rootNode);
}

- (NSString *)name
{
return([pathComponent lastPathComponent]);
}

- (NSURL *)fileURL
{
if (parent == NULL && fileURL != NULL)
	return(fileURL);
else
	return([NSURL fileURLWithPath:[self.parent.fileURL.path stringByAppendingPathComponent:self.pathComponent]]);
//return([NSURL URLWithString:self.name relativeToURL:self.parent.fileURL.absoluteURL]);
}

- (NSImage *)image
{
return(NULL);
}

@end
