//
//  CNodeProxy.h
//  Repoman
//
//  Created by Jonathan Wight on 12/24/09.
//  Copyright 2009 toxicsoftware.com. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class CNode;
@class CProjectWindowController;

@interface CNodeProxy : NSProxy {
}

@property (readonly, assign) CNode *node;
@property (readonly, assign) CProjectWindowController *controller;

- (id)initWithController:(CProjectWindowController *)inController node:(CNode *)inNode;

- (NSArray *)children;
- (NSUInteger)childCount;
- (BOOL)isLeaf;

@end
