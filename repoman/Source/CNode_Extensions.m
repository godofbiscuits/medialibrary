//
//  CNode_Extensions.m
//  Repoman
//
//  Created by Jonathan Wight on 12/23/09.
//  Copyright 2009 toxicsoftware.com. All rights reserved.
//

#import "CNode_Extensions.h"

@implementation CNode (CNode_Extensions)

- (BOOL)isRepository
{
return(NO);
}

- (id)branch
{
return(NULL);
}

- (id)hasChanges
{
return(NULL);
}

- (id)hasOutgoing
{
return(NULL);
}

- (id)hasIncoming
{
return(NULL);
}

@end
