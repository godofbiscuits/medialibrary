//
//  CRepository.h
//  Repoman
//
//  Created by Jonathan Wight on 12/23/09.
//  Copyright 2009 toxicsoftware.com. All rights reserved.
//

#import "CDirectory.h"

@class CComputedValue;

@interface CRepository : CDirectory {
}

@property (readwrite, retain) NSString *branch;
@property (readwrite, retain) CComputedValue *hasChanges;
@property (readwrite, retain) CComputedValue *hasOutgoing;
@property (readwrite, retain) CComputedValue *hasIncoming;
@property (readonly, retain) NSArray *subrepositories;

- (void)watch;

@end
