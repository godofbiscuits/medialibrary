PATH=$PATH:/usr/local/bin
export PATH

pman()
{
    man -t "${1}" | open -f -a /Applications/Preview.app/
}