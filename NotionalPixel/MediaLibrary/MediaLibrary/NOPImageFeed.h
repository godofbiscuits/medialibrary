//
//  NOPImageFeed.h
//  MediaLibrary
//
//  Created by Jeff Barbose on 6/22/13.
//  Copyright (c) 2013 Jeff Barbose. All rights reserved.
//

@import MediaLibrary;

#import <Foundation/Foundation.h>

@interface NOPImageFeed : NSObject


-(instancetype) initWithLibrary:(MLMediaLibrary*)library;

@end
