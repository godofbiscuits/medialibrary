//
//  NOPImageFeed.m
//  MediaLibrary
//
//  Created by Jeff Barbose on 6/22/13.
//  Copyright (c) 2013 Jeff Barbose. All rights reserved.
//

#import "NOPImageFeed.h"


@interface NOPImageFeed ()

@property (strong, nonatomic) MLMediaLibrary* mediaLibrary;
@property (strong, nonatomic) MLMediaSource* iPhotoSource;
@property (strong, nonatomic) MLMediaGroup* iPhotoRootGroup;
@property (strong, nonatomic) MLMediaGroup* iPhotoAllPhotosGroup;
@property (strong, nonatomic) NSArray* allPhotoObjects;
-(void) performImageCollection;

@end


@implementation NOPImageFeed

-(instancetype) initWithLibrary:(MLMediaLibrary*)library;
{
    if ( ( self = [super init] ) )
    {
        _mediaLibrary = library;
        _iPhotoSource = nil;
        
        [self.mediaLibrary addObserver:self
                       forKeyPath:@"mediaSources"
                          options:( NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew )
                          context:nil];
        
    }
    
    return self;
}



-(void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ( object == self.mediaLibrary )
    {
        if ( [keyPath isEqualToString:@"mediaSources"] )
        {
            NSLog( @"mediaSources: %@;  changeKind:%@ / new:%@", self.mediaLibrary.mediaSources, change[@"kind"], change[@"new"] );
           
            if ( self.iPhotoSource == nil )
            {
                self.iPhotoSource = self.mediaLibrary.mediaSources[MLMediaSourceiPhotoIdentifier];
                
                MLMediaGroup* group = [self.iPhotoSource mediaGroupForIdentifier:@"Images"];
                NSDictionary* attribs = self.iPhotoSource.attributes;
                MLMediaGroup* rootGroup = self.iPhotoSource.rootMediaGroup;
                
                [self.iPhotoSource addObserver:self
                                    forKeyPath:@"rootMediaGroup"
                                       options:( NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew )
                                       context:nil];
            }
        }
    }
    else if ( object == self.iPhotoSource )
    {
        if ( [keyPath isEqualToString:@"rootMediaGroup"] )
        {
            if ( self.iPhotoSource.rootMediaGroup != nil )
            {
                self.iPhotoRootGroup = self.iPhotoSource.rootMediaGroup;
                
                self.iPhotoAllPhotosGroup = [self.iPhotoSource mediaGroupForIdentifier:@"allPhotosAlbum"];
                
                NSAssert( self.iPhotoAllPhotosGroup != nil, @"the all-photos group must always exist!" );
                
                NSArray* photos = self.iPhotoAllPhotosGroup.mediaObjects;  // trigger fetch for these.  yikes.
                
                if ( photos == nil )
                {
                    [self.iPhotoAllPhotosGroup addObserver:self
                                                forKeyPath:@"mediaObjects"
                                                   options:( NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew )
                                                   context:nil];
                }
                else
                {
                    self.allPhotoObjects = photos;
                }
                

            }
            
            
            [self performImageCollection];
        }
    }
    else if ( object == self.iPhotoAllPhotosGroup )
    {
        if ( [keyPath isEqualToString:@"mediaObjects"] )
        {
            NSLog( @"iPhotoAllPhotosgroup.mediaObjects = %@", self.iPhotoAllPhotosGroup.mediaObjects );
            
            if ( self.iPhotoAllPhotosGroup.mediaObjects != nil )
            {
                self.allPhotoObjects = self.iPhotoAllPhotosGroup.mediaObjects;
                
                [self performImageCollection];
            }
        }
    }
}



-(void) performImageCollection
{
    NSLog( @"iPhoto root media group = %@", self.iPhotoSource.rootMediaGroup );
    
    
    NSLog( @"mediaSources.allKeys = %@", self.mediaLibrary.mediaSources.allKeys );
    
    if ( self.allPhotoObjects != nil )
    {
        [self.allPhotoObjects enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            
            MLMediaObject* photo = (MLMediaObject*)obj;
            
            NSURL* thumbnailURL = photo.thumbnailURL;
            
            NSLog( @"thumbnailURL = %@", thumbnailURL.absoluteString );
            
        }];
    }
}













-(void) dealloc
{
        // don't call super because we're using ARC
        // only here to get unobserve
    
    [self.mediaLibrary removeObserver:self forKeyPath:@"mediaSources"];
    [self.iPhotoSource removeObserver:self forKeyPath:@"rootMediaGroup"];
    [self.iPhotoAllPhotosGroup removeObserver:self forKeyPath:@"mediaObjects"];
}
@end
