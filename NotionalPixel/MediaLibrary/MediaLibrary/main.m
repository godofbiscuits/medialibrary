//
//  main.m
//  MediaLibrary
//
//  Created by Jeff Barbose on 6/16/13.
//  Copyright (c) 2013 Jeff Barbose. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[])
{
    return NSApplicationMain(argc, argv);
}
