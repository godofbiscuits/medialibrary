//
//  NOPAppDelegate.h
//  MediaLibrary
//
//  Created by Jeff Barbose on 6/16/13.
//  Copyright (c) 2013 Jeff Barbose. All rights reserved.
//

@import Cocoa;


@interface NOPAppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;

@end
