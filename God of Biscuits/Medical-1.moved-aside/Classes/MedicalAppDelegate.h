//
//  MedicalAppDelegate.h
//  Medical
//
//  Created by Jeff Barbose on 9/22/10.
//  Copyright HowLand Software 2010. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MedicalViewController;

@interface MedicalAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    MedicalViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet MedicalViewController *viewController;

@end

