
var IsiPhone = navigator.userAgent.indexOf("iPhone") != -1 ;
var IsiPod = navigator.userAgent.indexOf("iPod") != -1 ;
var IsiPad = navigator.userAgent.indexOf("iPad") != -1 ;

var IsiPhoneOS = IsiPhone || IsiPad || IsiPod ;

// Input options:
var AmountOptions = null ; 
var EventOptions = null ;

// UI elements:
var SelectedRow = null ;
var Log = document.getElementById("log") ;


/**
 Adds recently used values to the input options caches.
 */
function AnalyzeRecents (entries) {
 AmountOptions = ["..."] ;
 EventOptions = ["..."] ;
 var amountsCache = [] ;
 var eventsCache = {} ;
 for (var i = 0; i < entries.length; ++i) {
  var e = entries[i] ;
  if (!eventsCache[e.event]) {
   EventOptions.push(e.event) ;
   eventsCache[e.event] = true ;
  }
  if (!amountsCache[e.amount]) {
   AmountOptions.push(e.amount) ;
   amountsCache[e.amount] = true ;
  }
 }
}

/**
 Generates a handler to delete a row.
 */
function GenerateDeleteRow(id) {
 return function (event) {
  $.post('./delete-log-entry.php',{id: id}, function (receipt) {
   console.log("deleted: " + receipt) ;
   $("#row-" + id).hide() ;
   $("#options-row-" + id).hide() ;
  }) ;
 }
} 


/**
 Inserts a new log entry.
 */
function InsertLogEntry (event,amount,notes) {
  $.post('./insert-into-log.php',{event: event, amount: amount, notes: notes}, function (receipt) {
   InitializeTable() ;
  }) ; 
}


/**
 Generates a handler to delete a row.
 */
function GenerateDuplicateRow(row) {
 return function (event) {
  InsertLogEntry(row.data.event, row.data.amount, "");
 }
} 



/**
 Handler to select a row for editing.
 */
function SelectRow(event) {
 var t = event.target ;
 while (t.tagName != "TR")
  t = t.parentNode ;

 if (SelectedRow == t) return ;

 if (SelectedRow != null) {
  SelectedRow.style.backgroundColor = "white" ;
  SelectedRow.hideOptions() ;
 }
 SelectedRow = t ;
 
 SelectedRow.style.backgroundColor = "#9999ff" ;
 SelectedRow.showOptions() ;

 var cell = document.getElementById("field-" + t.dbID + "-time") ;
 cell.innerHTML = "" ;
 cell.appendChild(CreateDate(cell.id,cell.time)) ;
}



// Install a handler on the refresh button:
$("#refresh-button").click(function () { location.reload() ; }) ;


/**
 Adds a new event to the log.
 */
function AddEvent() {
 InsertLogEntry("NEW",0,"") ;
}

// Add's a handler to the "Add New Event" button:
$("#add-event-button").each(function (i,el) {
 function down() {
  el.style.backgroundColor = "8137ca" ;
 }

 function up() {
  el.style.backgroundColor = "white" ;
  AddEvent() ;
 }

 if (IsiPhoneOS) {
  el.ontouchstart = down ;
  el.ontouchend = up ;
 } else { 
  el.onmousedown = down ;
  el.onmouseup = up ;
 }
 
}) ;


/**
 Converts a field id into its constituents: database id and field name.
 */
function ParseFieldId (fieldId) {
 var parts = fieldId.split("-") ;
 return {
  id : parts[1] ,
  name : parts[2] 
 } ;
}


/**
 Contacts the server to update the value of a field.
 */
function UpdateFieldValue(id, field, value) {
 var cell = document.getElementById("field-" + id + "-" + field) ;
 if (field == "time") field = "moment" ;
 cell.style.backgroundColor = "#ff9999" ;
 $.post('./update-log-entry.php', {id: id, field: field, value: value}, function (response) {
  cell.style.backgroundColor = "transparent" ;
 }) ;
}


/**
 Handler to sync a changed field with the server.
 */
function SyncInput(event) {
 if (event.target.value == "...") {
  var customValue = prompt("Enter a custom value") ;
  if (!customValue)
   return ;
  $(event.target).append('<option value="'+customValue+'" selected="true">'+customValue+'</option>') ;
 }
 var v = event.target.value ;
 var fid = ParseFieldId(event.target.id) ; 
 UpdateFieldValue(fid.id, fid.name, v) ;
}




/**
 Creates a select input with the given options.
 */
function CreateSelect(id,options,def) {
 var el = document.createElement("select") ;
 el.id = "input" + id ;
 el.onchange = SyncInput ;
 var opt = document.createElement("option") ;
 opt.value = def ;
 opt.innerHTML = def ;
 el.appendChild(opt) ;
 for (var i = 0; i < options.length; ++i) {
  var opt = document.createElement("option") ;
  opt.value = options[i] ;
  opt.innerHTML = options[i] ;
  el.appendChild(opt) ;
 }
 return el ;
}

/**
 Creates a time input around the given time.
 */
function CreateDate(id,selectedTime) {
 var el = document.createElement("select") ;
 el.id = id ;
 el.onchange = SyncInput ;
 var t = selectedTime - (60*60*1000*1) ;
 var sts = (new Date(selectedTime)).toTimeString() ;
 for (var i = 0; i < 120; ++i) {
  var n = new Date(t) ;
  var ts = n.toTimeString() ;
  var selected = ts == sts ? "selected" : "" ;
  var opt = document.createElement("option") ;
  opt.value = t/1000 ;
  opt.selected = selected ;
  opt.innerHTML = ts ;
  el.appendChild(opt) ;
  t += 60*1000 ;
 }
 return el ;
}

/**
 Returns a HH:MM(am|pm) time string.
 */
Date.prototype.toTimeString = function () {
 var hours = this.getHours() % 12 ;
 var ampm = this.getHours() >= 12 ? "pm" : "am" ;
 if (hours == 0) hours = 12 ;
 var mins = this.getMinutes() ;
 if (mins < 10) mins = "0" + mins ;
 return hours + ":" + mins  + ampm ; 
} ;




// Fields displayed:
var fields = ["time","event","amount"] ;


/**
 Pulls the latest database values and updates the table.
 */
function InitializeTable () {
 $.getJSON('./show-log.php', function (entries) {
  var RequestFinish = (new Date()).getTime() ;

  Log.innerHTML = "" ;

  // Analyze entries:
  AnalyzeRecents(entries) ;

  // Iterate over all entries:
  var lastDate = null ;
  for (var i = 0; i < entries.length; ++i) {
   var rowId = 'row-'+entries[i].id ;

   // Check if a new date row should be added:
   var thisDate = new Date(entries[i].time * 1000) ;
   if ((lastDate == null) || (thisDate.getDay() != lastDate.getDay())) {
    var timeRow = document.createElement("tr") ;
    timeRow.className = "date-row" ;
    var cell = document.createElement("td") ;
    cell.setAttribute ("colspan", "3") ;
    cell.innerHTML = '<b>' + thisDate.toLocaleDateString() + '</b>' ;
    timeRow.appendChild(cell) ;
    Log.appendChild(timeRow) ;
   } 
   lastDate = thisDate ;  

   // Tag the last row with the class last-child:
   var lastClass = "" ;
   if (i == entries.length-1) 
    lastClass = " last-child"  ;

   // Create a new row for the log:
   var logRow = document.createElement("tr") ;
   logRow.className = "log-row"+lastClass ;
   logRow.id = rowId ;
   logRow.dbID = entries[i].id ;
   logRow.data = entries[i] ;

   // Iterate over fields:
   for (var j = 0; j < fields.length; ++j) {
    var f = fields[j] ;
    var fieldId = 'field-'+entries[i].id+'-'+f;
    var value = entries[i][f] ;
    var cell = document.createElement("td") ;

    cell.id = fieldId ;
    cell.className = "log-field" ;
    switch (f) {
     case "time":
      var t = value * 1000 ;
      cell.time = t ;
      cell.innerHTML = new Date(t).toTimeString() ;
      cell.onclick = SelectRow ;
      break ;
  
     case "event":
      var options = EventOptions;
      var el = CreateSelect(fieldId,options,entries[i][f]) ;
      if (IsiPhone || IsiPod) {
       el.style.width = "95px" ;
      }
      cell.appendChild(el) ;
      break;

     case "amount":
      var options = AmountOptions ;
      var el = CreateSelect(fieldId,options,entries[i][f]) ;
      cell.appendChild(el) ; 
      break;
 
     default:
      cell.innerHTML = (entries[i][fields[j]]) ;
    }
    logRow.appendChild(cell) ;
   }
   Log.appendChild(logRow) ;

   // Create a (default) hidden row with options:
   var optRow = document.createElement("tr") ;   
   optRow.className = "options-row" ;
   optRow.id = "options-row-" + entries[i].id ;

   var cell = document.createElement("td") ;
   var cell2 = document.createElement("td") ;
   var cell3 = document.createElement("td") ;

   var deleteButton = document.createElement("input") ;
   deleteButton.setAttribute("type","button") ;
   deleteButton.onclick = GenerateDeleteRow(entries[i].id) ; 
   deleteButton.value = "Delete" ;

   var notesField = document.createElement("input") ;
   cell2.id = "field-" + entries[i].id + "-notes" ;
   notesField.id = "input" + cell2.id ;
   notesField.setAttribute("type","text") ;
   notesField.onchange = SyncInput ;
   notesField.value = entries[i].notes ;  
   notesField.style.width = "9.5em"; 

   var duplicateButton = document.createElement("input") ;
   duplicateButton.setAttribute("type","button") ;
   duplicateButton.onclick = GenerateDuplicateRow(logRow) ; 
   duplicateButton.value = "Duplicate" ;

   cell.appendChild(duplicateButton) ; 
   cell2.appendChild(notesField) ; 
   cell3.appendChild(deleteButton) ; 

   optRow.appendChild(cell) ;
   optRow.appendChild(cell2) ;
   optRow.appendChild(cell3) ;
   Log.appendChild(optRow) ;

   with ({optRow: optRow}) {
    logRow.hideOptions = function () {
     optRow.style.backgroundColor = "white" ;
     optRow.style.display = "none" ;
    };
    logRow.showOptions = function () {
     optRow.style.display = "table-row" ; 
     optRow.style.backgroundColor = "#9999ff" ;
    };
   }
  }

  
 }) ;
}

InitializeTable() ;
