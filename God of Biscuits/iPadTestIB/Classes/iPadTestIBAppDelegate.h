//
//  iPadTestIBAppDelegate.h
//  iPadTestIB
//
//  Created by Jeff Barbose on 9/15/10.
//  Copyright HowLand Software 2010. All rights reserved.
//

#import <UIKit/UIKit.h>

@class iPadTestIBViewController;

@interface iPadTestIBAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    iPadTestIBViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet iPadTestIBViewController *viewController;

@end

