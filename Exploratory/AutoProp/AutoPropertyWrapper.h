//
//  AutoPropertyWrapper.h
//  AutoProp
//
//  Created by Jeff Barbose on 2/18/11.
//  Copyright 2011 HowLand Software. All rights reserved.
//

#include <Cocoa/Cocoa.h>

class AutoPropertyWrapper 
{
    NSObject* propObj;
    
public:
    AutoPropertyWrapper( NSObject* anObj, bool shouldRetain = false );
    ~AutoPropertyWrapper();
};