//
//  Foo.mm
//  AutoProp
//
//  Created by Jeff Barbose on 2/18/11.
//  Copyright 2011 HowLand Software. All rights reserved.
//

#import "Foo.h"
#import <Cocoa/Cocoa.h>
#import "AutoPropertyWrapper.h"



@implementation Bar

@synthesize str;

@end




@implementation Foo

- (id)init
{
    self = [super init];
	
	Bar* outerBar = nil;
	
    if (self)
	{
		Bar* bar = [[Bar alloc] init];
		{
			AutoPropertyWrapper wrapper = AutoPropertyWrapper( (NSObject*)bar );
			bar.str = @"blah";
			outerBar = bar;
			
		}

		outerBar = bar;
	
	}
	
	NSLog( @"outerBar = %@", outerBar );
    
    return self;
}

- (void)dealloc
{
    [super dealloc];
}

@end
