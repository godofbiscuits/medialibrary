//
//  Foo.h
//  AutoProp
//
//  Created by Jeff Barbose on 2/18/11.
//  Copyright 2011 HowLand Software. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface Foo : NSObject 
{
    
}

@end


@interface Bar : NSObject
{
	NSString* str;
}

@property (retain) NSString* str;

@end
