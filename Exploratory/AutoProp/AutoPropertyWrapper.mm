//
//  AutoPropertyWrapper.mm
//  AutoProp
//
//  Created by Jeff Barbose on 2/18/11.
//  Copyright 2011 HowLand Software. All rights reserved.
//

#include "AutoPropertyWrapper.h"


AutoPropertyWrapper::AutoPropertyWrapper( NSObject* anObj, bool shouldRetain )
{
    this->propObj = ( shouldRetain ? [anObj retain] : anObj );
}



AutoPropertyWrapper::~AutoPropertyWrapper()
{
    [this->propObj release];
    this->propObj = nil;
}
