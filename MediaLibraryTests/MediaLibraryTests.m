//
//  MediaLibraryTests.m
//  MediaLibraryTests
//
//  Created by Jeff Barbose on 6/16/13.
//  Copyright (c) 2013 Jeff Barbose. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface MediaLibraryTests : XCTestCase

@end

@implementation MediaLibraryTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testExample
{
    XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
}

@end
