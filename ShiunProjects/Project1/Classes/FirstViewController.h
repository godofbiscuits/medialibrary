//
//  FirstViewController.h
//  Project1
//
//  Created by Jeff Barbose on 2/3/10.
//  Copyright HowLand Software 2010. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface FirstViewController : UIViewController
{
    UILabel* helloLabel;
    NSNotificationCenter* nc;
}

@property (nonatomic, retain) IBOutlet UILabel* helloLabel;


// IBAction

- (IBAction) restoreDefaultLabelText:(id)sender;

@end
