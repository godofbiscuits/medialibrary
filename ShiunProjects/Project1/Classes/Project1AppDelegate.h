//
//  Project1AppDelegate.h
//  Project1
//
//  Created by Jeff Barbose on 2/3/10.
//  Copyright HowLand Software 2010. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Project1AppDelegate : NSObject <UIApplicationDelegate, UITabBarControllerDelegate> {
    UIWindow *window;
    UITabBarController *tabBarController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet UITabBarController *tabBarController;

@end
