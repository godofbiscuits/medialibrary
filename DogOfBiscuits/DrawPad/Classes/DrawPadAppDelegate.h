//
//  DrawPadAppDelegate.h
//  DrawPad
//
//  Created by Jeff Barbose on 11/4/10.
//  Copyright 2010 HowLand Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DrawPadViewController;

@interface DrawPadAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    DrawPadViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet DrawPadViewController *viewController;

@end

