//
//  DrawPadViewController.h
//  DrawPad
//
//  Created by Jeff Barbose on 11/4/10.
//  Copyright 2010 HowLand Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DrawableView;


@interface DrawPadViewController : UIViewController 
{
    DrawableView* drawableView;
    UITextField* editableTextField;
}

@property (retain) IBOutlet DrawableView* drawableView;
@property (retain) IBOutlet UITextField* editableTextField;


@end

