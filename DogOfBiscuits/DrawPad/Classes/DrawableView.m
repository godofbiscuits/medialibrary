//
//  DrawView.m
//  DrawPad
//
//  Created by Jeff Barbose on 11/4/10.
//  Copyright 2010 HowLand Software. All rights reserved.
//

#import "DrawableView.h"


@implementation DrawableView


- (id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code.
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code.
}
*/

- (void)dealloc {
    [super dealloc];
}


@end
