//
//  PDFTestViewController.h
//  PDFTest
//
//  Created by Jeff Barbose on 11/7/10.
//  Copyright 2010 HowLand Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDFTestViewController : UIViewController <UITextFieldDelegate, UIWebViewDelegate>
{
    UITextField *locationField;
    UIWebView *webView;
}

@property (retain) IBOutlet UITextField *locationField;
@property (retain) IBOutlet UIWebView *webView;

@end

