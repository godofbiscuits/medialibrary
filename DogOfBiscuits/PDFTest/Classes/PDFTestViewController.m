//
//  PDFTestViewController.m
//  PDFTest
//
//  Created by Jeff Barbose on 11/7/10.
//  Copyright 2010 HowLand Software. All rights reserved.
//

#import "PDFTestViewController.h"

@implementation PDFTestViewController


@synthesize locationField, webView;


/*
// The designated initializer. Override to perform setup that is required before the view is loaded.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad 
{
    [super viewDidLoad];
    
    
}


// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return YES;
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}


#pragma mark -
#pragma mark === UIWebViewDelegate ===
#pragma mark 




- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSString *webTitle = [self.webView stringByEvaluatingJavaScriptFromString:@"document.title"];
    NSString *DOM = [self.webView stringByEvaluatingJavaScriptFromString:@"document"];
    
    NSLog( @"Is DOM present?  Javascript thinks title = '%@'", webTitle );
    
}




#pragma mark -
#pragma mark === UITextFieldDelegate ===
#pragma mark 



- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSURL* url = [NSURL URLWithString:self.locationField.text];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];

    [self.webView loadRequest:request];
    
    return YES;     // make the keyboard go away
}






@end
