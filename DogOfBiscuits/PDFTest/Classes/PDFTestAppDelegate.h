//
//  PDFTestAppDelegate.h
//  PDFTest
//
//  Created by Jeff Barbose on 11/7/10.
//  Copyright 2010 HowLand Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PDFTestViewController;

@interface PDFTestAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    PDFTestViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet PDFTestViewController *viewController;

@end

