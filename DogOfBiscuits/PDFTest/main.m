//
//  main.m
//  PDFTest
//
//  Created by Jeff Barbose on 11/7/10.
//  Copyright 2010 HowLand Software. All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[]) {
    
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
    int retVal = UIApplicationMain(argc, argv, nil, nil);
    [pool release];
    return retVal;
}
