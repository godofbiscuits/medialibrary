//
//  main.m
//  ColdCall
//
//  Created by Jeffrey Barbose on 28/06/2011.
//  Copyright 2011 Razorfish, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[])
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    int retVal = UIApplicationMain(argc, argv, nil, nil);
    [pool release];
    return retVal;
}
