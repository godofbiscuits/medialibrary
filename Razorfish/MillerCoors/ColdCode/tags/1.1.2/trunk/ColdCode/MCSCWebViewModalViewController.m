//
//  MCSCWebViewModalViewController.m
//  ColdCode
//
//  Created by Jeffrey Barbose on 02-07-11.
//  Copyright 2011 Razorfish, LLC. All rights reserved.
//

#import "MCSCWebViewModalViewController.h"


@implementation MCSCWebViewModalViewController
@synthesize webView;
@synthesize filePath;
@synthesize webDelegate;

- (id) initWithFilePath:(NSString*) aFilePath delegate:(id<UIWebViewDelegate>)aWebDelegate
{
        //if ( ( self = [self initWithNibName:[NSStringFromClass([self class]) bundle:[NSBundle mainBundle]]] ) )
    if ( ( self = [super initWithNibName:nil /*@"MCSCWebViewModalViewController"*/ bundle:nil] ) )
    {
        webDelegate = aWebDelegate;
        filePath = aFilePath;
        
        return self;
    }
    else
    {
        return nil;
    }
}




- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [webView release];
    [filePath release];
    
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.webView.delegate = self.webDelegate;

    if ( self.filePath != nil )
    {
        NSError* err = nil;
        
        NSString* htmlString = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:[self.filePath stringByDeletingPathExtension] ofType:@"html"] encoding:NSUTF8StringEncoding error:&err];
        
        if ( err == nil )
        {
            [self.webView loadHTMLString:htmlString baseURL:nil];
        }
    }
}

- (void)viewDidUnload
{
    [self setWebView:nil];

    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


- (void) setFilePath:(NSString *)newFilePath
{
    if ( ! [self.filePath isEqualToString:newFilePath] )
    {
        if ( newFilePath == nil )
            newFilePath = @"empty.html";
        
        NSString* hold = filePath;
        filePath = [newFilePath retain];
        [hold release];
        
        NSError* err = nil;
        
        NSString* htmlString = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:[self.filePath stringByDeletingPathExtension] ofType:@"html"] encoding:NSUTF8StringEncoding error:&err];
        
        if ( err == nil )
        {
            [self.webView loadHTMLString:htmlString baseURL:nil];
        }
    }
}



- (IBAction)dismissModal:(id)sender
{
    [self dismissModalViewControllerAnimated:YES];
}



@end
