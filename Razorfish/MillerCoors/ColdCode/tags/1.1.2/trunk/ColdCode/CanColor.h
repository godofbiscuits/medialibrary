//
//  CanColor.h
//  coors_test
//
//  Created by Patrick D Miller on 7/16/11.
//  Copyright 2011 Deeplocal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <opencv2/imgproc/imgproc_c.h>
#import <opencv2/objdetect/objdetect.hpp>
@class DLCVHSBColorSpec;
@class DLCVRawAnalysis;

@interface CanColor : NSObject {
    NSString* type;
    BOOL showDebug;
    float r,g,b,a;
    float h,s,v;
    float herror, serror, verror;
    float rmin, gmin, bmin, amin;
    float rmax, gmax, bmax, amax;
    NSMutableArray* rects;
    DLCVHSBColorSpec* color;
    
    
}

@property float r,g,b,a;
@property float h,s,v;
@property (nonatomic, retain) NSMutableArray* rects;
@property (nonatomic, retain) NSString* type;
@property (nonatomic, retain) DLCVHSBColorSpec* color;
@property BOOL showDebug;
-(id) initWithColor:(float)inr
                  g:(float)ing
                  b:(float)inb
                  a:(float)ina;

-(void) initColorRange:(float)inrmin
                  ming:(float)ingmin
                  minb:(float)inbmin
                  maxr:(float)inrmax
                  maxg:(float)ingmax
                  maxb:(float)inbmax;

-(CvScalar) to_cvscalar;

-(float) match:(IplImage*)inImg;
-(void) addRect:(float)x1
             y1:(float)y1
             x2:(float)x2
             y2:(float)y2;
-(int ) findConsecutive:(IplImage*)inImg
                 pixels:(int) pixels;
-(int ) findConsecutive3:(IplImage*)inImg
                 pixels:(int) pixels;

-(void) log;
@end
