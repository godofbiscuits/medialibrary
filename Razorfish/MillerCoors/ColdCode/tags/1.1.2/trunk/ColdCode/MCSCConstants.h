//
//  MCSCConstants.h
//  ColdCode
//
//  Created by Jeffrey Barbose on 20/07/2011.
//  Copyright 2011 Razorfish, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>


extern NSString* MCSCCoorsContainerBottleOrCanKey;
extern NSString* MCSCCoorsContainerTemperatureKey;
extern NSString* MCSCCoorsContainerLockedImageKey;
extern NSString* MCSCCoorsContainerTimeStampLockKey;
extern NSString* MCSCCoorsContainerChillLocationKey;

extern NSString* MCSCChillCommenceDateKey;
extern NSString* MCSCCoorsCanKey;
extern NSString* MCSCCoorsColdCalcKey;