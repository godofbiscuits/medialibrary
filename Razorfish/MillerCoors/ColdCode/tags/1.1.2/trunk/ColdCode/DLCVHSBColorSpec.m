//
//  DLCVHSBColorSpec.m
//  coors_test
//
//  Created by Eamae Mirkin on 7/14/11.
//  Copyright 2011 Deeplocal. All rights reserved.
//

#import "DLCVHSBColorSpec.h"


@implementation DLCVHSBColorSpec

@synthesize hue;
@synthesize saturation;
@synthesize brightness;
@synthesize hueTolerance;
@synthesize saturationTolerance;
@synthesize brightnessTolerance;

-(id) initWithVals:(float)h
                  s:(float)s
                  v:(float)v
                 hT:(float)hT
                 sT:(float)sT
                 vT:(float)vT{
    [super init];
    hue = h;
    saturation = s;
    brightness = v;
    hueTolerance = hT;
    saturationTolerance = sT;
    brightnessTolerance = vT;
    return self;
}

@end
