//
//  MCSCAgeGateModalViewController.h
//  ColdCode
//
//  Created by Jeffrey Barbose on 29-06-11.
//  Copyright 2011 Razorfish, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface MCSCAgeGateModalViewController : UIViewController <UIWebViewDelegate, UINavigationControllerDelegate>
{
    
    UIDatePicker *datePicker;
    UIWebView *tocWarningWebView;
    UILabel *enterYourBirthdayLabel;
    UIBarButtonItem *enterButtonItem;
    
    BOOL tncSheetShown;
}

@property (nonatomic, retain) IBOutlet UIDatePicker *datePicker;
@property (nonatomic, retain) IBOutlet UIWebView *tocWarningWebView;
@property (nonatomic, retain) IBOutlet UILabel *enterYourBirthdayLabel;
@property (nonatomic, retain) UIBarButtonItem *enterButtonItem;



@end
