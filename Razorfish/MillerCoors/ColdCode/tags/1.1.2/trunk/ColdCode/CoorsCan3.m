//
//  CoorsCan.m
//  coors_test
//
//  Created by Patrick D Miller on 7/10/11.
//  Copyright 2011 Deeplocal. All rights reserved.
//

#import "CoorsCan.h"
#import "CanColor.h"
#import "DLCVHSBColorSpec.h"
#import "DLCVRawAnalysis.h"
@implementation CoorsCan

@synthesize currentFrame;
@synthesize contourCount;
@synthesize temperature;
@synthesize isProcessing;
@synthesize cred, cwhite, cblack, cblueB, notred, cblueC, cblueB3, cblueC3;

@synthesize rscoreThreshold;
@synthesize bscoreThreshold;
@synthesize nrscoreThreshold;
@synthesize coldScoreThreshold;
@synthesize coldPixelLength;
@synthesize bottleOrCan, numStripes;

- (BOOL) sendImage:(CMSampleBufferRef)inBuffer
            /* debug:(UILabel *)debug */
{
    if(!isProcessing){
        self.isProcessing = YES;
        
        NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
        UIImage* image2 = [self imageFromSampleBuffer2:inBuffer];
        //convert it to an ipl
        IplImage *tempImage = [self CreateIplImageFromUIImage:image2];
        IplImage *ipl = cvCreateImage(cvSize(tempImage->height, tempImage->width), 8, 3);
        
        //its landscape at first, need to rotate and flip matrix of points
        cvTranspose(tempImage, ipl);
        cvFlip(ipl, ipl, 1);
        cvReleaseImage(&tempImage);
        
        //calc the scores
        float rscore = [cred match:ipl];
        float bscore = [cblack match:ipl];
        float nrscore =[notred match:ipl];
        self.temperature = 0;
        
        //this is all debug info
        
        float rh, rs, rv, bh, bs, bv;
        uchar* rptr = cvPtr2D(ipl,205, 165 , NULL);
        uchar* bptr = cvPtr2D(ipl,313, 157 , NULL);
        
        [DLCVRawAnalysis getHSVFromR:rptr[0] G:rptr[1] B:rptr[2] storeAsH:&rh S:&rs V:&rv];
        [DLCVRawAnalysis getHSVFromR:bptr[0] G:bptr[1] B:bptr[2] storeAsH:&bh S:&bs V:&bv];
        
            //[debug performSelectorOnMainThread:@selector(setText:) withObject:[NSString stringWithFormat: @"r:%d, %d, %d  b:%d, %d, %d    - sr %1.2f, sb %1.2f, snr %1.2f", (int)rh, (int)rs, (int)rv, (int)bh, (int)bs, (int)bv, rscore, bscore, nrscore ] waitUntilDone:YES];
        
        
        //this is how you can draw a rect around a pixel you want to display
        //cvRectangle(ipl, cvPoint(159, 200), cvPoint(170, 213), cvScalar(255,255,0, 100),1,8,0);
        
        //if(rscore > 0.20 && bscore > 0.64 && nrscore < 0.1){
        if(rscore > rscoreThreshold && bscore > bscoreThreshold && nrscore < nrscoreThreshold){
            //NSLog(@"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! r = %f  ::: b = %f  :::nr=%f", rscore, bscore, nrscore);
            matchCount++;
            //we make sure it gets it in 3 consecutive frames
            if(matchCount > 3){
                //float bluescore = [cblue match:ipl];
                //now get the score of the blue
                //int blues = [cblue findConsecutive:ipl pixels:8];
                NSLog(@"bc = %d and numstripes = %d", self.bottleOrCan, self.numStripes);
                if(self.numStripes < 3){
                    if(self.bottleOrCan == 1 || self.bottleOrCan ==3){
                        self.temperature = [cblueC findConsecutive3:ipl pixels:coldPixelLength];
                        NSLog(@"can");
                    }else{
                        self.temperature = [cblueB findConsecutive3:ipl pixels:coldPixelLength];
                        NSLog(@"bottle");
                    }
                }else{
                    if(self.bottleOrCan == 1 || self.bottleOrCan ==3){
                        self.temperature = [cblueC3 findConsecutive3:ipl pixels:coldPixelLength];
                        NSLog(@"can");
                    }else{
                        self.temperature = [cblueB3 findConsecutive3:ipl pixels:coldPixelLength];
                        NSLog(@"bottle");
                    }
                }
                NSLog(@"temp: %i", self.temperature);
                //NSLog(@"blue score = %d", blues);
                return YES;
            }
        }
        else{
            matchCount = 0;
        }
        // NSLog(@"r = %f  ::: b = %f :::nr=%f ", rscore, bscore, nrscore);
        self.currentFrame = [self UIImageFromIplImage:ipl];
        cvReleaseImage(&ipl);
        [pool drain];
        return NO;
    }
    isProcessing = NO;
    return NO;
}

- (IplImage *)CreateIplImageFromCGImageRef:(CGImageRef)image {
	//CGImageRef imageRef = image.CGImage;
    
	CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    //	IplImage *iplimage = cvCreateImage(cvSize(CGImageGetWidth(image)/6, CGImageGetHeight(image)/6), IPL_DEPTH_8U, 4);
    IplImage *iplimage = cvCreateImage(cvSize(CGImageGetWidth(image)/3, CGImageGetHeight(image)/3), IPL_DEPTH_8U, 4);
	CGContextRef contextRef = CGBitmapContextCreate(iplimage->imageData, iplimage->width, iplimage->height,
													iplimage->depth, iplimage->widthStep,
													colorSpace, kCGImageAlphaPremultipliedLast|kCGBitmapByteOrderDefault);
    //	CGContextDrawImage(contextRef, CGRectMake(0, 0, CGImageGetWidth(image)/6, CGImageGetHeight(image)/6), image);
    CGContextDrawImage(contextRef, CGRectMake(0, 0, CGImageGetWidth(image)/3, CGImageGetHeight(image)/3), image);
    
    
    
	CGContextRelease(contextRef);
	CGColorSpaceRelease(colorSpace);
    
	IplImage *ret = cvCreateImage(cvGetSize(iplimage), IPL_DEPTH_8U, 3);
	//cvCvtColor(iplimage, ret, CV_RGBA2BGR);
    cvCvtColor(iplimage, ret, CV_RGBA2RGB);
    
	cvReleaseImage(&iplimage);
    
	return ret;
}

- (IplImage *)CreateIplImageFromUIImage:(UIImage *)image {
	CGImageRef imageRef = image.CGImage;
    
	CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
	IplImage *iplimage = cvCreateImage(cvSize(image.size.width, image.size.height), IPL_DEPTH_8U, 4);
	CGContextRef contextRef = CGBitmapContextCreate(iplimage->imageData, iplimage->width, iplimage->height,
													iplimage->depth, iplimage->widthStep,
													colorSpace, kCGImageAlphaPremultipliedLast|kCGBitmapByteOrderDefault);
	CGContextDrawImage(contextRef, CGRectMake(0, 0, image.size.width, image.size.height), imageRef);
	CGContextRelease(contextRef);
	CGColorSpaceRelease(colorSpace);
    
	IplImage *ret = cvCreateImage(cvGetSize(iplimage), IPL_DEPTH_8U, 3);
    
	cvCvtColor(iplimage, ret, CV_RGBA2RGB);
    //cvCvtColor(iplimage, ret, CV_RGBA2RGBA);
	cvReleaseImage(&iplimage);
    
	return ret;
}

- (UIImage *)UIImageFromIplImage:(IplImage *)image {
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    // Allocating the buffer for CGImage
    NSData *data =
    [NSData dataWithBytes:image->imageData length:image->imageSize];
    CGDataProviderRef provider =
    CGDataProviderCreateWithCFData((CFDataRef)data);
    // Creating CGImage from chunk of IplImage
    CGImageRef imageRef = CGImageCreate(
                                        image->width, image->height,
                                        image->depth, image->depth * image->nChannels, image->widthStep,
                                        colorSpace, kCGImageAlphaNone|kCGBitmapByteOrderDefault,
                                        provider, NULL, false, kCGRenderingIntentDefault
                                        );
    // Getting UIImage from CGImage
    UIImage *ret = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    CGDataProviderRelease(provider);
    CGColorSpaceRelease(colorSpace);
    return ret;
}

- (UIImage *) imageFromSampleBuffer2:(CMSampleBufferRef) sampleBuffer 
{
    // Get a CMSampleBuffer's Core Video image buffer for the media data
    CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer); 
    // Lock the base address of the pixel buffer
    CVPixelBufferLockBaseAddress(imageBuffer, 0); 
    
    // Get the number of bytes per row for the pixel buffer
    void *baseAddress = CVPixelBufferGetBaseAddress(imageBuffer); 
    
    // Get the number of bytes per row for the pixel buffer
    size_t bytesPerRow = CVPixelBufferGetBytesPerRow(imageBuffer); 
    // Get the pixel buffer width and height
    size_t width = CVPixelBufferGetWidth(imageBuffer); 
    size_t height = CVPixelBufferGetHeight(imageBuffer); 
    
    // Create a device-dependent RGB color space
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB(); 
    
    // Create a bitmap graphics context with the sample buffer data
    CGContextRef context = CGBitmapContextCreate(baseAddress, width, height, 8, 
                                                 bytesPerRow, colorSpace, kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedFirst); 
    // Create a Quartz image from the pixel data in the bitmap graphics context
    CGImageRef quartzImage = CGBitmapContextCreateImage(context); 
    // Unlock the pixel buffer
    CVPixelBufferUnlockBaseAddress(imageBuffer,0);
    
    // Free up the context and color space
    CGContextRelease(context); 
    CGColorSpaceRelease(colorSpace);
    
    // Create an image object from the Quartz image
    UIImage *image = [UIImage imageWithCGImage:quartzImage];
    
    // Release the Quartz image
    CGImageRelease(quartzImage);
    
    return (image);
}

-(id) init {
    // NSLog(@"Created a coors can instance");
    //create colors
    self = [super init];
    [[self currentFrame] init];
    self.contourCount = 0;
    self.temperature = 0;
    self.isProcessing = NO;
    matchCount = 0;
    //these are the defaults, bottle with 2 stripes, set these values in the class that uses a coorscan object
    self.bottleOrCan = 0;
    self.numStripes = 3;
    
    
    //set the colors...
    //old red values
    // cred = [[CanColor alloc] initWithColor:200 g:30 b:30 a:255];
    // [cred initColorRange:190 ming:0 minb:0 maxr:255 maxg:30 maxb:30];
    
    //this is for a can....
    //cred = [[CanColor alloc] initWithColor:155 g:19 b:25 a:255];
    cred = [[CanColor alloc] initWithColor:255 g:0 b:0 a:255];
    
    cwhite =[[CanColor alloc] initWithColor:255 g:255 b:255 a:255];
    //[cwhite initColorRange:230 ming:230 minb:230 maxr:255 maxg:255 maxb:255];
    
    cblack = [[CanColor alloc] initWithColor:35 g:32 b:0 a:255];
    //[cblack initColorRange:0 ming:0 minb:0 maxr:30 maxg:30 maxb:30];
    
    //not red is basically a red, but we use it to say 'these spots should not be red'
    notred = [[CanColor alloc] initWithColor:cred.r g:cred.g b:cred.b a:255];
    
    //set the positions for each color
    //[cred addRect:165 y1:205 x2:167 y2:207];
    [cred addRect:159 y1:200 x2:170 y2:213];
    [cred addRect:245 y1:245 x2:255 y2:258];
    [cred addRect:224 y1:209 x2:235 y2:210];
    [cred addRect:129 y1:254 x2:137 y2:261];
    [cred addRect:187 y1:253 x2:209 y2:254];
    // cred.color.hueTolerance = 10;
    //cred.color.hueTolerance = 15;
    cred.color.hueTolerance = 10;
    cred.color.saturationTolerance = 50;
    //cred.color.brightnessTolerance = 60;
    cred.color.brightnessTolerance = 70;
    
    [cblack addRect:156 y1:308 x2:158 y2:319];
    [cblack addRect:161 y1:344 x2:193 y2:346];
    cblack.color.hueTolerance = 180;
    cblack.color.saturationTolerance = 220;
    cblack.color.brightnessTolerance = 40;
    
    [notred addRect:89 y1:89 x2:125 y2:99];
    [notred addRect:233 y1:94 x2:269 y2:103];
    [notred addRect:85 y1:375 x2:114 y2:385];
    [notred addRect:242 y1:373 x2:276 y2:385];
    [notred addRect:42 y1:234 x2:48 y2:279];
    [notred addRect:321 y1:240 x2:328 y2:272];
    notred.color.hueTolerance = cred.color.hueTolerance;
    notred.color.saturationTolerance =cred.color.saturationTolerance;
    notred.color.brightnessTolerance = cred.color.brightnessTolerance;
    
    /* temp detection info ---------------------- */
    //blue for temp detection
    //bottle
    // cblue = [[CanColor alloc] initWithColor:54 g:70 b:179 a:255];
    
    //can
    // cblue = [[CanColor alloc] initWithColor:77 g:121 b:255 a:255];
    
    //bottle default
    // cblue = [[CanColor alloc] initWithColor:33 g:56 b:110 a:225];
    //bottle in the dark...
    
    
    
    /*
     cblue = [[CanColor alloc] initWithColor:33 g:44 b:130 a:225];
     ///maybe use this if its a can??
     //cblue = [[CanColor alloc] initWithColor:87 g:103 b:140 a:255];
     //some possible hsv for cans
     //222,52,30
     //221, 50, 75
     
     [cblue addRect:211 y1:360 x2:212 y2:467];
     [cblue addRect:166 y1:360 x2:166 y2:467];
     [cblue addRect:136 y1:360 x2:136 y2:467];
     //added for can
     [cblue addRect:235 y1:360 x2:236 y2:467];
     [cblue addRect:122 y1:360 x2:123 y2:467];
     //added some more...
     [cblue addRect:270 y1:360 x2:270 y2:467];
     
     //changed this to 15 from 14 since it was missing on darks...
     cblue.color.hueTolerance = 15;
     cblue.color.saturationTolerance = 30;
     cblue.color.brightnessTolerance = 30;
     
     */
    
    
    //crazy loose settings
    //cblue = [[CanColor alloc] initWithColor:41 g:38 b:128 a:225];
    // cblue = [[CanColor alloc]   initWithColor:47 g:45 b:128 a:255];
    
    //2 stripes****************************
    cblueB = [[CanColor alloc]   initWithColor:60 g:58  b:166 a:255];
    
    ///maybe use this if its a can??
    //cblue = [[CanColor alloc] initWithColor:87 g:103 b:140 a:255];
    //some possible hsv for cans
    //222,52,30
    //221, 50, 75
    
    [cblueB addRect:211 y1:390 x2:212 y2:450];
    [cblueB addRect:166 y1:390 x2:166 y2:450];
    [cblueB addRect:136 y1:390 x2:136 y2:450];
    //added for can
    [cblueB addRect:235 y1:390 x2:236 y2:450];
    [cblueB addRect:122 y1:390 x2:123 y2:450];
    //added some more..
    [cblueB addRect:270 y1:390 x2:270 y2:453];
    
    //changed this to 15 from 14 since it was missing on darks...
    //used to be 30
    cblueB.color.hueTolerance = 35;
    cblueB.color.saturationTolerance = 36;
    cblueB.color.brightnessTolerance = 36;
    
    //try this junk for blues on can
    //cblueC = [[CanColor alloc] initWithColor:77 g:121 b:255 a:255];
    //225,70,100
    //what you want i like 225, []40-100], 50-100
    cblueC = [[CanColor alloc] initWithColor:57 g:91 b:191 a:255];
    [cblueC addRect:211 y1:370 x2:212 y2:460];
    [cblueC addRect:166 y1:370 x2:166 y2:460];
    // [cblueC addRect:136 y1:370 x2:136 y2:460];
    [cblueC addRect:235 y1:370 x2:236 y2:460];
    [cblueC addRect:122 y1:370 x2:123 y2:460];
    [cblueC addRect:150 y1:370 x2:150 y2:460];
    cblueC.color.hueTolerance = 35;
    cblueC.color.saturationTolerance = 31;
    cblueC.color.brightnessTolerance = 26;
    
    
    // 3 stripes *******************************
    // cblueB3 = [[CanColor alloc]   initWithColor:60 g:58  b:166 a:255];
    cblueB3 = [[CanColor alloc] initWithColor:38 g:77 b:191 a:255];
    
    ///maybe use this if its a can??
    //cblue = [[CanColor alloc] initWithColor:87 g:103 b:140 a:255];
    //some possible hsv for cans
    //222,52,30
    //221, 50, 75
    
    [cblueB3 addRect:211 y1:360 x2:212 y2:450];
    [cblueB3 addRect:166 y1:360 x2:166 y2:450];
    [cblueB3 addRect:136 y1:360 x2:136 y2:450];
    //added for can
    [cblueB3 addRect:235 y1:360 x2:236 y2:450];
    [cblueB3 addRect:122 y1:360 x2:123 y2:450];
    //added some more..
    [cblueB3 addRect:270 y1:360 x2:270 y2:453];
    
    //changed this to 15 from 14 since it was missing on darks...
    //used to be 30
    cblueB3.color.hueTolerance = 35;
    cblueB3.color.saturationTolerance = 21;
    cblueB3.color.brightnessTolerance = 26;
    
    //try this junk for blues on can
    //cblueC = [[CanColor alloc] initWithColor:77 g:121 b:255 a:255];
    //225,70,100
    //what you want i like 225, []40-100], 50-100
    cblueC3 = [[CanColor alloc] initWithColor:38 g:77 b:191 a:255];
    [cblueC3 addRect:211 y1:360 x2:212 y2:460];
    [cblueC3 addRect:166 y1:360 x2:166 y2:460];
    // [cblueC addRect:136 y1:370 x2:136 y2:460];
    [cblueC3 addRect:235 y1:360 x2:236 y2:460];
    [cblueC3 addRect:122 y1:360 x2:123 y2:460];
    [cblueC3 addRect:150 y1:360 x2:150 y2:460];
    cblueC3.color.hueTolerance = 35;
    cblueC3.color.saturationTolerance = 21;
    cblueC3.color.brightnessTolerance = 26;
    
    //cblueB.showDebug = YES;
    //sent to RF
    //    cblue.color.hueTolerance = 10;
    //    cblue.color.saturationTolerance = 40;
    //    cblue.color.brightnessTolerance = 60;
    //    
    //    cblue.color.hueTolerance = 10;
    //    cblue.color.saturationTolerance = 40;
    //    cblue.color.brightnessTolerance = 60;
    
    /* some can testing 
     cblue.color.hueTolerance = 20;
     cblue.color.saturationTolerance = 32;
     cblue.color.brightnessTolerance = 60;
     */
    //heres some default thresholds, override these if desired
    self.rscoreThreshold = 0.20;
    self.bscoreThreshold = 0.64;
    self.nrscoreThreshold = 0.1;
    self.coldPixelLength = 8;
    
    return self;
}
//222,52,30
//221, 50, 75
- (void)dealloc
{
    self.currentFrame = nil;
    [self.cred release];
    [self.cwhite release];
    [self.cblack release];
    [self.notred release];
    [self.cblueB release];
    [self.cblueB release];
    [self.cblueC release];
    [self.cblueC3 release];
    [super dealloc];
}
@end
