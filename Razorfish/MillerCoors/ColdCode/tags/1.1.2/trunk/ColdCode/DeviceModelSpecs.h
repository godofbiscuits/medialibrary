//
//  DeviceModelSpecs.h
//  coors_test
//
//  Created by Eamae Mirkin on 7/12/11.
//  Copyright 2011 Deeplocal. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum{
    iOS_Device_iPhone_4,
    iOS_Device_iPhone_3GS,
    iOS_Device_iPhone_3G,
    iOS_Device_iPhone_1,
    iOS_Device_iPod_Touch_3,
    iOS_Device_iPod_Touch_2,
    iOS_Device_iPod_Touch_1,
    iOS_Simulator,
}iOS_Device_Type;



@interface DeviceModelSpecs : NSObject 
{
    iOS_Device_Type deviceType;
}

@property (nonatomic,assign) iOS_Device_Type deviceType;



@end
