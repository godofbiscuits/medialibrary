//
//  MCSCWebViewModalViewController.h
//  ColdCode
//
//  Created by Jeffrey Barbose on 02-07-11.
//  Copyright 2011 Razorfish, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface MCSCWebViewModalViewController : UIViewController 
{
    UIWebView *webView;
    NSString* filePath;
    id<UIWebViewDelegate> webDelegate;
}


@property (nonatomic, retain) IBOutlet UIWebView *webView;
@property (nonatomic, retain, setter=setFilePath:) IBOutlet NSString* filePath;
@property (nonatomic, retain) id<UIWebViewDelegate> webDelegate;

- (id) initWithFilePath:(NSString*) filePath delegate:(id<UIWebViewDelegate>)webDelegate;


@end
