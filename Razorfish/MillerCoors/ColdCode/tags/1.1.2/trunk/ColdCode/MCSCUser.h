//
//  MCSCUser.h
//  ColdCode
//
//  Created by Jeff Barbose on 6/24/11.
//  Copyright 2011 Razorfish, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface MCSCUser : NSObject <NSCoding>
{
    NSString* lastLocation;
    NSDate* lastSaveTime;
    NSDate* userBirthdate;
}

@property (retain) NSString* lastLocation;
@property (retain) NSDate* lastSaveTime;
@property (retain) NSDate* userBirthdate;



@end
