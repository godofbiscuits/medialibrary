//
//  DLCVHSBColorSpec.h
//  coors_test
//
//  Created by Eamae Mirkin on 7/14/11.
//  Copyright 2011 Deeplocal. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "DLCVTypes.h"

@interface DLCVHSBColorSpec : NSObject {
    float hue;
    float saturation;
    float brightness;
    float hueTolerance;
    float saturationTolerance;
    float brightnessTolerance;
}

@property (nonatomic, assign) float hue;
@property (nonatomic, assign) float saturation;
@property (nonatomic, assign) float brightness;
@property (nonatomic, assign) float hueTolerance;
@property (nonatomic, assign) float saturationTolerance;
@property (nonatomic, assign) float brightnessTolerance;

-(id) initWithVals:(float)h
                  s:(float)s
                  v:(float)v
                 hT:(float)hT
                 sT:(float)sT
                 vT:(float)vT;

@end
