//
//  MCSCTestLaunchViewController.h
//  ColdCode
//
//  Created by Jeff Barbose on 6/26/11.
//  Copyright 2011 Razorfish, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface MCSCTestLaunchViewController : UIViewController {
    
    UIButton *initialScreenButton;
    UIButton *cameraButton;
    UIButton *countdownScreen;
    UIButton *timesUpButton;
    UIButton *localNotifInTenSecButton;
    UITextView *localNotificationLaunchDictionaryTextView;
    UIButton *ageGateButton;
    
    UILocalNotification* localNotification;
}

@property (nonatomic, retain) IBOutlet UIButton *initialScreenButton;
@property (nonatomic, retain) IBOutlet UIButton *cameraButton;
@property (nonatomic, retain) IBOutlet UIButton *countdownScreen;
@property (nonatomic, retain) IBOutlet UIButton *timesUpButton;
@property (nonatomic, retain) IBOutlet UIButton *localNotifInTenSecButton;
@property (nonatomic, retain) IBOutlet UITextView *localNotificationLaunchDictionaryTextView;
@property (nonatomic, retain) IBOutlet UIButton *ageGateButton;

@property (retain) UILocalNotification* localNotification;




@end
