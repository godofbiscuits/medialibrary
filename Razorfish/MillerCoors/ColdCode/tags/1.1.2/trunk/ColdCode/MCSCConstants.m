//
//  MCSCConstants.m
//  ColdCode
//
//  Created by Jeffrey Barbose on 20/07/2011.
//  Copyright 2011 Razorfish, LLC. All rights reserved.
//

#import "MCSCConstants.h"


NSString* MCSCCoorsContainerBottleOrCanKey = @"MCSCCoorsContainerBottleOrCanKey";
NSString* MCSCCoorsContainerTemperatureKey = @"MCSCCoorsContainerTemperatureKey";
NSString* MCSCCoorsContainerLockedImageKey = @"MCSCCoorsContainerLockedImageKey";
NSString* MCSCCoorsContainerTimeStampLockKey = @"MCSCCoorsContainerTimeStampLockKey";
NSString* MCSCCoorsContainerChillLocationKey = @"MCSCCoorsContainerChillLocationKey";


NSString* MCSCChillCommenceDateKey = @"MCSCChillCommenceDateKey";

NSString* MCSCCoorsCanKey = @"MCSCCoorsCanKey";
NSString* MCSCCoorsColdCalcKey = @"MCSCCoorsColdCalcKey";