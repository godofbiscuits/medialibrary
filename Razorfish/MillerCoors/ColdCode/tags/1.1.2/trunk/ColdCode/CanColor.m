//
//  CanColor.m
//  coors_test
//
//  Created by Patrick D Miller on 7/16/11.
//  Copyright 2011 Deeplocal. All rights reserved.
//

#import "CanColor.h"
#import "DLCVRawAnalysis.h"
#import "DLCVHSBColorSpec.h"

@implementation CanColor
@synthesize r,g,b,a,h,s,v, rects, color, showDebug;
@synthesize type;

-(void) makeHSV{
	float   y, r1,g1,b1;                              
	
	/* first thing, convert them to 0-1*/
	//r = red/255.0;
	//g = green/255.0;
	//b = blue/255.0;
	v = (float)r;
	if (v<g) v=g;
	if (v<b) v=b;
	y = r;
	if (y>g) y=g;
	if (y>b) y=b;
	if (v != 0) s = (v-y)/v;
	else { s = 0; }
	if (s == 0) {
		h = 0;
		s = 0;
        //		v = (int)(v*100);
		v = (int)(v);
        
		//goto setit;
	}    
	else {
        
		r1 = (v-r)/(v-y);
		g1 = (v-g)/(v-y);
		b1 = (v-b)/(v-y);
		if (r == v){
			if (g == y) h = 5.+b1;
			else h = 1.-g1;
		}
		else if (g == v){
			if (b == y) h = r1+1.;
			else h = 3.-b1;
		}
		else{
			if (r == y) h = 3.+g1;
			else h = 5.-r1;
		}
		// convert it all
		h = h * 60.; 
		if (h >= 360.) h = h-360.;
	}
	h = (int)h;   //hue = (int)((h/360.)*255.0);
	s = (int)(s*100.); /* was 0-1 from the start */
    //	v = (int)(v*100.); /* it was 0-1 from the start */
	v = (int)(v/2.55); /* not 100% sure why this works, but... it does -Eamae*/
    
	
}
-(id) initWithColor:(float)inr
                  g:(float)ing
                  b:(float)inb
                  a:(float)ina{
    
    
    [super init];
    self.r = inr;
    self.g = ing;
    self.b = inb;
    self.a = ina;
    [DLCVRawAnalysis getHSVFromR:r G:g B:b storeAsH:&h S:&s V:&v];
    color = [[DLCVHSBColorSpec alloc] initWithVals:h s:s v:v hT:10 sT:10 vT:20];
    //hardcode error for now...
    herror = 10;
    serror = 10;
    verror = 10;
    self.rects =  [[NSMutableArray alloc ] init];
    return self;
}


-(void) initColorRange:(float)inrmin
                  ming:(float)ingmin
                  minb:(float)inbmin
                  maxr:(float)inrmax
                  maxg:(float)ingmax
                  maxb:(float)inbmax{
    rmin = inrmin;
    gmin = ingmin;
    bmin = ingmin;
    rmax = inrmax;
    gmax = ingmax;
    bmax = inbmax;
}

-(CvScalar) to_cvscalar{
    CvScalar cvs = cvScalar(r,g,b,a);
    return cvs;
}



-(float) match:(IplImage*)inImg{
    
    float score = 0;
    for(int i =0; i < [rects count] ; i ++){
        CGRect tempRect = [[rects objectAtIndex:i] CGRectValue];
        score = ((score * (i+1)) + [DLCVRawAnalysis   getPercentOfColor:color inImage:inImg insideRect:&tempRect])/ (i+1);
    }
    return (score / [self.rects count]);
}

-(int ) findConsecutive:(IplImage*)inImg
                 pixels:(int) pixels{
    //return the number of blocks that have consecutive pixels within threshold.
    float match=0;
    int matches =0;
    int totalMatchesC=0;
    int totalMatchesS=0;
    for(int i =0; i < [rects count] ; i ++){
        if(showDebug==YES){
            NSLog(@"newstripe");
        } 
        matches = 0;
        CGRect tempRect = [[rects objectAtIndex:i] CGRectValue];
        float x = CGRectGetMinX(tempRect);
        int pixelMatches =0;
        //now for each vertical pixel in the rect we check against getPercentofcolor
        for(int i=(int)CGRectGetMinY(tempRect); i < (int)CGRectGetMaxY(tempRect); i++){
            CGRect pixelRect = CGRectMake(x, (float)i , 1, 1);
            if(showDebug==YES){
                match = [DLCVRawAnalysis   getPercentOfColorD:color inImage:inImg insideRect:&pixelRect];
            }else{
                match = [DLCVRawAnalysis   getPercentOfColor:color inImage:inImg insideRect:&pixelRect];
            }            if(match > 0){
                if(showDebug==YES){
                    NSLog(@"yes");
                }
                pixelMatches++;
            }else{
                if(showDebug==YES){
                    NSLog(@"no");
                }
                //if previous was a match, then we add matches++ if pixelmatches > pixels

                if(pixelMatches >=pixels){
                    matches++;
                }
                pixelMatches=1;
            }

        }
        if(matches > 0){
            totalMatchesC++;
            if(matches > 1){
                totalMatchesS++;
                
            }
        }
    }
    
    if(totalMatchesS > 0){
        return 2;
    }
    if(totalMatchesC > 0){
        return 1;
    }
    return 0;
}

-(int ) findConsecutive3:(IplImage*)inImg
                 pixels:(int) pixels{
    //return the number of blocks that have consecutive pixels within threshold.
    float match=0;
    int matches =0;
    int totalMatchesC=0;
    int totalMatchesS=0;
    int totalMatchesR=0;
    
    for(int i =0; i < [rects count] ; i ++){
        if(showDebug==YES){
            NSLog(@"newstripe");
        }        matches = 0;
        CGRect tempRect = [[rects objectAtIndex:i] CGRectValue];
        float x = CGRectGetMinX(tempRect);
        int pixelMatches =0;
        //now for each vertical pixel in the rect we check against getPercentofcolor
        for(int i=(int)CGRectGetMinY(tempRect); i < (int)CGRectGetMaxY(tempRect); i++){
            CGRect pixelRect = CGRectMake(x, (float)i , 1, 1);
            if(showDebug==YES){
                match = [DLCVRawAnalysis   getPercentOfColorD:color inImage:inImg insideRect:&pixelRect];
            }else{
                match = [DLCVRawAnalysis   getPercentOfColor:color inImage:inImg insideRect:&pixelRect];
            }
            if(match > 0){
                if(showDebug==YES){
                      NSLog(@"yes");
                }
                pixelMatches++;
            }else{
                if(showDebug==YES){
                    NSLog(@"no");
                }
                //if previous was a match, then we add matches++ if pixelmatches > pixels
                
                if(pixelMatches >=pixels){
                    matches++;
                }
                pixelMatches=1;
            }
            
        }
        if(matches > 0){
            totalMatchesR++;
            if(matches > 1){
                totalMatchesC++;
                if(matches > 2){
                    totalMatchesS++;
                }
                
            }
        }
    }

    //if super cold
    if(totalMatchesS > 0){
        return 3;
    }
    //if cold
    if(totalMatchesC > 0){
        return 2;
    }
    //if room temp
    if(totalMatchesR > 0){
        return 1;
    }
    return 0;
}

- (void) drawRects:(IplImage *)inimg
{
    
}


-(void)addRect:(float)x1 y1:(float)y1 x2:(float)x2 y2:(float)y2{
    [self.rects addObject:[NSValue valueWithCGRect:CGRectMake(x1, y1, (x2-x1), (y2-y1))]];
}

-(void) log{
    CGRect tempRect = [[rects objectAtIndex:0] CGRectValue];
   NSLog(@"%i rects. First rect(%f, %f, %f, %f); rgb:(%f, %f, %f); hsb:(%f,%f,%f)", [rects count], CGRectGetMinX(tempRect), CGRectGetMinY(tempRect), CGRectGetMaxX(tempRect), CGRectGetMaxY(tempRect), r, g, b, h, s, v);
}


- (void) dealloc {
    [self.rects release];
    [self.color release];
    [super dealloc];
}
@end
