//
//  DLCVMath.h
//  coors_test
//
//  Created by Eamae Mirkin on 7/13/11.
//  Copyright 2011 Deeplocal. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "DLCV.h"

#ifndef PI
#define PI       3.14159265358979323846
#endif

#ifndef TWO_PI
#define TWO_PI   6.28318530717958647693
#endif

@interface DLCVMath : NSObject {
    
}

+ (angle_f) circularSubtraction:(angle_f)v1 minus:(angle_f)v2 withMin:(float)minValue andMax:(float)maxValue;

+ (angle_f) circularSubtraction:(angle_f)v1 minus:(angle_f)v2;

@end
