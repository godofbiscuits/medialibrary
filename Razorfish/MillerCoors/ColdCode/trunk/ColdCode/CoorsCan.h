//
//  CoorsCan.h
//  coors_test
//
//  Created by Patrick D Miller on 7/10/11.
//  Copyright 2011 Deeplocal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <opencv2/imgproc/imgproc_c.h>
#import <opencv2/objdetect/objdetect.hpp>
#import <CoreGraphics/CoreGraphics.h>
#import <CoreVideo/CoreVideo.h>
#import <CoreMedia/CoreMedia.h>
@class CanColor;
//@class CoorsRect;

@interface CoorsCan : NSObject {
    //thresholds for color detection
    float rscoreThreshold;
    float bscoreThreshold;
    float nrscoreThreshold;
    float coldScoreThreshold;
    float coldPixelLength;
    int numStripes;
    //bottleORCan - not currently used, but may be in the future.
    //0 = normal size bottle
    //1 = normal size can
    //2 = small bott;e
    //3 = big can
    int bottleOrCan;
    
    //returns 0 for room, 1 for cold, 2 for super cold
    int temperature;
    int contourCount;
    //current frame that is drawn every frame that this class reads
    UIImage *currentFrame;
    BOOL isProcessing;
    float scaleX, scaleY;
    
    //colors
    CanColor* cred;
    CanColor* cwhite;
    CanColor* cblack;
    CanColor* notred;
    CanColor* cblueB;
    CanColor* cblueB3;
    CanColor* cblueC;
    CanColor* cblueC3;
    //how many consecutive frames have matched.
    int matchCount;
    
}

@property (nonatomic, assign) int temperature;
@property (nonatomic, assign) int contourCount;
@property (nonatomic, retain) UIImage  *currentFrame;
@property (nonatomic, assign) BOOL isProcessing;
@property (nonatomic, retain) CanColor* cred;
@property (nonatomic, retain) CanColor* cwhite;
@property (nonatomic, retain) CanColor* cblack;

@property (nonatomic, retain) CanColor* notred;
@property (nonatomic, retain) CanColor* cblueB;
@property (nonatomic, retain) CanColor* cblueB3;
@property (nonatomic, retain) CanColor* cblueC;
@property (nonatomic, retain) CanColor* cblueC3;


@property float rscoreThreshold;
@property float bscoreThreshold;
@property float nrscoreThreshold;
@property float coldScoreThreshold;
@property float coldPixelLength;
@property int numStripes;
@property int bottleOrCan;

//send an image and it will update the temperature of the instance... 
//returns true when can detected. 

//debug version that takes a label to output debug info
- (BOOL) sendImage:(CMSampleBufferRef)inBuffer; //debug:(UILabel*)debug;

//- (BOOL) sendImage:(CMSampleBufferRef)inBuffer;

- (IplImage *)CreateIplImageFromCGImageRef:(CGImageRef)image;
- (IplImage *)CreateIplImageFromUIImage:(UIImage *)image;
- (UIImage *)UIImageFromIplImage:(IplImage *)image;
- (UIImage *) imageFromSampleBuffer2:(CMSampleBufferRef) sampleBuffer ;


@end
