//
//  ColdCodeAppDelegate.h
//  ColdCode
//
//  Created by Jeff Barbose on 6/24/11.
//  Copyright 2011 Razorfish, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "MCSCUser.h"
#import "CoorsColdCalc.h"


@class ColdCodeAppDelegate;

extern ColdCodeAppDelegate* coldCodeDelegate;


/*
 typedef enum {
    kFreezer = 1001,
    kFridge = 1002,
    kCooler = 1003
} MCSCChillLocationType;
 */

    // post-prototype, move these to constants.h-type file
extern NSString* MCSCUserDidPassAgeGateNotification;

extern NSString* MCSCAbsoluteSuperColdTimeChangedNotification;
extern NSString* MCSCAbsoluteColdTimeChangedNotification;

extern NSString* MCSCCameraDidAcquireNewInformationNotification;

extern NSString* MCSCColdReachedLocalNotification;
extern NSString* MCSCSuperColdReachedLocalNotification;

extern NSString* MCSCAbsoluteTime;


@interface ColdCodeAppDelegate : NSObject <UIApplicationDelegate> 
{
    MCSCUser* user;
    NSDate* absoluteSuperColdTime;
    NSDate* absoluteColdTime;

    UILocalNotification* coldLocalNotification;
    UILocalNotification* superColdLocalNotification;
    
        //UILocalNotification* sentinelLocalNotification;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;

@property (nonatomic, retain, readonly) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, retain, readonly) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, retain, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@property (nonatomic, retain) IBOutlet UINavigationController *navigationController;

@property (retain) NSDictionary* localNotificationLaunchDictionary;

@property (retain, setter=setUser:, getter=user) MCSCUser* user;

@property (retain, getter=absoluteSuperColdTime, setter=setAbsoluteSuperColdTime:) NSDate* absoluteSuperColdTime;
@property (retain, getter=absoluteColdTime, setter=setAbsoluteColdTime:) NSDate* absoluteColdTime;

@property (nonatomic, retain) UILocalNotification* coldLocalNotification;
@property (nonatomic, retain) UILocalNotification* superColdLocalNotification;

    //@property (nonatomic, retain) UILocalNotification* sentinelLocalNotification;


+ (ColdCodeAppDelegate*) coldCodeDelegate;


+ (UIFont*) bodyFont;
+ (UIFont*) buttonFont;
+ (UIFont*) clockFont;

+ (MCSCUser*) user;
+ (BOOL) saveUser;

+ (BOOL) saveChillLocation:(MCSCChillLocationType)newChillLocation;
+ (MCSCChillLocationType) chillLocation;
+ (NSString*) resourceNameForCurrentChillLocation;

+ (BOOL) saveUserAge:(NSDate*)newUserAge;

+ (NSString*) resourceNameForChillLocation:(MCSCChillLocationType)chillLocation;


@end
