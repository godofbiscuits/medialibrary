//
//  MCSCCameraViewController.h
//  ColdCode
//
//  Created by Jeffrey Barbose on 03-07-11.
//  Copyright 2011 Razorfish, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@class CoorsCan;
@class SoundEffect;


@interface MCSCCameraViewController : UIViewController <AVCaptureVideoDataOutputSampleBufferDelegate>
{
    UIView *videoPreviewView;
    UIBarButtonItem *bottleOrCanToggleControl;
    UIBarButtonItem* cancelScanButtonItem;
    
    AVCaptureSession *captureSession;


    BOOL shouldProcess;
    
    CoorsCan* coorsCan;
    
    UIImageView* spots;
    UIImageView* overlay;
    
    SoundEffect* lockSound;
}

@property BOOL shouldProcess;
@property (retain) AVCaptureSession* captureSession;

@property (retain) CoorsCan* coorsCan;

@property (nonatomic, retain) UIImageView* spots;
@property (nonatomic, retain) UIImageView* overlay;



    // IBOutets
@property (nonatomic, retain) IBOutlet UIView *videoPreviewView;
@property (nonatomic, retain) IBOutlet UIBarButtonItem *bottleOrCanToggleControl;
@property (nonatomic, retain) IBOutlet UIBarButtonItem* cancelScanButtonItem;

@property (nonatomic, readonly, retain) SoundEffect* lockSound;


@end
