//
//  DeviceModelSpecs.m
//  coors_test
//
//  Created by Eamae Mirkin on 7/12/11.
//  Copyright 2011 Deeplocal. All rights reserved.
//

#import "DeviceModelSpecs.h"
#include <sys/types.h>
#include <sys/sysctl.h>


@implementation DeviceModelSpecs

@synthesize deviceType;

-(id) init
{
    self = [super init];
    
    // Figure out what kind of device we're running on
    
    size_t size;
    
    // Set 'oldp' parameter to NULL to get the size of the data
    // returned so we can allocate appropriate amount of space
    sysctlbyname("hw.machine", NULL, &size, NULL, 0); 
    
    // Allocate the space to store name
    char *name = malloc(size);
    
    // Get the platform name
    sysctlbyname("hw.machine", name, &size, NULL, 0);
    
    // Place name into a string
    NSString *machine = [NSString stringWithUTF8String:name];
    
    if([machine isEqualToString: @"iPhone3,1"]){
        deviceType = iOS_Device_iPhone_4;
    }
    else if([machine isEqualToString: @"iPhone2,1"]){
        deviceType = iOS_Device_iPhone_3GS;
    }
    else if([machine isEqualToString: @"iPhone1,2"]){
        deviceType = iOS_Device_iPhone_3G;
    }    
    else if([machine isEqualToString: @"iPhone1,1"]){
        deviceType = iOS_Device_iPhone_1;
    }    
    else if([machine isEqualToString: @"iPod3,1"]){
        deviceType = iOS_Device_iPod_Touch_3;
    }
    else if([machine isEqualToString: @"iPod2,1"]){
        deviceType = iOS_Device_iPod_Touch_2;
    }
    else if([machine isEqualToString: @"iPod3,1"]){
        deviceType = iOS_Device_iPod_Touch_1;
    }
    else if([machine isEqualToString: @"i386"]){
        deviceType = iOS_Simulator;
    }
    
    free(name);
    
    return self;
}

@end
