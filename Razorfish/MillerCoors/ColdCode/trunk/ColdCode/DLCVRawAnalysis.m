//
//  DLCVRawAnalysis.m
//  coors_test
//
//  Created by Eamae Mirkin on 7/16/11.
//  Copyright 2011 Deeplocal. All rights reserved.
//
@class DLCVHSBColorSpec;
#import "DLCVMath.h"
#import "DLCVRawAnalysis.h"


@implementation DLCVRawAnalysis
// percent of pixels that matched within a threshold
+ (float) getPercentOfColor:(DLCVHSBColorSpec*)color 
                    inImage:(IplImage*)image 
                 insideRect:(CGRect*)roi{    // some old code...
   // char* pixelData = image->imageData;
   // int bytesPerPixel = image->nChannels;
   // int totalPixelSize = image->width * image->height * bytesPerPixel;
    
    int roiHeight = (int)floor(CGRectGetHeight(*roi));
    int roiWidth  = (int)floor(CGRectGetWidth(*roi));
    
    int totalMatches = 0;
    
    for (int y=0; y<roiHeight; y++){
		for (int x=0; x<roiWidth; x++) {			
			uchar *ptr = cvPtr2D(image, (CGRectGetMinY(*roi) + y), CGRectGetMinX(*roi) + x, NULL);
			if(true){	
				float pixelHue, pixelSaturation, pixelBrightness;
				
                [DLCVRawAnalysis getHSVFromR:ptr[0] G:ptr[1] B:ptr[2] storeAsH:&pixelHue S:&pixelSaturation V:&pixelBrightness];
				//NSLog(@" the red is %d, %d, %d   ---  %1.2f, %1.2f, %1.2f",ptr[0] , ptr[1] , ptr[2],  pixelHue, pixelSaturation, pixelBrightness);
                
				if (fabs([DLCVMath circularSubtraction:pixelHue minus:color.hue withMin:0 andMax:360] < color.hueTolerance) && 
                         fabs(pixelSaturation - color.saturation) < color.saturationTolerance   &&
                         fabs(pixelBrightness - color.brightness) < color.brightnessTolerance) {
                    totalMatches++;
				}
            }
		}
	}
	
	return (float)totalMatches/(float)(roiWidth*roiHeight);
    
    
}

+ (float) getPercentOfColorD:(DLCVHSBColorSpec*)color 
                    inImage:(IplImage*)image 
                  insideRect:(CGRect*)roi{
    // some old code...
    // char* pixelData = image->imageData;
    // int bytesPerPixel = image->nChannels;
    // int totalPixelSize = image->width * image->height * bytesPerPixel;
    
    int roiHeight = (int)floor(CGRectGetHeight(*roi));
    int roiWidth  = (int)floor(CGRectGetWidth(*roi));
    
    int totalMatches = 0;
    
    for (int y=0; y<roiHeight; y++){
		for (int x=0; x<roiWidth; x++) {			
			uchar *ptr = cvPtr2D(image, (CGRectGetMinY(*roi) + y), CGRectGetMinX(*roi) + x, NULL);
			if(true){	
				float pixelHue, pixelSaturation, pixelBrightness;
				
                [DLCVRawAnalysis getHSVFromR:ptr[0] G:ptr[1] B:ptr[2] storeAsH:&pixelHue S:&pixelSaturation V:&pixelBrightness];
                NSLog(@"Comparing %2.1f, %2.1f, %2.1f WITH %2.1f, %2.1f, %2.1f", color.hue, color.saturation, color.brightness, pixelHue, pixelSaturation, pixelBrightness);
				//NSLog(@" the red is %d, %d, %d   ---  %1.2f, %1.2f, %1.2f",ptr[0] , ptr[1] , ptr[2],  pixelHue, pixelSaturation, pixelBrightness);
                
				if (fabs([DLCVMath circularSubtraction:pixelHue minus:color.hue withMin:0 andMax:360] < color.hueTolerance) && 
                    fabs(pixelSaturation - color.saturation) < color.saturationTolerance   &&
                    fabs(pixelBrightness - color.brightness) < color.brightnessTolerance) {
                    totalMatches++;
				}
            }
		}
	}
	
	return (float)totalMatches/(float)(roiWidth*roiHeight);
    
    
}
+ (void) getHSVFromR:(float)r 
                   G:(float)g 
                   B:(float)b
            storeAsH:(float*)_h 
                   S:(float*)_s 
                   V:(float*)_v{
    
	double   y, r1,g1,b1;                              
    float   h,s,v;
    
	v = r;
	if (v<g) v=g;
	if (v<b) v=b;
	y = r;
	if (y>g) y=g;
	if (y>b) y=b;
	if (v != 0) s = (v-y)/v;
	else { s = 0; }
	if (s == 0) {
		h = 0;
		s = 0;
        //		v = (int)(v*100);
		v = (int)(v);
        
	}    
	else {
        
		r1 = (v-r)/(v-y);
		g1 = (v-g)/(v-y);
		b1 = (v-b)/(v-y);
		if (r == v){
			if (g == y) h = 5.+b1;
			else h = 1.-g1;
		}
		else if (g == v){
			if (b == y) h = r1+1.;
			else h = 3.-b1;
		}
		else{
			if (r == y) h = 3.+g1;
			else h = 5.-r1;
		}
		// convert it all
		h = h * 60.; 
		if (h >= 360.) h = h-360.;
	}
	*_h = (int)h;   //hue = (int)((h/360.)*255.0);
	*_s = (int)(s*100.); /* was 0-1 from the start */
	*_v = (int)(v/2.55); /* not 100% sure why this works, but... it does -Eamae*/
}

@end
