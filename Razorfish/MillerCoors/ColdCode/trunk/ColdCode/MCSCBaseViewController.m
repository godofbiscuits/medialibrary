//
//  MCSCBaseViewController.m
//  ColdCode
//
//  Created by Jeffrey Barbose on 02-07-11.
//  Copyright 2011 Razorfish, LLC. All rights reserved.
//

#import "MCSCAgeGateModalViewController.h"
#import "MCSCBaseViewController.h"
#import "ColdCodeAppDelegate.h"
#import "MCSCCameraViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "CoorsColdCalc.h"
#import "CoorsCan.h"


const NSTimeInterval kMCSCTimerFireTimeInterval = 0.5;  // 10 times / second
NSTimeInterval kMCSCTimerBlueMountainsAnimationInterval = 0.5 / 3;  




@interface MCSCBaseViewController ()

- (void) presentAgeGateIfNecessary;

- (void) handleUserAge:(NSNotification*)n;
- (void) adjustTimerOnNewSuperColdTime:(NSNotification*)n;
- (void) aboutToReturnFromCamera:(NSNotification*)n;
- (void) configureOnBecomeActive:(NSNotification*)n;

- (void) updateClock:(NSTimer*)updateClockLabelTimer;

- (void) abortCountdownAndResetApp;

    //- (void) postLocalNotificationName:(NSString*)localNotificationName fireDate:(NSDate*)when userInfo:(NSDictionary*)userInfo;

@end

@interface MCSCBaseViewController (WebViewLoading)

- (void) populateUserStartLabelWebView;
- (void) populateTimeTilSuperColdLabelWebView;
- (void) populateSuperColdIsHereLabelWebView;

@end



@interface MCSCBaseViewController (SubViewUpdating)

- (void) updateCountdownTimeMinutes:(NSInteger)minutesRemaining seconds:(NSInteger)secondsRemaining;

- (void) updateBlueMountains:(CGFloat)opacity;
- (void) updateFrost:(CGFloat) opacity;

- (void) configureForSuperColdIsHere;
- (void) performWarmRestartTransition;

- (void) performSuperColdIsHereTransition;


@end



@implementation MCSCBaseViewController

@synthesize userIsOldEnough;

@synthesize marqueeLocationView;
@synthesize freezerButton;
@synthesize fridgeButton;
@synthesize coolerButton;
@synthesize chillLocationQuestionLabel;
@synthesize blueFillerImageView;
@synthesize coldnessBannerView;
@synthesize frostOverlayImageView;
@synthesize marqueeUserStartCountdownView;
@synthesize startCountdownStartButton;
@synthesize startCountdownCancelButton;
@synthesize startCountdownLabelWebView;
@synthesize marqueeSuperColdTimerView;
@synthesize timeTilSuperColdLabelWebView;
@synthesize timeTilSuperColdCancelButton;
@synthesize minutesLabel;
@synthesize secondsLabel;
@synthesize marqueeSuperColdIsHereView;
@synthesize superColdIsHereChillAnotherButton;
@synthesize superColdIsHereWebView;
@synthesize timeSeparatorLabel;
@synthesize clockLabelUpdaterTimer;

@synthesize currentUserAppState;

@synthesize didPassColdTime;


    //@synthesize coldLocalNotification;
    //@synthesize superColdLocalNotification;

@synthesize sentinelFreezerNotification;

@synthesize toUserSuperColdIsHere;
@synthesize toUserStartCountdown;

@synthesize calc;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) 
    {
        userIsOldEnough = NO;
        
        didPassColdTime = NO;
        sentinelFreezerNotification = nil;
        
        [[UIApplication sharedApplication] cancelAllLocalNotifications];
    }
    return self;
}

- (void)dealloc
{
    [freezerButton release];
    [fridgeButton release];
    [coolerButton release];
    [chillLocationQuestionLabel release];
    [blueFillerImageView release];
    [coldnessBannerView release];
    [marqueeLocationView release];
    [marqueeUserStartCountdownView release];
    [marqueeSuperColdTimerView release];
    [startCountdownLabelWebView release];
    [startCountdownStartButton release];
    [startCountdownCancelButton release];
    [timeTilSuperColdLabelWebView release];
    [timeTilSuperColdCancelButton release];
    
    [sentinelFreezerNotification release];
    
    [marqueeSuperColdIsHereView release];
    [superColdIsHereChillAnotherButton release];
    [superColdIsHereWebView release];
    [frostOverlayImageView release];
    [minutesLabel release];
    [secondsLabel release];
    [timeSeparatorLabel release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}



#pragma mark - WebView Subview Populate Methods

- (void) populateUserStartLabelWebView
{
        // load the HTML for the webView
    MCSCChillLocationType loc = [ColdCodeAppDelegate chillLocation];
    NSString* chillLocationStr = [ColdCodeAppDelegate resourceNameForChillLocation:loc];
    NSString* fileName = [NSString stringWithFormat:@"%@Start", chillLocationStr];
    
    NSError* err = nil;
    
    NSString* htmlString = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:fileName ofType:@"html"] encoding:NSUTF8StringEncoding error:&err];
    
    if ( err == nil )
    {
        [self.startCountdownLabelWebView loadHTMLString:htmlString baseURL:nil];
        
        self.startCountdownLabelWebView.opaque = NO;
        self.startCountdownLabelWebView.backgroundColor = [UIColor clearColor];
    }
}





- (void) populateTimeTilSuperColdLabelWebView
{
        // load the HTML for the webView    
    NSError* err = nil;
    
    NSString* htmlString = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"TimeTilSuperCold" ofType:@"html"] encoding:NSUTF8StringEncoding error:&err];
    
    if ( err == nil )
    {
        [self.timeTilSuperColdLabelWebView loadHTMLString:htmlString baseURL:nil];
        
        self.timeTilSuperColdLabelWebView.opaque = NO;
        self.timeTilSuperColdLabelWebView.backgroundColor = [UIColor clearColor];
    }
}



- (void) populateSuperColdIsHereLabelWebView
{
    // load the HTML for the webView
    MCSCChillLocationType loc = [ColdCodeAppDelegate chillLocation];
    NSString* chillLocationStr = [ColdCodeAppDelegate resourceNameForChillLocation:loc];
    NSString* fileName = [NSString stringWithFormat:@"CheckThe%@", chillLocationStr];
    
    NSError* err = nil;
    
    NSString* htmlString = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:fileName ofType:@"html"] encoding:NSUTF8StringEncoding error:&err];
    
    if ( err == nil )
    {
        [self.superColdIsHereWebView loadHTMLString:htmlString baseURL:nil];
        
        self.superColdIsHereWebView.opaque = NO;
        self.superColdIsHereWebView.backgroundColor = [UIColor clearColor];
    }
}



#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIFont* buttonFont = [ColdCodeAppDelegate buttonFont];
        
    UIImage* buttonBackgroundImage = [[UIImage imageNamed:@"button.png"] stretchableImageWithLeftCapWidth:8 topCapHeight:0];
    UIImage* buttonBackgroundHighlightImage = [[UIImage imageNamed:@"highlightedButton.png"] stretchableImageWithLeftCapWidth:8 topCapHeight:0];
    
    
    /* choose location subviews */
    
    self.freezerButton.titleLabel.font = buttonFont;
    [self.freezerButton setBackgroundImage:buttonBackgroundImage forState: UIControlStateNormal];
    [self.freezerButton setBackgroundImage:buttonBackgroundHighlightImage forState: UIControlStateHighlighted];
    
    self.fridgeButton.titleLabel.font = buttonFont;
    [self.fridgeButton setBackgroundImage:buttonBackgroundImage forState: UIControlStateNormal];
    [self.fridgeButton setBackgroundImage:buttonBackgroundHighlightImage forState: UIControlStateHighlighted];
    
    self.coolerButton.titleLabel.font = buttonFont;
    [self.coolerButton setBackgroundImage:buttonBackgroundImage forState: UIControlStateNormal];
    [self.coolerButton setBackgroundImage:buttonBackgroundHighlightImage forState: UIControlStateHighlighted];
    
    self.chillLocationQuestionLabel.font = [ColdCodeAppDelegate bodyFont];
    
    
    
    
    /* user finalizes chill transaction subviews */
    
    self.startCountdownStartButton.titleLabel.font = buttonFont;
    [self.startCountdownStartButton setBackgroundImage:buttonBackgroundImage forState: UIControlStateNormal];
    [self.startCountdownStartButton setBackgroundImage:buttonBackgroundHighlightImage forState: UIControlStateHighlighted];

    self.startCountdownCancelButton.titleLabel.font = buttonFont;
    [self.startCountdownCancelButton setBackgroundImage:buttonBackgroundImage forState: UIControlStateNormal];
    [self.startCountdownCancelButton setBackgroundImage:buttonBackgroundHighlightImage forState: UIControlStateHighlighted];
    
        // can't do this until the user selects a location
        //[self populateUserStartLabelWebView];
    
    
    
    
    
    /* main event - the countdown timer subviews */
    
    self.timeTilSuperColdCancelButton.titleLabel.font = buttonFont;
    [self.timeTilSuperColdCancelButton setBackgroundImage:buttonBackgroundImage forState: UIControlStateNormal];
    [self.timeTilSuperColdCancelButton setBackgroundImage:buttonBackgroundHighlightImage forState: UIControlStateHighlighted];
    
    self.secondsLabel.font = [ColdCodeAppDelegate clockFont];
    self.minutesLabel.font = [ColdCodeAppDelegate clockFont];
    self.timeSeparatorLabel.font = [ColdCodeAppDelegate clockFont];
    
    [self populateTimeTilSuperColdLabelWebView];
    
    
    
    
    
    /* super cold is here subviews */
    
    self.superColdIsHereChillAnotherButton.titleLabel.font = buttonFont;
    [self.superColdIsHereChillAnotherButton setBackgroundImage:buttonBackgroundImage forState: UIControlStateNormal];
    [self.superColdIsHereChillAnotherButton setBackgroundImage:buttonBackgroundHighlightImage forState: UIControlStateHighlighted];
    
    
    
    
    
    
    [self.navigationController setNavigationBarHidden:YES];

    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    
    [nc addObserver:self selector:@selector(handleUserAge:) name:MCSCUserDidPassAgeGateNotification object:nil];
    
    [nc addObserver:self selector:@selector(adjustTimerOnNewSuperColdTime:) name:MCSCAbsoluteSuperColdTimeChangedNotification object:nil];
    [nc addObserver:self selector:@selector(adjustTimerOnNewSuperColdTime:) name:MCSCAbsoluteColdTimeChangedNotification object:nil];
    
    

    [nc addObserver:self selector:@selector(aboutToReturnFromCamera:) name:MCSCCameraDidAcquireNewInformationNotification object:nil];
    
    self.currentUserAppState = MCSCUserStateColdStart;
    
    
    
    //self.blueFillerImageView.alpha = 0.0;
    [self updateBlueMountains:0.0];
     
    self.didPassColdTime = NO;

}

- (void)viewDidUnload
{
    [self setFreezerButton:nil];
    [self setFridgeButton:nil];
    [self setCoolerButton:nil];
    [self setChillLocationQuestionLabel:nil];
    [self setBlueFillerImageView:nil];
    [self setColdnessBannerView:nil];
    [self setMarqueeUserStartCountdownView:nil];
    [self setMarqueeLocationView:nil];
    
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    
    [nc removeObserver:self name:MCSCUserDidPassAgeGateNotification object:nil];
    
    [nc removeObserver:self name:MCSCAbsoluteSuperColdTimeChangedNotification object:nil];
    [nc removeObserver:self name:MCSCAbsoluteColdTimeChangedNotification object:nil];
    [nc removeObserver:self name:MCSCCameraDidAcquireNewInformationNotification object:nil];
        
    [self setMarqueeUserStartCountdownView:nil];
    [self setMarqueeSuperColdTimerView:nil];
    [self setStartCountdownLabelWebView:nil];
    [self setStartCountdownStartButton:nil];
    [self setStartCountdownCancelButton:nil];
    [self setTimeTilSuperColdLabelWebView:nil];
    [self setTimeTilSuperColdCancelButton:nil];
    [self setMarqueeSuperColdIsHereView:nil];
    [self setSuperColdIsHereChillAnotherButton:nil];
    [self setSuperColdIsHereWebView:nil];
    [self setFrostOverlayImageView:nil];
    [self setMinutesLabel:nil];
    [self setSecondsLabel:nil];
    [self setTimeSeparatorLabel:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    NSLog( @"Current app state = %u", self.currentUserAppState );
    
    switch ( self.currentUserAppState ) 
    {
        case MCSCUserStateColdStart:
        {
            self.marqueeLocationView.hidden = NO;
            self.marqueeUserStartCountdownView.hidden = YES;
            self.marqueeSuperColdTimerView.hidden = YES;
            self.marqueeSuperColdIsHereView.hidden = YES;
            
            currentUserAppState = MCSCUserStateWarmRestart;
            
            self.didPassColdTime = NO;
        }
            break;
            
            
        case MCSCUserStateWarmRestart:
        {
            self.marqueeLocationView.hidden = NO;
            self.marqueeUserStartCountdownView.hidden = YES;
            self.marqueeSuperColdTimerView.hidden = YES;
            self.marqueeSuperColdIsHereView.hidden = YES;
            
            self.didPassColdTime = NO;

        }
            break;
            
            
        case MCSCUserStateDidJustAcquireColdData:
        {
            [self updateCountdownTimeMinutes:0 seconds:0];
            
            self.marqueeLocationView.hidden = YES;
            self.marqueeUserStartCountdownView.hidden = NO;
            self.marqueeSuperColdTimerView.hidden = YES;
            self.marqueeSuperColdIsHereView.hidden = YES;
           
            self.currentUserAppState = MCSCUserStateWarmRestart;
            
            self.didPassColdTime = NO;
        }
            break;
            
            
        case MCSCUserStateDidJustFinalizeChillTransaction:
        {
            self.marqueeLocationView.hidden = YES;
            self.marqueeUserStartCountdownView.hidden = YES;
            self.marqueeSuperColdTimerView.hidden = NO;
            self.marqueeSuperColdIsHereView.hidden = YES;
           
            self.currentUserAppState = MCSCUserStateWarmRestart;
            
            self.didPassColdTime = NO;
        }
            break;
            
        case MCSCUserStateSuperColdIsHere:
        {
            self.marqueeLocationView.hidden = YES;
            self.marqueeUserStartCountdownView.hidden = YES;
            self.marqueeSuperColdTimerView.hidden = YES;
            self.marqueeSuperColdIsHereView.hidden = YES;
            self.marqueeSuperColdIsHereView.hidden = NO;
           
            currentUserAppState = MCSCUserStateWarmRestart;
            
            self.didPassColdTime = YES;
        }
            
            
       default:
            break;
    }
    
}


- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
        // show the age gate.  this should happen only once per clean-install of app after successful input > 21 yrs
    [self presentAgeGateIfNecessary];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}



#pragma mark - Target/Action Methods





- (IBAction)userSelectedChillLocation:(id)sender
{
        // save the location where the user will put the beer
    MCSCChillLocationType loc = ((UIButton*)sender).tag;
    [ColdCodeAppDelegate saveChillLocation:loc];
    
    
        // populate the start/cancel UI
    [self populateUserStartLabelWebView];
    [self populateSuperColdIsHereLabelWebView];


    
        // show the acquisition view controller, pose modally
    MCSCCameraViewController* cameraVC = [[MCSCCameraViewController alloc] initWithNibName:@"MCSCCameraViewController" bundle:nil];
    
    cameraVC.shouldProcess = YES;

        // flip it around to imply "out-of-band" functionality (it lives literally behind the UI)
    
    UINavigationController* navController = [[UINavigationController alloc] initWithRootViewController:cameraVC];
    [cameraVC release];
    navController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    navController.navigationBar.barStyle = UIBarStyleBlackTranslucent;

        // show it modally
    [self presentModalViewController:navController animated:YES];
    
    [navController release];
}


- (IBAction)userStartSuperColdTimer:(id)sender 
{
        // transition to timer panel
    
    	// First create a CATransition object to describe the transition
	CATransition *transition = [CATransition animation];
        // Animate over 1/2 of a second
	transition.duration = 0.5;
	/*
        // using the ease in/out timing function
	transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
	*/
	
	transition.type = kCATransitionFade;

    transition.delegate = self;
    
        // Finally, to avoid overlapping transitions we assign ourselves as the delegate for the animation and wait for the
        // -animationDidStop:finished: message. When it comes in, we will flag that we are no longer transitioning.
	
        // Next add it to the containerView's layer. This will perform the transition based on how we change its contents.
	[self.view.layer addAnimation:transition forKey:nil];
	
        // Here we hide view1, and show view2, which will cause Core Animation to animate view1 away and view2 in.
	self.marqueeUserStartCountdownView.hidden = YES;
	self.marqueeSuperColdTimerView.hidden = NO;

    self.currentUserAppState = MCSCUserStateDidJustFinalizeChillTransaction;
    
    
        //    All Three of these should be moved to a class function in the delegate, but not until we get final data from Miller Coors on curves for different containers.
    NSTimeInterval initialSeconds = 0.0;
    NSTimeInterval regularColdMilestone = 0.0;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    BOOL runInDemoMode = [defaults boolForKey:@"demoModeEnabled"];
    
    if ( runInDemoMode )
    {
        initialSeconds = 15.0;
        regularColdMilestone = -9.0;
    }
    else
    {
        BOOL success = [self.calc calculateStarting:[NSDate date]];
        
        initialSeconds = [self.calc.superColdDate timeIntervalSinceDate:self.calc.commenceDate];
        regularColdMilestone = [self.calc.coldDate timeIntervalSinceDate:self.calc.superColdDate];
        
        NSLog( @"calculating lookup times success = %@\nseconds to superCold:\t%f\ndelta to Cold:\t%f", success ? @"YES" : @"NO", initialSeconds, regularColdMilestone );

    
}
    

    
        // set Super Cold 
    NSMutableDictionary* userInfo = [NSMutableDictionary dictionaryWithCapacity:0];
    NSDate* nowPlusInitial = nil;
    NSDate* backwardToColdTime = nil;
        
    if ( runInDemoMode )
    {
        nowPlusInitial = [[NSDate date] dateByAddingTimeInterval:initialSeconds];
        backwardToColdTime = [nowPlusInitial dateByAddingTimeInterval:regularColdMilestone];
    }
    else
    {
        nowPlusInitial = [[self.calc.superColdDate copy] autorelease];
        backwardToColdTime = [[self.calc.coldDate copy] autorelease];
    }
        

    [ColdCodeAppDelegate coldCodeDelegate].absoluteSuperColdTime = nowPlusInitial;
    [ColdCodeAppDelegate coldCodeDelegate].absoluteColdTime = backwardToColdTime;
    
    [userInfo setObject:nowPlusInitial forKey:@"superColdDate"];
    
    
    NSDate* coldDate = [ColdCodeAppDelegate coldCodeDelegate].absoluteColdTime;
    [userInfo setObject:coldDate forKey:@"coldDate"];
    
    
#if ! TESTING
    if ( [ColdCodeAppDelegate chillLocation] == kFreezer )
    {
            // set sentinel to reasonable time if in freezer 
            // set a reasonable default.  
        NSTimeInterval twentyMinutes = 20 * 60;
        NSDate* twentyMinutesFromNow = [NSDate dateWithTimeIntervalSinceNow:twentyMinutes];
        
        UILocalNotification* sentinel = [[UILocalNotification alloc] init];
        sentinel.fireDate = twentyMinutesFromNow;
        sentinel.timeZone = [NSTimeZone defaultTimeZone];
        sentinel.soundName = UILocalNotificationDefaultSoundName;
        sentinel.applicationIconBadgeNumber = 1;
        sentinel.alertBody = NSLocalizedString( @"Make sure you have removed your Coors Light from the Freezer!", @"" );
        sentinel.alertAction = NSLocalizedString( @"Done", @"" );
        sentinel.userInfo = [NSDictionary dictionaryWithObject:NSLocalizedString( @"20", @"timeInMinutes" ) forKey:@"timeInMinutes"];
        
        self.sentinelFreezerNotification = sentinel;
        
        [[UIApplication sharedApplication] scheduleLocalNotification:sentinel];
    }
#endif
    
    
    
    
    
    
    self.clockLabelUpdaterTimer = [NSTimer scheduledTimerWithTimeInterval:kMCSCTimerFireTimeInterval target:self selector:@selector(updateClock:) userInfo:userInfo repeats:YES];
}


- (IBAction)userCancelSuperColdTimer:(id)sender 
{
    if ( self.sentinelFreezerNotification != nil )
    {
        [[UIApplication sharedApplication] cancelLocalNotification:self.sentinelFreezerNotification];
        
        self.sentinelFreezerNotification = nil;
    }
    
    if ( self.clockLabelUpdaterTimer != nil )
    {
        [self.clockLabelUpdaterTimer invalidate];
    }
    
    [self performWarmRestartTransition];
}


- (IBAction)userCancelChilling:(id)sender 
{
    NSLog( @"-userCancelChilling:  " );
    
    
    if ( self.sentinelFreezerNotification != nil )
    {
        [[UIApplication sharedApplication] cancelLocalNotification:self.sentinelFreezerNotification];
        
        self.sentinelFreezerNotification = nil;
    }
    
    [self performWarmRestartTransition];
}



- (IBAction)userDidStartChillAnother:(id)sender 
{
    if ( self.sentinelFreezerNotification != nil )
    {
        [[UIApplication sharedApplication] cancelLocalNotification:self.sentinelFreezerNotification];
        
        self.sentinelFreezerNotification = nil;
    }
    
    [self performWarmRestartTransition];
}









#pragma mark - Notification Methods


- (void) adjustTimerOnNewSuperColdTime:(NSNotification*)n
{
        // for convenience and conservation of namespace, using the notification name for the key, 
        // since there's only one thing in the dict
    
    NSDate* newTime = (NSDate*)[[n userInfo] objectForKey:[n name]];
    
    [self.clockLabelUpdaterTimer setFireDate:newTime];
}


- (void) aboutToReturnFromCamera:(NSNotification*)n
{
    self.currentUserAppState = MCSCUserStateDidJustAcquireColdData;
    
    self.calc = (CoorsColdCalc*)[[n userInfo] objectForKey:MCSCCoorsColdCalcKey];
        //CoorsColdCalc* calculator = [[CoorsColdCalc alloc] initWithCoorsCan:cc andLocation:[ColdCodeAppDelegate chillLocation]];
                            
        //self.calc = calculator;
    

}


- (void) configureOnBecomeActive:(NSNotification*)n
{
    switch ( self.currentUserAppState ) 
    {
        case MCSCUserStateWarmRestart:
                //[self.navigationController popToRootViewControllerAnimated:NO];
            break;
            
        default:
            break;
    }
}




#pragma mark - Timer & Countdown Display Methods


- (void) updateClock:(NSTimer*)updateClockLabelTimer
{
    NSDictionary* userInfo = [updateClockLabelTimer userInfo];
    NSDate* superColdDate = [userInfo objectForKey:@"superColdDate"];
    NSDate* coldDate = [userInfo objectForKey:@"coldDate"];
   
    
    // algorithm:  take the time delta between cold and super-cold, extrapolate backwards from regular-cold
    // by a factor of two (Jason said that the chill curve was roughly geometric) and use linear curve
    // as a run up to peg 'cold' at no higher than x alpha. 
    // After regular-cold, the run up between regular-cold and super-cold will be a linear curve between
    // x and y (where x and y are both < 1.0).
    // Why?  Cognitively, we want Cold to appear light blue so that Super Cold is punched up, and then only when
    // the timer hits 00:00 is the mountain fully blue.
    
    /*
     
     Step Function:
     t = time away from super-cold,
     divisor = ( 2 * (superCold - regularCold) ):
     
     superCold      < t < regularCold   : 0.5 - 0.8
     regularCold    < t < divisor       : 0.0 - 0.5
     divisor        < t                 : 0.0
     
     
     */
    

    const CGFloat coldAlphaMax = 0.5;
    const CGFloat superColdAlphaMax = 0.8;
    CGFloat superColdInterval = ( superColdAlphaMax - coldAlphaMax );
    
    
    NSTimeInterval deltaColdAndSuperCold = [superColdDate timeIntervalSinceDate:coldDate];
    NSTimeInterval divisor = deltaColdAndSuperCold * 2.0;

        // NSLog( @"coldDate = %@", coldDate );
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    BOOL runInDemoMode = [defaults boolForKey:@"demoModeEnabled"];

    NSTimeInterval timeUntilCold = runInDemoMode ? [coldDate timeIntervalSinceNow] 
                                                 : [coldDate timeIntervalSinceDate:self.calc.commenceDate];
    
    NSTimeInterval timeUntilSuperCold = [superColdDate timeIntervalSinceDate:[updateClockLabelTimer fireDate]];
            
    NSInteger minutes = timeUntilSuperCold / 60;
    NSInteger remainderSecs =  timeUntilSuperCold - ( minutes * 60 );
    
        // set it immediately to avoid aesthetic weirdness
    [self updateCountdownTimeMinutes:minutes seconds:remainderSecs];

    
    CGFloat blueMountainsAlpha = 0.0;
    CGFloat frostAlpha = 0.0;
    
    if ( timeUntilSuperCold <= 0 )
    {
        [updateClockLabelTimer invalidate];
        
            //NSLog( @"SUPERCOLD!");
        
        [self configureForSuperColdIsHere];
    }
    else
    {
        if ( ! self.didPassColdTime )
        {
            // heading towards regular-cold, peg blueMountainsAlpha at 0.5
            
                //NSLog( @" not yet at Regular Cold.  timeUntilCold = %f, timeUntilSuperCold = %f", timeUntilCold, timeUntilSuperCold);
            
            NSTimeInterval alphaCoefficient = floor( fabsl( timeUntilCold ) );
            NSTimeInterval dividend = ( alphaCoefficient > divisor ) ? divisor : alphaCoefficient;
            
            blueMountainsAlpha = ( ( divisor - dividend ) / divisor ) * coldAlphaMax;
            frostAlpha = 0.0;

            if ( timeUntilCold <= 0 )
            {                 
                NSLog( @"timeUntilCold = %f", timeUntilCold );
                
                NSLog( @"REGULAR COLD! update banner" );
                // cold banner
                self.coldnessBannerView.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"banner_cold" ofType:@"png"]];
                
                self.timeTilSuperColdCancelButton.enabled = YES;
                
                self.didPassColdTime = YES;
                
                    //NSLog( @"cancel Button is %@enabled%@", ( self.timeTilSuperColdCancelButton.enabled == YES ? @"" : @"NOT " ), ( self.timeTilSuperColdCancelButton.enabled == YES ? @"." : @"!!!" ) );
                
                NSLog( @"cancel Button isUserInteractionEnabled == %@", ( [self.timeTilSuperColdCancelButton isUserInteractionEnabled] ? @"YES" : @"NO" ) );


            }
            
        }
        else
        {
            //NSLog( @"DID pass cold time. timeUntilCold = %f.  should have hit regular cold.", timeUntilCold );
            
                 blueMountainsAlpha = ( ( ( deltaColdAndSuperCold - timeUntilSuperCold ) / deltaColdAndSuperCold ) * superColdInterval ) + coldAlphaMax;  // from coldAlphaMax to 1.0
            
                //frostAlpha =  ( ( ( deltaColdAndSuperCold - timeUntilSuperCold ) / deltaColdAndSuperCold ) * superColdAlphaMax ) ;  // from 0.0 to superColdAlphaMax
            
                //NSLog( @"blueMountainsAlpha = %f", blueMountainsAlpha );
                //NSLog( @"frostAlpha = %f", frostAlpha );
            
            
                //NSLog( @"cancel Button isUserInteractionEnabled == %@", ( [self.timeTilSuperColdCancelButton isUserInteractionEnabled] ? @"YES" : @"NO" ) );

        }
        
        

        [UIView animateWithDuration:kMCSCTimerBlueMountainsAnimationInterval
                         animations:^{ 
                             [self updateBlueMountains:blueMountainsAlpha];
                                 //[self updateFrost:frostAlpha];
                         }
                         completion:nil];
        
        
        [self updateCountdownTimeMinutes:minutes seconds:remainderSecs];
    }
}




#pragma mark - User State Set up Methods

- (void) handleUserAge:(NSNotification*)n
{
    self.userIsOldEnough = YES;
    
    NSDate* birthdate = [[n userInfo] objectForKey:[n name]];
    
    if ( birthdate != nil )
    {
        [ColdCodeAppDelegate user].userBirthdate = birthdate;
        
        [ColdCodeAppDelegate saveUser];
    }
    
}



- (void) presentAgeGateIfNecessary
{
    BOOL userIs21OrOlder = self.userIsOldEnough;
    
    if ( ! userIs21OrOlder )
    {
        MCSCUser* user = [ColdCodeAppDelegate user];
        
        if ( ( user != nil ) && ( user.userBirthdate != nil ) )
        {
            NSDateComponents* comps = [[NSDateComponents alloc] init];
            [comps setYear:-21];
            
            NSDate* tMinus21Years = [[NSCalendar autoupdatingCurrentCalendar] dateByAddingComponents:comps toDate:[NSDate date] options:0];
            
            [comps release];
            
            userIs21OrOlder = ([user.userBirthdate timeIntervalSinceDate:tMinus21Years] <= 0 );
            
            if ( ! userIs21OrOlder )
            {
                MCSCAgeGateModalViewController* ageGateVC = [[MCSCAgeGateModalViewController alloc] initWithNibName:@"MCSCAgeGateModalViewController" bundle:nil];
                
                
                UINavigationController* nav = [[UINavigationController alloc] initWithRootViewController:ageGateVC];
                nav.navigationBar.barStyle = UIBarStyleBlackTranslucent;
                
                [ageGateVC release];
                
                nav.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
                
                [self presentModalViewController:nav animated:YES];
                
                [nav release];
            }
        }
    }
}



- (void) updateCountdownTimeMinutes:(NSInteger)minutesRemaining seconds:(NSInteger)secondsRemaining
{
    self.minutesLabel.text = [NSString stringWithFormat:@"%02u", minutesRemaining];
    self.secondsLabel.text = [NSString stringWithFormat:@"%02u", secondsRemaining];
}


- (void) updateBlueMountains:(CGFloat)opacity
{
    if ( opacity == 0.0 )
    {
        //NSLog( @"Resetting blue mountains alpha to 0.0!" );
    }
        //NSLog( @"About to set blue Mountains alpha to %f", opacity );
    
    self.blueFillerImageView.alpha = opacity;
}


- (void) updateFrost:(CGFloat) opacity
{
    // add the frost here
}


- (void) configureForSuperColdIsHere
{
    [self updateCountdownTimeMinutes:0 seconds:0];

        // give 'er all she's got
    [self updateBlueMountains:1.0];
    [self updateFrost:1.0];
    
    self.coldnessBannerView.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"banner_superCold" ofType:@"png"]];
    

    
    [self performSuperColdIsHereTransition];
    
   // [self performSelectorOnMainThread:@selector(performSuperColdIsHereTransition) withObject:nil waitUntilDone:NO];
   // [self performSelector:@selector(performSuperColdIsHereTransition) withObject:nil afterDelay:0.5];
}


- (void) performSuperColdIsHereTransition
{
    CATransition* frostFadeIn = [CATransition animation];
    frostFadeIn.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    
    frostFadeIn.type = kCATransitionFade;
    
    frostFadeIn.duration = 3.0;
    frostFadeIn.beginTime = 0;
    frostFadeIn.delegate = self;
    
    self.frostOverlayImageView.alpha = 1.0;
    
    [self.frostOverlayImageView.layer addAnimation:frostFadeIn forKey:nil];
    
    
    
        //   [UIView animateWithDuration:3.0
        //                   animations:^{ 
                         CATransition* fadeOut = [CATransition animation];
                         // Animate over 3/4 of a second
                         //fadeOut.duration = 0.9;
                         // using the ease in/out timing function
                         fadeOut.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
                         
                         fadeOut.type = kCATransitionFade;
                         
                         fadeOut.beginTime = 0.0;
                         fadeOut.duration = 1.0;

                         fadeOut.delegate = self;
                         
                             //fadeOut.startProgress = 0.0;
                             //fadeOut.endProgress = 0.33;
                         
                         self.marqueeSuperColdTimerView.hidden = YES;

                         
                         
                         
                         CATransition* fadeIn = [CATransition animation];
                         // Animate over 3/4 of a second
                         //fadeIn.duration = 0.9;
                         // using the ease in/out timing function
                         fadeIn.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
                         
                         fadeIn.type = kCATransitionFade;
                         
                         fadeOut.beginTime = 2.0;
                         fadeOut.duration = 1.0;
                         
                         fadeIn.delegate = self;
                         
                             //fadeIn.startProgress = 0.67;
                             //fadeIn.endProgress = 1.0;
                         
                         self.marqueeSuperColdIsHereView.hidden = NO;
                         
                         [self.marqueeSuperColdTimerView.layer addAnimation:fadeOut forKey:@"fadeOut"];
                         [self.marqueeSuperColdIsHereView.layer addAnimation:fadeIn forKey:@"fadeIn"];
        //                   }
        //                   completion:nil];
    
    
    self.currentUserAppState = MCSCUserStateSuperColdIsHere;
}



- (void) performWarmRestartTransition
{
        // First create a CATransition object to describe the transition
	CATransition *transition = [CATransition animation];
        // Animate over 3/4 of a second
	transition.duration = 1.0;
        // using the ease in/out timing function
	transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
	
	transition.type = kCATransitionFade;
    
    transition.delegate = self;
    
    
    self.marqueeLocationView.hidden = NO;
    self.marqueeUserStartCountdownView.hidden = YES;
    self.marqueeSuperColdTimerView.hidden = YES;
    self.marqueeSuperColdIsHereView.hidden = YES;
    
    self.frostOverlayImageView.alpha = 0.0;
    self.coldnessBannerView.image = [UIImage imageNamed:@"banner_notCold.png"];
    self.blueFillerImageView.alpha = 0.0;

    
        // Finally, to avoid overlapping transitions we assign ourselves as the delegate for the animation and wait for the
        // -animationDidStop:finished: message. When it comes in, we will flag that we are no longer transitioning.
	
        // Next add it to the containerView's layer. This will perform the transition based on how we change its contents.
	[self.view.layer addAnimation:transition forKey:nil];
	
        // Here we hide view1, and show view2, which will cause Core Animation to animate view1 away and view2 in.
    
    self.currentUserAppState = MCSCUserStateWarmRestart;
    
    [self abortCountdownAndResetApp];
    
    /*
    if ( self.clockLabelUpdaterTimer != nil )
    {
        [self.clockLabelUpdaterTimer invalidate];
        
        self.clockLabelUpdaterTimer = nil;
    }
    
    self.didPassColdTime = NO;
    */
}



- (void) abortCountdownAndResetApp
{
    if ( self.sentinelFreezerNotification != nil )
    {
        [[UIApplication sharedApplication] cancelLocalNotification:self.sentinelFreezerNotification];
        
        self.sentinelFreezerNotification = nil;
    }
    
        // here we can safely kill all local notifications, because it will just be at most, cold, supercold and sentinel
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    
    [ColdCodeAppDelegate coldCodeDelegate].superColdLocalNotification = nil;
    [ColdCodeAppDelegate coldCodeDelegate].coldLocalNotification = nil;
    self.sentinelFreezerNotification = nil;
    
    
    if ( self.clockLabelUpdaterTimer != nil )
    {
        [self.clockLabelUpdaterTimer invalidate];
    }
    
    
    self.marqueeLocationView.hidden = NO;
    self.marqueeUserStartCountdownView.hidden = YES;
    self.marqueeSuperColdTimerView.hidden = YES;
    self.marqueeSuperColdIsHereView.hidden = YES;
    
    self.frostOverlayImageView.alpha = 0.0;
    self.coldnessBannerView.image = [UIImage imageNamed:@"banner_notCold.png"];
    self.blueFillerImageView.alpha = 0.0;

    self.didPassColdTime = NO;
}




@end
