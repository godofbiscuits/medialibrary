//
//  DLCVRawAnalysis.h
//  coors_test
//
//  Created by Eamae Mirkin on 7/16/11.
//  Copyright 2011 Deeplocal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DLCVHSBColorSpec.h"
#import <opencv2/imgproc/imgproc_c.h>


@interface DLCVRawAnalysis : NSObject {
    
}

+ (float) getPercentOfColor:(DLCVHSBColorSpec*)color inImage:(IplImage*)image insideRect:(CGRect*)rect;
+ (float) getPercentOfColorD:(DLCVHSBColorSpec*)color inImage:(IplImage*)image insideRect:(CGRect*)rect;

+ (void) getHSVFromR:(float)r 
                   G:(float)g 
                   B:(float)b
            storeAsH:(float*)h 
                   S:(float*)s 
                   V:(float*)v;


@end
