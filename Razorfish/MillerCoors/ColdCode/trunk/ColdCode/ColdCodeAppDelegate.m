//
//  ColdCodeAppDelegate.m
//  ColdCode
//
//  Created by Jeff Barbose on 6/24/11.
//  Copyright 2011 Razorfish, LLC. All rights reserved.
//

#import "ColdCodeAppDelegate.h"

#import "MCSCColdCodeRootViewController.h"
#import "MCSCTestLaunchViewController.h"

NSString* MCSCUserDidPassAgeGateNotification = @"MCSCUserDidPassAgeGateNotification";

NSString* MCSCAbsoluteSuperColdTimeChangedNotification = @"MCSCAbsoluteSuperColdTimeChangedNotification";
NSString* MCSCAbsoluteColdTimeChangedNotification = @"MCSCAbsoluteColdTimeChangedNotification";

NSString* MCSCCameraDidAcquireNewInformationNotification = @"MCSCCameraDidAcquireNewInformationNotification";

NSString* MCSCUserArchiveFileName = @"MCSCUserArchiveFileName";

NSString* MCSCColdReachedLocalNotification = @"MCSCColdReachedLocalNotification";
NSString* MCSCSuperColdReachedLocalNotification = @"MCSCSuperColdReachedLocalNotification";

NSString* MCSCAbsoluteTime = @"MCSCAbsoluteTime";


ColdCodeAppDelegate* coldCodeDelegate = nil;


@interface ColdCodeAppDelegate ()

- (void) postLocalNotificationName:(NSString*)localNotificationName fireDate:(NSDate*)when userInfo:(NSDictionary*)newUserInfo;

@end



@implementation ColdCodeAppDelegate


@synthesize window=_window;

@synthesize managedObjectContext=__managedObjectContext;

@synthesize managedObjectModel=__managedObjectModel;

@synthesize persistentStoreCoordinator=__persistentStoreCoordinator;

@synthesize navigationController=_navigationController;

@synthesize localNotificationLaunchDictionary;

@synthesize coldLocalNotification, superColdLocalNotification;


+ (ColdCodeAppDelegate*) coldCodeDelegate
{
    
    if ( coldCodeDelegate == nil )
    {
        coldCodeDelegate = (ColdCodeAppDelegate*)[[UIApplication sharedApplication] delegate];
    }
    return coldCodeDelegate;
}

+ (UIFont*) bodyFont
{
    static UIFont* bodyFont = nil;
    
    if ( bodyFont == nil )
    {
        bodyFont = [[UIFont fontWithName:@"Interstate-Regular" size:17] retain];
    }
    
    return bodyFont;
}

+ (UIFont*) buttonFont
{
    static UIFont* buttonFont = nil;
    
    if ( buttonFont == nil )
    {
        buttonFont = [[UIFont fontWithName:@"Interstate-Black" size:18] retain];
    }
    
    return buttonFont;
}


+ (UIFont*) clockFont
{
    static UIFont* clockFont = nil;
    
    if ( clockFont == nil )
    {
        clockFont = [[UIFont fontWithName:@"Interstate-Black" size:48] retain];
    }
    
    return clockFont;
}


+ (MCSCUser*) user
{
    ColdCodeAppDelegate* del = [ColdCodeAppDelegate coldCodeDelegate];
    
    return [del user];
}


+ (BOOL) saveUser
{
    ColdCodeAppDelegate* del = [ColdCodeAppDelegate coldCodeDelegate];
    MCSCUser* theUser = [del user];

    NSURL* fullPathURL = [[del applicationDocumentsDirectory] URLByAppendingPathComponent:MCSCUserArchiveFileName];
    NSString* fullPath = [fullPathURL path];
    
    theUser.lastSaveTime = [NSDate date];
    BOOL success = [NSKeyedArchiver archiveRootObject:theUser toFile:fullPath];
    
    
    if ( ! success )
    {
        NSLog( @"Failed to save user to Documents directory." );
    }
    
    
/*    
    
    NSError* err = nil;
    BOOL success = NO;
    
    if ( ( success = [[ColdCodeAppDelegate coldCodeDelegate].managedObjectContext save:&err] ) )
    {
            // handle error
    }
    */
    
    return success;
}


+ (BOOL) saveChillLocation:(MCSCChillLocationType)newChillLocation
{
    MCSCUser* user = [ColdCodeAppDelegate user];
    
    user.lastLocation = [[NSNumber numberWithInt:newChillLocation] stringValue];
    
    return [ColdCodeAppDelegate saveUser];
}


+ (MCSCChillLocationType) chillLocation
{
    MCSCUser* user = [ColdCodeAppDelegate user];
    
    MCSCChillLocationType loc = (MCSCChillLocationType)[user.lastLocation integerValue];
    
    return loc;
}


+ (NSString*) resourceNameForCurrentChillLocation
{
    return [ColdCodeAppDelegate resourceNameForChillLocation:[ColdCodeAppDelegate chillLocation]];
}


+ (BOOL) saveUserAge:(NSDate*)newUserAge
{
    MCSCUser* user = [ColdCodeAppDelegate user];
    
    user.userBirthdate = newUserAge;
    
    return [ColdCodeAppDelegate saveUser];
}


+ (NSString*) resourceNameForChillLocation:(MCSCChillLocationType)chillLocation
{
        // this is just nasty.
    switch ( chillLocation ) 
    {
        case kFreezer:
            return @"Freezer";
            break;
            
        case kFridge:
            return @"Fridge";
            break;
            
        case kCooler:
            return @"Cooler";
            break;
            
        default:
            return @"empty";
            break;
    }
}



- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    UILocalNotification* notif = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
    
    if ( notif != nil )
    {
        self.localNotificationLaunchDictionary = [notif.userInfo retain];
    }
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
    
    user = nil;
    
     /*
      
      // set a reasonable default.  
    NSTimeInterval twentyMinutes = 20 * 60;
    NSDate* twentyMinutesFromNow = [NSDate dateWithTimeIntervalSinceNow:twentyMinutes];
    
    
    
        // self.absoluteSuperColdTime = twentyMinutesFromNow;
    
    UILocalNotification* sentinel = [[UILocalNotification alloc] init];
    sentinel.fireDate = twentyMinutesFromNow;
    sentinel.timeZone = [NSTimeZone defaultTimeZone];
    sentinel.soundName = UILocalNotificationDefaultSoundName;
    sentinel.applicationIconBadgeNumber = 1;
    sentinel.alertBody = NSLocalizedString( @"", @"" );
    sentinel.alertAction = NSLocalizedString( @"", @"" );
    sentinel.userInfo = [NSDictionary dictionaryWithObject:NSLocalizedString( @"20", @"timeInMinutes" ) forKey:@"timeInMinutes"];
    
    self.sentinelLocalNotification = sentinel;
    
    [[UIApplication sharedApplication] scheduleLocalNotification:notif];
    */
    
        // Override point for customization after application launch.
        // Add the navigation controller's view to the window and display.
    self.window.rootViewController = self.navigationController;
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
     */
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    /*
     Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
     */
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
    
        //(void)[self.navigationController popToRootViewControllerAnimated:NO];
    
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
    
    (void)[self.navigationController popToRootViewControllerAnimated:NO];

}


- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    self.localNotificationLaunchDictionary = [notification.userInfo retain];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"LaunchWithLocalNotification" object:self userInfo:notification.userInfo];
    
    NSLog( @"-applicationDidReceiveLocalNotification: %@", notification.alertAction );

    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
}


- (void)dealloc
{
    [_window release];
    [__managedObjectContext release];
    [__managedObjectModel release];
    [__persistentStoreCoordinator release];
    [_navigationController release];
    
    [absoluteSuperColdTime release];
    
    [super dealloc];
}

- (void)awakeFromNib
{
#if TESTING
    user = nil;
    absoluteSuperColdTime = nil;

#else
    MCSCColdCodeRootViewController *rootViewController = (MCSCColdCodeRootViewController *)[self.navigationController topViewController];
    rootViewController.managedObjectContext = self.managedObjectContext;
#endif
    
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil)
    {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error])
        {
            /*
             Replace this implementation with code to handle the error appropriately.
             
             abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. If it is not possible to recover from the error, display an alert panel that instructs the user to quit the application by pressing the Home button.
             */
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        } 
    }
}

#pragma mark - Core Data stack

/**
 Returns the managed object context for the application.
 If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
 */
- (NSManagedObjectContext *)managedObjectContext
{
    if (__managedObjectContext != nil)
    {
        return __managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil)
    {
        __managedObjectContext = [[NSManagedObjectContext alloc] init];
        [__managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return __managedObjectContext;
}

/**
 Returns the managed object model for the application.
 If the model doesn't already exist, it is created from the application's model.
 */
- (NSManagedObjectModel *)managedObjectModel
{
    if (__managedObjectModel != nil)
    {
        return __managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"ColdCode" withExtension:@"momd"];
    __managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];    
    return __managedObjectModel;
}

/**
 Returns the persistent store coordinator for the application.
 If the coordinator doesn't already exist, it is created and the application's store added to it.
 */
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (__persistentStoreCoordinator != nil)
    {
        return __persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"ColdCode.sqlite"];
    
    NSError *error = nil;
    __persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![__persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error])
    {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. If it is not possible to recover from the error, display an alert panel that instructs the user to quit the application by pressing the Home button.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter: 
         [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption, [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }    
    
    return __persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

/**
 Returns the URL to the application's Documents directory.
 */
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}



- (MCSCUser*) user
{
    
    if ( user != nil )
    {
        return user;
    }
    else
    {
        ColdCodeAppDelegate* del = [ColdCodeAppDelegate coldCodeDelegate];
        
        NSURL* fullPathURL = [[del applicationDocumentsDirectory] URLByAppendingPathComponent:MCSCUserArchiveFileName];
        NSString* fullPath = [fullPathURL path];
        
        MCSCUser* savedUser = [NSKeyedUnarchiver unarchiveObjectWithFile:fullPath];
        
        if ( savedUser == nil )
        {
            MCSCUser* newUser = [[MCSCUser alloc] init];
            newUser.lastLocation = nil;
            newUser.userBirthdate = [NSDate date];
            newUser.lastSaveTime = [NSDate date];
            
            user = newUser;
        }
        else
        {
            user = [savedUser retain];
        }
            
         /*   
            // Create the fetch request for the entity.
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
            // Edit the entity name as appropriate.
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"MCSCUser" inManagedObjectContext:self.managedObjectContext];
        [fetchRequest setEntity:entity];
        
            // Set the batch size to a suitable number.
        [fetchRequest setFetchBatchSize:1];
        
            // Edit the sort key as appropriate.
        NSError *error = nil;
        NSArray *fetchedObjects = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
        if (fetchedObjects == nil) 
        {
                // Handle the error
        }
        else if ( fetchedObjects.count == 0 )
        {
            MCSCUser* newUser = [[MCSCUser alloc] initWithEntity:entity insertIntoManagedObjectContext:self.managedObjectContext];
            newUser.lastLocation = [[NSNumber numberWithInt:kFreezer] stringValue];
            newUser.userBirthdate = [NSDate date];
            newUser.lastSaveTime = [NSDate date];
            

            NSError* err = nil;
            
            if ( ! [self.managedObjectContext save:&err] )
            {
                    // handle the error
            }
            else
            {
                [self setUser:newUser];
            }
        }
        else
        {
            [self setUser:[fetchedObjects objectAtIndex:0]];
        }
        
        [fetchRequest release];
        
        [fetchedObjects release];
          
          */
        
        return user;
    }
}


- (void) setUser:(MCSCUser*)newUser
{
    MCSCUser* holdUser = user;
    
    user = [newUser retain];
    
    [holdUser release];
}


- (void) setAbsoluteSuperColdTime:(NSDate*) newAbsoluteTime
{
    NSDate* holdDate = absoluteSuperColdTime;
    
    absoluteSuperColdTime = [newAbsoluteTime retain];
    
    [holdDate release];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:MCSCAbsoluteSuperColdTimeChangedNotification object:self userInfo:[NSDictionary dictionaryWithObject:absoluteSuperColdTime forKey:MCSCAbsoluteTime]];
    
    [self postLocalNotificationName:MCSCSuperColdReachedLocalNotification fireDate:absoluteSuperColdTime userInfo:nil];
    
}


- (NSDate*) absoluteSuperColdTime
{
    return absoluteSuperColdTime;
}


- (void) setAbsoluteColdTime:(NSDate*) newAbsoluteTime
{
    NSDate* holdDate = absoluteColdTime;
    
    absoluteColdTime = [newAbsoluteTime retain];
    
    [holdDate release];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:MCSCAbsoluteColdTimeChangedNotification object:self userInfo:[NSDictionary dictionaryWithObject:absoluteColdTime forKey:MCSCAbsoluteTime]];
    
        //[self postLocalNotificationName:MCSCColdReachedLocalNotification fireDate:absoluteColdTime userInfo:nil];
}


- (NSDate*) absoluteColdTime
{
    return absoluteColdTime;
}



- (void) postLocalNotificationName:(NSString*)localNotificationName fireDate:(NSDate*)when userInfo:(NSDictionary*)newUserInfo
{
    UILocalNotification* notif = [[UILocalNotification alloc] init];
    
    notif.fireDate = when;
    notif.timeZone = [NSTimeZone defaultTimeZone];
    notif.soundName = UILocalNotificationDefaultSoundName;
    notif.applicationIconBadgeNumber = 1;
    
    
    NSMutableDictionary* dict = [NSMutableDictionary dictionaryWithCapacity:1];
    
    if ( newUserInfo != nil )
    {
        [dict addEntriesFromDictionary:newUserInfo];
    }
    
    if ( when == nil )
    {
        NSDate* fireTime = [dict objectForKey:MCSCAbsoluteTime];
        
        notif.fireDate = fireTime;
    }
    
#if TESTING
    [dict setObject:@"value" forKey:@"key"];
#endif
    
    notif.userInfo = dict;

    
    
    
    if ( [localNotificationName isEqualToString:MCSCColdReachedLocalNotification] )
    {
        notif.alertBody = NSLocalizedString( @"Your Coors Light is Cold!", @"Cold Local Notification Alert Body Text" );
        notif.alertAction = NSLocalizedString( @"Cold", @"Cold Local Notification Alert Action" );
        
        NSLog( @"BaseView: Cold Local Notification Generated" );
        
        if ( self.coldLocalNotification != nil )
        {
            [[UIApplication sharedApplication] cancelLocalNotification:self.coldLocalNotification];
        }

        self.coldLocalNotification = notif;
    }
    else if ( [localNotificationName isEqualToString:MCSCSuperColdReachedLocalNotification] )
    {
        notif.alertBody = NSLocalizedString( @"Your Coors Light is Super Cold!", @"Super Cold Local Notification Alert Body Text" );
        notif.alertAction = NSLocalizedString( @"Super Cold", @"Super Cold Local Notification Alert Action" );
        
        NSLog( @"BaseView: SuperCold Local Notification Generated" );
        
        if ( self.superColdLocalNotification != nil )
        {
            [[UIApplication sharedApplication] cancelLocalNotification:self.superColdLocalNotification];
        }

        self.superColdLocalNotification = notif;
        
        
            // user set a real time, so shut down the sentinel notification (drop-dead date to remove from freezer
            // [[UIApplication sharedApplication] cancelLocalNotification:self.sentinelLocalNotification];
            //self.sentinelLocalNotification = nil;
            
    }
    
    
    [[UIApplication sharedApplication] scheduleLocalNotification:notif];
    

    [notif release];
}


@end
