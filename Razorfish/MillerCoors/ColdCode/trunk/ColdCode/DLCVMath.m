//
//  DLCVMath.m
//  coors_test
//
//  Created by Eamae Mirkin on 7/13/11.
//  Copyright 2011 Deeplocal. All rights reserved.
//

#import "DLCVMath.h"


@implementation DLCVMath

+ (angle_f) circularSubtraction:(angle_f)v1 minus:(angle_f)v2 withMin:(float)minValue andMax:(float)maxValue{
    angle_f diff = v1 -v2;
	
	if(fabs(diff) > (maxValue - minValue) /2 && diff != 0) {
		diff = -fabs(diff)/diff * ((maxValue - minValue) - fabs(diff));
	}
	return diff;
}

+ (angle_f) circularSubtraction:(angle_f)v1 minus:(angle_f)v2{
    return [DLCVMath circularSubtraction:v1 minus:v2 withMin:-PI andMax:PI];
}


@end
