//
//  MCSCTestLaunchViewController.m
//  ColdCode
//
//  Created by Jeff Barbose on 6/26/11.
//  Copyright 2011 Razorfish, LLC. All rights reserved.
//

#import "MCSCTestLaunchViewController.h"
#import "ColdCodeAppDelegate.h"
#import "MCSCAgeGateModalViewController.h"
#import "MCSCBaseViewController.h"


@interface MCSCTestLaunchViewController (Notifications)

- (void) updateTextView:(NSNotification*)n;
- (void) restoreOnBecomeActive:(NSNotification*)n;

@end


@implementation MCSCTestLaunchViewController



@synthesize initialScreenButton;
@synthesize cameraButton;
@synthesize countdownScreen;
@synthesize timesUpButton;
@synthesize localNotifInTenSecButton;
@synthesize localNotificationLaunchDictionaryTextView;
@synthesize ageGateButton;

@synthesize localNotification;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [initialScreenButton release];
    [cameraButton release];
    [countdownScreen release];
    [timesUpButton release];
    [localNotifInTenSecButton release];
    
    [localNotification release];
    
    [localNotificationLaunchDictionaryTextView release];
    [ageGateButton release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CGRect frame = CGRectMake(0, 0, 180, 44);
    UILabel *label = [[[UILabel alloc] initWithFrame:frame] autorelease];
    
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:22.0];
    label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.25];
    label.textAlignment = UITextAlignmentCenter;
    
    CGFloat red = 160.0/256.0;
    CGFloat green = 19.0/256.0;
    CGFloat blue = 37.0/256.0;
    
    UIColor* coorsRed = [UIColor colorWithRed:red green:green blue:blue alpha:1.0];
    label.textColor = coorsRed;
    UINavigationItem* navItem = self.navigationItem;
    
    navItem.titleView = label;
    
    label.text = NSLocalizedString(@"Cold Code Test", @"test title only");
    [label sizeToFit];
    [label release];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateTextView:) name:@"LaunchWithLocalNotification" object:[[UIApplication sharedApplication] delegate]];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cleanupOnTerminate:) name: UIApplicationDidBecomeActiveNotification object:[[UIApplication sharedApplication] delegate]];
    
     
}


- (void) viewWillAppear:(BOOL)animated
{
    NSDictionary* launchDict = ((ColdCodeAppDelegate*)[[UIApplication sharedApplication] delegate]).localNotificationLaunchDictionary;
    
    if ( launchDict != nil )
    {
        self.localNotificationLaunchDictionaryTextView.hidden = NO;
        self.localNotificationLaunchDictionaryTextView.text = @"";
        
        NSString* key = [[launchDict allKeys] objectAtIndex:0];
        NSString* value = [[launchDict allValues] objectAtIndex:0];
        
        NSString* displayString = [NSString localizedStringWithFormat:@"Local Notification Dictionary: key='%@', value='%@'", key, value];
        
        self.localNotificationLaunchDictionaryTextView.text = displayString;
        
        launchDict = nil;
    }
    else
    {
        self.localNotificationLaunchDictionaryTextView.hidden = YES;
    }
}


- (void)viewDidUnload
{
    [self setInitialScreenButton:nil];
    [self setCameraButton:nil];
    [self setCountdownScreen:nil];
    [self setTimesUpButton:nil];
    [self setLocalNotifInTenSecButton:nil];
    [self setLocalNotificationLaunchDictionaryTextView:nil];

    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"LaunchWithLocalNotification" object:[[UIApplication sharedApplication] delegate]];
    [[NSNotificationCenter defaultCenter] removeObserver:self name: UIApplicationDidBecomeActiveNotification object:[[UIApplication sharedApplication] delegate]];
    
    [self setAgeGateButton:nil];
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark -
#pragma mark Notifications

- (IBAction)fireTestLocalNotification:(id)sender 
{
    self.localNotificationLaunchDictionaryTextView.hidden = YES;
    self.localNotificationLaunchDictionaryTextView.text = @"";
    
    if ( self.localNotification == nil )
    {
        UILocalNotification* locNot = [[UILocalNotification alloc] init];
        self.localNotification = locNot;
        [locNot release];
    }
    
    NSDate* nowPlusTenSeconds = [[NSDate date] dateByAddingTimeInterval:10];
    
    UILocalNotification* notif = self.localNotification;
    
    notif.fireDate = nowPlusTenSeconds;
    notif.timeZone = [NSTimeZone defaultTimeZone];
    notif.alertBody = NSLocalizedString( @"Super Cold!", @"" );
    notif.alertAction = NSLocalizedString( @"Drink", @"" );
    notif.soundName = UILocalNotificationDefaultSoundName;
    notif.applicationIconBadgeNumber = 1;
    
    NSDictionary* infoDict = [NSDictionary dictionaryWithObject:@"value" forKey:@"key"];
    
    notif.userInfo = infoDict;
    
    [[UIApplication sharedApplication] scheduleLocalNotification:notif];
}


#pragma mark -
#pragma mark Notifications


- (void) updateTextView:(NSNotification*)n
{
    NSDictionary* userinfo = [n userInfo];
    
    self.localNotificationLaunchDictionaryTextView.hidden = NO;
    self.localNotificationLaunchDictionaryTextView.text = @"";
    
    NSString* key = [[userinfo allKeys] objectAtIndex:0];
    NSString* value = [[userinfo allValues] objectAtIndex:0];
    
    NSString* displayString = [NSString localizedStringWithFormat:@"Local Notification Dictionary: key='%@', value='%@'", key, value];
    
    self.localNotificationLaunchDictionaryTextView.text = displayString;
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
}


- (void) restoreOnBecomeActive:(NSNotification*)n
{
    (void)[self.navigationController popToRootViewControllerAnimated:NO];
}


#pragma mark -
#pragma mark Target/Action


- (IBAction)presentAgeGateModally:(id)sender 
{

    MCSCAgeGateModalViewController* ageGateVC = [[MCSCAgeGateModalViewController alloc] initWithNibName:@"MCSCAgeGateModalViewController" bundle:nil];
    
    UINavigationController* nav = [[UINavigationController alloc] initWithRootViewController:ageGateVC];
    nav.navigationBar.barStyle = UIBarStyleBlackTranslucent;

    [ageGateVC release];
    
    nav.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    
    [self presentModalViewController:nav animated:YES];
    
    [nav release];
}


- (IBAction)pushBaseViewController:(id)sender 
{
    MCSCBaseViewController* baseViewController = [[MCSCBaseViewController alloc] initWithNibName:@"MCSCBaseViewController" bundle:nil];
    
    [self.navigationController pushViewController:baseViewController animated:YES];
    
    [baseViewController release];
}






















@end
