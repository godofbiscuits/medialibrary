//
//  MCSCCameraViewController.m
//  ColdCode
//
//  Created by Jeffrey Barbose on 03-07-11.
//  Copyright 2011 Razorfish, LLC. All rights reserved.
//

#import "MCSCCameraViewController.h"
#import "ColdCodeAppDelegate.h"

#import "CoorsCan.h"
#import "SoundEffect.h"


#ifdef DEMO_1
NSTimeInterval demoTimeInterval = 22.0;
#endif




@interface MCSCCameraViewController ()

@property (nonatomic, readwrite, retain) SoundEffect* lockSound;


- (void) cancelColdAcquisition:(id) sender;

- (void) setupAndStartAVSession;
- (void) stopAVSession;


- (void)delayedCleanup:(NSDictionary*)coldDict;

- (void)cleanupAndDismiss:(NSTimer*)timer;


@end



@implementation MCSCCameraViewController

@synthesize shouldProcess;
@synthesize captureSession;

@synthesize videoPreviewView;
@synthesize bottleOrCanToggleControl;
@synthesize cancelScanButtonItem;
@synthesize coorsCan;

@synthesize spots;
@synthesize overlay;

@synthesize lockSound;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if ( self != nil ) 
    {
        // Custom initialization
    }
    
    return self;
}


- (void)dealloc
{
    [videoPreviewView release];
    [bottleOrCanToggleControl release];
    [cancelScanButtonItem release];
    
    [super dealloc];
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}


#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //self.navigationController.navigationItem.title = NSLocalizedString( @"Aim @ Coors Light", @"" ); 
    
    UINavigationItem* navItem = self.navigationItem;
    
        //UIBarButtonItem* cancelItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemStop target:self action:@selector(cancelColdAcquisition:)];

    UIBarButtonItem* cancelItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelColdAcquisition:)];

        // UIBarButtonItem* acquireItem = [[UIBarButtonItem alloc] initWithTitle:@"Acquire" style:UIBarButtonItemStyleBordered target:self action:@selector(lockAndTemperatureAcquired:)];
    

    
    UILabel* titleView = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, 250.0, 30.0)];
        //titleView.font = [ColdCodeAppDelegate bodyFont];
    titleView.font = [UIFont fontWithName:@"Interstate-Regular" size:14.0];
    titleView.textColor = [UIColor whiteColor];
    [titleView setMinimumFontSize:4.0];
    titleView.textAlignment = UITextAlignmentRight;
    
    titleView.opaque = NO;
    titleView.backgroundColor = [UIColor clearColor];
    
    titleView.text = NSLocalizedString( @"Aim at the front of your Coors Light", @"" );
    [titleView adjustsFontSizeToFitWidth];
    
    navItem.titleView = titleView;
    
    [titleView release];
    
    navItem.leftBarButtonItem = cancelItem;
    
        //navItem.title = NSLocalizedString( @"Aim @ Coors Light", @"" );
    
    self.cancelScanButtonItem = cancelItem;
    
    [cancelItem release];
    
    
    
        // set up vision stuff
    
    coorsCan = [[CoorsCan alloc] init];
    self.coorsCan.bottleOrCan = 0;
    
    
    lockSound = [[SoundEffect alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"farSound" ofType:@"caf"]];

    
    [self setupAndStartAVSession];
}


- (void)viewDidUnload
{
    [self stopAVSession];

    [self setVideoPreviewView:nil];
    [self setBottleOrCanToggleControl:nil];
    [self setCancelScanButtonItem:nil];
    
    self.lockSound = nil;
    
    
    [super viewDidUnload];
}


- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
}

- (void) viewWillDisappear:(BOOL)animated
{
    [self stopAVSession];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}



- (IBAction)lockAndTemperatureAcquired:(id)sender 
{
        //[ColdCodeAppDelegate coldCodeDelegate].absoluteSuperColdTime = [NSDate dateWithTimeIntervalSinceNow:demoTimeInterval];
    
        [[NSNotificationCenter defaultCenter] postNotificationName:MCSCCameraDidAcquireNewInformationNotification object:self userInfo:nil];

    
    [self dismissModalViewControllerAnimated:YES];
}


- (IBAction)toggleBottleOrCan:(id)sender
{
        // we're only listening to "valueChanged", so assume that it's always different
    
    NSInteger selectedIndex = ((UISegmentedControl*)sender).selectedSegmentIndex;
    
    switch ( selectedIndex )
    {
        case 0:
            self.coorsCan.bottleOrCan = 0; // 0 = normal sized bottle, 2 = small bottle;
            break;
            
        case 1:
            self.coorsCan.bottleOrCan = 1; // 1 = normal sized can, 3 = small can;
            
        default:
            break;
    }
    
}





- (void) cancelColdAcquisition:(id)sender
{
    [self dismissModalViewControllerAnimated:YES];
}


#pragma mark - Private Methods

- (void) setupAndStartAVSession
{
        //set process to false
        //self.shouldProcess = NO;
    
        //add the output
        //output = [[UIImageView alloc] initWithFrame:arView.frame];
        //output.frame  = CGRectMake(000, 0, 150, 200);
        //[self.arView addSubview:output];
    
        // start capturing frames
        // Create the AVCapture Session
    
    if ( self.captureSession == nil )
    {
        AVCaptureSession* aSession = [[AVCaptureSession alloc] init];
        self.captureSession = aSession;
        [aSession release];
    }
    
    CGRect kPreviewLayerFrameRect = CGRectMake(0, 0, 320, 427);

	
        // create a preview layer to show the output from the camera
	AVCaptureVideoPreviewLayer *previewLayer = [AVCaptureVideoPreviewLayer layerWithSession:self.captureSession];
        //previewLayer.frame = CGRectMake(000, 0, 150, 200);
    previewLayer.frame = kPreviewLayerFrameRect;
    
    previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    
    previewLayer.opacity=1.0;
	[self.videoPreviewView.layer addSublayer:previewLayer];
	self.videoPreviewView.backgroundColor = [UIColor blueColor];
    
    
    
        // set up the user interface subviews programmatically.
        // easier to doublecheck and maintain precision of placement of overlay, etc.
   

    
        // user-targeting view
    if ( self.overlay != nil )
    {
        [self.overlay removeFromSuperview];
         self.overlay = nil;
    }
    overlay = [[UIImageView alloc] initWithImage:[UIImage imageNamed: @"CoorsCanOverlay.png"]];
    [self.overlay setFrame:kPreviewLayerFrameRect];
    [self.overlay setAlpha:0.5];
    [self.videoPreviewView addSubview:self.overlay];

    
        // feedback "ghost" image after positive lock
    if ( self.spots != nil )
    {
        [self.overlay removeFromSuperview];
        self.spots = nil;
    }
    spots = [[UIImageView alloc] initWithFrame:kPreviewLayerFrameRect];
    [self.spots setAlpha:0.5];
    [self.videoPreviewView addSubview:self.spots];
    
    
    
        // Get the default camera device
	AVCaptureDevice* camera = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
	
        // Create a AVCaptureInput with the camera device
	NSError* error = nil;
	AVCaptureInput* cameraInput = [[AVCaptureDeviceInput alloc] initWithDevice:camera error:&error];
    
	if ( cameraInput == nil ) 
    {
		NSLog( @"Error to create camera capture:%@", error );
	}
	
        // Set the output
	AVCaptureVideoDataOutput* videoOutput = [[AVCaptureVideoDataOutput alloc] init];
    
    videoOutput.alwaysDiscardsLateVideoFrames = YES;
	
        // create a queue to run the capture on
	dispatch_queue_t captureQueue=dispatch_queue_create("captureQueue", NULL);
	
        // setup our delegate
	[videoOutput setSampleBufferDelegate:self queue:captureQueue];
    
        // configure the pixel format
	videoOutput.videoSettings = [NSDictionary dictionaryWithObjectsAndKeys:
                                 [NSNumber numberWithUnsignedInt:kCVPixelFormatType_32BGRA], 
                                 (id)kCVPixelBufferPixelFormatTypeKey,
                                 nil];
    
        // and the size of the frames we want
        //	[self.captureSession setSessionPreset:AVCaptureSessionPresetMedium];
	[self.captureSession setSessionPreset:AVCaptureSessionPresetMedium];
    
        // Add the input and output
	[self.captureSession addInput:cameraInput];
	[self.captureSession addOutput:videoOutput];

        //[cameraInput release];
        //[videoOutput release];
    
        // Start the session
	[self.captureSession startRunning];	

}

- (void) stopAVSession
{
	[self.captureSession stopRunning];
        //[self.captureSession release];
	self.captureSession = nil;
    
}


#pragma mark - AVSession Delegate Methods

- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection 
{
    /*We create an autorelease pool because as we are not in the main_queue our code is
	 not executed in the main thread. So we have to create an autorelease pool for the thread we are in*/
    
        //only do this stuff if the process button is true
        // ??? - jjb -- do we still need a shouldProcess state in the final app?
    if( self.shouldProcess == YES )
    {
        NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
        

        BOOL foundContainer = [self.coorsCan sendImage:sampleBuffer];
        
            //NSLog( @"captureOutput delegate method: foundContainer = %@", foundContainer ? @"YES" : @"NO" );
        
        
        if ( foundContainer == YES )
        {
            self.shouldProcess = NO;
                //here i'm just putting the temp on the label, 0=warm, 1 = cold, 2 = suuuper cold
                //[self.notifications setText:@"Found!"];
            NSLog(@"temp: %i", self.coorsCan.temperature);
            
                //[debug performSelectorOnMainThread:@selector(setText:) withObject:[NSString stringWithFormat: @"Temp: %i", self.cc.temperature ] waitUntilDone:YES];
                //[self.notifications performSelectorOnMainThread:@selector(setText:) withObject:@"Found" waitUntilDone:YES];
            
            [self.spots performSelectorOnMainThread:@selector(setImage:) withObject:self.coorsCan.currentFrame waitUntilDone:NO];
            [self.lockSound performSelectorOnMainThread:@selector(play) withObject:nil waitUntilDone:NO];
            
            CoorsColdCalc* calc = [[CoorsColdCalc alloc] initWithCoorsCan:self.coorsCan andLocation:[ColdCodeAppDelegate chillLocation]];
            

            
            NSDictionary* coldDict = [NSDictionary dictionaryWithObjectsAndKeys:
                                      calc, MCSCCoorsColdCalcKey,
                                      [NSDate date], MCSCCoorsContainerTimeStampLockKey,
                                      [NSNumber numberWithInteger:(NSInteger)self.coorsCan.bottleOrCan], MCSCCoorsContainerBottleOrCanKey,
                                      [NSNumber numberWithInteger:(NSInteger)self.coorsCan.temperature], MCSCCoorsContainerTemperatureKey, nil];
            
                //NSLog( @"coldDict = \n%@", coldDict );
            
            [calc release];
            
            
                // (void) [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(delayedCleanup:) userInfo:coldDict repeats:NO];
            
                //            [self performSelector:@selector(cleanupAndDismiss:) withObject:coldDict afterDelay:2.0];
            [self performSelectorOnMainThread:@selector(delayedCleanup:) withObject:coldDict waitUntilDone:NO];
            
            
                //(void)[NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(cleanupAndDismiss:) userInfo:coldDict repeats:NO];
            
            NSLog( @"just after performSelectorOnMainThread:withObject:waitUntilDone:" );
        }
            //did it find a can?
            // NSLog(@"did we find a can? %@", result?@" YES":@" NO");
        
            //whats the current temp?
            // NSLog(@"current temp = %f", self.cc.temperature);
        
            //say you wanted to debug and see first how many contours you found
            // NSLog(@"contours found =  %i", self.cc.contourCount);
        
            //you can even display the current frame...
        
            //if you want to draw the debug frame
            // [self.spots performSelectorOnMainThread:@selector(setImage:) withObject:self.cc.currentFrame waitUntilDone:NO];
        
        [pool drain];
    }
    
}



#pragma mark - Misc Methods


- (void)delayedCleanup:(NSDictionary*)coldDict
{
    NSTimer* timer = [NSTimer scheduledTimerWithTimeInterval:2.5 target:self selector:@selector(cleanupAndDismiss:) userInfo:coldDict repeats:NO];
}


- (void)cleanupAndDismiss:(NSTimer*)timer
{
    NSDictionary* coldDict = [timer userInfo];
        
    [[NSNotificationCenter defaultCenter] postNotificationName:MCSCCameraDidAcquireNewInformationNotification object:self userInfo:coldDict];
    
    
    [self dismissModalViewControllerAnimated:YES];
}



@end
