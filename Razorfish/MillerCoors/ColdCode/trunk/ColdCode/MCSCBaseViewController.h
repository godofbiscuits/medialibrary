//
//  MCSCBaseViewController.h
//  ColdCode
//
//  Created by Jeffrey Barbose on 02-07-11.
//  Copyright 2011 Razorfish, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@class CoorsColdCalc;


typedef enum {
    MCSCUserStateColdStart = 1,
    MCSCUserStateWarmRestart = 2,
    MCSCUserStateDidJustAcquireColdData = 3,
    MCSCUserStateDidJustFinalizeChillTransaction = 4,
    MCSCUserStateSuperColdIsHere = 5
} MCSCUserState;


@interface MCSCBaseViewController : UIViewController <NSFetchedResultsControllerDelegate>
{
    BOOL userIsOldEnough;
    
    UIButton *freezerButton;
    UIButton *fridgeButton;
    UIButton *coolerButton;
    UILabel *chillLocationQuestionLabel;
    UIImageView *blueFillerImageView;
    UIImageView *coldnessBannerView;
    UIImageView *frostOverlayImageView;
    UIView *marqueeUserStartCountdownView;
    UIButton *startCountdownStartButton;
    UIButton *startCountdownCancelButton;
    UIWebView *startCountdownLabelWebView;
    UIView *marqueeSuperColdTimerView;
    UIWebView *timeTilSuperColdLabelWebView;
    UIButton *timeTilSuperColdCancelButton;
    UILabel *minutesLabel;
    UILabel *secondsLabel;
    UIView *marqueeLocationView;
    
    MCSCUserState currentUserAppState;
    
    NSTimer* clockLabelUpdaterTimer;
    
    BOOL didPassColdTime;
    
    UILocalNotification* sentinelFreezerNotification;
        // UILocalNotification* superColdLocalNotification;
    
    UIView *marqueeSuperColdIsHereView;
    UIButton *superColdIsHereChillAnotherButton;
    UIWebView *superColdIsHereWebView;
    UILabel *timeSeparatorLabel;
    
    CATransition* toUserSuperColdIsHere;
    CATransition* toUserStartCountdown;
    
    CoorsColdCalc* calc;
}

@property (nonatomic) BOOL userIsOldEnough;

@property (nonatomic, retain) IBOutlet UIImageView *blueFillerImageView;
@property (nonatomic, retain) IBOutlet UIImageView *coldnessBannerView;
@property (nonatomic, retain) IBOutlet UIImageView *frostOverlayImageView;

/* freezer, fridge, cooler panel */
@property (nonatomic, retain) IBOutlet UIView *marqueeLocationView;
@property (nonatomic, retain) IBOutlet UIButton *freezerButton;
@property (nonatomic, retain) IBOutlet UIButton *fridgeButton;
@property (nonatomic, retain) IBOutlet UIButton *coolerButton;
@property (nonatomic, retain) IBOutlet UILabel *chillLocationQuestionLabel;

/* user is ready to chill */
@property (nonatomic, retain) IBOutlet UIView *marqueeUserStartCountdownView;
@property (nonatomic, retain) IBOutlet UIButton *startCountdownStartButton;
@property (nonatomic, retain) IBOutlet UIButton *startCountdownCancelButton;
@property (nonatomic, retain) IBOutlet UIWebView *startCountdownLabelWebView;

/* timer is on-screen */
@property (nonatomic, retain) IBOutlet UIView *marqueeSuperColdTimerView;
@property (nonatomic, retain) IBOutlet UIWebView *timeTilSuperColdLabelWebView;
@property (nonatomic, retain) IBOutlet UIButton *timeTilSuperColdCancelButton;
@property (nonatomic, retain) IBOutlet UILabel *minutesLabel;
@property (nonatomic, retain) IBOutlet UILabel *secondsLabel;
@property (nonatomic, retain) IBOutlet UILabel *timeSeparatorLabel;


/* super cold is here */
@property (nonatomic, retain) IBOutlet UIView *marqueeSuperColdIsHereView;
@property (nonatomic, retain) IBOutlet UIButton *superColdIsHereChillAnotherButton;
@property (nonatomic, retain) IBOutlet UIWebView *superColdIsHereWebView;






@property (nonatomic, retain) NSTimer* clockLabelUpdaterTimer;

@property MCSCUserState currentUserAppState;

@property BOOL didPassColdTime;

    //@property (nonatomic, retain) UILocalNotification* coldLocalNotification;
    //@property (nonatomic, retain) UILocalNotification* superColdLocalNotification;

@property (nonatomic, retain) UILocalNotification* sentinelFreezerNotification;



@property (nonatomic, assign) CATransition* toUserSuperColdIsHere;
@property (nonatomic, assign) CATransition* toUserStartCountdown;


@property (nonatomic, retain) CoorsColdCalc* calc;


@end
