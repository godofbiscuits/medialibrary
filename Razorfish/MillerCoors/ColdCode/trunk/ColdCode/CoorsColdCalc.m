//
//  CoorsColdCalc.m
//  ColdCode
//
//  Created by Jeffrey Barbose on 20/07/2011.
//  Copyright 2011 Razorfish, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CoorsColdCalc.h"
#import "MCSCConstants.h"
#import "CoorsCan.h"


@interface CoorsColdCalc()

@property BOOL calculated;
@property BOOL isThreeInk;
@property (nonatomic, retain) NSDictionary* calcTable;


- (BOOL) validate;

- (NSTimeInterval) lookupTimerData;

@end


@implementation CoorsColdCalc

@synthesize commenceDate, coldDate, superColdDate, tempState, containerType, chillLocation;
@synthesize calculated, isThreeInk;
    //@synthesize coorsCan;
@synthesize calcTable;


- (CoorsColdCalc*) initWithCoorsCan:(CoorsCan*)cc andLocation:(MCSCChillLocationType)chillLoc
{
    if ( cc == nil )
    {
        return nil;
    }
    else
    {
        if ( ( self = [super init] ) )
        {
                //coorsCan = [cc retain];
            
            calculated = NO;
            
            isThreeInk = ( cc.numStripes > 2 );   
            tempState = cc.temperature;

            
            if ( isThreeInk )
            {
                if ( tempState > 0 )
                    --tempState;
                else
                    tempState = kMCSCContainerTemperatureWarm;
            }
            
            containerType = (MCSCContainerType)(cc.bottleOrCan);
            chillLocation = chillLoc;
            
            NSString* dataFileName = isThreeInk ? @"3InkChillData" : @"2InkChillData";
            
            NSURL* plistURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:dataFileName ofType:@"plist"]];
            calcTable = [[NSDictionary alloc] initWithContentsOfURL:plistURL];
            
            
            return self;
        }
        else
        {
            return nil;
        }
    }
}


- (void) dealloc
{
    [commenceDate release];
    [coldDate release];
    [superColdDate release];
        //[coorsCan release];
    
    [super dealloc];
}



- (NSTimeInterval) absoluteMinutesTilSuperCold
{
    NSTimeInterval wholeMinutes = 0;

    if ( ! self.calculated )
    {
        return 0;
    }
    else
    {

    }

    return wholeMinutes;
}


- (NSTimeInterval) absoluteRemainderSecondsTilSuperCold
{
    NSTimeInterval remainderSeconds = 0;
    
    
    return remainderSeconds;
}


- (NSTimeInterval) absoluteTotalTimeTilSuperColdInSeconds
{
    NSTimeInterval totalSeconds = 0;
    
    
    return totalSeconds;
}



- (BOOL) validate
{
    return ( ( self.tempState != kMCSCContainerTemperatureUnknown ) 
     && ( self.containerType != kMCSCContainerTypeUnknown ) 
     && ( self.chillLocation = kMCSCChillLocationUnknown ) 
            && ( self.commenceDate != nil ) );
}


- (BOOL) calculateStarting:(NSDate*)commenceTime
{
    if ( commenceTime != nil )
    {
        self.calculated = NO;
        
        self.commenceDate = commenceTime;
        
        
        NSTimeInterval totalSecondsTilSupercold = [self lookupTimerData];
        
        NSDate* targetSuperColdDate = [NSDate dateWithTimeInterval:totalSecondsTilSupercold sinceDate:self.commenceDate];
        
        self.superColdDate = targetSuperColdDate;
        
            // do a recalc to see when regular-cold is
        
        if ( self.tempState < kMCSCContainerTemperatureCold )
        {
            MCSCContainerTemperatureState holdState = self.tempState;
            
            self.tempState = kMCSCContainerTemperatureCold;
            NSTimeInterval secsTilCold = [self lookupTimerData];
            
            self.tempState = holdState;
            
            self.coldDate = [NSDate dateWithTimeInterval:secsTilCold sinceDate:self.commenceDate];
        }
        else
        {
            self.coldDate = self.commenceDate; 
        }
        
        self.calculated = YES;
    }
    else
    {
        NSDictionary* userInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                                  [NSNumber numberWithInteger:self.tempState], MCSCCoorsContainerTemperatureKey,
                                  [NSNumber numberWithInteger:self.containerType], MCSCCoorsContainerBottleOrCanKey,
                                  [NSNumber numberWithInteger:self.chillLocation], MCSCCoorsContainerChillLocationKey,
                                  self.commenceDate, MCSCChillCommenceDateKey,
                                  nil];
        
        NSException* e = [NSException exceptionWithName:@"ColdCalcFieldsInvalid" reason:@"Didn't fill in location, start temp state and/or container type" userInfo:userInfo];
        
        [e raise];

        
        return NO;
    }
    
    return self.calculated;
}



- (NSTimeInterval) lookupTimerData
{
    NSTimeInterval totalSeconds = 0;
    
    NSString* locationKey = nil;
    
    switch ( self.chillLocation ) 
    {
        case kFreezer:
            locationKey = @"freezer";
            break;
            
        case kFridge:
            locationKey = @"fridge";
            break;
            
        case kCooler:
            locationKey = @"cooler";
            break;
            
        default:
            break;
    }

    
    NSString* temperatureKey = nil;
    
    switch ( self.tempState ) 
    {
        case kMCSCContainerTemperatureWarm:
            temperatureKey = @"Warm";
            break;
            
        case kMCSCContainerTemperatureRoom:
            temperatureKey = @"Room";
            break;
            
        case kMCSCContainerTemperatureCold:
            temperatureKey = @"Cold";
            break;
            
        case kMCSCContainerTemperatureSuperCold:
            temperatureKey = @"SuperCold";
            break;
            
        default:
            break;
    }
    
        
    NSDictionary* locationDict = [self.calcTable objectForKey:locationKey];
    NSArray* tempsArrayForLocation = [locationDict objectForKey:temperatureKey];

    NSNumber* minutesTilSuperCold = [tempsArrayForLocation objectAtIndex:self.containerType];
    
    totalSeconds = [minutesTilSuperCold integerValue] * 60;
    
    return totalSeconds;
}






@end
