//
//  CoorsColdCalc.h
//  ColdCode
//
//  Created by Jeffrey Barbose on 20/07/2011.
//  Copyright 2011 Razorfish, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CoorsCan;

typedef enum {
    kMCSCChillLocationUnknown = 9999,
    kFreezer = 1001,
    kFridge = 1002,
    kCooler = 1003
} MCSCChillLocationType;

typedef enum {
    kMCSCContainerTypeUnknown = 9999,
    kMCSCContainerTypeNormalBottle = 0,
    kMCSCContainerTypeNormalCan = 1,
    kMCSCContainerTypeSmallBottle = 2,
    kMCSCContainerTypeSmallCan = 3
} MCSCContainerType;

typedef enum {
    kMCSCContainerTemperatureUnknown = 9999,
    kMCSCContainerTemperatureRoom = 0,
    kMCSCContainerTemperatureCold = 1,
    kMCSCContainerTemperatureSuperCold = 2,
    kMCSCContainerTemperatureWarm = 3
} MCSCContainerTemperatureState;


@interface CoorsColdCalc : NSObject
{
    NSDate* commenceDate;
    NSDate* coldDate;
    NSDate* superColdDate;
    NSInteger tempState;
    NSInteger containerType;
    NSInteger chillLocation;
        //CoorsCan* coorsCan;
    
@private
    BOOL calculated;
    BOOL isThreeInk;
    NSDictionary* calcTable;
}

@property (nonatomic, retain) NSDate* commenceDate;
@property (nonatomic, retain) NSDate* coldDate;
@property (nonatomic, retain) NSDate* superColdDate;
@property NSInteger tempState;
@property NSInteger containerType;
@property NSInteger chillLocation;
    //@property (nonatomic, retain) CoorsCan* coorsCan;



- (CoorsColdCalc*) initWithCoorsCan:(CoorsCan*)cc andLocation:(MCSCChillLocationType)chillLoc;
- (BOOL) calculateStarting:(NSDate*)commenceTime;


- (NSTimeInterval) absoluteMinutesTilSuperCold;
- (NSTimeInterval) absoluteRemainderSecondsTilSuperCold;
- (NSTimeInterval) absoluteTotalTimeTilSuperColdInSeconds;




@end
