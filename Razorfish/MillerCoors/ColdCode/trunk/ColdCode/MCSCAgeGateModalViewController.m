//
//  MCSCAgeGateModalViewController.m
//  ColdCode
//
//  Created by Jeffrey Barbose on 29-06-11.
//  Copyright 2011 Razorfish, LLC. All rights reserved.
//

#import "MCSCAgeGateModalViewController.h"
#import "MCSCWebViewModalViewController.h"
#import "ColdCodeAppDelegate.h"

NSString* MCSCMillerCoorsStartDate = @"10/09/2008";
NSString* kDateFormatString = @"%m/%d/%Y";

@interface MCSCAgeGateModalViewController ()

- (void) resetDateToMCStartDate;
- (void) resetDateTo21;
- (void) resetDateToNow;

- (NSDate*) tMinus21Years;

- (BOOL) userIsOfLegalDrinkingAge;

- (void) loadTOCWarning;


@end



@implementation MCSCAgeGateModalViewController

@synthesize datePicker;
@synthesize tocWarningWebView;
@synthesize enterYourBirthdayLabel;
@synthesize enterButtonItem;




- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) 
    {
        [self resetDateToMCStartDate];
        
        tncSheetShown = NO;
        
        self.title = NSLocalizedString( @"Age Verification", @"Age Gate Title" );
        
        UINavigationItem* navItem = self.navigationItem;
        
        self.enterButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString( @"Enter", @"enter button item title" ) style:UIBarButtonItemStyleBordered target:self action:@selector(dismissModal:)];
        
            //- (void)addTarget:(id)target action:(SEL)action forControlEvents:(UIControlEvents)controlEvents
        
        [self.datePicker addTarget:self action:@selector(pickerValueChanged:) forControlEvents:UIControlEventValueChanged];
                            
                            
        navItem.rightBarButtonItem = self.enterButtonItem;
    }
                                
    return self;
}

                                
                                
- (void)dealloc
{
    [datePicker release];
    [tocWarningWebView release];
    [enterYourBirthdayLabel release];
    [enterButtonItem release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIFont* interstateBlackFont = [UIFont fontWithName:@"Interstate-Black" size:18];
    
    enterYourBirthdayLabel.font = interstateBlackFont;
    
    [self loadTOCWarning];
    
    self.enterButtonItem.enabled = [self userIsOfLegalDrinkingAge];
    
    [self.datePicker addTarget:self action:@selector(pickerValueChanged:) forControlEvents:UIControlEventValueChanged];

    tncSheetShown = NO;

}



- (void)viewDidUnload
{
    [self setDatePicker:nil];
    [self setTocWarningWebView:nil];
    [self setEnterYourBirthdayLabel:nil];
    [self setEnterButtonItem:nil];
    
    tncSheetShown = NO;

    [self.datePicker removeTarget:self action:@selector(pickerValueChanged:) forControlEvents:UIControlEventValueChanged];

    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if ( NO == tncSheetShown )
    {
            //[self resetDateTo21];
        [self resetDateToNow];
    }
    else // if we're here because user just dismissed the TNC web sheet, don't reset the date picker, just reset the flag
    {
        tncSheetShown = NO;
    }
}


- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if ( NO == tncSheetShown )
    {
        //[self resetDateToMCStartDate];
    }
    else // if we're here because user just dismissed the TNC web sheet, don't reset the date picker, but do NOT reset the flag yet, do that in DID appear
    {
            //tncSheetShown = NO;
    }
}


- (void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
}


- (void) viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
}



- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}



- (IBAction)dismissModal:(id)sender 
{
    if ( [self userIsOfLegalDrinkingAge] )
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:MCSCUserDidPassAgeGateNotification object:self userInfo:[NSDictionary dictionaryWithObject:self.datePicker.date forKey:MCSCUserDidPassAgeGateNotification]];
        
        [self dismissModalViewControllerAnimated:YES];
    }
    
}



#pragma - Class Continuance Methods

- (void) resetDateToMCStartDate
{
        // nifty Easter Egg
        // start with MillerCoors inauguration date, then roll tumblers to 21 years ago today
    
    NSCalendar* calendar = [NSCalendar autoupdatingCurrentCalendar];
    
    NSDateComponents* comps = [[NSDateComponents alloc] init];
    [comps setMonth:10];
    [comps setDay:9];
    [comps setYear:2008];
    
    NSDate* mcStartDate = [calendar dateFromComponents:comps];
    
    [self.datePicker setDate:mcStartDate animated:YES];
    self.enterButtonItem.enabled = [self userIsOfLegalDrinkingAge];
    
    [comps release];
}


- (void) resetDateTo21
{
        // wind the date to 21 years ago today:  your birthday for minimum drinking age
    
    [self.datePicker setDate:[self tMinus21Years] animated:YES];
    self.enterButtonItem.enabled = [self userIsOfLegalDrinkingAge];
}


- (void) resetDateToNow
{
    [self.datePicker setDate:[NSDate date] animated:YES];
    self.enterButtonItem.enabled = NO;
}


- (NSDate*) tMinus21Years
{
    NSCalendar* calendar = [NSCalendar autoupdatingCurrentCalendar];
    
    NSDateComponents* comps = [[NSDateComponents alloc] init];
    [comps setYear:-21];

    NSDate* tMinus21Years = [calendar dateByAddingComponents:comps toDate:[NSDate date] options:0];
    
    [comps release];
    
    return tMinus21Years;
}


- (BOOL) userIsOfLegalDrinkingAge
{
    NSDate* tMinus21Years = [self tMinus21Years];
    
    NSDate* userEnteredDate = self.datePicker.date;
    
    
    NSTimeInterval diff = [userEnteredDate timeIntervalSinceDate:tMinus21Years];
    
    return ( diff <= 0 );  // is the user-entered date before the cutoff for 21yo?
}


#pragma mark - WebViewDelegate Methods


- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if ( ( webView == self.tocWarningWebView ) && [[request.URL.path lastPathComponent] isEqualToString:@"TermsAndConditions.html"] )
    {
        MCSCWebViewModalViewController* webSheet = [[MCSCWebViewModalViewController alloc] initWithNibName:@"MCSCWebViewModalViewController" bundle:nil];
        
    
        webSheet.webDelegate = self;
        webSheet.filePath = @"TermsAndConditions.html";
        
        [self.navigationController presentModalViewController:webSheet animated:YES];
        
        tncSheetShown = YES;

        
        [webSheet release];
        
        return NO;
    }
    else
    {
        return YES;
    }
}





#pragma mark - WebView Others

- (void) loadTOCWarning;
{
    NSError* err = nil;
    
    NSString* htmlString = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"TOCWarning" ofType:@"html"] encoding:NSUTF8StringEncoding error:&err];
    
    if ( err == nil )
    {
        [self.tocWarningWebView loadHTMLString:htmlString baseURL:nil];
    }
    
    self.tocWarningWebView.opaque = NO;
    self.tocWarningWebView.backgroundColor = [UIColor clearColor];
    
}


#pragma mark - Target/Action

- (IBAction)pickerValueChanged:(id)sender
{
        // Enable/disable Enter Button.
    
    BOOL shouldEnable = [self userIsOfLegalDrinkingAge];
    
    self.enterButtonItem.enabled = shouldEnable;
}




@end
