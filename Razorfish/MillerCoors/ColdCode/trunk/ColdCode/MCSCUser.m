//
//  MCSCUser.m
//  ColdCode
//
//  Created by Jeff Barbose on 6/24/11.
//  Copyright 2011 Razorfish, LLC. All rights reserved.
//

#import "MCSCUser.h"


@implementation MCSCUser

@synthesize lastLocation;
@synthesize lastSaveTime;
@synthesize userBirthdate;

- (void)encodeWithCoder:(NSCoder *)coder 
{
    [coder encodeObject:self.lastLocation forKey:@"lastLocation"];
    [coder encodeObject:self.lastSaveTime forKey:@"lastSaveTime"];
    [coder encodeObject:self.userBirthdate forKey:@"userBirthdate"];
}


- (id)initWithCoder:(NSCoder *)coder 
{
	if ( ( self = [super init] ) != nil )
    {
		self.lastLocation = [coder decodeObjectForKey:@"lastLocation"];
		self.lastSaveTime = [coder decodeObjectForKey:@"lastSaveTime"];
		self.userBirthdate = [coder decodeObjectForKey:@"userBirthdate"];
	}
    
	return self;
}





@end
