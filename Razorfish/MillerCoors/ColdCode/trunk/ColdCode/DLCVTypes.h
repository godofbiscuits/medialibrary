/*
 *  DLCVTypes.h
 *  emptyExample
 *
 *  Created by Eamae Mirkin on 6/24/11.
 *  Copyright 2011 Deeplocal, Inc. All rights reserved.
 *
 */
#import <opencv2/imgproc/imgproc_c.h>
#import <opencv2/objdetect/objdetect.hpp>


#ifndef _DLCV_TYPES
#define _DLCV_TYPES

//#define globalShapeCornerDistanceBucketSize 0.2f
//#define globalDefaultShapeMatchingThreshold 0.85f
//basic geometry
typedef float angle_f;


/*
typedef enum{
	DLCVShapeFeature_Convex,
	DLCVShapeFeature_Concave,
	DLCVShapeFeature_Straight
} DLCVShapeFeatureType;
*/

/*

static void DLCVMakeShapeCornerPrototype(DLCVShapeCorner* srcCorner, 
                                         DLCVShapeCornerPrototype* destPrototype, 
                                         DLCVShapePrototypeToleranceSpec* tolerance){
    
}
 */

/*
typedef union {
	struct  {
		float hue;
		float saturation;
		float brightness;
		float hueTolerance;
		float saturationTolerance;
		float brightnessTolerance;
	};
	float data[6];
}DLCVHSBColorSpec;
*/
#endif