//
//  NSString+URLEncoding.m
//  Medallia
//
//  Created by Jeff Barbose on 3/12/10.

#import "NSString+URLEncoding.h"


@implementation NSString (URLEncoding)

- (NSString*) urlencode {
    CFStringRef encodedCFString = CFURLCreateStringByAddingPercentEscapes(NULL,
                                                                          (CFStringRef)self,
                                                                          NULL,
                                                                          (CFStringRef)@"!*'\"();:@&=+$,/?%#[]% ",
                                                                          kCFStringEncodingUTF8);
    NSString* encodedString = [NSString stringWithString:(NSString*) encodedCFString];
    CFRelease(encodedCFString);
    
    return encodedString;
}
- (NSString*) urldecode {
    return [self stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
}

@end
