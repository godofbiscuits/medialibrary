//
//  CallLogViewController.m
//  Medallia
//
//  Created by Jeff Barbose on 3/8/10.
//  Copyright 2010 Medallia, Inc. All rights reserved.
//

#import "CallLogViewController.h"
#import "UISwitch-Extended.h"
#import "NSString+URLEncoding.h"
#import "ResponsesEntryImpl.h"
#import "ResponsesEntryHolder.h"
#import "ResponseDetails.h"
#import "ResponseList.h"

@interface CallLogViewController ()

- (void) layoutForAlertActionKind;

@end


@interface CallLogViewController ( Notifications )

- (void) keyboardWasShown:(NSNotification*)n;
- (void) keyboardWasHidden:(NSNotification*)n;

@end



@implementation CallLogViewController

@synthesize scrollView, customerNameLabel, nameTitleLabel, callInitatedDateLabel, callInitatedDateTitleLabel;
@synthesize alertSwitch, alertSwitchLabel, alertSwitchEnclosingView, comments, falseCover, spinner;
@synthesize completionController, responsesEntryHolder, info;

- (id) init {
	return [super initWithNibName:@"CallLogViewController" bundle:nil];
}


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad 
{
    [super viewDidLoad];
        
    UINavigationItem* navItem = self.navigationItem;
    
    navItem.title = info.navTitle;
    
    UIBarButtonItem* cancelItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancel:)];
    
    [self layoutForAlertActionKind];
    
    navItem.leftBarButtonItem = cancelItem;
    
    [cancelItem release];
    
        // customer name
    self.customerNameLabel.text = info.customerName;
	self.nameTitleLabel.text = info.nameLabel;
	
    self.callInitatedDateLabel.text = [info statusOrCallInitiated];
	self.callInitatedDateTitleLabel.text = info.statusLabel;

    //[self.comments becomeFirstResponder];
    
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:nil];
	//[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasHidden:) name:UIKeyboardDidHideNotification object:nil];
}



- (void)viewDidUnload 
{
	[[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidHideNotification object:nil];
    
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc 
{
	[[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidHideNotification object:nil];
	[info release];
    
    [super dealloc];
}






- (void) layoutForAlertActionKind
{
    BOOL isAlertContext = ( info.alert_action_name != nil );
    
    UIBarButtonItem* saveItem = nil;
    
    if ( isAlertContext )
    {
        // customize the switch
        UISwitch* yesNoSwitch = [UISwitch switchWithLeftText:NSLocalizedString( @"YES", @"" ) 
                                                    andRight:NSLocalizedString( @"NO", @"" )];
        
        
        CGRect switchFrame = self.alertSwitch.frame;
        
        [self.alertSwitch removeFromSuperview];
        
        self.alertSwitch = yesNoSwitch;
        [self.alertSwitchEnclosingView addSubview:self.alertSwitch];
        
        self.alertSwitch.frame = switchFrame;
        
        // need to re-wire target/action since it's a new object
        [self.alertSwitch addTarget:self action:@selector(alertSwitched:) forControlEvents:UIControlEventValueChanged];
        
        NSString* localizedFormatString = NSLocalizedString( @"%@?", @"" );
        NSString* alertLabel = [NSString stringWithFormat:localizedFormatString, [info.alert_action_name capitalizedString]];
        self.alertSwitchLabel.text = alertLabel;
        //[self.alertSwitchLabel setNeedsDisplay];
        
        [self.alertSwitch setOn:NO animated:NO];
  
        NSString* saveItemTitle = ( self.alertSwitch.isOn ? [info.alert_action_name capitalizedString] : NSLocalizedString( @"Save", @"" ) );
        
        saveItem = [[UIBarButtonItem alloc] initWithTitle:saveItemTitle style:UIBarButtonItemStyleDone target:self action:@selector(saveLog:)];
    }
    else
    {
        CGRect switchFrame = self.alertSwitchEnclosingView.frame;
        CGRect commentsFrame = self.comments.frame;
        
            // no alert, so remove the switch
        [self.alertSwitchEnclosingView removeFromSuperview];
        
            // resize the comments frame up
        CGRect newCommentsFrame = CGRectMake( commentsFrame.origin.x, ( commentsFrame.origin.y - switchFrame.size.height ), commentsFrame.size.width, ( commentsFrame.size.height + switchFrame.size.height ) );
        
        self.comments.frame = newCommentsFrame;
        self.falseCover.frame = newCommentsFrame;

        saveItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString( @"Save", @"" ) style:UIBarButtonItemStyleDone target:self action:@selector(saveLog:)];
    }
    
    
    self.navigationItem.rightBarButtonItem = saveItem;
    
    [saveItem release];
}





- (IBAction) removeFalseCover:(id)sender;
{
	if ( self.falseCover.superview != nil )
	{
		[self.falseCover removeFromSuperview];
        [self.comments becomeFirstResponder];
	}
}

- (IBAction) saveLog:(id)sender
{
	URLBuilder * url = [URLBuilder createBuilder:info.detailsURL];
	[url appendKey:@"call_initiated" withValue:[info.callInitiated description]];
	[url appendKey:@"note_content" withValue:self.comments.text];
	if (self.alertSwitch.isOn) {
		[url appendKey:@"alert_action" withValue:info.alert_action_id];
	}
    [[[SimpleRequest alloc] initStartingRequest:[NSURLRequest requestWithURL:[url getURL]] for:self dropOld:YES] autorelease];
	[spinner startAnimating];
}

- (IBAction) cancel:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}



- (IBAction) alertSwitched:(id)sender
{
    NSString* saveTitle;
	if (self.alertSwitch.isOn && info.alert_action_name != nil) {
		saveTitle = [info.alert_action_name capitalizedString];
	} else {
		saveTitle = NSLocalizedString( @"Save", @"" );
	}
        
    UIBarButtonItem* saveOrResolveItem = [[UIBarButtonItem alloc] initWithTitle:saveTitle style:UIBarButtonItemStyleDone target:self action:@selector(saveLog:)];
    
    self.navigationItem.rightBarButtonItem = saveOrResolveItem;
    
    [saveOrResolveItem release];
}




#pragma mark -
#pragma mark === UITextView Delegate Methods ===
#pragma mark -

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
   // [self.comments scrollRangeToVisible:NSMakeRange(self.comments.text.length, 0)];
    return YES;
}


#pragma mark -
#pragma mark === SRDelegate Methods ===
#pragma mark -


- (void)requestSucceeded:(NSData*)data
{
	SingleResponse * response = [SingleResponse fromPlist:[data parsePlist]];
    
	if (self.completionController) {
			// Build a single Response Entry
		ResponsesEntryImpl* rei = [[ResponsesEntryImpl alloc] init];
		rei.fullResponse = response;
		rei.detailsURL = info.detailsURL;
		rei.name = info.customerName;
		
		// now a list to put it into, and put it into it
		ResponsesEntryList* responsesList = [[[ResponsesEntryList alloc] init] autorelease];
		[responsesList.responses addObject:rei];
		
		[self.completionController performSelector:@selector(showCallLogResponseDetails:) withObject:responsesList afterDelay:0.5];
		
		[rei release];
	}
	
	if (self.responsesEntryHolder) {
		[self.responsesEntryHolder get].fullResponse = response;
	}

	[self.navigationController popViewControllerAnimated:YES];
}



- (void)requestFailed:(NSError*)err 
{
	[spinner stopAnimating];
	UIAlertView *alert= [[UIAlertView alloc]
                         initWithTitle:NSLocalizedString( @"Error", @"" )
                         message:NSLocalizedString( @"Sorry, note entry could not be saved.", @"" )
                         delegate:nil
                         cancelButtonTitle:NSLocalizedString( @"OK", @"" )
                         otherButtonTitles:nil];
	
	[alert show];
	[alert autorelease];
}


@end
