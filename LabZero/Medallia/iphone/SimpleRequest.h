#import <UIKit/UIKit.h>
#import "Gen.h"

#import "NSString+URLEncoding.h"

/*
 NSURLConnection gives the client the data in chunks. This simplifies the interface to deliver the whole chunk at once.
 This will obviously run out of memory if the server decides to give you a gig of data. Don't.
 
 If the dropOld flag is set, any previous requests for the same delegate will be ignored.
*/

@protocol SRDelegate
- (void)requestSucceeded:(NSData*)data;
- (void)requestFailed:(NSError*)err;
@end

@interface SimpleRequest : NSObject{	
	ivars_SimpleRequest;
	SEL good, bad;
	BOOL dropOld;
}
@property(retain) id<SRDelegate> delegate;
@property(retain) NSMutableData *buffer;
- (id)initStartingRequest:(NSURLRequest*)req for:(id<SRDelegate>)d dropOld:(BOOL)u;
- (void)abort;
@end
