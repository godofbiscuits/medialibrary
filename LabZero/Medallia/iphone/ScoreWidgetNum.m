//
//  ScoreWidgetNum.m
//  Medallia
//
//  Created by Jeff Barbose on 2/28/10.
//  Copyright 2010 Medallia, Inc. All rights reserved.
//

#import "ScoreWidgetNum.h"



@implementation ScoreWidgetNum


@synthesize backgroundImage;
@synthesize differenceL;
@synthesize titleL;
@synthesize numberL;
@synthesize checkboxImageView;



- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) 
    {
        // Initialization code
    }
    return self;
}

- (void) prepareForReuse
{
    titleL.text = @"";
    numberL.text = @"0.0";
    
    self.backgroundView.opaque = YES;
    self.backgroundColor = [UIColor whiteColor];
    self.accessoryType = UITableViewCellAccessoryNone;
}


- (NSString*) description
{
    // convenience for NSLog and console output
    return  [NSString stringWithFormat:@" fields:\r\ndifferenceL:%@\r\ntitleL:%@\r\nnumberL:%@\r\nbackgroundImage:%@\r\n\r\n", self.differenceL.text, self.titleL.text, self.numberL.text, self.backgroundImage];
}












- (void)dealloc 
{
    [backgroundImage release];
    [differenceL release];
    [titleL release];
    [numberL release];
    
    
    [super dealloc];
}


@end
