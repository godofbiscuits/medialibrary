//
//  NSString+URLEncoding.h
//  Medallia
//
//  Created by Jeff Barbose on 3/12/10.
//  Copyright 2010 Medallia, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSString (URLEncoding)

- (NSString*) urlencode;
- (NSString*) urldecode;

@end
