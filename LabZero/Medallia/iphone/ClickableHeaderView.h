//
//  ClickableHeader.h
//  Medallia
//
//  Created by Jeff Barbose on 3/16/10.
//  Copyright 2010 Medallia, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataFormat.h"
#import "AltDelegateConnector.h"

@protocol ClickableHeaderDelegate <SavesWidget>
- (UINavigationController*)getNavController;
- (WidgetInstance*)getWidgetInstance;
- (void)upperRightCornerPressedAction;
@end

@interface /*NOGENERATE*/ ClickableHeaderView : UIView 
{
	id<ClickableHeaderDelegate> delegate;
	
	// Used on Dashboard, Ranker, and Responses
    UIButton* unitScopeButton;
    UILabel* unitScopeLabel;
    
	// Used on Dashboard, Ranker, and Responses
    UIButton* customerSegmentsButton;
    UILabel* customerSegmentsLabel;
    
	// Used on Dashboard, Ranker, and Responses
    UIButton* timePeriodButton;
    UILabel* timePeriodLabel;
    
	// Shown on Dashboard and Ranker
    UILabel* deltaLabel;
    
    UIButton* customAreaButton;
    UIView* customAreaView;
    //UILabel* filteredResponsesLabel;
	
	// Only used on the Ranker
	UIButton* scoreSelectorButton;
    
    UIImageView* backgroundImageView;
    UIImage* backgroundImage;
    
    UIImageView* topSideGrooveView;
    UIImageView* leftSideGrooveView;
	UIImageView* bottomSideGrooveView;
	UIImageView* rightSideGrooveView;
    
    
    // view controller specific visual elements
    
    UIView* haveCommentsElement;
    UIView* smileyElement;
    
    UILabel* rankerQuestionLabel;
    
    
    UIImage* haveCommentsToggleOFFImage;
    UIImage* haveCommentsToggleONImage;
    
}

@property (nonatomic, retain) id<ClickableHeaderDelegate> delegate;

@property (nonatomic, retain) IBOutlet UIButton* unitScopeButton;
@property (nonatomic, retain) IBOutlet UILabel* unitScopeLabel;

@property (nonatomic, retain) IBOutlet UIButton* customerSegmentsButton;
@property (nonatomic, retain) IBOutlet UILabel* customerSegmentsLabel;

@property (nonatomic, retain) IBOutlet UIButton* timePeriodButton;
@property (nonatomic, retain) IBOutlet UILabel* timePeriodLabel;

@property (nonatomic, retain) IBOutlet UILabel* deltaLabel;

@property (nonatomic, retain) IBOutlet UIButton* customAreaButton;
@property (nonatomic, retain) IBOutlet UIView* customAreaView;

@property (nonatomic, retain) IBOutlet UIImageView* backgroundImageView;
@property (nonatomic, assign, setter=setBackgroundImage:) IBOutlet UIImage* backgroundImage;

@property (nonatomic, retain) IBOutlet UIImageView* topSideGrooveView;
@property (nonatomic, retain) IBOutlet UIImageView* leftSideGrooveView;
@property (nonatomic, retain) IBOutlet UIImageView* bottomSideGrooveView;
@property (nonatomic, retain) IBOutlet UIImageView* rightSideGrooveView;


// view controller specific visual elements

@property (nonatomic, retain) IBOutlet UIView* haveCommentsElement;
@property (nonatomic, retain) IBOutlet UIView* smileyElement;

@property (nonatomic, retain) IBOutlet UILabel* rankerQuestionLabel;


- (ConfigPart*)findConfigPartFor:(ClickableHeaderElement)headerElement within:(WidgetType*)widgetType;
- (void)launchConfigOf:(ClickableHeaderElement)headerElement;

- (IBAction)unitScopeButtonPressedAction:(id)sender;
- (IBAction)customerSegmentsButtonPressedAction:(id)sender;
- (IBAction)timePeriodButtonPressedAction:(id)sender;
- (IBAction)upperRightCornerPressedAction:(id)sender;

- (void)adjustForResponsesView;

-(void)setButtonLabels;
-(void)setBenchmarkTitle:(NSString*)title;

- (void) finalizeLayout;

- (void) setHaveCommentsButtonToOnState:(BOOL)yesOrNo;


@end
