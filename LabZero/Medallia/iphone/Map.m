
#import "Map.h"

@implementation NSDictionary (Map)
+ (NSDictionary*)dictionaryFromArray:(NSArray*)arr keySelector:(SEL)sel {
	NSMutableDictionary *md = [NSMutableDictionary dictionaryWithCapacity:[arr count]];
	for (id x in arr) {
		[md setObject:x forKey:[x performSelector:sel]];
	}
	return md;
}
@end
@implementation NSArray (Map)
+ (NSArray*)mapArray:(NSArray*)array usingSelector:(SEL)sel {
	NSMutableArray *m = [NSMutableArray arrayWithCapacity:[array count]];
	for (id x in array) 
		[m addObject: [x performSelector:sel]];
	return m;
}
+ (NSArray*)mapArray:(NSArray*)array usingSelector:(SEL)sel ofReceiver:(id)rec {
	NSMutableArray *m = [NSMutableArray arrayWithCapacity:[array count]];
	for (id x in array) 
		[m addObject: [rec performSelector:sel withObject:x]];
	return m;
}
@end
