#import <UIKit/UIKit.h>
#import "Gen.h"
#import "DataFormat.h"

// The sliding widget used at the top of the scoreboard for selecting the time period

@protocol CTBDelegate
- (void)collapsingTabBarSelectedIndex:(int)i;
- (void)collapsingTabBarCommittedIndex:(int)i;
@end

@interface CollapsedTabBar : NSObject {
	IBOutlet UIView *rolloutView;
	IBOutlet UILabel *currentValueLabel;
	IBOutlet UIImageView *currentValueBackground;
	IBOutlet id<CTBDelegate> delegate;
	ivars_CollapsedTabBar;
	
	CGRect fullFrame;
	BOOL open, changed;
	float mainWidth;
	int selected;
}
@property(copy) NSString *text;
@property(retain) NSString_List *tabNames;
@property(retain) UIButton_List *buttons;
- (void)setSelectedIndex:(int)i;
- (IBAction)mainButtonClicked;
- (IBAction)clicked:(UIButton*)choice;
@end
