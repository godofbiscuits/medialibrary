//
//  ScoreWidgetNum.h
//  Medallia
//
//  Created by Jeff Barbose on 2/28/10.
//  Copyright 2010 Medallia, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface /*NOGENERATE*/ ScoreWidgetNum : UITableViewCell 
{
    UILabel* differenceL;
    UILabel* titleL;
    UILabel* numberL;
	UIImageView *backgroundImage;
    UIImageView* checkboxImageView;
}

@property (nonatomic, retain) IBOutlet UILabel* differenceL;
@property (nonatomic, retain) IBOutlet UILabel* titleL;
@property (nonatomic, retain) IBOutlet UILabel* numberL;
@property (nonatomic, retain) IBOutlet UIImageView *backgroundImage;
@property (nonatomic, retain) IBOutlet UIImageView* checkboxImageView;


@end
