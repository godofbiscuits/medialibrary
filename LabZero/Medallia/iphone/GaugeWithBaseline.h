#import "Gen.h"


#import <UIKit/UIKit.h>

// basically something like a UIProgressView, but with an additional 'baseline' readout

@interface GaugeWithBaseline : UIView {
  ivars_GaugeWithBaseline;
}
@property float baseline;
@property float value;
@end
