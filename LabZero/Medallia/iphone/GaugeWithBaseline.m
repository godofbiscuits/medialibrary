
#import "GaugeWithBaseline.h"


@implementation GaugeWithBaseline
synth_GaugeWithBaseline

- (void)drawRect:(CGRect)paintRect {
	CGRect rect = [self bounds];
	float height = 7; // total height of the bar itself
	float edge = 7; // pixels for left/right edge
	float yOffset = 2; // pixels to nudge the marker; grrrrr
	UIImage *blank =  [[UIImage imageNamed:@"db-meter-empty.png"] stretchableImageWithLeftCapWidth:4 topCapHeight:3];
	UIImage *filled = [[UIImage imageNamed:@"db-meter-full.png"]  stretchableImageWithLeftCapWidth:4 topCapHeight:3];
	CGRect barRect = CGRectInset(rect, 0, (rect.size.height-height)/2);
	float barWidth = barRect.size.width - edge*2;
	[blank drawInRect:CGRectMake(barRect.origin.x + edge,
								 barRect.origin.y,
								 barRect.size.width - edge,
								 barRect.size.height)];
	[filled drawInRect:CGRectMake(barRect.origin.x,
								  barRect.origin.y,
								  barWidth*self.value + edge*2,
								  barRect.size.height)];
	if (self.baseline >= 0) {
		UIImage *marker = [UIImage imageNamed:@"new-bar-marker.png"];	
		[marker drawAtPoint:CGPointMake(barRect.origin.x + edge + barWidth*self.baseline - marker.size.width/2,
										barRect.origin.y + barRect.size.height/2 - marker.size.height/2 + yOffset)];
	}

}

@end
