//
//  ClickableHeader.m
//  Medallia
//
//  Created by Jeff Barbose on 3/16/10.
//  Copyright 2010 Medallia, Inc. All rights reserved.
//

#import "ClickableHeaderView.h"
#import "Metadata.h"


#define HAVE_COMMENTS_BACKGROUND_IMAGE_VIEW_TAG 77777




@implementation ClickableHeaderView

@synthesize delegate;
@synthesize unitScopeButton, unitScopeLabel;
@synthesize customerSegmentsButton, customerSegmentsLabel;
@synthesize timePeriodButton, timePeriodLabel;
@synthesize deltaLabel;
@synthesize customAreaButton, customAreaView;
@synthesize backgroundImage, backgroundImageView;
@synthesize topSideGrooveView, leftSideGrooveView, bottomSideGrooveView, rightSideGrooveView;

// view controller specific visual elements

@synthesize haveCommentsElement;
@synthesize smileyElement;
@synthesize rankerQuestionLabel;



- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame])
    {
        haveCommentsToggleOFFImage = nil;
        haveCommentsToggleONImage = nil;
    }
    return self;
}


- (void) dealloc
{
    [delegate release];
    
    [unitScopeLabel release];
    [unitScopeButton release];
    
    [customerSegmentsButton release];
    [customerSegmentsLabel release];
    
    [timePeriodLabel release];
    [timePeriodButton release];
    
    [deltaLabel release];
    
    [customAreaButton release];
    [customAreaView release];
    
    [scoreSelectorButton release];
    
    [backgroundImageView release];
    backgroundImage = nil;
    
    [topSideGrooveView release];
    [leftSideGrooveView release];
    [bottomSideGrooveView release];
    [rightSideGrooveView release];
    
    [haveCommentsElement release];
    [smileyElement release];
    
    [rankerQuestionLabel release];
    
    [haveCommentsToggleOFFImage release];
    [haveCommentsToggleONImage release];
    
    
    [super dealloc];
}




#pragma mark -
#pragma mark === property implementations ===
#pragma mark -

- (void) setBackgroundImage:(UIImage*)newBackgroundImage
{
    backgroundImage = newBackgroundImage;
    
    self.backgroundImageView.image = self.backgroundImage;
}


#pragma mark -



-(ConfigPart*)findConfigPartFor:(ClickableHeaderElement)headerElement within:(WidgetType*)widgetType {
	for(ConfigSection *section in widgetType.configSections) {
		for (ConfigPart *part in section.parts) {
			if (part.headerElement == headerElement) {
				return part;
			}
		}
	}
	return nil;
}

-(NSString*)findHeaderValueFor:(ClickableHeaderElement)headerElement {
	WidgetInstance* instance = [delegate getWidgetInstance];
	ConfigPart* part = [self findConfigPartFor:headerElement within:instance.widgetType];
	NSString* key = [instance.arguments objectForKey:part.key];
	AltTable* table = [[Metadata current].data altTableForKey:part.table];
	Alt* alt = [table findAltFor:key];
	NSString* value;
	if (alt == nil) {
		value = @"";
	} else {
		value = alt.headerValue;
		if (!value) value = alt.value;
	}
	return value;
}

-(void)setButtonLabels {
	unitScopeLabel.text = [self findHeaderValueFor:CHUnitScope];
	customerSegmentsLabel.text = [self findHeaderValueFor:CHCustomerSegment];
	timePeriodLabel.text = [self findHeaderValueFor:CHTimePeriod];
	rankerQuestionLabel.text = [self findHeaderValueFor:CHScoreSelector];
}

-(void)setBenchmarkTitle:(NSString*)title {
	if ([title length]) {
		if ([title isEqualToString:@"xx"]) {
			title = @"";
		}
		title = [@"∆" stringByAppendingString:title];
	}
	self.deltaLabel.text = title;
}

-(UIViewController*)viewControllerForConfigOf:(ClickableHeaderElement)headerElement forInstance:(WidgetInstance*)instance withTarget:(id<SavesWidget>)aTarget {
	ConfigPart* part = [self findConfigPartFor:headerElement within:instance.widgetType];
	if (part == nil) {
		NSLog(@"Did not find a ConfigPart for headerElement %d in %@", headerElement, [instance.widgetType toPlist]);
		return nil;
	}
	AltDelegateConnector* connector = [[AltDelegateConnector alloc] initForInstance:instance withTarget:aTarget];
	AltTableDropdown* drop = [[AltTableDropdown alloc] initWithConfig:part delegate:connector];
	drop.oldSelection = [instance.arguments objectForKey:part.key];
	NSLog(@"Creating AltTableDropdown with part=%@ oldSelection=%@", [part toPlist], drop.oldSelection);
	return drop;
}

- (void)launchConfigOf:(ClickableHeaderElement)headerElement {
	NSLog(@"launchConfigOf:headerElement invoked with headerElement %d", headerElement);
	UINavigationController* navController = [delegate getNavController];
	if (navController == nil) {
		NSLog(@"Got nil navController from delegate");
	} else {
		NSLog(@"Got navController: %@", navController);
	}
	UIViewController* configController = [self viewControllerForConfigOf:headerElement forInstance:[delegate getWidgetInstance] withTarget:delegate];
	[navController pushViewController:configController animated:YES];
}

- (IBAction)unitScopeButtonPressedAction:(id)sender {
	[self launchConfigOf:CHUnitScope];
}

- (IBAction)customerSegmentsButtonPressedAction:(id)sender {
	[self launchConfigOf:CHCustomerSegment];
}

- (IBAction)timePeriodButtonPressedAction:(id)sender {
	[self launchConfigOf:CHTimePeriod];
}

- (IBAction)upperRightCornerPressedAction:(id)sender 
{
	[self.delegate upperRightCornerPressedAction];
}




- (void) finalizeLayout
{
        // bottom side groove
    {
        CGRect deltaLabelFrame = self.deltaLabel.frame;
        CGFloat bottomGrooveXOrigin = deltaLabelFrame.origin.x - 1.0;
        
        CGRect bottomGrooveFrame = self.bottomSideGrooveView.frame;
        CGFloat bottomGrooveYOrigin = self.frame.size.height - bottomGrooveFrame.size.height; //- 1.0;
        
        CGRect newBottomGrooveFrame = CGRectMake( bottomGrooveXOrigin, bottomGrooveYOrigin, bottomGrooveFrame.size.width, bottomGrooveFrame.size.height);
        
        self.bottomSideGrooveView.frame = newBottomGrooveFrame;
    }

    
    
        // top side groove -- for now, it's just going to be vertically aligned with the bottom groove
    {
        CGRect bottomGrooveFrame = self.bottomSideGrooveView.frame;
        CGRect topGrooveFrame = self.topSideGrooveView.frame;
        CGFloat topGrooveXOrigin = bottomGrooveFrame.origin.x;
        
        CGRect newTopGrooveFrame = CGRectMake( topGrooveXOrigin, 0.0, topGrooveFrame.size.width, topGrooveFrame.size.height);
        
        self.topSideGrooveView.frame = newTopGrooveFrame;
    }

    
    
        // left side groove
    {
        CGRect unitScopeFrame = self.unitScopeLabel.frame;
        CGFloat leftGrooveYOrigin = unitScopeFrame.origin.y + unitScopeFrame.size.height + 1.0;
        
        CGRect leftGrooveFrame = self.leftSideGrooveView.frame;
        CGRect newLeftGrooveFrame = CGRectMake( 0.0, leftGrooveYOrigin, leftGrooveFrame.size.width, leftGrooveFrame.size.height );
        
        self.leftSideGrooveView.frame = newLeftGrooveFrame;
    }
    
    
    
        // right side groove -- instead of aligning with top of the time period area, alight with the left groove
    {
        CGRect leftGrooveFrame = self.leftSideGrooveView.frame;
        CGRect rightGrooveFrame = self.rightSideGrooveView.frame;
        
        CGFloat rightGrooveYOrigin = leftGrooveFrame.origin.y;
        CGFloat rightGrooveXOrigin = ( self.frame.size.width - rightGrooveFrame.size.width + 0.0 );
        
        CGRect newRightGrooveFrame = CGRectMake( rightGrooveXOrigin, rightGrooveYOrigin, rightGrooveFrame.size.width, rightGrooveFrame.size.height );
        
        self.rightSideGrooveView.frame = newRightGrooveFrame;
    }

    
        
        // set up stretchable images for "HAVE COMMENTS" button
    UIImageView* haveCommentsBackgroundImageView = (UIImageView*)[self.haveCommentsElement viewWithTag:HAVE_COMMENTS_BACKGROUND_IMAGE_VIEW_TAG];
    
    
    UIImage* imageOFF = [UIImage imageNamed:@"clickable_header_button_off.png"];
    UIImage* imageON = [UIImage imageNamed:@"clickable_header_button_on.png"];
    
    haveCommentsToggleOFFImage = [[imageOFF stretchableImageWithLeftCapWidth:2 topCapHeight:2] retain];
    haveCommentsToggleONImage = [[imageON stretchableImageWithLeftCapWidth:2 topCapHeight:2] retain];
    
    haveCommentsBackgroundImageView.image = haveCommentsToggleOFFImage;
    
    
    [self setNeedsDisplay];
}




- (void) setHaveCommentsButtonToOnState:(BOOL)yesOrNo
{
	UIImageView* haveCommentsBackgroundImageView = (UIImageView*)[self.haveCommentsElement viewWithTag:HAVE_COMMENTS_BACKGROUND_IMAGE_VIEW_TAG];
	haveCommentsBackgroundImageView.image = yesOrNo ? haveCommentsToggleONImage : haveCommentsToggleOFFImage ;
}

- (void)adjustForResponsesView {
	CGRect frame = self.timePeriodLabel.frame;
	frame.size.height = 29;
	self.timePeriodLabel.frame = frame;
}


@end
