#import <UIKit/UIKit.h>
#import "DataFormat.h"
#import "Scrolly.h"
#import "Widget.h"

@protocol TransitionHelper
- (void)setViewC:(UIViewController*)vc transition:(UIViewAnimationTransition)transition;
@end

@interface /*NOGENERATE*/ Dashboard : UIViewController <ScrollyDelegate, WidgetOwner, SavesWidget, UIScrollViewDelegate> {
	WidgetInstance_MList *instances;
	WidgetType_List *widgetTypes_;
	NSString *prefsKey_;
	BOOL viewRotatable;
	
	IBOutlet Scrolly *scrollView;
	IBOutlet UIPageControl *pager;
	id<TransitionHelper> transitionHelper; // cycle
}

@property(retain) WidgetInstance_MList *instances;
@property(retain) WidgetType_List *widgetTypes;
@property(copy) NSString *prefsKey;
@property(nonatomic) BOOL viewRotatable;

@property(retain) IBOutlet Scrolly *scrollView;
@property(retain) IBOutlet UIPageControl *pager;
@property(assign) id<TransitionHelper> transitionHelper; // cycle

- (id)initWithDefault:(WidgetInstance_MList*)defList widgetTypes:(WidgetType_List*)widgetTypes prefsKey:(NSString*)prefsKey rotatable:(BOOL)rotatable;
- (IBAction)pagerChanged;

- (void)noteWidgetsChanged;

@end
