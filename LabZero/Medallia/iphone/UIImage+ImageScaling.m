//
//  UIImage+ImageScaling.m
//  
//
//  Created by Jeff Barbose on 26-11-08.
//  Copyright 2009 Jeffrey J Barbose, Inc. All rights reserved.
//

#import "UIImage+ImageScaling.h"
#import "math.h"

#pragma mark - Utility Function Declarations -

CGImageRef ScaledCGImageFromCGImage( CGImageRef image, CGFloat scale );


@implementation UIImage (DNAImageScaling)

#pragma mark - Scaling By Factor -

+ (UIImage*) imageWithCGImage:(CGImageRef)cgImage scaledBy:(CGFloat)scalar
{
	CGImageRef scaledImage = ScaledCGImageFromCGImage( cgImage, scalar );
	UIImage* image = [[[UIImage alloc] initWithCGImage:scaledImage] autorelease];

	return image;
}

+ (UIImage*) imageNamed:(NSString*)imageName scaledBy:(CGFloat)scalar
{
	CGImageRef cgImage = [[UIImage imageNamed:imageName] CGImage];
	return [UIImage imageWithCGImage:cgImage scaledBy:scalar];
}


#pragma mark - Scaling to Fixed Number -

+ (UIImage*) cgImage:(CGImageRef)cgImage sizedTo:(CGFloat)newImageHeight
{
	NSInteger currentHeight = CGImageGetHeight( cgImage );
	CGFloat ratio = newImageHeight / currentHeight;
	
	return [UIImage imageWithCGImage:cgImage scaledBy:ratio];
}



+ (UIImage*) imageNamed:(NSString*)imageName sizedTo:(CGFloat)newImageHeight
{
	CGImageRef cgImage = [[UIImage imageNamed:imageName] CGImage];
	
	NSInteger currentHeight = CGImageGetHeight( cgImage );
	CGFloat ratio = newImageHeight / currentHeight;
	
	return [UIImage imageWithCGImage:cgImage scaledBy:ratio];
}

+ (UIImage*) imageWithData:(NSData*)imageData sizedToMax:(CGFloat)maxImageSize
{
	CGImageRef cgImage = [[UIImage imageWithData:imageData] CGImage];

	NSInteger currentHeight = CGImageGetHeight( cgImage );
	NSInteger currentWidth = CGImageGetWidth( cgImage );
	CGFloat ratio = maxImageSize / MAX(currentHeight, currentWidth);

	return [UIImage imageWithCGImage:cgImage scaledBy:ratio];
}


+ (UIImage*) rotatedImageForExternalUseFromImage:(UIImage*)image
{
	//int kMaxResolution = 320; // Or whatever

	CGImageRef imgRef = image.CGImage;
	
	
	CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
	
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    
	/*
	if (width > kMaxResolution || height > kMaxResolution) {
        CGFloat ratio = width/height;
        if (ratio > 1) {
            bounds.size.width = kMaxResolution;
            bounds.size.height = bounds.size.width / ratio;
        }
        else {
            bounds.size.height = kMaxResolution;
            bounds.size.width = bounds.size.height * ratio;
        }
    }
	*/
    CGFloat scaleRatio = bounds.size.width / width;
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGFloat boundHeight;

	
	
	
	
    UIImageOrientation orientation = image.imageOrientation;
	
    switch ( orientation ) {
			
        case UIImageOrientationUp: //EXIF = 1
		{
            transform = CGAffineTransformIdentity;
            break;
		}
			
        case UIImageOrientationUpMirrored: //EXIF = 2
		{
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
		}
			
        case UIImageOrientationDown: //EXIF = 3
		{
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
		}
			
        case UIImageOrientationDownMirrored: //EXIF = 4
		{
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
		}
			
        case UIImageOrientationLeftMirrored: //EXIF = 5
		{
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
		}
			
        case UIImageOrientationLeft: //EXIF = 6
		{
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
		}
			
        case UIImageOrientationRightMirrored: //EXIF = 7
		{
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
		}
			
        case UIImageOrientationRight: //EXIF = 8
		{
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
		}
			
        default:
		{
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
			break;
		}
			
    }
	
    UIGraphicsBeginImageContext(bounds.size);
	
    CGContextRef context = UIGraphicsGetCurrentContext();
	
    if (orientation == UIImageOrientationRight || orientation == UIImageOrientationLeft) {
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    }
    else {
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }
	
    CGContextConcatCTM(context, transform);
	
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
	
    return imageCopy;
}



@end



CGImageRef ScaledCGImageFromCGImage( CGImageRef image, CGFloat scale ) 
{	
	// Create the bitmap context
	CGContextRef	context = NULL;
	void*			bitmapData;
	NSInteger		bitmapByteCount;
	NSInteger		bitmapBytesPerRow;
	
	// Get image width, height  We'll use the entire image 
	NSInteger width = CGImageGetWidth( image ) * scale;
	NSInteger height = CGImageGetHeight( image ) * scale;
	
	// Declare the number of bytes per row  Each pixel in the bitmap in this
	// example is represented by 4 bytes; 8 bits each of red, green, blue, and
	// alpha 
	bitmapBytesPerRow = (width * 4);
	bitmapByteCount = (bitmapBytesPerRow * height);
	
	// Allocate memory for image data  This is the destination in memory
	// where any drawing to the bitmap context will be rendered 
	bitmapData = malloc( bitmapByteCount );
	
	if ( bitmapData == NULL )
	{
		return nil;
	}
	
	// Create the bitmap context. We want pre-multiplied ARGB, 8-bits
	// per component. Regardless of what the source image format is
	// (CMYK, Grayscale, and so on) it will be converted over to the format
	// specified here by CGBitmapContextCreate.
	CGColorSpaceRef colorSpace = CGImageGetColorSpace( image );
	context = CGBitmapContextCreate ( bitmapData, width, height, 8, bitmapBytesPerRow, colorSpace, kCGImageAlphaNoneSkipFirst );
	//CGColorSpaceRelease( colorSpace );
	
	if ( context == NULL )
		// error creating context
		return nil;
	
	// Draw the image to the bitmap context. Once we draw, the memory
	// allocated for the context for rendering will then contain the
	// raw image data in the specified color space.
	CGContextDrawImage( context, CGRectMake( 0, 0, width, height ), image );
	
	CGImageRef imgRef = CGBitmapContextCreateImage( context );
	CGContextRelease( context );
	free( bitmapData );
	
	return imgRef;
}

