#import "Widget.h"
#import "WidgetConfig.h"
#import "UIViewControllerShared.h"
#import "SimpleRequest.h"

@implementation Widget
synth_Widget;

- (void)editAction {
	[self.owner widgetNeedsEdit:self];
}

- (void)viewDidLoad {
	[super viewDidLoad];
//	self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"main_app_bg.png"]];
}

- (WidgetInstance*)getWidgetInstance {
	return self.settings;
}

- (UINavigationController*)getNavController {
	return [self.owner getNavController];
}

- (void)widgetSaved:(WidgetInstance*)widget position:(int)i {
	self.settings = widget;
	[self.owner saveWidgets];
	UINavigationController * sharedController = [self getNavController];
	[sharedController popToRootViewControllerAnimated:YES];
	[self startLoadingData];
}

- (void)startLoadingData {
	NSURL *url = [self.settings makeURL];
    NSURLRequest *req = [NSURLRequest requestWithURL:url];
    [self.spinner startAnimating];
    [[[SimpleRequest alloc] initStartingRequest:req for:(id<SRDelegate>)self dropOld:YES] autorelease];
}


@end
