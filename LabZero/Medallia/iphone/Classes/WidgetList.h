#import "Gen.h"
#import <UIKit/UIKit.h>
#import "NibTableCell.h"
#import "DataFormat.h"
#import "SimpleDropdown.h"
#import "WidgetConfig.h"

// This tableView shows the list of widgets, and allows the user to reorder, delete and add.

// Very straightforward.

@interface WidgetList : UITableViewController <DropDelegate, SavesWidget> {
	ivars_WidgetList;
}
@property(retain) WidgetType_List *widgetTypes;
@property(retain) WidgetInstance_MList *instances;
@property(retain) id delegate;
- (id)initWithList:(WidgetInstance_MList*)list widgetTypes:(WidgetType_List *)widgetTypes;
@end


@interface WidgetListCell : NibTableCell {
	ivars_WidgetListCell;
}
@property(retain) IBOutlet UILabel *titleL;  // "Scoreboard"
@property(retain) IBOutlet UILabel *detailsL;  // "All properties; only with comments"
- (id)takeDataFrom:(WidgetInstance*)inst;
@end
