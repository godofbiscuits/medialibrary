#import "Gen.h"
#import <Foundation/Foundation.h>
#import "DataFormat.h"

/* This used to be have more logic in it but is now just a global variable holding an instance of ConfigFile.
 *
 * See DataFormat.h for the definitions of the actual format
 */

@interface Metadata : NSObject {
  ivars_Metadata;
}
@property(retain) ConfigFile *data;

+ (Metadata*)current;
- (AltTable*)altTable:(NSString*)s;
@end
