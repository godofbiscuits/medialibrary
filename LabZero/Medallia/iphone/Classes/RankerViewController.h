//
//  RankerViewController.h
//  Medallia
//
//  Created by Max Bennedich on 3/4/10.
//  Copyright 2010 Medallia, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RankerTableViewCell.h"
#import "DataFormat.h"
#import "Gen.h"
#import "WidgetConfig.h"
#import "SimpleRequest.h"
#import "Widget.h"
#import "WidgetList.h"
#import "ClickableHeaderView.h"

@class RankerData;
@class ClickableHeaderView;

typedef enum {
	SCORE_FIELD,
	DIFF_FIELD,
	NR_SORT_FIELDS,
} SortField;

@interface /*NOGENERATE*/ RankerViewController : Widget <UITableViewDelegate, UITableViewDataSource, SRDelegate, ClickableHeaderDelegate> {
	UITableView *table;
	
	RankerTableViewCell *tempCell;
	
	RankerData *rankerData;
	
	RankerUnit_MList *shownUnits;
	NSMutableArray *indices; // index for each shown unit -- same size as shownUnits -- stored separately to not mess up RankerUnit which is used in the server
	WidgetInstance_MList *instances;
	WidgetInstance *settings;

	SortField sortField;
	BOOL *sortAscending;
	BOOL showMyUnits;
    
    ClickableHeaderView* clickableTableHeaderView;

}

@property(nonatomic,retain) IBOutlet UITableView *table;
@property(nonatomic,assign) IBOutlet RankerTableViewCell *tempCell;
@property(nonatomic,retain) RankerData *rankerData;

@property(nonatomic,retain) RankerUnit_MList *shownUnits;
@property(nonatomic,retain) NSMutableArray *indices;
@property(nonatomic,retain) WidgetInstance_MList *instances;
@property(retain) WidgetInstance *settings;
@property(nonatomic) SortField sortField;
@property(nonatomic) BOOL *sortAscending;
@property(nonatomic) BOOL showMyUnits;

@property(nonatomic, retain, getter=clickableHeaderView) ClickableHeaderView* clickableTableHeaderView;

- (void)buildInitialView;
- (void)goToPage:(int)i;
- (void)buildPage: (int)idx;
- (void)updateTable;
- (IBAction)toggleScoreSort:(id)sender;
- (IBAction)toggleDiffSort:(id)sender;
- (IBAction)toggleMyUnits:(id)sender;
@end
