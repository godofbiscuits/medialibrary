//
//  CaseManagementInfo.h
//  Medallia
//
//  Created by Øyvind Grotmol on 23.03.10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface /*NOGENERATE*/ CaseManagementInfo : NSObject {
	NSString * surveyid;
	NSDate * callInitiated;
	NSString * customerName;
	NSString * alertStatus;
	NSString * detailsURL;
	NSString * alert_action_id;
	NSString * alert_action_name;
}

@property (retain) NSDate * callInitiated;
@property (retain) NSString * customerName;
@property (retain) NSString * alertStatus;
@property (retain) NSString * detailsURL;

/** The value to set as the 'alert_action' request parameter when making the log note API call,
 if the user opted to close/resolve the alert. Example values: "closed" or "resolved". */
@property (retain) NSString * alert_action_id;

/** The value to show to the user for prompting to "Close alert" or "Resolve alert".
 Note that this value can be nil, in which case the user should not be prompted. */
@property (retain) NSString * alert_action_name;

-(NSString*)navTitle;
-(NSString*)nameLabel;
-(NSString*)statusLabel;

-(NSString*)statusOrCallInitiated;

@end
