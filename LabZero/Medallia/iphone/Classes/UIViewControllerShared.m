
#import "UIViewControllerShared.h"


@implementation UIViewController (Shared)

- (UIViewController*)sharedController {
	if(self.tabBarController)
		return self.tabBarController;
	return self;
}
- (UINavigationController*)sharedNavController {
	return self.sharedController.navigationController;
}

@end
