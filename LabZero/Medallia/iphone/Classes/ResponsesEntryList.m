//
//  ResponsesList.m
//  Medallia
//
//  Created by Øyvind Grotmol on 09.03.10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "ResponsesEntryList.h"


@implementation ResponsesEntryList

@synthesize responses;

- (id) init
{
	if (self = [super init]) {
		self.responses = [NSMutableArray array];
	}
	return self;
}

- (int) count
{
	return self.responses.count;
}

- (id<ResponsesEntryProtocol>)responseAt:(int)position
{
	return [self.responses objectAtIndex:position];
}

- (void) updateResponseAtIndex:(int)position withResponse:(ResponsesEntry*)response {
	[self.responses replaceObjectAtIndex:position withObject:response];
}


- (void)addLump:(ResponsesLump*)lump {
	[self.responses addObjectsFromArray:lump.entries];
}

@end
