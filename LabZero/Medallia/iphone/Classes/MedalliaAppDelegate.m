#import "RankerViewController.h"
#import "MedalliaAppDelegate.h"
#import "WidgetConfig.h"
#import "Metadata.h"
#import "LoginController.h"
#import "Dummy.h"
#import "SharableTabBarController.h"
#import "DataFormat.h"
#import "SimpleRequest.h"
#import "PhoneCallInfo.h"
#import "CallLogViewController.h"

@interface MedalliaAppDelegate ()

- (void) handleReturnFromPhoneCall;

@end



@implementation MedalliaAppDelegate
synth_MedalliaAppDelegate;


+ (MedalliaAppDelegate*) sharedAppDelegate
{
    return (MedalliaAppDelegate*)( [UIApplication sharedApplication].delegate );
}




void fail(NSString*a, NSString* b) {
	[[[[UIAlertView alloc] initWithTitle:a
								 message:b
								delegate:nil
					   cancelButtonTitle:@"Abort"
					   otherButtonTitles:nil] autorelease] show];
}

- (void)applicationDidFinishLaunching:(UIApplication *)application {
	// in the Settings bundle there is a switch to reset the app, deleting any customizations made
	// hopefully we'll never need it
	NSUserDefaults *defs = [NSUserDefaults standardUserDefaults];
	if ([defs boolForKey:@"M.reset_now"]) {
		for (NSString *x in [defs dictionaryRepresentation]) {
			if ([x hasPrefix:@"M."]) {	
				NSLog(@"Removing %@", x);
				[defs removeObjectForKey:x];
			}
		}
	}
    
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleBlackOpaque;
    
	
       // 
	self.loginCtlr = [[[LoginController alloc] init] autorelease];
	self.loginCtlr.delegate = self;
	[self setViewC:self.loginCtlr transition:UIViewAnimationTransitionNone];
    [self.window makeKeyAndVisible];
	[self.loginCtlr tryLogin];
}

- (void)setViewC:(UIViewController*)vc transition:(UIViewAnimationTransition)transition {
	// when helping out for the dashboard:
	if (!vc) vc = self.navCtlr;
	
	// the flip transition needs a black background (for that floating in space look)
	self.window.backgroundColor = [UIColor blackColor];
	
	[vc viewWillAppear:YES];
	[UIView beginAnimations:@"Foo" context:nil];
	[UIView commitAnimations];
	if (self.current) {
		[self.current viewWillDisappear:YES];
		[UIView beginAnimations:@"Foo" context:nil];
		[UIView setAnimationDuration:0.8];
		[UIView setAnimationTransition:transition
							   forView:self.window cache:NO];
		[self.current.view removeFromSuperview];
	}
	[self.window addSubview:vc.view];
	if (self.current) {
		[self.current viewDidDisappear:YES];
		[UIView commitAnimations];
	}
	[vc viewDidAppear:YES];
	
	self.current = vc;
}
- (void)askLogout {
	UIActionSheet *av = [[UIActionSheet alloc] initWithTitle:nil
													delegate:self 
										   cancelButtonTitle:@"Cancel" 
									  destructiveButtonTitle:nil
										   otherButtonTitles:@"Log out", nil];
	av.actionSheetStyle = UIActionSheetStyleBlackOpaque;
	[av showInView:self.navCtlr.view];
	[av release];
}
- (void)doLogout {
	[self.loginCtlr clearPassword];
	[self setViewC:self.loginCtlr transition:UIViewAnimationTransitionCurlDown];
}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
	if (buttonIndex == 0) [self doLogout];
}
- (void)readyForLogin:(NSString**)arguments {
	NSString *root = [[NSUserDefaults standardUserDefaults] stringForKey:@"root_url"];
	
	if (!root || !root.length) root = @"http://dev4.medallia.com:5009/";
	NSURL *u = [NSURL URLWithString:[NSString stringWithFormat:@"%@?req=login&c=%@&u=%@&p=%@&version=%d",
									 root,
									 [arguments[0] urlencode],
									 [arguments[1] urlencode],
									 [arguments[2] urlencode],
									 CURRENT_CLIENT_VERSION]];
	
	[self.loginCtlr setWorking];
	[[[SimpleRequest alloc] initStartingRequest:[NSURLRequest requestWithURL:u] for:self dropOld:YES] autorelease];
}
- (void) requestFailed:(NSError*)error {
	[self.loginCtlr setActive];
	fail(@"Login failed", [error description]);
}

- (void) requestSucceeded:(NSData*)data {
	[self.loginCtlr setPassive];
	id obj = [data parsePlist];
	if ([obj isKindOfClass:[NSString class]]) {
		fail(@"Login failed", obj);
		[self.loginCtlr setActive];
		return;
	}
	[Metadata current].data = [ConfigFile fromPlist:obj];
	
	Dashboard *tab1 = [[[Dashboard alloc] initWithDefault:[[[Metadata current].data.initialScoresWidgets mutableCopy] autorelease]
                                              widgetTypes:[[[Metadata current].data.scoresWidgetTypes mutableCopy] autorelease]
                                                 prefsKey:@"dashboardwidgets"
                                                rotatable:YES] autorelease];
	tab1.tabBarItem.title = @"Dashboard";
	tab1.tabBarItem.image = [UIImage imageNamed:@"icon-tab-scorecards.png"];
	tab1.transitionHelper = self;
	
	Dashboard *tab2 = [[[Dashboard alloc] initWithDefault:[[[Metadata current].data.initialRankerWidgets mutableCopy] autorelease]
                                              widgetTypes:[[[Metadata current].data.rankerWidgetTypes mutableCopy] autorelease]
                                                 prefsKey:@"rankerwidgets"
                                                rotatable:NO] autorelease];
	tab2.tabBarItem.title = @"Ranker";
	tab2.tabBarItem.image = [UIImage imageNamed:@"icon-tab-ranker.png"];
	tab2.transitionHelper = self;
	
	ResponseList *tab3 = [[[ResponseList alloc] initWithDefaultSettings:[Metadata current].data.initialResponseFilter] autorelease];
	tab3.tabBarItem.title = @"Alerts";
	tab3.tabBarItem.image = [UIImage imageNamed:@"icon-tab-alerts.png"];
	tab3.tabBarItem.badgeValue = [Metadata current].data.nrOpenAlerts;
	
	ResponseList *tab4 = [[[ResponseList alloc] initWithDefaultSettings:[Metadata current].data.initialResponseFilter alertList:tab3] autorelease];
	tab4.tabBarItem.title = @"Responses";
	tab4.tabBarItem.image = [UIImage imageNamed:@"icon-tab-responses.png"];
	self.responsesTab = tab4;
	
	self.tabCtlr = [[[SharableTabBarController alloc] init] autorelease];	
	self.tabCtlr.navigationItem.leftBarButtonItem = 
	[[[UIBarButtonItem alloc] initWithTitle:@"Log out" style:UIBarButtonItemStyleBordered target:self action:@selector(askLogout)] autorelease];

	self.tabCtlr.viewControllers = [NSArray arrayWithObjects:tab1,tab2,tab3,tab4,nil];
	self.navCtlr = [[[UINavigationController alloc] initWithRootViewController:self.tabCtlr] autorelease];
	self.navCtlr.navigationBar.barStyle = UIBarStyleBlackOpaque;

	self.navCtlr.delegate = self;
	[self setViewC:self.navCtlr transition:UIViewAnimationTransitionCurlUp];
	
    
    
    
    
	//[main performSelector:@selector(updateNavItems:) withObject:self afterDelay:0.5];
	
	[self performSelector:@selector(handleReturnFromPhoneCall) withObject:nil afterDelay:0.3];
}



- (void) handleReturnFromPhoneCall
{
   	PhoneCallInfo* callInfo = [PhoneCallInfo load];
	NSLog( @"didMakeCall: %@", callInfo.didCall ? @"YES" : @"NO" );
	if (callInfo.didCall)
	{
		[PhoneCallInfo didLogCall];

        self.tabCtlr.selectedViewController = self.responsesTab;
        [self.tabCtlr updateNavItems:YES];
        
        //[self.tabCtlr.selectedViewController captureCallLogDetails];
        [self.responsesTab performSelector:@selector(captureCallLogDetails) withObject:nil afterDelay:1.0];
        
	}
    else
    {
        [self.tabCtlr updateNavItems:YES];
    }
}








@end
