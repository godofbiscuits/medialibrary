//
//  AltDelegateConnector.h
//  Medallia
//
//  Created by Øyvind Grotmol on 18.03.10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AltTableDropdown.h"
#import "WidgetConfig.h"

@interface AltDelegateConnector : NSObject <AltDelegate, Configurator> {
	WidgetInstance * instance;
	id<SavesWidget> target;
	int targetPos;
}

@property(retain) WidgetInstance *instance;
@property(retain) id<SavesWidget> target;
@property int targetPos;

- (id)initForInstance:(WidgetInstance*)inst withTarget:(id<SavesWidget>)aTarget;

- (void)alternativeWasSelected:(Alt*)a forDropdown:(AltTableDropdown*)dropdown;

@end
