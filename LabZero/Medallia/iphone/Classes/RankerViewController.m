//
//  RankerViewController.m
//  Medallia
//
//  Created by Max Bennedich on 3/4/10.
//  Copyright 2010 Medallia, Inc. All rights reserved.
//

#import "RankerViewController.h"
#import "Metadata.h"
#import "DataFormat.h"
#import "UIViewControllerShared.h"
#import "ClickableHeaderView.h"


@interface RankerViewController ()

- (ClickableHeaderView*) clickableHeaderView;

@end



@implementation RankerViewController

@synthesize table;
@synthesize tempCell;
@synthesize rankerData;
@synthesize shownUnits;
@synthesize indices;
@synthesize instances;
@synthesize settings;
@synthesize sortField;
@synthesize sortAscending;
@synthesize showMyUnits;

@synthesize clickableTableHeaderView;



- (id)init {
	// called from [Dashboard makeControllerForPart]
    if (self = [super initWithNibName:@"RankerViewController" bundle:nil]) {
//        parsedPageData = nil;
//        page = nil;
		self.sortAscending = (BOOL*)malloc(sizeof(BOOL) * NR_SORT_FIELDS);
		self.shownUnits = (RankerUnit_MList*)self.rankerData.entries;
        return self;
    } else {
        return nil;
    }
}

- (void)requestSucceeded:(NSData*)data {	
	[self.spinner stopAnimating];
	self.rankerData = [RankerData fromPlist:[data parsePlist]];
	[clickableTableHeaderView setButtonLabels];
	[clickableTableHeaderView setBenchmarkTitle:self.rankerData.benchTitle];
	self.sortField = SCORE_FIELD;
	for (int i = 0; i < NR_SORT_FIELDS; ++i)
		self.sortAscending[i] = NO;
	self.showMyUnits = NO;
	[self updateTable];
	[self buildInitialView];
}

- (void)requestFailed:(NSError*)err {
	[self.spinner stopAnimating];
//	self.titleL.text = @"Network error";
//	self.subtitleL.text = [err description];
}

- (void)buildInitialView {
	int p = 0;
	id x = [self.settings.arguments objectForKey:@VIEWSTATE_SUBPAGE];
	if ([x isKindOfClass:[NSNumber class]]) p = [x intValue];	
	[self goToPage:p];
}

- (void)goToPage:(int)np {
//	self.currentPage = np;
	[self buildPage:np];
}

// delete the contents (if any), and replace them with page 'self.currentPage'.
- (void)buildPage: (int)idx {
//    // page = [self.pages objectAtIndex:idx];
//	
//    self.currentPage = idx;
//	
//    self.selectedTableRow = 2;
	
    [self.table reloadData];
}

- (void)viewDidLoad 
{
    [super viewDidLoad];
    
    self.table.scrollsToTop = YES;
	
	/*if (self.settings.currentData) {
		[self buildInitialView];
	} else*/ {
        
//		self.subtitleL.text = @"Loading...";
		[self startLoadingData];
	}
    
    if ( self.table.tableHeaderView == nil )
    {
        self.table.tableHeaderView = self.clickableTableHeaderView;
        

    }
}



- (void)dealloc {
    [super dealloc];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView 
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [self.shownUnits count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{
    static UIImage* cellBackgroundImage = nil;
    
    
	RankerTableViewCell *cell = (RankerTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"RankerTableViewCell"];
	
	if (cell == nil) 
    {
		[[NSBundle mainBundle] loadNibNamed:@"RankerTableViewCell" owner:self options:nil];
		cell = self.tempCell;
		self.tempCell = nil;
	}
	
    
    if ( cell.backgroundView == nil )
    {
        if ( cellBackgroundImage == nil )
        {
            UIImage* originalCellBackground = [UIImage imageNamed:@"score_widget_table_cell_bg.png"];
            cellBackgroundImage = [[originalCellBackground stretchableImageWithLeftCapWidth:4.0 topCapHeight:37.0] retain];
        }
        
        
        
        UIImageView* backgroundView = [[UIImageView alloc] initWithImage:cellBackgroundImage];
        backgroundView.contentMode = UIViewContentModeScaleToFill;
        cell.backgroundView = backgroundView;
        
        [backgroundView release];
    }
    
	// set up the cell
	RankerUnit *unit = [self.shownUnits objectAtIndex:indexPath.row];
	cell.index.text = [NSString stringWithFormat:@"%@.", [self.indices objectAtIndex:indexPath.row]];
	if (unit.selected) {
		cell.selectedImage.image = [UIImage imageNamed:@"ranker_mine_badge.png"];
	} else {
		cell.selectedImage.image = nil;
	}
	cell.unitName.text = unit.name;
	cell.score.text = unit.score;
	cell.difference.text = unit.difference;
	
	return cell;
}

int scoreSort(id u1, id u2, void *context) {
	NSNumber *s1 = [u1 scoreNumeric];
	NSNumber *s2 = [u2 scoreNumeric];
	int order;
	// if scores are equal, sort by name
	if ([s1 isEqualToNumber:s2])
		order = [[u1 name] compare:[u2 name]];
	else
		order = [s2 compare:s1];
	BOOL ascending = *(BOOL*)context;
	return ascending ? -order : order;
}

int diffSort(id u1, id u2, void *context) {
	NSNumber *d1 = [u1 differenceNumeric];
	NSNumber *d2 = [u2 differenceNumeric];

	// for equal numbers, sort by score
	if ([d1 isEqualToNumber:d2])
		return scoreSort(u1, u2, context);
	
	// put nans at the end
	if (isnan([d1 doubleValue]))
		return NSOrderedDescending;
	if (isnan([d2 doubleValue]))
		return NSOrderedAscending;

	// sort two different non-nan numbers
	BOOL ascending = *(BOOL*)context;
	return ascending ? [d1 compare:d2] : [d2 compare:d1];
}

- (void)sortUsingFunction:(NSInteger (*)(id, id, void *))compare {
	[self.rankerData.entries sortUsingFunction:compare context:&sortAscending[self.sortField]];
}

- (void)updateTable {
	// first sort (we need to sort all units before filtering since ranks are based on unfiltered results)
	switch (self.sortField) {
		case SCORE_FIELD:
			[self sortUsingFunction:scoreSort];
			break;
		case DIFF_FIELD:
			[self sortUsingFunction:diffSort];
			break;
		default:
			[NSException raise:@"Server error" format:@"Unknown sort field: %d", self.sortField];
	}
	
	// if only showing "my units", but there are none, revert to showing all units
	if (self.showMyUnits) {
		bool anyMyUnit = false;
		for (RankerUnit *unit in self.rankerData.entries) {
			if (unit.selected) {
				anyMyUnit = true;
				break;
			}
		}
		if (!anyMyUnit)
			self.showMyUnits = NO;
	}
	
	// then find out which units to display
	self.shownUnits = [[NSMutableArray alloc] init];
	self.indices = [[NSMutableArray alloc] init];
	for (int i = 0; i < [self.rankerData.entries count]; ++i) {
		RankerUnit *unit = [self.rankerData.entries objectAtIndex:i];
		if (!self.showMyUnits || unit.selected) {
			[self.shownUnits addObject:unit];
			[self.indices addObject:[NSNumber numberWithInt:i+1]];
		}
	}
	
	// finally reload table
    [self.table reloadData];
}

- (void)toggleSort:(SortField)field {
	// if changing sort field, don't toggle sort order
	if (self.sortField == field)
		self.sortAscending[field] = !self.sortAscending[field];
	else
		self.sortField = field;
	[self updateTable];
}

- (IBAction)toggleScoreSort:(id)sender {
	[self toggleSort:SCORE_FIELD];
}

- (IBAction)toggleDiffSort:(id)sender {
	[self toggleSort:DIFF_FIELD];
}

- (IBAction)toggleMyUnits:(id)sender {
	self.showMyUnits = !self.showMyUnits;
	[self updateTable];
}


- (ClickableHeaderView*) clickableHeaderView
{
    if ( clickableTableHeaderView == nil )
    {
        NSDictionary* proxies = [NSDictionary dictionaryWithObject:[UIImage imageNamed:@"widget_filter_bg_bordered.png"] forKey:@"ClickableHeaderBackgroundImage"];
        NSDictionary* options = [NSDictionary dictionaryWithObject:proxies forKey:UINibExternalObjects];
        
        NSArray* topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"ClickableHeaderView" owner:self options:options];
        
        ClickableHeaderView* header = nil;
        UIImage* img = nil;
        
        for ( NSObject* obj in topLevelObjects )
        {
            if ( [obj isMemberOfClass:[ClickableHeaderView class]] )
            {
                header = (ClickableHeaderView*)obj;
            }
            else if ( [obj isMemberOfClass:[UIImage class]] )
            {
                img = (UIImage*)obj;
            }
        }
        
        if ( header != nil )
        {
            clickableTableHeaderView = [header retain];

            [clickableTableHeaderView setBackgroundImage:img];
			
            clickableTableHeaderView.delegate = self;
			[clickableTableHeaderView setButtonLabels];
            
            
            // view controller specific visual elements
            clickableTableHeaderView.haveCommentsElement.hidden = YES;
            clickableTableHeaderView.smileyElement.hidden = YES;
            clickableTableHeaderView.rankerQuestionLabel = NO;
            
        }
        
        
        // resize width to match
        
        CGFloat tableWidth = self.table.frame.size.width;
        CGRect headerViewFrame = clickableTableHeaderView.frame;
        
        if ( tableWidth != headerViewFrame.size.width )
        {
            CGRect newHeaderFrame = CGRectMake( headerViewFrame.origin.x, headerViewFrame.origin.y, tableWidth, headerViewFrame.size.height );
            
            clickableTableHeaderView.frame = newHeaderFrame;
        }
        
        
        
        [clickableTableHeaderView finalizeLayout];

        
        
        // hide grooves
        
            //hide none!
        
        
    }
    
    
    return clickableTableHeaderView;
}

- (void)upperRightCornerPressedAction {
	[self.clickableTableHeaderView launchConfigOf:CHScoreSelector];
}


@end
