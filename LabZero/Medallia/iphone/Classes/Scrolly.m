#import "Scrolly.h"
#import "Dummy.h"
#import "ScoreWidget.h"
@implementation Scrolly
synth_Scrolly
- (void)awakeFromNib {
	[super awakeFromNib];
	
    // jjb -- this was so convoluted. Dashboard was doing delegation for scrolly anyway as a "ScrollyDelegate" and
    // there are enough public methods on Scrolly for Dashboard to also serve as the UIScrollViewDelegate, which is desired because
    // it's in the responder chain, where we want our scroll delegate to be.   delegate relatonship is set up in the xib
    
    //self.delegate = self;
	
    
    
    
    self.pagingEnabled = YES;
	self.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
	self.controllers = [NSMutableArray array];
//	self.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"main_app_bg.png"]];
    self.clipsToBounds = NO;
    
    self.scrollsToTop = NO;
    
	[self reloadViews];
}

- (void) reloadViews {
	dirtyViews = YES;
	[self setNeedsLayout];
}

- (void) layoutSubviews
{
	if (!dirtyViews) 
    {
        return;
    }
    
	dirtyViews = NO;
    
	self.controllers = [NSMutableArray array];
	
    while ( [self.subviews count] > 0 ) 
    {
		[[self.subviews lastObject] removeFromSuperview];
	}
    
	int n = [scrollyDelegate numberOfParts];
	CGSize sz = self.bounds.size;
	self.contentSize = CGSizeMake(sz.width * n, sz.height);
	
    for (int i=0; i<n; i++) 
    {
		UIViewController *vc = [self controllerForPart:i];
		UIView *v = [vc view];		
		
        v.frame = CGRectMake( sz.width*i, 0, sz.width, sz.height );
		
        [vc viewWillAppear:NO];
		[self addSubview:v];
	}
}
- (void)scrollToPage:(int)i {
	[self setContentOffset:CGPointMake(self.bounds.size.width * i, 0) animated:YES]; 
}
- (UIViewController*) controllerForPart:(int)i {
	if (i < [self.controllers count])  {
		id vc = [self.controllers objectAtIndex:i];
		if (vc != [NSNull null]) return vc;
	}
	while ([self.controllers count] <= i) [self.controllers addObject:[NSNull null]];
	id vc = [scrollyDelegate makeControllerForPart:i];
	if (vc) [self.controllers replaceObjectAtIndex:i withObject:vc];
	return vc;
}
- (int)currentPage {
	float w = self.bounds.size.width;
	int i = self.contentOffset.x/w + 0.5;
	int n = [scrollyDelegate numberOfParts];
	if (i>=n) i = n-1;
	if (i<0) i = 0;
	return i;
}

/*
- (void)scrollViewDidScroll:(UIScrollView *)scrollView 
{
	[scrollyDelegate pageChanged:[self currentPage]];
}

- (BOOL)scrollViewShouldScrollToTop:(UIScrollView *)scrollView
{
    NSLog( @"[Scrolly scrollViewShouldScrollToTop]" );
    
    return YES;
}

*/

@end
