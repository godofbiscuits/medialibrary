//
//  ResponsesEntryProtocol.h
//  Medallia
//
//  Created by Øyvind Grotmol on 09.03.10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SingleResponse;

@protocol ResponsesEntryProtocol

@required @property(copy, readonly) NSString * detailsURL;
@required @property(copy, readonly) NSString * name;
@required @property(retain) SingleResponse * fullResponse;

@end
