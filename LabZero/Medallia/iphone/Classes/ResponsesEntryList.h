//
//  ResponsesList.h
//  Medallia
//
//  Created by Øyvind Grotmol on 09.03.10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ResponsesEntryProtocol.h"
#import "DataFormat.h"

@interface /*NOGENERATE*/ ResponsesEntryList : NSObject {

	NSMutableArray * responses;
	
}

@property (nonatomic, retain) NSMutableArray * responses;

@property (readonly) int count;

- (id<ResponsesEntryProtocol>)responseAt:(int)position;

- (void) updateResponseAtIndex:(int)position withResponse:(ResponsesEntry*)response;

- (void)addLump:(ResponsesLump*)lump;

@end
