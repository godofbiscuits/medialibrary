
#import "SharableTabBarController.h"

@implementation SharableTabBarController
- (id)init {
	self = [super init];
	self.delegate = self;
	return self;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return [self.selectedViewController shouldAutorotateToInterfaceOrientation:interfaceOrientation];
}

- (void) updateNavItems:(BOOL)animated {
	UIViewController *vc = self.selectedViewController;
	if (!vc) vc = [self.viewControllers objectAtIndex:0];
	UINavigationItem *to = self.navigationItem, *from = vc.navigationItem;
	to.title = from.title;
	to.prompt = from.prompt;
	to.backBarButtonItem = from.backBarButtonItem;
	to.titleView = from.titleView;
	[to setRightBarButtonItem: from.rightBarButtonItem animated:animated];
}
- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController {
	[self updateNavItems:YES];
}
@end
