#import "MedalliaAppDelegate.h"

#import "ResponseDetails.h"
#import <AudioToolbox/AudioToolbox.h>
#import "UIViewControllerShared.h"
#import "Dummy.h"
#import "SimpleRequest.h"
#import "NotesViewController.h"
#import "PhoneCallInfo.h"
#import "ResponsesEntryHolder.h"
#import "CallLogViewController.h"

#import "EmployeeScorecardViewController.h"


#define IS_TESTING_PHONE_ACTION NO

    // button indexes
NSUInteger phoneButtonIndex = -1;
NSUInteger emailButtonIndex = -1;

NSString* kMEDWebViewURLSchemeOpen = @"open";
NSString* kMEDWebViewURLQueryScorecard = @"scorecard";
NSString* kMEDWebViewURLQueryNotes = @"Notes";
NSString* kMEDWebViewURLAlert = @"Alert";


@interface ResponseDetails ( EmailComposition )

- (void) sendEmailUsingEmailAction;
- (void) makePhonecallUsingPhoneAction;

@end


@interface ResponseDetails ( URLHanding )

- (void) viewNotes;
- (void) manageAlert;
- (void) viewEmployeeScorecard:(NSURL*)url;

@end



@implementation ResponseDetails

@synthesize list;
@synthesize alertList;

- (id<ResponsesEntryProtocol>) getCurrentResponseEntry
{
	return [self.list responseAt:currentPosition];
}

- (SingleResponse*)shownResponse
{
	return [self getCurrentResponseEntry].fullResponse;
}

- (void)setShownResponse:(SingleResponse*)response
{
	[self getCurrentResponseEntry].fullResponse = response;
}

- (NSURL*) phoneURL
{
	return [NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", self.shownResponse.customerPhone]];
}

- (void)configureToolbar:(BOOL)show
{
	if (!self.shownResponse) 
    {
        return;
    }
    
    BOOL canSendMail = [MFMailComposeViewController canSendMail];
	BOOL showEmailButton = canSendMail && self.shownResponse.emailAction != nil;
	
	//NSURL * phoneCallURL = [self phoneURL];
    BOOL canMakePhonecall = [[UIApplication sharedApplication] canOpenURL:[self phoneURL]] || IS_TESTING_PHONE_ACTION;
	BOOL showPhoneButton = canMakePhonecall && self.shownResponse.customerPhone != nil;
    
    
    if ( showEmailButton || showPhoneButton )
    {
		if (show) {
	        UIActionSheet* actionSheet = [[[UIActionSheet alloc] init] autorelease];
	        
	        if (showPhoneButton)
			{
				phoneButtonIndex = [actionSheet addButtonWithTitle:NSLocalizedString( @"Call customer", @"" )];
				actionSheet.destructiveButtonIndex = phoneButtonIndex;
			}
	        
	        if ( showEmailButton )
	        {
	            // [buttonTitles addObject:NSLocalizedString( @"Forward by email", @"" )];
	            emailButtonIndex = [actionSheet addButtonWithTitle:NSLocalizedString( @"Forward by email", @"" )];
	            
	            actionSheet.title = NSLocalizedString( @"Available Actions", @"" );
	        }
	        else if ( !canSendMail )
	        {
	            // modify text of sheet to explain absence of email button
	            actionSheet.title = NSLocalizedString( @"Note: this device is not configured to send email.", @"" );
	        }
	        
	        actionSheet.delegate = self;
	        
	        actionSheet.cancelButtonIndex = [actionSheet addButtonWithTitle:@"Cancel"];
	        
	        [actionSheet showFromToolbar:toolbar];
		}
			
		toolbar.hidden = NO;
    }
    else
    {
        toolbar.hidden = YES;
    }
}



- (void)refreshHtml
{
	if (self.shownResponse) {
		[webView loadHTMLString:self.shownResponse.html baseURL:[NSURL URLWithString:@"http://localhost"]];
		[self configureToolbar:NO];
	}
}

- (id)initWithList:(ResponsesEntryList*)aList position:(int)i alertList:(ResponseList*)anAlertList {
	self = [super initWithNibName:@"ResponseDetails" bundle:nil];
    
    // ??? jjb - in previous version, webview was set only to detect phone numbers.  Do we want to detect links as well?  
    // pivotal: https://www.pivotaltracker.com/story/show/2524803
    
    webView.dataDetectorTypes = ( UIDataDetectorTypePhoneNumber );	
    
    currentPosition = i;
	self.list = aList;
	self.alertList = anAlertList;
	return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
	prevNext.segmentedControlStyle = UISegmentedControlStyleBar;
	prevNext.tintColor = [UIColor colorWithRed:0.3 green:0.3 blue:0.3 alpha:1];
	
	self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:prevNext] autorelease];
	[self loadPage:currentPosition];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
	
	// in case we're coming back from the "Add Note" screen, and this view should update
	[self refreshHtml];
}

- (void)viewWillDisappear:(BOOL)animated {
	[super viewWillDisappear:animated];
}
- (bool)loadPage:(int)i {
	if (i<0 || i >= self.list.count) return NO;
	id<ResponsesEntryProtocol> e = [self.list responseAt:i];
	self.navigationItem.title = e.name;
	currentPosition = i;
	if (self.shownResponse) {
		[self refreshHtml];
	} else {
		[spinner startAnimating];
		[[[SimpleRequest alloc] initStartingRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:e.detailsURL]] for:self dropOld:YES] autorelease];
	}
	[prevNext setEnabled:i > 0 forSegmentAtIndex:0];
	[prevNext setEnabled:i < self.list.count-1 forSegmentAtIndex:1];
	return YES;
}

- (void)requestSucceeded:(NSData*)data {
	self.shownResponse = [SingleResponse fromPlist:[data parsePlist]];
	[self refreshHtml];
	
	// update nr of open alerts badge value
	if (self.shownResponse.nrOpenAlerts && alertList)
		alertList.tabBarItem.badgeValue = self.shownResponse.nrOpenAlerts;
}

- (void)requestFailed:(NSError*)err {
	[webView loadHTMLString:@"Failed" baseURL:[NSURL URLWithString:@"http://localhost"]];
}

- (void)webViewDidFinishLoad:(UIWebView *)wv {
	[spinner stopAnimating];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return ( interfaceOrientation == UIInterfaceOrientationPortrait );
}


- (BOOL)webView:(UIWebView *)wv shouldStartLoadWithRequest:(NSURLRequest *)req
 navigationType:(UIWebViewNavigationType)navType {	
	// open:?http%3a//... - open url with MobileSafari; can be used for mailto: etc as well
    NSLog(@"url '%@'", req.URL);
    NSLog(@"query '%@'", req.URL.query);
	
	//NSString * queryString = req.URL.query;

	if ( [kMEDWebViewURLSchemeOpen isEqualToString:[[req URL] scheme]] )
    {
        if ( [kMEDWebViewURLQueryNotes isEqualToString:req.URL.query] ) {
            [self viewNotes];
        } else if ( [kMEDWebViewURLAlert isEqualToString:req.URL.query] ) {
			[self manageAlert];
		} else if ( [req.URL.query hasPrefix:kMEDWebViewURLQueryScorecard] ) 
        {
			NSString * keyEncoded = [req.URL.query substringFromIndex:[kMEDWebViewURLQueryScorecard length]];
            
			NSMutableString * stringForURLToSend = [self.shownResponse.scoreboardBaseURL mutableCopy];
			[stringForURLToSend appendFormat:@"&%@=%@", @"response_text", keyEncoded];
            
			NSURL * url = [NSURL URLWithString:stringForURLToSend];
			NSLog(@"User wants to see scorecard with key = %@\n%@", [keyEncoded urldecode], url);
			// todo: launch scorecard
            
            [stringForURLToSend release];
            
            [self viewEmployeeScorecard:url];
		}
        else 
        {
            NSURL *s = [NSURL URLWithString:[req.URL.query urldecode]];
            NSLog(@"openurl %@", s);
            [[UIApplication sharedApplication] openURL:s];
        }
        
		return NO;
	}

	return YES;
}

- (IBAction)prevNextAction {
	// TODO: scroll list view accordingly?
	[self loadPage:currentPosition + (prevNext.selectedSegmentIndex == 0 ? -1 : 1)];
}

NSString *escape(NSString* s) {
	return [s stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
}

- (IBAction)actionAction 
{
	[self configureToolbar:YES];
}



- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex 
{
	if ( self.shownResponse == nil ) 
    {
        return;
    }
    
    /* //jjb
     if ( buttonIndex >= self.shownResponse.buttons.count )
     {
     return;
     }
     
     
     RIButton *rb = [self.shownResponse.buttons objectAtIndex:buttonIndex];
     
     [self sendEmailWithButtonInfo:rb];
     */
    
    
    
    if ( buttonIndex == phoneButtonIndex )
    {
        [self makePhonecallUsingPhoneAction];
    }
    else if ( buttonIndex == emailButtonIndex )
    {
        [self sendEmailUsingEmailAction];
    }

}


#pragma -
#pragma === ResponseDetails EmailComposition Methods
#pragma -

- (void) sendEmailUsingEmailAction
{
    if ( [MFMailComposeViewController canSendMail] == NO )
    {
        // no longer needed as we're checking upstream, but might as well leave it in place
        
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString( @"Cannot Send Email", @"" ) 
                                                        message:NSLocalizedString( @"This device is not configured to send email.", @"" ) 
                                                       delegate:self 
                                              cancelButtonTitle:nil 
                                              otherButtonTitles:@"OK", nil];
        
        [alert show];
        [alert release];
    }
    else
    {
        MFMailComposeViewController* composer = [[MFMailComposeViewController alloc] init];
        composer.mailComposeDelegate = self;
        
        
        EmailAction* ea = self.shownResponse.emailAction;
        
        [composer setSubject:ea.subject];
        [composer setToRecipients:[ea.to componentsSeparatedByString:@","]];
        [composer setMessageBody:ea.body isHTML:( ( ea.html == nil ) ? NO : YES )];
        
        
        // these fields don't exist in EmailAction as of 2010-02-20, but if they're implemented in the future
        // we can handle them when they do exist.  Thanks, dynamic runtime!
		
		// We don't want warnings, so let's just comment out this
        
//        if ( [ea respondsToSelector:@selector(cc)] )
//        {
//            [composer setCcRecipients:[[ea cc] componentsSeparatedByString:@","]];
//        }
//        
//        if ( [ea respondsToSelector:@selector(bcc)] )
//        {
//            [composer setBccRecipients:[[ea bcc] componentsSeparatedByString:@","]];
//        }
        
        
        [self presentModalViewController:composer animated:YES];
        
        [composer release];
    }
}

- (void) populateCaseManagementInfo:(CaseManagementInfo*)info {
	id<ResponsesEntryProtocol> e = [self getCurrentResponseEntry];
	info.detailsURL = e.detailsURL;
	
	if (self.shownResponse.alertActions.count > 0) {
		AlertAction * action = [self.shownResponse.alertActions objectAtIndex:0];
		info.alert_action_id = action.action;
		info.alert_action_name = action.text;
	}
}

- (void) makePhonecallUsingPhoneAction
{
    NSLog( @"[ResponseDetails makePhonecallUsingPhoneAction]: use PhoneAction to get phone #" );
	
	NSURL * url = [self phoneURL];
    
    BOOL canMakePhoneCall = [[UIApplication sharedApplication] canOpenURL:url];
    
    NSLog( @"canMakeCall: %@", canMakePhoneCall ? @"YES" : @"NO" );

	id<ResponsesEntryProtocol> e = [self getCurrentResponseEntry];
	
	PhoneCallInfo * phoneCallInfo = [[PhoneCallInfo alloc] init];
	phoneCallInfo.didCall = TRUE;
	phoneCallInfo.callInitiated = [NSDate date];
	phoneCallInfo.customerName = e.name;
	[self populateCaseManagementInfo:phoneCallInfo];
	
	[phoneCallInfo store];
	
    NSLog( @"alert_action_id: %@ alert_action_name: %@", phoneCallInfo.alert_action_id, phoneCallInfo.alert_action_name);
	
	[[UIApplication sharedApplication] openURL:url];
	
    [phoneCallInfo release];
//    [e release]; TODO: what should we do here?
    
}

- (void) viewNotes
{
	ResponsesEntryHolder * holder = [[[ResponsesEntryHolder alloc] initWithList:self.list atPosition:currentPosition] autorelease];
	NotesViewController *nvc = [[[NotesViewController alloc] init:holder] autorelease];
	[self.sharedNavController pushViewController:nvc animated:YES];
}

- (void) manageAlert
{
	CaseManagementInfo* info = [[CaseManagementInfo alloc] init];
	info.customerName = self.shownResponse.alertName;
	info.alertStatus = self.shownResponse.alertStatus;
	[self populateCaseManagementInfo:info];
	
    CallLogViewController* callLogController = [[CallLogViewController alloc] init];
	callLogController.info = info;
	
	callLogController.responsesEntryHolder = [[[ResponsesEntryHolder alloc] initWithList:self.list atPosition:currentPosition] autorelease];
	
    [self.navigationController pushViewController:callLogController animated:YES];
    [callLogController release];

}



- (void) viewEmployeeScorecard:(NSURL*)url
{

    EmployeeScorecardViewController* scorecardViewController = [[EmployeeScorecardViewController alloc] initWithServletURL:url];
    
    [self.sharedNavController pushViewController:scorecardViewController animated:YES];
    
    [scorecardViewController release];
}






#pragma -
#pragma === MFMailComposeViewControllerDelegate
#pragma -


- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error 
{	
	[self dismissModalViewControllerAnimated:YES];
    
	
    if ( result == MFMailComposeResultFailed )
    {
        UIAlertView* mailFailAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString( @"Email failed to be sent", @"" )
                                                                message:NSLocalizedString( @"Be sure that Mail is configured properly to send mail and try again later.", @"" )
                                                               delegate:self
                                                      cancelButtonTitle:nil
                                                      otherButtonTitles:NSLocalizedString( @"OK", @"" )];
        
        [mailFailAlert show];
        [mailFailAlert release];
    }
}


#pragma -
#pragma === UIAlertViewDelegate
#pragma -





@end
