#import "LoginController.h"

#define S_COMPANY @"M.login.company"
#define S_USER @"M.login.user"
#define S_PASSWORD @"M.login.password"

@implementation LoginController
synth_LoginController

- (id)init {
	self = [super initWithNibName:@"LoginView" bundle:nil];
	self.navigationItem.title = @"Log out";
	return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
	textFields[0] = tf1; textFields[1] = tf2; textFields[2] = tf3;
	
	for (int i=3; i-->0; ) {
		textFields[i].textColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:1];
		textFields[i].delegate = self;
	}
}

- (void)viewWillAppear:(BOOL)anim {
	[self resetCU];
}
- (void)viewWillDisappear:(BOOL)anim {
	NSString *a = textFields[0].text;
	NSString *b = textFields[1].text;
	NSString *c = textFields[2].text;
	if (!a) a = @""; if (!b) b = @""; if(!c) c = @"";
	[[NSUserDefaults standardUserDefaults] setObject:a forKey:S_COMPANY];
	[[NSUserDefaults standardUserDefaults] setObject:b forKey:S_USER];
	[[NSUserDefaults standardUserDefaults] setObject:c forKey:S_PASSWORD];
	[[NSUserDefaults standardUserDefaults] synchronize];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation 
{
    return ( interfaceOrientation == UIInterfaceOrientationPortrait );
}

- (BOOL)textFieldShouldReturn:(UITextField *)tf {
	if (tf == textFields[0]) [textFields[1] becomeFirstResponder];
	if (tf == textFields[1]) [textFields[2] becomeFirstResponder];
	if (tf == textFields[2]) {
		[self tryLogin];
	}
	return YES;
}

- (IBAction)tryLogin {
	NSString *arguments[3];
	for (int i=0; i<3; i++) {
		NSString *s = arguments[i] = textFields[i].text;
		if (s == nil || [s length] <= 0) {
			[textFields[i] becomeFirstResponder];
			return;
		}
	}
	if ([arguments[0] isEqualToString:@"reset"]) {
		[[NSUserDefaults standardUserDefaults]
		 removePersistentDomainForName:[[NSBundle bundleForClass:
										 [self class]] bundleIdentifier]];
		[[[[UIAlertView alloc] initWithTitle:@"All settings cleared"
									 message:nil
									delegate:nil
						   cancelButtonTitle:@"Ok"
						   otherButtonTitles:nil] autorelease] show];
		for (int i=3; i-->0; )
			textFields[i].text = @"";
		[textFields[0] becomeFirstResponder];
	} else {
		if (self.delegate) [self.delegate readyForLogin:arguments];
	}
}

- (void)setWorking {
	[self.spinner startAnimating];
}
- (void)setActive {
	[self.spinner stopAnimating];
}
- (void)setPassive {
	[self.spinner stopAnimating];
}
- (void)clearPassword {
	[[NSUserDefaults standardUserDefaults] setObject:nil forKey:S_PASSWORD];
}
- (void)resetCU {
	textFields[0].text = [[NSUserDefaults standardUserDefaults] stringForKey:S_COMPANY];
	textFields[1].text = [[NSUserDefaults standardUserDefaults] stringForKey:S_USER];
	textFields[2].text = [[NSUserDefaults standardUserDefaults] stringForKey:S_PASSWORD];
	for (int i=0; i<3; i++) if (!textFields[i].text || !textFields[i].text.length) {
		[textFields[i] becomeFirstResponder];
		return;
	}
	[textFields[2] becomeFirstResponder];
}
@end
