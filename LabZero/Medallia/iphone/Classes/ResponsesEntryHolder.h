//
//  ResponsesEntryHolder.h
//  Medallia
//
//  Created by Øyvind Grotmol on 10.03.10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ResponsesEntryList.h"

@interface /*NOGENERATE*/ ResponsesEntryHolder : NSObject {

	int position;
	ResponsesEntryList * list;
	
}

@property (nonatomic, assign) int position;
@property (nonatomic, retain) ResponsesEntryList * list;

-(id)initWithList:(ResponsesEntryList*)list atPosition:(int)position;

-(id<ResponsesEntryProtocol>)get;

-(void)update:(ResponsesEntry*)response;

@end
