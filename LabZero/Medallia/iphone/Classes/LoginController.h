#import "Gen.h"
#import <UIKit/UIKit.h>

// This controls the login screen UI. When the user has entered a company/user/password triple, we pass it to the delegate.

@protocol LoginDelegate
- (void)readyForLogin:(NSString**)arguments;
@end

@interface LoginController : UIViewController <UITextFieldDelegate> {
	// Interface Builder doesn't seem to support arrays very well, so we do this. These are only used in viewDidLoad to populate the
	// arrays below.
	IBOutlet UITextField *tf1, *tf2, *tf3;
       
	UITextField *textFields[3];
	ivars_LoginController;
}
@property(retain) id<LoginDelegate> delegate;
@property(retain) IBOutlet UIActivityIndicatorView *spinner;

- (void)setWorking;
- (void)setActive;
- (void)setPassive;
- (void)resetCU;
- (void)clearPassword;
- (IBAction)tryLogin;
@end
