#import <UIKit/UIKit.h>
#import "Gen.h"
#import "DataFormat.h"

// Allows the user to choose one Alt from an AltTable; sends the selected value to the Delegate.

@class AltTableDropdown;

@protocol AltDelegate
- (void)alternativeWasSelected:(Alt*)a forDropdown:(AltTableDropdown*)dropdown;
@end

@interface AltTableDropdown : UITableViewController {
	ivars_AltTableDropdown;
}

@property(retain) ConfigPart *configPart;
@property(retain) AltTable *table;
@property(retain) id<AltDelegate> delegate;
@property(copy) NSString *title;
@property(copy) NSString *oldSelection; // key

- (id)initWithConfig:(ConfigPart*)config delegate:(id<AltDelegate>)d;

@end
