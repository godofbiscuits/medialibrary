//
//  CaseManagementInfo.m
//  Medallia
//
//  Created by Øyvind Grotmol on 23.03.10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "CaseManagementInfo.h"

@implementation CaseManagementInfo

@synthesize callInitiated, customerName, alertStatus, detailsURL, alert_action_id, alert_action_name;

-(NSString*)navTitle {
	return @"Alert";
}

-(NSString*)nameLabel {
	return @"Alert";
}

-(NSString*)statusLabel {
	return @"Status";
}

-(NSString*)statusOrCallInitiated {
	if (self.callInitiated) {
        // call initiated date as short-format date string
		NSDateFormatter* fmt = [[[NSDateFormatter alloc] init] autorelease];
		[fmt setDateStyle:NSDateFormatterShortStyle];
		[fmt setTimeStyle:NSDateFormatterShortStyle];
		return [fmt stringFromDate:self.callInitiated];
	} else {
		return self.alertStatus;
	}
}

@end
