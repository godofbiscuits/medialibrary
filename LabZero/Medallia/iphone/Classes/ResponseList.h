#import "Gen.h"
#import <UIKit/UIKit.h>
#import "DataFormat.h"
#import "NibTableCell.h"
#import "WidgetConfig.h"
#import "SimpleRequest.h"
#import "ColoredImage.h"
#import "ResponsesEntryList.h"
#import "ClickableHeaderView.h"

@interface ResponsesCell : NibTableCell {
	IBOutlet ColoredImage *scoreBackground;
	IBOutlet UIImageView *alertImage;
	IBOutlet UILabel *scoreLabel, *alertLabel, *titleLabel, *subheaderLabel;
	IBOutlet UILabel *bodyLabel;
	int n;
}
+ (CGFloat)heightWithFontSize:(int)fs lines:(int)ls response:(ResponsesEntry*)response;
- (id)replaceWithDataFrom:(ResponsesEntry*)data fontSize:(int)ps lines:(int)ls;
@end



@class ClickableHeaderView;


@interface /*NOGENERATE*/ ResponseList : UITableViewController <ClickableHeaderDelegate, SRDelegate, UISearchBarDelegate> {
	IBOutlet UIView *footer;
	IBOutlet UIView *loadingFooter;
	IBOutlet UISearchBar *searchBar;
	IBOutlet UILabel *footerL;

    BOOL isAlertList_;
	NSNumber_MList *sectionOffsets_;
	ResponsesLump *lastLump_;
	ResponsesEntryList *entries_;
	WidgetInstance *settings_;
	
	NSArray *buttons;

	ResponseList *alertList;
	
	NSString *firstVisibleID;
    ClickableHeaderView* clickableHeadersCellContentView;
    
    UIView* haveCommentsButtonView;
    
    
    IBOutlet UITableViewCell* clickableHeadersCell;
}


@property (assign) BOOL isAlertList;
@property (retain) NSNumber_MList *sectionOffsets;
@property (retain) ResponsesLump *lastLump; // for the 'more' section
@property (retain) ResponsesEntryList *entries;
@property (retain) WidgetInstance *settings;
@property (retain) NSArray *buttons;
@property (nonatomic, retain, readonly, getter=clickableHeaderView) IBOutlet ClickableHeaderView* clickableHeadersCellContentView;
@property (nonatomic, retain) IBOutlet UITableViewCell* clickableHeadersCell;
@property (nonatomic, assign) IBOutlet UIView* haveCommentsButtonView;

@property (retain) ResponseList *alertList; // needed to update alert count
- (id)initWithDefaultSettings:(WidgetInstance*)defaultSettings;
- (id)initWithDefaultSettings:(WidgetInstance*)defaultSettings alertList:(ResponseList*)alertList;
- (IBAction)loadMore;
- (void)startLoadingFromScratch;
- (void)startLoadingFrom:(NSURL*)u;
- (int)sectionZeroAbeyantOffsetForSection:(int)s;
- (void)saveSettings;

- (void) showCallLogResponseDetails:(ResponsesEntryList*)entryList;
- (UINavigationController*)getNavController;
- (WidgetInstance*)getWidgetInstance;

- (void)setSelectedScopeButton:(NSString*)key;

- (void) captureCallLogDetails;

@end
