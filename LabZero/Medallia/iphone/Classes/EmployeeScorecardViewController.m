//
//  EmployeeScorecardViewController.m
//  Medallia
//
//  Created by Jeff Barbose on 3/5/10.
//  Copyright 2010 Medallia, Inc. All rights reserved.
//

#import "EmployeeScorecardViewController.h"
#import "ResponseDetails.h"
#import "UIImage+ImageScaling.h"



//#import "EmployeeHistoryDetail.h"

enum EmployeeHistoryDetailSections {
    EmployeeHistoryDetailSectionsBaseValue = -1,
    EmployeeHistoryDetailSectionScores,
    EmployeeHistoryDetailSectionResponses
} ;

typedef NSUInteger EmployeeHistoryDetailSection;


@interface /*NOGENERATE*/ EmployeeScorecardViewController ()

@property (nonatomic, retain, readwrite) Scorecard* scorecardModelData;

@end




@implementation EmployeeScorecardViewController

@synthesize scorecardModelData;
@synthesize employeeDataTableView;
@synthesize employeeTableHeaderView;
@synthesize scoresTableCell, responsesTableCell;
@synthesize spinner;
@synthesize displayedResponses;
@synthesize responsesSectionRowHeight;
@synthesize scoresSectionRowHeight;

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/

- (ResponsesEntry*)responseAt:(int)position {
	return (ResponsesEntry*)[self.displayedResponses responseAt:position];
}

- (EmployeeScorecardViewController*) initWithServletURL:(NSURL*) aServletURL { 
    (void) [[[SimpleRequest alloc] initStartingRequest:[NSURLRequest requestWithURL:aServletURL] for:self dropOld:YES] autorelease];

    if (self = [super initWithNibName:@"EmployeeScorecardViewController" bundle:nil]) {
		[self.spinner startAnimating];
        return self;
    } else {
        return nil;
    }
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];

    self.employeeDataTableView.allowsSelection = YES;
    
    [[NSBundle mainBundle] loadNibNamed:@"EmployeeScoresTableCell" owner:self options:nil];
    scoresSectionRowHeight = scoresTableCell.frame.size.height;
    scoresTableCell = nil;

    [[NSBundle mainBundle] loadNibNamed:@"EmployeeResponseListCell" owner:self options:nil];
    responsesSectionRowHeight = responsesTableCell.frame.size.height;
    responsesTableCell = nil;
}



/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning 
{
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload 
{
    [super viewDidUnload];
    
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}




- (void)dealloc 
{
    [scorecardModelData release];
    
    [employeeDataTableView release];
    [employeeTableHeaderView release];
    
    scoresTableCell = nil;
    responsesTableCell = nil;
    
    [super dealloc];
}





#pragma mark -
#pragma mark === TableView DataSource & Delegate Methods
#pragma mark -
#pragma mark • DataSource •


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch ( section ) 
    {
        case EmployeeHistoryDetailSectionScores:
        {
            NSLog( @"Scores Section rowCount:%d", self.scorecardModelData.scores.count );
            
            return self.scorecardModelData.scores.count;
            
            break;
        }
        
        
        case EmployeeHistoryDetailSectionResponses:
        {
            NSLog( @"Responses Section rowCount:%d", self.scorecardModelData.responsesLump.entries.count );
            
            return self.scorecardModelData.responsesLump.entries.count;
            
            break;
        }
            
        default:
        {
            return 0;
            
            break;
        }
    }
    
}



- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    switch ( section ) 
    {
        case EmployeeHistoryDetailSectionScores:
        {
            return NSLocalizedString( @"Scores — past 12 months", @"" );
            
            break;
        }
            
            
        case EmployeeHistoryDetailSectionResponses:
        {
            return NSLocalizedString( @"Responses", @"" );
            
            break;
        }
            
        default:
        {
            return NSLocalizedString( @"Header", @"" );
            
            break;
        }
    }
}



- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    {
        switch ( section ) 
        {
            case EmployeeHistoryDetailSectionScores:
            {
                if ( self.scorecardModelData != nil )
                {
                    return [NSString stringWithFormat:@"Number of responses: %@", self.scorecardModelData.numberOfResponses];
                }
                else
                {
                    return nil;
                }
                
                break;
            }
                
                
            case EmployeeHistoryDetailSectionResponses:
            default:
            {
                return nil;
                
                break;
            }
        }
    }
}




- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* ScoreCellIdentifer = @"ScoreCellIdentifier";
    static NSString* ResponseCellIdentifier = @"ResponseCellIdentifier";
    
    NSString* cellID = ( ( indexPath.section == EmployeeHistoryDetailSectionScores ) ? ScoreCellIdentifer 
                        : ( indexPath.section == EmployeeHistoryDetailSectionResponses ) ? ResponseCellIdentifier
                        : nil );
    
    if ( cellID == nil )
    {
        return nil; // this should never happen
    }
    
    
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:cellID];

    if ( cell == nil )
    {
        if ( cellID == ScoreCellIdentifer )
        {
            [[NSBundle mainBundle] loadNibNamed:@"EmployeeScoresTableCell" owner:self options:nil];
            cell = self.scoresTableCell;
            self.scoresTableCell = nil;
        }
        else if ( cellID == ResponseCellIdentifier )
        {
            [[NSBundle mainBundle] loadNibNamed:@"EmployeeResponseListCell" owner:self options:nil];
            cell = self.responsesTableCell;
            self.responsesTableCell = nil;
        }
    }
    
    
    if ( cellID == ScoreCellIdentifer )
    {
        EmployeeScoresTableCell* scoreCell = (EmployeeScoresTableCell*)cell;
        
        ScorecardScore* singleScore = [self.scorecardModelData.scores objectAtIndex:indexPath.row];
        
        scoreCell.titleLabel.text = singleScore.title;
        
        NSString* backgroundColorName = singleScore.scoreBackgroundColor;
                
        if ( backgroundColorName == nil )
        {
            scoreCell.valueBackgroundImageView.image = nil;
            scoreCell.valueLabel.textColor = [UIColor blackColor];
        }
        else
        {
            NSString* backgroundImageName = [NSString stringWithFormat:@"db-score-bg-%@.png", backgroundColorName];
            
            UIImage* scaledImage = [UIImage imageNamed:backgroundImageName];
            
            scoreCell.valueBackgroundImageView.image = scaledImage;
        }
        
        scoreCell.valueLabel.text = singleScore.value;
        CGFloat fontSize = scoreCell.valueLabel.font.pointSize;
        scoreCell.valueLabel.font = [UIFont boldSystemFontOfSize:fontSize];
        
        return scoreCell;
    }
    else if ( cellID == ResponseCellIdentifier )
    {
        ResponsesCell* responseCell = (ResponsesCell*)cell;
        
        ResponsesEntry* entry = [self responseAt:indexPath.row];
        
        [responseCell replaceWithDataFrom:entry fontSize:13 lines:3];
        
        responseCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        responseCell.selectionStyle = UITableViewCellSelectionStyleBlue;
        
        return responseCell;
    }
    
    return cell;
}




#pragma mark -
#pragma mark • Delegate •



- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    return (indexPath.section == EmployeeHistoryDetailSectionScores ) ? nil : indexPath;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
	ResponseDetails* employeeResponseDetailsViewController = [[ResponseDetails alloc] 
                                                              initWithList:self.displayedResponses
                                                              position:indexPath.row
                                                              alertList:nil
	];

	[self.navigationController pushViewController:employeeResponseDetailsViewController animated:YES];
    
    [employeeResponseDetailsViewController release];
}




- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch ( indexPath.section ) 
    {
        case EmployeeHistoryDetailSectionScores:
        {
            return self.scoresSectionRowHeight;
            
            break;
        }
            
            
        case EmployeeHistoryDetailSectionResponses:
        {
            return self.responsesSectionRowHeight;
            
            break;
        }
            
        default:
        {
            return 44; // default row height in IB
            
            break;
        }
    }
    
}


/*
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
}
*/


#pragma mark -
#pragma mark === SRDelegate Methods
#pragma mark -


- (void)requestSucceeded:(NSData*)data {
	[self.spinner stopAnimating];
	
    //NSString* plistString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    //[plistString writeToFile:@"/tmp/plistString" atomically:YES];
    
    self.scorecardModelData = [Scorecard fromPlist:[data parsePlist]];
    
	self.navigationItem.title = scorecardModelData.scorecardName;
	
    self.displayedResponses = [[ResponsesEntryList alloc] init];
    [self.displayedResponses addLump:self.scorecardModelData.responsesLump];
    
    [self.employeeDataTableView reloadData];
}



- (void)requestFailed:(NSError*)err {
	[self.spinner stopAnimating];
}

/*

- (void)requestSucceeded:(NSData*)data {
	SingleResponse * response = [SingleResponse fromPlist:[data parsePlist]];
}

- (void)requestFailed:(NSError*)err {
	UIAlertView *alert= [[UIAlertView alloc]
                         initWithTitle:NSLocalizedString( @"Server Error", @"" )
                         message:NSLocalizedString( @"Sorry, note could not be saved.", @"" )
                         delegate:self
                         cancelButtonTitle:NSLocalizedString( @"OK", @"" )
                         otherButtonTitles:nil];
	
	[alert show];
	[alert autorelease];
}
*/




@end
