//
//  NotesViewController.m
//  Medallia
//
//  Created by Øyvind Grotmol on 02.03.10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "NotesViewController.h"
#import "NoteTableViewCell.h"
#import "DataFormat.h"
#import "AddNoteViewController.h"
#import "UIViewControllerShared.h"
#import "ResponsesEntryHolder.h"

@implementation NotesViewController

@synthesize tempCell, responsesEntryHolder;

-(SingleResponse*) fullResponse
{
	return [self.responsesEntryHolder get].fullResponse;
}

- (id)init:(ResponsesEntryHolder*)response
{
	if (self = [super init]) {
		self.responsesEntryHolder = response;
	}
	return self;
}


/*
- (id)initWithStyle:(UITableViewStyle)style {
    // Override initWithStyle: if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
    if (self = [super initWithStyle:style]) {
    }
    return self;
}
*/


- (void)viewDidLoad {
    [super viewDidLoad];

	self.navigationItem.title = @"Notes";
	
	self.navigationItem.rightBarButtonItem =
	[[[UIBarButtonItem alloc] initWithTitle:@"Add note" style:0 target:self action:@selector(addNote)] autorelease];

	self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:nil action:nil];

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

	// in case we're coming back from the "Add Note" screen, and this view should update
	[self.tableView reloadData];
}

/*
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}
*/
/*
- (void)viewWillDisappear:(BOOL)animated {
	[super viewWillDisappear:animated];
}
*/
/*
- (void)viewDidDisappear:(BOOL)animated {
	[super viewDidDisappear:animated];
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


#define FONT_SIZE 14.0f
#define CELL_CONTENT_WIDTH 292.0f
#define CELL_CONTENT_MARGIN 10.0f

- (CGFloat)heightForContent:(NSString *)text;
{
	CGSize constraint = CGSizeMake(CELL_CONTENT_WIDTH, 20000.0f);
	CGSize size = [text sizeWithFont:[UIFont systemFontOfSize:FONT_SIZE] constrainedToSize:constraint lineBreakMode:UILineBreakModeWordWrap];
	return size.height;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
	LogNote * note = [self.fullResponse.notes objectAtIndex:indexPath.row];
	CGFloat height = [self heightForContent:note.content];
	return height + 50;
}



#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.fullResponse.notes count];
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
	static NSString *CellIdentifier = @"NoteTableViewCell";
    
    NoteTableViewCell* cell = (NoteTableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];

    if (cell == nil) {
		//        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
		[[NSBundle mainBundle] loadNibNamed:@"NoteTableViewCell" owner:self options:nil];
		cell = self.tempCell;
		self.tempCell = nil;
	}
		
    
    // Set up the cell...
	
	LogNote * note = [self.fullResponse.notes objectAtIndex:indexPath.row];
	
	cell.user.text = note.user;
	cell.date.text = note.date;
	cell.title.text = note.title;
	cell.content.text = note.content;
	
	[cell.content setLineBreakMode:UILineBreakModeWordWrap];
	[cell.content setMinimumFontSize:FONT_SIZE];
	[cell.content setNumberOfLines:0];
	[cell.content setFont:[UIFont systemFontOfSize:FONT_SIZE]];
	
	CGRect rect = cell.content.frame;
	rect.size.width = CELL_CONTENT_WIDTH;
	rect.size.height = [self heightForContent:cell.content.text];
	cell.content.frame = rect;
	
    return cell;
}

- (void)addNote {
	AddNoteViewController * addNoteVC = [[[AddNoteViewController alloc] init] autorelease];
	addNoteVC.responsesEntryHolder = self.responsesEntryHolder;
	[self.sharedNavController pushViewController:addNoteVC animated:YES];
}

- (void)dealloc {
    [super dealloc];
}


@end

