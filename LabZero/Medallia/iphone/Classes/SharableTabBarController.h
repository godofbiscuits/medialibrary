
#import <Foundation/Foundation.h>

//: A TabBarController which multiplxes the navigationItem (see UIViewControllerShared)

@interface SharableTabBarController : UITabBarController <UITabBarControllerDelegate> {
}
- (void) updateNavItems:(BOOL)animated;
@end
