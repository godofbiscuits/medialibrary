#import "Gen.h"


#import <UIKit/UIKit.h>

@protocol DropDelegate
- (void)dropdownDidSelect:(NSString*)object forContext:(id)ctx;
@end

@interface SimpleDropdown : UITableViewController {
  ivars_SimpleDropdown;
}
- (id)initWithList:(NSArray*)names keys:(NSArray*)keys callbackTo:(id<DropDelegate>)delegate withContext:(id)ctx;
@property(copy) NSString *oldSelection; // previous selection (shows a checkmark)
@property(copy) NSString *title; // for navigation bar
@property(retain) id<DropDelegate> delegate;
@property(retain) id context;
@property(retain) NSArray *keys; // list of keys (returned when a selection is made; also used for oldSelection)
@property(retain) NSArray *names; // list of strings displayed
@property UITableViewCellAccessoryType accessory;
@end
