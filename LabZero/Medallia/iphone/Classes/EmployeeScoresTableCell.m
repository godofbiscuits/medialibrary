//
//  EmployeeScoresTableCell.m
//  Medallia
//
//  Created by Jeff Barbose on 3/5/10.
//  Copyright 2010 Medallia, Inc. All rights reserved.
//

#import "EmployeeScoresTableCell.h"


@implementation EmployeeScoresTableCell

@synthesize titleLabel;
@synthesize valueBackgroundImageView;
@synthesize valueLabel;



- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier 
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) 
    {
        // Initialization code
    }
    return self;
}


- (void) prepareForReuse
{
    [super prepareForReuse];
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    self.titleLabel.text = NSLocalizedString( @"Parameter Title", @"" );
    self.valueLabel.text = @"0.0";
    self.valueLabel.backgroundColor = [UIColor clearColor];
    self.valueLabel.textColor = [UIColor blackColor];
}





- (void)dealloc 
{
    [titleLabel release];
    [valueBackgroundImageView release];
    [valueLabel release];
    
    [super dealloc];
}


@end
