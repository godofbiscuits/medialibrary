//
//  AltDelegateConnector.m
//  Medallia
//
//  Created by Øyvind Grotmol on 18.03.10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "AltDelegateConnector.h"


@implementation AltDelegateConnector

@synthesize instance, target, targetPos;

- (id)initForInstance:(WidgetInstance*)inst withTarget:(id<SavesWidget>)aTarget {
	self = [super init];
	self.instance = inst;
	self.target = aTarget;
	self.targetPos = -1;
	return self;
}

// callback from dropdown
- (void)alternativeWasSelected:(Alt*)sel forDropdown:(AltTableDropdown*)dropdown {
    [self.instance.arguments setObject:sel.key forKey:dropdown.configPart.key];
	[self.target widgetSaved:self.instance position:self.targetPos];
}


@end
