
#import <Foundation/Foundation.h>

//: We nest some controller Foo inside a TabBarController inside a NavigationController. The innermost controller
//: needs access to the NavigationController in order to show more controllers.

@interface UIViewController (Shared)
- (UIViewController*)sharedController;
- (UINavigationController*)sharedNavController;
@end
