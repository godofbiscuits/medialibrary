//
//  NoteTableViewCell.h
//  Medallia
//
//  Created by Øyvind Grotmol on 03.03.10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface /*NOGENERATE*/ NoteTableViewCell : UITableViewCell {
	
	IBOutlet UILabel * date;
	IBOutlet UILabel * user;
	IBOutlet UILabel * title;
	IBOutlet UILabel * content;

}

@property (retain) UILabel * user;
@property (retain) UILabel * title;
@property (retain) UILabel * date;
@property (retain) UILabel * content;

@end
