//
//  GraphThemeBase.m
//  Medallia
//
//  Created by Øyvind Grotmol on 15.03.10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "GraphThemeBase.h"

#import "CPXYGraph.h"
#import "CPColor.h"
#import "CPGradient.h"
#import "CPFill.h"
#import "CPPlotArea.h"
#import "CPXYPlotSpace.h"
#import "CPUtilities.h"
#import "CPXYAxisSet.h"
#import "CPXYAxis.h"
#import "CPLineStyle.h"
#import "CPTextStyle.h"
#import "CPBorderedLayer.h"
#import "CPExceptions.h"

#pragma mark -


@implementation GraphThemeBase

-(void)applyThemeToAxisSet:(CPXYAxisSet *)axisSet {
    CPLineStyle* axisLineStyle = [CPLineStyle lineStyle];
    axisLineStyle.lineColor = [CPColor colorWithGenericGray:0.2];
    axisLineStyle.lineWidth = 1.0f;
	
    CPLineStyle* gridLineStyle = [CPLineStyle lineStyle];
    gridLineStyle.lineColor = [CPColor colorWithGenericGray:0.2];
    gridLineStyle.lineWidth = 1.0f;
	
    CPTextStyle* labelTextStyle = [CPTextStyle textStyle];
    labelTextStyle.color = [CPColor whiteColor];
	
    // customize axis sets
    CPXYAxis* x = axisSet.xAxis;
    CPXYAxis* y = axisSet.yAxis;
	
    x.majorTickLineStyle = axisLineStyle;
    x.majorTickLength = 1.0f;
    x.titleOffset = 3.0f;
	
    //y.preferredNumberOfMajorTicks = 5;
    x.preferredNumberOfMajorTicks = 0;
    x.labelingPolicy = CPAxisLabelingPolicyNone;
    y.labelingPolicy = CPAxisLabelingPolicyNone;
	
    y.majorTickLineStyle = axisLineStyle;
    y.majorTickLength = 1.0f;
    y.minorTickLength = 0.0f;
    //y.titleOffset = 3.0f;
	
    // need to set this so axes draw
    x.axisLineStyle = axisLineStyle;
    y.axisLineStyle = axisLineStyle;
    x.labelTextStyle = labelTextStyle;
    y.labelTextStyle = labelTextStyle;
    x.majorGridLineStyle = gridLineStyle;
    y.majorGridLineStyle = gridLineStyle;
}

-(void)applyThemeToBackground:(CPXYGraph *)graph
{
//	CPColor *endColor = [CPColor colorWithGenericGray:0.1];
//	CPGradient *graphGradient = [CPGradient gradientWithBeginningColor:endColor endingColor:endColor];
//	graphGradient = [graphGradient addColorStop:[CPColor colorWithGenericGray:0.2] atPosition:0.3];
//	graphGradient = [graphGradient addColorStop:[CPColor colorWithGenericGray:0.3] atPosition:0.5];
//	graphGradient = [graphGradient addColorStop:[CPColor colorWithGenericGray:0.2] atPosition:0.6];
//	graphGradient.angle = 90.0;
//	graph.fill = [CPFill fillWithGradient:graphGradient];
	
	//    CPGradient *stocksBackgroundGradient = [[[CPGradient alloc] init] autorelease];
	//    stocksBackgroundGradient = [stocksBackgroundGradient addColorStop:[CPColor colorWithComponentRed:0.21569 green:0.28627 blue:0.44706 alpha:1.0] atPosition:0.0];
	//	stocksBackgroundGradient = [stocksBackgroundGradient addColorStop:[CPColor colorWithComponentRed:0.09412 green:0.17255 blue:0.36078 alpha:1.0] atPosition:0.5];
	//	stocksBackgroundGradient = [stocksBackgroundGradient addColorStop:[CPColor colorWithComponentRed:0.05882 green:0.13333 blue:0.33333 alpha:1.0] atPosition:0.5];
	//	stocksBackgroundGradient = [stocksBackgroundGradient addColorStop:[CPColor colorWithComponentRed:0.05882 green:0.13333 blue:0.33333 alpha:1.0] atPosition:1.0];
	//    stocksBackgroundGradient.angle = 270.0;
	//	graph.fill = [CPFill fillWithGradient:stocksBackgroundGradient];
	//	
	//	CPLineStyle *borderLineStyle = [CPLineStyle lineStyle];
	//	borderLineStyle.lineColor = [CPColor colorWithGenericGray:0.2];
	//	borderLineStyle.lineWidth = 0.0;
	//	
	//	graph.borderLineStyle = borderLineStyle;
	//	graph.cornerRadius = 9.0;
}

-(void)applyThemeToPlotArea:(CPPlotArea *)plotArea 
{
	// Dark theme:
	//	CPGradient *gradient = [CPGradient gradientWithBeginningColor:[CPColor colorWithGenericGray:0.1] endingColor:[CPColor colorWithGenericGray:0.3]];
	//    gradient.angle = 90.0;
	//	plotArea.fill = [CPFill fillWithGradient:gradient]; 
	//	
	//	CPLineStyle *borderLineStyle = [CPLineStyle lineStyle];
	//	borderLineStyle.lineColor = [CPColor colorWithGenericGray:0.2];
	//	borderLineStyle.lineWidth = 4.0;
	//	
	//	plotArea.borderLineStyle = borderLineStyle;
	//	plotArea.cornerRadius = 10.0;
	//    
	//    plotArea.masksToBorder = YES;
	
	//	CPColor *endColor = [CPColor colorWithGenericGray:0.1];
	//	CPGradient *graphGradient = [CPGradient gradientWithBeginningColor:endColor endingColor:endColor];
	//	graphGradient = [graphGradient addColorStop:[CPColor colorWithGenericGray:0.2] atPosition:0.3];
	//	graphGradient = [graphGradient addColorStop:[CPColor colorWithGenericGray:0.3] atPosition:0.5];
	//	graphGradient = [graphGradient addColorStop:[CPColor colorWithGenericGray:0.2] atPosition:0.6];
	//	graphGradient.angle = 90.0;
	//	graph.fill = [CPFill fillWithGradient:graphGradient];
}


@end
