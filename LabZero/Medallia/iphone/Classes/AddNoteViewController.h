//
//  AddNoteViewController.h
//  Medallia
//
//  Created by Øyvind Grotmol on 04.03.10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SimpleRequest.h"
#import "ResponsesEntryHolder.h"

@interface /*NOGENERATE*/ AddNoteViewController : UIViewController <SRDelegate, UIAlertViewDelegate> {

	UITextView *textView;

	ResponsesEntryHolder * responsesEntryHolder;

	UIActivityIndicatorView *spinner;

}

@property (nonatomic, retain) ResponsesEntryHolder * responsesEntryHolder;
@property (assign, nonatomic) UITextView * textView;

@end
