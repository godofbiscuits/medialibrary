//
//  PhoneCallInfo.m
//  Medallia
//
//  Created by Øyvind Grotmol on 05.03.10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "PhoneCallInfo.h"


#define S_DID_CALL @"M.phone.didCall"
#define S_CALL_INITIATED @"M.phone.callInitiated"
#define S_CUSTOMER_NAME @"M.phone.customerName"
#define S_DETAILS_URL @"M.phone.detailsURL"
#define S_ALERT_ACTION_ID @"M.phone.alert_action_id"
#define S_ALERT_ACTION_NAME @"M.phone.alert_action_name"

@implementation PhoneCallInfo

@synthesize didCall;

-(NSString*)navTitle {
	return @"Call Log";
}

-(NSString*)nameLabel {
	return @"Name";
}

-(NSString*)statusLabel {
	return @"Call initiated";
}



+ (PhoneCallInfo*) load
{
	NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
	PhoneCallInfo* phone = [[[PhoneCallInfo alloc] init] autorelease];
	phone.didCall = [defaults boolForKey:S_DID_CALL];
//	phone.callInitiated = [NSDate dateWithString:[defaults objectForKey:S_CALL_INITIATED]];
	phone.callInitiated = [defaults objectForKey:S_CALL_INITIATED];
	phone.customerName = [defaults stringForKey:S_CUSTOMER_NAME];
	phone.detailsURL = [defaults stringForKey:S_DETAILS_URL];
	phone.alert_action_id = [defaults stringForKey:S_ALERT_ACTION_ID];
	phone.alert_action_name = [defaults stringForKey:S_ALERT_ACTION_NAME];
	
	NSLog(@"Loaded PhoneCallInfo %@", self);
    
	[phone retain];
	return phone;
}


+ (void) didLogCall
{
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:S_DID_CALL];
}


- (void) store
{
	NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
	[defaults setBool:self.didCall forKey:S_DID_CALL];
	[defaults setObject:self.callInitiated forKey:S_CALL_INITIATED];
	[defaults setObject:self.customerName forKey:S_CUSTOMER_NAME];
	[defaults setObject:self.detailsURL forKey:S_DETAILS_URL];
	[defaults setObject:self.alert_action_id forKey:S_ALERT_ACTION_ID];
	[defaults setObject:self.alert_action_name forKey:S_ALERT_ACTION_NAME];
	[defaults synchronize];
}

- (NSString *)description {
	return [NSString stringWithFormat:@"didCall=%@ callInitiated=%@ customerName=%@ detailsURL=%@ alert_action_id=%@ alert_action_name=%@", didCall ? @"YES" : @"NO", callInitiated, customerName, detailsURL, alert_action_id, alert_action_name];
}

@end
