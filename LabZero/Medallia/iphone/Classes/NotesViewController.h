//
//  NotesViewController.h
//  Medallia
//
//  Created by Øyvind Grotmol on 02.03.10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ResponsesEntryHolder;
@class NoteTableViewCell;
@class SingleResponse;

@interface /*NOGENERATE*/ NotesViewController : UITableViewController {

	IBOutlet NoteTableViewCell * tempCell;
	
	ResponsesEntryHolder * responsesEntryHolder;
	
}

- (id)init:(ResponsesEntryHolder*)response;

@property (nonatomic, assign) IBOutlet NoteTableViewCell * tempCell;

@property (nonatomic, retain) ResponsesEntryHolder * responsesEntryHolder;

@property (nonatomic, retain, readonly) SingleResponse * fullResponse;

@end
