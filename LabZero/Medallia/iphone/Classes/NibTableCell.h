#import <Foundation/Foundation.h>

// Subclass this and return the name of your nib file from nibName.

@interface NibTableCell : UITableViewCell {
}
+ (id)possiblyCachedForTableView:(UITableView*)tv;
+ (NSString*)nibName;
@end
