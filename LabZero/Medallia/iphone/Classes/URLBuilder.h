//
//  URLBuilder.h
//  Medallia
//
//  Created by Øyvind Grotmol on 17.03.10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface URLBuilder : NSObject {
	
	NSMutableString * partialURL;

}

+(URLBuilder*)createBuilder:(NSString*)baseURL;

-(void)appendKey:(NSString*)key withValue:(NSString*)value;

-(NSURL*)getURL;

@end
