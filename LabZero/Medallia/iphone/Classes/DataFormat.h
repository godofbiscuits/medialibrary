#import "Gen.h"
#import <Foundation/Foundation.h>
#import "LoadableData.h"
#import "ResponsesEntryProtocol.h"
#import "URLBuilder.h"

#define CURRENT_CLIENT_VERSION 60 // increase this whenever the data model changes; please note that fact in the commit message as well
#define FIRST_COMPATIBLE_CLIENT_VERSION 50 // first client version that can read this version; if the changes are incompatible, set this to the above value


// when making changes, remember that not all servers are upgraded at the same time; an old server may well be used with a newer client


/*
 This file defines the data format used when interacting with the server.
 
 GeneratedDataModel.java is generated from this, and may be more readable.
 
 The root classes (as of v42) are
    ConfigFile -- the root file returned at login
    ResponsesLump -- response investigator list
    SWPage -- used by the score widget
 
 Whenever changes are made to this file, run generate.pl and convert.pl to regenerate Gen.h and the Java files.

*/

// This lists the types for which we will generate specialized xx_Dict and xx_List classes. See LoadableType.h
#define ForConfigTypes(t) \
  t(WidgetType) t(ConfigSection) t(ConfigPart) t(NSNumber) \
  t(ResponsesEntry) t(FilterButton) t(AltTable) t(Alt) t(WidgetInstance) t(AltSection) t(SWLine) \
  t(RankerUnit) t(TimePeriod) t(Graph) t(GraphLabel) t(LogNote) t(AlertAction) t(ScorecardScore) \
  t(LineStyle) t(LineData)

ForConfigTypes(_ForwardDeclare)

@class WidgetType;
@class WidgetInstance;
@class AltTable;
@class Alt;
@class EmailAction;
@class SingleResponse;

// LOGIN ACTION
// req = login
// c            company
// p            password
// u            username
// version      version of iPhone code
// RETURNS: ConfigFile
@interface ConfigFile : Loadable { // This is the type of the main file sent to the client upon login.
   ivars_ConfigFile;
}
@property(retain) PlistReq AltTable_List *tables; // The (shared) lists of values such as property names, age groups, etc (alternative sets)
@property(copy) PlistReq NSString *userKey; // Some global user identifier -- used to keep different users' settings separate
@property(retain) PlistReq WidgetType_List *scoresWidgetTypes; // All the possible widget types the user may install on the scores dashboard (scoreboard)
@property(retain) PlistReq WidgetType_List *rankerWidgetTypes; // All the possible widget types the user may install on the ranker
@property(retain) PlistReq WidgetInstance_List *initialScoresWidgets; // The initial list of scores widgets (installed if the user has no widgets)
@property(retain) PlistReq WidgetInstance_List *initialRankerWidgets; // The initial list of ranker widgets (installed if the user has no widgets)
@property(retain) PlistReq WidgetType *responsesFilterConfig; // Config used to set filters for the Response Investigator
@property(retain) PlistReq WidgetInstance *initialResponseFilter; // Default values for the RI filter
@property(copy) PlistReq NSString *nrOpenAlerts; // Number of open alerts

// lookup a widget type by name (used by WidgetInstance)
- (WidgetType*)widgetTypeForKey:(NSString*)s;
- (AltTable*)altTableForKey:(NSString*)s;
@end

@interface WidgetType : Loadable { // a WidgetType represents a single type of dashboard the user can install, e.g. Scoreboard, Response Investigator, Ranker
   ivars_WidgetType;
}
@property(copy) PlistReq NSString *name; // "Scoreboard"
@property(copy) PlistOpt NSString *desc; // "Overview of foo, feh and fiddle"
@property(copy) PlistOpt NSString *key; // "score"
@property(copy) PlistReq NSString *viewClass; // "ScoreWidget" (class name)
@property(copy) PlistReq NSString *urlBase; // base URL for fetching data
@property(copy) PlistOpt NSString *responsesFilterButtonKey; // key for the responses filter button to view when clicking through to see underlying responses
@property(retain) PlistReq ConfigSection_List *configSections; // List of sections, each containing one or more edit controls
@property(retain) PlistOpt NSMutableDictionary *arguments; // Optional list of arguments for the widget type (parameter key and default value). Used for parameters that are hardcoded to the WidgetType and not part of the Config (i.e. a ConfigPartType), for example ranker scope.

// Creates a new WidgetInstance with type set to this WidgetType
- (WidgetInstance*)instance;
@end

@interface ConfigSection : Loadable { // Settings pages are divided into sections
   ivars_ConfigSection;
}
@property(copy) PlistReq NSString *name; // Section header
@property(retain) PlistReq ConfigPart_List *parts; // List of parts
@end


// An enum for the config parts
#define ConfigPartTypeDef(def,and) def(CEDummy) and def(CEDropdown) and def(CECheckbox) and def(CERange)
DeclareEnum(ConfigPartType)

// An enum for the report header elements
#define ClickableHeaderElementDef(def,and) def(CHNone) and def(CHUnitScope) and def(CHCustomerSegment) and def(CHTimePeriod) and def(CHScoreSelector)
DeclareEnum(ClickableHeaderElement)

@interface ConfigPart : Loadable { // One table cell on the settings page. Can be a CECheckbox, CEDropdown or CERange (the double slider).
   ivars_ConfigPart;
}
@property PlistManual ConfigPartType type; // the type of control
@property PlistManual ClickableHeaderElement headerElement; // which clickable header element this configpart is associated with
@property(copy) PlistReq NSString *key; // "ev_filter_foo" (the server's name for this field)
@property(copy) PlistOpt NSString *name; // name shown ("Property")
@property(copy) PlistOpt NSString *def; // default value
@property(copy) PlistOpt NSString *table; // for CEDropdown, the name of the AltTable to use
@property(copy) PlistOpt NSNumber *minValue; // for CERange
@property(copy) PlistOpt NSNumber *maxValue; // for CERange

// text string for describing this
- (NSString*)descriptionEntry:(WidgetInstance*)inst;
@end


// RESPONSES ACTION
// req = responses
// c                   company
// p                   password
// u                   username
// tp                  time period id
// unitid              if set, only show surveys for this unit/unitgroup id ("all" or missing for all units)
// satrange            if set, only show surveys with "overall satisfaction" within this range (e.g. "8.000000:10.000000")
// alerts_only         if set, only show surveys with alerts
// comments_only       if set, only show surveys with comments
// offset              if set, offset on Responses list view (skip the first 'offset' responses)
// response_text       if set, only show surveys which match this text
// button_filter       if set, key of filter to apply (may differ per client, used by apple as a program (unit group) selector)
// preview_line_count  client-side only: number of comment lines to show
// preview_lines       client-side only: ? unsure what this is used for since we have the above
// vs.lastid           client-side only: most recent survey id (to calculate new count)
// preview_font_size   client-side only: name/comment font size
// RETURNS: ResponsesLump
@interface ResponsesLump : Loadable { // A batch of responses
	ivars_ResponsesLump
}
@property(copy) PlistReq NSString *fetchMoreText; // The text shown at the end of the list
@property(copy) PlistOpt NSString *fetchMoreURL; // If set, the URL to use to fetch the next ResponsesLump. If null, the button will open the config page instead.
@property(retain) PlistReq ResponsesEntry_List *entries; // The list of surveys.
@property(retain) PlistReq FilterButton_List *filterButtons; // List of filter buttons shown under the search bar.
@property(copy) PlistOpt NSString *nrOpenAlerts; // Number of open alerts (optional -- only sent when alerts_only is set)
@end

@interface ResponsesEntry : Loadable <ResponsesEntryProtocol> { // One entry in the list of responses. Contains only the data needed for the list.
   ivars_ResponsesEntry;
}
@property(copy) PlistReq NSString *uniqueIdentifier; // Some persistent identifier for this record
@property(copy) PlistReq NSString *sectionHeader; // Adjacent records with the same header will be grouped together. At the moment we use the survey date here; that could change.
@property(copy) PlistReq NSString *score; // The main score for this survey, e.g. "7"
@property(copy) PlistOpt NSString *alert; // The name of the alert, if there is an alert for this survey. E.g. "R1"
@property(copy) PlistReq NSString *name; // The header to show for each response, typically the customer name, e.g. "John Doe"
@property(copy) PlistOpt NSString *subheader; // An optional sub-header to show under the main header, above the comment. E.g. "Palo Alto Store"
@property(copy) PlistOpt NSString *text; // The comment of the survey, e.g. "My iPod didn't have a Centronics connector. Boo!"
@property(copy) PlistReq NSString *detailsURL; // URL of details page; that page should return an HTML page
@property(retain) PlistReq UIColor *scoreColor; // Color for the score background; the text is white, so use a darkish shade.
@property(retain) SingleResponse *fullResponse; // The ResponsesEntry may have an associated SingleResponse.
@end

@interface FilterButton : Loadable { // A filter button
	ivars_FilterButton;
}
@property(copy) PlistReq NSString *key; // Key used by server to filter results.
@property(copy) PlistReq NSString *title; // Text shown on button.
@end

// RANKER ACTION
// req = ranker
// c                   company
// p                   password
// u                   username
// tp                  time period id
// unitid              If set, only include units/unitgroups that are members of this unit group. This will never be a unit. Use "all" or exclude parameter for all units.
// scope               Unit group parent
// RETURNS: RankerData
@interface RankerData : Loadable { // A list of units/unit groups
	ivars_RankerData;
}
@property(copy) PlistReq NSString *benchTitle; // "vs 6m" - explains the benchmark period we are comparing to
@property(retain) PlistReq RankerUnit_List *entries; // The list of units/unit groups
@end

@interface RankerUnit : Loadable { // One entry in the list of units/unit groups
    ivars_RankerUnit;
}
@property(copy) PlistReq NSString *uniqueIdentifier; // Some persistent identifier for this record
@property(copy) PlistReq NSString *name; // "Palo Alto"
@property(copy) PlistReq NSString *score; // "58.7", this is the number that is actually displayed on the client
@property(copy) PlistReq NSString *difference; // "+0.2", this is the number that is actually displayed on the client
@property(copy) PlistReq NSNumber *scoreNumeric; // Numeric version of the score. Used for sorting on the client.
@property(copy) PlistReq NSNumber *differenceNumeric; // Numeric version of the difference. Used for sorting on the client.
@property(retain) PlistReq UIColor *diffColor; // The color used for the "+0.2" text
@property(copy) PlistOpt NSString *selected; // missing if unit is not selected
@end


@interface AltTable : Loadable { // One table of alternatives (used by ConfigParts of type CEDropdown)
   ivars_AltTable;
}
@property(copy) PlistReq NSString *key; // Key (used to reference this from ConfigPart.table())
@property(retain) PlistReq AltSection_List *sections; // List of sections; if you don't want the alternatives grouped, use a single section with no header.
- (NSString*)niceNameFor:(NSString*)key;
- (Alt*) findAltFor:(NSString*)key;
@end

@interface AltSection : Loadable { // Alternatives may be shown in groups
	ivars_AltSection;
}
@property(copy) PlistOpt NSString *header; // Group header
@property(retain) PlistReq Alt_List *alternatives; // List of alternatives in the group
@end

@interface Alt : Loadable { // A single alternative
   ivars_Alt;
}
@property(copy) PlistReq NSString *key; // "42" - must be unique within an AltTable
@property(copy) PlistReq NSString *value; // "Quarter to Date" - full text, to be displayed in the table when making selection
@property(copy) PlistOpt NSNumber *level; // Indentation level. 0 is left-aligned, 1 is slightly indented, 2 is more indented, etc.
@property(copy) PlistOpt NSString *headerValue; // "QTD" - an alternative text to be displayed in report header
@property(retain) PlistOpt AltTable *table; // Optional link to new alternative table. Used if this alternative links to a new screen of alternatives.
-(BOOL) hasAlternatives;
@end

#define VIEWSTATE_SUBPAGE "vs.subpage" // client-side only: last active scoreboard page
#define VIEWSTATE_LAST_SURVEYID "vs.lastid" // client-side only: most recent survey id (to calculate new count)

// some magic settings names:
#define PREVIEW_FONT_SIZE "preview_font_size" // this setting appears as a regular ConfigPart, but is only used by the client
#define PREVIEW_FONT_SIZE_DEFAULT 13
#define PREVIEW_LINE_COUNT "preview_line_count"
#define PREVIEW_LINE_COUNT_DEFAULT 3
#define COMMENTS_ONLY_KEY "comments_only"

#define RESPONSES_TEXT_SEARCH_PARAMETER "response_text" // this setting is provided by the client as part of the request for a response list; it is *not* specified as a ConfigPart

@interface WidgetInstance : Loadable <NSMutableCopying> { // An instance of a Widget -- the user may have three instances of the scoreboard, for, uhm, instance.
   ivars_WidgetInstance;
}
@property(copy) PlistReq NSString *type; // The type of widget; this should match the key() of a WidgetType
@property(retain) PlistReq NSMutableDictionary *arguments; // Arguments; this is a map from ConfigPart.key() to values
@property(retain) id currentData; // Server-provided data. Not persisted.
- (WidgetType*) widgetType;
- (NSString*) description; // Generated from the config automatically (basically a list of dropdown values)
- initWithType:(WidgetType*)type;
- (void)resetDefaults;
- (URLBuilder*)makeURLBuilder;
- (NSURL*)makeURL;
@end


// SCOREBOARD ACTION
// req = scores[.*]    (e.g. just "scores" or "scores.RZ" or "scores.FR" to distinguish between different scoreboards)
// c                   company
// p                   password
// u                   username
// unitid              if set, only include surveys for this unit/unitgroup id ("all" or missing for all units)
// tp                  time period id
// RETURNS: SWPage
@interface SWPage : Loadable { ivars_SWPage; } // Each scoreboard widget has one page
@property(copy) PlistOpt NSString *title; // The main title for this widget (defaults to widgetType.name if not present)
@property(copy) PlistOpt NSString *subTitle; // The subtitle (defaults to a description of the current filters if not present)
@property(copy) PlistReq NSString *benchTitle; // "vs 6m" - explains the benchmark period we are comparing to
@property(retain) PlistReq TimePeriod_List *timePeriods; // List of time periods. There will be as many of these as graphs for each line.
@property(retain) PlistReq LineStyle_List *lineStyles; // List of line styles. There will be as many of these as lines in each graph.
@property(retain) PlistReq SWLine_List *lines; // The page contents
@end

@interface SWLine : Loadable { ivars_SWLine; } // Represents a single line of the Scoreboard Widget
@property(copy) PlistReq NSString *tableCellNibName; // for now, this must be ScoreWidgetNum (the only nib supported at the moment)
@property(copy) PlistReq NSString *title; // "Responses" - the text shown in the large font
@property(copy) PlistReq NSString *value; // "8.4" - the main number, it's a string so that it can be customized and formatted on the server side
@property(copy) PlistOpt NSString *difference; // For ScoreWidgetNum only, the "+4.7" difference against the benchmark
@property(copy) PlistOpt NSString *scoreBackgroundColor; // For ScoreWidgetNum only, the color for the number background; "green"/"yellow"/"red" for now
@property(retain) PlistOpt UIColor *diffColor; // For ScoreWidgetNum only, the color used for the "+4.7" text; white or red in the mockups
@property(retain) PlistOpt Graph_List *graphSmall; // For ScoreWidgetNum only; small version of graph for each time period. There will be as many of these as time periods.
@property(retain) PlistOpt Graph_List *graphLarge; // For ScoreWidgetNum only; large version of graph for each time period. There will be as many of these as time periods.
@end

@interface TimePeriod : Loadable { ivars_TimePeriod; } // Time period.
@property(copy) PlistReq NSString *title; // Short time period title ("1w", "1m", "3m", etc).
@property(retain) PlistOpt GraphLabel_List *labelsSmallGraph; // List of graph x labels for the small graph. (These will be identical for all small graphs and are thus stored outside the graph object.)
@property(retain) PlistOpt GraphLabel_List *labelsLargeGraph; // List of graph x labels for the large graph. (These will be identical for all large graphs and are thus stored outside the graph object.)
@end

@interface LineStyle : Loadable { ivars_LineStyle; } // Line style.
@property(copy) PlistOpt NSString *featured; // Missing if line is not featured.
@property(copy) PlistReq NSString *legendText; // Text describing this line that may be shown in a legend.
@property(retain) PlistReq UIColor *color; // Color of line.
@end

@interface GraphLabel : Loadable { ivars_GraphLabel; } // Graph x label.
@property(copy) PlistReq NSString *label; // Graph x label.
@property(copy) PlistReq NSNumber *position; // Label x position, ordered and ranged between 0 and 1 (inclusive).
@end

@interface Graph : Loadable { ivars_Graph; } // A graph. Contains data for a list of lines.
@property(copy) PlistOpt NSString *minY; // Suggested graph y range minimum (will be overridden if data lower than this appear).
@property(copy) PlistOpt NSString *maxY; // Suggested graph y range maximum (will be overridden if data higher than this appear).
@property(retain) PlistReq LineData_List *lineData; // Data for a list of lines. There will be as many lines as line styles defined in the SWPage.
@end

@interface LineData : Loadable { ivars_LineData; } // Line data. Contains a list of data points for a single line.
@property(copy) PlistReq NSString *xData; // List of pipe-separated x values for the line, ordered and ranged between 0 and 1 (inclusive). May be empty if there is no data. Leading zeros are omitted, e.g. '0.25' is written as '.25', but a single '0' is still written as '0'. Each x point has a corresponding y point.
@property(copy) PlistReq NSString *yData; // List of pipe-separated y values for the line. May be empty if there is no data. Need not conform to the range given by [minY, maxY]. Leading zeros are omitted, e.g. '0.25' is written as '.25', but a single '0' is still written as '0'. Each y point has a corresponding x point.
@end


// SCORECARD ACTION
// req = scorecard
// c                   company
// p                   password
// u                   username
// response_text       only include surveys matching this text
// unitid              if set, only include surveys for this unit/unitgroup id ("all" or missing for all units)
// scorecard_name      the name of the scorecard
// RETURNS: Scorecard
@interface Scorecard : Loadable { ivars_Scorecard; } // Scorecard. Contains a summary of scores for a set of surveys matching a given search string, e.g. the name of an employee.
@property(copy) PlistReq NSString *scorecardName; // The name of the scorecard (who/what the scorecard is for).
@property(copy) PlistReq NSNumber *numberOfResponses; // The number of responses over the past 12 months.
@property(retain) PlistReq ScorecardScore_List *scores; // The list of scores for the past 12 months.
@property(retain) PlistReq ResponsesLump *responsesLump; // The lump of responses for this scorecard.
@end

@interface ScorecardScore : Loadable { ivars_ScorecardScore; } // An individual score shown on the scorecard.
@property(copy) PlistReq NSString *title; // Score title, e.g. "Overall satisfaction".
@property(copy) PlistReq NSString *value; // Score, e.g. "8.4". It's a string so that it can be customized and formatted on the server side.
@property(copy) PlistOpt NSString *scoreBackgroundColor; // The color for the score background; "green"/"yellow"/"red" for now
@end


// RESPONSE DETAIL ACTION
// req = details
// c                   company
// p                   password
// u                   username
// id                  survey id
// alert_action        if set, what action to take with alert, i.e. "closed", "resolved", or "in_progress"
// note_content        if set, content of log note that should be added
// call_initiated      if this is a phone call log note, then this should be set to the date-time when the phone call was initiated, in the ISO format "yyyy-MM-dd HH:mm:ss"
// RETURNS: SingleResponse

@interface SingleResponse : Loadable { // A single survey. Contains HTML for rendering in WebKit, and information about actions like adding note, calling customer, forwarding email, etc.
	ivars_SingleResponse;
}
@property(copy) PlistReq NSString *html; // HTML to be rendered by WebKit. This should ideally be self-contained; images are slow on a cell phone.
@property(copy) PlistReq NSString *name; // survey taker name (needed for notes/alerts screen)
@property(copy) PlistOpt NSString *canAddNote; // if present, the user has permission to add notes
@property(copy) PlistOpt NSString *customerPhone; // if present, the user can call the customer
@property(copy) PlistOpt NSString *scoreboardBaseURL; // the base URL to which parameters for the specific selected scoreboard should be appended in order to load it
@property(retain) PlistReq LogNote_List *notes; // log notes
@property(copy) PlistOpt NSString *alertName; // Name of alert, if there is an alert
@property(copy) PlistOpt NSString *alertStatus; // Status of alert, if there is an alert
@property(retain) PlistReq AlertAction_List *alertActions; // Alert actions (e.g. "close alert")
@property(retain) PlistOpt EmailAction *emailAction; // optional email action
@property(copy) PlistOpt NSString *nrOpenAlerts; // Number of open alerts (optional -- only sent after an alert has been closed)
@end

@interface LogNote : Loadable { // A survey log note
	ivars_LogNote;
}
@property(copy) PlistReq NSString *date; // Date note was added, e.g. "January 31, 2010"
@property(copy) PlistReq NSString *user; // User that added the note, e.g. "Ryan Smith" or "System"
@property(copy) PlistReq NSString *title; // Note title, e.g. "Alert Closed"
@property(copy) PlistReq NSString *content; // Note content, e.g. "I spoke to the guest and resolved the issue."
@end

@interface AlertAction : Loadable { // An alert action (e.g. "close alert")
	ivars_AlertAction;
}
@property(copy) PlistReq NSString *text; // Button text
@property(copy) PlistReq NSString *action; // Alert action that will be sent back to the servlet and then forwarded to the api ("closed", "resolved", "in_progress")
@end

@interface EmailAction : Loadable { // Information about an email template to be edited & sent by the user. Typically triggered by a button in the action menu
	ivars_EmailAction;
}
@property(copy) PlistOpt NSString *to; // If set, the to-field will be filled out with this
@property(copy) PlistReq NSString *subject; // Email subject
@property(copy) PlistReq NSString *body; // Email body
@property(copy) PlistOpt NSString *html; // If set, the body will be interpreted as HTML. (boolean - null means no, anything else is yes)
@end

ForConfigTypes(_DeclareType)
