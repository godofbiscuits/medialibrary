#import "ResponseList.h"
#import "UIViewControllerShared.h"
#import "Dummy.h"
#import "DataFormat.h"
#import "WidgetConfig.h"
#import "Metadata.h"
#import "ResponseDetails.h"
#import "CallLogViewController.h"
#import "ClickableHeaderView.h"
#import "URLBuilder.h"
#import "PhoneCallInfo.h"

NSUInteger kClickableHeaderCellSectionAdjustment = 1;
NSInteger kClickableHeaderCellSectionNumber = 0;

@interface /*NOGENERATION*/ ResponseList ()

- (ClickableHeaderView*) clickableHeaderView;


@end


@implementation ResponseList
@synthesize isAlertList = isAlertList_;
@synthesize sectionOffsets = sectionOffsets_;
@synthesize lastLump = lastLump_;
@synthesize entries = entries_;
@synthesize settings = settings_;
@synthesize buttons;
@synthesize alertList;

@synthesize clickableHeadersCellContentView, clickableHeadersCell;
@synthesize haveCommentsButtonView;

// chunking and adding lumps
- (NSInteger)sectionZeroAbeyantOffsetForSection:(NSInteger)s 
{
    NSInteger adjustedSectionNumber = s;
	if (!self.isAlertList) {
		adjustedSectionNumber -= kClickableHeaderCellSectionAdjustment;
	}
    
	if ( adjustedSectionNumber >= self.sectionOffsets.count )
    {
		// This is used for the "rowsInPresumptiveSection" calculation in numberOfRowsInSection
        return self.entries.count;
    }
    NSInteger offset = [[self.sectionOffsets objectAtIndex:adjustedSectionNumber] intValue];
    
	return offset;
}


- (int)offsetForIndexPath:(NSIndexPath*)indexPath 
{
	return ( [self sectionZeroAbeyantOffsetForSection:indexPath.section] + indexPath.row );
}

- (ResponsesEntry*)responseAt:(int)position {
	return (ResponsesEntry*)[self.entries responseAt:position];
}

- (ResponsesEntry*)responseAtIndexPath:(NSIndexPath*)indexPath {
	return [self responseAt:[self offsetForIndexPath:indexPath]];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return ( interfaceOrientation == UIInterfaceOrientationPortrait );
}




- (void)addLump:(ResponsesLump*)lump {
	int position = self.entries.count;
	ResponsesEntry *last = nil;
    
	if ( position > 0 ) 
    {
        last = [self responseAt:(position-1)];
    }
    
	for (ResponsesEntry *e in lump.entries) 
    {
        if (!last || ![e.sectionHeader isEqualToString:last.sectionHeader])
		{
            [self.sectionOffsets addObject:[NSNumber numberWithInt:position]];
        }

		last = e;
		++position;
	}
    
	[self.entries addLump:lump];
	self.lastLump = lump;
	self.tableView.tableFooterView = footer;
	[self.tableView reloadData]; // could animate, but it would be painful
	footerL.text = lump.fetchMoreText;
}

- (void)showLoading {
	self.tableView.tableFooterView = loadingFooter;
}

- (NSString*)prefsKey {
	return [NSString stringWithFormat:@"M.%@.responses", [Metadata current].data.userKey];
}

// call this when you're initializing the alert list
- (id)initWithDefaultSettings:(WidgetInstance*)defaultSettings {
	return [self initWithDefaultSettings:defaultSettings alertList:self];
}

// call this when you're not initializing the alert list, and send a parameter with the alert list (needed to update the open alerts count)
- (id)initWithDefaultSettings:(WidgetInstance*)defaultSettings alertList:(ResponseList*)anAlertList {
	self = [super initWithNibName:@"ResponseList" bundle:nil];
	self.isAlertList = self == anAlertList;
	self.alertList = anAlertList;
	WidgetInstance* w = 0;
	id x = [[NSUserDefaults standardUserDefaults] valueForKey:[self prefsKey]];
	if(x) w = [WidgetInstance fromPlist:x];
	self.settings = w ? w : defaultSettings;
	[self startLoadingFromScratch];
	return self;
}

- (void)dealloc 
{ 
    [sectionOffsets_ release];
	[lastLump_ release];
	[entries_ release];
	[settings_ release];
    
    [clickableHeadersCellContentView release];
    
    [super dealloc];
}

- (BOOL)isCommentsOnly {
	NSString * currentValue = [self.settings.arguments valueForKey: @COMMENTS_ONLY_KEY];
	return currentValue != nil && [currentValue length] > 0;
}

- (void)setHaveCommentsButton
{
	if (![self isAlertList]) {
		[self.clickableHeadersCellContentView setHaveCommentsButtonToOnState:[self isCommentsOnly]];
	}
}


- (void)viewDidLoad 
{
    [super viewDidLoad];
	self.tableView.backgroundColor = [UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:1.0];
	self.tableView.tableFooterView = loadingFooter;
    
	if (!self.isAlertList) {
		clickableHeadersCellContentView = [self clickableHeaderView];
		[self.clickableHeadersCell.contentView addSubview:clickableHeadersCellContentView];
		
		CGRect headersFrame = clickableHeadersCellContentView.frame;
		headersFrame.origin.x = 0.0;
		headersFrame.origin.y = 0.0;
        headersFrame.size.width = self.tableView.frame.size.width;
        
        clickableHeadersCellContentView.frame = headersFrame;
        
        [clickableHeadersCellContentView finalizeLayout];
		
		[clickableHeadersCellContentView adjustForResponsesView];
		
		[self.clickableHeadersCell.contentView bringSubviewToFront:clickableHeadersCellContentView];
	}

    // create 
    /*
    CGRect clickableFrame = self.clickableHeaderView.bounds;
    CGFloat searchBarHeight = searchBar.frame.size.height;
    
    CGRect enclosingFrame = CGRectMake( 0.0, 0.0, self.tableView.frame.size.width, ( clickableFrame.size.height + searchBarHeight ) );
    
    UIView* tableHeaderView = [[[UIView alloc] initWithFrame:enclosingFrame] autorelease];
    [tableHeaderView addSubview:self.clickableHeaderView];
    
    CGRect searchBarFrame = searchBar.bounds;
    searchBarFrame.origin.y = clickableFrame.size.height;
    searchBar.frame = searchBarFrame;
    [tableHeaderView addSubview:searchBar];
    
    self.tableView.tableHeaderView = tableHeaderView;
    */
	
	self.navigationItem.backBarButtonItem = [[[UIBarButtonItem alloc] initWithTitle:@"Back" style:0 target:self action:@selector(backToList)] autorelease];
	
	if (self.isAlertList) {
		self.navigationItem.title = @"Open Alerts";
	} else {
		self.navigationItem.title = @"Responses";
		self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithTitle:@"Filter" style:0 target:self action:@selector(openFilter)] autorelease];
	}
	[self setHaveCommentsButton];
}


- (void)backToList {
	[self.sharedNavController popViewControllerAnimated:YES];
}

- (void)requestSucceeded:(NSData*)data {
	ResponsesLump* lump = [ResponsesLump fromPlist:[data parsePlist]];
	[self addLump: lump];
	
	// update nr of open alerts badge value
	if (self.isAlertList && lump.nrOpenAlerts)
		self.tabBarItem.badgeValue = lump.nrOpenAlerts;

	[self.clickableHeadersCellContentView setButtonLabels];
	[self setHaveCommentsButton];

	// update filter buttons
	if ([lump.filterButtons count] > 0) {
		self.buttons = lump.filterButtons;
		NSMutableArray *titles = [NSMutableArray arrayWithCapacity:[lump.filterButtons count]];
		for (FilterButton *button in lump.filterButtons) {
			[titles addObject:button.title];
		}
		searchBar.scopeButtonTitles = titles;
        
        searchBar.barStyle = UIBarStyleBlackTranslucent;
        
		searchBar.showsScopeBar = YES;
		[searchBar sizeToFit];
        
            // the search bar has changed height because we've shown the scope bar non-transiently, which is non-HIG
            // somewhere deep down it doens't notify anyone it has changed size like a regular view would, so what we
            // have to do is remove the search bar as the header view, then set it back to be the table's header view
            // now that it's the proper height, otherwise the scope bar would be (and was) occluding the top 44px of the table itself
        
        self.tableView.tableHeaderView = nil;           // remove the searchBar
        self.tableView.tableHeaderView = searchBar;     // put it back
        
        
        NSLog ( @"IsAlert:%@", self.isAlertList ? @"YES" : @"NO" );
    }
}


- (void)requestFailed:(NSError*)err 
{
}


- (void)startLoadingFrom:(NSURL*)u {
	[self showLoading];
	[[[SimpleRequest alloc] initStartingRequest:[NSURLRequest requestWithURL:u] for:self dropOld:YES] autorelease];
}
- (void)startLoadingFromScratch 
{
	sectionOffsets_ = [[NSMutableArray alloc]initWithCapacity:1];
	self.lastLump = nil;
	entries_ = [[ResponsesEntryList alloc] init];
	
    [self.tableView reloadData];

	URLBuilder* url;
	if (self.isAlertList) 
    {
		url = [URLBuilder createBuilder:self.settings.widgetType.urlBase];
		[url appendKey:@"alerts_only" withValue:@"selected"];
		[url appendKey:@RESPONSES_TEXT_SEARCH_PARAMETER withValue:searchBar.text];
	} else {
		[self.settings.arguments setValue:searchBar.text forKey:@RESPONSES_TEXT_SEARCH_PARAMETER];
		url = [self.settings makeURLBuilder];
	}
	if (self.buttons) 
    {
            // do this here in case the view gets clicked on before a lump arrives
        if ( searchBar.showsScopeBar )
        {
            self.tableView.tableHeaderView = nil;           // remove the searchBar
            self.tableView.tableHeaderView = searchBar;     // put it back
        }
        
		FilterButton *button = [self.buttons objectAtIndex:searchBar.selectedScopeButtonIndex];
		[url appendKey:@"resp_filter" withValue:button.key];
	}
	[self startLoadingFrom:[url getURL]];
}

- (void)setSelectedScopeButton:(NSString*)key {
	if (self.buttons && key) {
		int index = 0;
		for (FilterButton * button in self.buttons) {
			if ([button.key isEqualToString:key]) {
				searchBar.selectedScopeButtonIndex = index;
			}
			++index;
		}
	}
}

- (void)openFilter {
	WidgetInstance *temp = [[self.settings mutableCopy] autorelease];
	WidgetConfig *wc = [[[WidgetConfig alloc] initForInstance:temp] autorelease];
	wc.target = self;
	wc.targetPos = -1;
	self.navigationItem.backBarButtonItem.title = @"Back";
	[self.sharedNavController pushViewController:wc animated:YES];
}

- (void)saveSettings {
	[[NSUserDefaults standardUserDefaults] setValue:[self.settings toPlist] forKey:[self prefsKey]];
	[[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)widgetSaved:(WidgetInstance*)widget position:(int)i {
	self.settings = widget;
	[self saveSettings];
	[self.tableView reloadData];
	[self.sharedNavController popToViewController:[self sharedController] animated:YES];
	[self startLoadingFromScratch];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
    // Release anything that's not essential, such as cached data
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	int n = ( self.sectionOffsets.count + kClickableHeaderCellSectionAdjustment );
	return n ? n : 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section 
{
    if ( !self.isAlertList && section == kClickableHeaderCellSectionNumber )
    {
        return 1;
    }
    else
    {
            // account for clickable headers being section zero
        //NSInteger section = section - kClickableHeaderCellSectionAdjustment;
        
        // officeForSection does the adjustments for the clickableHeader
        
        NSInteger rowsInSection = [self sectionZeroAbeyantOffsetForSection:section];
        
        NSInteger presumptiveSectionNumber = ( section + 1 );
        NSInteger rowsInPresumptiveSection = [self sectionZeroAbeyantOffsetForSection:presumptiveSectionNumber];
        
        NSInteger calculatedRowsInSection =  rowsInPresumptiveSection - rowsInSection;
        
        return calculatedRowsInSection;
                
        //return [self offsetForSection:section+1] - [self offsetForSection:section];
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if ( !self.isAlertList && section == kClickableHeaderCellSectionNumber )
    {
        return @"";
    }
    else
    {
        int n = [self sectionZeroAbeyantOffsetForSection:section];
        
        if (n < self.entries.count) 
        {
            return [self responseAt:n].sectionHeader;
        }
    }
    
    return nil;
}

- (int)previewLines { int n = [[self.settings.arguments valueForKey:@ PREVIEW_LINE_COUNT] intValue]; return n?n:PREVIEW_LINE_COUNT_DEFAULT; }
- (int)previewFontSize { int n = [[self.settings.arguments valueForKey:@ PREVIEW_FONT_SIZE] intValue]; return n?n:PREVIEW_FONT_SIZE_DEFAULT; }

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath 
{
    //static NSString* ClickableIdentifier = @"ClickableHeadersCellIdentifier";
    
    if ( !self.isAlertList && indexPath.section == kClickableHeaderCellSectionNumber )
    {
        return self.clickableHeadersCell;
    }
    else
    {
        static UIImage* cellBackgroundImage = nil;
        
        ResponsesCell *rc = [ResponsesCell possiblyCachedForTableView:self.tableView];
        
        ResponsesCell* returnCell = [rc replaceWithDataFrom:[self responseAtIndexPath:indexPath]
                              fontSize:self.previewFontSize
                                 lines:self.previewLines];
        
        if ( returnCell.backgroundView == nil )
        {
            if ( cellBackgroundImage == nil )
            {
                UIImage* originalCellBackground = [UIImage imageNamed:@"response_table_cell_bg.png"];
                cellBackgroundImage = [[originalCellBackground stretchableImageWithLeftCapWidth:1.0 topCapHeight:37.0] retain];
            }
            
            
            
            UIImageView* backgroundView = [[UIImageView alloc] initWithImage:cellBackgroundImage];
            backgroundView.contentMode = UIViewContentModeScaleToFill;
            returnCell.backgroundView = backgroundView;
            
            [backgroundView release];
        }
        
        return returnCell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	if (!self.isAlertList && indexPath.section == kClickableHeaderCellSectionNumber) {
		// header row cannot be selected
		return;
	}
	ResponseDetails *sr = [[[ResponseDetails alloc] initWithList:self.entries
														position:[self offsetForIndexPath:indexPath]
													   alertList:self.alertList] autorelease];
	[self.sharedNavController pushViewController:sr animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath 
{
    if ( !self.isAlertList && indexPath.section == kClickableHeaderCellSectionNumber )
    {
        CGFloat rowHeight = self.clickableHeadersCellContentView.frame.size.height;
        return rowHeight;
    }
	else
    {
        CGFloat rowHeight = [ResponsesCell heightWithFontSize:[self previewFontSize] lines:[self previewLines] response:[self responseAtIndexPath:indexPath]];
        return rowHeight;
    }
}

- (void)loadMore {
	if (self.lastLump && self.lastLump.fetchMoreURL)
		[self startLoadingFrom:[NSURL URLWithString:self.lastLump.fetchMoreURL]];
	else {
		if (!self.isAlertList) {
			// The alert list does not have a filter
			[self openFilter];
		}
	}
}

- (void)setShowCancel:(BOOL)v {
	if ([searchBar respondsToSelector:@selector(setShowsCancelButton:animated:)]) {
		[searchBar setShowsCancelButton:v animated:YES];
	} else {
		searchBar.showsCancelButton = v;
	}
}
// when the text search is cancelled, remove the keyboard and reload the (full) results
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar_ {	
	[self setShowCancel:NO];
	searchBar.text = @"";
	[searchBar resignFirstResponder];
	[self startLoadingFromScratch];
}
// show the 'cancel' button as soon as they start typing, to provide a way to remove the keyboard
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar_ {
	[self setShowCancel:YES];
	return YES;
}
// remove the keyboard while loading the results
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar_ {
	[self setShowCancel:YES];
	[searchBar resignFirstResponder];
	[self startLoadingFromScratch];
}

- (void)searchBar:(UISearchBar *)searchBar_ selectedScopeButtonIndexDidChange:(NSInteger)selectedScope {
	for (ConfigSection *sect in self.settings.widgetType.configSections) {
		for (ConfigPart *cp in sect.parts) {
			if (cp.headerElement == CHCustomerSegment) {
				[self.settings.arguments setValue:cp.def forKey:cp.key];
			}
		}
	}
	[self startLoadingFromScratch];
}


- (void) captureCallLogDetails
{
    CallLogViewController* callLogController = [[CallLogViewController alloc] init];
	callLogController.info = [PhoneCallInfo load];
    callLogController.completionController = self;
    [self.navigationController pushViewController:callLogController animated:YES];
    [callLogController release];
}

- (void) showCallLogResponseDetails:(ResponsesEntryList*)entryList
{
    // Display it
    ResponseDetails* responseDetailsController = [[ResponseDetails alloc] initWithList:entryList position:0 alertList:self.alertList];
    
    [self.navigationController pushViewController:responseDetailsController animated:YES];
    
    [responseDetailsController release];
}



- (ClickableHeaderView*) clickableHeaderView
{
    if ( clickableHeadersCellContentView == nil )
    {
        NSDictionary* proxies = [NSDictionary dictionaryWithObject:[UIImage imageNamed:@"widget_filter_bg_bordered.png"] forKey:@"ClickableHeaderBackgroundImage"];
        NSDictionary* options = [NSDictionary dictionaryWithObject:proxies forKey:UINibExternalObjects];
        
        NSArray* topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"ClickableHeaderView" owner:self options:options];
        
        ClickableHeaderView* header = nil;
        UIImage* img = nil;
        
        for ( NSObject* obj in topLevelObjects )
        {
            if ( [obj isMemberOfClass:[ClickableHeaderView class]] )
            {
                header = (ClickableHeaderView*)obj;
            }
            else if ( [obj isMemberOfClass:[UIImage class]] )
            {
                img = (UIImage*)obj;
            }
        }
        
        if ( header != nil )
        {
            clickableHeadersCellContentView = [header retain];
            
            [clickableHeadersCellContentView setBackgroundImage:img];
			
			clickableHeadersCellContentView.delegate = self;
			[clickableHeadersCellContentView setButtonLabels];
			[clickableHeadersCellContentView setBenchmarkTitle:@""];

            
            // view controller specific visual elements
            clickableHeadersCellContentView.haveCommentsElement.hidden = NO;
            clickableHeadersCellContentView.smileyElement.hidden = YES;
            clickableHeadersCellContentView.rankerQuestionLabel.hidden = YES;
            
            
            // hide grooves
            
            clickableHeadersCellContentView.topSideGrooveView.hidden = YES;
            clickableHeadersCellContentView.rightSideGrooveView.hidden = YES;
            
        }
    }
    
    return clickableHeadersCellContentView;
}


- (UINavigationController*)getNavController {
	return self.sharedNavController;
}

- (WidgetInstance*)getWidgetInstance {
	return self.settings;
}

- (void)upperRightCornerPressedAction {
	NSLog(@"upperRightCornerPressedAction in ResponseList.m");
	NSString* newValue;
	if ([self isCommentsOnly]) {
		newValue = nil;
	} else {
		newValue = @"selected";
	}
	[self.settings.arguments setValue:newValue forKey: @COMMENTS_ONLY_KEY];
	[self widgetSaved:self.settings position:-1];
	[self setHaveCommentsButton];
}

@end

int pixelSizeForFontSize(int i) {
	return round(i*1.28);
}

@implementation ResponsesCell

#define TEXT_WIDTH 226

+ (CGFloat)heightWithFontSize:(int)fs lines:(int)ls response:(ResponsesEntry*)response {
	CGSize size;
	size.height = pixelSizeForFontSize(fs)*ls+1;
	size.width = TEXT_WIDTH;
	size = [response.text sizeWithFont:[UIFont systemFontOfSize:fs]
			         constrainedToSize:size
				         lineBreakMode:UILineBreakModeWordWrap];
	CGFloat height = size.height + pixelSizeForFontSize(fs) + 8;
	if (response.subheader) height += pixelSizeForFontSize(fs);
	return height < 69 ? 69 : height;
}


- (id)replaceWithDataFrom:(ResponsesEntry*)re fontSize:(int)fs lines:(int)ls {
	scoreBackground.color = re.scoreColor;
	alertImage.hidden = re.alert == nil;
	scoreLabel.text = re.score;
	alertLabel.text = re.alert;	
	titleLabel.font = [UIFont boldSystemFontOfSize:fs];
	titleLabel.text = re.name;
	titleLabel.frame = CGRectMake(titleLabel.frame.origin.x, titleLabel.frame.origin.y, TEXT_WIDTH, pixelSizeForFontSize(fs));
	UILabel * bottomHeader = titleLabel;
	if (re.subheader) {
		subheaderLabel.text = re.subheader;
		subheaderLabel.font = [UIFont boldSystemFontOfSize:fs];
		bottomHeader = subheaderLabel;
		CGRect subheaderFrame = subheaderLabel.frame;
		subheaderFrame.origin.x = titleLabel.frame.origin.x;
		subheaderFrame.origin.y = titleLabel.frame.origin.y + pixelSizeForFontSize(fs);
		subheaderFrame.size.width = TEXT_WIDTH;
		subheaderFrame.size.height =  pixelSizeForFontSize(fs)+1;
		subheaderLabel.frame = subheaderFrame;
	}
	bodyLabel.font = [UIFont systemFontOfSize:fs];
	CGRect bodyFrame = bodyLabel.frame;
	bodyFrame.origin.y = bottomHeader.frame.origin.y + pixelSizeForFontSize(fs);
	bodyFrame.size.width = TEXT_WIDTH;
	bodyFrame.size.height =  pixelSizeForFontSize(fs)*ls+1;
	bodyFrame.size = [re.text sizeWithFont:bodyLabel.font 
						constrainedToSize:bodyFrame.size
					  lineBreakMode:UILineBreakModeWordWrap];
	bodyLabel.frame = bodyFrame;
	bodyLabel.numberOfLines = ls;
	bodyLabel.text = re.text;	
	self.clipsToBounds = YES;
	// CGImageCreate
	return self;
}

+ (NSString*)nibName {
	return @"ResponseListCell";
}

@end
