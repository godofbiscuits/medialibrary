//
//  ResponsesEntryHolder.m
//  Medallia
//
//  Created by Øyvind Grotmol on 10.03.10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "ResponsesEntryHolder.h"


@implementation ResponsesEntryHolder

@synthesize position, list;

-(id)initWithList:(ResponsesEntryList*)aList atPosition:(int)aPosition {
	if (self = [super init]) {
		self.list = aList;
		self.position = aPosition;
	}
	return self;
}


-(id<ResponsesEntryProtocol>)get {
	return [self.list responseAt:position];
}

-(void)update:(ResponsesEntry*)response {
	[self.list updateResponseAtIndex:position withResponse:response];
}


@end
