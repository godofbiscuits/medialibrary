
#import "WidgetList.h"
#import "UIViewControllerShared.h"
#import "Metadata.h"
#import "Map.h"
#import "WidgetConfig.h"

@implementation WidgetList
synth_WidgetList
- (id)initWithList:(WidgetInstance_MList*)list widgetTypes:(WidgetType_List *)widgetTypes{
	self = [super initWithStyle:UITableViewStyleGrouped];
	self.instances = list;
	self.widgetTypes = widgetTypes;
	return self;
}
- (void)noteWidgetsChanged {
	if(self.delegate) [self.delegate performSelector:_cmd];
}
- (void)viewDidLoad {
    [super viewDidLoad];
	self.tableView.rowHeight = 44;
	self.editing = YES;
	self.tableView.allowsSelectionDuringEditing = YES;
	self.navigationItem.title = @"Settings";
	self.navigationItem.backBarButtonItem = 
	[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
												   target:self action:@selector(backToSelf)] autorelease];
	self.navigationItem.leftBarButtonItem =
	[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
												   target:self action:@selector(addAction)] autorelease];
	self.navigationItem.rightBarButtonItem =
	[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
												   target:self action:@selector(doneAction)] autorelease];
}

- (void)doneAction {
	[self.navigationController popViewControllerAnimated:YES];
}
- (void)backToSelf {
	[self.navigationController popToViewController:self animated:YES];
}

- (void)addAction {
	NSArray *keys = [NSArray mapArray:self.widgetTypes usingSelector:@selector(key)];
	NSArray *names = [NSArray mapArray:self.widgetTypes usingSelector:@selector(name)];
	SimpleDropdown *sd = [[[SimpleDropdown alloc] initWithList:names keys:keys callbackTo:self withContext:nil] autorelease];
	sd.title = @"New Widget";
	sd.accessory = UITableViewCellAccessoryDisclosureIndicator;
	[self.sharedNavController pushViewController:sd animated:YES];
}
- (void)dropdownDidSelect:(NSString*)object forContext:(id)ctx {
	WidgetType *wt = [[Metadata current].data widgetTypeForKey:object];
	if (!wt) return;
	WidgetConfig *wc = [[[WidgetConfig alloc] initForInstance:[wt instance]] autorelease];
	wc.target = self;
	wc.targetPos = -1;
	[self.sharedNavController pushViewController:wc animated:YES]; 
}

- (void)widgetSaved:(WidgetInstance*)widget position:(int)i {
	if (i >= 0  && i < self.instances.count) {
		[self.instances replaceObjectAtIndex:i withObject:widget];
	} else {
		[self.instances addObject:widget];
	}
	[self.tableView reloadData];
	[self noteWidgetsChanged];
	[self.sharedNavController popToViewController:self.sharedController animated:YES];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	WidgetConfig *wc = [[[WidgetConfig alloc] initForInstance:[self.instances objectAtIndex:indexPath.row]] autorelease];
	wc.target = self;
	wc.targetPos = indexPath.row;
	[self.sharedNavController pushViewController:wc animated:YES];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation 
{
    return ( interfaceOrientation == UIInterfaceOrientationPortrait );
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.instances count];
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	return @"Enabled Dashboard Widgets";
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    WidgetListCell *wc = [[WidgetListCell possiblyCachedForTableView:self.tableView] 
			takeDataFrom:[self.instances objectAtIndex:indexPath.row]];
	return wc;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {    
	// we don't do inserts here
    if (editingStyle == UITableViewCellEditingStyleDelete) {
		[self.instances removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationLeft];
		[self noteWidgetsChanged];
    }   
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
	id x = [self.instances objectAtIndex:fromIndexPath.row];
	[x retain];
	[self.instances removeObjectAtIndex:fromIndexPath.row];
	[self.instances insertObject:x atIndex:toIndexPath.row];
	[x release];
	[self noteWidgetsChanged];
}

- (BOOL)tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
}
@end

@implementation WidgetListCell
synth_WidgetListCell
- (id)takeDataFrom:(WidgetInstance*)inst {
	self.titleL.text = inst.widgetType.name;
	self.detailsL.text = inst.description;
	return self;
}
+ (NSString*) nibName {
	return @"WidgetListCell";
}
@end
