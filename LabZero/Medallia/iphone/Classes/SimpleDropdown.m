
#import "SimpleDropdown.h"
#import "UIViewControllerShared.h"

@implementation SimpleDropdown
synth_SimpleDropdown

- (id)initWithList:(NSArray*)names keys:(NSArray*)keys callbackTo:(id<DropDelegate>)delegate withContext:(id)ctx {
	self = [super init];
	self.keys = keys ? keys : names;
	self.names = names;
	self.delegate = delegate;
	self.context = ctx;
	self.accessory = UITableViewCellAccessoryNone;
	return self;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return ( interfaceOrientation == UIInterfaceOrientationPortrait );
}

#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.keys count];
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier] autorelease];
    }

	NSString* key = [self.keys objectAtIndex:indexPath.row];
    cell.textLabel.text = [self.names objectAtIndex:indexPath.row];
	bool selected = self.oldSelection && [self.oldSelection isEqualToString:key];
	cell.accessoryType = selected ? UITableViewCellAccessoryCheckmark : self.accessory;

    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	[self.delegate dropdownDidSelect:[self.keys objectAtIndex:indexPath.row] forContext:self.context];
}

- (void)viewWillAppear:(BOOL)anim {
       // remove the selection if they return to an already-active dropdown
	NSIndexPath *i = [self.tableView indexPathForSelectedRow];
	if (i) [self.tableView deselectRowAtIndexPath:i animated:NO];
       
	self.navigationItem.title = self.title;
	UIView* customView = [[[UIView alloc] init] autorelease];
	self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:customView] autorelease];
	self.navigationItem.rightBarButtonItem = 
	[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(doCancel)] autorelease];
}
- (void)doCancel {
	[self.navigationController popViewControllerAnimated:YES];
}
@end

