#import "UIViewControllerShared.h"
#import "AltTableDropdown.h"
#import "Map.h"
#import "Metadata.h"

@implementation AltTableDropdown

synth_AltTableDropdown

- (id)initWithConfig:(ConfigPart*)config delegate:(id<AltDelegate>)d {
	self = [super init];
	self.configPart = config;
	self.delegate = d;
	self.table = [[Metadata current] altTable:config.table];
	self.title = config.name;
	return self;
}

- (Alt*)altForPath:(NSIndexPath*)indexPath {
	return [[self.table.sections objectAtIndex:indexPath.section].alternatives objectAtIndex:indexPath.row];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation 
{
    return ( interfaceOrientation == UIInterfaceOrientationPortrait );
}

#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.table.sections.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.table.sections objectAtIndex:section].alternatives.count;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return [self.table.sections objectAtIndex:section].header;
}
- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
	return index;
}
- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
	if ([self.table.sections count] > 1) 
		return [NSArray mapArray:self.table.sections usingSelector:@selector(headerOrBlank)];
	return nil;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"atd"];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:@"atd"] autorelease];
    }
	Alt *a = [self altForPath:indexPath];

	NSString *text = @"";
	for (int i = 0; i < [a.level intValue]; ++i)
		text = [text stringByAppendingString:@"    "];
    cell.textLabel.text = [text stringByAppendingString:a.value];
    cell.imageView.image = [UIImage imageNamed: [a.key isEqual:self.oldSelection] ? @"checkbox.png" : @"checkbox0.png"];

    if ([a hasAlternatives]) {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
	
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Alt *a = [self altForPath:indexPath];
    if ([a hasAlternatives]) {
        AltTableDropdown *drop = [[[AltTableDropdown alloc] initWithConfig:self.configPart delegate:self.delegate] autorelease];
		drop.table = a.table;
        drop.title = a.value;
        drop.oldSelection = self.oldSelection;
        [self.sharedNavController pushViewController:drop animated:YES];
    }
    else {
        [self.delegate alternativeWasSelected:[self altForPath:indexPath] forDropdown:self];
    }
}

@end

