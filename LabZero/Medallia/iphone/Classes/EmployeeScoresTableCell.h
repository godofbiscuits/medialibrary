//
//  EmployeeScoresTableCell.h
//  Medallia
//
//  Created by Jeff Barbose on 3/5/10.
//  Copyright 2010 Medallia, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface /*NOGENERATE*/ EmployeeScoresTableCell : UITableViewCell 
{
    UILabel* titleLabel;
    UIImageView* valueBackgroundImageView;
    UILabel* valueLabel;
}

@property (nonatomic, retain) IBOutlet UILabel* titleLabel;
@property (nonatomic, retain) IBOutlet UIImageView* valueBackgroundImageView;
@property (nonatomic, retain) IBOutlet UILabel* valueLabel;

@end
