//
//  ResponsesEntryImpl.h
//  Medallia
//
//  Created by Øyvind Grotmol on 09.03.10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ResponsesEntryProtocol.h"
#import "DataFormat.h"

@interface /*NOGENERATE*/ ResponsesEntryImpl : NSObject <ResponsesEntryProtocol> {

	NSString * detailsURL;
	NSString * name;
	SingleResponse * fullResponse;

}

@property(copy) NSString * detailsURL;
@property(copy) NSString * name;
@property(retain) SingleResponse * fullResponse;

@end
