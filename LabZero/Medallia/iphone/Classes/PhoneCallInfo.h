//
//  PhoneCallInfo.h
//  Medallia
//
//  Created by Øyvind Grotmol on 05.03.10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CaseManagementInfo.h"

@interface /*NOGENERATE*/ PhoneCallInfo : CaseManagementInfo {
	BOOL didCall;
}

@property (assign) BOOL didCall;

+ (PhoneCallInfo*) load;

+ (void) didLogCall;

- (void) store;

@end
