#import "Gen.h"
#import <UIKit/UIKit.h>
#import "LoginController.h"
#import "SimpleRequest.h"
#import "Dashboard.h"
#import "ResponseList.h"
#import "SharableTabBarController.h"

// The main appDelegate; responsible for instantiating the main components of the app

// See README for a description of the main UI

@interface MedalliaAppDelegate : NSObject <UIApplicationDelegate, UINavigationControllerDelegate, LoginDelegate, SRDelegate, TransitionHelper, UIActionSheetDelegate> {
  ivars_MedalliaAppDelegate;
}

@property(retain) IBOutlet UIWindow *window;
@property(retain) LoginController *loginCtlr;
@property(retain) UINavigationController *navCtlr;
@property(retain) SharableTabBarController *tabCtlr;

@property(retain) ResponseList *responsesTab;

@property(retain) UIViewController *current; // currently active


+ (MedalliaAppDelegate*) sharedAppDelegate;


- (void)setViewC:(UIViewController*)vc transition:(UIViewAnimationTransition)transition;
@end
