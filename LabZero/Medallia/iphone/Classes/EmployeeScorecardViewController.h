//
//  EmployeeScorecardViewController.h
//  Medallia
//
//  Created by Jeff Barbose on 3/5/10.
//  Copyright 2010 Medallia, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SimpleRequest.h"


#import "EmployeeScorecardViewController.h"
#import "ResponseList.h"
#import "ResponsesEntryList.h"
#import "EmployeeScoresTableCell.h"

#import "DataFormat.h"


@interface /*NOGENERATE*/ EmployeeScorecardViewController : UIViewController <SRDelegate>
{
    Scorecard* scorecardModelData;
    
    UITableView* employeeDataTableView;
    
    UIView* employeeTableHeaderView;
    
    EmployeeScoresTableCell* scoresTableCell;
    ResponsesCell* responsesTableCell;
    
	UIActivityIndicatorView *spinner;
	
    ResponsesEntryList* displayedResponses;
    
    CGFloat responsesSectionRowHeight;
    CGFloat scoresSectionRowHeight;
}

@property (nonatomic, retain, readonly) Scorecard* scorecardModelData;

@property (nonatomic, retain) IBOutlet UITableView* employeeDataTableView;

@property (nonatomic, retain) IBOutlet UIView* employeeTableHeaderView;

@property (nonatomic, assign) IBOutlet EmployeeScoresTableCell* scoresTableCell;
@property (nonatomic, assign) IBOutlet ResponsesCell* responsesTableCell;

@property(retain) IBOutlet UIActivityIndicatorView *spinner;

@property (nonatomic, retain) ResponsesEntryList* displayedResponses;

@property (nonatomic, readonly) CGFloat responsesSectionRowHeight;
@property (nonatomic, readonly) CGFloat scoresSectionRowHeight;


- (EmployeeScorecardViewController*) initWithServletURL:(NSURL*) servletURL;

@end
