#import "Dummy.h"
#import "UIViewControllerShared.h"

@implementation Dummy

- (void)loadView {
	static int n = 0;
	number = ++n;
	UILabel *label = self.view = [[[UILabel alloc] initWithFrame:CGRectZero] autorelease];
	label.text = [NSString stringWithFormat:@"Nothing here yet [%d]", number];
	label.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
	label.backgroundColor = [UIColor colorWithRed:0.7+((n&1)>0)*0.2
											green:0.7+((n&2)>0)*0.2 
											 blue:0.7+((n&4)>0)*0.2 alpha:1];
	label.textAlignment = UITextAlignmentCenter;	
	self.navigationItem.title = [NSString stringWithFormat:@"Placeholder %d", number];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return ( interfaceOrientation == UIInterfaceOrientationPortrait );
}

@end
