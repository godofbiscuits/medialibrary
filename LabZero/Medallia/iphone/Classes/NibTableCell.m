#import "NibTableCell.h"

@implementation NibTableCell
+ (id)possiblyCachedForTableView:(UITableView*)tv 
{
    id cell = [tv dequeueReusableCellWithIdentifier:[self nibName]];
	if ( cell != nil )
    {
        return cell;
    }
	
    NSArray *arr = [[NSBundle mainBundle] loadNibNamed:[self nibName] owner:self options:nil];
    
	if ([arr count] == 0 || ![[arr lastObject] isMemberOfClass:[self class]])
	{	
        [NSException raise:@"Error" format:@"Expected an instance of %@ from %@", self, [self nibName]];
    }
    
    
	cell = [arr lastObject];
	return cell;
}
+ (NSString*)nibName {
	return @"-"; // [self className] would be nice
}
@end
