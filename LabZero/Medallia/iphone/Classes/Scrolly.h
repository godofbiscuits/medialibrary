#import "Gen.h"
#import <UIKit/UIKit.h>

// A paged UIScrollView

@protocol ScrollyDelegate
- (int) numberOfParts;
- (UIViewController*) makeControllerForPart:(int)i;
- (void) pageChanged:(int)i;
@end

@interface Scrolly : UIScrollView /*<UIScrollViewDelegate>*/ {
	int dirtyViews;
	ivars_Scrolly;
	IBOutlet id<ScrollyDelegate> scrollyDelegate;
}
@property(retain) NSMutableArray *controllers;
- (UIViewController*) controllerForPart:(int)i;
- (int)currentPage;
- (void)reloadViews;
- (void)scrollToPage:(int)i;
@end
