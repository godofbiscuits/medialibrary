#import "Map.h"
#import "DataFormat.h"
#import "Metadata.h"
#import "SimpleRequest.h"

@implementation ConfigFile
synth_ConfigFile
- (WidgetType*)widgetTypeForKey:(NSString*)s {
	if ([@"ResponsesConfig" isEqualToString:s]) return self.responsesFilterConfig; // slight hack
	for (WidgetType* t in self.scoresWidgetTypes)
		if ([s isEqualToString:t.key]) return t;
	for (WidgetType* t in self.rankerWidgetTypes)
		if ([s isEqualToString:t.key]) return t;
	return nil;
}
- (AltTable*)altTableForKey:(NSString*)s {
	for (AltTable* t in self.tables)
		if ([s isEqualToString:t.key]) return t;
	return nil;
}
@end

@implementation WidgetType
synth_WidgetType
- (WidgetInstance*)instance { return [[[WidgetInstance alloc] initWithType:self] autorelease]; }
@end

@implementation ConfigSection
synth_ConfigSection
@end

@implementation ConfigPart
synth_ConfigPart;

- (void)readFromDict:(NSDictionary*)d {
	[self autoReadFromDict:d];
#define T(e) if ([@#e isEqualToString:[d objectForKey:@"type"]]) self.type = e;
	ConfigPartTypeDef(T,_Nothing)
#undef T
#define T(e) if ([@#e isEqualToString:[d objectForKey:@"headerElement"]]) self.headerElement = e;
	ClickableHeaderElementDef(T,_Nothing)
#undef T
}
- (NSString*)descriptionEntry:(WidgetInstance*)inst {
	switch(self.type) {
	case CEDropdown:
		return [[[Metadata current] altTable:self.table] niceNameFor:[inst.arguments objectForKey:self.key]];
	}
	return nil;
}
@end

@implementation ResponsesLump
synth_ResponsesLump;
@end
@implementation ResponsesEntry
synth_ResponsesEntry;
@end

@implementation AltTable
synth_AltTable

+ (Alt*) findAltFor:(NSString*)key inTable:(AltTable*)table {
	for (AltSection *sec in table.sections) {
		for (Alt *x in sec.alternatives) {
			if ([x.key isEqualToString:key]) {
				return x;
			}
			if ([x hasAlternatives]) {
				Alt* alt = [AltTable findAltFor:key inTable:x.table];
				if (alt != nil) return alt;
			}
		}
	}
	return nil;
}

- (Alt*) findAltFor:(NSString*)key {
	return [AltTable findAltFor:key inTable:self];
}

- (NSString*)niceNameFor:(NSString*)key {
	Alt* alt = [self findAltFor:key];
	NSLog(@"found %@ with %@ for %@ in $@", alt, alt ? alt.value : @"-", key, [self toPlist]);
	if (alt == nil) return nil;
	return alt.value;
}

@end

@implementation AltSection
synth_AltSection
- (NSString*)headerOrBlank { return self.header ? self.header : @""; }

@end

@implementation WidgetInstance
synth_WidgetInstance
- (void)resetDefaults {
	self.arguments = [NSMutableDictionary dictionary];
	// grab defaults
	for (ConfigSection *sect in self.widgetType.configSections) {
		for (ConfigPart *cp in sect.parts) {
			[self.arguments setValue:cp.def forKey:cp.key];
		}
	}
	// add any additional arguments that are not in the config (e.g. ranker scope)
	for (id key in self.widgetType.arguments) {
		[self.arguments setValue:[self.widgetType.arguments objectForKey:key] forKey:key];
	}
}
- (id)initWithType:(WidgetType*)type {
	self = [super init];
	self.type = type.key;
	[self resetDefaults];
	return self;
}
- (WidgetType*)widgetType {
	return [[Metadata current].data widgetTypeForKey:self.type];
}
- (NSString*)description {
	NSMutableString *buffer = [NSMutableString string];
	for (ConfigSection *sect in self.widgetType.configSections) {
		for (ConfigPart *cp in sect.parts) {
			NSString *s = [cp descriptionEntry:self];
			if (s) {
				if ([buffer length]) [buffer appendString:@" - "];
				[buffer appendString: s];
			}
		}
	}
	if ([buffer length]) {
		return buffer;
	} else {
		return self.widgetType.desc;
	}
}
- (id)mutableCopyWithZone:(NSZone *)zone {
	WidgetInstance *i = [[self class] allocWithZone:zone];
	i.type = self.type;
	i.arguments = [self.arguments mutableCopy]; 
	return i;
}
- (URLBuilder*)makeURLBuilder {
	URLBuilder * u = [URLBuilder createBuilder:self.widgetType.urlBase];
	for (NSString *key in self.arguments) {
		NSString *value = [[self.arguments objectForKey:key] description];
		[u appendKey:key withValue:value];
	}
	return u;
}
- (NSURL*)makeURL {
	return [[self makeURLBuilder] getURL];
}
@end

@implementation SWLine synth_SWLine @end
@implementation SWPage synth_SWPage @end
@implementation SingleResponse synth_SingleResponse @end
@implementation FilterButton synth_FilterButton @end
@implementation Scorecard synth_Scorecard @end
@implementation ScorecardScore synth_ScorecardScore @end
@implementation Alt synth_Alt
-(BOOL) hasAlternatives { return self.table && (self.table.sections.count > 0); }
@end
@implementation Graph synth_Graph @end
@implementation GraphLabel synth_GraphLabel @end
@implementation TimePeriod synth_TimePeriod @end
@implementation LineStyle synth_LineStyle @end
@implementation LineData synth_LineData @end
@implementation LogNote synth_LogNote @end
@implementation AlertAction synth_AlertAction @end
@implementation EmailAction synth_EmailAction @end
@implementation RankerData synth_RankerData @end
@implementation RankerUnit synth_RankerUnit @end
