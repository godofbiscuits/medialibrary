#import "Gen.h"


#import <UIKit/UIKit.h>
#import "DataFormat.h"
#import "SimpleRequest.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "ResponsesEntryList.h"
#import "ResponseList.h"


@interface /*NOGENERATE*/ ResponseDetails : UIViewController <UIWebViewDelegate, SRDelegate, UIActionSheetDelegate, MFMailComposeViewControllerDelegate, UIAlertViewDelegate> {

	NSInteger currentPosition;
	ResponsesEntryList *list;

	ResponseList *alertList;
	
	IBOutlet UIWebView *webView;
	IBOutlet UISegmentedControl *prevNext;
	IBOutlet UIActivityIndicatorView *spinner;	
	IBOutlet UIToolbar *toolbar;
}

@property(retain) ResponsesEntryList *list;
@property(retain) SingleResponse *shownResponse;
@property(retain) ResponseList *alertList;

- (id)initWithList:(ResponsesEntryList*)list position:(int)i alertList:(ResponseList*)alertList;
- (bool)loadPage:(int)i;
- (IBAction)prevNextAction;
- (IBAction)actionAction;
@end
