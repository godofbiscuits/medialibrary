#import "WKWidget.h"

@implementation WKWidget

// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
	UIWebView *wv = [[[UIWebView alloc] initWithFrame:CGRectZero] autorelease];
	[wv loadRequest:[NSURLRequest requestWithURL:[self.settings makeURL]]];
	wv.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
	wv.scalesPageToFit = NO;
	self.view = wv;
}

@end
