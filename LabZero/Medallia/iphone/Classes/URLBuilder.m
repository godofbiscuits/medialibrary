//
//  URLBuilder.m
//  Medallia
//
//  Created by Øyvind Grotmol on 17.03.10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "URLBuilder.h"
#import "SimpleRequest.h"

@implementation URLBuilder

-(URLBuilder*)initWithBase:(NSString*)baseURL {
	partialURL = [baseURL mutableCopy];
	return self;
}

+(URLBuilder*)createBuilder:(NSString*)baseURL {
	return [[[URLBuilder alloc] initWithBase:baseURL] autorelease];
}

-(void)appendKey:(NSString*)key withValue:(NSString*)value {
	if (value && value.length > 0) {
		[partialURL appendFormat:@"&%@=%@", key, [value urlencode]];
	}
}

-(NSURL*)getURL {
	return [NSURL URLWithString:partialURL];
}


@end
