#import "Gen.h"
#import <UIKit/UIKit.h>
#import "AltTableDropdown.h"
#import "DataFormat.h"

// WidgetConfig shows the UI for configuring the parameters of a single WidgetInstance (q.v.)

// Calls the 'SavesWidget' delegate when the new config is saved.

@protocol SavesWidget 
- (void)widgetSaved:(WidgetInstance*)i position:(int)p;
@end

@protocol Configurator
@property(retain) WidgetInstance *instance;
@property(retain) id<SavesWidget> target;
@property int targetPos;
@end

@interface WidgetConfig : UITableViewController <AltDelegate, Configurator> {
  ivars_WidgetConfig;
}
@property(retain) WidgetInstance *instance;
@property(retain) id<SavesWidget> target;
@property int targetPos;

- (id)initForInstance:(WidgetInstance*)inst;
@end
