//
//  AddNoteViewController.m
//  Medallia
//
//  Created by Øyvind Grotmol on 04.03.10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "DataFormat.h"
#import "AddNoteViewController.h"
#import "SimpleRequest.h"
#import "UIViewControllerShared.h"

@implementation AddNoteViewController

@synthesize textView, responsesEntryHolder;

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/


// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
	CGRect tfFrame = CGRectMake(0, 0, 320, 200);
	textView = [[UITextView alloc] initWithFrame:tfFrame];
	textView.editable = YES;
	UIView *viewContainer = [[UIView alloc] init];
	[viewContainer addSubview:textView];
	self.view = viewContainer;
}


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	
	self.navigationItem.title = @"New note";
	
	self.navigationItem.rightBarButtonItem =
	[[[UIBarButtonItem alloc] initWithTitle:@"Save" style:0 target:self action:@selector(submitNote)] autorelease];

	[self.textView becomeFirstResponder];
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

-(void)addSpinner {
	if (!spinner) {
		spinner = [[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge] autorelease];
		[self.textView addSubview:spinner];
		spinner.center = self.textView.center;
	}
	[spinner startAnimating];
}

- (void)submitNote {
	[self addSpinner];
	
	URLBuilder * url = [URLBuilder createBuilder:[self.responsesEntryHolder get].detailsURL];
	[url appendKey:@"note_content" withValue:self.textView.text];
	[[[SimpleRequest alloc] initStartingRequest:[NSURLRequest requestWithURL:[url getURL]] for:self dropOld:YES] autorelease];
}

- (void)requestSucceeded:(NSData*)data {
	SingleResponse * response = [SingleResponse fromPlist:[data parsePlist]];
	[self.responsesEntryHolder get].fullResponse = response;

	[self.sharedNavController popViewControllerAnimated:YES];
}

- (void)requestFailed:(NSError*)err {
	[spinner stopAnimating];
	UIAlertView *alert= [[UIAlertView alloc]
							  initWithTitle:NSLocalizedString( @"Error", @"" )
							  message:NSLocalizedString( @"Sorry, note could not be saved.", @"" )
							  delegate:nil
							  cancelButtonTitle:NSLocalizedString( @"OK", @"" )
							  otherButtonTitles:nil];
	
	[alert show];
	[alert autorelease];
}


- (void)dealloc {
    [super dealloc];
}


@end
