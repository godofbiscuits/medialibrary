
#import "Metadata.h"


@implementation Metadata
synth_Metadata;

+ (Metadata*) current {
	static Metadata*m = 0;
	if(!m) m = [Metadata new]; // retained!
	return m;
}
- (AltTable*)altTable:(NSString*)s {
	return [self.data altTableForKey:s];
}
@end
