#import "Map.h"
#import "UIViewControllerShared.h"
#import "WidgetConfig.h"
#import "Metadata.h"
#import "DataFormat.h"
#import "AltTableDropdown.h"
#import "LinkedSlider.h"

@implementation WidgetConfig
synth_WidgetConfig

- (ConfigSection_List*)sections {
	return self.instance.widgetType.configSections;
}
- (NSMutableDictionary*)data {
	return self.instance.arguments;
}

// What ConfigPart is used for a given table cell
- (ConfigPart*)partForPath:(NSIndexPath*)indexPath {
	ConfigSection *section = [[self sections] objectAtIndex:indexPath.section];
	return [section.parts objectAtIndex:indexPath.row];
}

- (id)initForInstance:(WidgetInstance*)inst {
    if (self = [super initWithStyle:UITableViewStyleGrouped]) {
		self.instance = [inst mutableCopy];
    }
    return self;
}

- (void)doSave:(id)x {
	if (self.target) {
		[self.target widgetSaved:self.instance position:self.targetPos];
	}
}

- (void)viewDidLoad {
    [super viewDidLoad];
	self.navigationItem.title = @"Settings";
	self.navigationItem.backBarButtonItem = [[[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStyleBordered target:nil action:nil] autorelease];
       
    // By decree, there will be no Cancel button, even if we're in a NavigationController
    UIView* customView = [[[UIView alloc] init] autorelease];
    self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:customView] autorelease];

	self.navigationItem.rightBarButtonItem = 
	[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doSave:)] autorelease];
}


- (void)viewWillAppear:(BOOL)animated {
       // remove selection on focus
	NSIndexPath *sel = [self.tableView indexPathForSelectedRow];
	if (sel) [self.tableView deselectRowAtIndexPath:sel animated:animated];	
}


// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return ( interfaceOrientation == UIInterfaceOrientationPortrait );
}

#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return self.sections.count;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return [[[self sections] objectAtIndex:section] name];
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.sections objectAtIndex:section].parts.count;
}


// return a label
UILabel *labelAt(UITableViewCell *c, CGRect rect, NSString *text) {
	UILabel *l = [[[UILabel alloc] init] autorelease];
	l.text = text;
	l.frame = rect;
	[c.contentView addSubview:l];
	return l;
}

// show a key/value pair (caption on left, value on right); this is a builtin for iPhoneOS 3.0, but we can't use that :(
- (void)buildLabel:(NSString*)ltext value:(NSString*)rtext inCell:(UITableViewCell*)cell {
	UIFont *leftFont = [UIFont boldSystemFontOfSize:[UIFont labelFontSize]];
	UIFont *rightFont = [UIFont systemFontOfSize:[UIFont labelFontSize]];
	float leftW = [ltext sizeWithFont:leftFont].width;
	
	CGRect rect = [cell bounds];
	rect = CGRectInset(rect, 10, 2);
	CGRect left, right;
	CGRectDivide(rect, &left, &right, leftW, CGRectMinXEdge);
	UILabel *l = labelAt(cell, left, ltext);
	l.font = leftFont;
	l.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
	UILabel *r = labelAt(cell, right, rtext);
	r.font = rightFont;
	r.textAlignment = UITextAlignmentRight;
	l.textColor = [UIColor blackColor]; // caption
	r.textColor = [UIColor blueColor]; // value
	r.autoresizingMask = UIViewAutoresizingFlexibleWidth;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
       // we don't recycle cells; we don't have more than a screenful anyway
    UITableViewCell *cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier] autorelease];
	
	ConfigPart *part = [self partForPath:indexPath];
	NSString *value = [self.data objectForKey:part.key];
	switch(part.type) {
		case CECheckbox: {
			UISwitch *sw = [[[UISwitch alloc] initWithFrame:CGRectZero] autorelease];
			sw.on = !!value;
			[sw addTarget:self action:@selector(switched:) forControlEvents:UIControlEventValueChanged];
			cell.accessoryView = sw;
			cell.textLabel.text = part.name;
			break;
		}
		case CEDropdown: {
			AltTable* table = [[Metadata current].data altTableForKey:part.table];
			NSString *valueText = [table niceNameFor:value];
			[self buildLabel:part.name value:valueText inCell:cell];
			cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
			break;
		}
		case CERange: {
			// see LinkedSlider for an explanation of this
                       
			// it would probably be simpler to load this from a nib, but the code exists already :)
                       
			LinkedSlider *slider[2];
			UILabel *tfield[2];
			UIImage *nullBar = [[UIImage imageNamed:@"nullBar.png"] stretchableImageWithLeftCapWidth:5 topCapHeight:3];
			UIImage *blueBar = [[UIImage imageNamed:@"blueBar.png"] stretchableImageWithLeftCapWidth:5 topCapHeight:3];
			UIImage *grayBar = [[UIImage imageNamed:@"whiteBar.png"] stretchableImageWithLeftCapWidth:5 topCapHeight:3];
                       
			// get values, if present; otherwise, select full range
			NSString_List *edges = (id) [value componentsSeparatedByString:@":"];
			float vals[] = {part.minValue.floatValue, part.maxValue.floatValue};
			if (edges.count == 2) {
				vals[0] = [edges objectAtIndex:0].floatValue;
				vals[1] = [edges objectAtIndex:1].floatValue;
			}
			
			CGRect full = CGRectMake(0,5,300,35);
			
			// create the left and right textfields
			for (int i=2; i-->0; ) {
				CGRect slice, rest;
				CGRectDivide(CGRectInset(full, 8, 0), &slice, &rest, 25, i ? CGRectMaxXEdge : CGRectMinXEdge);
				UILabel *tf = [[[UILabel alloc] initWithFrame:slice] autorelease];
				tf.textAlignment = i ? UITextAlignmentRight : UITextAlignmentLeft;
				tf.text = @"?";
				tf.textColor = [UIColor blueColor];
				[cell.contentView addSubview:tf];
				tfield[i] = tf;
			}
			
			// create the two linked sliders
			for (int i=2; i-->0; ) {				
				LinkedSlider *s = slider[i] = [[[LinkedSlider alloc] initWithFrame:CGRectInset(full, 30, 5)] autorelease];
				s.isRightSlider = i ? YES : NO;
				s.textField = tfield[i];
				s.continuous = YES; s.discrete = YES; // careful now
				
				s.minimumValue = slider_hack_offset + part.minValue.floatValue + 0;
				s.maximumValue = slider_hack_offset + part.maxValue.floatValue + 1; // half-open intervals
				s.value = slider_hack_offset + vals[i] + i; // rightmost slider gets + 1
				[s setMinimumTrackImage:(i ? blueBar : grayBar) forState:UIControlStateNormal];
				[s setMaximumTrackImage:(i ? grayBar : nullBar) forState:UIControlStateNormal];
				[cell.contentView addSubview:s];
				[s addTarget:self action:@selector(sliderSlid:) forControlEvents:UIControlEventValueChanged];
			}
			
			// connect them to eachother
			for (int i=2;i-->0;) {
				slider[i].part = part;
				slider[i].otherSlider = slider[i^1];
			}
			break;
		}
		case CEDummy:
		default: {
			cell.textLabel.text = @"» » Unknown part";
			break;
		}
	}

    return cell;
}

UITableViewCell* cellOf(UIView* v) {
	if (v == nil || [v isKindOfClass:[UITableViewCell class]]) return (UITableViewCell*) v;
	return cellOf([v superview]);
}

// callback from switch
- (void)switched:(UISwitch*)sw {
	UITableViewCell *cell = cellOf(sw);
	if (!cell) return;
	ConfigPart *part = [self partForPath: [self.tableView indexPathForCell:cell]];
	[self.data setValue:([self.data objectForKey:part.key] ? nil : @"selected") forKey:part.key];
	[self.tableView reloadData];
}

// callback from LinkedSlider
- (void)sliderSlid:(LinkedSlider*)sw {
	LinkedSlider *left = sw.isRightSlider ? sw.otherSlider : sw;
	LinkedSlider *right = left.otherSlider;
	NSString *res = [NSString stringWithFormat:@"%f:%f", left.value - slider_hack_offset, right.value - slider_hack_offset - 1];
	[self.data setValue:res forKey:sw.part.key];
}

// callback from dropdown
- (void)alternativeWasSelected:(Alt*)sel forDropdown:(AltTableDropdown*)dropdown {
       [self.data setObject:sel.key forKey:dropdown.configPart.key];
       [self.tableView reloadData];
       [self.sharedNavController popToViewController:self animated:YES];
}


- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	ConfigPart *part = [self partForPath:indexPath];
	switch(part.type) {
               case CERange: return nil; // selecting the slider rows looks ugly
	}
	return indexPath;

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	ConfigPart *part = [self partForPath:indexPath];
	NSString *oldValue = [self.data objectForKey:part.key];
	switch(part.type) {
		case CECheckbox: {
			[self.data setValue:(oldValue ? nil : @"1") forKey:part.key];
			[self.tableView reloadData];
			[self.tableView deselectRowAtIndexPath:indexPath animated:YES];
			break;
		}
		case CEDropdown: {
			AltTableDropdown *drop = [[[AltTableDropdown alloc] initWithConfig:part delegate:self] autorelease];
			drop.oldSelection = oldValue;
			[self.sharedNavController pushViewController:drop animated:YES];
			break;
		}
		default: [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
	}
}

@end

