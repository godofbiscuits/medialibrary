#import <Foundation/Foundation.h>

/*
 * We use property lists (Apple's JSON-like format, but expressed as XML) to communicate with the server.
 *
 * Objects to be transferred need to implement the method
 *    [someFoo toPlist]
 * and the class method
 *    [Foo fromPlist:someDictionary]
 *
 * For arrays of objects, the convenience 
 *    [Foo listFromListOfPlists:...]
 * will return a Foo_List (actually an NSArray; see below)
 */

@interface Loadable : NSObject {
}
+ (id)fromPlist:(NSDictionary*)d;
- (id)toPlist;
+ (id)listFromListOfPlists:(NSArray*)d;

// implement these if you want
- (void)readFromDict:(NSDictionary*)d;
- (void)writeToDict:(NSMutableDictionary*)d;
// generate.pl will implement these:
- (void)autoReadFromDict:(NSDictionary*)d;
- (void)autoWriteToDict:(NSMutableDictionary*)d;

- (id)requiredValue:(id)o name:(NSString*)s;
@end

/*
 Macros to generate specialized versions of NSArray and NSDictionary (with String keys)
 
 Objective-C doesn't have generics, so [someList objectAtIndex:3] will return an untyped 'id'.

 These classes do not exist at runtime.
 
 Use           _ForwardDeclare(Foo); _DeclareType(x)
 to generate   classes Foo_Dict and Foo_List
*/

#define _Paste(a,b) a##b
#define _GA(x,sfx,sel,idx,t) @interface _Paste(x,sfx) : t - (x*)sel:(idx)i; - (int)count; + (x*)fromPlist:(id)x; - (id)toPlist; - (void)sortUsingFunction:(NSInteger (*)(id, id, void *))compare context:(void *)context; @end
#define _GD(x,sfx,sel,idx,t) @interface _Paste(x,sfx) : t - (x*)sel:(idx)i; - (int)count; + (x*)fromPlist:(id)x; - (id)toPlist; @end
#define _Gen(x) \
	_GA(x,_List,objectAtIndex,int,NSArray) \
	_GA(x,_MList,objectAtIndex,int,NSMutableArray) \
	_GD(x,_Dict,objectForKey,NSString*,NSDictionary) \
	_GD(x,_MDict,objectForKey,NSString*,NSMutableDictionary)
#define _DeclareType(x)    _Gen(x)
#define _ForwardDeclare(x) @class _Paste(x,_List); @class _Paste(x,_MList); @class _Paste(x,_Dict);

/*
  Macros for generating an enum and the string recognizer for it
 
  Declare:       #define MyEnumDef(one,and) one(Foo) and one(Bar) and one(Baz)
 
  Use            DeclareEnum(MyEnum)	
  to generate    typedef enum { Foo,Bar,Baz } MyEnum;
*/
         
#define _JustName(x) x
#define _Comma ,
#define _Nothing
#define DeclareEnum(x) typedef enum { _Paste(x,Def)(_JustName,_Comma) } x;

_DeclareType(NSString)
_DeclareType(NSDictionary)
_DeclareType(UIButton)

/*
 * Helper method: cast an object or throw an error.
 */
@interface NSObject (TypeAssert)
+ (id)verifyClassOf:(id)x;
@end

// Deserialize a plist
@interface NSData (PlistParse)
- parsePlist;
@end

// Plist conversions for some built-in types
@interface UIColor (PlistParse)
+ fromPlist:s;
- toPlist;
@end
@interface NSString (PlistParse)
+ fromPlist:x;
- toPlist;
@end
@interface NSDictionary (PlistParse) // we allow an untyped dictionary in a plist; beware
+ fromPlist:x;
- toPlist;
@end
@interface NSNumber (PlistParse)
+ fromPlist:x;
- toPlist;
@end
@interface NSArray (PlistParse) // note that there is no fromPlist:, as we wouldn't know what type to use for the elements
- toPlist;
@end
