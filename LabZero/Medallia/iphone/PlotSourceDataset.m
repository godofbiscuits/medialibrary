//
//  PlotSourceDataset.m
//  Medallia
//
//  Created by Jeff Barbose on 3/1/10.
//  Copyright 2010 Medallia, Inc. All rights reserved.
//

#import "PlotSourceDataset.h"


@implementation PlotSourceDataset

@synthesize xPoints, yPoints;

+ (PlotSourceDataset*) datasetWithXPoints:(NSArray*)xPointsArray yPoints:(NSArray*)yPointsArray
{
    PlotSourceDataset* psd = [[PlotSourceDataset alloc] initWithXPoints:xPointsArray yPoints:yPointsArray];
    
    return [psd autorelease];
}

- (PlotSourceDataset*) initWithXPoints:(NSArray*)xPointsArray yPoints:(NSArray*)yPointsArray
{
    if ( self = [super init] )
    {
        xPoints = [xPointsArray retain];
        yPoints = [yPointsArray retain];
        
        return self;
    }
    else
    {
        return nil;
    }
}


- (NSString*) description
{
    NSMutableString* descript = [NSMutableString stringWithString:@"PlotSourceDataset {\r\n"];
    
    [descript appendFormat:@"xPoints: %@\r\n", [xPoints componentsJoinedByString:@","]];
    [descript appendFormat:@"yPoints: %@\r\n", [yPoints componentsJoinedByString:@","]];
    
    return descript;
}


- (BOOL) hasData;
{
    return (self.xPoints.count > 1);
}

- (void) dealloc
{
    [xPoints release];
    [yPoints release];
    
    [super dealloc];
}



- (NSUInteger) count {
    return xPoints.count;
}


@end
