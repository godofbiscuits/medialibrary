#import "Dashboard.h"
#import "WidgetList.h"
#import "ScoreWidget.h"
#import "UIViewControllerShared.h"
#import "Metadata.h"

@implementation Dashboard

@synthesize instances;
@synthesize widgetTypes = widgetTypes_;
@synthesize prefsKey = prefsKey_;
@synthesize viewRotatable;
@synthesize scrollView;
@synthesize pager;
@synthesize transitionHelper;

- (id)initWithDefault:(WidgetInstance_MList*)defList widgetTypes:(WidgetType_List*)widgetTypes prefsKey:(NSString*)prefsKey rotatable:(BOOL)rotatable {
	self = [super initWithNibName:@"Dashboard" bundle:nil];	
	self.instances = defList;
	self.widgetTypes = widgetTypes;
	self.prefsKey = [NSString stringWithFormat:@"M.%@.%@", [Metadata current].data.userKey, prefsKey];
	self.viewRotatable = rotatable;
	
	// use stored widget instances instead if they exist
	id grub = [[NSUserDefaults standardUserDefaults] valueForKey:[self prefsKey]];
	if (grub) {
		NSArray *x = [WidgetInstance listFromListOfPlists:grub];
		if (x.count) self.instances = [x mutableCopy];
	}
	return self;
}

- (void) setTopTitle {
	NSString * value = [self.instances objectAtIndex:self.pager.currentPage].widgetType.name;
	self.sharedNavController.navigationBar.topItem.title = value;
	self.navigationItem.title = value;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return self.viewRotatable && (interfaceOrientation == UIInterfaceOrientationPortrait ||
                                  interfaceOrientation == UIInterfaceOrientationLandscapeLeft ||
                                  interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    if (toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft ||
        toInterfaceOrientation == UIInterfaceOrientationLandscapeRight)
    {
        ((UITabBarController*) self.parentViewController).tabBar.hidden = YES;
        [[UIApplication sharedApplication] setStatusBarHidden:YES animated:NO];
        [[self navigationController] setNavigationBarHidden:YES animated:NO];
    }
    else {
        ((UITabBarController*) self.parentViewController).tabBar.hidden = NO;
        [[UIApplication sharedApplication] setStatusBarHidden:NO animated:NO];
        [[self navigationController] setNavigationBarHidden:NO animated:NO];
    }

    Widget *widget = (Widget*) [self.scrollView controllerForPart:self.pager.currentPage];
	[widget willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
}

- (void)viewDidLoad 
{
    [super viewDidLoad];

	self.pager.defersCurrentPageDisplay = YES;
	self.navigationItem.rightBarButtonItem =
	[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(doEdit)] autorelease];	
	
	[self noteWidgetsChanged];
	[self setTopTitle];
    
    self.scrollView.scrollsToTop = NO;
}

- (void)saveWidgets {
	[[NSUserDefaults standardUserDefaults] setObject:[self.instances toPlist] forKey:[self prefsKey]];
	[[NSUserDefaults standardUserDefaults] synchronize];
}
- (void)noteWidgetsChanged {
	self.pager.numberOfPages = [self numberOfParts];
	[self saveWidgets];
	[self.scrollView reloadViews]; // this will cause data to be reloaded from server
}

- (void)doEdit {
	WidgetList *vc = [[[WidgetList alloc] initWithList:self.instances widgetTypes:self.widgetTypes] autorelease];
	vc.delegate = self;
	[self.sharedNavController pushViewController:vc animated:YES];
}

// from pager
- (void)pagerChanged {
	int i = self.scrollView.currentPage;
	int j = self.pager.currentPage;
	if (i != j) {
		[self.scrollView scrollToPage:j];
	}
}

// from scroller
- (void)pageChanged:(int)i {
	self.pager.currentPage = i;
	[self setTopTitle];
}
- (int) numberOfParts {
	return self.instances.count;
}
- (UIViewController*) makeControllerForPart:(int)i {
	WidgetInstance *set = [self.instances objectAtIndex:i];
	WidgetType *wt = set.widgetType;
	Widget *w = [[[NSClassFromString(wt.viewClass) alloc] init] autorelease];
	w.settings = set;
	w.owner = self;
	return w;
}
- (void)widgetNeedsEdit:(Widget*)w {	
	WidgetConfig *wc = [[[WidgetConfig alloc] initForInstance:w.settings] autorelease];
	wc.target = self;
	wc.targetPos = [self.instances indexOfObject:w.settings];
	UINavigationController *ch = [[[UINavigationController alloc] initWithRootViewController:wc] autorelease];
	ch.navigationBar.barStyle = UIBarStyleBlackOpaque;
	[wc view]; // make sure the view is loaded

	// we're not doing cancel here?
//	wc.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel 
//																						 target:self
//																						 action:@selector(doCancelConfig)] autorelease];
	[self.transitionHelper setViewC:ch transition:UIViewAnimationTransitionFlipFromLeft];
}

- (void)doCancelConfig {
	[self.transitionHelper setViewC:nil transition:UIViewAnimationTransitionFlipFromRight];
}
- (void)widgetSaved:(WidgetInstance*)i position:(int)p {
	[self.instances replaceObjectAtIndex:p withObject:i];
	[self noteWidgetsChanged];
	[self.transitionHelper setViewC:nil transition:UIViewAnimationTransitionFlipFromLeft];
	// [self.sharedNavController popToViewController:self.sharedController animated:YES];
}

- (UINavigationController*)getNavController {
	return self.sharedNavController;
}




#pragma mark -
#pragma mark === UIScrollViewDelegate Methods ===
#pragma mark -



- (BOOL)scrollViewShouldScrollToTop:(UIScrollView *)aScrollView
{
    if ( aScrollView == self.scrollView )
    {
       // NSLog( @"scrollViewShouldScrollToTop called." );
    }
    
    return YES;
    
}


- (void)scrollViewDidScroll:(UIScrollView *)aScrollView
{
    [self pageChanged:[self.scrollView currentPage]];
    
    
    if ( aScrollView == self.scrollView )
    {
       // NSLog( @"We're connected as scroll view delegate." );
    }
    
    
}


@end

