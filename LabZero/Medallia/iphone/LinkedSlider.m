#import "LinkedSlider.h"


@implementation LinkedSlider
synth_LinkedSlider;
- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
       // if we are the topmost slider, and the touch is to the right of our 'thumb', pass the event through.
       // perhaps better to pass through events to the right of the midpoint between our thumb and the other slider's thumb?
	if (!self.isRightSlider) {
		CGRect track = [self trackRectForBounds:self.bounds];
		CGRect thumb = [self thumbRectForBounds:self.bounds trackRect:track value:self.value];
		if (point.x > CGRectGetMaxX(thumb)) return nil;
	}
	return [super hitTest:point withEvent:event];
}

// force self's value into the range [min,max]
- (void)_clampBetween:(float)min and:(float)max {
       forced = 1; // don't allow
	if (self.value < min) [self setValue:min animated:YES];
	else if (self.value > max) [self setValue:max animated:YES];
	forced = 0;
}

// the user wants to move this slider to 'value'; calculate what value we will allow (by snapping to the closest int), and adjust
// the other slider if necessary
- (float)_change:(float)value {
	if (! self.otherSlider) return value;
	if (self.discrete) value = round(value);
	if (!forced) {
		if (self.isRightSlider) {
                       if (value < self.minimumValue+1) value = self.minimumValue+1; // don't allow the rightmost slider to reach 0 (there would be no room for the left slider)
                       [self.otherSlider _clampBetween:self.minimumValue and:value-1]; // move the left slider over if we reached it
		} else {
                       if (value > self.maximumValue-1) value = self.maximumValue-1; // likewise
			[self.otherSlider _clampBetween:value+1 and:self.maximumValue];
		}
	}
	return value;
}

// update the matching text field
- (void)showText:(float)f {
	self.textField.text = [NSString stringWithFormat:@"%.0f", f - slider_hack_offset - (self.isRightSlider ? 1 : 0)];
}

- (void)setValue:(float)value animated:(BOOL)animated {
	[super setValue:value=[self _change:value] animated:animated];
	[self showText:value];
}
- (void)setValue:(float)value {
	[super setValue:value=[self _change:value]];
	[self showText:value];
} 
 @end
