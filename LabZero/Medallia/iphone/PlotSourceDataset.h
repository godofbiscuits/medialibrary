//
//  PlotSourceDataset.h
//  Medallia
//
//  Created by Jeff Barbose on 3/1/10.
//  Copyright 2010 Medallia, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface /*NOGENERATE*/ PlotSourceDataset : NSObject 
{
    NSArray* xPoints;
    NSArray* yPoints;
}

@property (nonatomic, retain) NSArray* xPoints;
@property (nonatomic, retain) NSArray* yPoints;
@property (nonatomic, readonly) NSUInteger count;
@property (nonatomic, readonly) BOOL hasData;


+ (PlotSourceDataset*) datasetWithXPoints:(NSArray*)xPointsArray yPoints:(NSArray*)yPointsArray;

- (PlotSourceDataset*) initWithXPoints:(NSArray*)xPointsArray yPoints:(NSArray*)yPointsArray;


@end
