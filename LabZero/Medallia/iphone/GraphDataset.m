//
//  GraphDataset.m
//  Medallia
//
//  Created by Doug McBride on 3/17/10.
//  Copyright 2010 Lab Zero, Inc. All rights reserved.
//

#import "GraphDataset.h"

#import "NSNumberExtensions.h"

@interface GraphDataset (Private)
- (PlotSourceDataset*) plotDataForPlot:(CPPlot*)plot;
- (void) addPlotAtIndex:(NSInteger)i toGraph:(CPXYGraph*)graph;
- (void) applyLabels:(GraphLabel_List*)labelList toGraph:(CPXYGraph*)graph;
@end

@implementation GraphDataset

- (id) init {
    if (self = [super init]) {
        lines = [[NSMutableArray alloc] init];
        plotDictionary = [[NSMutableDictionary alloc] init];
    }

    return self;
}

- (id) initWithServerGraph:(Graph*)graph lineStyles:(LineStyle_List*)theLineStyles {
    if (self = [self init]) {
        lineStyles = [theLineStyles retain];

        for (LineData* lineData in graph.lineData) {
            NSString* xDataStr = lineData.xData;
            NSString* yDataStr = lineData.yData;

            NSArray* xDataArray = ( ( xDataStr == nil ) || ( xDataStr.length == 0 ) ) ? nil : [xDataStr componentsSeparatedByString:@"|"];
            NSArray* yDataArray = ( ( yDataStr == nil ) || ( yDataStr.length == 0 ) ) ? nil : [yDataStr componentsSeparatedByString:@"|"];

            NSMutableArray* xNumberArray = [[[NSMutableArray alloc] init] autorelease];

            for ( NSUInteger i = 0 ; i < xDataArray.count ; i++ ) {
                NSNumber* number = [[NSNumber alloc] initWithFloat:[[xDataArray objectAtIndex:i] floatValue]];
                [xNumberArray addObject:number];
                [number release];
            }

            NSMutableArray* yNumberArray = [[[NSMutableArray alloc] init] autorelease];

            for ( NSUInteger i = 0 ; i < yDataArray.count ; i++ ) {
                NSNumber* number = [[NSNumber alloc] initWithFloat:[[yDataArray objectAtIndex:i] floatValue]];
                [yNumberArray addObject:number];
                [number release];
            }

            PlotSourceDataset* psd = [[PlotSourceDataset alloc] initWithXPoints:xNumberArray yPoints:yNumberArray];
            [self addPlotDataset:psd];
            [psd release];
        }
    }

    return self;
}

- (void) dealloc {
    [lines release];
    [lineStyles release];
    [plotDictionary release];

    [super dealloc];
}

- (void) addPlotDataset:(PlotSourceDataset*)plotSourceDataset {
    [lines addObject:plotSourceDataset];
}

- (PlotSourceDataset*) plotDataForPlot:(CPPlot*)plot {
    return [plotDictionary objectForKey:plot.identifier];
}

- (BOOL) hasData {
    for (PlotSourceDataset* plotSourceDataset in lines) {
        if (plotSourceDataset.hasData) {
            return YES;
        }
    }
    
    return NO;
}

- (void) applyToGraph:(CPXYGraph*)graph labels:(GraphLabel_List*)labelList {
    if (self.hasData) {
        [self applyLabels:labelList toGraph:graph];

        for (CPPlot* plot in [graph allPlots]) {
            [graph removePlot:plot];
        }

        [plotDictionary release];
        plotDictionary = [[NSMutableDictionary alloc] init];

		NSInteger indexForFeaturedGraph = -1;
		for (NSInteger i = 0; i<lines.count; ++i) {
			if (((LineStyle*)[lineStyles objectAtIndex:i]).featured) {
				indexForFeaturedGraph = i;
			}
		}
		if (indexForFeaturedGraph >= 0 && indexForFeaturedGraph < lines.count-1) {
			// there is a featured graph, and it is not the first one to be added to the graph
			[self addPlotAtIndex:indexForFeaturedGraph toGraph:graph];
		}
        for (NSInteger i = lines.count-1; i >= 0; --i) {
            [self addPlotAtIndex:i toGraph:graph];
        }

        [graph reloadData];
        CPXYPlotSpace* plotSpace = (CPXYPlotSpace*) graph.defaultPlotSpace;
        [plotSpace scaleToFitPlots:[graph allPlots]];

        // X is always 0 to 1
        [plotSpace setPlotRange:[CPPlotRange plotRangeWithLocation:[[NSDecimalNumber decimalNumberWithString:@"0"] decimalValue]
                         length:[[NSDecimalNumber decimalNumberWithString:@"1"] decimalValue]] forCoordinate:CPCoordinateX];

        CPPlotRange* yRange = [plotSpace plotRangeForCoordinate:CPCoordinateY];

		// Make sure the graph includes 0 on the y axix
		NSDecimal zero = [[NSDecimalNumber zero] decimalValue];
		CPPlotRange * zeroRange = [CPPlotRange plotRangeWithLocation:zero length:zero];
		[yRange unionPlotRange:zeroRange];

        // Increase height by 10%, as scaleToFitPlots doesn't seem to allow enough height
		yRange.doublePrecisionLength *= 1.10;
		
        // Fill under the first plot line
        CPScatterPlot *firstPlot = (CPScatterPlot*) [graph plotAtIndex:0];
        firstPlot.areaBaseValue = yRange.location;
		
        [graph.axisSet relabelAxes];
    }
}

- (void) applyLabels:(GraphLabel_List*)labelList toGraph:(CPXYGraph*)graph {
    CPXYAxisSet *axisSet = (CPXYAxisSet*) graph.axisSet;

    if (labelList) {
        NSMutableArray* labelArray = [[[NSMutableArray alloc] init] autorelease];

        CPTextStyle* labelTextStyle = [CPTextStyle textStyle];
        labelTextStyle.color = [CPColor whiteColor];

        for (GraphLabel* gl in labelList) {
            CPAxisLabel* label = [[[CPAxisLabel alloc] initWithText:gl.label textStyle:labelTextStyle] autorelease];
            label.tickLocation = [[gl.position decimalNumber] decimalValue];
            label.offset = 2.0f;
            [labelArray addObject:label];
        }

        axisSet.xAxis.axisLabels = [NSSet setWithArray:labelArray];
    }

    axisSet.xAxis.labelingPolicy = CPAxisLabelingPolicyNone;
    axisSet.yAxis.labelingPolicy = CPAxisLabelingPolicyAutomatic;
}

- (void) addPlotAtIndex:(NSInteger)i toGraph:(CPXYGraph*)graph {
    CPScatterPlot* connectedPointsScatterPlot = [[[CPScatterPlot alloc] initWithFrame:graph.bounds] autorelease];
    connectedPointsScatterPlot.identifier = [NSNumber numberWithUnsignedInt:i];
    LineStyle* lineStyle = (LineStyle*) [lineStyles objectAtIndex:i];
    connectedPointsScatterPlot.dataLineStyle.lineWidth = lineStyle.featured ? 2.5f : 1.5f;
    connectedPointsScatterPlot.dataLineStyle.lineColor = [CPColor colorWithCGColor:lineStyle.color.CGColor];
    connectedPointsScatterPlot.dataSource = self;
	NSLog(@"Adding graph at %d with featured=%d", i, lineStyle.featured);

	if (lineStyle.featured) {
		CPGradient *fillGradient = [[[CPGradient alloc] init] autorelease];
		fillGradient = [fillGradient addColorStop:[CPColor colorWithComponentRed:0.0 green:0.0 blue:0.0 alpha:0.0] atPosition:0.0];
		fillGradient = [fillGradient addColorStop:[CPColor colorWithComponentRed:1.0 green:1.0 blue:1.0 alpha:0.7] atPosition:1.0];
		fillGradient.angle = 90.0;
		connectedPointsScatterPlot.areaFill = [CPFill fillWithGradient:fillGradient];
	}

	[plotDictionary setObject:[lines objectAtIndex:i] forKey:connectedPointsScatterPlot.identifier];
    [graph addPlot:connectedPointsScatterPlot];
}

#pragma -
#pragma === CPPlotDataSource Methods ===
#pragma -


-(NSUInteger)numberOfRecordsForPlot:(CPPlot *)plot
{
    PlotSourceDataset *plotSourceDataset = [self plotDataForPlot:plot];
    return plotSourceDataset.count;
}



-(NSNumber* )numberForPlot:(CPPlot *)plot 
                     field:(NSUInteger)fieldEnum 
               recordIndex:(NSUInteger)index 
{
    PlotSourceDataset *plotSourceDataset = [self plotDataForPlot:plot];

    switch (fieldEnum) {
        case CPScatterPlotFieldX:
        return [plotSourceDataset.xPoints objectAtIndex:index];

        case CPScatterPlotFieldY:
        return [plotSourceDataset.yPoints objectAtIndex:index];
    }
    return nil;
}

@end
