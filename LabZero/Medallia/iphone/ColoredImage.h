#import <UIKit/UIKit.h>
#import "Gen.h"

// Shows a single color, but using alpha data from an image mask

@interface ColoredImage : UIView {
	ivars_ColoredImage;
}
@property(retain) OnSet(setNeedsDisplay) UIColor *color;
@end
