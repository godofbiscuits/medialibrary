//
//  CallLogViewController.h
//  Medallia
//
//  Created by Jeff Barbose on 3/8/10.
//  Copyright 2010 Medallia, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CaseManagementInfo.h"
#import "SimpleRequest.h"

@class ResponseList;
@class ResponsesEntryHolder;


@interface /*NOGENERATE*/ CallLogViewController : UIViewController <UITextViewDelegate, SRDelegate>
{
    UIScrollView* scrollView;

    UILabel* customerNameLabel;
	UILabel* nameTitleLabel;
	
    UILabel* callInitatedDateLabel;
	UILabel* callInitatedDateTitleLabel;
	
    UISwitch* alertSwitch;
    UILabel* alertSwitchLabel;
    UIView* alertSwitchEnclosingView;
    UITextView* comments;
    
    UIView* falseCover;
    
    ResponseList* completionController;
	ResponsesEntryHolder * responsesEntryHolder;
    
    CaseManagementInfo* info;

	UIActivityIndicatorView *spinner;

}
@property (nonatomic, retain) IBOutlet UIScrollView* scrollView;
@property (nonatomic, retain) IBOutlet UILabel* customerNameLabel;
@property (nonatomic, retain) IBOutlet UILabel* nameTitleLabel;
@property (nonatomic, retain) IBOutlet UILabel* callInitatedDateLabel;
@property (nonatomic, retain) IBOutlet UILabel* callInitatedDateTitleLabel;
@property (nonatomic, retain) IBOutlet UISwitch* alertSwitch;
@property (nonatomic, retain) IBOutlet UILabel* alertSwitchLabel;
@property (nonatomic, retain) IBOutlet UIView* alertSwitchEnclosingView;
@property (nonatomic, retain) IBOutlet UITextView* comments;
@property (nonatomic, retain) IBOutlet UIView* falseCover;
@property(retain) IBOutlet UIActivityIndicatorView *spinner;

@property (nonatomic, assign) ResponseList* completionController;
@property (nonatomic, retain) ResponsesEntryHolder * responsesEntryHolder;
@property (nonatomic, retain) CaseManagementInfo* info;

- (id) init;
- (IBAction) removeFalseCover:(id)sender;
- (IBAction) saveLog:(id)sender;
- (IBAction) cancel:(id)sender;

- (IBAction) alertSwitched:(id)sender;


@end
