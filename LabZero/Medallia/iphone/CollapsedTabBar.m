#import "CollapsedTabBar.h"
#import "Map.h"


@implementation CollapsedTabBar
synth_CollapsedTabBar;
- (UIButton*)makeButton:(NSString*) s {
	UIButton *b = [[[UIButton alloc] init] autorelease];
	b.titleLabel.font = currentValueLabel.font;
	b.showsTouchWhenHighlighted = YES;
	[b addTarget:self action:@selector(clicked:) forControlEvents:UIControlEventTouchUpInside];
	[b setTitle:s forState:UIControlStateNormal];
	return b;
}
- (void)setTrayClosed {
	rolloutView.frame = CGRectMake(CGRectGetMaxX(fullFrame), fullFrame.origin.y, 0, fullFrame.size.height);
	currentValueBackground.image = [UIImage imageNamed:@"db-scoreboard-timetoggle-bg-active.png"];
}
- (void)setTrayOpen {
	rolloutView.frame = fullFrame;
	currentValueBackground.image = [UIImage imageNamed:@"db-scoreboard-timetoggle-bg-inactive.png"];
}
- (IBAction)mainButtonClicked {
	// do nothing if the page hasn't loaded yet
	if (!self.tabNames) return;
	
	// grab the size of the tray
	if (!fullFrame.size.width) {
		fullFrame = rolloutView.frame;
	}
	
	// create the buttons the first time they're needed.
	if (!self.buttons) {
		self.buttons = (id) [NSArray mapArray:self.tabNames usingSelector:@selector(makeButton:) ofReceiver:self];
		
		// We divide the tray evenly. Since the labels are centered in their bounding boxes, we'll get something like
		// [ --- foo --- ] [ --- bar --- ]
		// or, visibly,
		// [ --- foo ----------- bar --- ]
		// with twice as much space between elements as at each side. We lop off a few pixels on each side to counteract
		// this
		
		CGRect rect = fullFrame;
		rect.origin = CGPointZero;
		rect = CGRectInset(rect, 10, 0);
		rect.size.width /= self.buttons.count;
		for (UIButton *b in self.buttons) {
			b.frame = rect;
			rect.origin.x += rect.size.width;
			[rolloutView addSubview:b];
		}
	}
	if (!open) {
		open = YES;
		changed = NO;
		[UIView beginAnimations:@"Foo" context:nil];
		// start with a zero-width view almost hidden behind the open button
		// use ease-out, so the tray rolls out at full speed, then slows to a halt		
		[UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
		rolloutView.hidden = NO; // the tray is hidden initially (from interface builder)		
		[self setTrayClosed];
		[self setTrayOpen];
		[UIView commitAnimations];
	} else {	
		if (changed) {
			[delegate collapsingTabBarCommittedIndex:selected];
		}
		open = NO;
		[UIView beginAnimations:@"Foo" context:nil];
		[UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
		[self setTrayOpen];
		[self setTrayClosed];
		[UIView commitAnimations];
	}
}

- (IBAction)clicked:(UIButton*)choice {	
	[self setSelectedIndex:[self.buttons indexOfObject:choice]];
	changed = YES;
	[delegate collapsingTabBarSelectedIndex:selected];
}
- (void)setSelectedIndex:(int)i {
	selected = i;
	if (i>=0 && i < self.tabNames.count) {
		currentValueLabel.text = [self.tabNames objectAtIndex:i];
	}
}

@end
