#import "SimpleRequest.h"

static NSMutableDictionary* existing = 0;

@implementation SimpleRequest
synth_SimpleRequest;

- (id)hashKey {
	return [NSString stringWithFormat:@"%x", (int)self.delegate];
}
- (BOOL)stillPertinent {
	return !dropOld || (self.delegate && [existing objectForKey:self.hashKey] == self);
}
- (id)initStartingRequest:(NSURLRequest*)req for:(id<SRDelegate>)d dropOld:(BOOL)u {
	self = [super init];
	self.delegate = d;
	dropOld = u;
    NSLog(@"req = %@", req);
	[NSURLConnection connectionWithRequest:req delegate:self];
	if (dropOld) {
		if (!existing) existing = [[NSMutableDictionary dictionary] retain];
		SimpleRequest* old = [existing objectForKey:self.hashKey];
		if (old)
			[old abort];
		[existing setObject:self forKey:self.hashKey];
	}
	return self;
}

- (void)clear {
	if (!self.delegate) return;
	if (dropOld) [existing removeObjectForKey:self.hashKey];
	self.delegate = nil;
	self.buffer = nil;
}

- (void)abort {
	[self clear];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
	if (![self stillPertinent]) return;
	@try {
		[self.delegate requestSucceeded:self.buffer];
	} @catch (NSException *e) {
		NSLog(@"Exception: %@", e);
		[[[UIAlertView alloc] initWithTitle:[e name] message:[e reason] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
	}
	[self clear];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	if (![self stillPertinent]) return;
	if (!self.buffer) self.buffer = [data mutableCopy];
	else [self.buffer appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
	if (![self stillPertinent]) return;
	[self.delegate requestFailed:error];
	[self clear];
}
@end
