#import <UIKit/UIKit.h>
#import "Gen.h"
#import "DataFormat.h"

// The 'range' control is implemented by stacking two LinkedSlider instances on top of each other.
// 
// Objects:
//
//   "top"
//     |       [------- O - - - - - - - -]    top slider: left is gray, right is transparent
//     |       [================ O ------]    bottom slider: left is blue, right is gray
//   "bottom"
//
// On screen:
//             [------- O ====== O ------]


// XXX: for some reason value=0 makes the thumb disappear; appears to be a library bug. we add an arbitrary constant.
#define slider_hack_offset 1000

@interface LinkedSlider : UISlider {
	ivars_LinkedSlider;
	int forced;
}
@property BOOL isRightSlider;
@property BOOL discrete;
@property(assign) LinkedSlider *otherSlider; // XXX: cyclical, so don't retain
@property(retain) UILabel *textField;
@property(retain) ConfigPart *part;
@end
