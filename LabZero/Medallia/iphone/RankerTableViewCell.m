//
//  RankerTableViewCell.m
//  Medallia
//
//  Created by Max Bennedich on 3/4/10.
//  Copyright 2010 Medallia, Inc. All rights reserved.
//

#import "RankerTableViewCell.h"

@implementation RankerTableViewCell

@synthesize index;
@synthesize unitName;
@synthesize score;
@synthesize difference;
@synthesize selectedImage;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
	self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
	return self;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
	[super setSelected:selected animated:animated];
}

- (void)dealloc {
    [super dealloc];
}

@end
