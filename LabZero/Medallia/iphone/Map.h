#import <Foundation/Foundation.h>

//: Some functional programming cliches

@interface NSDictionary (Map)
// given array of x, return dictionary mapping [x selector] to x
+ (NSDictionary*)dictionaryFromArray:(NSArray*)arr keySelector:(SEL)selector;
@end
@interface NSArray (Map)
// given array of x, return array of result of [x selector]
+ (NSArray*)mapArray:(NSArray*)array usingSelector:(SEL)selector;
+ (NSArray*)mapArray:(NSArray*)array usingSelector:(SEL)selector ofReceiver:(id)rec;
@end
