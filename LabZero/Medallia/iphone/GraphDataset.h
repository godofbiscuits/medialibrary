//
//  GraphDataset.h
//  Medallia
//
//  Created by Doug McBride on 3/17/10.

#import <Foundation/Foundation.h>

#import "CorePlot-CocoaTouch.h"

#import "PlotSourceDataset.h"
#import "DataFormat.h"

@interface /*NOGENERATE*/ GraphDataset : NSObject <CPPlotDataSource> {
    NSMutableArray* lines;
    NSMutableDictionary* plotDictionary;
    NSArray* lineStyles;
}

@property (nonatomic, readonly) BOOL hasData;

- (void) addPlotDataset:(PlotSourceDataset*)plotSourceDataset;
- (id) initWithServerGraph:(Graph*)graph lineStyles:(LineStyle_List*)lineStyles;
- (void) applyToGraph:(CPXYGraph*)graph labels:(GraphLabel_List*)labelList;

@end
