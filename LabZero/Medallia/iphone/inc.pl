use strict;
my %edge;
my %util = qw(Allocr 1 Map 1 Gen 1 DataFormat 1 Metadata 1);

for my $f (<*>, <*/*>) {    
    my ($file, $h) = $f =~ m/(\w+)\.([hm])$/ or next; 
    my $style = $h eq 'h' ? "solid" : "dashed";
    open F, $f;
    for (<F>) {
	/import "(\w+)\.h"/ and $1 ne $file and !$util{$1} and !$util{$file} and $edge{qq'"$file" -> "$1" [style=$style];\n'}++;
    }
    
}
print "digraph G {", keys %edge, "}";
