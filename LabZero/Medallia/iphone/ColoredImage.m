#import "ColoredImage.h"

@implementation ColoredImage
synth_ColoredImage;

- (void)drawRect:(CGRect)rect {
	static CGImageRef mask = 0;
	if (!mask) {
		int xs = 53, ys = 54;
		// alpha channel extracted with
		//    convert /ds/response\ box\ masked.png -depth 8 rgba:- | perl -pe 's/...(.)/$1/gsm' > ri-scorebox-mask.gray
		NSString *path = [[NSBundle mainBundle] pathForResource:@"ri-2-scorebox-mask" ofType:@"gray"];
		CGDataProviderRef provider = CGDataProviderCreateWithFilename([path cStringUsingEncoding:[NSString defaultCStringEncoding]]);
		CGColorSpaceRef space = CGColorSpaceCreateDeviceGray();
		float decode[] = {0, 1};
		mask = CGImageCreate(xs, ys, 8, 8, xs, space, kCGImageAlphaNone, provider, decode, true, kCGRenderingIntentDefault);
        CGDataProviderRelease(provider);
        CGColorSpaceRelease(space);
	}
	CGContextRef ctx = UIGraphicsGetCurrentContext();
	CGContextClipToMask(ctx, self.bounds, mask);
	[self.color setFill];
	CGContextFillRect(ctx, rect);
}

@end
