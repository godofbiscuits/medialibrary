<?php
if ($_GET[sleep]) sleep($_GET[sleep]);
function base() { return "http://$_SERVER[HTTP_HOST]$_SERVER[PHP_SELF]"; }

# this file provides some dummy data

# no arguments -> xml spec
# i=2&j=3 -> html for one response
# widget=score&prop=1&segment=2 -> data for score plugin

$comments = file(dirname(__FILE__).'/testdata/comments_anything_else.txt');

function pick($a) { if(!is_array($a)) $a = split(' ', $a); return $a[rand(0, count($a)-1)]; }

$colors = split(' ','333333 333333 cc0000 cc0000 cc0000 cc0000 cccc00 cccc00 cccc00 00cc00 00cc00');

function generate($i=0) { global $comments, $colors;
	 if (!$i) $i = crc32(microtime());
	 srand($i);
	 $first = pick('Michael Jennifer Christopher Amy Jason Melissa David Michelle James Kimberly John Lisa Robert Angela Brian Heather William Stephanie Matthew Nicole Joseph Jessica Daniel Elizabeth Kevin Rebecca Eric Kelly Jeffrey Mary Richard Christina Scott Amanda Mark Julie Steven Sarah Thomas Laura Timothy Shannon Anthony Christine Charles Tammy Joshua Tracy Ryan Karen Jeremy Dawn Paul Susan Andrew Andrea Gregory Tina Chad Patricia Kenneth Cynthia Jonathan Lori Stephen Rachel Shawn April Aaron Maria Adam Wendy Patrick Crystal Justin Stacy Sean Erin Edward Jamie Todd Carrie Donald Tiffany Ronald Tara Benjamin Sandra Keith Monica Bryan Danielle Gary Stacey Jose Pamela Nathan Tonya Douglas Sara');
	 $last = pick('Smith Johnson Williams Brown Jones Miller Davis Anderson Taylor Thomas Moore Martin Jackson Thompson White Lee Harris Clark Lewis Robinson Walker Hall Young Allen Wright King Scott Green Baker Adams Nelson Hill Campbell Mitchell Roberts Carter Phillips Evans Turner Torres Parker Collins Edwards Stewart Flores Morris Nguyen Murphy Rivera Cook Rogers Morgan Peterson Cooper Reed Bailey Bell Kelly Howard Ward Cox Richardson Wood Watson Brooks Bennett Gray James Reyes Cruz Hughes Price Myers Long Foster Sanders Ross Morales Powell Sullivan Russell Ortiz Jenkins Perry Butler Barnes Fisher');
	 $sc = pick('0 0 1 1 2 3 4 5 6 7 7 7 8 8 8 8 9 9 9 9 10 10 10 10 10');
	 $a =  array(
	 	'score' => $sc, 'scoreColor' => $colors[$sc],
		'name' => "$first $last",
		'sectionHeader' => pick(array('January 1', 'February 3')),
		'detailsURL' => base().'?details='.$i,
		'text' => pick($comments));
	if ($sc<4 && rand(0,3)==0)  $a[alert] = pick('A1 A2 T1');
	return $a;
}

function kv($k, $v) { return array(key=>"$k", value=>$v); }
$props = array(array('alternatives' => array(kv('all', 'All properties'))));

# organize properties by first letter

foreach (file(dirname(__FILE__)."/data/properties.txt") as $i => $v) {
  $z[$v[0]][] = kv($i, chop($v));
}
ksort($z);
foreach ($z as $k => $vals) $props[] = array(header => $k, alternatives => $vals);


# print php objects as a plist

function dump($d, $ind = '') {
  $ind .= '  ';
  if(is_array($d)) {
    $k = array_keys($d);
    if(!$k || is_numeric($k[0])) {
      echo "$ind<array>\n";
      foreach($d as $v) dump($v, $ind);
      echo "$ind</array>\n";
    } else {
      echo "$ind<dict>\n";
      foreach($d as $k => $v) {
        echo "$ind  <key>$k</key>\n";
        dump($v, $ind);
      }
      echo "$ind</dict>\n";
    }
  } else if (gettype($d) == 'integer') {
    echo "$ind<integer>$d</integer>\n";
  } else if (gettype($d) == 'double') {
    echo "$ind<real>$d</real>\n";
  } else if (gettype($d) == 'string') { $d = htmlspecialchars($d); 
    echo "$ind<string>$d</string>\n";
  } else die("<h2>".gettype($d)."</h2>\n");
}
function plist($d) {
  if ($_GET['print']) { print_r($d); die(); }
  
  echo  '<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
';
  dump($d);
  echo "</plist>\n";
}

if ($_GET['details']) {
   $x = generate($_GET['details']);
  header("Pragma: cache");
  header("Expires: Tue, 31 Dec 2030 00:00:00 GMT");
  header("Cache-Control: max-age=946080000, public, cache");                
  extract($x);
  include 'fallbacktest.html';
  die;
}
if (isset($_GET['responses'])) {
   $e = array(generate(),generate(),generate());
   if (!rand(0,3) && !$_GET['more']) {
     $z = array(fetchMoreText => 'No results; click to adjust filter.', 'entries' => array());
   } else if (rand(0,1)) {
     $z = array(fetchMoreText => '42 More...', fetchMoreURL => base().'?responses=1&more=1', 'entries' => $e);
   } else {
     $z = array(fetchMoreText => 'No more results; click to adjust filter.', 'entries' => $e);
   } 
   plist($z);
   die();
}
if ($_GET['widget']) {  
  die("<h2>Dummy HTML widget</h2>");
}
if ($_GET['scores']) {
   srand(crc32($_GET['prop'] . $_GET['segment']));
   $words = split(' ', 'Daily Weekly Monthly Quarterly Annual');
   $r = array();
   foreach (split(' ', 'Today 1w 1m 3m 1y') as $i => $k) {   
     $plus = rand(0,1);
     $lines = array();
     foreach (array('Personal Training', 'Genius Bar', 'Red Zone') as $glork) {
         $lines[] = 
     	  array('title' => 'NPS '.$glork, 'color' => pick('cc0000 00cc00 bbbb00'), 'benchText' => "vs 6m", 'nibClass'=>'ScoreWidgetNum', 'difference' => 
	  		   ($plus?"+":"-").(rand(0,25)*0.1), 'diffColor' => $plus ? 0xff3333 : 0xffffff,
			   				     'number' => rand(50,95));
     }
     foreach (array('Responses', 'Alerts') as $glork) {
         $lines[] = 
     	  array('title' => $glork, 'baseline' => rand(11,20), 'number' => rand(11,20), 'benchText' => $words[$i].' avg', 'nibClass'=>'ScoreWidgetGauge');

     }
     $r[] = array('timeText' => $k, 'lines' => $lines);
   }
   plist($r);
   die();
}
if ($_GET['p'] == 'bad') plist(array('error' => 'Incorrect password.'));
plist(array(
  'responsesFilterConfig' => array(
      'name' => 'Settings',
      'urlBase' => base()."?responses=1",
      'viewClass' => 'ResponseList',
      'configSections' => array(
        array(
          'name' => 'Filter',
          'parts' => array(
            array(
              'key' => 'satrange',
	      'minValue' => -5, # for testing
	      'maxValue' => 5,
              'type' => 'CERange'),
            array(
              'type' => 'CEDropdown',
              'def' => 'all',
              'name' => 'Property',
              'key' => 'prop',
              'table' => 'PROPERTIES'),
            array(
              'type' => 'CECheckbox',
              'name' => 'Only with comments',
              'key' => 'only_comments'),
            array(
              'type' => 'CECheckbox',
              'name' => 'Only with alerts',
              'key' => 'only_alerts'))),          
        array(
          'name' => 'Formatting',
          'parts' => array(
            array(
              'type' => 'CEDropdown',
              'name' => 'Preview size',
              'def' => '3',
              'key' => 'preview_lines',
              'table' => 'LINES_PREVIEW'))))),
  'widgetTypes' => array(
    array(
      'name' => 'Scoreboard',
      'key' => 'score',
      'desc' => 'Overview of current scores',
      'viewClass' => 'ScoreWidget',
      'urlBase' => base()."?scores=1",
      'configSections' => array(
        0 => array(
          'name' => 'Scoreboard settings',
          'parts' => array(
            0 => array(
              'name' => 'Property',
              'key' => 'prop',
              'type' => 'CEDropdown',
              'def' => 'all',
              'table' => 'PROPERTIES'),
            1 => array(
              'name' => 'Segment',
              'key' => 'segment',
              'type' => 'CEDropdown',
              'def' => 'all',
              'table' => 'SEGMENTS'))))),
    array(
      'name' => 'Placeholder',
      'key' => 'ph',
      'desc' => 'Any HTML-based widget',
      'viewClass' => 'WKWidget',
      'urlBase' => base()."?widget=1",
      'configSections' => array(
        0 => array(
          'name' => 'No settings',
          'parts' => array())))),
  'initialResponseFilter' => 
     array('type' => 'ResponsesConfig', 'arguments' => 
     		  array('prop' => 'all', 'only_comments' => '1')),
  'initialWidgets' => array(
     array('type'=>'score', 'arguments' => array('prop' => '1')),
     array('type'=>'score', 'arguments' => array('prop' => '2')),
     array('type'=>'ph', 'arguments' => array('prop' => '2')),
  ),
  'userKey' => 'fooUser',
  'tables' => array(
     array('key' => 'LINES_PREVIEW', 'sections' => array(array('alternatives' => array(
        kv('62', '3 lines'),
        kv('75', '4 lines'),
        kv('88', '5 lines'))))),
     array('key' => 'PROPERTIES', 'sections' => $props),
     array('key' => 'SEGMENTS', 'sections' => array(array('alternatives' => array(
       kv('all', 'All'),
       kv('m', 'Male'),
       kv('f', 'Female'),
       kv('ip', 'iPhone 3G'))))))));

