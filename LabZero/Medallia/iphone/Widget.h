#import <UIKit/UIKit.h>
#import "Gen.h"
#import "DataFormat.h"
#import "WidgetConfig.h"

@class Widget;

// base class for widgets; provides an "edit" action

@protocol WidgetOwner
- (void)widgetNeedsEdit:(Widget*)w;
- (void)saveWidgets;
- (UINavigationController*)getNavController;
@end

@interface Widget : UIViewController {
	ivars_Widget;
}
@property(retain) IBOutlet UIActivityIndicatorView *spinner;

@property(retain) WidgetInstance *settings;
@property(retain) id<WidgetOwner> owner;

- (IBAction)editAction;

- (WidgetInstance*)getWidgetInstance;
- (UINavigationController*)getNavController;
- (void)widgetSaved:(WidgetInstance*)widget position:(int)i;
- (void)startLoadingData;

@end
