<script>
document.documentElement.style.webkitTapHighlightColor = 'rgba(24,92,224,0.8)';
document.documentElement.style.webkitTouchCallout = "none";
</script>

<style>
body{background: #d8d8d8;font-family:helvetica;margin:0;padding:8px;}
body, h2{font-size:14px;}
h1{font-size:16px;color:#444;text-shadow: rgba(255, 255, 255, 0.75) 0 1px 0;margin:13px 10px 8px;}
div.box{padding:0;border:1px solid #c0c0c0;border-top:1px solid #fff;border-bottom:1px solid #999;background:#fff url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAIGNIUk0AAG4nAABzrwAA90AAAIaOAABt7QAA6uUAADHIAAAYZWUn6MsAAABHSURBVHja7M3BCQAgDATBU3z49iME++8mWFIs40B2GphWVXLoMiEmJiYmJiYmJib+KB43c1piScsVb1ccrvg44gcAAP//AwBJQAYTlRKMEAAAAABJRU5ErkJggg==) bottom left repeat-x;-webkit-border-radius:6px;}
h2{margin:4px 0px;font-weight:bold;}
a{color:black;text-decoration:none;}
img.disclose{float:right;}
.clear{clear:both;}

.url{color:#4d4d70;font-size:13px;}
div.clicked{background-color:#48e;background:-webkit-gradient(linear, left top, left bottom, from(#48e), to(#26c));}
div.row{position:relative;padding:10px;background:transparent url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAIGNIUk0AAG4nAABzrwAA90AAAIaOAABt7QAA6uUAADHIAAAYZWUn6MsAAABHSURBVHja7M3BCQAgDATBU3z49iME++8mWFIs40B2GphWVXLoMiEmJiYmJiYmJib+KB43c1piScsVb1ccrvg44gcAAP//AwBJQAYTlRKMEAAAAABJRU5ErkJggg==) bottom left repeat-x;}
h2{margin:10px 9px;}
.row h2{margin:0 0 3px;}
.row p{line-height:1.33em;margin:0 0 7px;color:#222;}

hr{border:none;height:1px;background-color:#ddd;margin:0;clear:both;-webkit-box-shadow:#fff 0 1px 0;}
.row .left{font-weight:bold;}
.normal .left{min-width:100px;float:left;padding-right:5px;} /* remove min- to wrap */
.normal .right{color:#444;text-align:right;float:right;}
.normal-right .right{color:#444;float:right;text-align:right}
.alerttype .right{color:#bd0700;font-size:16px;font-weight:bold;text-align:right}
.score .right{position:absolute;display:block;top:7px;right:8px;width:25px;font-size:16px;font-weight:bold;color:#fff;text-align:center;padding:2px;text-shadow: rgba(0, 0, 0, 0.2) 0 -1px 0;}

.score .right{-webkit-border-radius:4px;border-bottom:1px solid #888 !important;}

/* self-clear floats */
.clearfix {display: inline-block}
.clearfix {display: block}
.clearfix:after{content:".";display:block;height:0;clear:both;visibility:hidden;}
</style>
<body>
$sections$
<div id="paddingfortoolbar" style="height:50px;"></div>
</body>

