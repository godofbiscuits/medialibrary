package com.medallia.mobile.test.framework.querier.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import com.medallia.mobile.test.framework.querier.Querier;

/**
 * Trivial annotation used to specify that a method is not implemented in the 
 * different implementations of {@link Querier}.
 * 
 * @author fernando
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface NotImplemented {
}
