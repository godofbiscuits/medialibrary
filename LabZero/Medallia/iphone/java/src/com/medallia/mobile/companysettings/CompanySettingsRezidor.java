package com.medallia.mobile.companysettings;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.medallia.api.orm.Field;
import com.medallia.api.orm.Field.StringField;
import com.medallia.mobile.DetailFields;
import com.medallia.mobile.DetailFieldsDefaultBase;
import com.medallia.mobile.FieldWithColor;
import com.medallia.mobile.RequestParameterMap.LogonInfo;
import common.Pair;

public class CompanySettingsRezidor extends CompanySettingsDefaultBase {

	public CompanySettingsRezidor(LogonInfo req) {
		super(req);
	}

	@Override
	protected String getCompany() {
		return "rezidor";
	}

	@Override
	protected String getBaseURI0() {
		return "https://expressqa16.medallia.com"; // TODO for testing
//		return "https://carlson.medallia.com";
	}

	@Override
	public String getUnitTranslation() {
		return "Property";
	}

	@Override
	public Pair<Double, Double> getGraphYRange() {
		return Pair.cons(6.0, 10.0);
	}
	
	@Override
	public Date getStartTime() {
		return getDate(2009, 2, 29);
	}

	@Override
	public FieldWithColor getMainScoreField() {
		return new FieldWithColor("ov_service", "Overall service");	// q_ov_service
	}

	@Override
	public List<FieldWithColor> getScoreFields(String widgetRequestAction) {
		List<FieldWithColor> list = new ArrayList<FieldWithColor>();
		list.add(new FieldWithColor("h_rezidor_gqi", "GQI", "h_rezidor_gqi_count", false));	// r_h_rezidor_gqi
		list.add(new FieldWithColor("h_rezidor_gsi", "GSI", "h_rezidor_gsi_count", false));	// r_h_rezidor_gsi
		return list;
	}

	@Override
	public DetailFields getDetailFields() {
		return new DetailFieldsDefaultBase() {
			@Override
			public StringField getPhoneField() {
                return new StringField("phone", "Phone");
			}

			@Override
			public List<StringField> getNameFields() {
				List<StringField> list = new ArrayList<StringField>();
				list.add(new StringField("rezidor_anon_name", "Name"));	// k_rezidor_anon_name
				return list;
			}

			@Override
			public List<Field> getParticipantFields() {
				List<Field> list = new ArrayList<Field>();
				return list;
			}

			@Override
			public List<StringField> getCommentFields() {
				List<StringField> list = new ArrayList<StringField>();
				list.add(new StringField("h_rezidor_com_make_more_sat", "How To Improve Satisfaction"));	// q_h_rezidor_com_make_more_sat
				list.add(new StringField("h_rezidor_positive_comment", "Positive comments about staff"));	// q_h_rezidor_positive_comment
				return list;
			}

			@Override
			public List<FieldWithColor> getScoreFields() {
				List<FieldWithColor> list = new ArrayList<FieldWithColor>();
				list.add(new FieldWithColor("ov_experience", "Overall experience"));						// q_ov_experience
				list.add(new FieldWithColor("rezidor_loyalty_recommend", "Likelihood to recommend"));	// q_rezidor_loyalty_recommend
				return list;
			}
		};
	}

	@Override
    public List<FieldWithColor> getScorecardFields() {
		return getScoreFields("");
    }

}
