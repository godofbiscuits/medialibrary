package com.medallia.mobile.dto.builders;
import java.util.*;
import com.medallia.mobile.*;
import com.medallia.mobile.dto.*;
import com.medallia.mobile.dto.builders.*;
import com.medallia.mobile.color.UIColor;

// These files are generated automatically from declarations in DataFormat.h; see convert.pl for details.
//
// Do *not* modify this file directly!




@SuppressWarnings("unused")
public class Build {


/**
 *  This is the type of the main file sent to the client upon login.
 */
public static class ConfigFileBuilder extends Builder<ConfigFile> {

	@Required List<AltTable> tables;

	/**
	 *  The (shared) lists of values such as property names, age groups, etc (alternative sets)
	 */
	public ConfigFileBuilder tables(List<AltTable> x) {
		tables = x;
		return this;
	}

	@Required String userKey;

	/**
	 *  Some global user identifier -- used to keep different users' settings separate
	 */
	public ConfigFileBuilder userKey(String x) {
		userKey = x;
		return this;
	}

	@Required List<WidgetType> scoresWidgetTypes;

	/**
	 *  All the possible widget types the user may install on the scores dashboard (scoreboard)
	 */
	public ConfigFileBuilder scoresWidgetTypes(List<WidgetType> x) {
		scoresWidgetTypes = x;
		return this;
	}

	@Required List<WidgetType> rankerWidgetTypes;

	/**
	 *  All the possible widget types the user may install on the ranker
	 */
	public ConfigFileBuilder rankerWidgetTypes(List<WidgetType> x) {
		rankerWidgetTypes = x;
		return this;
	}

	@Required List<WidgetInstance> initialScoresWidgets;

	/**
	 *  The initial list of scores widgets (installed if the user has no widgets)
	 */
	public ConfigFileBuilder initialScoresWidgets(List<WidgetInstance> x) {
		initialScoresWidgets = x;
		return this;
	}

	@Required List<WidgetInstance> initialRankerWidgets;

	/**
	 *  The initial list of ranker widgets (installed if the user has no widgets)
	 */
	public ConfigFileBuilder initialRankerWidgets(List<WidgetInstance> x) {
		initialRankerWidgets = x;
		return this;
	}

	@Required WidgetType responsesFilterConfig;

	/**
	 *  Config used to set filters for the Response Investigator
	 */
	public ConfigFileBuilder responsesFilterConfig(WidgetType x) {
		responsesFilterConfig = x;
		return this;
	}

	@Required WidgetInstance initialResponseFilter;

	/**
	 *  Default values for the RI filter
	 */
	public ConfigFileBuilder initialResponseFilter(WidgetInstance x) {
		initialResponseFilter = x;
		return this;
	}

	@Required String nrOpenAlerts;

	/**
	 *  Number of open alerts
	 */
	public ConfigFileBuilder nrOpenAlerts(String x) {
		nrOpenAlerts = x;
		return this;
	}

	public ConfigFile get() {
		check();
		return new ConfigFile() {
			public List<AltTable> tables() {
				return tables;
			}
			public String userKey() {
				return userKey;
			}
			public List<WidgetType> scoresWidgetTypes() {
				return scoresWidgetTypes;
			}
			public List<WidgetType> rankerWidgetTypes() {
				return rankerWidgetTypes;
			}
			public List<WidgetInstance> initialScoresWidgets() {
				return initialScoresWidgets;
			}
			public List<WidgetInstance> initialRankerWidgets() {
				return initialRankerWidgets;
			}
			public WidgetType responsesFilterConfig() {
				return responsesFilterConfig;
			}
			public WidgetInstance initialResponseFilter() {
				return initialResponseFilter;
			}
			public String nrOpenAlerts() {
				return nrOpenAlerts;
			}
		};
	}
}


/**
 *  a WidgetType represents a single type of dashboard the user can install, e.g. Scoreboard, Response Investigator, Ranker
 */
public static class WidgetTypeBuilder extends Builder<WidgetType> {

	@Required String name;

	/**
	 *  "Scoreboard"
	 */
	public WidgetTypeBuilder name(String x) {
		name = x;
		return this;
	}

	String desc;

	/**
	 *  "Overview of foo, feh and fiddle"
	 */
	public WidgetTypeBuilder desc(String x) {
		desc = x;
		return this;
	}

	String key;

	/**
	 *  "score"
	 */
	public WidgetTypeBuilder key(String x) {
		key = x;
		return this;
	}

	@Required String viewClass;

	/**
	 *  "ScoreWidget" (class name)
	 */
	public WidgetTypeBuilder viewClass(String x) {
		viewClass = x;
		return this;
	}

	@Required String urlBase;

	/**
	 *  base URL for fetching data
	 */
	public WidgetTypeBuilder urlBase(String x) {
		urlBase = x;
		return this;
	}

	String responsesFilterButtonKey;

	/**
	 *  key for the responses filter button to view when clicking through to see underlying responses
	 */
	public WidgetTypeBuilder responsesFilterButtonKey(String x) {
		responsesFilterButtonKey = x;
		return this;
	}

	@Required List<ConfigSection> configSections;

	/**
	 *  List of sections, each containing one or more edit controls
	 */
	public WidgetTypeBuilder configSections(List<ConfigSection> x) {
		configSections = x;
		return this;
	}

	Map<String,String> arguments;

	/**
	 *  Optional list of arguments for the widget type (parameter key and default value). Used for parameters that are hardcoded to the WidgetType and not part of the Config (i.e. a ConfigPartType), for example ranker scope.
	 */
	public WidgetTypeBuilder arguments(Map<String,String> x) {
		arguments = x;
		return this;
	}

	public WidgetType get() {
		check();
		return new WidgetType() {
			public String name() {
				return name;
			}
			public String desc() {
				return desc;
			}
			public String key() {
				return key;
			}
			public String viewClass() {
				return viewClass;
			}
			public String urlBase() {
				return urlBase;
			}
			public String responsesFilterButtonKey() {
				return responsesFilterButtonKey;
			}
			public List<ConfigSection> configSections() {
				return configSections;
			}
			public Map<String,String> arguments() {
				return arguments;
			}
		};
	}
}


/**
 *  Settings pages are divided into sections
 */
public static class ConfigSectionBuilder extends Builder<ConfigSection> {

	@Required String name;

	/**
	 *  Section header
	 */
	public ConfigSectionBuilder name(String x) {
		name = x;
		return this;
	}

	@Required List<ConfigPart> parts;

	/**
	 *  List of parts
	 */
	public ConfigSectionBuilder parts(List<ConfigPart> x) {
		parts = x;
		return this;
	}

	public ConfigSection get() {
		check();
		return new ConfigSection() {
			public String name() {
				return name;
			}
			public List<ConfigPart> parts() {
				return parts;
			}
		};
	}
}


/**
 *  One table cell on the settings page. Can be a CECheckbox, CEDropdown or CERange (the double slider).
 */
public static class ConfigPartBuilder extends Builder<ConfigPart> {

	ConfigPartType type;

	/**
	 *  the type of control
	 */
	public ConfigPartBuilder type(ConfigPartType x) {
		type = x;
		return this;
	}

	ClickableHeaderElement headerElement;

	/**
	 *  which clickable header element this configpart is associated with
	 */
	public ConfigPartBuilder headerElement(ClickableHeaderElement x) {
		headerElement = x;
		return this;
	}

	@Required String key;

	/**
	 *  "ev_filter_foo" (the server's name for this field)
	 */
	public ConfigPartBuilder key(String x) {
		key = x;
		return this;
	}

	String name;

	/**
	 *  name shown ("Property")
	 */
	public ConfigPartBuilder name(String x) {
		name = x;
		return this;
	}

	String def;

	/**
	 *  default value
	 */
	public ConfigPartBuilder def(String x) {
		def = x;
		return this;
	}

	String table;

	/**
	 *  for CEDropdown, the name of the AltTable to use
	 */
	public ConfigPartBuilder table(String x) {
		table = x;
		return this;
	}

	double minValue;

	/**
	 *  for CERange
	 */
	public ConfigPartBuilder minValue(double x) {
		minValue = x;
		return this;
	}

	double maxValue;

	/**
	 *  for CERange
	 */
	public ConfigPartBuilder maxValue(double x) {
		maxValue = x;
		return this;
	}

	public ConfigPart get() {
		check();
		return new ConfigPart() {
			public ConfigPartType type() {
				return type;
			}
			public ClickableHeaderElement headerElement() {
				return headerElement;
			}
			public String key() {
				return key;
			}
			public String name() {
				return name;
			}
			public String def() {
				return def;
			}
			public String table() {
				return table;
			}
			public double minValue() {
				return minValue;
			}
			public double maxValue() {
				return maxValue;
			}
		};
	}
}


/**
 *  A batch of responses
 */
public static class ResponsesLumpBuilder extends Builder<ResponsesLump> {

	@Required String fetchMoreText;

	/**
	 *  The text shown at the end of the list
	 */
	public ResponsesLumpBuilder fetchMoreText(String x) {
		fetchMoreText = x;
		return this;
	}

	String fetchMoreURL;

	/**
	 *  If set, the URL to use to fetch the next ResponsesLump. If null, the button will open the config page instead.
	 */
	public ResponsesLumpBuilder fetchMoreURL(String x) {
		fetchMoreURL = x;
		return this;
	}

	@Required List<ResponsesEntry> entries;

	/**
	 *  The list of surveys.
	 */
	public ResponsesLumpBuilder entries(List<ResponsesEntry> x) {
		entries = x;
		return this;
	}

	@Required List<FilterButton> filterButtons;

	/**
	 *  List of filter buttons shown under the search bar.
	 */
	public ResponsesLumpBuilder filterButtons(List<FilterButton> x) {
		filterButtons = x;
		return this;
	}

	String nrOpenAlerts;

	/**
	 *  Number of open alerts (optional -- only sent when alerts_only is set)
	 */
	public ResponsesLumpBuilder nrOpenAlerts(String x) {
		nrOpenAlerts = x;
		return this;
	}

	public ResponsesLump get() {
		check();
		return new ResponsesLump() {
			public String fetchMoreText() {
				return fetchMoreText;
			}
			public String fetchMoreURL() {
				return fetchMoreURL;
			}
			public List<ResponsesEntry> entries() {
				return entries;
			}
			public List<FilterButton> filterButtons() {
				return filterButtons;
			}
			public String nrOpenAlerts() {
				return nrOpenAlerts;
			}
		};
	}
}


/**
 *  One entry in the list of responses. Contains only the data needed for the list.
 */
public static class ResponsesEntryBuilder extends Builder<ResponsesEntry> {

	@Required String uniqueIdentifier;

	/**
	 *  Some persistent identifier for this record
	 */
	public ResponsesEntryBuilder uniqueIdentifier(String x) {
		uniqueIdentifier = x;
		return this;
	}

	@Required String sectionHeader;

	/**
	 *  Adjacent records with the same header will be grouped together. At the moment we use the survey date here; that could change.
	 */
	public ResponsesEntryBuilder sectionHeader(String x) {
		sectionHeader = x;
		return this;
	}

	@Required String score;

	/**
	 *  The main score for this survey, e.g. "7"
	 */
	public ResponsesEntryBuilder score(String x) {
		score = x;
		return this;
	}

	String alert;

	/**
	 *  The name of the alert, if there is an alert for this survey. E.g. "R1"
	 */
	public ResponsesEntryBuilder alert(String x) {
		alert = x;
		return this;
	}

	@Required String name;

	/**
	 *  The header to show for each response, typically the customer name, e.g. "John Doe"
	 */
	public ResponsesEntryBuilder name(String x) {
		name = x;
		return this;
	}

	String subheader;

	/**
	 *  An optional sub-header to show under the main header, above the comment. E.g. "Palo Alto Store"
	 */
	public ResponsesEntryBuilder subheader(String x) {
		subheader = x;
		return this;
	}

	String text;

	/**
	 *  The comment of the survey, e.g. "My iPod didn't have a Centronics connector. Boo!"
	 */
	public ResponsesEntryBuilder text(String x) {
		text = x;
		return this;
	}

	@Required String detailsURL;

	/**
	 *  URL of details page; that page should return an HTML page
	 */
	public ResponsesEntryBuilder detailsURL(String x) {
		detailsURL = x;
		return this;
	}

	@Required UIColor scoreColor;

	/**
	 *  Color for the score background; the text is white, so use a darkish shade.
	 */
	public ResponsesEntryBuilder scoreColor(UIColor x) {
		scoreColor = x;
		return this;
	}

	public ResponsesEntry get() {
		check();
		return new ResponsesEntry() {
			public String uniqueIdentifier() {
				return uniqueIdentifier;
			}
			public String sectionHeader() {
				return sectionHeader;
			}
			public String score() {
				return score;
			}
			public String alert() {
				return alert;
			}
			public String name() {
				return name;
			}
			public String subheader() {
				return subheader;
			}
			public String text() {
				return text;
			}
			public String detailsURL() {
				return detailsURL;
			}
			public UIColor scoreColor() {
				return scoreColor;
			}
		};
	}
}


/**
 *  A filter button
 */
public static class FilterButtonBuilder extends Builder<FilterButton> {

	@Required String key;

	/**
	 *  Key used by server to filter results.
	 */
	public FilterButtonBuilder key(String x) {
		key = x;
		return this;
	}

	@Required String title;

	/**
	 *  Text shown on button.
	 */
	public FilterButtonBuilder title(String x) {
		title = x;
		return this;
	}

	public FilterButton get() {
		check();
		return new FilterButton() {
			public String key() {
				return key;
			}
			public String title() {
				return title;
			}
		};
	}
}


/**
 *  A list of units/unit groups
 */
public static class RankerDataBuilder extends Builder<RankerData> {

	@Required String benchTitle;

	/**
	 *  "vs 6m" - explains the benchmark period we are comparing to
	 */
	public RankerDataBuilder benchTitle(String x) {
		benchTitle = x;
		return this;
	}

	@Required List<RankerUnit> entries;

	/**
	 *  The list of units/unit groups
	 */
	public RankerDataBuilder entries(List<RankerUnit> x) {
		entries = x;
		return this;
	}

	public RankerData get() {
		check();
		return new RankerData() {
			public String benchTitle() {
				return benchTitle;
			}
			public List<RankerUnit> entries() {
				return entries;
			}
		};
	}
}


/**
 *  One entry in the list of units/unit groups
 */
public static class RankerUnitBuilder extends Builder<RankerUnit> {

	@Required String uniqueIdentifier;

	/**
	 *  Some persistent identifier for this record
	 */
	public RankerUnitBuilder uniqueIdentifier(String x) {
		uniqueIdentifier = x;
		return this;
	}

	@Required String name;

	/**
	 *  "Palo Alto"
	 */
	public RankerUnitBuilder name(String x) {
		name = x;
		return this;
	}

	@Required String score;

	/**
	 *  "58.7", this is the number that is actually displayed on the client
	 */
	public RankerUnitBuilder score(String x) {
		score = x;
		return this;
	}

	@Required String difference;

	/**
	 *  "+0.2", this is the number that is actually displayed on the client
	 */
	public RankerUnitBuilder difference(String x) {
		difference = x;
		return this;
	}

	@Required double scoreNumeric;

	/**
	 *  Numeric version of the score. Used for sorting on the client.
	 */
	public RankerUnitBuilder scoreNumeric(double x) {
		scoreNumeric = x;
		return this;
	}

	@Required double differenceNumeric;

	/**
	 *  Numeric version of the difference. Used for sorting on the client.
	 */
	public RankerUnitBuilder differenceNumeric(double x) {
		differenceNumeric = x;
		return this;
	}

	@Required UIColor diffColor;

	/**
	 *  The color used for the "+0.2" text
	 */
	public RankerUnitBuilder diffColor(UIColor x) {
		diffColor = x;
		return this;
	}

	String selected;

	/**
	 *  missing if unit is not selected
	 */
	public RankerUnitBuilder selected(String x) {
		selected = x;
		return this;
	}

	public RankerUnit get() {
		check();
		return new RankerUnit() {
			public String uniqueIdentifier() {
				return uniqueIdentifier;
			}
			public String name() {
				return name;
			}
			public String score() {
				return score;
			}
			public String difference() {
				return difference;
			}
			public double scoreNumeric() {
				return scoreNumeric;
			}
			public double differenceNumeric() {
				return differenceNumeric;
			}
			public UIColor diffColor() {
				return diffColor;
			}
			public String selected() {
				return selected;
			}
		};
	}
}


/**
 *  One table of alternatives (used by ConfigParts of type CEDropdown)
 */
public static class AltTableBuilder extends Builder<AltTable> {

	@Required String key;

	/**
	 *  Key (used to reference this from ConfigPart.table())
	 */
	public AltTableBuilder key(String x) {
		key = x;
		return this;
	}

	@Required List<AltSection> sections;

	/**
	 *  List of sections; if you don't want the alternatives grouped, use a single section with no header.
	 */
	public AltTableBuilder sections(List<AltSection> x) {
		sections = x;
		return this;
	}

	public AltTable get() {
		check();
		return new AltTable() {
			public String key() {
				return key;
			}
			public List<AltSection> sections() {
				return sections;
			}
		};
	}
}


/**
 *  Alternatives may be shown in groups
 */
public static class AltSectionBuilder extends Builder<AltSection> {

	String header;

	/**
	 *  Group header
	 */
	public AltSectionBuilder header(String x) {
		header = x;
		return this;
	}

	@Required List<Alt> alternatives;

	/**
	 *  List of alternatives in the group
	 */
	public AltSectionBuilder alternatives(List<Alt> x) {
		alternatives = x;
		return this;
	}

	public AltSection get() {
		check();
		return new AltSection() {
			public String header() {
				return header;
			}
			public List<Alt> alternatives() {
				return alternatives;
			}
		};
	}
}


/**
 *  A single alternative
 */
public static class AltBuilder extends Builder<Alt> {

	@Required String key;

	/**
	 *  "42" - must be unique within an AltTable
	 */
	public AltBuilder key(String x) {
		key = x;
		return this;
	}

	@Required String value;

	/**
	 *  "Quarter to Date" - full text, to be displayed in the table when making selection
	 */
	public AltBuilder value(String x) {
		value = x;
		return this;
	}

	double level;

	/**
	 *  Indentation level. 0 is left-aligned, 1 is slightly indented, 2 is more indented, etc.
	 */
	public AltBuilder level(double x) {
		level = x;
		return this;
	}

	String headerValue;

	/**
	 *  "QTD" - an alternative text to be displayed in report header
	 */
	public AltBuilder headerValue(String x) {
		headerValue = x;
		return this;
	}

	AltTable table;

	/**
	 *  Optional link to new alternative table. Used if this alternative links to a new screen of alternatives.
	 */
	public AltBuilder table(AltTable x) {
		table = x;
		return this;
	}

	public Alt get() {
		check();
		return new Alt() {
			public String key() {
				return key;
			}
			public String value() {
				return value;
			}
			public double level() {
				return level;
			}
			public String headerValue() {
				return headerValue;
			}
			public AltTable table() {
				return table;
			}
		};
	}
}


/**
 *  An instance of a Widget -- the user may have three instances of the scoreboard, for, uhm, instance.
 */
public static class WidgetInstanceBuilder extends Builder<WidgetInstance> {

	@Required String type;

	/**
	 *  The type of widget; this should match the key() of a WidgetType
	 */
	public WidgetInstanceBuilder type(String x) {
		type = x;
		return this;
	}

	@Required Map<String,String> arguments;

	/**
	 *  Arguments; this is a map from ConfigPart.key() to values
	 */
	public WidgetInstanceBuilder arguments(Map<String,String> x) {
		arguments = x;
		return this;
	}

	public WidgetInstance get() {
		check();
		return new WidgetInstance() {
			public String type() {
				return type;
			}
			public Map<String,String> arguments() {
				return arguments;
			}
		};
	}
}


/**
 *  Each scoreboard widget has one page
 */
public static class SWPageBuilder extends Builder<SWPage> {

	String title;

	/**
	 *  The main title for this widget (defaults to widgetType.name if not present)
	 */
	public SWPageBuilder title(String x) {
		title = x;
		return this;
	}

	String subTitle;

	/**
	 *  The subtitle (defaults to a description of the current filters if not present)
	 */
	public SWPageBuilder subTitle(String x) {
		subTitle = x;
		return this;
	}

	@Required String benchTitle;

	/**
	 *  "vs 6m" - explains the benchmark period we are comparing to
	 */
	public SWPageBuilder benchTitle(String x) {
		benchTitle = x;
		return this;
	}

	@Required List<TimePeriod> timePeriods;

	/**
	 *  List of time periods. There will be as many of these as graphs for each line.
	 */
	public SWPageBuilder timePeriods(List<TimePeriod> x) {
		timePeriods = x;
		return this;
	}

	@Required List<LineStyle> lineStyles;

	/**
	 *  List of line styles. There will be as many of these as lines in each graph.
	 */
	public SWPageBuilder lineStyles(List<LineStyle> x) {
		lineStyles = x;
		return this;
	}

	@Required List<SWLine> lines;

	/**
	 *  The page contents
	 */
	public SWPageBuilder lines(List<SWLine> x) {
		lines = x;
		return this;
	}

	public SWPage get() {
		check();
		return new SWPage() {
			public String title() {
				return title;
			}
			public String subTitle() {
				return subTitle;
			}
			public String benchTitle() {
				return benchTitle;
			}
			public List<TimePeriod> timePeriods() {
				return timePeriods;
			}
			public List<LineStyle> lineStyles() {
				return lineStyles;
			}
			public List<SWLine> lines() {
				return lines;
			}
		};
	}
}


/**
 *  Represents a single line of the Scoreboard Widget
 */
public static class SWLineBuilder extends Builder<SWLine> {

	@Required String tableCellNibName;

	/**
	 *  for now, this must be ScoreWidgetNum (the only nib supported at the moment)
	 */
	public SWLineBuilder tableCellNibName(String x) {
		tableCellNibName = x;
		return this;
	}

	@Required String title;

	/**
	 *  "Responses" - the text shown in the large font
	 */
	public SWLineBuilder title(String x) {
		title = x;
		return this;
	}

	@Required String value;

	/**
	 *  "8.4" - the main number, it's a string so that it can be customized and formatted on the server side
	 */
	public SWLineBuilder value(String x) {
		value = x;
		return this;
	}

	String difference;

	/**
	 *  For ScoreWidgetNum only, the "+4.7" difference against the benchmark
	 */
	public SWLineBuilder difference(String x) {
		difference = x;
		return this;
	}

	String scoreBackgroundColor;

	/**
	 *  For ScoreWidgetNum only, the color for the number background; "green"/"yellow"/"red" for now
	 */
	public SWLineBuilder scoreBackgroundColor(String x) {
		scoreBackgroundColor = x;
		return this;
	}

	UIColor diffColor;

	/**
	 *  For ScoreWidgetNum only, the color used for the "+4.7" text; white or red in the mockups
	 */
	public SWLineBuilder diffColor(UIColor x) {
		diffColor = x;
		return this;
	}

	List<Graph> graphSmall;

	/**
	 *  For ScoreWidgetNum only; small version of graph for each time period. There will be as many of these as time periods.
	 */
	public SWLineBuilder graphSmall(List<Graph> x) {
		graphSmall = x;
		return this;
	}

	List<Graph> graphLarge;

	/**
	 *  For ScoreWidgetNum only; large version of graph for each time period. There will be as many of these as time periods.
	 */
	public SWLineBuilder graphLarge(List<Graph> x) {
		graphLarge = x;
		return this;
	}

	public SWLine get() {
		check();
		return new SWLine() {
			public String tableCellNibName() {
				return tableCellNibName;
			}
			public String title() {
				return title;
			}
			public String value() {
				return value;
			}
			public String difference() {
				return difference;
			}
			public String scoreBackgroundColor() {
				return scoreBackgroundColor;
			}
			public UIColor diffColor() {
				return diffColor;
			}
			public List<Graph> graphSmall() {
				return graphSmall;
			}
			public List<Graph> graphLarge() {
				return graphLarge;
			}
		};
	}
}


/**
 *  Time period.
 */
public static class TimePeriodBuilder extends Builder<TimePeriod> {

	@Required String title;

	/**
	 *  Short time period title ("1w", "1m", "3m", etc).
	 */
	public TimePeriodBuilder title(String x) {
		title = x;
		return this;
	}

	List<GraphLabel> labelsSmallGraph;

	/**
	 *  List of graph x labels for the small graph. (These will be identical for all small graphs and are thus stored outside the graph object.)
	 */
	public TimePeriodBuilder labelsSmallGraph(List<GraphLabel> x) {
		labelsSmallGraph = x;
		return this;
	}

	List<GraphLabel> labelsLargeGraph;

	/**
	 *  List of graph x labels for the large graph. (These will be identical for all large graphs and are thus stored outside the graph object.)
	 */
	public TimePeriodBuilder labelsLargeGraph(List<GraphLabel> x) {
		labelsLargeGraph = x;
		return this;
	}

	public TimePeriod get() {
		check();
		return new TimePeriod() {
			public String title() {
				return title;
			}
			public List<GraphLabel> labelsSmallGraph() {
				return labelsSmallGraph;
			}
			public List<GraphLabel> labelsLargeGraph() {
				return labelsLargeGraph;
			}
		};
	}
}


/**
 *  Line style.
 */
public static class LineStyleBuilder extends Builder<LineStyle> {

	String featured;

	/**
	 *  Missing if line is not featured.
	 */
	public LineStyleBuilder featured(String x) {
		featured = x;
		return this;
	}

	@Required String legendText;

	/**
	 *  Text describing this line that may be shown in a legend.
	 */
	public LineStyleBuilder legendText(String x) {
		legendText = x;
		return this;
	}

	@Required UIColor color;

	/**
	 *  Color of line.
	 */
	public LineStyleBuilder color(UIColor x) {
		color = x;
		return this;
	}

	public LineStyle get() {
		check();
		return new LineStyle() {
			public String featured() {
				return featured;
			}
			public String legendText() {
				return legendText;
			}
			public UIColor color() {
				return color;
			}
		};
	}
}


/**
 *  Graph x label.
 */
public static class GraphLabelBuilder extends Builder<GraphLabel> {

	@Required String label;

	/**
	 *  Graph x label.
	 */
	public GraphLabelBuilder label(String x) {
		label = x;
		return this;
	}

	@Required double position;

	/**
	 *  Label x position, ordered and ranged between 0 and 1 (inclusive).
	 */
	public GraphLabelBuilder position(double x) {
		position = x;
		return this;
	}

	public GraphLabel get() {
		check();
		return new GraphLabel() {
			public String label() {
				return label;
			}
			public double position() {
				return position;
			}
		};
	}
}


/**
 *  A graph. Contains data for a list of lines.
 */
public static class GraphBuilder extends Builder<Graph> {

	String minY;

	/**
	 *  Suggested graph y range minimum (will be overridden if data lower than this appear).
	 */
	public GraphBuilder minY(String x) {
		minY = x;
		return this;
	}

	String maxY;

	/**
	 *  Suggested graph y range maximum (will be overridden if data higher than this appear).
	 */
	public GraphBuilder maxY(String x) {
		maxY = x;
		return this;
	}

	@Required List<LineData> lineData;

	/**
	 *  Data for a list of lines. There will be as many lines as line styles defined in the SWPage.
	 */
	public GraphBuilder lineData(List<LineData> x) {
		lineData = x;
		return this;
	}

	public Graph get() {
		check();
		return new Graph() {
			public String minY() {
				return minY;
			}
			public String maxY() {
				return maxY;
			}
			public List<LineData> lineData() {
				return lineData;
			}
		};
	}
}


/**
 *  Line data. Contains a list of data points for a single line.
 */
public static class LineDataBuilder extends Builder<LineData> {

	@Required String xData;

	/**
	 *  List of pipe-separated x values for the line, ordered and ranged between 0 and 1 (inclusive). May be empty if there is no data. Leading zeros are omitted, e.g. '0.25' is written as '.25', but a single '0' is still written as '0'. Each x point has a corresponding y point.
	 */
	public LineDataBuilder xData(String x) {
		xData = x;
		return this;
	}

	@Required String yData;

	/**
	 *  List of pipe-separated y values for the line. May be empty if there is no data. Need not conform to the range given by [minY, maxY]. Leading zeros are omitted, e.g. '0.25' is written as '.25', but a single '0' is still written as '0'. Each y point has a corresponding x point.
	 */
	public LineDataBuilder yData(String x) {
		yData = x;
		return this;
	}

	public LineData get() {
		check();
		return new LineData() {
			public String xData() {
				return xData;
			}
			public String yData() {
				return yData;
			}
		};
	}
}


/**
 *  Scorecard. Contains a summary of scores for a set of surveys matching a given search string, e.g. the name of an employee.
 */
public static class ScorecardBuilder extends Builder<Scorecard> {

	@Required String scorecardName;

	/**
	 *  The name of the scorecard (who/what the scorecard is for).
	 */
	public ScorecardBuilder scorecardName(String x) {
		scorecardName = x;
		return this;
	}

	@Required double numberOfResponses;

	/**
	 *  The number of responses over the past 12 months.
	 */
	public ScorecardBuilder numberOfResponses(double x) {
		numberOfResponses = x;
		return this;
	}

	@Required List<ScorecardScore> scores;

	/**
	 *  The list of scores for the past 12 months.
	 */
	public ScorecardBuilder scores(List<ScorecardScore> x) {
		scores = x;
		return this;
	}

	@Required ResponsesLump responsesLump;

	/**
	 *  The lump of responses for this scorecard.
	 */
	public ScorecardBuilder responsesLump(ResponsesLump x) {
		responsesLump = x;
		return this;
	}

	public Scorecard get() {
		check();
		return new Scorecard() {
			public String scorecardName() {
				return scorecardName;
			}
			public double numberOfResponses() {
				return numberOfResponses;
			}
			public List<ScorecardScore> scores() {
				return scores;
			}
			public ResponsesLump responsesLump() {
				return responsesLump;
			}
		};
	}
}


/**
 *  An individual score shown on the scorecard.
 */
public static class ScorecardScoreBuilder extends Builder<ScorecardScore> {

	@Required String title;

	/**
	 *  Score title, e.g. "Overall satisfaction".
	 */
	public ScorecardScoreBuilder title(String x) {
		title = x;
		return this;
	}

	@Required String value;

	/**
	 *  Score, e.g. "8.4". It's a string so that it can be customized and formatted on the server side.
	 */
	public ScorecardScoreBuilder value(String x) {
		value = x;
		return this;
	}

	String scoreBackgroundColor;

	/**
	 *  The color for the score background; "green"/"yellow"/"red" for now
	 */
	public ScorecardScoreBuilder scoreBackgroundColor(String x) {
		scoreBackgroundColor = x;
		return this;
	}

	public ScorecardScore get() {
		check();
		return new ScorecardScore() {
			public String title() {
				return title;
			}
			public String value() {
				return value;
			}
			public String scoreBackgroundColor() {
				return scoreBackgroundColor;
			}
		};
	}
}


/**
 *  A single survey. Contains HTML for rendering in WebKit, and information about actions like adding note, calling customer, forwarding email, etc.
 */
public static class SingleResponseBuilder extends Builder<SingleResponse> {

	@Required String html;

	/**
	 *  HTML to be rendered by WebKit. This should ideally be self-contained; images are slow on a cell phone.
	 */
	public SingleResponseBuilder html(String x) {
		html = x;
		return this;
	}

	@Required String name;

	/**
	 *  survey taker name (needed for notes/alerts screen)
	 */
	public SingleResponseBuilder name(String x) {
		name = x;
		return this;
	}

	String canAddNote;

	/**
	 *  if present, the user has permission to add notes
	 */
	public SingleResponseBuilder canAddNote(String x) {
		canAddNote = x;
		return this;
	}

	String customerPhone;

	/**
	 *  if present, the user can call the customer
	 */
	public SingleResponseBuilder customerPhone(String x) {
		customerPhone = x;
		return this;
	}

	String scoreboardBaseURL;

	/**
	 *  the base URL to which parameters for the specific selected scoreboard should be appended in order to load it
	 */
	public SingleResponseBuilder scoreboardBaseURL(String x) {
		scoreboardBaseURL = x;
		return this;
	}

	@Required List<LogNote> notes;

	/**
	 *  log notes
	 */
	public SingleResponseBuilder notes(List<LogNote> x) {
		notes = x;
		return this;
	}

	String alertName;

	/**
	 *  Name of alert, if there is an alert
	 */
	public SingleResponseBuilder alertName(String x) {
		alertName = x;
		return this;
	}

	String alertStatus;

	/**
	 *  Status of alert, if there is an alert
	 */
	public SingleResponseBuilder alertStatus(String x) {
		alertStatus = x;
		return this;
	}

	@Required List<AlertAction> alertActions;

	/**
	 *  Alert actions (e.g. "close alert")
	 */
	public SingleResponseBuilder alertActions(List<AlertAction> x) {
		alertActions = x;
		return this;
	}

	EmailAction emailAction;

	/**
	 *  optional email action
	 */
	public SingleResponseBuilder emailAction(EmailAction x) {
		emailAction = x;
		return this;
	}

	String nrOpenAlerts;

	/**
	 *  Number of open alerts (optional -- only sent after an alert has been closed)
	 */
	public SingleResponseBuilder nrOpenAlerts(String x) {
		nrOpenAlerts = x;
		return this;
	}

	public SingleResponse get() {
		check();
		return new SingleResponse() {
			public String html() {
				return html;
			}
			public String name() {
				return name;
			}
			public String canAddNote() {
				return canAddNote;
			}
			public String customerPhone() {
				return customerPhone;
			}
			public String scoreboardBaseURL() {
				return scoreboardBaseURL;
			}
			public List<LogNote> notes() {
				return notes;
			}
			public String alertName() {
				return alertName;
			}
			public String alertStatus() {
				return alertStatus;
			}
			public List<AlertAction> alertActions() {
				return alertActions;
			}
			public EmailAction emailAction() {
				return emailAction;
			}
			public String nrOpenAlerts() {
				return nrOpenAlerts;
			}
		};
	}
}


/**
 *  A survey log note
 */
public static class LogNoteBuilder extends Builder<LogNote> {

	@Required String date;

	/**
	 *  Date note was added, e.g. "January 31, 2010"
	 */
	public LogNoteBuilder date(String x) {
		date = x;
		return this;
	}

	@Required String user;

	/**
	 *  User that added the note, e.g. "Ryan Smith" or "System"
	 */
	public LogNoteBuilder user(String x) {
		user = x;
		return this;
	}

	@Required String title;

	/**
	 *  Note title, e.g. "Alert Closed"
	 */
	public LogNoteBuilder title(String x) {
		title = x;
		return this;
	}

	@Required String content;

	/**
	 *  Note content, e.g. "I spoke to the guest and resolved the issue."
	 */
	public LogNoteBuilder content(String x) {
		content = x;
		return this;
	}

	public LogNote get() {
		check();
		return new LogNote() {
			public String date() {
				return date;
			}
			public String user() {
				return user;
			}
			public String title() {
				return title;
			}
			public String content() {
				return content;
			}
		};
	}
}


/**
 *  An alert action (e.g. "close alert")
 */
public static class AlertActionBuilder extends Builder<AlertAction> {

	@Required String text;

	/**
	 *  Button text
	 */
	public AlertActionBuilder text(String x) {
		text = x;
		return this;
	}

	@Required String action;

	/**
	 *  Alert action that will be sent back to the servlet and then forwarded to the api ("closed", "resolved", "in_progress")
	 */
	public AlertActionBuilder action(String x) {
		action = x;
		return this;
	}

	public AlertAction get() {
		check();
		return new AlertAction() {
			public String text() {
				return text;
			}
			public String action() {
				return action;
			}
		};
	}
}


/**
 *  Information about an email template to be edited & sent by the user. Typically triggered by a button in the action menu
 */
public static class EmailActionBuilder extends Builder<EmailAction> {

	String to;

	/**
	 *  If set, the to-field will be filled out with this
	 */
	public EmailActionBuilder to(String x) {
		to = x;
		return this;
	}

	@Required String subject;

	/**
	 *  Email subject
	 */
	public EmailActionBuilder subject(String x) {
		subject = x;
		return this;
	}

	@Required String body;

	/**
	 *  Email body
	 */
	public EmailActionBuilder body(String x) {
		body = x;
		return this;
	}

	String html;

	/**
	 *  If set, the body will be interpreted as HTML. (boolean - null means no, anything else is yes)
	 */
	public EmailActionBuilder html(String x) {
		html = x;
		return this;
	}

	public EmailAction get() {
		check();
		return new EmailAction() {
			public String to() {
				return to;
			}
			public String subject() {
				return subject;
			}
			public String body() {
				return body;
			}
			public String html() {
				return html;
			}
		};
	}
}


public static class Constants {
	/**  increase this whenever the data model changes; please note that fact in the commit message as well */
	public static final int CURRENT_CLIENT_VERSION = 60;
	/**  first client version that can read this version; if the changes are incompatible, set this to the above value */
	public static final int FIRST_COMPATIBLE_CLIENT_VERSION = 50;
	/**  client-side only: last active scoreboard page */
	public static final String VIEWSTATE_SUBPAGE = "vs.subpage";
	/**  client-side only: most recent survey id (to calculate new count) */
	public static final String VIEWSTATE_LAST_SURVEYID = "vs.lastid";
	/**  this setting appears as a regular ConfigPart, but is only used by the client */
	public static final String PREVIEW_FONT_SIZE = "preview_font_size";
	public static final int PREVIEW_FONT_SIZE_DEFAULT = 13;
	public static final String PREVIEW_LINE_COUNT = "preview_line_count";
	public static final int PREVIEW_LINE_COUNT_DEFAULT = 3;
	public static final String COMMENTS_ONLY_KEY = "comments_only";
	/**  this setting is provided by the client as part of the request for a response list; it is *not* specified as a ConfigPart */
	public static final String RESPONSES_TEXT_SEARCH_PARAMETER = "response_text";
	
}

	/**
	 *  This is the type of the main file sent to the client upon login.
	 */
	public static ConfigFileBuilder configFile() {
		return new ConfigFileBuilder();
	}

	/**
	 *  a WidgetType represents a single type of dashboard the user can install, e.g. Scoreboard, Response Investigator, Ranker
	 */
	public static WidgetTypeBuilder widgetType() {
		return new WidgetTypeBuilder();
	}

	/**
	 *  Settings pages are divided into sections
	 */
	public static ConfigSectionBuilder configSection() {
		return new ConfigSectionBuilder();
	}

	/**
	 *  One table cell on the settings page. Can be a CECheckbox, CEDropdown or CERange (the double slider).
	 */
	public static ConfigPartBuilder configPart() {
		return new ConfigPartBuilder();
	}

	/**
	 *  A batch of responses
	 */
	public static ResponsesLumpBuilder responsesLump() {
		return new ResponsesLumpBuilder();
	}

	/**
	 *  One entry in the list of responses. Contains only the data needed for the list.
	 */
	public static ResponsesEntryBuilder responsesEntry() {
		return new ResponsesEntryBuilder();
	}

	/**
	 *  A filter button
	 */
	public static FilterButtonBuilder filterButton() {
		return new FilterButtonBuilder();
	}

	/**
	 *  A list of units/unit groups
	 */
	public static RankerDataBuilder rankerData() {
		return new RankerDataBuilder();
	}

	/**
	 *  One entry in the list of units/unit groups
	 */
	public static RankerUnitBuilder rankerUnit() {
		return new RankerUnitBuilder();
	}

	/**
	 *  One table of alternatives (used by ConfigParts of type CEDropdown)
	 */
	public static AltTableBuilder altTable() {
		return new AltTableBuilder();
	}

	/**
	 *  Alternatives may be shown in groups
	 */
	public static AltSectionBuilder altSection() {
		return new AltSectionBuilder();
	}

	/**
	 *  A single alternative
	 */
	public static AltBuilder alt() {
		return new AltBuilder();
	}

	/**
	 *  An instance of a Widget -- the user may have three instances of the scoreboard, for, uhm, instance.
	 */
	public static WidgetInstanceBuilder widgetInstance() {
		return new WidgetInstanceBuilder();
	}

	/**
	 *  Each scoreboard widget has one page
	 */
	public static SWPageBuilder sWPage() {
		return new SWPageBuilder();
	}

	/**
	 *  Represents a single line of the Scoreboard Widget
	 */
	public static SWLineBuilder sWLine() {
		return new SWLineBuilder();
	}

	/**
	 *  Time period.
	 */
	public static TimePeriodBuilder timePeriod() {
		return new TimePeriodBuilder();
	}

	/**
	 *  Line style.
	 */
	public static LineStyleBuilder lineStyle() {
		return new LineStyleBuilder();
	}

	/**
	 *  Graph x label.
	 */
	public static GraphLabelBuilder graphLabel() {
		return new GraphLabelBuilder();
	}

	/**
	 *  A graph. Contains data for a list of lines.
	 */
	public static GraphBuilder graph() {
		return new GraphBuilder();
	}

	/**
	 *  Line data. Contains a list of data points for a single line.
	 */
	public static LineDataBuilder lineData() {
		return new LineDataBuilder();
	}

	/**
	 *  Scorecard. Contains a summary of scores for a set of surveys matching a given search string, e.g. the name of an employee.
	 */
	public static ScorecardBuilder scorecard() {
		return new ScorecardBuilder();
	}

	/**
	 *  An individual score shown on the scorecard.
	 */
	public static ScorecardScoreBuilder scorecardScore() {
		return new ScorecardScoreBuilder();
	}

	/**
	 *  A single survey. Contains HTML for rendering in WebKit, and information about actions like adding note, calling customer, forwarding email, etc.
	 */
	public static SingleResponseBuilder singleResponse() {
		return new SingleResponseBuilder();
	}

	/**
	 *  A survey log note
	 */
	public static LogNoteBuilder logNote() {
		return new LogNoteBuilder();
	}

	/**
	 *  An alert action (e.g. "close alert")
	 */
	public static AlertActionBuilder alertAction() {
		return new AlertActionBuilder();
	}

	/**
	 *  Information about an email template to be edited & sent by the user. Typically triggered by a button in the action menu
	 */
	public static EmailActionBuilder emailAction() {
		return new EmailActionBuilder();
	}

}
