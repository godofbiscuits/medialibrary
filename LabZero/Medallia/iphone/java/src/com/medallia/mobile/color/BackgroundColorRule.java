package com.medallia.mobile.color;


public interface BackgroundColorRule {
	public BackgroundColor getBackgroundColor(double score);
}
