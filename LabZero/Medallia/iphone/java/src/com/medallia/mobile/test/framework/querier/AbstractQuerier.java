package com.medallia.mobile.test.framework.querier;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.NotImplementedException;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.WebClient;
import com.medallia.api.datamodel.Timeperiod;
import com.medallia.mobile.test.framework.beans.Id;
import com.medallia.mobile.test.framework.beans.LogNote;
import com.medallia.mobile.test.framework.config.ExpressEnvironment;
import com.medallia.mobile.test.framework.querier.annotation.NotImplemented;
import common.web.HttpBasicAuthentication;

/**
 * Skeleton implementation with some handy methods used by all the implementations of
 * {@link Querier}.
 * 
 * @author fernando
 */
public abstract class AbstractQuerier implements Querier {

	private static String BASIC_AUTH_USER = "medallia"; 
	private static String BASIC_AUTH_PASSWORD = "Kb9mMKJ4u7"; 
	
	/**
	 * Used for querying the express server.
	 */
	protected final WebClient webClient = new WebClient();
	
	/**
	 * Contains the express environment used in the test.
	 */
	protected ExpressEnvironment express;
	
	/**
	 * Shoots a {@link NotImplementedException} in case that an unimplemented method
	 * is called.
	 */
	protected void notImplemented() {
		String method = Thread.currentThread().getStackTrace()[2].getMethodName();
		throw new NotImplementedException(method + " is not implemented in " + this.getClass().getSimpleName());
	}
	
	/**
	 * Sends a request with a header and a set of parameters.
	 * 
	 * <p> Parameters are in the form of "paramName=paramValue". paramValue should be url enconded.
	 * 
	 * @param header the header of the request.
	 * @param parameters a list of parameters.
	 * @return
	 */
	protected String getResponse(String header, String... parameters) {
		Page response = null;
		try {
			StringBuffer query = new StringBuffer(header);
			for(String parameter:parameters) {
				query.append("&" + parameter);
			}
			HttpBasicAuthentication auth = HttpBasicAuthentication.fromUserPass(BASIC_AUTH_USER, BASIC_AUTH_PASSWORD);
			webClient.addRequestHeader("Authorization", auth.getAuthString());
	
			response = webClient.getPage(query.toString());
		} catch (FailingHttpStatusCodeException e) {
			throw new RuntimeException(
					"The server responded wuth a Failing HTTP Status Code: ", e);
		} catch (MalformedURLException e) {
			throw new RuntimeException("The url is not vaild: ", e);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		return response.getWebResponse().getContentAsString();
	}
	
	/**
	 * Sleeps for a period of time in milliseconds.
	 * 
	 * @param timeInMillis sleep time.
	 */
	protected void sleep(long timeInMillis) {
		try {
			Thread.sleep(timeInMillis);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	@Override
	@NotImplemented
	public List<LogNote> getLogNotes(Id<?> surveyId) {
		notImplemented();
		return null;
	}

	@Override
	@NotImplemented
	public Id<?> createDummySurvey(String company, String unit) {
		notImplemented();
		return null;
	}
	
	@Override
	@NotImplemented
	public double getAverageFromSurvey(String field, String beginDate,
			String endDate) {
		notImplemented();
		return 0;
	}
	
	@Override
	@NotImplemented
	public void triggerAlert(String company, String alert, Id<?> survey) {
		notImplemented();
	}
	
	@Override
	@NotImplemented
	public double getAverageFromSurvey(String field, Id<?> timePeriod) {
		notImplemented();
		return 0;
	}
	
	@Override
	@NotImplemented
	public List<Timeperiod> getTimePeriods() {
		notImplemented();
		return null;
	}
	
	@Override
	@NotImplemented
	public int getNumberOfSurveys(Timeperiod timePeriod,
			Map<String, String> filters) {
		notImplemented();
		return 0;
	}
	
	@Override
	@NotImplemented
	public int getNumberOfSurveys(Timeperiod timePeriod) {
		notImplemented();
		return 0;
	}
}
