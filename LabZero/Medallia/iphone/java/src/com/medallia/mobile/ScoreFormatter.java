/**
 * 
 */
package com.medallia.mobile;

import java.text.DecimalFormat;

import com.medallia.mobile.color.UIColor;
import com.medallia.mobile.companysettings.CompanySettings;

public class ScoreFormatter {
	private CompanySettings config;
	private double score;
	private double diff;
	private boolean usesBenchmark;
	
	public ScoreFormatter(CompanySettings config, double score, double bench, boolean usesBenchmark) {
		this.config = config;
		this.score = score;
		this.diff = score - bench;
		this.usesBenchmark = usesBenchmark && !Double.isNaN(score);
	}
	
	public UIColor getDiffColor() {
		return diff < 0 && formatDiff(diff).startsWith("-") ? UIColor.RED : UIColor.BLACK;
	}
	
	public String getScoreText() {
		return formatValue(score);
	}
	
	public String getDiffText() {
		return usesBenchmark ? formatDiff(diff) : "";
	}

	public boolean usesBenchmark() {
		return usesBenchmark;
	}
	
	public double getScore() {
		return score;
	}
	
	public double getDiff() {
		return usesBenchmark ? diff : Double.NaN;
	}

    /** @return Floating point value formatted for iPhone. NaNs will be formatted properly. */
	private String formatValue(double value) {
		DecimalFormat format = new DecimalFormat();
		format.setMaximumFractionDigits(config.numberOfscoreDecimals());
		format.setMinimumFractionDigits(config.numberOfscoreDecimals());
		return Double.isNaN(value) ? "-" : format.format(value);
	}
	
    /** @return Floating point benchmark difference formatted for iPhone. NaNs will be formatted properly. */
	public String formatDiff(double diff) {
		if (Double.isNaN(diff))
			return "-";
		DecimalFormat format = new DecimalFormat();
		format.setMaximumFractionDigits(config.numberOfscoreDecimals());
		format.setMinimumFractionDigits(config.numberOfscoreDecimals());
		format.setPositivePrefix("+");
		String s = format.format(diff);
		// remove leading sign if the string is 0 (the below regex matches various forms of 0: "0", ".0", "0.", "0.00", etc)
		return s.length() > 1 && s.matches("[+-]0?\\.?0*") ? s.substring(1) : s;
	}
}