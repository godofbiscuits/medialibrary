package com.medallia.mobile.test.framework.util;

import java.io.Reader;

import com.medallia.mobile.dto.ConfigFile;
import com.medallia.mobile.dto.RankerData;
import com.medallia.mobile.dto.ResponsesLump;
import com.medallia.mobile.dto.SWPage;
import com.medallia.mobile.dto.Scorecard;
import com.medallia.mobile.dto.SingleResponse;

/**
 * Decodes the plist received from the Mobile Servlet into a 
 * meaningful object. 
 */
public interface PListDecoder {

	/**
	 * Parses the plist into a {@link SingleResponse} 
	 */
	public SingleResponse singleResponsePage(Reader plist);

	/**
	 * Parses the plist into a {@link SingleResponse} 
	 */
	public ResponsesLump responsesPage(Reader plist);

	/**
	 * Parses the plist into a {@link SWPage} 
	 */
	public SWPage scoresPage(Reader plist);

	/**
	 * Parses the plist into a {@link RankerData} 
	 */
	public RankerData rankerPage(Reader plist);

	/**
	 * Parses the plist into a {@link Scorecard} 
	 */
	public Scorecard scorecardPage(Reader plist);

	/**
	 * Parses the plist into a {@link ConfigFile} 
	 */
	public ConfigFile loginPage(Reader plist);
}
