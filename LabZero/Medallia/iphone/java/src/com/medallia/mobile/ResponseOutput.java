package com.medallia.mobile;

import org.antlr.stringtemplate.StringTemplate;
import org.antlr.stringtemplate.StringTemplateGroup;

import com.medallia.mobile.color.UIColor;

public class ResponseOutput {
    static StringTemplateGroup sg = new StringTemplateGroup("Responses");
    
    static String path = ResponseOutput.class.getPackage().getName().replace('.','/');
    static StringTemplate template(String s) {
        return sg.getInstanceOf(path+"/"+s);
    }
    
    StringTemplate wholeFile = template("Body");
    StringTemplate currentSection;
    public ResponseOutput newSection(String title) {
        currentSection = template("Section");
        wholeFile.setAttribute("sections", currentSection);
        currentSection.setAttribute("title", title);
        return this;
    }

    /**
     * Adds a row to the current section.
     * @param args List of ST argument names/values. Folds pairs where either name/value is null.
     */
    private ResponseOutput addRow(String name, String... args) {
        StringTemplate st = template(name);
        for (int i=0; i+1<args.length; i += 2)
        	if (args[i] != null && args[i+1] != null)
        		st.setAttribute(args[i], args[i+1]);
        currentSection.setAttribute("rows", st);
        return this;
    }
    
    public ResponseOutput addScoreRow(String text, String score, UIColor color) {
        return addRow("RowScore", 
                "text", text,
                "score", score,
                "color", String.format("%06x", color.getRGB()));
    }
    
    public ResponseOutput addFreeText(String questionName, String text) {
        return addRow("RowText",
                "questionName", questionName,
                "text", text);
    }    

    public ResponseOutput addValueRow(String caption, String value) {
        return addRow("RowValue", "caption", caption, "value", value);
    }
    
    public ResponseOutput addAlertRow(String caption, String value) {
        return addRow("RowAlert", "caption", caption, "value", value);
    }
    
    public ResponseOutput addValueRowRightAligned(String caption, String value) {
        return addRow("RowValueRA", "caption", caption, "value", value);
    }
    
    public ResponseOutput addActionRow(String url, String text) {
        return addActionRow(url, text, null);
    }
    
	public ResponseOutput addActionRow(String url, String text, String detail) {
		return addActionRowAlreadyEncoded(Common.urlencode(url), text, detail);
	}
	
	public ResponseOutput addActionRowAlreadyEncoded(String url, String text, String detail) {
		return addRow("RowAction", "url", "open://x/?"+url, "text", text, "detail", detail);
	}
	
    public Object getRoot() {
        return wholeFile;
    }

}
