package com.medallia.mobile.dto;
import java.util.*;
import com.medallia.mobile.*;
import com.medallia.mobile.dto.*;
import com.medallia.mobile.dto.builders.*;
import com.medallia.mobile.color.UIColor;

// These files are generated automatically from declarations in DataFormat.h; see convert.pl for details.
//
// Do *not* modify this file directly!


/**
 *  A survey log note
 */
@SuppressWarnings("unused")
public interface LogNote extends Plist.Plistable {

	/**
	 *  Date note was added, e.g. "January 31, 2010"
	 */
	@Required String date();

	/**
	 *  User that added the note, e.g. "Ryan Smith" or "System"
	 */
	@Required String user();

	/**
	 *  Note title, e.g. "Alert Closed"
	 */
	@Required String title();

	/**
	 *  Note content, e.g. "I spoke to the guest and resolved the issue."
	 */
	@Required String content();


}
