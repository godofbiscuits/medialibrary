package com.medallia.mobile.dto.builders;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/** Marker interface for required fields */
@Retention(RetentionPolicy.RUNTIME)
public @interface Required { 
    
}
