package com.medallia.mobile;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.medallia.api.datamodel.Timeperiod;
import com.medallia.mobile.companysettings.CompanySettings;

/**
 * Temporary hack to augment the timeperiod class with shortname and benchmark timeperiod,
 * until we can add this information to the Express setup system and the API.
 */
public class TemporaryTimeperiod extends Timeperiod {
	
	private final TPEnum tp;
	private TemporaryTimeperiod benchmark;

	public TemporaryTimeperiod(Timeperiod t, TPEnum tp) {
		super(t.id, t.name, t.startDate, t.endDate, t.priority);
		this.tp = tp;
	}
	
	/** @return the shortname to use for this timeperiod (defaults to normal name if no shortname has been specified) */
	public String getShortName() {
		return tp.shortName == null ? tp.name : tp.shortName;
	}
	
	/** @return The {@link BenchmarkTimeperiod}. */
	public BenchmarkTimeperiod getBenchmark() {
		return new BenchmarkTimeperiod(tp.usesBenchmark, benchmark);
	}
	
	/**
	 * List of all recognized time periods. For a time period to appear in the iPhone app, it must be an active
	 * time period (see constructors of the enum) and must also appear in the Express database by the same name.
	 */
	private enum TPEnum {
		// inactive pure benchmark time periods
		P30D("Change vs. Prior 30 Days", "p30d"),
		P60D("Change vs. Prior 60 Days", "p60d"),
		P90D("Change vs. Prior 90 Days", "p90d"),
		PWEEK("Prior Week", "pWeek"),
		PMONTH("Prior Month", "pMonth"),
		PQUARTER("Prior Quarter", "pQuarter"),

		// active time periods
		L30D("Last 30 Days", "30d", P30D),
		L60D("Last 60 Days", "60d", P60D),
		L90D("Last 90 Days", "90d", P90D),
		L12M("Last 12 Months", "12m", true),
		WEEK("Current Week to Date", "WTD", PWEEK),
		MONTH("Current Month to Date", "MTD", PMONTH),
		QUARTER("Current Quarter to Date", "QTD", PQUARTER),
		L8Q("Last 8 Quarters", "8Q", true),
		L4Q("Last 4 Quarters", "4Q", true),
		F2008("Fiscal Year 2008", "2008", false),
		F2009("Fiscal Year 2009", "2009", F2008),
		F2010("Fiscal Year 2010 to Date", "2010", F2009),
		
		// rezidor
		L12MTD("Past 12 Months to Date", "12m", true),
		;
		
		private final String name;
		private final String shortName;
		private final TPEnum benchmarkTP;
		private final boolean usesBenchmark;
		
		private static class Content {
			private static Map<String, TPEnum> allTPs = new HashMap<String, TPEnum>();
			private static Set<String> activeTPs = new HashSet<String>();
		}
		
		private static Map<String, TPEnum> getAllTPs() { return Content.allTPs; }
		private static Set<String> getActiveTPs() { return Content.activeTPs; }

		private TPEnum(String name, String shortName, TPEnum benchTP, boolean hasBench) {
			this.name = name;
			this.shortName = shortName;
			this.benchmarkTP = benchTP;
			this.usesBenchmark = hasBench;
		}
		
		/** constructor used for pure benchmark time periods (not active in the app) */
		private TPEnum(String name, String shortName) {
			this(name, shortName, null, false);
			Content.allTPs.put(name, this);
		}
		
		/** constructor used for active time periods that may or may not use a heuristic benchmark */
		private TPEnum(String name, String shortName, boolean hasBenchmark) {
			this(name, shortName, null, hasBenchmark);
			Content.allTPs.put(name, this);
			Content.activeTPs.add(name);
		}
		
		/** constructor used for active time periods that use another time period as a benchmark */
		private TPEnum(String name, String shortName, TPEnum benchmarkTP) {
			this(name, shortName, benchmarkTP, true);
			Content.allTPs.put(name, this);
			Content.activeTPs.add(name);
		}
	}
	
	private static List<TemporaryTimeperiod> convert(Collection<Timeperiod> timeperiods) {
		Map<String, TemporaryTimeperiod> ttByName = new HashMap<String, TemporaryTimeperiod>();
		for (Timeperiod t : timeperiods)
			if (TPEnum.getAllTPs().containsKey(t.name))
				ttByName.put(t.name, new TemporaryTimeperiod(t, TPEnum.getAllTPs().get(t.name)));
		
		List<TemporaryTimeperiod> tts = new ArrayList<TemporaryTimeperiod>();
		Set<String> seen = new HashSet<String>();
		for (Timeperiod t : timeperiods) {
			if (!seen.contains(t.name) && TPEnum.getActiveTPs().contains(t.name)) {
				seen.add(t.name);
				TemporaryTimeperiod tt = ttByName.get(t.name);
				TPEnum benchTP = TPEnum.getAllTPs().get(t.name).benchmarkTP;
				tt.benchmark = benchTP == null ? null : ttByName.get(benchTP.name);
				tts.add(tt);
			}
		}
		return tts;
	}

	/** @return List of time periods with augmented short name and benchmark. See {@link TemporaryTimeperiod}. */
	public static List<TemporaryTimeperiod> getTemporaryTimeperiods(CompanySettings config) {
		return convert(Timeperiod.getTimeperiodQuery().run(config.getConnection()));
	}
	
	/**
	 * @return The TemporaryTimeperiod for the given timePeriodId.
	 * @throws AssertionError If the timePeriodId doesn't exist.
	 */
	public static TemporaryTimeperiod getTemporaryTimeperiod(CompanySettings config, String timePeriodId) {
	   	for (TemporaryTimeperiod timeperiod : TemporaryTimeperiod.getTemporaryTimeperiods(config))
			if (timeperiod.id.equals(timePeriodId))
				return timeperiod;
	   	throw new AssertionError("No time period found for " + timePeriodId);
	}
	
	@Override
	public String toString() {
		return name+" "+startDate+","+endDate+", tp="+tp+", bench=["+benchmark+"]";
	}
}
