package com.medallia.mobile.dto;
import java.util.*;
import com.medallia.mobile.*;
import com.medallia.mobile.dto.*;
import com.medallia.mobile.dto.builders.*;
import com.medallia.mobile.color.UIColor;

// These files are generated automatically from declarations in DataFormat.h; see convert.pl for details.
//
// Do *not* modify this file directly!


/**
 *  Time period.
 */
@SuppressWarnings("unused")
public interface TimePeriod extends Plist.Plistable {

	/**
	 *  Short time period title ("1w", "1m", "3m", etc).
	 */
	@Required String title();

	/**
	 *  List of graph x labels for the small graph. (These will be identical for all small graphs and are thus stored outside the graph object.)
	 */
	List<GraphLabel> labelsSmallGraph();

	/**
	 *  List of graph x labels for the large graph. (These will be identical for all large graphs and are thus stored outside the graph object.)
	 */
	List<GraphLabel> labelsLargeGraph();


}
