package com.medallia.mobile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.mortbay.util.ajax.JSON;

import com.medallia.api.orm.Statement;
import common.web.HttpBasicAuthentication;

public class DirectApiConnection extends PasswordApiConnection {

	private static String BASIC_AUTH_USER = "medallia"; 
	private static String BASIC_AUTH_PASSWORD = "Kb9mMKJ4u7"; 
	
	protected final String expressURI;
	
	public DirectApiConnection(String company, String user, String password, String expressURI) {
		super(company, user, password);
		this.expressURI = expressURI;
	}

    private static void addHeaderForBasicAuth(HttpMethod method) {
		HttpBasicAuthentication auth = HttpBasicAuthentication.fromUserPass(BASIC_AUTH_USER, BASIC_AUTH_PASSWORD);
		method.addRequestHeader(auth.getHeader());
	}
	
	@Override
	public List<List<String>> runQuery(Statement statement) {
		System.err.println("received query = " + statement.getQueryString());
		HttpClient hc = new HttpClient();
		
		String queryURI = getQueryURI(statement);
		System.err.println("Query URI = " + queryURI);
		HttpMethod method = new GetMethod(queryURI);
		
		//method.addRequestHeader("Authorization", "Basic bWVkYWxsaWE6S2I5bU1LSjR1Nw==");
		addHeaderForBasicAuth(method);
		
		List<List<String>> table = new ArrayList<List<String>>();
		try {
			hc.executeMethod(method);
			String result = method.getResponseBodyAsString();
			Map map = (Map)(new JSON().fromJSON(result));
			if (map.keySet().equals(Collections.singleton("query")))
				map = (Map) map.get("query");
			
			Number resCode = (Number)map.get("resultCode");
			if (resCode == null || resCode.intValue() != 0)
				throw new QueryException(resCode, ""+map.get("error"));
			
			for (Object o2 : (Object[]) map.get("table"))
				table.add(Arrays.asList(Arrays.asList((Object[])o2).toArray(new String[0])));
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		return table;
	}

	@Override
	protected String getBaseURI() {
		return expressURI + "/api?company=" + Common.urlencode(company);
	}

}
