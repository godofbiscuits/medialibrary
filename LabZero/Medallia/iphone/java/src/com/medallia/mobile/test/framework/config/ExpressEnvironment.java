package com.medallia.mobile.test.framework.config;

/**
 * Defines the different express environments that can be used.
 * 
 * @author fernando
 */
public enum ExpressEnvironment {
	EARTH("kristian", "corvette", "earth", "68", 131, "http://localhost:8080", "earth_ov_experience"),
	TEST("testUser", "testPassword", "testCompany", "1", 0, "", "ov_experience"),
	MOCK("user", "password", "mock", "1", 0, "http://localhost:53394", "ov_experience"),
	MEDALLIA("a", "humbug", "medallia", "443", 123, "http://localhost:8080", "ov_experience"),
	;
	
	
	private final String user;
	
	private final String password;
	
	private final String company;
	
	private final String validSurveyId;
	
	private final String url;
	
	private final int timeperiod;
	
	private final String mainScore;
	
	private ExpressEnvironment(String user, String password, String company, String validSurveyId, int timeperiod, String url,
			String mainScore) {
		this.user = user;
		this.password = password;
		this.company = company;
		this.url = url;
		this.validSurveyId = validSurveyId;
		this.timeperiod = timeperiod;
		this.mainScore = mainScore;
	}
	
	public String getMainScore() {
		return mainScore;
	}
	
	public String getUser() {
		return user;
	}

	public String getPassword() {
		return password;
	}

	public String getCompany() {
		return company;
	}
	
	public String getValidSurveyId() {
		return validSurveyId;
	}

	public int getTimeperiod() {
		return timeperiod;
	}

	public String getUrl() {
		return url;
	}
}
