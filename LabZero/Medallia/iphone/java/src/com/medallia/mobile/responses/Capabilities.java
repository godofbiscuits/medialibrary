package com.medallia.mobile.responses;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import com.medallia.api.datamodel.Capability;
import com.medallia.mobile.companysettings.CompanySettings;

class Capabilities {
	private Set<Cap> capabilities = new HashSet<Cap>();

	Capabilities(CompanySettings config) {
		Collection<Capability> capabilities = Capability.getCapabilityQuery().run(config.getConnection());
        for (Capability capability : capabilities) {
        	if (Cap.getCapMap().contains(capability.id))
        		this.capabilities.add(Cap.valueOf(capability.id));
        }
	}
	
	boolean hasCap(Cap cap) {
		return capabilities.contains(cap);
	}
}