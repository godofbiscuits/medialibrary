package com.medallia.mobile;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.medallia.api.datamodel.SelectQueryWithBooleanFilter;
import com.medallia.api.datamodel.Unit;
import com.medallia.mobile.RequestParameterMap.Param;
import com.medallia.mobile.companysettings.CompanySettings;

public class ApiUtil {

	public static String getTimePeriodId(RequestParameterMap req) {
		String timePeriodId = req.get(Param.tp);
		if (timePeriodId == null && "appleretail".equals(req.get(Param.c)))
			return "1409"; // TODO for testing with old objc code // 1306
		return timePeriodId;
	}
	
	/** add condition "timeperiod(XX, responsedate)" to WHERE clause */
	public static void whereTimePeriod(SelectQueryWithBooleanFilter query, RequestParameterMap req) {
		String timePeriodId = getTimePeriodId(req);
		if (timePeriodId == null) return;
		query.whereTimePeriod(timePeriodId);
	}

	/**
	 * add WHERE clause for responsedate being in the benchmark period for the requested time period
	 * @return benchmark time period
	 */
	public static BenchmarkTimeperiod whereBenchTimePeriod(SelectQueryWithBooleanFilter query, CompanySettings config, RequestParameterMap req) {
		String timePeriodId = getTimePeriodId(req);
		if (timePeriodId == null) return new BenchmarkTimeperiod(false, null); // no time filter
		
		TemporaryTimeperiod timeperiod = TemporaryTimeperiod.getTemporaryTimeperiod(config, timePeriodId);
		BenchmarkTimeperiod benchPeriod = timeperiod.getBenchmark();
		if (benchPeriod.hasBenchPeriod()) {
			query.whereTimePeriod(benchPeriod.getBenchPeriod().id);
		} else if (benchPeriod.hasHeuristicBenchmark()){
			// use heuristic benchmark period -- prior period of equal length
			Date startDate = timeperiod.startDate;
			Date endDate = timeperiod.endDate;
			long diff = endDate.getTime() - startDate.getTime();
			endDate.setTime(startDate.getTime() - Common.MS_PER_DAY);
			startDate.setTime(endDate.getTime() - diff);
			query.whereTimePeriod(startDate, endDate);
		}
		return benchPeriod;
	}

    private static Unit merge(Unit a, Unit b) {
    	return new Unit(a.getId() + "|" + b.getId(), a.getName());
    }
    
    /**
     * Apple specific - merging all units with the same name, because they
     * represent the same store.
     * @return the merged list of units
     */
    private static List<Unit> mergeEqualName(List<Unit> units) {
    	List<Unit> mergedUnits = new ArrayList<Unit>();
    	Unit current = null;
    	for (Unit unit : units) {
    		if (current == null || current.compareTo(unit) != 0) {
    			if (current != null) mergedUnits.add(current);
    			current = unit;
    		} else {
    			current = merge(current, unit);
    		}
    	}
    	if (current != null) mergedUnits.add(current);
    	return mergedUnits;
    }
    
	public static List<Unit> getUnitsMerged(CompanySettings config, boolean extendedScope) {
	    List<Unit> units = Unit.getUnits(config.getConnection(), extendedScope);
	    if (config.mergeEqualUnits()) {
	    	units = mergeEqualName(units);
	    }
		return units;
	}
	
}
