package com.medallia.mobile;

import java.util.Collections;
import java.util.Map;

import com.medallia.mobile.companysettings.CompanySettings;
import com.medallia.mobile.companysettings.CompanySettingsAppleRetail;
import com.medallia.mobile.companysettings.CompanySettingsDefaultBase;
import com.medallia.mobile.companysettings.CompanySettingsGlobal;
import com.medallia.mobile.companysettings.CompanySettingsHHCQA;
import com.medallia.mobile.companysettings.CompanySettingsIntuit;
import com.medallia.mobile.companysettings.CompanySettingsMedallia;
import com.medallia.mobile.companysettings.CompanySettingsMilkyWayEarth;
import com.medallia.mobile.companysettings.CompanySettingsMock;
import com.medallia.mobile.companysettings.CompanySettingsProductFeedback;
import com.medallia.mobile.companysettings.CompanySettingsRezidor;
import com.medallia.mobile.dto.builders.Build.Constants;

public class RequestParameterMap {
	
	public enum Param {
		// GENERAL
		c,                  // company
		p,                  // password
		req,                // what is requested
		u,                  // username
		version,            // version of iPhone code
		unitid,             // filter on a particular unit
		tp,                 // timeperiod

		// SCOREBOARD-ONLY
		// (none)
		
		// RESPONSES LIST VIEW
		alerts_only,        // only show surveys with alerts
		comments_only,      // only show surveys with comments
		offset,             // offset on Responses list view
		satrange,           // only show surveys with "overall satisfaction" within this range
		response_text,      // full-text search
		resp_filter,        // if set, key of filter to apply (for the search button filters, may differ per client, used by apple as a program (unit group) selector)
		employee,           // if set, this is the employee view
		
		// RESPONSE DETAIL VIEW
		id,                 // the survey id
		note_content,       // content of log note added
		alert_action,       // "closed", "resolved"
		call_initiated,     // Time at which customer call was initiated
		
		// SCORECARD VIEW
		scorecard_name,     // the name of the scorecard

		// RANKER
		rankerq,            // question used to rank by, e.g. "ov_experience"
		scope,              // ranker scope (region, market, store, etc)
		
		// COMPANY-SPECIFIC
		param01,            // company custom parameter #1
		param02,            // company custom parameter #2
		param03,            // company custom parameter #3
		param04,            // company custom parameter #4
		param05             // company custom parameter #5
	}
	
	static {
		assert Param.response_text.name().equals(Constants.RESPONSES_TEXT_SEARCH_PARAMETER);
	}
	
	public class LogonInfo {
		private final String company;
		private final String user;
		private final String password;
		private final String version;
		public String getCompany() {
            return company;
        }
        public String getUser() {
            return user;
        }
        public String getPassword() {
            return password;
        }
        public LogonInfo() {
			company = get(Param.c);
			user = get(Param.u);
			password = get(Param.p);
			version = get(Param.version);
		}
		public String getBase() {
			return base;
		}
		public String createURL(String req) {
			return String.format("%s?req=%s&c=%s&u=%s&p=%s&version=%s", base, Common.urlencode(req), Common.urlencode(company), Common.urlencode(user), Common.urlencode(password), version);
		}
	}

	private final Map<String, String> parameters;
	private final String base;
	private final LogonInfo logonInfo;
	
	public RequestParameterMap(Map<String, String> parameters, String base) {
		this.parameters = parameters;
		this.base = base;
		logonInfo = new LogonInfo();
	}

	public String get(Param p) {
		return parameters.get(p.name());
	}
	
	public Map<String, String> getParameters() {
		return Collections.unmodifiableMap(parameters);
	}
	
	/** Use this method only for company-specific keys that are not part of Param */
	public String getRaw(String key) {
		for (Param p : Param.values()) {
			if (p.name().equals(key)) {
				throw new RuntimeException("Trying to access the common parameter " + p + " through get(String) method, which is not allowed");
			}
		}
		return parameters.get(key);
	}
	
	public LogonInfo getLogonInfo() {
		return logonInfo;
	}
	
	private boolean matches(String company, String param) {
		return company.equalsIgnoreCase(param);
	}
	
	public CompanySettings getConfig() {
		String c = logonInfo.company;
		if (matches("global", c)) return new CompanySettingsGlobal(logonInfo);
		if (matches("appleretail", c)) return new CompanySettingsAppleRetail(logonInfo);
		if (matches("productfeedback", c)) return new CompanySettingsProductFeedback(logonInfo);
		if (matches("medallia", c)) return new CompanySettingsMedallia(logonInfo);
		if (matches("intuit", c)) return new CompanySettingsIntuit(logonInfo);
		if (matches("hhcqa", c)) return new CompanySettingsHHCQA(logonInfo);
		if (matches("rezidor", c)) return new CompanySettingsRezidor(logonInfo);
		if (matches("rezidorqa", c)) {
			CompanySettingsDefaultBase settings =  new CompanySettingsRezidor(logonInfo);
			settings.setOverridingBaseURI("https://expressqa16.medallia.com");
			return settings;
		}
		if (matches("earth", c)) return new CompanySettingsMilkyWayEarth(logonInfo);
		if (matches("mock", c)) return new CompanySettingsMock(logonInfo);
		return null;
	}

	public enum Action {
		login,
		scores,
		ranker,
		responses,
		details,
		scorecard,
		test,
	}
	
	public Action getAction() {
		String what = getActionRaw();
		for (Action a : Action.values()) {
			if (a.name().equals(what)) {
				return a;
			}
		}
		// TODO For now, assume that the request is a scoreboard widget key.  However, this is 
		// dangerous, we should be a bit smarter about handling bogus requests.
		return Action.scores;
	}

	public String getActionRaw() {
		return get(Param.req);
	}
}
