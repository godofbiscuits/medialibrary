package com.medallia.mobile.test.framework.querier.impl;

import com.medallia.mobile.Common;
import com.medallia.mobile.test.framework.beans.Id;
import com.medallia.mobile.test.framework.config.ExpressEnvironment;
import com.medallia.mobile.test.framework.querier.AbstractQuerier;
import com.medallia.mobile.test.framework.querier.Querier;

/**
 * Implementation of a {@link Querier} that calls the Test Page of Express.
 * 
 * <p>
 * 	Shouldn't be used directly, {@link ExpressQuerier} should be used instead.
 * 
 * @author fernando
 */
public class TestPageQuerier extends AbstractQuerier {
	
	@Override
	public void triggerAlert(String company, String alert, Id<?> survey) {
		String response = getResponse(getTestHeader(), "cmd=trigger_alert", "company=" + Common.urlencode(company),
				"alert=" + Common.urlencode(alert), "survey=" + Common.urlencode(survey.toString()));
		if (response.contains("Failed")) {
			throw new IllegalArgumentException(response);
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Id<?> createDummySurvey(String company, String unit) {
		String response = getResponse(getTestHeader(), "cmd=create_survey", "company=" + Common.urlencode(company),
				"unit=" + Common.urlencode(unit));
		/**
		 * Some times the Survey does not show up instantly.
		 */
		sleep(100);
		return new Id(response.substring(response.indexOf("=") + 1, response.indexOf("]")));
	}
	
	/**
	 * Returns a header for querying the Test Page. 
	 */
	private String getTestHeader() {
		return String.format("%s/.test?", express.getUrl());		
	}
	
	
	private TestPageQuerier(ExpressEnvironment express) {
		this.express = express;
	}
	
	static Querier createQuerier(ExpressEnvironment express) {
		return new TestPageQuerier(express);
	}
}
