package com.medallia.mobile.dto;
import java.util.*;
import com.medallia.mobile.*;
import com.medallia.mobile.dto.*;
import com.medallia.mobile.dto.builders.*;
import com.medallia.mobile.color.UIColor;

// These files are generated automatically from declarations in DataFormat.h; see convert.pl for details.
//
// Do *not* modify this file directly!


/**
 *  Graph x label.
 */
@SuppressWarnings("unused")
public interface GraphLabel extends Plist.Plistable {

	/**
	 *  Graph x label.
	 */
	@Required String label();

	/**
	 *  Label x position, ordered and ranged between 0 and 1 (inclusive).
	 */
	@Required double position();


}
