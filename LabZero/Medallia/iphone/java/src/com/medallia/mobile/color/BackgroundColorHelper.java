package com.medallia.mobile.color;

import com.medallia.mobile.FieldWithColor;
import com.medallia.mobile.companysettings.CompanySettings;
import com.medallia.mobile.dashboard.ScoreDisplay;
import com.medallia.mobile.dto.builders.Build.SWLineBuilder;

public class BackgroundColorHelper {

	public final CompanySettings config;
	private final FieldWithColor field;

	private BackgroundColorHelper(CompanySettings config, FieldWithColor field) {
		this.config = config;
		this.field = field;
	}
	
	public static BackgroundColorHelper getInstance(CompanySettings config, FieldWithColor field) {
		return new BackgroundColorHelper(config, field);
	}

	/**
	 * This method helps {@link ScoreDisplay#get()} to skip the scoreBackgroundColor if the field 
     * should not have a background color.  If the field should have a background color, then
     * the field's background color rule is applied based on the score.
     *   
     * TODO Parts of this method should possibly be refactored, perhaps placed in the {@link FieldWithColor}
     * class.
     *  
	 * @param lineBuilder
	 * @param score
	 * @return
	 */
	public SWLineBuilder scoreBackgroundColor(SWLineBuilder lineBuilder, double score) {
		BackgroundColor color = field.chooseBackgroundColor(config, score);
		if (color != null) {
			lineBuilder.scoreBackgroundColor(color.getColorName());
		}
		return lineBuilder;
	}

	/**
	 * If {@link #field} should not have a background color, then this method returns the defaultColor.
	 * If {@link #field} should have a background color, then the field's background color rule is 
	 * applied based on the score.
     * 
     * TODO Parts of this method should possibly be refactored, perhaps placed in the {@link FieldWithColor}
     * class.
     * 
	 * @param score
	 * @param defaultColor
	 * @return
	 */
	public UIColor uiColorForBackground(double score, UIColor defaultColor) {
		BackgroundColor color = field.chooseBackgroundColor(config, score);
    	if (color == null) {
    		return defaultColor;
    	} else {
    		return color.getUIColor();	
    	}        			
	}

	
}
