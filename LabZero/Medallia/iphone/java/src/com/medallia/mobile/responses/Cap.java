package com.medallia.mobile.responses;

import java.util.HashSet;
import java.util.Set;

/** Note that the names must match the names in the Express "Cap" enum. */
enum Cap {
	ALERT_RESOLVE,
	ALERT_CLOSE,
	ALERT_ADD_NOTE,
	;
	
	private static class CapMap {
		private static Set<String> caps = new HashSet<String>();
	}
	
	Cap() {
		CapMap.caps.add(name());
	}
	
	static Set<String> getCapMap() {
		return CapMap.caps;
	}
}