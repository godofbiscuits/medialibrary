package com.medallia.mobile.companysettings;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.medallia.api.datamodel.Survey;
import com.medallia.api.orm.Field;
import com.medallia.api.orm.Field.StringField;
import com.medallia.mobile.DetailFields;
import com.medallia.mobile.DetailFieldsDefaultBase;
import com.medallia.mobile.FieldWithColor;
import com.medallia.mobile.RequestParameterMap.LogonInfo;
import com.medallia.mobile.color.BackgroundColor;
import com.medallia.mobile.color.BackgroundColorRule;
import com.medallia.mobile.color.SimpleBackgroundColorRule;

public class CompanySettingsIntuit extends CompanySettingsDefaultBase {

    public CompanySettingsIntuit(LogonInfo req) {
        super(req);
    }

    @Override
    protected String getCompany() {
        return "intuit";
    }

    @Override
    protected String getBaseURI0() {
        return "https://login3.medallia.com";
    }

    @Override
    public String getUnitTranslation() {
        return "Line Manager";
    }

    @Override
    public Date getStartTime() {
        return getDate(2009, 1, 1);
    }

    @Override
    public FieldWithColor getMainScoreField() {
        return new FieldWithColor("intuit_recommendation", "Likely to Recommend (0-10)");
    }

    @Override
    public List<FieldWithColor> getScoreFields(String widgetRequestAction) {
        List<FieldWithColor> list = new ArrayList<FieldWithColor>();
        list.add(new FieldWithColor("intuit_cp2_survey_nps_new", "CP2 NPS", false));
        list.add(new FieldWithColor("intuit_cp2_survey_top2_new", "CP2 Overall Satisfaction (Top 2)", false));
        list.add(new FieldWithColor("intuit_css_survey_nps", "CSS NPS", false));
        list.add(new FieldWithColor("intuit_css_survey_top2_new", "CSS Overall Satisfaction (Top 2)", false));
        list.add(new FieldWithColor("intuit_online_survey_nps_new", "Online NPS", false));
        list.add(new FieldWithColor("intuit_online_survey_top2_new", "Online Overall Satisfaction (Top 2)", false));        
        return list;
    }

	@Override public List<FieldWithColor> getScorecardFields() {
		return getScoreFields("");
	}
	
    @Override
    public DetailFields getDetailFields() {
        return new DetailFieldsDefaultBase() {
            @Override
            public StringField getPhoneField() {
                return Survey.phone;
            }

            @Override
            public List<StringField> getNameFields() {
                List<StringField> list = new ArrayList<StringField>();
                list.add(Survey.firstname);
                list.add(Survey.lastname);
                return list;
            }

            @Override
            public List<Field> getParticipantFields() {
                List<Field> list = new ArrayList<Field>();
                list.add(new StringField("b2b_intuit_cp3_agent", "Intuit CP3 Agent"));
                list.add(new StringField("b2b_intuit_cp2_order_rep_name", "Intuit CP2 Order Rep Name"));
                list.add(new StringField("b2b_intuit_product_group", "Product Group"));
                list.add(new StringField("intuit_product_combined", "Product"));               
                return list;
            }

            @Override
            public List<StringField> getCommentFields() {
                List<StringField> list = new ArrayList<StringField>();
                list.add(new StringField("b_int_share_comments", "CSS - Intuit Share Comments"));
                list.add(new StringField("fs_intuit_css_comment_like_and_improve", "CSS - Comment Like and Improve"));
                list.add(new StringField("b_int_share_comments_cp2", "CP2 - Intuit Share Comments"));
                list.add(new StringField("fs_intuit_ob1_likes_comment", "OB1 - Comment Likes"));
                list.add(new StringField("fs_intuit_cp2_comment_like_and_improve", "CP2 - Comment Like and Improve"));
                list.add(new StringField("fs_intuit_lt1_thoughts_comment", "LT1 - Comment Thoughts"));
                list.add(new StringField("fs_intuit_ob1_improve_comment", "OB1 - Comment Improve"));
                list.add(new StringField("fs_intuit_ob2_renew_comment", "OB2 - Comment Renew"));
                list.add(new StringField("fs_intuit_ob2_likes_comment", "OB2 - Comment Likes"));
                list.add(new StringField("fs_intuit_ob2_suggestions_comment", "OB2 - Comment Suggestions"));
                list.add(new StringField("fs_intuit_ob2_confidence_comment", "OB2 - Comment Confidence"));
                list.add(new StringField("fs_intuit_feedless_comment", "Feedless - Comment"));

                return list;
            }

            @Override
            public List<FieldWithColor> getScoreFields() {
                List<FieldWithColor> list = new ArrayList<FieldWithColor>();
                list.add(new FieldWithColor("intuit_recommendation", " Likely to Recommend (0-10)", true, COLOR_RULE_0_10));
                list.add(new FieldWithColor("intuit_satisfaction", "Overall Satisfaction (0-10)", true, COLOR_RULE_0_10));
                list.add(new FieldWithColor("intuit_confidence", "Confidence (0-10)", true, COLOR_RULE_0_10));
                list.add(new FieldWithColor("intuit_time_spent", "Time Spent (0-10)", true, COLOR_RULE_0_10));
                list.add(new FieldWithColor("intuit_effort", "Effort (0-10)", true, COLOR_RULE_0_10));
                list.add(new FieldWithColor("intuit_knowledge", "Knowledge (0-10)", true, COLOR_RULE_0_10));
                list.add(new FieldWithColor("intuit_caring", "Caring (0-10)", true, COLOR_RULE_0_10));
                list.add(new FieldWithColor("intuit_comprehensiveness", "Comprehensive Info (0-10)", true, COLOR_RULE_0_10));
                list.add(new FieldWithColor("intuit_contact", "Number of Contacts (1-4)", true, COLOR_RULE_1_4));
                return list;
            }
        };
    }

	private static final BackgroundColorRule COLOR_RULE_0_10 = new SimpleBackgroundColorRule(4, 7);
//	private static final BackgroundColorRule COLOR_RULE_1_5 = new SimpleBackgroundColorRule(2, 4);
	
	// 1 green, 4 red and 2 and 3 yellow:
	private static final BackgroundColorRule COLOR_RULE_1_4 = new BackgroundColorRule() {
		@Override
		public BackgroundColor getBackgroundColor(double score) {
			if ( score <= 1 ) return BackgroundColor.GREEN;
			if ( score >= 4 ) return BackgroundColor.RED;
			return BackgroundColor.YELLOW;
		}
	};
	
	@Override public boolean showAlertTypeInResponsesList() { return false; }
	@Override public int getMinIntRange() { return 0; }
	@Override public int getMaxIntRange() { return 10; }

}
