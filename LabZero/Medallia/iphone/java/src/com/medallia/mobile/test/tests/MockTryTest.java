package com.medallia.mobile.test.tests;

import java.io.File;

import com.medallia.mobile.Common;
import com.medallia.mobile.test.framework.MobileTestCase;
import com.medallia.mobile.test.framework.config.Environment;
import com.medallia.mobile.test.framework.config.ExpressEnvironment;
import com.medallia.mobile.test.framework.config.MobileEnvironment;
import com.medallia.mobile.test.framework.requests.MobileRequest;

@Environment(express=ExpressEnvironment.MOCK, mobile=MobileEnvironment.LOCALHOST)
public class MockTryTest extends MobileTestCase {

	MobileRequest request;
	
	public void testRows() throws Exception {
		addMockResponse("SELECT *", "unitid,name,active,propertysegment,roomcount,amenity",
				"Addison,Addison,yes,,0,", "Albuquerque Airport,Albuquerque Airport,yes,,0,",
				"Anatole,Anatole,yes,,0,", "Andover,Andover,yes,,0,");
		String encoded = Common.urlencode("SELECT *");
		String response = getResponse("http://localhost:53394/api?company=mock&user=user&pass=password&charSet=utf8&query=" + encoded + "&output=json&version=1");
	}
	
	public void testFile() throws Exception {
		addMockResponse("SELECT2", new File("example.txt"));
		String encoded = Common.urlencode("SELECT2");
		String response = getResponse("http://localhost:53394/api?company=mock&user=user&pass=password&charSet=utf8&query=" + encoded + "&output=json&version=1");
		
	}
}
