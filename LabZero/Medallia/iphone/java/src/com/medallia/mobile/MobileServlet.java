package com.medallia.mobile;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.zip.GZIPOutputStream;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.medallia.api.datamodel.Unit;
import com.medallia.mobile.Plist.Plistable;
import com.medallia.mobile.RequestParameterMap.Action;
import com.medallia.mobile.companysettings.CompanySettings;

/** Simple example servlet for the mobile client */

public class MobileServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    public void init(ServletConfig config) throws ServletException {
    	super.init(config);
    }
    @SuppressWarnings("unchecked")
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
    	Map<String,String> m = new HashMap<String, String>();
        for (String k : (Set<String>) req.getParameterMap().keySet()) 
            m.put(k, req.getParameter(k));
        
        System.err.println(req.getRequestURL()+" "+m);
        
//        if (req.getRequestURL().toString().matches(".*slo+w.*") || System.getProperty("slow") != null) {
//            try {
//                Thread.sleep(3000);
//            } catch (InterruptedException e) {
//                throw null;
//            }
//        }

        // handle the test page outside MobileHandler since it's not intended to be used by the mobile
        RequestParameterMap reqMap = new RequestParameterMap(m, req.getRequestURL().toString()); 
        if (Action.test == reqMap.getAction())
        	handleTestAction(resp, reqMap);
        else
        	handleMobileAction(req, resp, reqMap);
        
        System.err.println("Finished request");
    }
    
    /**
     * Run a query to retrieve all properties and write an error message if the query fails
     * (and OK message otherwise).
     */
    private void handleTestAction(HttpServletResponse resp, RequestParameterMap reqMap) throws IOException {
    	CompanySettings config = reqMap.getConfig();
    	List<Unit> properties = new ArrayList<Unit>();
    	long time = System.nanoTime();
    	try {
    		properties = Unit.getUnits(config.getConnection(), false);
    	} catch (Exception e) {
    		e.printStackTrace(System.err);
    		// error message will be returned below since property list is empty
    	}
    	time = System.nanoTime() - time;
    	
    	// the below numbers can be plotted by Nagios
    	Writer w = new OutputStreamWriter(resp.getOutputStream(), "utf-8");
    	w.write((properties.isEmpty() ? "ERROR" : "OK") + ": time " + (time/1000000) + "; properties " + properties.size());
    	w.close();
    }
    
    private Writer getOutputStreamWriter(HttpServletRequest req, HttpServletResponse resp) throws IOException {
    	// we are sending XML to a cell phone! *Not* using gzip would be very rude.
    	OutputStream out = resp.getOutputStream();
    	String acc = req.getHeader("Accept-Encoding");
    	if (acc != null && acc.contains("gzip")) {
    		resp.setHeader("Content-Encoding", "gzip");
    		resp.setHeader("Vary", "Accept-Encoding");
    		out = new GZIPOutputStream(out);
    	}
    	return new OutputStreamWriter(out, "utf-8");
    }
    
    private void handleMobileAction(HttpServletRequest req, HttpServletResponse resp, RequestParameterMap reqMap) throws IOException {
    	Writer w = getOutputStreamWriter(req, resp);
    	MobileHandler mobileHandler = new MobileHandler(reqMap.getLogonInfo());
    	Plistable o = mobileHandler.handle(reqMap);
    	Plist.dump(o, w, false);
    	if (false) {
    		Writer debugOut = new OutputStreamWriter(System.out);
    		Plist.dump(o, debugOut, true);
    		debugOut.flush();
    	}
    	
    	w.close();
    }

}
