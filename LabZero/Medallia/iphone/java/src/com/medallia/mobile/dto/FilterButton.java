package com.medallia.mobile.dto;
import java.util.*;
import com.medallia.mobile.*;
import com.medallia.mobile.dto.*;
import com.medallia.mobile.dto.builders.*;
import com.medallia.mobile.color.UIColor;

// These files are generated automatically from declarations in DataFormat.h; see convert.pl for details.
//
// Do *not* modify this file directly!


/**
 *  A filter button
 */
@SuppressWarnings("unused")
public interface FilterButton extends Plist.Plistable {

	/**
	 *  Key used by server to filter results.
	 */
	@Required String key();

	/**
	 *  Text shown on button.
	 */
	@Required String title();


}
