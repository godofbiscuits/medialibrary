package com.medallia.mobile.responses;

import java.util.ArrayList;
import java.util.List;

import com.medallia.api.datamodel.AlertStatus;
import com.medallia.api.datamodel.Note;
import com.medallia.api.datamodel.Survey;
import com.medallia.api.datamodel.SurveyQuery;
import com.medallia.api.orm.Field;
import com.medallia.api.orm.Row;
import com.medallia.api.orm.SelectQuery;
import com.medallia.api.orm.Field.DateField;
import com.medallia.api.orm.Field.StringField;
import com.medallia.mobile.Common;
import com.medallia.mobile.DetailFields;
import com.medallia.mobile.FieldAndAction;
import com.medallia.mobile.FieldWithColor;
import com.medallia.mobile.RequestParameterMap;
import com.medallia.mobile.ResponseOutput;
import com.medallia.mobile.RequestParameterMap.Param;
import com.medallia.mobile.color.BackgroundColorHelper;
import com.medallia.mobile.color.UIColor;
import com.medallia.mobile.companysettings.CompanySettings;
import com.medallia.mobile.dto.AlertAction;
import com.medallia.mobile.dto.EmailAction;
import com.medallia.mobile.dto.LogNote;
import com.medallia.mobile.dto.SingleResponse;
import com.medallia.mobile.dto.builders.Build;
import com.medallia.mobile.dto.builders.Build.SingleResponseBuilder;
import com.medallia.mobile.filter.UnitFilter;

public class DetailedResponseScreen {
	private final CompanySettings config;
	private final RequestParameterMap req;
	
	private static final StringField UNITID_FIELD = new StringField("unitid", "Unit id");
	
	public DetailedResponseScreen(CompanySettings config, RequestParameterMap req) {
		this.config = config;
		this.req = req;
	}
	
	private static boolean isEmpty(String s) {
		return s == null || s.trim().length() == 0;
	}
	
	private String getPhoneNr(DetailFields fields, Row response) {
    	String nr = config.getDetailFields().getHardCodedPhoneNr();
    	if (nr != null) return nr;
        StringField phone = config.getDetailFields().getPhoneField();
        if (phone == null) return null;
        return response.getString(phone);
	}

	private void handleCapabilities(SingleResponseBuilder responseBuilder, String alertStatus) {
    	Capabilities caps = new Capabilities(config);
    	List<AlertAction> alertActions = new ArrayList<AlertAction>();
    	if (alertStatus != null && !alertStatus.isEmpty()) { // has an alert
    		AlertStatus status = AlertStatus.parse(alertStatus);
	    	if (config.usesResolvedAlertState() && caps.hasCap(Cap.ALERT_RESOLVE) && status != AlertStatus.CLOSED && status != AlertStatus.RESOLVED) {
	    		alertActions.add(AlertStatusAction.RESOLVED.getAlertAction());
	    	}
	    	if (caps.hasCap(Cap.ALERT_CLOSE)) {
	    		if (AlertStatus.CLOSED == status) {
	    			alertActions.add(AlertStatusAction.IN_PROGRESS.getAlertAction());
	    		} else {
	    			alertActions.add(AlertStatusAction.CLOSED.getAlertAction());
	    		}
	    	}
    	}
    	responseBuilder.alertActions(alertActions);
    	
    	if (caps.hasCap(Cap.ALERT_ADD_NOTE)) {
    		responseBuilder.canAddNote("1");
    	}
	}
	
	private String buildResponseHtml(SingleResponseBuilder responseBuilder, Survey response, DetailFields fields, String surveyId, int nrLogNotes) {
        ResponseOutput out = new ResponseOutput();
        boolean hasComment = false;
        boolean isEmployeeScorecard = req.get(Param.employee) != null;
        
        for (StringField f : fields.getCommentFields()) {
        	if (!response.getString(f).isEmpty()) {
        		hasComment = true;
        		break;
        	}
        }
        if (hasComment) {
            out.newSection("Comments");
            for (StringField f : fields.getCommentFields()) {
            	String comment = response.getString(f);
            	if (isEmpty(comment)) continue;
            	out.addFreeText(f.name, comment);
            }
        }
        
		out.newSection("Notes").addActionRow("Notes", "View/Add", String.valueOf(nrLogNotes));
        
        String alertType = response.getString(Survey.ALERT_TYPE);
        String alertStatus = response.getString(Survey.ALERT_STATUS);
        if (fields.showAlert() && !alertType.isEmpty()) {
    		out.newSection("Alert").addActionRow("Alert", alertType, alertStatus);
        }

        // We don't want to add the "Employee" section header if there is no data
    	boolean hasAddedSection = false;
        for (FieldAndAction faa : fields.getActionFields()) {
        	String value = faa.getValue(response);
        	if (value.isEmpty()) {
        		System.out.println("Skipping FieldAndAction " + faa.getName() + " because no data");
        		continue;
        	}
        	if (!hasAddedSection) {
        		hasAddedSection = true;
            	out.newSection("Employee"); // TODO: make this configurable
        	}
        	if (isEmployeeScorecard) {
        		out.addValueRowRightAligned(faa.getName(), value);
        	} else {
	        	String url = Common.urlencode(faa.getTextSearch(response)) + "&" + Param.unitid + "=" + UnitFilter.UNIT_PREFIX + Common.urlencode(response.getString(UNITID_FIELD)) +
	        		"&" + Param.scorecard_name + "=" + Common.urlencode(value);
	        	String responsesFilter = config.extractResponsesFilter(response);
	        	if (responsesFilter != null) {
	        		url += "&" + Param.resp_filter + "=" + Common.urlencode(responsesFilter);
	        	}
	        	out.addActionRowAlreadyEncoded(url, faa.getName(), value);
        	}
        }
        
        out.newSection("Score Summary");
        for (FieldWithColor f : fields.getScoreFields()) {
        	String scoreStr = f.getStringValue(response);
        	UIColor color = UIColor.BLACK;
        	try {
	        	Double score = response.getDouble(f);
	        	if (score == null) continue;
	        	color = BackgroundColorHelper.getInstance(config, f).uiColorForBackground(score, color);
        	} catch (NumberFormatException e) {
        		// OK, it's "N/A" or similar
        	}
        	out.addScoreRow(f.name, scoreStr, color);
        }
        
        out.newSection("Participant");
        for (Field f : fields.getParticipantFields()) {
        	out.addValueRow(f.name, response.getValueRaw(f));
        }
        
        // see http://developer.apple.com/safari/library/featuredarticles/iPhoneURLScheme_Reference/iPhoneURLScheme_Reference.pdf        
        // the most useful url schemes are tel:1234, sms:1234 and mailto:foo@bar.com?subject=foo&body=Hello%0aWorld
       
        out.newSection("Full response").addActionRow(config.getSurveyURI(surveyId), "View full response");

        responseBuilder.html(out.getRoot().toString());
        
        return alertStatus;
	}
	
	private void addPhone(SingleResponseBuilder responseBuilder, Row response, DetailFields fields) {
        String phoneNr = getPhoneNr(fields, response);
        if (!isEmpty(phoneNr)) {
            responseBuilder.customerPhone(phoneNr);
        }
	}
	
	private void addEmailAction(SingleResponseBuilder responseBuilder, Row response) {
        StringField commentField = config.getDetailFields().getCommentField();
        EmailAction emailAction = null;
        if (commentField != null) {
        	String name = "";
            List<StringField> nameFields = config.getDetailFields().getNameFields();
        	for (StringField nameField : nameFields) {
        		if (name.length() > 0) name += " ";
        		name += response.getString(nameField);
        	}
        	String comment = response.getString(commentField);
            if (!isEmpty(comment)) {
            	String subject = isEmpty(name) ? "Survey response" : ("Survey response from " + name);
            	String body = "\n\n-- Original text:\n" + comment;
            	emailAction = Build.emailAction().subject(subject).body(body).get();
            }
        }
        responseBuilder.emailAction(emailAction);
        
	}
	
	private List<LogNote> getLogNotes(String surveyId) {
		SelectQuery<Note> query = Note.getNoteQuery(surveyId);
		List<LogNote> notes = new ArrayList<LogNote>();
		for (Note note : query.run(config.getConnection())) {
			notes.add(Build.logNote().date(DateField.ISO_DF.format(note.date)).user(note.user).title(note.title).content(note.content).get());
		}
		return notes;
	}

    public SingleResponse get() {
    	// get survey scores
		String surveyId = req.get(Param.id);
		
    	DetailFields fields = config.getDetailFields();

		SurveyQuery query = new SurveyQuery();
    	query.addFields(fields.getCommentFields());
    	query.addField(Survey.ALERT_TYPE);
    	query.addField(Survey.ALERT_STATUS);
    	query.addFieldOrNull(fields.getPhoneField());
    	query.addFields(fields.getScoreFields());
    	query.addFields(fields.getParticipantFields());
    	query.addFieldOrNull(fields.getCommentField());
    	query.addField(fields.getPhoneField());
    	query.addFields(fields.getNameFields());
    	query.addField(UNITID_FIELD); // needed for creating scorecard url
    	query.addFields(fields.getExtraFieldsToLoad());
    	
    	for (FieldAndAction faa : fields.getActionFields()) {
    		query.addField(faa.getField());
    	}
    	
    	query.addCondition("surveyid = " + surveyId);
    	query.setLimit(1);

    	Survey response = query.runSingleRowQuery(config.getConnection());
    	if (response == null) throw new RuntimeException("Survey not found");
        
    	SingleResponseBuilder responseBuilder = Build.singleResponse();
    	
		// get log notes
        List<LogNote> notes = getLogNotes(surveyId);
        int nrLogNotes = notes.size();
        responseBuilder.notes(notes);

	    
    	String alertStatus = buildResponseHtml(responseBuilder, response, fields, surveyId, nrLogNotes);

        handleCapabilities(responseBuilder, alertStatus);

        addPhone(responseBuilder, response, fields);
        
        addEmailAction(responseBuilder, response);
        
        responseBuilder.scoreboardBaseURL(req.getLogonInfo().createURL("scorecard"));
        
        responseBuilder.name("foo");
        
        responseBuilder.alertStatus(response.getString(Survey.ALERT_STATUS));
        responseBuilder.alertName(response.getString(Survey.ALERT_TYPE));

        if (AlertStatusAction.CLOSED.getAlertAction().equals(req.get(Param.alert_action)))
        	responseBuilder.nrOpenAlerts(AlertNoteAction.getNrOpenAlerts(config));
        
        return responseBuilder.get();
    	
    }

}
