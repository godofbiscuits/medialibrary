package com.medallia.mobile.dashboard;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;
import tiny.Empty;

import com.medallia.api.datamodel.SurveyAggregate;
import com.medallia.api.datamodel.SurveyAggregateQuery;
import com.medallia.mobile.ApiUtil;
import com.medallia.mobile.BenchmarkTimeperiod;
import com.medallia.mobile.FieldWithColor;
import com.medallia.mobile.RequestParameterMap;
import com.medallia.mobile.ScoreFormatter;
import com.medallia.mobile.color.BackgroundColorHelper;
import com.medallia.mobile.color.UIColor;
import com.medallia.mobile.companysettings.CompanySettings;
import com.medallia.mobile.dto.Graph;
import com.medallia.mobile.dto.LineStyle;
import com.medallia.mobile.dto.SWLine;
import com.medallia.mobile.dto.SWPage;
import com.medallia.mobile.dto.builders.Build;
import com.medallia.mobile.dto.builders.Build.SWLineBuilder;
import com.medallia.mobile.filter.DashboardFilter;
import com.medallia.mobile.filter.UnitFilter.FilterUnitGroup;

public class ScoreDisplay {
	private final CompanySettings config;
	private final RequestParameterMap req;
	private final List<FieldWithColor> fields;
	private final FilterUnitGroup filterUnitGroup;
	
	private final GraphDataBuilder graphBuilder;
	
	public ScoreDisplay(CompanySettings config, RequestParameterMap req) {
		this.config = config;
		this.req = req;
		this.fields = config.getScoreFields(req.getActionRaw());
		this.filterUnitGroup = config.getScoreFilterUnitGroup(req.getActionRaw());
		graphBuilder = new GraphDataBuilder(config, req);
	}

	public SWPage get() {
		List<ScoreAndBench> scoreAndBench = new ArrayList<ScoreAndBench>();
		BenchmarkTimeperiod benchPeriod = getScoreAndBenchData(scoreAndBench);
		GraphData graphData = graphBuilder.getGraphData();
		List<List<List<Graph>>> graphScoreData = graphData.getMergedGraphScoreData();
		List<SWLine> lines = new ArrayList<SWLine>();
		for (int fIdx = 0; fIdx < fields.size(); ++fIdx) {
			ScoreFormatter sf = new ScoreFormatter(config, scoreAndBench.get(fIdx).score, scoreAndBench.get(fIdx).benchScore, benchPeriod.usesBenchmark());
			SWLineBuilder lineBuilder = Build.sWLine()
			        .title(fields.get(fIdx).name)
			        .tableCellNibName("ScoreWidgetNum")
			        .difference(sf.getDiffText())
			        .diffColor(sf.getDiffColor())
			        .value(sf.getScoreText())
			        .graphSmall(graphScoreData.get(0).get(fIdx))
			        .graphLarge(graphScoreData.get(1).get(fIdx));
			lineBuilder = BackgroundColorHelper
					.getInstance(config, fields.get(fIdx))
					.scoreBackgroundColor(lineBuilder, sf.getScore());
			lines.add(lineBuilder.get());
		}
		List<String> titles = graphData.getTitles();
		List<LineStyle> lineStyles = Empty.list();
		for (int i = 0; i < titles.size(); ++i) {
			if (i == 0) lineStyles.add(Build.lineStyle().legendText(titles.get(i)).featured("1").color(UIColor.LIGHTBLUE).get());
			else if (i == 1) lineStyles.add(Build.lineStyle().legendText(titles.get(i)).color(UIColor.GRAY).get());
			else if (i == 2) lineStyles.add(Build.lineStyle().legendText(titles.get(i)).color(UIColor.GRAY).get());
		}
		return Build.sWPage().benchTitle(benchPeriod.getShortName()).lines(lines).timePeriods(GraphTimePeriod.getTimePeriods()).lineStyles(lineStyles).get();
	}
	
	/**
	 * Add score and bench data for each field to the given list.
	 * @return benchmark time period
	 */
	private BenchmarkTimeperiod getScoreAndBenchData(List<ScoreAndBench> scoreAndBench) {
		SurveyAggregateQuery scoresQuery = new SurveyAggregateQuery();
		ApiUtil.whereTimePeriod(scoresQuery, req);
		List<Double> scores = getScoreData(scoresQuery);
		
		SurveyAggregateQuery benchQuery = new SurveyAggregateQuery();
		BenchmarkTimeperiod benchPeriod = ApiUtil.whereBenchTimePeriod(benchQuery, config, req);
		List<Double> bench;
		if (benchPeriod.usesBenchmark()) {
			bench = getScoreData(benchQuery);
			Assert.assertEquals(scores.size(), bench.size());
		} else {
			// just use scores -- it won't be displayed anyway
			bench = scores;
		}
		for (int i = 0; i < scores.size(); ++i)
			scoreAndBench.add(new ScoreAndBench(scores.get(i), bench.get(i)));
		return benchPeriod;
	}

	private void buildQuery(SurveyAggregateQuery query) {
		for (FieldWithColor f : fields)
			query.addField(f.avg());
		if (filterUnitGroup != null)
			query.addCondition("group = '" + filterUnitGroup.id + "'");
		for (DashboardFilter filter : config.getShownScoreboardFilters(req.getActionRaw()))
			filter.addFilter(req, query);
	}

	/** @return Scores for each field. */
	private List<Double> getScoreData(SurveyAggregateQuery query) {
		buildQuery(query);
		System.err.println("built query = " + query.getQueryString());
		
		SurveyAggregate row = query.runSingleRowQuery(config.getConnection());
		List<Double> scores = new ArrayList<Double>();
		for (FieldWithColor f : fields)
			scores.add(row.getDouble(f.avg())); // note: may be NaN if no data during period
		return scores;
	}
}
