package com.medallia.mobile.test.framework.querier.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.NotImplementedException;

import com.medallia.api.datamodel.Timeperiod;
import com.medallia.mobile.test.framework.beans.Id;
import com.medallia.mobile.test.framework.beans.LogNote;
import com.medallia.mobile.test.framework.config.ExpressEnvironment;
import com.medallia.mobile.test.framework.querier.AbstractQuerier;
import com.medallia.mobile.test.framework.querier.Querier;
import com.medallia.mobile.test.framework.querier.annotation.NotImplemented;


/**
 * Main class for querying the express server. 
 * 
 * <p>
 * 	This class should be the one used for querying the server.
 * 
 * <p>
 * 	Basically, this class implements the commands defined in {@link Querier}, and contains
 * the different implementations for querying the Express Server (through API, through DB or through
 * the Test Page, for now). 
 * 	Based on a priority (first querying through API, then querying through the Test Page and finally
 * going to the DB), this class figures it out which of the implementations has that particular method
 * implemented, and executes it.
 * 
 * @author fernando
 */
public class ExpressQuerier extends AbstractQuerier {
	
	private final Querier dBQuerier;
	private final Querier qAApiQuerier;
	private final Querier productionApiQuerier;
	private final Querier testPageQuerier;
	
	@Override
	public int getNumberOfSurveys(Timeperiod timePeriod) {
		Querier querier = getQuerier(Timeperiod.class);
		return querier.getNumberOfSurveys(timePeriod);
	}
	
	@Override
	public int getNumberOfSurveys(Timeperiod timePeriod,
			Map<String, String> filters) {
		Querier querier = getQuerier(Timeperiod.class, Map.class);
		return querier.getNumberOfSurveys(timePeriod, filters);
	}
	
	@Override
	public List<Timeperiod> getTimePeriods() {
		Querier querier = getQuerier();
		return querier.getTimePeriods();
	}
	
	@Override
	public double getAverageFromSurvey(String field, String beginDate,
			String endDate) {
		Querier querier = getQuerier(String.class, String.class, String.class);
		return querier.getAverageFromSurvey(field, beginDate, endDate);
	}
	
	@Override
	public double getAverageFromSurvey(String field, Id<?> timePeriod) {
		Querier querier = getQuerier(String.class, Id.class);
		return querier.getAverageFromSurvey(field, timePeriod);
	}
	
	@Override
	public List<LogNote> getLogNotes(Id<?> surveyId) {
		Querier querier = getQuerier(Id.class);
		return querier.getLogNotes(surveyId);
	}
	
	@Override
	public Id<?> createDummySurvey(String company, String unit) {
		Querier querier = getQuerier(String.class, String.class);
		return querier.createDummySurvey(company, unit);
	}
	
	@Override
	public void triggerAlert(String company, String alert, Id<?> survey) {
		Querier querier = getQuerier(String.class, String.class, Id.class);
		querier.triggerAlert(company, alert, survey);
	}	

	/**
	 * Figures it out which {@link Querier} has the method implemented, and returns that {@link Querier}.
	 * 
	 * <p> Chooses the {@link Querier} by a defined priority:
	 * <ul>
	 * 	<li> 1. The Production Api Querier.
	 *  <li> 2. The QA Api Querier (if we ever have one).
	 *  <li> 3. The Test Page.
	 *  <li> 4. Directly to the DB (if we ever do that).
	 * </ul>
	 * @param parameterTypes 
	 * 
	 * @return the {@link Querier} that is able to execute the method.
	 */
	private Querier getQuerier(Class<?>... parameterTypes) {
		String methodName = Thread.currentThread().getStackTrace()[2].getMethodName();
		try {
		
		//	List<Class<?>> parameterTypeList = methodParameters.get()
			if (productionApiQuerier.getClass().getMethod(methodName, parameterTypes)
					.getAnnotation(NotImplemented.class) == null) {
				return productionApiQuerier;
			}
			if (qAApiQuerier.getClass().getMethod(methodName, parameterTypes).getAnnotation(
					NotImplemented.class) == null) {
				return qAApiQuerier;
			}
			if (testPageQuerier.getClass().getMethod(methodName, parameterTypes).getAnnotation(
					NotImplemented.class) == null) {
				return testPageQuerier;
			}			
			if (dBQuerier.getClass().getMethod(methodName, parameterTypes).getAnnotation(
					NotImplemented.class) == null) {
				return dBQuerier;
			}
		} catch (SecurityException e) {
			throw new RuntimeException(e);
		} catch (NoSuchMethodException e) {
			throw new RuntimeException(e);
		}
		throw new NotImplementedException(methodName
				+ " is not implemented at all yet");
	}

	/**
	 * Creates an {@link ExpressQuerier}. Use the static creator instead.
	 */
	private ExpressQuerier(ExpressEnvironment express) {
		dBQuerier = DBQuerier.createQuerier(express);
		qAApiQuerier = QAApiQuerier.createQuerier(express);
		productionApiQuerier = ProductionApiQuerier.createQuerier(express);
		testPageQuerier = TestPageQuerier.createQuerier(express);
	}
	
	/**
	 * Creates and returns a {@link Querier} based on a specific {@link ExpressEnvironment}. 
	 */
	public static Querier createExpressQuerier(ExpressEnvironment express) {
		return new ExpressQuerier(express);
	}
}
