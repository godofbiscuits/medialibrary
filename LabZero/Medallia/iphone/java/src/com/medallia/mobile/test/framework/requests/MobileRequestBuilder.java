package com.medallia.mobile.test.framework.requests;

import java.util.HashMap;
import java.util.Map;

import com.medallia.mobile.RequestParameterMap.Action;
import com.medallia.mobile.dto.builders.Build.Constants;
import com.medallia.mobile.test.framework.beans.Id;
import com.medallia.mobile.test.framework.config.ExpressEnvironment;
import com.medallia.mobile.test.framework.requests.impl.MobileRequestImpl;

/**
 * Utility class that helps creating {@link MobileRequests}. 
 * 
 * @author fernando
 */
public class MobileRequestBuilder {

	private String user;
	
	private String password;
	
	private String company;
	
	private String surveyId;
	
	private int timeperiod;
	
	private Map<String, String> otherParameters;
	
	/**
	 * Creates a builder with the generic info of the environment.
	 */
	private MobileRequestBuilder(ExpressEnvironment expressEnvironment) {
		otherParameters = new HashMap<String, String>();
		user = expressEnvironment.getUser();
		password = expressEnvironment.getPassword();
		company = expressEnvironment.getCompany();
		surveyId = expressEnvironment.getValidSurveyId();
		timeperiod = expressEnvironment.getTimeperiod();
	}
	
	/**
	 * Sets the user. 
	 */
	public MobileRequestBuilder user(String user) {
		this.user = user;
		return this;
	}
	
	/**
	 * Sets the password. 
	 */
	public MobileRequestBuilder password(String password) {
		this.password = password;
		return this;
	}
	
	/**
	 * Sets the company. 
	 */
	public MobileRequestBuilder company(String company) {
		this.company = company;
		return this;
	}
	
	/**
	 * Sets the version. 
	 */
	public MobileRequestBuilder version(int version) {
		otherParameters.put("version", Integer.toString(version));
		return this;
	}
	
	/**
	 * Sets the alert action.
	 */
	public MobileRequestBuilder alert(String alert) {
		otherParameters.put("alert_action", alert);
		return this;
	}
	
	/**
	 * Sets the surveyId. 
	 */
	public MobileRequestBuilder surveyId(Id<?> id) {
		if (id==null) {
			otherParameters.put("id", null);	
		} else {
			otherParameters.put("id", id.toString());			
		}
		return this;
	}
	
	/**
	 * Sets a Log Note;
	 */
	public MobileRequestBuilder logNote(String logNote) {
		otherParameters.put("note_content", logNote);
		return this;
	}
	
	/**
	 * Sets a Time Period;
	 */
	public MobileRequestBuilder timePeriod(Id<?> id) {
		if (id==null) {
			otherParameters.put("tp", null);	
		} else {
			otherParameters.put("tp", id.toString());
		}
		return this;
	}
	
	/**
	 * Sets a Scope.
	 */
	public MobileRequestBuilder scope(String scope) {
		otherParameters.put("scope", scope);
		return this;
	}
	
	/**
	 * Sets the Satisfaction Range. 
	 */
	public MobileRequestBuilder satRange(String range) {
		otherParameters.put("satrange", range);
		return this;
	}
	
	/**
	 * Sets a Unit
	 */
	public MobileRequestBuilder unit(String id) {
		otherParameters.put("unitid", id);
		return this;
	}
	
	public MobileRequestBuilder scorecardName(String scorecardName) {
		otherParameters.put("scorecard_name", scorecardName);
		return this;
	}
	
	public MobileRequestBuilder rankerRequest(String rankerRequest) {
		otherParameters.put("rankerq", "earth_ov_service");
		return this;
	}
	
	/**
	 * Sets the alerts only.
	 */
	public MobileRequestBuilder alertsOnly(boolean enable) {
		if (enable) {
			otherParameters.put("alerts_only", "1");
		} else {
			if (otherParameters.containsKey("alerts_only")) {
				otherParameters.remove("alerts_only");
			}
		}
		return this;
	}


	public MobileRequestBuilder commentsOnly(boolean enable) {
		if (enable) {
			otherParameters.put("comments_only", "1");
		} else {
			if (otherParameters.containsKey("comments_only")) {
				otherParameters.remove("comments_only");
			}
		}
		return this;
	}
	
	/**
	 * Sets the response text.
	 */
	public MobileRequestBuilder responseText(String responseText) {
		otherParameters.put("response_text", responseText);
		return this;
	}
	
	/**
	 * Sets the offset.
	 */
	public MobileRequestBuilder offset(int offset) {
		otherParameters.put("offset", String.valueOf(offset));
		return this;
	}
	
	/**
	 * Returns a {@link MobileRequest} for the login request. 
	 */
	public MobileRequest buildLoginRequest() {
		if (!otherParameters.containsKey("version")) {
			otherParameters.put("version", Integer.toString(Constants.FIRST_COMPATIBLE_CLIENT_VERSION));
		}
		return build(Action.login);
	}
	
	/**
	 * Returns a {@link MobileRequest} for the test request. 
	 */
	public MobileRequest buildTestRequest() {
		return build(Action.test);
	}
	
	/**
	 * Returns a {@link MobileRequest} for the details request. 
	 */
	public MobileRequest buildDetailsRequest() {
		if (!otherParameters.containsKey("id")) {
			otherParameters.put("id", surveyId);
		}
		return build(Action.details);
	}
	
	/**
	 * Returns a {@link MobileRequest} for the responses request. 
	 */
	public MobileRequest buildResponsesRequest() {
		return build(Action.responses);
	}
	
	/**
	 * Returns a {@link MobileRequest} for the ranker request. 
	 */
	public MobileRequest buildRankerRequest() {
		if (!otherParameters.containsKey("tp")) {
			otherParameters.put("tp", String.valueOf(timeperiod));
		}
		return build(Action.ranker);
	}
	
	public MobileRequest buildAlertsRequest() {
		if (!otherParameters.containsKey("id")) {
			otherParameters.put("id", surveyId);
		}
		return build(Action.details);
	}
	
	public MobileRequest buildLogNotesRequest() {
		if (!otherParameters.containsKey("id")) {
			otherParameters.put("id", surveyId);
		}
		return build(Action.details);
	}
	
	/**
	 * Returns a {@link MobileRequest} for the scores request.
	 */
	public MobileRequest buildScoresRequest() {
		if (!otherParameters.containsKey("tp")) {
			otherParameters.put("tp", String.valueOf(timeperiod));
		}return build(Action.scores);
	}
	
	/**
	 * Returns a {@link MobileRequest} for the scorecard request.
	 */
	public MobileRequest buildScorecardRequest() {
		if (!otherParameters.containsKey("tp")) {
			otherParameters.put("tp", String.valueOf(timeperiod));
		}
		return build(Action.scorecard);
	}
	
	/**
	 * Returns a {@link MobileRequest} for a generic request 
	 */
	private MobileRequest build(Action action) {
		MobileRequest mobileRequest = new MobileRequestImpl(user, password, company, action.name());
		for(Map.Entry<String, String> parameter : otherParameters.entrySet()) {
		    mobileRequest.addParameter(parameter.getKey(), parameter.getValue());
		}
		return mobileRequest;		
	}
	
	/**
	 * Static method for creating a Builder.
	 */
	public static MobileRequestBuilder getBuilder(ExpressEnvironment expressEnvironment) {
		return new MobileRequestBuilder(expressEnvironment);
	}
}
