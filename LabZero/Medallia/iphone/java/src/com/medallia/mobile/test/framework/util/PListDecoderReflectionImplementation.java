package com.medallia.mobile.test.framework.util;

import java.io.IOException;
import java.io.Reader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

import com.medallia.mobile.color.UIColor;
import com.medallia.mobile.dto.ClickableHeaderElement;
import com.medallia.mobile.dto.ConfigFile;
import com.medallia.mobile.dto.ConfigPart;
import com.medallia.mobile.dto.ConfigPartType;
import com.medallia.mobile.dto.RankerData;
import com.medallia.mobile.dto.ResponsesLump;
import com.medallia.mobile.dto.SWPage;
import com.medallia.mobile.dto.Scorecard;
import com.medallia.mobile.dto.SingleResponse;
import com.medallia.mobile.dto.builders.Build;

/**
 *	Implementation of the {@link PListDecoder} based on reflection.
 *
 * <p> Notes and Todos:
 * There is a hack when parsing an element that is a string:
 * <ul>
 * 	<li> Right now if it is a {@link UIColor} it hard codes a value, also it
 * 		 specifically looks for the string "Diffcolor" and "scoreColor", which is bad.
 *  <li> Also it does something similar for the type value on the {@link ConfigPart}.
 * </ul> 
 */
public class PListDecoderReflectionImplementation implements PListDecoder {

	private final Map<Class<?>, Class<?>> classMap = createMap();
	private final SAXBuilder builder = new SAXBuilder();
	
	@Override
	public ConfigFile loginPage(Reader plist) {
		return (ConfigFile) getResponse(Build.ConfigFileBuilder.class, plist);
	}

	@Override
	public RankerData rankerPage(Reader plist) {
		return (RankerData) getResponse(Build.RankerDataBuilder.class, plist);
	}

	@Override
	public ResponsesLump responsesPage(Reader plist) {
		return (ResponsesLump) getResponse(Build.ResponsesLumpBuilder.class, plist);
	}

	@Override
	public Scorecard scorecardPage(Reader plist) {
		return (Scorecard) getResponse(Build.ScorecardBuilder.class, plist);
	}

	@Override
	public SWPage scoresPage(Reader plist) {
		return (SWPage) getResponse(Build.SWPageBuilder.class, plist);
	}

	@Override
	public SingleResponse singleResponsePage(Reader plist) {
		return (SingleResponse) getResponse(Build.SingleResponseBuilder.class, plist);
	}

	/**
	 * Parses the plist and returns a response from the Mobile servlet.
	 * 
	 * @param clazz is the builder class corresponding to the type of the response.
	 * @param plist the raw response.
 	 * @return an object that is a logical representation of the raw response.
	 */
	private Object getResponse(Class<?> clazz, Reader plist) {
		try {
			Document document = builder.build(plist);
			Element root = document.getRootElement();
			return parse(clazz, root.getChild("dict"));
		} catch (IllegalArgumentException e) {
			throw new RuntimeException(e);
		} catch (SecurityException e) {
			throw new RuntimeException(e);
		} catch (InstantiationException e) {
			throw new RuntimeException(e);
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		} catch (InvocationTargetException e) {
			throw new RuntimeException(e);
		} catch (NoSuchMethodException e) {
			throw new RuntimeException(e);
		} catch (JDOMException e) {
			throw new RuntimeException(e);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Parses a specific element, returning a logical representation of
	 * the content.
	 * 
	 * <p> Brief description:
	 * It receives the builder (one of {@link Build}). There is a special
	 * case that happens when it receives a {@link Map} instead (for the arguments
	 * method).
	 * For each pair (key, value) in the data, it figures it out if what type of
	 * element contains.
	 * <ul>
	 * 	<li> For an array, it creates a List, and adds an element for each element
	 * 		 of the array.
	 * 	<li> If it is a real, it simple creates a double with the value.
	 * 	<li> If it is a String, it creates a String. This case has several 
	 *       special cases:
	 *  	<ul>
	 *  		<li> If it is diffColor or scoreColor or color, it tries to parse the value
	 *               and return the {@link UIColor}. (TODO) Only returns one color.
	 *          <li> If the builder is a {@link Map}, it doesn't use reflection
	 *               to figure it out the method, but just call put with the key and 
	 *               value.
	 *          <li> For the type method in {@link ConfigPart}, that receives a 
	 *          	 {@link ConfigPartType}, it parses the String and returns a
	 *          	 specific {@link ConfigPartType}.
	 *          <li> For the headerElement method in {@link ConfigPart}, that receives a 
	 *          	 {@link ClickableHeaderElement}, it parses the String and returns a
	 *          	 specific {@link ClickableHeaderElement}.
	 *          <li> For the rest, it uses reflection to call the specific method of
	 *               the builder.
	 *       </ul>
	 *  <li> If it is a Dict, it figures it out which type of object to create and
	 *       then parses it.
	 * </ul>
	 * Finally, it tries to call the get method of the builder. If it was a {@link Map}
	 * it simply returns the {@link Map}.
	 * 
	 * @param clazz the class to be used with reflection in order to create
	 *        the object.
	 * @param data the {@link Element} that contains the raw information.
 	 * @return an object that is a logical representation of the raw response.
	 */
	@SuppressWarnings("unchecked")
	private Object parse(Class<?> clazz, Element data)
			throws InstantiationException, IllegalAccessException,
			IllegalArgumentException, SecurityException,
			InvocationTargetException, NoSuchMethodException {
		Object builder = clazz.newInstance();
		List<Element> children = data.getChildren();
		int size = 0;
		while (size < children.size()) {
			Element key = children.get(size);
			Element value = children.get(size + 1);
			Object objectValue = null;
			if (value.getName().equals("array")) {
				List<Object> objectValueList = new LinkedList<Object>();
				List<Element> arrayChildren = value.getChildren();
				for (Element arrayChild : arrayChildren) {
					objectValueList.add(parse(getBuilderClass(key.getValue(),
							builder.getClass()), arrayChild));
				}
				objectValue = objectValueList;
				builder.getClass().getMethod(key.getValue(), List.class)
						.invoke(builder, objectValue);
			
			} else if (value.getName().equals("real")) {
					builder.getClass().getMethod(key.getValue(), double.class)
							.invoke(builder, new Double(value.getValue()));
			
			} else if (value.getName().equals("string")) {
				if (builder.getClass().equals(LinkedHashMap.class)) {
					((Map<String, String>) builder).put(key.getValue(),
							value.getValue());
				} else {
					if ((key.getValue().equals("type")) 
						&& (builder.getClass().equals(Build.ConfigPartBuilder.class))) {
						((Build.ConfigPartBuilder) builder)
							.type(ConfigPartType.valueOf(value.getValue()));
					} else if ((key.getValue().equals("headerElement")) 
						&& (builder.getClass().equals(Build.ConfigPartBuilder.class))) {
						((Build.ConfigPartBuilder) builder)
							.headerElement(ClickableHeaderElement.valueOf(value.getValue()));
					} else {
						objectValue = value.getValue();
						if (key.getValue().equals("diffColor")
							|| (key.getValue().equals("scoreColor"))
							|| (key.getValue().equals("color"))) {
							objectValue = handleColor(value.getValue());
						}
						builder.getClass().getMethod(key.getValue(),
								objectValue.getClass()).invoke(builder,objectValue);
 					}
				}
			
			} else if (value.getName().equals("dict")) {
					objectValue = parse(getBuilderClass(key.getValue(),
							builder.getClass()), value);
					try {
						builder.getClass().getMethod(key.getValue(),
								Class.forName(objectValue.getClass()
								.getInterfaces()[0].getName())).invoke(
								builder, objectValue);
					} catch (ClassNotFoundException e) {
						throw new RuntimeException(e);
					}
				}
				size += 2;
	
			}
		if (builder.getClass().equals(LinkedHashMap.class)) {
			return builder; 
		} else {
			return builder.getClass().getMethod("get").invoke(builder);			
		}
	}

	/**
	 * Based on the enclosing class, and the name of the method, it returns the
	 * builder that can create the object that the method needs as an input.
	 * 
	 * <p> Brief Description:
	 * There are 3 main options:
	 * <ul>
	 * 	<li> The method requires a specific object.
	 *  <li> The method requires a List\<object\>.
	 *  <li> The method requires a {@link Map}.
	 * </ul>
	 * The method will cycle through the different methods of the class,
	 * whenever it finds the method, it looks for what type of object needs.
	 * 
	 * @param name the name of the method.
	 * @param clazz the class that has the method.
	 * @return a builder that can create the object that the method needs as an input.
	 */
	private Class<?> getBuilderClass(String name, Class<?> clazz) {
		Method[] methods = clazz.getDeclaredMethods();
		for (int index = 0; index < methods.length; index++) {
			if (methods[index].getName().equals(name)) {
				if (methods[index].getGenericParameterTypes()[0] instanceof ParameterizedType) {
					ParameterizedType type = (ParameterizedType) methods[index]
							.getGenericParameterTypes()[0];
					if (type.getRawType().equals(Map.class)) {
						return LinkedHashMap.class;
					}
					return classMap.get(type.getActualTypeArguments()[0]);
				} else {
					try {
						return classMap.get(Class.forName(methods[index]
								.getParameterTypes()[0].getName()));
					} catch (ClassNotFoundException e) {
						throw new RuntimeException(e);
					}
				}
			}
		}
		return null;
	}

	/**
	 * Cycles using reflection through all the builders inside {@link Build}, and
	 * maps the builder with the type of object that it builds.
	 * 
	 * @return The map with the relations builder --> object that the builder creates.
	 */
	private Map<Class<?>, Class<?>> createMap() {
		Map<Class<?>, Class<?>> result = new LinkedHashMap<Class<?>, Class<?>>();
		Method[] methods = Build.class.getMethods();
		for (int index = 0; index < methods.length; index++) {
			Class<?> builder = methods[index].getReturnType();
			try {
				result.put(builder.getMethod("get").getReturnType(), builder);
			} catch (SecurityException e) {
				throw new RuntimeException(e);
			} catch (NoSuchMethodException e) {
				// Expected that not all methods have the get field.
			}
		}
		return result;
	}

	/**
	 * Returns the {@link UIColor} that represents the input value.
	 * 
	 * TODO (fernando) implement it.
	 * 
	 * @param value the input value, a color in hex.
	 * @return a {@link UIColor} that corresponds to the input value.
	 */
	private UIColor handleColor(String value) {
		return UIColor.BLACK;
	}
}