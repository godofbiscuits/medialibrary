package com.medallia.mobile.dashboard;

import java.util.ArrayList;
import java.util.List;

import com.medallia.mobile.FieldWithColor;

class FieldScores {
	private int todo; // TODO remove this class. only use singlescore. refactor ScoreDisplay.
	
	/** Scores for all fields. */
	public List<SingleScore> scores = new ArrayList<SingleScore>();

	/** Score for a single field. */
	public static class SingleScore {
		public int count;
		private double sum;
		
		public SingleScore() {
			count = 0;
			sum = 0;
		}
		
		public void add(FieldResult result) {
			count += result.count;
			sum += result.avg * result.count;
		}
		
		public double getAvg() {
			return sum / count;
		}
		
		@Override
		public String toString() {
			return String.valueOf(getAvg());
		}
	}
	
	FieldScores(List<FieldWithColor> fields) {
		for (int i = 0; i < fields.size(); ++i)
			scores.add(new SingleScore());
	}
	
	/** Adds all FieldResults for the given list to the scores for the fields in this instance. */
	void add(List<FieldResult> results) {
		assert scores.size() == results.size();
		for (int i = 0; i < scores.size(); ++i)
			scores.get(i).add(results.get(i));
	}

	/**
	 * Adds the average value of the scores in this instance to the end of the list of [list of graph point scores] for each field.
	 * This method should be called for each graph point (x value). Creates new lists of scores if the input list is empty.
	 */
	void addTo(List<List<GraphPoint>> graphDataAllFields, double x) {
		if (graphDataAllFields.isEmpty())
			for (int i = 0; i < scores.size(); ++i)
				graphDataAllFields.add(new ArrayList<GraphPoint>());
		for (int i = 0; i < scores.size(); ++i) {
			double y = scores.get(i).getAvg();
			if (!Double.isNaN(y)) {
				graphDataAllFields.get(i).add(new GraphPoint(x, y));
			}
		}
	}
	
	@Override
	public String toString() {
		return scores.toString();
	}
}