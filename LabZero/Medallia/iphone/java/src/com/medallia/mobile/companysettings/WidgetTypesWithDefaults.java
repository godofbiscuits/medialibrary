/**
 * 
 */
package com.medallia.mobile.companysettings;

import java.util.ArrayList;
import java.util.List;

import com.medallia.mobile.dto.WidgetInstance;
import com.medallia.mobile.dto.WidgetType;

public class WidgetTypesWithDefaults {
	public List<WidgetType> types = new ArrayList<WidgetType>();
	public List<WidgetInstance> instances = new ArrayList<WidgetInstance>();
	
	protected void add(WidgetType type, WidgetInstance instance) {
		types.add(type);
		instances.add(instance);
	}
}