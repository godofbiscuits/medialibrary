package com.medallia.mobile.test.tests;

import com.medallia.mobile.dto.SingleResponse;
import com.medallia.mobile.dto.builders.Build.Constants;
import com.medallia.mobile.test.framework.MobileTestCase;
import com.medallia.mobile.test.framework.beans.Id;
import com.medallia.mobile.test.framework.config.Environment;
import com.medallia.mobile.test.framework.config.ExpressEnvironment;
import com.medallia.mobile.test.framework.config.MobileEnvironment;
import com.medallia.mobile.test.framework.requests.MobileRequest;
import com.medallia.mobile.test.framework.requests.MobileRequestBuilder;

@Environment(express=ExpressEnvironment.EARTH, mobile=MobileEnvironment.LOCALHOST)
public class LocalDetailsRequestTest extends MobileTestCase {
	
	private MobileRequest detailsRequest;
	
	/**
	 * Checks that the different alert actions are shown depending on the different
	 * capabilities of the user.
	 * 
	 * <p> Preconditions
	 * <ul>
	 * 	<li> Survey with id 18 contains an alert.
	 *  <li> User Kristian can do everything.
	 *  <li> User customercare1 can only close alerts.
	 *  <li> User unitmanager1 can't don anything.
	 * </ul>
	 * <p> Steps:
	 * <ul>
	 * 	<li> Send a request with the user Kristian.
	 *  <li> Check that the alert actions contains both close and resolve actions.
	 *  <li> Send a request with the user customercare1.
	 *  <li> Check that the alert actions only contains the close action.
	 *  <li> Send a request with the user unitmanager1.
	 *  <li> Check that the alert actions list is empty.
	 * </ul>
	 */
	public void testAlertActionsAreShownWithSurveyWithAlert() throws Exception {
		detailsRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment())
		 .surveyId(new Id(18)).buildDetailsRequest();
		String re = getResponse(detailsRequest);
		SingleResponse response = getDetailedResponseResponse(detailsRequest);
		assertEquals(2, response.alertActions().size());
		assertEquals("resolved", response.alertActions().get(0).action());
	    assertEquals("Resolve", response.alertActions().get(0).text());
		assertEquals("closed", response.alertActions().get(1).action());
		assertEquals("Close", response.alertActions().get(1).text());
		
		detailsRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment())
			.user("customercare1").password("test1234").surveyId(new Id(18)).buildDetailsRequest();
		response = getDetailedResponseResponse(detailsRequest);
		assertEquals(1, response.alertActions().size());
		assertEquals("closed", response.alertActions().get(0).action());
		assertEquals("Close", response.alertActions().get(0).text());

		detailsRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment())
			.user("unitmanager1").password("test1234").surveyId(new Id(18)).buildDetailsRequest();
		response = getDetailedResponseResponse(detailsRequest);
		assertEquals(0, response.alertActions().size());
	}
	
	/**
	 * Checks that no alerts actions are received if the survey has no alert open.
	 * 
	 * <p> Preconditions:
	 * <ul>
	 * 	<li> The survey with id 68 does not have an alert.
	 * </ul>
	 * <p> Steps:
	 * <ul>
	 * 	<li> Send a request with user Kristian.
	 *  <li> Assert that the alert action list is empty.
	 * </ul>
	 */
	public void testNoAlertActionIsShownWithSurveyWithoutAlert() throws Exception {
		detailsRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment())
			.surveyId(new Id(68)).buildDetailsRequest();
		SingleResponse response = getDetailedResponseResponse(detailsRequest);
		assertEquals(0, response.alertActions().size());
	}
	
	/**
	 * Checks that the correct number of log notes are being shown
	 * 
	 * <p> Preconditions:
	 * <ul>
	 * 	<li> The survey with id 18 has two log notes.
	 * 	<li> The survey with id 68 does not have a log note.
	 * </ul>
	 * <p> Steps:
	 * <ul>
	 * 	<li> Send a request with survey id 18.
	 *  <li> Assert that the response contains 2 log notes, with the correct data.
	 *  <li> Send a request with survey id 68.
	 *  <li> Assert that the response does not contain a log note.
	 * </ul>
	 */
	public void testLogNotesAreBeingShown() throws Exception {
		detailsRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment())
			.surveyId(new Id(18)).buildDetailsRequest();
		SingleResponse response = getDetailedResponseResponse(detailsRequest);
		assertEquals(2, response.notes().size()); 
		assertEquals("2009-06-21",response.notes().get(0).date());
		assertEquals("Tag assigned",response.notes().get(0).title());
		assertEquals("kristian",response.notes().get(0).user());
		assertEquals("The tag Alert - Facilities was added to the alert",response.notes().get(0).content());
		assertEquals("2009-06-21",response.notes().get(1).date());
		assertEquals("Alert Created: A1",response.notes().get(1).title());
		assertEquals("",response.notes().get(1).user());
		assertEquals("Recommend <=2",response.notes().get(1).content());
		
		detailsRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment())
			.surveyId(new Id(68)).buildDetailsRequest();
		response = getDetailedResponseResponse(detailsRequest);
		assertEquals(0, response.notes().size());
		fail("The user of the second note is empty, in the UI it shows \"System Generated\", uncomment this if it is ok");
	}

	/**
	 * Checks that the canAddNote property is only set for a user that has that
	 * capability.
	 * 
	 * <p> Preconditions:
	 * <ul>
	 * 	<li> The user Kristian has the capability to add notes
	 * 	<li> The user unitmanager1 does not have the capability to add notes.
	 * </ul>
	 * <p> Steps:
	 * <ul>
	 * 	<li> Send a request with the user Kristian.
	 *  <li> Assert that he can add notes.
	 *  <li> Send a request with the user unitmanager1.
	 *  <li> Assert that he can't add notes.
	 * </ul>
	 */
	public void testCanAddNotesShowsUpWithCorrectCapability() throws Exception {
		detailsRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment())
			.surveyId(new Id(18)).buildDetailsRequest();
		SingleResponse response = getDetailedResponseResponse(detailsRequest);
		assertEquals("1", response.canAddNote());
		
		detailsRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment())
			.user("unitmanager1").password("test1234").surveyId(new Id(18)).buildDetailsRequest();
		response = getDetailedResponseResponse(detailsRequest);
		assertEquals(null, response.canAddNote());
	}
	
	/**
	 * Checks that the emailAction contains the right info.
	 * 
	 * <p> Preconditions:
	 * <ul>
	 * 	<li> Survey with id 17 does not have a name, and contains 2 comments.
	 * 	<li> Survey with id 68 contains a name, and also has 2 comments.
	 * 	<li> Survey with id 10 does not have comments.
	 * </ul>
	 * <p> Steps:
	 * <ul>
	 * 	<li> Send a request for survey with id 17.
	 * 	<li> Check that the name does not appear in the subject, and the body contains both comments.
	 * 	<li> Send a request for survey with id 68.
	 * 	<li> Check that the name of the person is shown in the subject, and also that the body contains
	 *       both comments.
	 *  <li> Send a request for survey with id 10.
	 *  <li> Check that the email action is empty.
	 * </ul>
	 */
	public void testEmailActionContainsRightInfo() throws Exception {
		detailsRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment())
			.surveyId(new Id(17)).buildDetailsRequest();
		SingleResponse response = getDetailedResponseResponse(detailsRequest);
		assertTrue(response.emailAction().body().contains("forgot clean towels"));
		assertTrue(response.emailAction().body().contains("SHUTTLE TRANSPORTATION AND PARKING"));
		assertEquals("Survey response", response.emailAction().subject());
		
		detailsRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment())
			.surveyId(new Id(68)).buildDetailsRequest();
		response = getDetailedResponseResponse(detailsRequest);
		assertTrue(response.emailAction().body().contains("Safe not working / Gym"));
		assertTrue(response.emailAction().body().contains("We have many meetings at this"));
		assertEquals("Survey response from Wayne Kelly", response.emailAction().subject());
		
		detailsRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment())
			.surveyId(new Id(10)).buildDetailsRequest();
		response = getDetailedResponseResponse(detailsRequest);
		assertNull(response.emailAction());		
	}
	
	/**
	 * ???
	 */
	public void testNameShowsCorrectName() throws Exception {
		detailsRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment())
			.surveyId(new Id(17)).buildDetailsRequest();
		SingleResponse response = getDetailedResponseResponse(detailsRequest);
		assertEquals("foo", response.name());
		fail("what should it be displayed here?");
	}
	
	
	/**
	 * Tests that it is not possible to get the request with an invalid survey id.
	 * 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Send a request with a non existent id.
	 * 	<li> Check that the request fails.
	 * </ul>
	 */
	public void testDetailsRequestFailsWithInvalidSurveyId() throws Exception {
		detailsRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).surveyId(new Id(432132343)).buildDetailsRequest();
		assertTextPresent(getResponse(detailsRequest), "Failed");
	}
	
	/**
	 * Tests that it is not possible to get the request with no survey id
	 * 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Send a request without an id.
	 * 	<li> Check that the request fails.
	 * </ul>
	 */	
	public void testDetailsRequestFailsWithNullSurveyId() throws Exception {
		detailsRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).surveyId(null).buildDetailsRequest();
		assertTextPresent(getResponse(detailsRequest), "Expected a number or string, but got 'null'");
	}
	
	/**
	 * Tests that it is not possible to login with an unknown company. 
	 * 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Sends a request with an unknown company.
	 * 	<li> Checks that the request does not go through and shows a proper message.
	 * </ul>
	 */
	public void testdetailsRequestFailsIfCompanyUnknown() throws Exception {
		detailsRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).company("unknownCompany")
			.version(Constants.FIRST_COMPATIBLE_CLIENT_VERSION).buildDetailsRequest();
		assertTextPresent(getResponse(detailsRequest), "Unknown company");
	}
	
	/**
	 * Tests that it is not possible to login with a wrong password.
	 * 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Sends a request with wrong password.
	 * 	<li> Checks that the request fails and shows a proper message.
	 * </ul> 	
	 */ 
	public void testdetailsRequestFailsIfWrongPassword() throws Exception {
		detailsRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).password("wrongPassword")
			.version(Constants.FIRST_COMPATIBLE_CLIENT_VERSION).buildDetailsRequest();
		assertTextPresent(getResponse(detailsRequest), "Not authorized");
	}
	
	/**
	 * Tests that it is not possible to login with a non existent user. 
	 * 	 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Sends a request with a user that does not exist.
	 * 	<li> Checks that the request fails with a proper message.
	 * </ul>
	 */
	public void testdetailsRequestFailsIfUserDoesNotExist() throws Exception {
		detailsRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).user("nonExistentUser")
		.version(Constants.FIRST_COMPATIBLE_CLIENT_VERSION).buildDetailsRequest();
		assertTextPresent(getResponse(detailsRequest), "Not authorized");
	}
	
	/**
	 * Tests that it is not possible to login with a null user. 
	 * 
	 * <p> Steps:
	 * <ul> 
	 * 	<li> Sends a request with a null user.
	 * 	<li> Checks that the request does not go through and shows a proper message.
	 * </ul>
	 */
	public void testdetailsRequestFailsWithNullUser() throws Exception {
		detailsRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).user(null)
			.version(Constants.FIRST_COMPATIBLE_CLIENT_VERSION).buildDetailsRequest();
		assertTextPresent(getResponse(detailsRequest), "Not authorized");
	}
	
	/**
	 * Tests that it is not possible to login with a null password. 
	 * 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Sends a request with null password.
	 * 	<li> Checks that the request fails and shows a proper message.
	 * </ul> 
	 */	
	public void testdetailsRequestFailsWithNullPassword() throws Exception {
		detailsRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).password(null)
			.version(Constants.FIRST_COMPATIBLE_CLIENT_VERSION).buildDetailsRequest();
		assertTextPresent(getResponse(detailsRequest), "Not authorized");
	}

	/**
	 * Tests that it is not possible to login with both a null user and a null password.
	 * 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Sends a request with both null user and null password.
	 * 	<li> Checks that the request fails and shows a proper message.
	 * </ul>
	 */
	public void testdetailsRequestFailsWithNullPasswordAndNullUser() throws Exception {
		detailsRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).password(null).user(null)
			.version(Constants.FIRST_COMPATIBLE_CLIENT_VERSION).buildDetailsRequest();
		assertTextPresent(getResponse(detailsRequest), "Not authorized");
	}
	
	/**
	 * Tests that it is not possible to login with a null company 
	 *
	 * <p> Steps:
	 * <ul>
	 * 	<li> Sends a request with a null company.
	 * 	<li> Checks that the request fails and shows a proper message.
	 * </ul>
	 */
	public void testdetailsRequestFailsWithNullCompany() throws Exception {
		detailsRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).company(null)
			.version(Constants.FIRST_COMPATIBLE_CLIENT_VERSION).buildDetailsRequest();
		assertTextPresent(getResponse(detailsRequest), "Unknown company");
	}
}

