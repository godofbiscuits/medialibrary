package com.medallia.mobile.dashboard;

/** A point in the graph. Contains an x and a y coordinate. */
class GraphPoint {
	double x;
	double y;

	/** Create a new graph points for the given x and y coordinate. */
	GraphPoint(double x, double y) {
		this.x = x;
		this.y = y;
	}
}