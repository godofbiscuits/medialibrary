package com.medallia.mobile.companysettings;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.medallia.mobile.DetailFields;
import com.medallia.mobile.DetailFieldsDefaultBase;
import com.medallia.mobile.FieldWithColor;
import com.medallia.mobile.RequestParameterMap.LogonInfo;
import com.medallia.mobile.filter.UnitFilter.FilterUnitGroup;

public class CompanySettingsMilkyWayEarth extends CompanySettingsDefaultBase {

	public CompanySettingsMilkyWayEarth(LogonInfo req) {
		super(req);
	}

	@Override
	protected String getBaseURI0() {
		return "http://localhost:8080";
	}

	@Override
	protected String getCompany() {
		return "earth";
	}

	@Override
	public Date getStartTime() {
		return getDate(2007, 3, 1);
	}
	
	@Override
	protected Date getTodayInternal() {
		return getDate(2009, 5, 21);
	}
	
	@Override
	public String getUnitTranslation() {
		return "Store";
	}

	@Override
	public boolean usesResolvedAlertState() {
		return true;
	}
	
	@Override public FieldWithColor getMainScoreField() {
		return new FieldWithColor("earth_ov_experience", "Overall Experience");
	}
	
	@Override public List<FieldWithColor> getScoreFields(String widgetRequestAction) {
		return Arrays.asList(getMainScoreField(), new FieldWithColor("earth_ov_service", "Overall Service"));
	}
	
	@Override public List<FieldWithColor> getScorecardFields() {
		return getScoreFields("");
	}
	
    @Override public List<FilterUnitGroup> getFilterUnitGroups() {
    	return Arrays.asList(
    			new FilterUnitGroup("Country/State", "Country"),
    			new FilterUnitGroup("Market", "Market"),
    			new FilterUnitGroup("Hotel")
    			);
    }

	// detailed response
	@Override public DetailFields getDetailFields() {
		return new DetailFieldsDefaultBase() {
			@Override public List<FieldWithColor> getScoreFields() {
				return CompanySettingsMilkyWayEarth.this.getScoreFields(null);
			}
			@Override public String getHardCodedPhoneNr() { return "650-796-9698"; } // mr bennedich
		};
	}
	
}
