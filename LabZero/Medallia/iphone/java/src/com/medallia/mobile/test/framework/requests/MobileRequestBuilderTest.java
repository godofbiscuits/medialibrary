package com.medallia.mobile.test.framework.requests;

import junit.framework.TestCase;

import com.medallia.mobile.test.framework.beans.Id;
import com.medallia.mobile.test.framework.config.ExpressEnvironment;

/**
 * Test Suite for the {@link MobileRequestBuilder}.
 * 
 * @author fernando
 */
public class MobileRequestBuilderTest extends TestCase {

	private MobileRequestBuilder requestBuilder;
	
	private MobileRequest mobileRequest;
	
	@Override
	protected void setUp() throws Exception {
		requestBuilder = MobileRequestBuilder.getBuilder(ExpressEnvironment.TEST);
		super.setUp();
	}
	/**
	 * Tests that a request is properly created.
	 */
	public void testSimpleRequestIsBuilt() throws Exception {
		
		mobileRequest = requestBuilder.buildTestRequest();
		assertEquals("testUser", mobileRequest.getUser());
		assertEquals("testPassword", mobileRequest.getPassword());
		assertEquals("testCompany", mobileRequest.getCompany());
	}
	
	/**
	 * Tests that a request is properly modified.
	 */
	public void testModifiersWork() throws Exception {
		mobileRequest = requestBuilder.user("newUser").password("newPassword")
			.company("newCompany").version(2).surveyId(new Id(2)).buildTestRequest();
		assertEquals("req=test&c=newCompany&u=newUser&p=newPassword&id=2&version=2",
				mobileRequest.getRequest());
	}
	
	/**
	 * Tests that a login request is properly created.
	 */
	public void testLoginRequestWorks() throws Exception {
		mobileRequest = requestBuilder.buildLoginRequest();
		assertTrue(mobileRequest.getRequest().contains("req=login"));
	}

	/**
	 * Tests that a details request is properly created.
	 */
	public void testDetailsDetailsWorks() throws Exception {
		mobileRequest = requestBuilder.buildDetailsRequest();
		assertTrue(mobileRequest.getRequest().contains("req=details"));
	}

	/**
	 * Tests that a Scores request is properly created.
	 */
	public void testLoginScoresWorks() throws Exception {
		mobileRequest = requestBuilder.buildScoresRequest();
		assertTrue(mobileRequest.getRequest().contains("req=scores"));
	}

	/**
	 * Tests that a responses request is properly created.
	 */
	public void testLoginResponsesWorks() throws Exception {
		mobileRequest = requestBuilder.buildResponsesRequest();
		assertTrue(mobileRequest.getRequest().contains("req=responses"));
	}
}
