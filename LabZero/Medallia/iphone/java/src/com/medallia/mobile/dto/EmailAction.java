package com.medallia.mobile.dto;
import java.util.*;
import com.medallia.mobile.*;
import com.medallia.mobile.dto.*;
import com.medallia.mobile.dto.builders.*;
import com.medallia.mobile.color.UIColor;

// These files are generated automatically from declarations in DataFormat.h; see convert.pl for details.
//
// Do *not* modify this file directly!


/**
 *  Information about an email template to be edited & sent by the user. Typically triggered by a button in the action menu
 */
@SuppressWarnings("unused")
public interface EmailAction extends Plist.Plistable {

	/**
	 *  If set, the to-field will be filled out with this
	 */
	String to();

	/**
	 *  Email subject
	 */
	@Required String subject();

	/**
	 *  Email body
	 */
	@Required String body();

	/**
	 *  If set, the body will be interpreted as HTML. (boolean - null means no, anything else is yes)
	 */
	String html();


}
