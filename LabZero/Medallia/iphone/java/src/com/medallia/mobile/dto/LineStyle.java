package com.medallia.mobile.dto;
import java.util.*;
import com.medallia.mobile.*;
import com.medallia.mobile.dto.*;
import com.medallia.mobile.dto.builders.*;
import com.medallia.mobile.color.UIColor;

// These files are generated automatically from declarations in DataFormat.h; see convert.pl for details.
//
// Do *not* modify this file directly!


/**
 *  Line style.
 */
@SuppressWarnings("unused")
public interface LineStyle extends Plist.Plistable {

	/**
	 *  Missing if line is not featured.
	 */
	String featured();

	/**
	 *  Text describing this line that may be shown in a legend.
	 */
	@Required String legendText();

	/**
	 *  Color of line.
	 */
	@Required UIColor color();


}
