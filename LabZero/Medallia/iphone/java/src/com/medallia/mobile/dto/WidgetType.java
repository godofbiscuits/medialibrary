package com.medallia.mobile.dto;
import java.util.*;
import com.medallia.mobile.*;
import com.medallia.mobile.dto.*;
import com.medallia.mobile.dto.builders.*;
import com.medallia.mobile.color.UIColor;

// These files are generated automatically from declarations in DataFormat.h; see convert.pl for details.
//
// Do *not* modify this file directly!


/**
 *  a WidgetType represents a single type of dashboard the user can install, e.g. Scoreboard, Response Investigator, Ranker
 */
@SuppressWarnings("unused")
public interface WidgetType extends Plist.Plistable {

	/**
	 *  "Scoreboard"
	 */
	@Required String name();

	/**
	 *  "Overview of foo, feh and fiddle"
	 */
	String desc();

	/**
	 *  "score"
	 */
	String key();

	/**
	 *  "ScoreWidget" (class name)
	 */
	@Required String viewClass();

	/**
	 *  base URL for fetching data
	 */
	@Required String urlBase();

	/**
	 *  key for the responses filter button to view when clicking through to see underlying responses
	 */
	String responsesFilterButtonKey();

	/**
	 *  List of sections, each containing one or more edit controls
	 */
	@Required List<ConfigSection> configSections();

	/**
	 *  Optional list of arguments for the widget type (parameter key and default value). Used for parameters that are hardcoded to the WidgetType and not part of the Config (i.e. a ConfigPartType), for example ranker scope.
	 */
	Map<String,String> arguments();


}
