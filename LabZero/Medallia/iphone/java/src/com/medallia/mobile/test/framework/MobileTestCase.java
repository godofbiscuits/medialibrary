package com.medallia.mobile.test.framework;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import junit.framework.TestCase;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.WebClient;
import com.medallia.mobile.Common;
import com.medallia.mobile.dto.ConfigFile;
import com.medallia.mobile.dto.RankerData;
import com.medallia.mobile.dto.ResponsesLump;
import com.medallia.mobile.dto.SWPage;
import com.medallia.mobile.dto.Scorecard;
import com.medallia.mobile.dto.SingleResponse;
import com.medallia.mobile.test.framework.config.Environment;
import com.medallia.mobile.test.framework.config.ExpressEnvironment;
import com.medallia.mobile.test.framework.querier.Querier;
import com.medallia.mobile.test.framework.querier.impl.ExpressQuerier;
import com.medallia.mobile.test.framework.requests.MobileRequest;
import com.medallia.mobile.test.framework.requests.MobileRequestBuilder;
import com.medallia.mobile.test.framework.util.PListDecoder;
import com.medallia.mobile.test.framework.util.PListDecoderReflectionImplementation;
import common.web.HttpBasicAuthentication;

/**
 * Extends JUnit Testcase, adding several handy methods that can be used for the
 * Mobile Tests.
 * 
 * <p>
 * All Mobile Tests should extend from this class.
 * 
 * <p>
 * When using a Mock Express server, the framework keeps track of all the queries that a test case adds, and
 * at the end, it clears those queries from the ApiMockServlet.
 * 
 * @author fernando
 */
public class MobileTestCase extends TestCase {

	private static String BASIC_AUTH_USER = "medallia"; 
	private static String BASIC_AUTH_PASSWORD = "Kb9mMKJ4u7"; 
	
	private PListDecoder decoder;
	private Querier querier;
	private Environment environment;
	private WebClient webClient;
	private MobileRequest testRequest;
	private Map<String, Set<String>> queries = new HashMap<String, Set<String>>();

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		webClient = new WebClient();
		queries.put(this.getName(), new HashSet<String>());
		setEnvironment();
		assertMobileServletIsHealthy();
		assertExpressServerIsHealthy();
		querier = ExpressQuerier.createExpressQuerier(environment.express());
		decoder = new PListDecoderReflectionImplementation();
		
		if (environment.express().equals(ExpressEnvironment.MOCK)) {
			addMockResponse("SELECT unitid, unitid.name, count(*) FROM survey",
			"Hg,Hg,4.0");
		}
	}

	@Override
	protected void tearDown() throws Exception {
		clearMockQueries();
		super.tearDown();
	}

	/**
	 * Gets the response of the Mobile Servlet.
	 * 
	 * @param mobileRequest the request sent to the mobile servlet.
	 */
	protected String getResponse(MobileRequest mobileRequest) {
		try {
			return getResponse(environment.mobile().getRequestHeader()
					+ mobileRequest.getRequest());
		} catch (IOException e) {
			throw new RuntimeException("Error sending the request", e);
		}
	}
	
	/**
	 * Gets the response and decodes it into a {@link ConfigFile}.
	 */
	protected ConfigFile getLoginResponse(MobileRequest mobileRequest) {
		return decoder.loginPage(new StringReader(getResponse(mobileRequest)));
	}
	
	/**
	 * Gets the response and decodes it into a {@link ResponseLump}.
	 */
	protected ResponsesLump getResponsesResponse(MobileRequest mobileRequest) {
		return decoder.responsesPage(new StringReader(getResponse(mobileRequest)));
	}
	
	/**
	 * Gets the response and decodes it into a {@link SingleResponse}.
	 */
	protected SingleResponse getDetailedResponseResponse(MobileRequest mobileRequest) {
		return decoder.singleResponsePage(new StringReader(getResponse(mobileRequest)));
	}
	
	/**
	 * Gets the response and decodes it into a {@link SingleResponse}.
	 */
	protected SingleResponse getAlertNoteResponse(MobileRequest mobileRequest) {
		return decoder.singleResponsePage(new StringReader(getResponse(mobileRequest)));
	}
	
	/**
	 * Gets the response and decodes it into a {@link Scorecard}.
	 */
	protected Scorecard getScorecardResponse(MobileRequest mobileRequest) {
		return decoder.scorecardPage(new StringReader(getResponse(mobileRequest)));
	}
	
	/**
	 * Gets the response and decodes it into a {@link RankerData}.
	 */
	protected RankerData getRankerDataRepsonse(MobileRequest mobileRequest) {
		return decoder.rankerPage(new BufferedReader(new StringReader(getResponse(mobileRequest))));
	}
	
	/**
	 * Gets the response and decodes it into a {@link SWPage}.
	 */
	protected SWPage getScoresResponse(MobileRequest mobileRequest) {
		return decoder.scoresPage(new StringReader(getResponse(mobileRequest)));
	}

	/**
	 * Returns the {@link ExpressEnvironment}.
	 */
	protected ExpressEnvironment getExpressEnvironment() {
		return environment.express();
	}

	/**
	 * Asserts that all the texts in values are present in the response.
	 */
	protected void assertTextNotPresent(String response, String... values) {
		for (String value : values) {
			if (response.contains(value)) {
				fail("Value " + value + "was not found in " + response);
			}
		}
	}

	/**
	 * Asserts that all the texts in values are not present in the response.
	 */
	protected void assertTextPresent(String response, String... values) {
		for (String value : values) {
			if (!response.contains(value)) {
				fail("Value " + value + "not found in " + response);
			}
		}
	}

	/**
	 *	Adds the specific query to the mock server. The response is based on the input file.
	 *
	 * <p> Example:
	 *   addMockResponse("SELECT XYZ", new File("example.txt")).
	 *   
	 *   If "example.txt" contains: 
	 *   	name1,name2,name3
	 *      a,b,c
	 *      d,e,f
	 *      
	 * Whenever the Mock gets the request "SELECT XYZ", it will respond with the json string:
	 * 	{"query":{"resultCode":0,"table":[["name1","name2","name3"],["a","b","c"],["d","e","f"]]}}
	 * 
	 * @param query the query to insert.
	 * @param file a file that contains the response to the query.
	 */
	protected void addMockResponse(String query, File file)
			throws FileNotFoundException {
		addMockResponse(query, new InputStreamReader(new FileInputStream(file)));
	}

	/**
	 *	Adds the specific query to the mock server. The response is based on a generic file reader.
	 * 
	 * <p> Example:
	 *   addMockResponse("SELECT XYZ", "name1,name2,name3","a,b,c","d,e,f").
	 *      
	 * Whenever the Mock gets the request "SELECT XYZ", it will respond with the json string:
	 * 	{"query":{"resultCode":0,"table":[["name1","name2","name3"],["a","b","c"],["d","e","f"]]}}
	 * 
	 * @param query the query to insert.
	 * @param fileReader the {@link InputStreamReader} that contains the response.
	 */
	protected void addMockResponse(String query, InputStreamReader fileReader) {
		try {
			assertExpressIsMocked();
			queries.get(this.getName()).add(query);
			StringBuffer buffer = new StringBuffer(getExpressRequestHeader(query, "mockkey"));
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			int rowCount = 1;
			String line = null;
			while ((line = bufferedReader.readLine()) != null) {
				buffer.append(String.format("&mockresultrow%s=%s", rowCount,
						Common.urlencode(line)));
				rowCount++;
			}
			buffer.append("&mockresultcount=" + (rowCount - 1));
			assertRequestHadNoError(getResponse(buffer.toString()));
		} catch (IOException e) {
			throw new RuntimeException("IO Error while reading the file", e);
		}
	}

	/**
	 * Adds the specific query to the mock server. Each row input corresponds to a row 
	 * in the response table.
	 * 
	 * @param query the query to insert.
	 * @param rows the rows that corresponds to the response.
	 */
	protected void addMockResponse(String query, String... rows) {
		assertExpressIsMocked();
		queries.get(this.getName()).add(query);
		int rowCount = 1;
		StringBuffer buffer = new StringBuffer(getExpressRequestHeader(query,
				"mockkey"));
		for (String row : rows) {
			buffer.append(String.format("&mockresultrow%s=%s", rowCount,
					Common.urlencode(row)));
			rowCount++;
		}
		buffer.append("&mockresultcount=" + (rowCount - 1));
		try {
			assertRequestHadNoError(getResponse(buffer.toString()));
		} catch (IOException e) {
			throw new RuntimeException("Error sending the request", e);
		}
	}

	/**
	 * Clears the mock queries if the tests are running against a mock express server.
	 */
	private void clearMockQueries() {
		if (this.getClass().getAnnotation(Environment.class).equals(
				ExpressEnvironment.MOCK)) {
			Set<String> testQueries = queries.get(this.getName());
			for (String query : testQueries) {
				clearMockQuery(query);
			}
		}
	}

	/**
	 * Clears a specific mock query.
	 */
	private void clearMockQuery(String query) {
		try {
			String request = getExpressRequestHeader(query, "mockremove"); 
			String response = getResponse(request);
			if (!response.contains("ApiMockServlet has removed key")) {
				fail("ApiMockServlet couldn remove the key, received: " + response);
			}
		} catch (IOException e) {
			throw new RuntimeException("Error sending the request", e);
		}
		
	}

	/**
	 * Sends the request and returns the response.
	 * @throws IOException 
	 */
	protected String getResponse(String request) throws IOException {
		Page response = null;
		try {
			HttpBasicAuthentication auth = HttpBasicAuthentication.fromUserPass(BASIC_AUTH_USER, BASIC_AUTH_PASSWORD);
			webClient.addRequestHeader("Authorization", auth.getAuthString());
			response = webClient.getPage(request);
		} catch (FailingHttpStatusCodeException e) {
			throw new RuntimeException(
					"The server responded wuth a Failing HTTP Status Code: ", e);
		} catch (MalformedURLException e) {
			throw new RuntimeException("The url is not vaild: ", e);
		}
		return response.getWebResponse().getContentAsString();
	}

	/**
	 * Sets the current environment, based on the annotation {@link Environment}
	 * .
	 */
	private void setEnvironment() {
		environment = this.getClass().getAnnotation(Environment.class);
		if (environment == null) {
			throw new RuntimeException(String.format(
					"The environment is not specified in the class %s, "
							+ "please set it with the Environment annotation",
					this.getClass()));
		}
		if (environment.express() == null) {
			throw new RuntimeException(String.format(
					"Set the express environment in the Environment "
							+ "annotation in class %s", this.getClass()));
		}
		if (environment.mobile() == null) {
			throw new RuntimeException(String.format(
					"Set the mobile environment in the Environment "
							+ "annotation in class %s", this.getClass()));
		}
	}

	/**
	 * Verifies that there was no error on the response from the Mock Servlet.
	 */
	private void assertRequestHadNoError(String response) {
		if (!response.contains("ApiMockServlet key/value has been added")) {
			fail("Request couldn't be set in the Mock ExpressServer, received: "
					+ response);
		}
	}

	/**
	 * Returns the header for querying Express Server.
	 */
	private String getExpressRequestHeader(String query, String operation) {
		String encodedQuery = Common.urlencode(query);
		String request = String.format(
				"%s/api?version=1&output=json&%s=%s",
				environment.express().getUrl(), operation, encodedQuery);
		return request;
	}

	/**
	 * Sends a request and figures it out if the Mobile Servlet is alive and
	 * able to respond.
	 */
	private void assertMobileServletIsHealthy()
			throws FailingHttpStatusCodeException {
		try {
			testRequest = MobileRequestBuilder
					.getBuilder(environment.express()).buildTestRequest();
			webClient.getPage(environment.mobile().getRequestHeader()
					+ testRequest.getRequest());
	} catch (IOException e) {
			throw new RuntimeException(String
					.format("Mobile Servlet is not responding on: %s:%s",
							environment.mobile().getHost(), environment
									.mobile().getPort()));
		}
	}

	/**
	 * Asserts if the current express environment is actually a mock.
	 */
	private void assertExpressIsMocked() {
		if (!environment.express().equals(ExpressEnvironment.MOCK)) {
			throw new RuntimeException("The express server is not mocked, you shouldn't try to add a mock response");
		}
	}
	
	/**
	 * Sends a request and figures it out if the Express Server is alive and
	 * able to respond.
	 */
	private void assertExpressServerIsHealthy() {
		try {
			getResponse(getExpressRequestHeader("INVALIDQUERY", "INVALIDOPERATION"));
		} catch (IOException e) {
			throw new RuntimeException("Express Server is not responding on: " + environment.express().getUrl());
		}
	}
	
	public Querier getQuerier() {
		return querier;
	}
}
