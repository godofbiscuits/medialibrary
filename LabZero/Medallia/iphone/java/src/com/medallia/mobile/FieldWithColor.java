package com.medallia.mobile;

import com.medallia.api.orm.Row;
import com.medallia.api.orm.Field.DoubleField;
import com.medallia.mobile.color.BackgroundColor;
import com.medallia.mobile.color.BackgroundColorRule;
import com.medallia.mobile.companysettings.CompanySettings;
import com.medallia.mobile.filter.UnitFilter.FilterUnitGroup;

/** Field, e.g. "Overall satisfaction" or "NPS". */
public class FieldWithColor extends DoubleField {

	private final boolean shouldHaveBackgroundColor;
	private final BackgroundColorRule backgroundColorRule;
	
	/**
	 * Field to base count on. This is needed for calculated reporting fields since we can't use count() on them
	 * (it will simply return the average). For non-calculated reporting fields, this field will simply be the 
	 * same as {@link #key}.
	 */
	private final String countField;
	
	/**
	 * In some cases, when using this FieldWithColor in a filter, we wish to add an additional filter on a unit group
	 * which is tied to this specific field. For example, for Apple, we have specific questions that only apply to
	 * Red Zone surveys and in that case we wish to apply a filter on the Red Zone unit group. If this field is null,
	 * no unit group filter is applied.
	 */
	public final FilterUnitGroup filterUnitGroup;
	
	/**
	 * Constructor with all parameters.
	 * @param key Field key.
	 * @param name Field name.
	 * @param countField Use {@code key} when Field is not a CRF (calculated reporting field; express key starts with r_).
	 *                   If it's a CRF, use the key for a field whose count of non-null values should be used to determine
	 *                   the count of the CRF.
	 * @param shouldHaveBackgroundColor
	 * @param backgroundColorRule
	 */
	public FieldWithColor(String key, String name, String countField, boolean shouldHaveBackgroundColor, BackgroundColorRule backgroundColorRule, FilterUnitGroup fug) {
		super(key, name);
		this.countField = countField;
		this.shouldHaveBackgroundColor = shouldHaveBackgroundColor;
		this.backgroundColorRule = backgroundColorRule;
		this.filterUnitGroup = fug;
	}

	public FieldWithColor(String key, String name, String countField, boolean shouldHaveBackgroundColor) {
		this(key, name,  countField, shouldHaveBackgroundColor, null, null);
	}
	
	public FieldWithColor(String key, String name) {
		this(key, name, key, true);
	}
	
	public FieldWithColor(String key, String name, boolean shouldHaveBackgroundColor, BackgroundColorRule backgroundColorRule) {
		this(key, name, key, shouldHaveBackgroundColor, backgroundColorRule, null);
	}
	
	public FieldWithColor(String key, String name, boolean shouldHaveBackgroundColor) {
		this(key, name, shouldHaveBackgroundColor, null);
	}

	@Override
	public IntField count() {
		return new IntField("count(" + countField + ")", name);
	}
	
	/** @return the desired BackgroundColor, or null if this field shouldn't have a background color */
	public BackgroundColor chooseBackgroundColor(CompanySettings config, double score) {
		if (!shouldHaveBackgroundColor) return null;
    	if ( backgroundColorRule == null ) {
    		return config.getDefaultBackgroundColor(score);
    	} 
    	return backgroundColorRule.getBackgroundColor(score);
    }
	
	/**
	 * Retrieves the value of this field from the given Row, and formats it as a string.
	 * If there is no value, a blank string is returned.
	 * If the value is an integer, the returned string has no decimals.
	 * Otherwise, the String.valueOf(Double) method is used.
	 * @return the value of this field from the given Row formatted as a string
	 */
	public String getStringValue(Row row) {
		String str = row.getValueRaw(this);
		if (str == null) return "";
		if (str.endsWith(".0")) return str.substring(0, str.length() - 2);
		return str;
	}
	
}
