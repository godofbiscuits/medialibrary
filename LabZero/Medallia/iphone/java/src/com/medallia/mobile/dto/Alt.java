package com.medallia.mobile.dto;
import java.util.*;
import com.medallia.mobile.*;
import com.medallia.mobile.dto.*;
import com.medallia.mobile.dto.builders.*;
import com.medallia.mobile.color.UIColor;

// These files are generated automatically from declarations in DataFormat.h; see convert.pl for details.
//
// Do *not* modify this file directly!


/**
 *  A single alternative
 */
@SuppressWarnings("unused")
public interface Alt extends Plist.Plistable {

	/**
	 *  "42" - must be unique within an AltTable
	 */
	@Required String key();

	/**
	 *  "Quarter to Date" - full text, to be displayed in the table when making selection
	 */
	@Required String value();

	/**
	 *  Indentation level. 0 is left-aligned, 1 is slightly indented, 2 is more indented, etc.
	 */
	double level();

	/**
	 *  "QTD" - an alternative text to be displayed in report header
	 */
	String headerValue();

	/**
	 *  Optional link to new alternative table. Used if this alternative links to a new screen of alternatives.
	 */
	AltTable table();


}
