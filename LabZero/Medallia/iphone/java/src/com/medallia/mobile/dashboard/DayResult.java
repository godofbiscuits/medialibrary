package com.medallia.mobile.dashboard;

import java.util.ArrayList;
import java.util.List;

/** TODO add doc */
class DayResult {
	/** doc -- days from TODAY */
	private int doc;
	int day;
	List<FieldResult> fieldResults;
	
	DayResult(int day) {
		this.day = day;
		fieldResults = new ArrayList<FieldResult>();
	}

	@Override
	public String toString() {
		return String.format("%d %s", day, fieldResults.toString());
	}
}