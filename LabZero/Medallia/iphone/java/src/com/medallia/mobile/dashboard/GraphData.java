package com.medallia.mobile.dashboard;

import java.util.List;

import tiny.Empty;

import com.medallia.mobile.dto.Graph;

/**
 * Class contains data for all the graphs for a given scoreboard (i.e. graphs for all benchmark periods, for all time periods,
 * for all fields and for all graph sizes).
 */
class GraphData {
	/** List of [list of [list of [list of graphs for each time period] for each field] for each graph size] for each benchmark graph. */
	private List<List<List<List<Graph>>>> graphScoreData = Empty.list();
	
	/** List of titles for each benchmark period. */
	private List<String> titles = Empty.list();
	
	void addData(List<List<List<Graph>>> data, String title) {
		graphScoreData.add(data);
		titles.add(title);
	}
	
	/**
	 * @return Graph score data with the data for all the benchmark lines merged into single graphs.
	 * The return type is a list of [list of [list of graphs for each time period] for each field] for each graph size.
	 */
	List<List<List<Graph>>> getMergedGraphScoreData() {
		List<List<List<Graph>>> mergedData = graphScoreData.get(0);
		for (int i = 1; i < graphScoreData.size(); ++i) {
			for (int j = 1; j < graphScoreData.get(i).size(); ++j) { // temp
				for (int k = 0; k < graphScoreData.get(i).get(j).size(); ++k) {
					for (int m = 0; m < graphScoreData.get(i).get(j).get(k).size(); ++m) {
						mergedData.get(j).get(k).get(m).lineData().add(graphScoreData.get(i).get(j).get(k).get(m).lineData().get(0));
					}
				}
			}
		}
		return mergedData;
	}
	
	/** @return List of titles for each benchmark period. */
	List<String> getTitles() {
		return titles;
	}
}