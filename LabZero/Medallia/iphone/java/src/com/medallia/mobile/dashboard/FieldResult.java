package com.medallia.mobile.dashboard;

import com.medallia.mobile.FieldWithColor;

class FieldResult {
	final FieldWithColor field;
	final int count;
	final double avg;

	FieldResult(FieldWithColor field, int count, double avg) {
		this.field = field;
		this.count = count;
		this.avg = avg;
	}

	@Override
	public String toString() {
		return String.format("%s: %d, %f", field.name, count, avg);
	}
}