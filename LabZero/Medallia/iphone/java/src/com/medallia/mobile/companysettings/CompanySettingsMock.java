package com.medallia.mobile.companysettings;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.medallia.mobile.DetailFields;
import com.medallia.mobile.DetailFieldsDefaultBase;
import com.medallia.mobile.FieldWithColor;
import com.medallia.mobile.RequestParameterMap.LogonInfo;
import com.medallia.mobile.test.framework.config.ExpressEnvironment;

public class CompanySettingsMock extends CompanySettingsDefaultBase {

	public CompanySettingsMock(LogonInfo req) {
		super(req);
	}

	@Override
	protected String getBaseURI0() {
		//Port is fixed in ApiMockServlet.java in express repository.
		return ExpressEnvironment.MOCK.getUrl();
	}

	@Override
	protected String getCompany() {
		return "mock";
	}

	@Override
	public DetailFields getDetailFields() {
		return new DetailFieldsDefaultBase() {
			@Override public List<FieldWithColor> getScoreFields() {
				return CompanySettingsMock.this.getScoreFields(null);
			}
		};
	}

	@Override 
	public FieldWithColor getMainScoreField() {
		return new FieldWithColor("ov_experience", "Overall Experience");
	}

	@Override 
	public List<FieldWithColor> getScoreFields(String widgetRequestAction) {
		return Arrays.asList(getMainScoreField());
	}

	@Override public List<FieldWithColor> getScorecardFields() {
		return getScoreFields("");
	}
	
	@Override
	public Date getStartTime() {
		return null;
	}

	@Override
	public String getUnitTranslation() {
		return null;
	}

}
