package com.medallia.mobile;
import java.io.PrintWriter;
import java.io.Writer;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.medallia.mobile.color.UIColor;


/** Code to write Apple-style property list xml.
 * For objects implementing Plistable, all "properties" (methods in the first interface)
 * will be dumped as a dictionary. */

public class Plist {
    public interface Plistable { }
    
    private final PrintWriter out;
    private final boolean doIndent;
    private int indent = 1;

    public Plist(Writer w, boolean doIndent) {
        this.out = new PrintWriter(w);
        this.doIndent = doIndent;
    }

    public static void dump(Plistable o, Writer w, boolean doIndent) {
        new Plist(w, doIndent).dumpFile(o);
    }

    private void dumpFile(Plistable o) {
        out.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + 
                "<!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">\n" + 
                "<plist version=\"1.0\">");
       
//        if (!doIndent) out.println("\n<!-- if you're reading this, you may want to turn indentation back on -->\n");
        
        dump(o);
        out.println("</plist>");
        out.flush();
    }

    private void dump(Object o) {
        if (o instanceof String || o instanceof Enum || o instanceof ErrorMessage)
            tag("string", o.toString());
        else if (o instanceof Double) 
            tag("real", o.toString());
        else if (o instanceof Map<?,?>) {
            Map<?,?> m = (Map<?,?>)o;
            line("<dict>");
            indent++;
            for (Object k : m.keySet()) {
                tag("key", k.toString());
                dump(m.get(k));
            }
            indent--;
            line("</dict>");
        } else if (o instanceof UIColor) {
            tag("string", String.format("%06x", ((UIColor)o).getRGB()));
        } else if (o instanceof Enum) {
        	tag("string", ((Enum)o).name());
        } else if (o instanceof Plistable) {
            Map<String,Object> map = new HashMap<String, Object>();
            for (Method m : o.getClass().getInterfaces()[0].getMethods()) {
                Object val;
                try {
                    val = m.invoke(o);
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
                if (val != null) map.put(m.getName(), val);
            }
            dump(map);
        } else if (o instanceof List) {
            line("<array>");
            indent++;
            for (Object e : (List<?>)o) dump(e);
            indent--;
            line("</array>");
        } else throw new IllegalArgumentException("Can't write "+o+" as plist"); 
    }

    static {
        assert !Common.escapeXML("é").contains("acute");
    }
    private void tag(String tag, String body) {
        line(String.format("<%s>%s</%s>", tag, Common.escapeXML(body), tag));
    }

    private void line(String text) {
        if (doIndent)
            out.printf("%" + indent*4 + "s%s\n", "", text);
        else
            out.print(text);
    }

}
