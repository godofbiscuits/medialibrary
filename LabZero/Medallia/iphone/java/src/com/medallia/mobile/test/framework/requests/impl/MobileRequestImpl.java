package com.medallia.mobile.test.framework.requests.impl;

import java.util.HashMap;
import java.util.Map;

import com.medallia.mobile.Common;
import com.medallia.mobile.test.framework.requests.MobileRequest;

/**
 * Simple implementation for the {@link MobileRequest} interface.
 * 
 * @author fernando
 */
public class MobileRequestImpl implements MobileRequest {

	private String company;
	private String password;
	private String user;
	private String request;
	private Map<String, String> extraParameters;
	
	public MobileRequestImpl(String user, String password, String company, String request) {
		if (company != null) { this.company = Common.urlencode(company); }
		if (password != null) { this.password = Common.urlencode(password); }
		if (user != null) { this.user = Common.urlencode(user); }
		if (request != null) { this.request = Common.urlencode(request); }
		extraParameters = new HashMap<String, String>();
	}
	
	@Override
	public MobileRequest addParameter(String name, String value) {
		if (value != null) { value = Common.urlencode(value); } 
		extraParameters.put(name, value);
		return this;
	}
	
	@Override
	public String getRequest() {
		StringBuffer buffer = new StringBuffer();
		for(Map.Entry<String, String> pair : extraParameters.entrySet()) {
			buffer.append(String.format("&%s=%s", pair.getKey(), pair.getValue()));
		}
		return String.format("req=%s&c=%s&u=%s&p=%s%s", request, company, user, password, buffer.toString());
	}
	
	@Override
	public String getCompany() {
		return company;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public String getUser() {
		return user;
	}
}
