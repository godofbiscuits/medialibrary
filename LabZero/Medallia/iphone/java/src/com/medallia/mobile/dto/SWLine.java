package com.medallia.mobile.dto;
import java.util.*;
import com.medallia.mobile.*;
import com.medallia.mobile.dto.*;
import com.medallia.mobile.dto.builders.*;
import com.medallia.mobile.color.UIColor;

// These files are generated automatically from declarations in DataFormat.h; see convert.pl for details.
//
// Do *not* modify this file directly!


/**
 *  Represents a single line of the Scoreboard Widget
 */
@SuppressWarnings("unused")
public interface SWLine extends Plist.Plistable {

	/**
	 *  for now, this must be ScoreWidgetNum (the only nib supported at the moment)
	 */
	@Required String tableCellNibName();

	/**
	 *  "Responses" - the text shown in the large font
	 */
	@Required String title();

	/**
	 *  "8.4" - the main number, it's a string so that it can be customized and formatted on the server side
	 */
	@Required String value();

	/**
	 *  For ScoreWidgetNum only, the "+4.7" difference against the benchmark
	 */
	String difference();

	/**
	 *  For ScoreWidgetNum only, the color for the number background; "green"/"yellow"/"red" for now
	 */
	String scoreBackgroundColor();

	/**
	 *  For ScoreWidgetNum only, the color used for the "+4.7" text; white or red in the mockups
	 */
	UIColor diffColor();

	/**
	 *  For ScoreWidgetNum only; small version of graph for each time period. There will be as many of these as time periods.
	 */
	List<Graph> graphSmall();

	/**
	 *  For ScoreWidgetNum only; large version of graph for each time period. There will be as many of these as time periods.
	 */
	List<Graph> graphLarge();


}
