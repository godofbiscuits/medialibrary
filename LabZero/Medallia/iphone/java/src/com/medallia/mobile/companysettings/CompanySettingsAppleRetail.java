package com.medallia.mobile.companysettings;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import tiny.Empty;

import com.medallia.api.datamodel.SelectQueryWithBooleanFilter;
import com.medallia.api.datamodel.Survey;
import com.medallia.api.orm.Field;
import com.medallia.api.orm.Field.StringField;
import com.medallia.mobile.DetailFields;
import com.medallia.mobile.DetailFieldsDefaultBase;
import com.medallia.mobile.FieldAndAction;
import com.medallia.mobile.FieldWithColor;
import com.medallia.mobile.RequestParameterMap;
import com.medallia.mobile.RequestParameterMap.LogonInfo;
import com.medallia.mobile.RequestParameterMap.Param;
import com.medallia.mobile.color.BackgroundColorRule;
import com.medallia.mobile.color.SimpleBackgroundColorRule;
import com.medallia.mobile.dashboard.GraphBenchmarkScope;
import com.medallia.mobile.dto.AltSection;
import com.medallia.mobile.dto.ClickableHeaderElement;
import com.medallia.mobile.dto.FilterButton;
import com.medallia.mobile.dto.WidgetType;
import com.medallia.mobile.dto.builders.Build;
import com.medallia.mobile.filter.DashboardFilter;
import com.medallia.mobile.filter.Filters;
import com.medallia.mobile.filter.UnitFilter;
import com.medallia.mobile.filter.UnitFilter.FilterUnitGroup;

public class CompanySettingsAppleRetail extends CompanySettingsDefaultBase {
	
	private static final StringField EMPLOYEE = new StringField("ar_employee", "Employee");
	private static final StringField NAME_FIELD = new StringField("ar_calc_name", "Name");
	
	// Note: This question's label actually starts with 'q_'
	private static final StringField PHONE_FIELD = new StringField("q_ar_fr_pt_phone", "Phone");

	private static final FieldWithColor RECOMMEND_FIELD = new FieldWithColor("ar_aggregate_rec", "REC");
	private static final StringField FR_QUEUE_TYPE = new StringField("ar_calc_fr_queue_type", "Queue Type");
	
	public CompanySettingsAppleRetail(LogonInfo req) {
		super(req);
	}
	
	@Override protected String getBaseURI0() {
		return "https://expressqa17.medallia.com"; // TODO for testing
//		return "https://apple.medallia.com";
	}
	
	@Override protected String getCompany() { return "appleretail"; }

	@Override public Date getStartTime() { return getDate(2007, 3, 1); }
	
	// uncomment for testing
//	@Override public Date getTodayInternal() { return getDate(2010, 2, 12); }
	
	@Override public int numberOfscoreDecimals() { return 0; }
	
	@Override public int getMinIntRange() { return 0; }
	
	private static final BackgroundColorRule DEFAULT_BACKGROUND_COLOR_RULE = new SimpleBackgroundColorRule(6, 9);
	@Override protected BackgroundColorRule getDefaultBackgroundColorRule() {
		return DEFAULT_BACKGROUND_COLOR_RULE;
	}	
	
	@Override public FieldWithColor getMainScoreField() {
		return RECOMMEND_FIELD;
	}
	
	private static final FilterUnitGroup RED_ZONE = new FilterUnitGroup("Red Zone", "Red Zone");
	private static final FilterUnitGroup FAMILY_ROOM = new FilterUnitGroup("Family Room", "Family Room");
	private static final FilterUnitGroup OUR_PEOPLE = new FilterUnitGroup("Our People", "Our People");
	
	@Override public List<FieldWithColor> getRankerFieldOptions() {
		return Arrays.asList(
				new FieldWithColor("ar_nps_rz", "NPS (RZ)", "ar_rz_recomm", false, null, RED_ZONE),
				new FieldWithColor("ar_nps_gb", "NPS (GB)", "ar_fr_gb_recomm", false, null, FAMILY_ROOM),
				new FieldWithColor("aenp_nps2", "NPS (NPP)", "ar_emp_recommend_2", false, null, OUR_PEOPLE)
		);
	}
	
	@Override public StringField getResponsesSubheaderField() {
		StringField defaultField = super.getResponsesSubheaderField();
		if (defaultField == null) {
			// this is the case for users with access to only one store
			return EMPLOYEE;
		} else {
			return defaultField;
		}
	}
	
	@Override
    public String getUnitTranslation() { return "Store"; }

	// detailed response
	@Override public DetailFields getDetailFields() {
		return new DetailFieldsDefaultBase() {
			@Override
			public StringField getPhoneField() {
				return PHONE_FIELD;
			}
			@Override
			public List<StringField> getNameFields() {
				return Arrays.asList(NAME_FIELD);
			}
			@Override
			public List<Field> getExtraFieldsToLoad() {
				// has to be here because of extractResponsesFilter!
				return Arrays.<Field>asList(FR_QUEUE_TYPE);
			}

			@Override
			public List<Field> getParticipantFields() {
				List<Field> list = new ArrayList<Field>();
				list.add(NAME_FIELD);
				list.add(getPropertyField());
				list.add(Survey.RESPONSEDATE);
				list.add(PHONE_FIELD);
				return list;
			}

			@Override
			public List<FieldAndAction> getActionFields() {
				return Arrays.asList(new FieldAndAction(EMPLOYEE)); //, new FieldAndAction(getPropertyField(), "unit_name")
			}

			@Override
			public List<StringField> getCommentFields() {
				return Arrays.asList(
						new StringField("ar_rz_rec_low", "Detractor: What are the reasons for your score?"),
						new StringField("ar_rz_rec_moderate", "Passive: What would make you more likely to recommend?"),
						new StringField("ar_rz_rec_high", "Promoter: What would you tell someone?"),
						new StringField("ar_fr_pt_anything_else", "Comment"),
						new StringField("ar_fr_pt_details_tell", "Incorrect Session Details"),
						new StringField("ar_emp_low_score_reasons_2", "Detractor: What are the reasons for your score?"),
						new StringField("ar_emp_moderate_score_reasons_2", "Passive: What would make you more likely to recommend?"),
						new StringField("ar_emp_high_score_reasons_2", "Promoter: What would you tell someone?"),
						new StringField("ar_emp_apple_support_you_2", "How can Apple support you better?"),
						new StringField("ar_fr_ws_not_attend_comment", "Comment (Did Not Attend Workshop)")
						);
			}
			@Override public List<FieldWithColor> getScoreFields() {
				return Arrays.asList(
						new FieldWithColor("ar_rz_recomm", "Recommend (NPS RZ)", true),
						new FieldWithColor("ar_rz_ov_sat", "Overall Satisfaction (RZ)", true),
						new FieldWithColor("ar_rz_emp_friendly", "Friendly & Welcoming", true),
						new FieldWithColor("ar_rz_assist_time", "Timely Assistance", true),
						new FieldWithColor("ar_rz_attention", "Personal Attention", true),
						new FieldWithColor("ar_rz_knowledge", "Knowledgeable", true),
						new FieldWithColor("ar_rz_cleartome", "Clear Purchase Process", true),
						new FieldWithColor("ar_rz_efficient_out", "Efficient Checkout", true),
						new FieldWithColor("ar_fr_gb_recomm", "Recommend (NPS GB)", true),
						new FieldWithColor("ar_fr_gb_ov_sat", "Overall Satisfaction (GB)", true),
						new FieldWithColor("ar_fr_gb_attitude", "Genius' Empathy", true),
						new FieldWithColor("ar_fr_gb_technical", "Genius' Technical Expertise", true),
						new FieldWithColor("ar_fr_gb_av_days", "Appointment Availability", true),
						new FieldWithColor("ar_fr_gb_interaction", "Standby Interaction", true),
						new FieldWithColor("ar_fr_gb_wait_time", "Wait Time", true),
						new FieldWithColor("ar_fr_pt_recommend", "Recommend (NPS PT)", true),
						new FieldWithColor("ar_fr_pt_ov_sat", "Overall Satisfaction (PT)", true),
						new FieldWithColor("ar_fr_pt_train_know", "Trainer's Knowledge", true),
						new FieldWithColor("ar_fr_pt_train_info", "Trainer's Teaching Skills", true),
						new FieldWithColor("ar_fr_pt_av_days", "Session Availability", true),
						new FieldWithColor("ar_fr_pt_booklet_usefulness_2", "Session Guide Usefulness", true),
						new FieldWithColor("ar_fr_pt_headphone_usefulness_2", "Headphone Usefulness", true),
						new FieldWithColor("ar_fr_ws_recommend", "Recommend (NPS WS)", true),
						new FieldWithColor("ar_fr_ws_ov_sat", "Overall Satisfaction (WS)", true),
						new FieldWithColor("ar_fr_ws_presenter_know_na", "Presenter's Knowledge", true),
						new FieldWithColor("ar_fr_ws_presenter_ability_na", "Presentation Skills", true),
						new FieldWithColor("ar_fr_ws_material_na", "Material Covered", true),
						new FieldWithColor("ar_fr_ws_try_things_na", "Try Things Out (Hands-On Only)", true),
						new FieldWithColor("ar_fr_ws_av_days_na", "Workshop Availability", true),
						new FieldWithColor("ar_fr_pset_recommend", "Recommend (NPS PS)", true),
						new FieldWithColor("ar_fr_pset_ov_sat", "Overall Satisfaction (PS)", true),
						new FieldWithColor("ar_fr_pset_data_transfer", "Data Transfer Accuracy", true),
						new FieldWithColor("ar_fr_pset_softhard_inst", "Software/Hardware Installation", true),
						new FieldWithColor("ar_fr_pset_courtesy", "Staff Courtesy", true),
						new FieldWithColor("ar_fr_pset_complete_time", "Setup Time", true),
						new FieldWithColor("ar_fr_pset_value_na", "Workshop Value (If Participated)", true)
						);
			}
		};
	}

	private static enum AppleResponsesFilters {
		ALL("All") {
			@Override void applyFilter(SelectQueryWithBooleanFilter query) { }
		},
		RZ("RZ") {
			@Override void applyFilter(SelectQueryWithBooleanFilter query) {
				query.addCondition("group = 'Red Zone'");
			}
		},
		GB("GB") {
			@Override void applyFilter(SelectQueryWithBooleanFilter query) {
				query.addCondition("group = 'Family Room' AND " + FR_QUEUE_TYPE.key + " in ('gb', 'gb_standby')");
			}
		},
		PT("PT") {
			@Override void applyFilter(SelectQueryWithBooleanFilter query) {
				query.addCondition("group = 'Family Room' AND " + FR_QUEUE_TYPE.key + " = 'training'");
			}
		},
		;
		
		private static class FilterButtons {
			private static List<FilterButton> buttons = new ArrayList<FilterButton>();
		}
		
		private AppleResponsesFilters(String title) {
			FilterButtons.buttons.add(Build.filterButton().key(name()).title(title).get());
		}

		private static List<FilterButton> getFilterButtons() { return FilterButtons.buttons; }
		
		abstract void applyFilter(SelectQueryWithBooleanFilter query);
	}
	
	@Override
    public List<FilterButton> getResponsesFilterButtons() {
		return AppleResponsesFilters.getFilterButtons();
	}
	
	@Override
    public void applyResponsesFilter(SelectQueryWithBooleanFilter query, String filter) {
		AppleResponsesFilters.valueOf(filter).applyFilter(query);
	}
	
	@Override
	public String extractResponsesFilter(Survey survey) {
		String queueType = survey.getString(FR_QUEUE_TYPE);
		if (queueType.startsWith("gb")) return AppleResponsesFilters.GB.name();
		if (queueType.startsWith("training")) return AppleResponsesFilters.PT.name();
		return AppleResponsesFilters.RZ.name();
	}

	/** @return Name with leading "ARMnnnn: " removed. */
	@Override
    protected String getFilteredUGName(String originalName) {
		return originalName.replaceFirst("ARM\\d+: ", "");
    }
	
	/**
	 * Apple Retail needs to merge equal names (this feature is specific to them)
	 * @return true
	 */
	@Override public boolean mergeEqualUnits() { return true; }
	
	@Override
	public Map<String, List<GraphBenchmarkScope>> getGraphBenchmarkScopes() {
		return Empty.
			buildMap(UnitFilter.UNIT_PARENT_ID, Collections.singletonList(new GraphBenchmarkScope("Geo/Region", 1))).
			put("Market", Collections.singletonList(new GraphBenchmarkScope("Geo/Region", 1))).
			put("Geo/Region", Arrays.asList(null, new GraphBenchmarkScope("Geo/Region", 0))).
			getMap();
	}
	
    @Override
    public List<FilterUnitGroup> getFilterUnitGroups() {
    	return Arrays.asList(
    			new FilterUnitGroup("Geo/Region", "Region"),
    			new FilterUnitGroup("Market", "Market"),
    			new FilterUnitGroup("Store")
    			);
    }
    
	@Override
	public List<DashboardFilter> getShownRankerFilters(String widgetKey) {
		List<DashboardFilter> filters = super.getShownRankerFilters(widgetKey);
		filters.add(MAIN_PURCHASE_FILTER);
		return filters;
	}
	
    private static final DashboardFilter MAIN_PURCHASE_FILTER = new DashboardFilter() {
		private static final String MAC = "Mac";
		private static final String IPHONE = "iPhone";
		private static final String IPOD = "iPod";
		private static final String OTHER = "Other";
		
		private final Param PARAM = Param.param02;
		
		@Override public String getDefault() { return "all"; }
		@Override public String getId() { return "MainPurchase"; }
		@Override public String getKey() { return PARAM.name(); }
		@Override public String getName() { return "Main Purchase (RZ)"; }
				
		@Override public void addFilter(RequestParameterMap req, SelectQueryWithBooleanFilter query) {
			String value = req.get(PARAM);
			if ( value == null || value.equals("all")) return;

			String operation = "";
			if ( value.equals(MAC) ) operation = "= 'Mac'";
			if ( value.equals(IPHONE) ) operation = "= 'iPhone'";
			if ( value.equals(IPOD) ) operation = "= 'iPod'";
			if ( value.equals(OTHER) ) operation = "= 'Other'";
			
			if (!operation.equals(""))
				query.addCondition("ar_calc_mac " + operation);
		}

		@Override public List<AltSection> getSections() {
			return Filters.altList(
				"all", "All",
				MAC, "Mac",
				IPHONE, "iPhone",
				IPOD, "iPod",
				OTHER, "Other"
				);
		}

		@Override
		public ClickableHeaderElement getHeaderElement() {
			return ClickableHeaderElement.CHCustomerSegment;
		}
	};
	
	@Override
	public List<WidgetType> getScoreWidgetTypes(LogonInfo logonInfo) {
		return Arrays.asList(
				getWidgetType(logonInfo, "RZ"),
				getWidgetType(logonInfo, "GB"),
				getWidgetType(logonInfo, "PT")
				);
	}	

	private WidgetType getWidgetType(LogonInfo logonInfo, String program) {
		return getWidgetType(
				logonInfo, 
				"Scoreboard (" + program + ") settings", 
				"score" + program, 
				"Scoreboard (" + program + ")", 
				"Overview of current " + program + " scores",
				"scores." + program,
				program
				);
	}

	// TODO Undoubtedly, there is a better way to associate the widget request action with the 
	// set of fields and unit groups to display.  Should refactor when time permits.
	@Override
	public List<DashboardFilter> getShownScoreboardFilters(String widgetKey) {
		List<DashboardFilter> filters = super.getShownScoreboardFilters(widgetKey);
		if (widgetKey == null || "scores.RZ".equals(widgetKey))
			filters.add(MAIN_PURCHASE_FILTER);
		return filters;
	}
	
	@Override public FilterUnitGroup getScoreFilterUnitGroup(String widgetRequestAction) {
		if ("scores.RZ".equals(widgetRequestAction))
			return RED_ZONE;
		if ("scores.GB".equals(widgetRequestAction))
			return FAMILY_ROOM;
		if ("scores.PT".equals(widgetRequestAction))
			return FAMILY_ROOM;
		return null;
	}
	
	@Override public List<FieldWithColor> getScoreFields(String widgetRequestAction) {
		if ("scores.RZ".equals(widgetRequestAction)) {
			return Arrays.asList(
					new FieldWithColor("ar_rz_recomm_nps", "Recommend (NPS)", "ar_rz_recomm", false),
					new FieldWithColor("ar_rz_ov_sat_nps", "Overall Satisfaction", "ar_rz_ov_sat", false),
					new FieldWithColor("ar_rz_emp_friendly_nps", "Friendly & Welcoming", "ar_rz_emp_friendly", false),
					new FieldWithColor("ar_rz_assist_time_nps", "Timely Assistance", "ar_rz_assist_time", false),
					new FieldWithColor("ar_rz_attention_nps", "Personal Attention", "ar_rz_attention", false),
					new FieldWithColor("ar_rz_knowledge_nps", "Knowledgeable", "ar_rz_knowledge", false),
					new FieldWithColor("ar_rz_cleartome_nps", "Clear Purchase Process", false),
					new FieldWithColor("ar_rz_efficient_out_nps", "Efficient Checkout", false)
					);
		}
		if ("scores.GB".equals(widgetRequestAction)) {
			return Arrays.asList(
					new FieldWithColor("ar_fr_gb_recomm_nps", "Recommend (NPS)", "ar_fr_gb_recomm", false),
					new FieldWithColor("ar_fr_gb_ov_sat_nps", "Overall Satisfaction", "ar_fr_gb_ov_sat", false),
					new FieldWithColor("ar_fr_gb_attitude_nps", "Genius' Empathy", "ar_fr_gb_attitude", false),
					new FieldWithColor("ar_fr_gb_technical_nps", "Genius' Technical Expertise", "ar_fr_gb_technical", false),
					new FieldWithColor("ar_fr_gb_av_days_nps", "Appointment Availability", "ar_fr_gb_av_days", false),
					new FieldWithColor("ar_fr_gb_interaction_nps", "Standby Interaction", "ar_fr_gb_interaction", false),
					new FieldWithColor("ar_fr_gb_wait_time_nps", "Wait Time", "ar_fr_gb_wait_time", false)
					);
		}
		if ("scores.PT".equals(widgetRequestAction)) {
			return Arrays.asList(
					new FieldWithColor("ar_fr_pt_recommend_nps", "Recommend (NPS)", "ar_fr_pt_recommend", false),
					new FieldWithColor("ar_fr_pt_ov_sat_nps", "Overall Satisfaction", "ar_fr_pt_ov_sat", false),
					new FieldWithColor("ar_fr_pt_train_know_nps", "Trainer's Knowledge", "ar_fr_pt_train_know", false),
					new FieldWithColor("ar_fr_pt_train_info_nps", "Trainer's Teaching Skills", "ar_fr_pt_train_info", false),
					new FieldWithColor("ar_fr_pt_av_days_nps", "Session Availability", "ar_fr_pt_av_days", false)
					);
		}
		return new ArrayList<FieldWithColor>();
	}

	@Override public List<FieldWithColor> getScorecardFields() {
		return Arrays.asList(
				new FieldWithColor("ar_rz_recomm_nps", "Recommend (NPS RZ)", "ar_rz_recomm", false),
				new FieldWithColor("ar_rz_ov_sat_nps", "Overall Satisfaction (RZ)", "ar_rz_ov_sat", false),
				new FieldWithColor("ar_rz_emp_friendly_nps", "Friendly & Welcoming", "ar_rz_emp_friendly", false),
				new FieldWithColor("ar_rz_assist_time_nps", "Timely Assistance", "ar_rz_assist_time", false),
				new FieldWithColor("ar_rz_attention_nps", "Personal Attention", "ar_rz_attention", false),
				new FieldWithColor("ar_rz_knowledge_nps", "Knowledgeable", "ar_rz_knowledge", false),

				new FieldWithColor("ar_fr_gb_recomm_nps", "Recommend (NPS GB)", "ar_fr_gb_recomm", false),
				new FieldWithColor("ar_fr_gb_ov_sat_nps", "Overall Satisfaction (GB)", "ar_fr_gb_ov_sat", false),
				new FieldWithColor("ar_fr_gb_attitude_nps", "Genius' Empathy", "ar_fr_gb_attitude", false),
				new FieldWithColor("ar_fr_gb_technical_nps", "Genius' Technical Expertise", "ar_fr_gb_technical", false),

				new FieldWithColor("ar_fr_pt_recommend_nps", "Recommend (NPS PT)", "ar_fr_pt_recommend", false),
				new FieldWithColor("ar_fr_pt_ov_sat_nps", "Overall Satisfaction (PT)", "ar_fr_pt_ov_sat", false),
				new FieldWithColor("ar_fr_pt_train_know_nps", "Trainer's Knowledge", "ar_fr_pt_train_know", false),
				new FieldWithColor("ar_fr_pt_train_info_nps", "Trainer's Teaching Skills", "ar_fr_pt_train_info", false)
		);
	}
	
}
