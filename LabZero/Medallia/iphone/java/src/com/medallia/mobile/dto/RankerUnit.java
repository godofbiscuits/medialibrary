package com.medallia.mobile.dto;
import java.util.*;
import com.medallia.mobile.*;
import com.medallia.mobile.dto.*;
import com.medallia.mobile.dto.builders.*;
import com.medallia.mobile.color.UIColor;

// These files are generated automatically from declarations in DataFormat.h; see convert.pl for details.
//
// Do *not* modify this file directly!


/**
 *  One entry in the list of units/unit groups
 */
@SuppressWarnings("unused")
public interface RankerUnit extends Plist.Plistable {

	/**
	 *  Some persistent identifier for this record
	 */
	@Required String uniqueIdentifier();

	/**
	 *  "Palo Alto"
	 */
	@Required String name();

	/**
	 *  "58.7", this is the number that is actually displayed on the client
	 */
	@Required String score();

	/**
	 *  "+0.2", this is the number that is actually displayed on the client
	 */
	@Required String difference();

	/**
	 *  Numeric version of the score. Used for sorting on the client.
	 */
	@Required double scoreNumeric();

	/**
	 *  Numeric version of the difference. Used for sorting on the client.
	 */
	@Required double differenceNumeric();

	/**
	 *  The color used for the "+0.2" text
	 */
	@Required UIColor diffColor();

	/**
	 *  missing if unit is not selected
	 */
	String selected();


}
