package com.medallia.mobile.test.framework.beans;

/**
 * Represents an Id.
 * 
 * <p> Used since I don't like returning Strings or longs in the methods. Return this type is much more
 * explicit and easens the understanding of the code. Ideally, it should also be specified the class type of 
 * the Id, something like Id\<Survey\>.
 * 
 * @author fernando
 *
 * @param <E> The class of the Id.
 */
public class Id<E> {

	private long id;
	
	/**
	 * Creates an Id based on a long.
	 */
	public Id(long id) {
		this.id = id;
	}
	
	/**
	 * Creates an Id based on a String. 
	 */
	public Id(String id) {
		this.id = Long.parseLong(id);
	}
	
	@Override
	public String toString() {
		return Long.toString(id);
	}
}
