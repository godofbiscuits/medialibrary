package com.medallia.mobile;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.medallia.mobile.Plist.Plistable;
import com.medallia.mobile.RequestParameterMap.LogonInfo;
import com.medallia.mobile.RequestParameterMap.Param;
import com.medallia.mobile.companysettings.CompanySettings;
import com.medallia.mobile.companysettings.CompanySettingsProductFeedback;
import com.medallia.mobile.companysettings.WidgetTypesWithDefaults;
import com.medallia.mobile.dashboard.ScoreDisplay;
import com.medallia.mobile.dto.ClickableHeaderElement;
import com.medallia.mobile.dto.ConfigFile;
import com.medallia.mobile.dto.ConfigPart;
import com.medallia.mobile.dto.ConfigPartType;
import com.medallia.mobile.dto.ConfigSection;
import com.medallia.mobile.dto.WidgetInstance;
import com.medallia.mobile.dto.WidgetType;
import com.medallia.mobile.dto.builders.Build;
import com.medallia.mobile.dto.builders.Build.Constants;
import com.medallia.mobile.filter.DashboardFilter;
import com.medallia.mobile.filter.Filters;
import com.medallia.mobile.ranker.RankerScreen;
import com.medallia.mobile.responses.AlertNoteAction;
import com.medallia.mobile.responses.DetailedResponseScreen;
import com.medallia.mobile.responses.ResponsesScreen;
import com.medallia.mobile.scorecard.ScorecardScreen;

public class MobileHandler {

	private LogonInfo logonInfo;
	
	public MobileHandler(LogonInfo logonInfo) {
		this.logonInfo = logonInfo;
	}
	
	public Plistable handle(RequestParameterMap req) {
		boolean debug = false;
		try {
			CompanySettings config = req.getConfig();
			System.err.println("CompanySettings " + config);
			if (config == null) {
				return new ErrorMessage("Unknown company");
			}
			if (config instanceof CompanySettingsProductFeedback) {
				debug = true;
			}
			return obtainResponse(req, config);
		} catch (QueryException e) {
			e.printStackTrace();
			return new ErrorMessage(e.error);
		} catch (Throwable e) {
			e.printStackTrace();
			if (debug) return new ErrorMessage("Failed: " + e.toString());
		}
		return new ErrorMessage("Failed");
	}

	private Plistable obtainResponse(RequestParameterMap req, CompanySettings config) {
		switch (req.getAction()) {
		case login:
			String v = req.get(Param.version);
			// xxx: whenever *at all* possible, make the new format compatible instead
			if (v == null || Integer.parseInt(v) < Constants.FIRST_COMPATIBLE_CLIENT_VERSION)
				return new ErrorMessage("This version of the application is outdated; please upgrade.");
			return buildConfigFile(req, config);
		case responses:
			return new ResponsesScreen(config, logonInfo, req, false).get();
		case details:
			new AlertNoteAction(config, req).handleRequest();
			return new DetailedResponseScreen(config, req).get();
		case scorecard:
			return new ScorecardScreen(config, logonInfo, req).get();
		case ranker:
			return new RankerScreen(config, req).get();
		case scores:
			return new ScoreDisplay(config, req).get();
		default:
			throw new RuntimeException("Unknown request action, " + req.getAction());
		}
	}
		
	public ConfigFile buildConfigFile(RequestParameterMap req, CompanySettings config) {
		WidgetTypesWithDefaults rankerWidgetTypesWithDefaults = config.getRankerWidgetTypesWithDefaults(logonInfo);
		return Build.configFile()
				.tables(Filters.tables(config))
				.userKey(req.get(Param.c) + "." + req.get(Param.u))
				.scoresWidgetTypes(config.getScoreWidgetTypes(logonInfo))
				.rankerWidgetTypes(rankerWidgetTypesWithDefaults.types)
				.initialScoresWidgets(initialScoreWidgets(config, logonInfo))
				.initialRankerWidgets(rankerWidgetTypesWithDefaults.instances)
				.responsesFilterConfig(responsesConfig(config))
				.initialResponseFilter(initialResponseFilterSettings(config))
				.nrOpenAlerts(AlertNoteAction.getNrOpenAlerts(config))
				.get();
	}
	
	private List<WidgetInstance> initialScoreWidgets(CompanySettings config, LogonInfo logonInfo) {
		List<WidgetInstance> list = new ArrayList<WidgetInstance>();
		for (WidgetType wt : config.getScoreWidgetTypes(logonInfo)) {
			Map<String, String> argumentMap = Filters.createDefaultArgumentMap(config.getScoreboardFilters());
			list.add(Build.widgetInstance().type(wt.key()).arguments(argumentMap).get());
		}		
		return list;
	}
	
	private WidgetInstance initialResponseFilterSettings(CompanySettings config) {
		Map<String, String> argumentMap = Filters.createDefaultArgumentMap(config.getScoreboardFilters());

		argumentMap.put(Param.comments_only.name(), "1");
		argumentMap.put(Constants.PREVIEW_FONT_SIZE, "" + Constants.PREVIEW_FONT_SIZE_DEFAULT);
		argumentMap.put(Constants.PREVIEW_LINE_COUNT, "" + Constants.PREVIEW_LINE_COUNT_DEFAULT);
		
		return Build.widgetInstance()
				.type("ResponsesConfig")
				.arguments(argumentMap)
				.get();
	}
	
	private static Map<String, String> stringStringMap(String... pairs) {
		Map<String,String> s = new HashMap<String, String>();
		for (int i=0; i<pairs.length-1; )
			s.put(pairs[i++], pairs[i++]);
		return s;
	}
	
	/** Widget type (i.e. config) definition for responses */
	private WidgetType responsesConfig(CompanySettings config) {
		ConfigSection formatting = responsesFormattingConfig();
		ConfigSection filter = responsesFilterConfig(config);
		
		return Build.widgetType()
			.name("Settings")
			.urlBase(logonInfo.createURL("responses"))
			.configSections(Arrays.asList(filter, formatting))
			.viewClass("ResponseList").get();
	}

	private ConfigSection responsesFilterConfig(CompanySettings config) {
	    List<ConfigPart> configParts = new ArrayList<ConfigPart>();
		configParts.add(Build.configPart()
				.key(Param.satrange.name())
				.minValue(config.getMinIntRange())
				.maxValue(config.getMaxIntRange())
				.type(ConfigPartType.CERange)
				.headerElement(ClickableHeaderElement.CHNone)
				.get());
		for (DashboardFilter sbf : config.getScoreboardFilters() ) {
			configParts.add(Build.configPart()
					.name(sbf.getName())
					.key(sbf.getKey())
					.type(ConfigPartType.CEDropdown)
					.def(sbf.getDefault())
					.table(sbf.getId())
					.headerElement(sbf.getHeaderElement())
					.get());
		}
		configParts.add(Build.configPart()
				.name("Only with comments")
				.key(Param.comments_only.name())
				.type(ConfigPartType.CECheckbox)
				.headerElement(ClickableHeaderElement.CHNone)
				.get());
		
		return Build.configSection()
				.name("Filter")
				.parts(configParts)
				.get();
    }

	private ConfigSection responsesFormattingConfig() {
	    return Build.configSection()
				.name("Formatting")
				.parts(Arrays.asList(
						Build.configPart()
							.name("Preview")
							.key(Constants.PREVIEW_LINE_COUNT)
							.type(ConfigPartType.CEDropdown)
							.def(""+Constants.PREVIEW_LINE_COUNT_DEFAULT)
							.table(Filters.ALT_PREVIEW_LINES)
							.headerElement(ClickableHeaderElement.CHNone)
							.get(),
						Build.configPart()
							.name("Font size")
							.key(Constants.PREVIEW_FONT_SIZE)
							.type(ConfigPartType.CEDropdown)
							.def(""+Constants.PREVIEW_FONT_SIZE_DEFAULT)
							.table(Filters.ALT_PREVIEW_FONT)
							.headerElement(ClickableHeaderElement.CHNone)
							.get()
						)
					)
				.get();
    }
	
	public static void main(String[] args) {
		Map<String, String> s = stringStringMap(args);
		RequestParameterMap reqMap = new RequestParameterMap(s, "http://foo/");
		Plistable res = new MobileHandler(reqMap.getLogonInfo()).handle(reqMap);
		Plist.dump(res, new OutputStreamWriter(System.err), true);
	}
	
}
