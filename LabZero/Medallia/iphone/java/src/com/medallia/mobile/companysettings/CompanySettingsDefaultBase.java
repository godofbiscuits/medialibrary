package com.medallia.mobile.companysettings;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import tiny.Empty;

import com.medallia.api.datamodel.SelectQueryWithBooleanFilter;
import com.medallia.api.datamodel.Timeperiod;
import com.medallia.api.orm.Field.StringField;
import com.medallia.mobile.ApiUtil;
import com.medallia.mobile.FieldWithColor;
import com.medallia.mobile.RequestParameterMap;
import com.medallia.mobile.TemporaryTimeperiod;
import com.medallia.mobile.RequestParameterMap.LogonInfo;
import com.medallia.mobile.RequestParameterMap.Param;
import com.medallia.mobile.color.BackgroundColorRule;
import com.medallia.mobile.color.SimpleBackgroundColorRule;
import com.medallia.mobile.dashboard.GraphBenchmarkScope;
import com.medallia.mobile.dto.Alt;
import com.medallia.mobile.dto.AltSection;
import com.medallia.mobile.dto.ClickableHeaderElement;
import com.medallia.mobile.dto.ConfigPart;
import com.medallia.mobile.dto.ConfigPartType;
import com.medallia.mobile.dto.ConfigSection;
import com.medallia.mobile.dto.FilterButton;
import com.medallia.mobile.dto.WidgetInstance;
import com.medallia.mobile.dto.WidgetType;
import com.medallia.mobile.dto.builders.Build;
import com.medallia.mobile.filter.DashboardFilter;
import com.medallia.mobile.filter.Filters;
import com.medallia.mobile.filter.TimePeriodFilter;
import com.medallia.mobile.filter.UnitFilter;
import com.medallia.mobile.filter.UnitParam;
import com.medallia.mobile.filter.UnitFilter.FilterUnitGroup;
import common.Pair;

public abstract class CompanySettingsDefaultBase extends CompanySettings {
	
	protected final String user, pass;
	private Timeperiod defaultTimeperiod;
	private String overridingBaseURI;
	
	public CompanySettingsDefaultBase(LogonInfo req) {
		user = req.getUser();
		pass = req.getPassword();
	}

	@Override protected final String getBaseURI() {
		if (overridingBaseURI != null) {
			return overridingBaseURI;
		}
		return getBaseURI0();
	}
	
	public void setOverridingBaseURI(String baseURI) {
		this.overridingBaseURI = baseURI;
	}
	
	protected abstract String getBaseURI0();
	
	@Override protected final String getUser() { return user; }
	@Override protected final String getPass() { return pass; }

	@Override public int getMinIntRange() { return 1; }
	@Override public int getMaxIntRange() { return 10; }
	
	@Override public Pair<Double, Double> getGraphYRange() {
		// by default, always include y = 0
		return Pair.cons(0., null);
	}
	
	private static final BackgroundColorRule DEFAULT_BACKGROUND_COLOR_RULE = new SimpleBackgroundColorRule(4, 7);
	@Override protected BackgroundColorRule getDefaultBackgroundColorRule() {
		return DEFAULT_BACKGROUND_COLOR_RULE;
	}

	@Override public boolean showAlertTypeInResponsesList() { return true; }
	
	protected StringField getPropertyField() { return new StringField("unitid.name", getUnitTranslation()); }
	
	@Override
	public Timeperiod getDefaultTimePeriod() {
		// TODO: optimize to avoid extra queries
		if (defaultTimeperiod == null) {
			List<TemporaryTimeperiod> timeperiods = TemporaryTimeperiod.getTemporaryTimeperiods(this);
			for (Timeperiod t : timeperiods) {
				if (defaultTimeperiod == null || t.priority < defaultTimeperiod.priority) {
					defaultTimeperiod = t;
				}
			}
		}
		return defaultTimeperiod;
	}
	
	@Override public List<FieldWithColor> getRankerFieldOptions() {
		return getScoreFields(null);
	}
	
	@Override
	public StringField getResponsesSubheaderField() {
		if (ApiUtil.getUnitsMerged(this, false).size() > 1) {
			// If the user has access to more than one business unit,
			// we want to show the unit name on the responses screen
			return getPropertyField();
		} else {
			// If the user only has access to one business unit,
			// there is no point in showing the unit name for every survey
			return null;
		}
	}
	
	@Override
	public List<DashboardFilter> getCommonFilters() {
		List<DashboardFilter> filters = new ArrayList<DashboardFilter>();
		
		filters.add(new DashboardFilter() {
			@Override public String getDefault() { return getDefaultTimePeriod().id; }
			@Override public String getId() { return Filters.ALT_TIME_PERIOD; }
			@Override public String getKey() { return Param.tp.name(); }
			@Override public String getName() { return "Time Period"; }
			@Override public void addFilter(RequestParameterMap req, SelectQueryWithBooleanFilter query) {
				// let calling code apply time period filter since it needs to deal with benchmark time period 
			}
			@Override public List<AltSection> getSections() { return TimePeriodFilter.getFilter(CompanySettingsDefaultBase.this); }
			@Override public ClickableHeaderElement getHeaderElement() { return ClickableHeaderElement.CHTimePeriod; }
		});

		return filters;
	}
	
	@Override
	public List<DashboardFilter> getShownScoreboardFilters(String widgetKey) {
		List<DashboardFilter> filters = new ArrayList<DashboardFilter>();
		
		filters.add(new DashboardFilter() {
			@Override public String getDefault() { return "all"; }
			@Override public String getId() { return Filters.ALT_UNIT; }
			@Override public String getKey() { return Param.unitid.name(); }
			@Override public String getName() { return getUnitTranslation(); }
			@Override public void addFilter(RequestParameterMap req, SelectQueryWithBooleanFilter query) {
				UnitFilter.applyUnitFilter(new UnitParam(req), query);
			}
			@Override public List<AltSection> getSections() { return UnitFilter.getFilter(CompanySettingsDefaultBase.this, getFilterUnitGroups()); }
			@Override public ClickableHeaderElement getHeaderElement() { return ClickableHeaderElement.CHUnitScope; }
		});

		filters.addAll(getCommonFilters());

		return filters;
	}
	
	protected DashboardFilter getRankerQuestionFilter() {
		return new DashboardFilter() {
			@Override public String getDefault() { return getRankerFieldOptions().get(0).key; }
			@Override public String getId() { return Filters.ALT_RANKER_QUESTION; }
			@Override public String getKey() { return Param.rankerq.name(); }
			@Override public String getName() { return "Ranker Question"; }
			@Override public void addFilter(RequestParameterMap req, SelectQueryWithBooleanFilter query) {
				// this is not a filter per se -- it specifies which question is selected in the query,
				// but the underlying surveys are the same regardless of which question is selected
			}
			@Override public List<AltSection> getSections() {
				List<Alt> alts = new ArrayList<Alt>();
				for (FieldWithColor rankerField : getRankerFieldOptions())
					alts.add(Build.alt().key(rankerField.key).value(rankerField.name).table(Build.altTable().key("").sections(new ArrayList<AltSection>()).get()).get());
				return Arrays.asList(Build.altSection().alternatives(alts).get());
			}
			@Override public ClickableHeaderElement getHeaderElement() { return ClickableHeaderElement.CHScoreSelector; }
		};
	}
	
	@Override
	public List<DashboardFilter> getShownRankerFilters(String widgetKey) {
		// the default behavior is to treat the filter unit groups returned by getFilterUnitGroups as a hierarchical list
		// override this method if that is not desired!
		boolean showAll = widgetKey == null;
		
		List<DashboardFilter> filters = new ArrayList<DashboardFilter>();
		
		filters.add(getRankerQuestionFilter());

		List<FilterUnitGroup> allFugs = getFilterUnitGroups();
		for (int i = 1; i < allFugs.size(); ++i) {
			if (showAll || ("ranker."+allFugs.get(i).id).equals(widgetKey)) {
				List<FilterUnitGroup> fugs = allFugs.subList(0, i);
				final List<AltSection> sections = UnitFilter.getFilter(CompanySettingsDefaultBase.this, fugs);
				if (UnitFilter.hasMultipleAlternatives(sections)) {
					final String id = "ranker." + allFugs.get(i).id;
					filters.add(new DashboardFilter() {
						@Override public String getDefault() { return "all"; }
						@Override public String getId() { return id; }
						@Override public String getKey() { return Param.unitid.name(); }
						@Override public String getName() { return "Filter"; }
						@Override public void addFilter(RequestParameterMap req, SelectQueryWithBooleanFilter query) {
							// ranker module will apply this filter
						}
						@Override public List<AltSection> getSections() { return sections; }
						@Override public ClickableHeaderElement getHeaderElement() { return ClickableHeaderElement.CHUnitScope; }
					});
				}
			}
		}
		
		filters.addAll(getCommonFilters());
		
		return filters;
	}
	
	@Override
	public Map<String, List<GraphBenchmarkScope>> getGraphBenchmarkScopes() {
		return Empty.hashMap();
	}
	
    @Override
    public List<FilterUnitGroup> getFilterUnitGroups() {
    	return Empty.list();
    }
    
	@Override
	public FilterUnitGroup getScoreFilterUnitGroup(String widgetRequestAction) {
		return null;
	}
	
	@Override
    protected String getFilteredUGName(String originalName) {
    	return originalName;
    }

	@Override
    public List<FilterButton> getResponsesFilterButtons() {
    	return Empty.list();
    }
    
    @Override
    public void applyResponsesFilter(SelectQueryWithBooleanFilter query, String filter) { /* no filters by default */ }
    
    @Override
	public List<WidgetType> getScoreWidgetTypes(LogonInfo logonInfo) {
		return Arrays.asList(getWidgetType(logonInfo, "Scoreboard settings", "score", "Scoreboard", "Overview of current scores", "scores", null));
	}

	@Override
	public WidgetTypesWithDefaults getRankerWidgetTypesWithDefaults(LogonInfo logonInfo) {
		// the default behavior is to create a widget type for each filter unit groups returned by getFilterUnitGroups
		// override this method if that is not desired!
		WidgetTypesWithDefaults types = new WidgetTypesWithDefaults();
		List<FilterUnitGroup> fugs = getFilterUnitGroups();
		Collections.reverse(fugs);
		if (fugs.isEmpty()) {
			// default is to rank units
			fugs = Arrays.asList(new FilterUnitGroup("Unit"));
		}
		for (FilterUnitGroup fug : fugs) {
			WidgetType type = getRankerWidgetType(logonInfo, "Ranker settings", "ranker." + fug.id, "Ranker (" + fug.name + ")", "Ranks on " + fug.name.toLowerCase() + " level", "ranker", fug.getMangledId());
			
			Map<String, String> argumentMap = Filters.createDefaultArgumentMap(getRankerFilters());
			argumentMap.putAll(type.arguments());
			WidgetInstance instance = Build.widgetInstance().type(type.key()).arguments(argumentMap).get();
			
			types.add(type, instance);
		}
		return types;
	}
	
	protected final WidgetType getRankerWidgetType(LogonInfo logonInfo, String configSectionName, String widgetKey, String widgetName, String widgetDescription, String widgetRequestAction, final String scope) {
		List<ConfigPart> parts = new ArrayList<ConfigPart>();
		for (DashboardFilter filter : getShownRankerFilters(widgetKey)) {
			parts.add(Build.configPart()
					.name(filter.getName())
					.key(filter.getKey())
					.type(ConfigPartType.CEDropdown)
					.def(filter.getDefault())
					.table(filter.getId())
					.headerElement(filter.getHeaderElement())
					.get()
					);
		}
		ConfigSection scoreboardSettings = Build.configSection()
				.name(configSectionName)
				.parts(parts)
				.get();
		return Build.widgetType()
				.key(widgetKey)
				.name(widgetName)
				.viewClass("RankerViewController")
				.desc(widgetDescription)
				.urlBase(logonInfo.createURL(widgetRequestAction))
				.configSections(Arrays.asList(scoreboardSettings))
				.arguments(new HashMap<String, String>() {{ put(Param.scope.name(), scope); }})
				.get();
	}

	protected final WidgetType getWidgetType(LogonInfo logonInfo, String configSectionName, String widgetKey, String widgetName, String widgetDescription, String widgetRequestAction, String responsesFilterButtonKey) {
		List<ConfigPart> parts = new ArrayList<ConfigPart>();
		for (DashboardFilter filter : getShownScoreboardFilters(widgetRequestAction)) {
			parts.add(Build.configPart()
					.name(filter.getName())
					.key(filter.getKey())
					.type(ConfigPartType.CEDropdown)
					.def(filter.getDefault())
					.table(filter.getId())
					.headerElement(filter.getHeaderElement())
					.get()
					);
		}
		ConfigSection scoreboardSettings = Build.configSection()
				.name(configSectionName)
				.parts(parts)
				.get();
			
		return Build.widgetType()
				.key(widgetKey)
				.name(widgetName)
				.viewClass("ScoreWidget")
				.desc(widgetDescription)
				.urlBase(logonInfo.createURL(widgetRequestAction))
				.responsesFilterButtonKey(responsesFilterButtonKey)
				.configSections(Arrays.asList(scoreboardSettings))
				.get();
	}
}
