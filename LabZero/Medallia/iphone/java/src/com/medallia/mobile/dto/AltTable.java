package com.medallia.mobile.dto;
import java.util.*;
import com.medallia.mobile.*;
import com.medallia.mobile.dto.*;
import com.medallia.mobile.dto.builders.*;
import com.medallia.mobile.color.UIColor;

// These files are generated automatically from declarations in DataFormat.h; see convert.pl for details.
//
// Do *not* modify this file directly!


/**
 *  One table of alternatives (used by ConfigParts of type CEDropdown)
 */
@SuppressWarnings("unused")
public interface AltTable extends Plist.Plistable {

	/**
	 *  Key (used to reference this from ConfigPart.table())
	 */
	@Required String key();

	/**
	 *  List of sections; if you don't want the alternatives grouped, use a single section with no header.
	 */
	@Required List<AltSection> sections();


}
