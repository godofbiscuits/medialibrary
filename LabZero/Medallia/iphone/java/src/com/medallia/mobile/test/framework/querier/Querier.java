package com.medallia.mobile.test.framework.querier;

import java.util.List;
import java.util.Map;

import com.medallia.api.datamodel.Timeperiod;
import com.medallia.mobile.test.framework.beans.Id;
import com.medallia.mobile.test.framework.beans.LogNote;

/**
 * Defines all the possible commands that can be queried against an
 * Express instance.
 * 
 * @author fernando
 */
public interface Querier {
	
	/**
	 * Gets all the {@link Timeperiod} for a specific company
	 */
	public List<Timeperiod> getTimePeriods();
	
	public int getNumberOfSurveys(Timeperiod timePeriod);
	
	public int getNumberOfSurveys(Timeperiod timePeriod, Map<String,String> filters);
	
	/**
	 * Gets all the Notes associated with a Survey.
	 * 
	 * @param surveyId the Id of the Survey.
	 * @return a List of {@link LogNote}.
	 */
	public List<LogNote> getLogNotes(Id<?> surveyId);

	/**
	 * Creates a Survey for a specific company and unit.
	 * 
	 * @param company the name of the company.
	 * @param unit the name of the unit/
	 * @return the Id of the recently created survey.
	 */
	public Id<?> createDummySurvey(String company, String unit);
	
	/**
	 * Triggers an alert.
	 * 
	 * @param company the company name.
	 * @param alert the alert name.
	 * @param survey the survey Id.
	 */
	public void triggerAlert(String company, String alert, Id<?> survey);
	
	/**
	 * Gets the average value of a field in a specific period.
	 * 
	 * @param field the field to be used for the average.
	 * @param timePeriod the time period to be used.
	 * @return the average of the field in that period.
	 */
	public double getAverageFromSurvey(String field, Id<?> timePeriod);
	
	/**
	 * Gets the average value of a field between a begin and end date..
	 * 
	 * @param field the field to be used for the average.
	 * @param beginDate the begin date.
	 * @param endDate the end date.
	 * @return the average of the field in that period.
	 */
	public double getAverageFromSurvey(String field, String beginDate, String endDate);
}
