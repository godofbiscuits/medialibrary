package com.medallia.mobile.dashboard;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import junit.framework.Assert;

import com.medallia.mobile.Common;
import com.medallia.mobile.dto.GraphLabel;
import com.medallia.mobile.dto.TimePeriod;
import com.medallia.mobile.dto.builders.Build;

/**
 * Graph time period. Used for the graphs on the score display. Most of this enum deals with labeling
 * the graph's time axis. Each enum constant contains the length of the time period, the length of
 * the step size used for labeling and the date format used for each label. The
 * {@link GraphTimePeriod#getAxisLabels()} creates the labels from this information. All labels are
 * guaranteed to be within the time period. For example, for half-monthly step size, and a 2 month
 * time period with today being Feb 24, the labels will be Jan 1, Jan 15, Feb 1, Feb 15.
 * <p/>
 * In addition to the text of the label, each label object also contains a floating point number
 * between 0 and 1 denoting the position of the label relative to the time period. 0 means at the
 * first day of the period, 1 at the last day, 0.5 in the middle, etc.
 */
enum GraphTimePeriod {
	/** One week. One label per day. */
	ONE_WEEK("1w", Calendar.DATE, 7, TimeUnit.DAY, 1, "MMM d"),
	/** One month. One label per week, ending today. */
	ONE_MONTH("1m", Calendar.MONTH, 1, TimeUnit.DAY, 7, "MMM d"),
	/** Three months. One label per half-month, the 1st and 15th of each month. */
	THREE_MONTHS("3m", Calendar.MONTH, 3, TimeUnit.HALF_MONTH, 1, "MMM d"),
	/** Six months. One label per month. Print the full month name, but not the year. */
	SIX_MONTHS("6m", Calendar.MONTH, 6, TimeUnit.MONTH, 1, "MMMM"),
	/** One year. One label per every second month. Include the year in the label to make it easier to read. */
	ONE_YEAR("1y", Calendar.YEAR, 1, TimeUnit.MONTH, 2, "MMM yy"),
	/** Two years. One label per every third month. Include the year in the label to make it easier to read. */
	TWO_YEARS("2y", Calendar.YEAR, 2, TimeUnit.MONTH, 3, "MMM yy"),
	;

	/** Time unit. Used for stepping backwards along a graph axis to create labels. */
	private enum TimeUnit {
		DAY {
			@Override void init(Calendar cal) { }
			@Override void step(Calendar cal, int stepSize) {
				cal.add(Calendar.DATE, -stepSize);
			}
		},
		HALF_MONTH {
			@Override void init(Calendar cal) {
				cal.set(Calendar.DATE, cal.get(Calendar.DATE) >= 15 ? 15 : 1);
			}
			@Override void step(Calendar cal, int stepSize) {
				Assert.assertEquals(1, stepSize);
				if (cal.get(Calendar.DATE) == 15) {
					cal.set(Calendar.DATE, 1);
				} else {
					cal.add(Calendar.MONTH, -1);
					cal.set(Calendar.DATE, 15);
				}
			}
		},
		MONTH {
			@Override void init(Calendar cal) {
				cal.set(Calendar.DATE, 1);
			}
			@Override void step(Calendar cal, int stepSize) {
				cal.add(Calendar.MONTH, -stepSize);
			}
		},
		;
		
		/** Initialize the given calendar. For example, set it to the 1st of the month. */
		abstract void init(Calendar cal);

		/** Take 'stepSize' steps backwards for the given calendar. */ 
		abstract void step(Calendar cal, int stepSize);
	}

	/** Class contains the list of time period titles. */
	static class TimePeriods {
		static List<TimePeriod> timePeriods = new ArrayList<TimePeriod>();
	}
	
	private final int periodUnit;
	private final int periodLength;
	private final TimeUnit stepUnit;
	private final int stepLength;
	private final DateFormat dateFormat;
	
	private GraphTimePeriod(String periodName, int periodUnit, int periodLength, TimeUnit stepUnit, int stepLength, String datePattern) {
		this.periodUnit = periodUnit;
		this.periodLength = periodLength;
		this.stepUnit = stepUnit;
		this.stepLength = stepLength;
		dateFormat = new SimpleDateFormat(datePattern);
		TimePeriods.timePeriods.add(Build.timePeriod().title(periodName).labelsSmallGraph(new ArrayList<GraphLabel>()).labelsLargeGraph(getAxisLabels()).get());
	}

	/** @return List of time period titles. */
	static List<TimePeriod> getTimePeriods() {
		return TimePeriods.timePeriods;
	}

	/**
	 * @return The number of days in this time period, using the given calendar for today's date. Today's date
	 * is needed to correctly tell how many days there are in the period (e.g. days in the past month).
	 * The given calendar will be modified, but reset to its original value before returned.
	 */
	int getPeriodDays(Calendar today) {
		long timeEnd = today.getTimeInMillis();
		today.add(periodUnit, -periodLength);
		int days = (int)((timeEnd - today.getTimeInMillis() + Common.MS_PER_DAY/2) / Common.MS_PER_DAY); // need to round in case of daylight savings :)
		today.add(periodUnit, periodLength);
		return days;
	}

	/** @return Today, with all fields but year, month and date cleared. */
	private Calendar getToday() {
		Calendar c = Calendar.getInstance();
		int y = c.get(Calendar.YEAR), m = c.get(Calendar.MONTH), d = c.get(Calendar.DATE);
		c.clear();
		c.set(y, m, d);
		return c;
	}

	/** @return List of axis labels for this time period. See {@link GraphTimePeriod}. */
	private List<GraphLabel> getAxisLabels() {
		List<GraphLabel> labels = new ArrayList<GraphLabel>();
		
		Calendar current = getToday();
		Calendar start = (Calendar)current.clone();
		start.add(Calendar.DATE, -getPeriodDays(current));
		
		long currentTime = current.getTimeInMillis();
		long startTime = start.getTimeInMillis() + Common.MS_PER_DAY;
		long diff = currentTime - startTime;
		
		stepUnit.init(current);
		while (current.after(start)) {
			currentTime = current.getTimeInMillis();
			double position = (currentTime - startTime) / (double)diff;
			labels.add(Build.graphLabel().label(dateFormat.format(current.getTime())).position(position).get());
			stepUnit.step(current, stepLength);
		}
		Collections.reverse(labels);
		return labels;
	}
}