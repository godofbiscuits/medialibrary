package com.medallia.mobile.test.tests;

import com.medallia.mobile.dto.Scorecard;
import com.medallia.mobile.test.framework.MobileTestCase;
import com.medallia.mobile.test.framework.beans.Id;
import com.medallia.mobile.test.framework.config.Environment;
import com.medallia.mobile.test.framework.config.ExpressEnvironment;
import com.medallia.mobile.test.framework.config.MobileEnvironment;
import com.medallia.mobile.test.framework.requests.MobileRequest;
import com.medallia.mobile.test.framework.requests.MobileRequestBuilder;

/**
 * Scenario Suite that runs several tests that exercises the Scorecard request.
 */
@Environment(express=ExpressEnvironment.EARTH, mobile=MobileEnvironment.LOCALHOST)
public class LocalScorecardRequestTest extends MobileTestCase {

	private MobileRequest scorecardRequest;
	
	/**
	 * Checks that filtering by Time Period and Response Text works.
	 * 
	 * <p> Preconditions:
	 * <ul>
	 * 	<li> There are 3 surveys with text 'help' in the year 09 (time period 180).
	 * </ul>
	 * 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Send a Request with id 180 and with response text 'help'
	 *  <li> Check that the response contains 3 surveys, all of them with the text 'help'.
	 *  <li> Send a Request with an inexistent text.
	 *  <li> Check that the response does not contains any survey.
	 * </ul>
	 */
	public void testFilterByTimePeriodAndResponseTextWorks() throws Exception {
		scorecardRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).timePeriod(new Id(180))
			.responseText("help").scorecardName("test").buildScorecardRequest();
		Scorecard scorecard = getScorecardResponse(scorecardRequest);
		assertEquals(3, scorecard.responsesLump().entries().size());
		assertTextPresent(scorecard.responsesLump().entries().get(0).text().toLowerCase(), "help");
		assertTextPresent(scorecard.responsesLump().entries().get(1).text().toLowerCase(), "help");
		assertTextPresent(scorecard.responsesLump().entries().get(2).text().toLowerCase(), "help");
		
		scorecardRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).timePeriod(new Id(180))
			.responseText("nonExistentComment").scorecardName("test").buildScorecardRequest();
		scorecard = getScorecardResponse(scorecardRequest);
		assertEquals(0, scorecard.responsesLump().entries().size());
	}
	
	/**
	 * Checks that the right score is shown.
	 * 
	 * <p> Preconditions:
	 * <ul>
	 * 	<li> There are 17 surveys with text 'good' in the year 09 (time period 180).
	 *  <li> Earth contains 'Overall Experience' and "Overall Service" as scores.
	 *  <li> The average score of those surveys is 4.94.
	 * </ul>
	 * 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Send a Request with id 180 and with response text 'good'
	 *  <li> Check that the average is 4.9 for overall experience and .
	 *  <li> Send a Request with an inexistent text.
	 *  <li> Check that the average is -.
	 * </ul>
	 */
	public void testScoresShowRightAverage() throws Exception {		
		scorecardRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).timePeriod(new Id(180))
			.responseText("NonExistent").scorecardName("test").buildScorecardRequest();
		Scorecard scorecard = getScorecardResponse(scorecardRequest);
		assertEquals(0, scorecard.scores().size());

		scorecardRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).timePeriod(new Id(180))
			.responseText("good").scorecardName("test").buildScorecardRequest();
		scorecard = getScorecardResponse(scorecardRequest);
		assertEquals(2, scorecard.scores().size());
		assertEquals("Overall Experience", scorecard.scores().get(0).title());
		assertEquals("4.9", scorecard.scores().get(0).value());
		assertEquals("Overall Service", scorecard.scores().get(1).title());
		assertEquals("6.0", scorecard.scores().get(1).value());
	}
	
	/**
	 * Checks that sending a request without time period fails.
	 * 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Send a request without timeperiod.
	 *  <li> Check that the response fails and a message is shown.
	 * </ul>
	 */
	public void testScorecardRequestWithoutTimePeriodFails() throws Exception {
		scorecardRequest = MobileRequestBuilder.getBuilder(
				getExpressEnvironment()).timePeriod(null).responseText("David")
				.scorecardName("test").buildScorecardRequest();
		assertTextPresent(getResponse(scorecardRequest), "Expected a parameter");
	}

	/**
	 * Checks that sending a request without a non existent time period fails.
	 * 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Send a request with a non existent time period
	 *  <li> Check that the response fails and a message is shown.
	 * </ul>
	 */
	public void testScorecardRequestWithWrongTimePeriodFails() throws Exception {
		scorecardRequest = MobileRequestBuilder.getBuilder(
				getExpressEnvironment()).timePeriod(new Id(4324321)).responseText("David")
				.scorecardName("test").buildScorecardRequest();
		assertTextPresent(getResponse(scorecardRequest), "Cannot find timeperiod");	
	}
	
	/**
	 * Checks that sending a request without response text fails.
	 *
	 * <p> Steps:
	 * <ul>
	 * 	<li> Send a request without a response text.
	 *  <li> Check that the response fails and a message is shown.
	 * </ul>  
	 */
	public void testScorecardRequestWithoutResponseTimeFails() throws Exception {
		scorecardRequest = MobileRequestBuilder.getBuilder(
				getExpressEnvironment()).timePeriod(new Id(180))
				.scorecardName("test").buildScorecardRequest();
		assertTextPresent(getResponse(scorecardRequest), "Failed");	
	}
	
	
	/**
	 * Tests that it is not possible to get the request with an unknown company.
	 * 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Sends a request with an unknown company.
	 * 	<li> Checks that the request does not go through and shows a proper message.
	 * </ul>
	 */
	public void testScorecardRequestFailsIfCompanyUnknown() throws Exception {
		scorecardRequest = MobileRequestBuilder.getBuilder(
				getExpressEnvironment()).company("unknownCompany").responseText("David")
				.scorecardName("test").timePeriod(new Id(180)).buildScorecardRequest();
		assertTextPresent(getResponse(scorecardRequest), "Unknown company");
	}

	/**
	 * Tests that it is not possible to get the request with a wrong password.
	 * 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Sends a request with wrong password.
	 * 	<li> Checks that the request fails and shows a proper message.
	 * </ul> 	
	 */
	public void testScorecardRequestFailsIfWrongPassword() throws Exception {
		scorecardRequest = MobileRequestBuilder.getBuilder(
				getExpressEnvironment()).password("wrongPassword").responseText("David")
				.scorecardName("test").timePeriod(new Id(180)).buildScorecardRequest();
		assertTextPresent(getResponse(scorecardRequest), "Not authorized");
	}

	/**
	 * Tests that it is not possible to get the request with a non existent
	 * user.
	 * 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Sends a request with a user that does not exist.
	 * 	<li> Checks that the request fails with a proper message.
	 * </ul>
	 */
	public void testScorecardRequestFailsIfUserDoesNotExist() throws Exception {
		scorecardRequest = MobileRequestBuilder.getBuilder(
				getExpressEnvironment()).user("nonExistent").responseText("David")
				.scorecardName("test").timePeriod(new Id(180)).buildScorecardRequest();
		assertTextPresent(getResponse(scorecardRequest), "Not authorized");
	}

	/**
	 * Tests that it is not possible to get the request with a null user.
	 * 
	 * <p> Steps:
	 * <ul> 
	 * 	<li> Sends a request with a null user.
	 * 	<li> Checks that the request does not go through and shows a proper message.
	 * </ul>
	 */
	public void testScorecardRequestFailsWithNullUser() throws Exception {
		scorecardRequest = MobileRequestBuilder.getBuilder(
				getExpressEnvironment()).user(null).scorecardName("test").responseText("David")
				.timePeriod(new Id(180)).buildScorecardRequest();
		assertTextPresent(getResponse(scorecardRequest), "Not authorized");
	}

	/**
	 * Tests that it is not possible to get the request with a null password.
	 * 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Sends a request with null password.
	 * 	<li> Checks that the request fails and shows a proper message.
	 * </ul> 	
	 */	
	public void testScorecardRequestFailsWithNullPassword() throws Exception {
		scorecardRequest = MobileRequestBuilder.getBuilder(
				getExpressEnvironment()).password(null).scorecardName("test").responseText("David")
				.timePeriod(new Id(180)).buildScorecardRequest();
		assertTextPresent(getResponse(scorecardRequest), "Not authorized");
	}

	/**
	 * Tests that it is not possible to get the request with both a null user
	 * and a null password.
	 * 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Sends a request with both null user and null password.
	 * 	<li> Checks that the request fails and shows a proper message.
	 * </ul>
	 */
	public void testScorecardRequestFailsWithNullPasswordAndNullUser()
			throws Exception {
		scorecardRequest = MobileRequestBuilder.getBuilder(
				getExpressEnvironment()).user(null).password(null).responseText("David")
				.timePeriod(new Id(180)).scorecardName("test").buildScorecardRequest();
		assertTextPresent(getResponse(scorecardRequest), "Not authorized");
	}

	/**
	 * Tests that it is not possible to get the request with a null company
	 * 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Sends a request with a null company.
	 * 	<li> Checks that the request fails and shows a proper message.
	 * </ul>
	 */
	public void testScorecardRequestFailsWithNullCompany() throws Exception {
		scorecardRequest = MobileRequestBuilder.getBuilder(
				getExpressEnvironment()).scorecardName("test").company(null).responseText("David")
				.timePeriod(new Id(180)).buildScorecardRequest();
		assertTextPresent(getResponse(scorecardRequest), "Unknown company");
	}
	
	/**
	 * Tests that it is not possible to get a request with an empty company.
	 *  
	 * <p> Steps:
	 * <ul>
	 * 	<li> Sends a request with an empty company.
	 * 	<li> Checks that the request fails and shows a proper message.
	 * </ul>
	 */
	public void testScorecardRequestFailsWithEmptyCompany() throws Exception {
		scorecardRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).responseText("David")
			.company("").scorecardName("test").timePeriod(new Id(180)).buildScorecardRequest();
		assertTextPresent(getResponse(scorecardRequest), "Unknown company");		
	}

	/**
	 * Tests that it is not possible to get a request with an empty user.
	 *  
	 * <p> Steps:
	 * <ul>
	 * 	<li> Sends a request with an empty user.
	 * 	<li> Checks that the request fails and shows a proper message.
	 * </ul>
	 */
	public void testScorecardRequestFailsWithEmptyUser() throws Exception {
		scorecardRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).responseText("David")
			.user("").scorecardName("test").timePeriod(new Id(180)).buildScorecardRequest();
		assertTextPresent(getResponse(scorecardRequest), "Not authorized");		
	}

	/**
	 * Tests that it is not possible to get a request with an empty password.
	 *  
	 * <p> Steps:
	 * <ul>
	 * 	<li> Sends a request with an empty password.
	 * 	<li> Checks that the request fails and shows a proper message.
	 * </ul>
	 */
	public void testScorecardRequestFailsWithEmptyPassword() throws Exception {
		scorecardRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).responseText("David")
			.password("").scorecardName("test").timePeriod(new Id(180)).buildScorecardRequest();
		assertTextPresent(getResponse(scorecardRequest), "Not authorized");		
	}
}