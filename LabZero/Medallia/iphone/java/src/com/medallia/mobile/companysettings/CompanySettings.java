package com.medallia.mobile.companysettings;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.medallia.api.datamodel.SelectQueryWithBooleanFilter;
import com.medallia.api.datamodel.Survey;
import com.medallia.api.datamodel.Timeperiod;
import com.medallia.api.datamodel.UnitGroup;
import com.medallia.api.orm.ApiConnection;
import com.medallia.api.orm.SelectQuery;
import com.medallia.api.orm.Statement;
import com.medallia.api.orm.Field.StringField;
import com.medallia.mobile.Common;
import com.medallia.mobile.DetailFields;
import com.medallia.mobile.DirectApiConnection;
import com.medallia.mobile.FieldWithColor;
import com.medallia.mobile.RequestParameterMap.LogonInfo;
import com.medallia.mobile.color.BackgroundColor;
import com.medallia.mobile.color.BackgroundColorRule;
import com.medallia.mobile.dashboard.GraphBenchmarkScope;
import com.medallia.mobile.dto.FilterButton;
import com.medallia.mobile.dto.WidgetType;
import com.medallia.mobile.filter.DashboardFilter;
import com.medallia.mobile.filter.UnitFilter;
import com.medallia.mobile.filter.UnitFilter.FilterUnitGroup;
import common.Pair;

public abstract class CompanySettings {
	
	/** 
	 * @return The URI of the reporting application.  For example, "https://express.medallia.com".
	 */
	protected abstract String getBaseURI();
	
	/**
	 * @return The name of the company, as embedded in the URL of the reporting application.  For example,
	 * if the URL for the reporting application is https://express.medallia.com/productfeedback/, then 
	 * the company is "productfeedback", and that is the value that should be returned here.
	 */
	protected abstract String getCompany();

	/**
	 * @return The user name in the reporting application.
	 */
	protected abstract String getUser();

	/**
	 * @return The password in the reporting application.
	 */
	protected abstract String getPass();

	/** 
	 * @return The minimum e_responsedate value to use for the data set for the iPhone application. 
	 */
	public abstract Date getStartTime();

	/** 
	 * @return The date representing today. For demo purposes, this method may return 
	 * a date other than today.
	 */
	public final Date getToday() {
		return getTodayInternal() == null ? new Date() : getTodayInternal();
	}

	/**
	 * @param y The four digit year
	 * @param m The zero-based index for the month.  For example, January = 0, ..., December = 11.
	 * @param d The day of the month.
	 * @return The {@link Date} object for the given parameters. The time will be set to midnight.
	 */
	protected final Date getDate(int y, int m, int d) {
		Calendar c = Calendar.getInstance();
		c.clear();
		c.set(y, m, d);
		return c.getTime();
	}
	
	/** 
	 * @return The date representing today.  Return null to actually use today's date. 
	 * Is is useful to set a date other than today to be able to demo with old data.
	 */
	protected Date getTodayInternal() { return null; }
	
	public int numberOfscoreDecimals() { return 1; }
	
	public boolean usesResolvedAlertState() { return false; }
	
	/** 
	 * @return The minimum value for satisfaction type questions (e.g. 1).  Used on the
	 * Responses Settings page in the iPhone application. 
	 */
	public abstract int getMinIntRange();
	
	/** 
	 * @return The maximum value for satisfaction type questions (e.g. 10).  Used on the
	 * Responses Settings page in the iPhone application.. 
     */
	public abstract int getMaxIntRange();

	/**
	 * @return Suggested graph y range [min, max]. Data lower or higher than the range will cause it to expand.
	 * One or both of the values may be null, indicating no range limit.
	 */
	public abstract Pair<Double, Double> getGraphYRange();
	
	/**
	 * @return The {@link BackgroundColorRule} to use for a numeric score {@link FieldWithColor} when it has 
	 * no {@link BackgroundColorRule} specified.
	 */
	protected abstract BackgroundColorRule getDefaultBackgroundColorRule();
	
	/**
	 * @return The default background color for the given score (i.e., when the {@link FieldWithColor} to
	 * which the score belongs does not have a {@link BackgroundColorRule}. 
	 */
	public final BackgroundColor getDefaultBackgroundColor(double score) {
		return getDefaultBackgroundColorRule().getBackgroundColor(score);
	}
	
	/**
	 * @return The unit group to filter on when building the dashboard.
	 * This was added for Apple, where we were using the question as a unit group filter, knowing that e.g. a
	 * Red Zone question would only be answered for surveys with Red Zone units. It turned out that this assumption
	 * is not true due to inconsistencies in the data. Therefore we need to add a unit group filter to ensure that
	 * results are correct.
	 */
	public abstract FilterUnitGroup getScoreFilterUnitGroup(String widgetRequestAction);

	/** @return The fields displayed on the dashboard. */
	public abstract List<FieldWithColor> getScoreFields(String widgetRequestAction);
	
	/** @return The fields displayed on the scorecard. */
	public abstract List<FieldWithColor> getScorecardFields();
	
	/** @return The score field displayed on responses list screen.  This field is also used for filtering. */
	public abstract FieldWithColor getMainScoreField();
	
	/** @return The field displayed as a sub header on responses list screen, or null if there shouldn't be a sub header. */
	public abstract StringField getResponsesSubheaderField();
	
	/** @return The ranker question configured for the given key, or null if none is found. */
	public FieldWithColor getRankerFieldByKey(String key) {
		// there will be few enough options that we can loop over them
		for (FieldWithColor field : getRankerFieldOptions())
			if (field.key.equals(key))
				return field;
		return null;
	}
	
	/** 
	 * @return The options for the field used on ranker. Only one field is ranked/displayed at a time,
	 * and this field will be selectable in a drop down on the iPhone.
	 */
	public abstract List<FieldWithColor> getRankerFieldOptions();
	
	/** @return Fields to show on the detailed responses page. */
	public abstract DetailFields getDetailFields();

	/** @return Common filters shown and used for e.g. scoreboards and ranker. */
	public abstract List<DashboardFilter> getCommonFilters();

	/** @return All possible filters used for the scoreboard. See {@link #getShownScoreboardFilters(String)}. */
	public final List<DashboardFilter> getScoreboardFilters() {
		return getShownScoreboardFilters(null);
	}

	/**
	 * @return Filters shown for the scoreboard for the widget with the given key.
	 * Pass in null to return all possible filters. Some filters might be hidden for certain widgets.
	 * This is used to be able to customize and hide filters depending on the widget type.
	 */
	public abstract List<DashboardFilter> getShownScoreboardFilters(String widgetKey);

	/** @return All possible filters used for the ranker. See {@link #getShownRankerFilters(String)}. */
	public final List<DashboardFilter> getRankerFilters() {
		return getShownRankerFilters(null);
	}

	/**
	 * @return Filters shown for the ranker for the widget with the given key.
	 * Pass in null to return all possible filters. Some filters might be hidden for certain widgets.
	 * This is used to be able to customize and hide the unit group selector depending on the widget type.
	 */
	public abstract List<DashboardFilter> getShownRankerFilters(String widgetKey);

	/** @return Collection of all the filters used in the various modules. */
	public Collection<DashboardFilter> getAllFilters() {
		Map<String, DashboardFilter> filters = new HashMap<String, DashboardFilter>();
		for (DashboardFilter filter : getScoreboardFilters())
			filters.put(filter.getId(), filter);
		for (DashboardFilter filter : getRankerFilters())
			filters.put(filter.getId(), filter);
		return filters.values();
	}
	
	/**
	 * @return A map from unit group scopes to lists of {@link GraphBenchmarkScope}s. A list indicates which GraphBenchmarkScope
	 * should be used for a selected unit group under the corresponding unit group scope. Positions in the list indicate 
	 * at which level the selected unit group is found under the scope, e.g. position 0 means the selection is a direct child
	 * under the scope, position 1 is a grand child, etc. A special key {@link UnitFilter#UNIT_PARENT_ID} with a singleton list
	 * indicates which GraphBenchmarkScope should be used for a selection of a single unit.
	 */
	public abstract Map<String, List<GraphBenchmarkScope>> getGraphBenchmarkScopes();
	
	/**
	 * @return UnitGroups that will be shown on the unit filter.
	 * Returning an empty list will only show one screen with all the units, rather than a nested unit/unit group hierarchy.
	 */
    public abstract List<FilterUnitGroup> getFilterUnitGroups();
    
    /**
     * @return Collection of all unit groups where each name might have been filtered specific to the company.
     * (E.g. apple removes the "ARMnnnn" codes in front of market names.) 
     */
    public List<UnitGroup> getUnitGroupsWithFilteredName(boolean extendedScope) {
		SelectQuery<UnitGroup> query = UnitGroup.getUnitGroupQuery();
		query.setExtended(extendedScope);
		List<UnitGroup> unitGroups = query.run(getConnection());
		List<UnitGroup> filtered = new ArrayList<UnitGroup>();
		for (UnitGroup ug : unitGroups)
			filtered.add(new UnitGroup(ug.unitgroupid, getFilteredUGName(ug.name), ug.parent_unitgroupid));
		return filtered;
    }

    /** @return Filtered version of unit group name. E.g. apple removes the "ARMnnnn" codes in front of market names. */
    protected abstract String getFilteredUGName(String originalName);

    /** @return Filter unit groups shown as buttons on the responses screen (e.g. "All", "RZ", "GB", "PT" for Apple). */
    public abstract List<FilterButton> getResponsesFilterButtons();
    
    /** Apply responses filter with the given key to the given query. */
    public abstract void applyResponsesFilter(SelectQueryWithBooleanFilter query, String filter);
    
    /** Get responses filter key for the given survey. */
    public String extractResponsesFilter(Survey survey) { return null; }
    
	/** @return Default time period */
    public abstract Timeperiod getDefaultTimePeriod();
	
	/** @return Whether to show the alert type on the Responses list page. */
	public abstract boolean showAlertTypeInResponsesList();
	
	/** 
	 * @return Whether to merge units with equal names.  As of Oct 7, 2009, this feature
	 * is only needed for Apple Retail. 
	 */
	public boolean mergeEqualUnits() { return false; }
	
	/**
	 * 
	 * @param query An MQL query.
	 * @return The URI for the data API query.
	 */
	public final String getQueryURI(Statement query) {
		return getBaseURI() + "/api?company=" + Common.urlencode(getCompany()) + 
		"&user=" + Common.urlencode(getUser()) + 
		"&pass=" + Common.urlencode(getPass()) + 
		"&charSet=utf8" + 
		(query.isExtendedScope() ? "&extended_scope=true" : "") +
		"&query=" + Common.urlencode(query.getQueryString()) + 
		"&output=json" +
		"&version=1"
		;
	}
	
	/** @return The ApiConnection to use for this company. */
	public final ApiConnection getConnection() {
		return new DirectApiConnection(getCompany(), getUser(), getPass(), getBaseURI());
	}

	/**
	 * @param surveyId
	 * @return The URI that takes the iPhone application user to the reporting application's
	 * Responses > Form page for the given survey ID.
	 */
	public final String getSurveyURI(String surveyId) {
		return getBaseURI() + "/" + getCompany() + "/respInvForm.do?username=" + getUser() + "&password=" + getPass() + "&surveyid=" + surveyId;
	}

	/** @return What a unit represents for the client (e.g., "Property", "Store", etc.) */
	public abstract String getUnitTranslation();

	/** @return Widget types for the scoreboard. */
	public abstract List<WidgetType> getScoreWidgetTypes(LogonInfo logonInfo);
	
	/** @return Widget types and default instances for the ranker. */
	public abstract WidgetTypesWithDefaults getRankerWidgetTypesWithDefaults(LogonInfo logonInfo);

	@Override public String toString() {
		return String.format("C=%s u=%s", getCompany(), getUser());
	}
}
