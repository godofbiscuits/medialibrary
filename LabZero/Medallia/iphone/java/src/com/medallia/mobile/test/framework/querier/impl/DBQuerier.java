package com.medallia.mobile.test.framework.querier.impl;

import com.medallia.mobile.test.framework.config.ExpressEnvironment;
import com.medallia.mobile.test.framework.querier.AbstractQuerier;
import com.medallia.mobile.test.framework.querier.Querier;

/**
 * Implementation of a {@link Querier} that directly connects to the Db.
 * 
 * <p>
 * 	Shouldn't be used directly, {@link ExpressQuerier} should be used instead.
 * 
 * <p> Not implemented yet.
 * 
 * @author fernando
 */
public class DBQuerier extends AbstractQuerier {
	
	private DBQuerier(ExpressEnvironment express) {
		this.express = express;
	}
	
	static Querier createQuerier(ExpressEnvironment express) {
		return new DBQuerier(express);
	}
}
