package com.medallia.mobile.companysettings;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.medallia.api.datamodel.Survey;
import com.medallia.api.orm.Field;
import com.medallia.api.orm.Field.StringField;
import com.medallia.mobile.DetailFields;
import com.medallia.mobile.DetailFieldsDefaultBase;
import com.medallia.mobile.FieldWithColor;
import com.medallia.mobile.RequestParameterMap.LogonInfo;

public class CompanySettingsProductFeedback extends CompanySettingsDefaultBase {

	public CompanySettingsProductFeedback(LogonInfo req) {
		super(req);
	}
	
	@Override protected String getBaseURI0() {
		return "https://express.medallia.com";
	}
	
	@Override protected String getCompany() { return "productfeedback"; }

	@Override public Date getStartTime() { return getDate(2009, 3, 2); }
	
	@Override public FieldWithColor getMainScoreField() {
		return new FieldWithColor("mi_product_recommend", "Likely to recommend");
	}
	
	protected StringField clientField = new StringField("unitid.name", "Client");

	@Override public List<FieldWithColor> getScoreFields(String widgetRequestAction) {
		return Arrays.asList(getMainScoreField(), new FieldWithColor("mi_product_osat", "Overall Satisfaction"));
	}
	
	@Override public List<FieldWithColor> getScorecardFields() {
		return getScoreFields("");
	}
	
	@Override
    public String getUnitTranslation() { return "Client"; }

	// detailed response
	@Override public DetailFields getDetailFields() {
		return new DetailFieldsDefaultBase() {
			@Override
			public List<StringField> getCommentFields() {
				return Arrays.asList(new StringField("mi_product_comment", "How are we doing?"));
			}
			@Override public List<FieldWithColor> getScoreFields() {
				return CompanySettingsProductFeedback.this.getScoreFields(null);
			}
			
			@Override
			public List<StringField> getNameFields() {
				return Arrays.asList(clientField);
			}

			@Override
			public List<Field> getParticipantFields() {
				List<Field> list = new ArrayList<Field>();
				list.add(Survey.RESPONSEDATE);
				list.add(clientField);
				list.add(new StringField("username", "Username"));
//				list.add(new Field("logins", "Logins"));
				list.add(new StringField("ag", "Account group"));
				list.add(new StringField("mi_product_contact_allow", "Allow contact"));
				list.add(new StringField("mi_product_contact_email", "Contact email"));
//				list.add(phone);
				return list;
			}
		};
	}
	
}
