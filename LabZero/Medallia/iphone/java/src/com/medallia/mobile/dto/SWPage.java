package com.medallia.mobile.dto;
import java.util.*;
import com.medallia.mobile.*;
import com.medallia.mobile.dto.*;
import com.medallia.mobile.dto.builders.*;
import com.medallia.mobile.color.UIColor;

// These files are generated automatically from declarations in DataFormat.h; see convert.pl for details.
//
// Do *not* modify this file directly!


/**
 *  Each scoreboard widget has one page
 */
@SuppressWarnings("unused")
public interface SWPage extends Plist.Plistable {

	/**
	 *  The main title for this widget (defaults to widgetType.name if not present)
	 */
	String title();

	/**
	 *  The subtitle (defaults to a description of the current filters if not present)
	 */
	String subTitle();

	/**
	 *  "vs 6m" - explains the benchmark period we are comparing to
	 */
	@Required String benchTitle();

	/**
	 *  List of time periods. There will be as many of these as graphs for each line.
	 */
	@Required List<TimePeriod> timePeriods();

	/**
	 *  List of line styles. There will be as many of these as lines in each graph.
	 */
	@Required List<LineStyle> lineStyles();

	/**
	 *  The page contents
	 */
	@Required List<SWLine> lines();


}
