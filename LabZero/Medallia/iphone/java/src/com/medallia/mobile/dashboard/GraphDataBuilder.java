package com.medallia.mobile.dashboard;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import tiny.CollUtils;
import tiny.Empty;

import com.medallia.api.datamodel.Survey;
import com.medallia.api.datamodel.SurveyAggregate;
import com.medallia.api.datamodel.SurveyAggregateQuery;
import com.medallia.api.datamodel.UnitGroup;
import com.medallia.api.datamodel.UnitGroupMembership;
import com.medallia.api.orm.Row;
import com.medallia.api.orm.SelectQuery;
import com.medallia.mobile.Common;
import com.medallia.mobile.FieldWithColor;
import com.medallia.mobile.RequestParameterMap;
import com.medallia.mobile.RequestParameterMap.Param;
import com.medallia.mobile.companysettings.CompanySettings;
import com.medallia.mobile.dashboard.FieldScores.SingleScore;
import com.medallia.mobile.dto.Graph;
import com.medallia.mobile.dto.builders.Build;
import com.medallia.mobile.dto.builders.Build.GraphBuilder;
import com.medallia.mobile.filter.DashboardFilter;
import com.medallia.mobile.filter.Filters;
import com.medallia.mobile.filter.UnitFilter;
import com.medallia.mobile.filter.UnitParam;
import com.medallia.mobile.filter.UnitFilter.FilterUnitGroup;
import common.Pair;

public class GraphDataBuilder {
	private final CompanySettings config;
	private final RequestParameterMap req;
	private final List<FieldWithColor> fields;
	private final FilterUnitGroup filterUnitGroup;
	
	/** number of graph points in small graphs */
	private static final int NR_SMALL_GRAPH_POINTS = 50;
	
	/** number of graph points in large graphs */
	private static final int NR_LARGE_GRAPH_POINTS = 50;
	
	public GraphDataBuilder(CompanySettings config, RequestParameterMap req) {
		this.config = config;
		this.req = req;
		this.fields = config.getScoreFields(req.getActionRaw());
		this.filterUnitGroup = config.getScoreFilterUnitGroup(req.getActionRaw());
	}
	
	/** @return Graph data for the {@link #req} request. */
	GraphData getGraphData() {
		UnitParam unitParam = new UnitParam(req.get(Param.unitid));
		
		GraphData graphData = new GraphData();
		graphData.addData(getGraphScoreDataForUG(unitParam), config.getUnitTranslation());
		
		if (!unitParam.isAll()) {
			// create map from unit group to list of unit group children
			SelectQuery<UnitGroup> unitQuery = UnitGroup.getUnitGroupQuery();
			unitQuery.setExtended(true);
			Collection<UnitGroup> unitGroups = unitQuery.run(config.getConnection());
	
			Map<String, List<String>> ugChildren = Empty.hashMap();
			Map<String, String> ugParent = Empty.hashMap();
			for (UnitGroup ug : unitGroups) {
				CollUtils.addToMapList(ugChildren, ug.parent_unitgroupid, ug.unitgroupid);
				ugParent.put(ug.unitgroupid, ug.parent_unitgroupid);
			}

			GraphBenchmarkScope benchmarkScope = GraphBenchmarkScope.getBenchmarkScope(config, ugParent, unitParam);
			
			if (benchmarkScope != null) {
				// create map from unit group to set of units
				SelectQuery<UnitGroupMembership> membershipQuery = UnitGroupMembership.getUnitGroupMembershipQuery();
				membershipQuery.setExtended(true);
				Collection<UnitGroupMembership> memberships = membershipQuery.run(config.getConnection());

				Map<String, Set<String>> unitsByUG = new HashMap<String, Set<String>>();
				for (UnitGroupMembership membership : memberships)
					CollUtils.addToMapSet(unitsByUG, membership.unitgroupid, membership.unitid);
		
				// find benchmark unit group
				Set<String> selectedUnits = unitParam.getAllUnits(unitsByUG);
				String benchmarkUG = benchmarkScope.getBenchmarkUG(ugChildren, memberships, selectedUnits);
				if (benchmarkUG != null) {
					System.out.println("## got parent: "+benchmarkUG);
					graphData.addData(getGraphScoreDataForUG(new UnitParam(UnitFilter.UNIT_GROUP_PREFIX + benchmarkUG)), benchmarkUG);
				}
			}
		}
		return graphData;
	}
	
	/** @return List of [list of [list of graphs for each time period] for each field] for each graph size. */
	private List<List<List<Graph>>> getGraphScoreDataForUG(UnitParam unitParam) {
		SurveyAggregateQuery query = new SurveyAggregateQuery();
		buildGraphQuery(query, unitParam);
		System.err.println("built query = " + query);

		Collection<SurveyAggregate> table = query.run(config.getConnection());

		// list of results per day; each day contains a list of results per field
		List<DayResult> dayResults = new ArrayList<DayResult>();
		
		// convert table of query results to a more efficient list of DayResults
		for (Row row : table) {
			int day = getDaysFromToday(row.getDate(Survey.RESPONSEDATE));
			DayResult dayResult = new DayResult(day);
			
			for (FieldWithColor f : fields) {
				int fieldCount = row.getInt(f.count());
				// value is undefined (actually NaN) for zero sample size, so set it to 0 to not mess up aggregates
				double fieldValue = fieldCount == 0 ? 0 : row.getDouble(f.avg());
				dayResult.fieldResults.add(new FieldResult(f, fieldCount, fieldValue));
			}
			
			dayResults.add(dayResult);
		}
		
		List<List<List<Graph>>> graphs = new ArrayList<List<List<Graph>>>();
		graphs.add(getGraphs(NR_SMALL_GRAPH_POINTS, dayResults));
		graphs.add(getGraphs(NR_LARGE_GRAPH_POINTS, dayResults));
		
		return graphs;
	}

	private void buildGraphQuery(SurveyAggregateQuery query, UnitParam unitParam) {
		query.addField(Survey.RESPONSEDATE);
		for (FieldWithColor f : fields) {
			query.addField(f.count());
			query.addField(f.avg());
		}
		query.whereTimePeriod(config.getStartTime(), config.getToday());
		if (filterUnitGroup != null)
			query.addCondition("group = '" + filterUnitGroup.id + "'");
		for (DashboardFilter filter : config.getShownScoreboardFilters(req.getActionRaw())) {
			// override the unit filter
			if (Filters.ALT_UNIT.equals(filter.getId())) {
				UnitFilter.applyUnitFilter(unitParam, query);
			} else {
				filter.addFilter(req, query);
			}
		}
		// order responses descending to build the score data from today and backwards
		query.setOrderBy("responsedate DESC");
	}

	/** @return The difference in days from today and the given date (e.g. returns 7 if the given date is 1 week ago). */
	private int getDaysFromToday(Date date) {
		Date today = config.getToday();
		return (int)((today.getTime() - date.getTime()) / Common.MS_PER_DAY);
	}
	
	/** @return List of [list of graphs for each time period] for each field. */
	private List<List<Graph>> getGraphs(int graphPoints, List<DayResult> dayResults) {
		// list of <list of graphs for each time period> for each field
		List<List<Graph>> graphs = new ArrayList<List<Graph>>();
		Calendar today = Calendar.getInstance();
		final double EPS = 1e-10;
		for (GraphTimePeriod period : GraphTimePeriod.values()) {
//		for (double magic : new double[] { .01, .02, .05, .1, .2, .5}) {
			// list of <list of graph point data> for each field
			List<List<GraphPoint>> graphDataAllFields = new ArrayList<List<GraphPoint>>();
//			int periodDays = GraphTimePeriod.TWO_YEARS.getPeriodDays(today);
			int periodDays = period.getPeriodDays(today);
			int fieldIdx = 0;
			for (@SuppressWarnings("unused") FieldWithColor f : fields) {
				List<GraphPoint> points = new ArrayList<GraphPoint>();
				SingleScore score = new SingleScore();
				double pointDist = 1. / (Math.min(periodDays, graphPoints)-1);
				double prevPointX = -pointDist-EPS;
				double firstX = Double.NaN;
				for (int dayIdx = 0; dayIdx < dayResults.size(); ++dayIdx) {
					int curDay = dayResults.get(dayIdx).day;
					double curX = (double)curDay/(periodDays-1);
					if (score.count == 0)
						firstX = curX;
					
					score.add(dayResults.get(dayIdx).fieldResults.get(fieldIdx));

					double pointX = (curX+firstX)/2, diffX = pointX - prevPointX;

					// add a point if either we haven't added one for 0.1 units OR it's been at least pointDist units and the magic formula evaluates to > 0.2
					// the magic formula is used to include more graph points the more data there is and the longer the distance to the last point
					if (diffX > 0.1 || (diffX * diffX * score.count > 0.2 && diffX >= pointDist)) {
						double y = score.getAvg();
						points.add(new GraphPoint(1-pointX, y));
						if ((prevPointX = pointX) >= 1)
							break; // we just added a point outside the chart -- means we're done
						score = new SingleScore();
					}
				}
				graphDataAllFields.add(points);
				++ fieldIdx;
			}
			addGraphs(graphs, graphDataAllFields);
		}
		return graphs;
	}
	
	/**
	 * Create one graph per field out of graphDataAllFields, and add them to the given
	 * list of [list of graphs for each time period].
	 */
	private void addGraphs(List<List<Graph>> graphs, List<List<GraphPoint>> graphDataAllFields) {
		if (graphs.isEmpty())
			for (int f = 0; f < fields.size(); ++f)
				graphs.add(new ArrayList<Graph>());
		for (int f = 0; f < fields.size(); ++f) {
			GraphBuilder graphBuilder = getGraphData(graphDataAllFields.get(f));
			graphs.get(f).add(graphBuilder.get());
		}
	}

	/** @return GraphBuilder with the graph data added to it. */
	private GraphBuilder getGraphData(List<GraphPoint> graphData) {
		StringBuilder sx = new StringBuilder();
		StringBuilder sy = new StringBuilder();
		for (int max = graphData.size()-1, v = max; v >= 0; --v) {
			if (v < max) {
				sx.append('|');
				sy.append('|');
			}
			sx.append(formatX(graphData.get(v).x));
			sy.append(formatY(graphData.get(v).y));
		}
		// mutable list so benchmark data can be added to it
		GraphBuilder gb = Build.graph().lineData(Empty.list(Arrays.asList(Build.lineData().xData(sx.toString()).yData(sy.toString()).get())));
		Pair<Double, Double> graphYRange = config.getGraphYRange();
		if (graphYRange.first != null)
			gb.minY(String.valueOf(graphYRange.first));
		if (graphYRange.second != null)
			gb.maxY(String.valueOf(graphYRange.second));
		return gb;
	}
	
	private String formatCoordinate(double v, int decimals) {
		String s = String.format("%." + decimals + "f", v);
		// remove leading 0 to save some space...
		return s.startsWith("0.") ? s.substring(1) : s;
	}
	
	private String formatX(double x) {
		return formatCoordinate(x, 4);
	}
	
	private String formatY(double y) {
		return formatCoordinate(y, 2);
	}
}
