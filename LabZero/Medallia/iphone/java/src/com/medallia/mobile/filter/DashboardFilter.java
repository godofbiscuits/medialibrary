package com.medallia.mobile.filter;

import java.util.List;

import com.medallia.api.datamodel.SelectQueryWithBooleanFilter;
import com.medallia.mobile.RequestParameterMap;
import com.medallia.mobile.dto.AltSection;
import com.medallia.mobile.dto.ClickableHeaderElement;

/** Dashboard filter. These are used for all widgets that have a filter screen, including the scoreboard, ranker and responses list. */
public interface DashboardFilter {
	/** @return Filter identifier, e.g. 'UNIT'. */
	public String getId();

	/** @return Filter name, e.g. 'Property'. */
	public String getName();
	
	/** @return Filter external key, used in the http request, e.g. 'unitid'. */
	public String getKey();
	
	/** @return the clickable header element to hook this filter up with,
	 *          or ClickableHeaderElement.CHNone if no connection should be made
	 */
	public ClickableHeaderElement getHeaderElement();
	
	/** @return Default alternative value, e.g. 'all'. */
	public String getDefault();
	
	/** @return Filter sections. */
	public List<AltSection> getSections();

	/** Add this filter to the given query. The filter settings are obtained from the given request map. */
	public void addFilter(RequestParameterMap req, SelectQueryWithBooleanFilter query);
}