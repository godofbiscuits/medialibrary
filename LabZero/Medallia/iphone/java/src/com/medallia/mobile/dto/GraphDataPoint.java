package com.medallia.mobile.dto;
import com.medallia.mobile.Plist;
import com.medallia.mobile.dto.builders.Required;

// These files are generated automatically from declarations in DataFormat.h; see convert.pl for details.
//
// Do *not* modify this file directly!


/**
 *  Graph data point.
 */
@SuppressWarnings("unused")
public interface GraphDataPoint extends Plist.Plistable {

	/**
	 *  Graph data point.
	 */
	@Required double value();


}
