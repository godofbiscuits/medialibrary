package com.medallia.mobile.dto;
import java.util.*;
import com.medallia.mobile.*;
import com.medallia.mobile.dto.*;
import com.medallia.mobile.dto.builders.*;
import com.medallia.mobile.color.UIColor;

// These files are generated automatically from declarations in DataFormat.h; see convert.pl for details.
//
// Do *not* modify this file directly!


/**
 *  Settings pages are divided into sections
 */
@SuppressWarnings("unused")
public interface ConfigSection extends Plist.Plistable {

	/**
	 *  Section header
	 */
	@Required String name();

	/**
	 *  List of parts
	 */
	@Required List<ConfigPart> parts();


}
