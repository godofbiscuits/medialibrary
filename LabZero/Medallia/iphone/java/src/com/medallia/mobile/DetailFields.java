package com.medallia.mobile;

import java.util.List;

import tiny.Empty;

import com.medallia.api.orm.Field;
import com.medallia.api.orm.Field.StringField;

/** A field to show on the detailed responses page. */
public abstract class DetailFields {
	public abstract boolean showAlert();
	public abstract List<StringField> getCommentFields();
	public abstract List<FieldWithColor> getScoreFields();
	public abstract List<Field> getParticipantFields();
	public abstract List<FieldAndAction> getActionFields();
	
	/** @return the fields to display as name in responses list screen */
	public abstract List<StringField> getNameFields();

	public abstract StringField getPhoneField();
	public abstract StringField getCommentField();
	
	public List<Field> getExtraFieldsToLoad() { return Empty.list(); }
	
	/** @return Hard coded phone number (used for demo purposes), or null to use the actual phone number from the {@link #getPhoneField}. */
	public String getHardCodedPhoneNr() {
		return null;
	}
	
}