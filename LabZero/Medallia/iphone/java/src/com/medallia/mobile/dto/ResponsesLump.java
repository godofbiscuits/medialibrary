package com.medallia.mobile.dto;
import java.util.*;
import com.medallia.mobile.*;
import com.medallia.mobile.dto.*;
import com.medallia.mobile.dto.builders.*;
import com.medallia.mobile.color.UIColor;

// These files are generated automatically from declarations in DataFormat.h; see convert.pl for details.
//
// Do *not* modify this file directly!


/**
 *  A batch of responses
 */
@SuppressWarnings("unused")
public interface ResponsesLump extends Plist.Plistable {

	/**
	 *  The text shown at the end of the list
	 */
	@Required String fetchMoreText();

	/**
	 *  If set, the URL to use to fetch the next ResponsesLump. If null, the button will open the config page instead.
	 */
	String fetchMoreURL();

	/**
	 *  The list of surveys.
	 */
	@Required List<ResponsesEntry> entries();

	/**
	 *  List of filter buttons shown under the search bar.
	 */
	@Required List<FilterButton> filterButtons();

	/**
	 *  Number of open alerts (optional -- only sent when alerts_only is set)
	 */
	String nrOpenAlerts();


}
