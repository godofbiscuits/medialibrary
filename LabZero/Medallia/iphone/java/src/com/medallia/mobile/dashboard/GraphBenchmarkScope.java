package com.medallia.mobile.dashboard;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import tiny.Empty;

import com.medallia.api.datamodel.UnitGroupMembership;
import com.medallia.mobile.companysettings.CompanySettings;
import com.medallia.mobile.filter.UnitFilter;
import com.medallia.mobile.filter.UnitParam;

/** Contains information about which scope to use for a benchmark graph. */
public class GraphBenchmarkScope {
	/**
	 * Scope unit group. The actual benchmark unit group will be a child (not necessarily direct) of this unit group.
	 * For example, the scope might be "Market" or "Geo/Region", and the actual benchmark unit group might be
	 * "Atlanta Market" or "Region 4: Southeast" (respectively). (The actual benchmark unit group will be found
	 * by seeing which child of this scope unit group contains the selected unit(s).)
	 */
	private String scopeUG;
	
	/**
	 * At which level under the scope unit group the actual benchmark unit group should be located. A level of 0
	 * means to look at the direct children of the scope UG, while level 1 means the grand children, etc.
	 * For example, if the scope is "Geo/Region" and level is 0, we could find the Geo unit group "Americas".
	 * If the level was 1, we could have found the Region unit group "Region 5: Mid-Atlantic".
	 */
	private int level;
	
	/** Create a new scope with the given {@link #scopeUG} and {@link #level}. */
	public GraphBenchmarkScope(String scopeUG, int level) {
		this.scopeUG = scopeUG;
		this.level = level;
	}

	/**
	 * @param ugParent Unit group parent by unit group.
	 * @return Graph benchmark scope for given parameters. Returns null if there shouldn't be any benchmarking for the given parameters.
	 */
	static GraphBenchmarkScope getBenchmarkScope(CompanySettings config, Map<String, String> ugParent, UnitParam unitParam) {
		Map<String, List<GraphBenchmarkScope>> scopeOptions = config.getGraphBenchmarkScopes();
		if (scopeOptions.isEmpty()) return null;
		if (unitParam.isUnit()) {
			return scopeOptions.get(UnitFilter.UNIT_PARENT_ID).get(0);
		} else {
			String ugid = unitParam.unitGroupId();
			int level = -1;
			do {
				ugid = ugParent.get(ugid);
				assert ugid != null; // parent should always be one of scope options, otherwise there's a configuration error
				++level;
			} while (!scopeOptions.keySet().contains(ugid));
			return scopeOptions.get(ugid).get(level);
		}
	}
	
	/**
	 * @return The actual benchmark unit group for the given set of selected units.
	 * Returns null if no or multiple benchmark unit groups were found.
	 */
	String getBenchmarkUG(Map<String, List<String>> ugChildren, Collection<UnitGroupMembership> memberships, Set<String> selectedUnits) {
		// find children of the benchmark scope; the benchmark unit group will be in this set
		Set<String> benchmarkUGs = Empty.hashSet(ugChildren.get(scopeUG));
		for (int i = 0; i < level; ++i) {
			Set<String> children = Empty.hashSet();
			for (String child : benchmarkUGs)
				if (ugChildren.containsKey(child))
					children.addAll(ugChildren.get(child));
			benchmarkUGs = children;
		}
		
		// create map from unit to benchmark unit group
		Map<String, String> benchmarkUGByUnit = new HashMap<String, String>();
		for (UnitGroupMembership membership : memberships)
			if (benchmarkUGs.contains(membership.unitgroupid))
				benchmarkUGByUnit.put(membership.unitid, membership.unitgroupid);

		Set<String> parentUGs = new HashSet<String>();
		for (Iterator<String> it = selectedUnits.iterator(); it.hasNext() && parentUGs.size() < 2; )
			parentUGs.add(benchmarkUGByUnit.get(it.next()));
		
		return parentUGs.size() == 1 ? parentUGs.iterator().next() : null;
	}
	
	@Override
	public String toString() {
		return scopeUG + " (" + level +")";
	}
}