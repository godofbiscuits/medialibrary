package com.medallia.mobile.dto.builders;
import java.lang.reflect.Field;

/** Base class for the auto-generated builders; uses reflection to verify
 * that required fields are indeed set. */
public class Builder<X> {
    public void check() {
        for (Field f : getClass().getDeclaredFields()) {
            try {
                f.setAccessible(true);
                if (f.isAnnotationPresent(Required.class) && f.get(this) == null)
                    throw new RuntimeException(getClass().getSimpleName()+": required field '"+f.getName()+"' missing");
            } catch (IllegalAccessException e) {
                throw new AssertionError();
            }
        }
    }    
}
