package com.medallia.mobile.filter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.medallia.mobile.companysettings.CompanySettings;
import com.medallia.mobile.dto.Alt;
import com.medallia.mobile.dto.AltSection;
import com.medallia.mobile.dto.AltTable;
import com.medallia.mobile.dto.builders.Build;

public class Filters {

	public static final String
		ALT_UNIT = "UNIT", // standard 'alternative sets'     
		ALT_TIME_PERIOD = "TIME_PERIOD",
		ALT_RANKER_QUESTION = "RANKER_QUESTION",
	    ALT_ROLE = "ROLE",
	    ALT_YEARS_WORKED = "YEARS_WORKED",
	    ALT_EMPLOYEES = "EMPLOYEES",
	    ALT_PREVIEW_LINES = "_previewLines",
	    ALT_PREVIEW_FONT = "_previewFont";
	
    public static List<AltTable> tables(CompanySettings config) {
    	List<AltTable> tables = new ArrayList<AltTable>();
    	for (DashboardFilter filter : config.getAllFilters())
    		tables.add(Build.altTable()
    				.key(filter.getId())
    				.sections(filter.getSections())
    				.get()
    				);
    	tables.add(Build.altTable().key(ALT_PREVIEW_LINES).sections(getPreviewLines()).get());
    	tables.add(Build.altTable().key(ALT_PREVIEW_FONT).sections(getPreviewFont()).get());
    	return tables;
    }
    
	public static List<AltSection> getPreviewLines() {
    	return altList(
            "3", "3 lines",
            "4", "4 lines",
            "5", "5 lines");
	}

	public static List<AltSection> getPreviewFont() {
    	return altList(
            "9", "Tiny",
            "12", "Default",
            "14", "Medium",
            "16", "Large");
	}
    
    public static List<AltSection> altList(String... pairs) {
        List<Alt> a = new ArrayList<Alt>();
        for (int i = 0; i < pairs.length-1; )
            a.add(Build.alt().key(pairs[i++]).value(pairs[i++]).table(Build.altTable().key("").sections(new ArrayList<AltSection>()).get()).get());
        return Arrays.asList(Build.altSection().alternatives(a).get());
    }

	public static Map<String, String> createDefaultArgumentMap(Collection<DashboardFilter> filters) {
		Map<String, String> argumentMap = new HashMap<String, String>();
		for (DashboardFilter sbf : filters) {
			argumentMap.put(sbf.getKey(), sbf.getDefault());
		}
		return argumentMap;
	}
            
}
