package com.medallia.mobile.responses;

import java.text.ParseException;
import java.util.Date;

import com.medallia.api.datamodel.Note;
import com.medallia.api.datamodel.Survey;
import com.medallia.api.datamodel.SurveyQuery;
import com.medallia.api.orm.InsertStatement;
import com.medallia.api.orm.Row;
import com.medallia.api.orm.UpdateStatement;
import com.medallia.mobile.Common;
import com.medallia.mobile.RequestParameterMap;
import com.medallia.mobile.RequestParameterMap.Param;
import com.medallia.mobile.companysettings.CompanySettings;

public class AlertNoteAction {
	private final CompanySettings config;
	private final RequestParameterMap req;
	
	public AlertNoteAction(CompanySettings config, RequestParameterMap req) {
		this.config = config;
		this.req = req;
	}

	public void handleRequest() {
		String surveyId = req.get(Param.id);
		
		StringBuilder noteContent = new StringBuilder();
		
		String alertAction = req.get(Param.alert_action);
		if (alertAction != null) {
			UpdateStatement statement = Survey.updateAlertStatement(surveyId, alertAction);
			String error = statement.runAcceptQuery(config.getConnection());
		    if (error == null) {
		    	System.out.println("alert action OK");
		    	AlertStatusAction statusAction = AlertStatusAction.getActionForStatus(alertAction);
		    	if (statusAction != null) {
		    		noteContent.append(statusAction.getNoteLogText());
		    	}
		    } else {
		    	System.err.println("Failed to update alert for surveyid=" + surveyId + ", action=" + alertAction + " (error=" + error + ")");
		    }
		}
		
		String callInitiated = req.get(Param.call_initiated);
		if (callInitiated != null) {
			Date date = null;
			try {
				date = Common.UTC_DF.parse(callInitiated);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			if (date != null) {
				if (noteContent.length() > 0) {
					noteContent.append("\n\n");
				}
				noteContent.append("Customer called at " + Common.LOG_DF.format(date));
			}
		}
	    
		String manualNote = req.get(Param.note_content);
		if (manualNote != null) {
			if (noteContent.length() > 0) {
				noteContent.append("\n\n");
			}
			noteContent.append(manualNote);
		}

		if (noteContent.length() > 0) {
			InsertStatement statement = Note.getNewNoteStatement(surveyId, noteContent.toString());
		    String error = statement.runAcceptQuery(config.getConnection());
		    if (error == null)
		    	System.out.println("note action OK");
		    else
		    	System.err.println("Failed to add note for surveyid=" + surveyId + ", noteContent=" + noteContent + " (error=" + error + ")");
		}
	}
	
	public static String getNrOpenAlerts(CompanySettings config) {
		SurveyQuery query = new SurveyQuery();
		query.addField(Survey.HAS_OPEN_ALERT.sum());
		Row row = query.runSingleRowQuery(config.getConnection());
		return String.valueOf(row.getLong(Survey.HAS_OPEN_ALERT.sum()));
	}
}
