package com.medallia.mobile.test.tests;

import com.medallia.mobile.dto.SWPage;
import com.medallia.mobile.filter.UnitFilter;
import com.medallia.mobile.test.framework.MobileTestCase;
import com.medallia.mobile.test.framework.beans.Id;
import com.medallia.mobile.test.framework.config.Environment;
import com.medallia.mobile.test.framework.config.ExpressEnvironment;
import com.medallia.mobile.test.framework.config.MobileEnvironment;
import com.medallia.mobile.test.framework.requests.MobileRequest;
import com.medallia.mobile.test.framework.requests.MobileRequestBuilder;

/**
 * Scenario Suite that runs several tests that exercises the Scores request.
 * 
 * @author fernando
 */
@Environment(express=ExpressEnvironment.EARTH, mobile=MobileEnvironment.LOCALHOST)
public class LocalScoresRequestTest extends MobileTestCase {

	private MobileRequest scoresRequest;
	
	
	/**
	 * Tests that the Title and Subtitle are shown correctly
	 * 
	 * <p> Preconditions:
	 * <ul>
	 * 	<li> Timeperiod 180 corresponds to Year 09.
	 * </ul>
	 * 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Send a request with timeperiod 180 and check that it contains the right title and
	 *       subtitle.
	 * </ul>
	 */
	public void testTitleAndSubtitleContainsCorrectValue() throws Exception {
		scoresRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment())
			.timePeriod(new Id(180)).buildScoresRequest();
		SWPage response = getScoresResponse(scoresRequest);
		assertEquals("", response.title());
		assertEquals("", response.subTitle());		
	}
	
	/**
	 * Tests that the request contains the right scores.
	 * 
	 * <p> IMPORTANT: It is very complicated to actually check that all the data points are
	 *     correct. Instead, for regression purposes, the data point values are hardcoded with
	 *     the values that the Mobile Servlet sent back. Those number might be wrong, but this
	 *     test will break in case those numbers changes, alerting that either a new bug was 
	 *     introduced or alerting that the calculation somehow changed.
	 * 
	 * <p> Preconditions:
	 * <ul>
	 * 	<li> Timeperiod 253 corresponds to Q2 Year 09.
	 * 	<li> Timeperiod 250 corresponds to Q1 Year 09.
	 *  <li> Earth contains 2 score fields, Overall Experience and Overall Service.
	 *  <li> For period Q2 09, Overall Experience is 7.17 (rounded to 7.2) and Overall Service 
	 *       is 7.26 (rounded to 7.3)
	 *  <li> For period Q 09, Overall Experience is 5.27 (rounded to 5.3) and Overall Service 
	 *       is 5.42 (rounded to 5.4)
	 *  <li> The exact difference between both time periods is 1.84 (rounded to 1.8) for
	 *       Overall Service and 1.9 for Overall Experience.
	 * </ul>
	 * 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Send a request with timeperiod 253.
	 *  <li> Check that it contains to scores. Overall Service and Overall Experience.
	 *  <li> Check that it contains the right scores and difference based on the rank.
	 *  <li> Check that each score contains the right number of Graphs, both small and large.
	 *  <li> Check that the data points displayed are right.
	 * </ul>
	 */
	public void testScoreRequestContainsCorrectScores() throws Exception {
		scoresRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment())
			.timePeriod(new Id(253)).buildScoresRequest();
		SWPage response = getScoresResponse(scoresRequest);
		assertEquals(2, response.lines().size());
		
		
		assertEquals("Overall Experience", response.lines().get(0).title());
		assertEquals("7.2", response.lines().get(0).value());
		assertEquals("+1.9", response.lines().get(0).difference());
		assertEquals(6, response.lines().get(0).graphLarge().size());
		assertEquals(6, response.lines().get(0).graphSmall().size());
		
		assertEquals(3, response.lines().get(0).graphLarge().get(0).lineData().size());
		assertEquals(".5833", response.lines().get(0).graphLarge().get(0).lineData().get(0).xData());
		assertEquals("10.00", response.lines().get(0).graphLarge().get(0).lineData().get(0).yData());
		assertEquals(".5833", response.lines().get(0).graphLarge().get(0).lineData().get(1).xData());
		assertEquals("9.06", response.lines().get(0).graphLarge().get(0).lineData().get(1).yData());
		assertEquals(".5833", response.lines().get(0).graphLarge().get(0).lineData().get(2).xData());
		assertEquals("8.96", response.lines().get(0).graphLarge().get(0).lineData().get(2).yData());
		
		assertEquals(3, response.lines().get(0).graphLarge().get(1).lineData().size());
		assertEquals(".0741|.1852|.3148|.4259|.5370|.6667|.9074",
				response.lines().get(0).graphLarge().get(1).lineData().get(0).xData());
		assertEquals("6.17|6.60|9.33|3.00|5.00|10.00|10.00",
				response.lines().get(0).graphLarge().get(1).lineData().get(0).yData());
		assertEquals(".0741|.1852|.3148|.4259|.5370|.6667|.9074",
				response.lines().get(0).graphLarge().get(1).lineData().get(1).xData());
		assertEquals("6.09|6.50|9.92|3.06|4.34|10.20|10.97",
				response.lines().get(0).graphLarge().get(1).lineData().get(1).yData());
		assertEquals(".0741|.1852|.3148|.4259|.5370|.6667|.9074",
				response.lines().get(0).graphLarge().get(1).lineData().get(2).xData());
		assertEquals("5.51|4.92|7.78|2.70|3.25|9.57|7.77",
				response.lines().get(0).graphLarge().get(1).lineData().get(2).yData());

		assertEquals(3, response.lines().get(0).graphLarge().get(2).lineData().size());
		assertEquals(".1742|.2865|.3989|.5000|.6011|.7079|.8090|.9157",
				response.lines().get(0).graphLarge().get(2).lineData().get(0).xData());
		assertEquals("8.00|7.50|7.71|7.75|7.25|6.94|6.50|7.88",
				response.lines().get(0).graphLarge().get(2).lineData().get(0).yData());
		assertEquals(".1742|.2865|.3989|.5000|.6011|.7079|.8090|.9157",
				response.lines().get(0).graphLarge().get(2).lineData().get(1).xData());
		assertEquals("8.34|6.05|6.53|7.61|5.82|7.61|6.32|7.88",
				response.lines().get(0).graphLarge().get(2).lineData().get(1).yData());
		assertEquals(".1742|.2865|.3989|.5000|.6011|.7079|.8090|.9157",
				response.lines().get(0).graphLarge().get(2).lineData().get(2).xData());
		assertEquals("7.55|6.43|6.02|5.27|4.87|4.41|4.53|6.82",
				response.lines().get(0).graphLarge().get(2).lineData().get(2).yData());

		assertEquals(3, response.lines().get(0).graphLarge().get(3).lineData().size());
		assertEquals(".2056|.3083|.4028|.5083|.6111|.7139|.8167|.9250",
				response.lines().get(0).graphLarge().get(3).lineData().get(0).xData());
		assertEquals("5.67|4.92|6.00|4.67|7.38|7.76|7.33|7.13",
				response.lines().get(0).graphLarge().get(3).lineData().get(0).yData());
		assertEquals(".2056|.3083|.4028|.5083|.6111|.7139|.8167|.9250",
				response.lines().get(0).graphLarge().get(3).lineData().get(1).xData());
		assertEquals("5.77|4.93|6.09|4.04|7.79|8.40|7.06|7.38",
				response.lines().get(0).graphLarge().get(3).lineData().get(1).yData());
		assertEquals(".2056|.3083|.4028|.5083|.6111|.7139|.8167|.9250",
				response.lines().get(0).graphLarge().get(3).lineData().get(2).xData());
		assertEquals("4.10|4.54|5.88|3.09|4.77|7.48|4.68|4.62",
				response.lines().get(0).graphLarge().get(3).lineData().get(2).yData());
		
		assertEquals(3, response.lines().get(0).graphLarge().get(4).lineData().size());
		assertEquals(".5852|.6676|.7555|.8365|.9368",
				response.lines().get(0).graphLarge().get(4).lineData().get(0).xData());
		assertEquals("5.23|4.93|5.38|7.57|7.24",
				response.lines().get(0).graphLarge().get(4).lineData().get(0).yData());
		assertEquals(".5852|.6676|.7555|.8365|.9368",
				response.lines().get(0).graphLarge().get(4).lineData().get(1).xData());
		assertEquals("5.66|4.01|4.98|7.32|7.95",
				response.lines().get(0).graphLarge().get(4).lineData().get(1).yData());
		assertEquals(".5852|.6676|.7555|.8365|.9368",
				response.lines().get(0).graphLarge().get(4).lineData().get(2).xData());
		assertEquals("3.32|4.30|4.50|5.47|5.21",
				response.lines().get(0).graphLarge().get(4).lineData().get(2).yData());
		
		assertEquals(3, response.lines().get(0).graphLarge().get(5).lineData().size());
		assertEquals(".8196|.8827|.9561",
				response.lines().get(0).graphLarge().get(5).lineData().get(0).xData());
		assertEquals("5.25|5.66|7.39",
				response.lines().get(0).graphLarge().get(5).lineData().get(0).yData());
		assertEquals(".8196|.8827|.9561",
				response.lines().get(0).graphLarge().get(5).lineData().get(1).xData());
		assertEquals("5.16|4.84|6.45",
				response.lines().get(0).graphLarge().get(5).lineData().get(1).yData());
		assertEquals(".8196|.8827|.9561",
				response.lines().get(0).graphLarge().get(5).lineData().get(2).xData());
		assertEquals("3.61|3.74|6.76",
				response.lines().get(0).graphLarge().get(5).lineData().get(2).yData());
		
		
		
		assertEquals("Overall Service", response.lines().get(1).title());
		assertEquals("+1.8", response.lines().get(1).difference());
		assertEquals(6, response.lines().get(1).graphLarge().size());
		assertEquals(6, response.lines().get(1).graphSmall().size());
		fail("seems that smallGraph and LargeGraph contains the same data points, what i am missing?");
		assertEquals("What this should contain", response.benchTitle());
		assertEquals("What this should contain", response.lines().get(0).title());
		//TODO test color when it is implemented in the test framework: response.lines().get(0).diffColor();
		//TODO test color when it is implemented in the test framework: response.lines().get(0).scoreBackgroundColor()
	}
	
	/**
	 * Tests that the filtering by unit works.
	 * 
	 * <p> Preconditions:
	 * <ul>
	 * 	<li> Timeperiod 253 corresponds to Q2 Year 09.
	 *  <li> Earth contains 2 score fields, Overall Experience and Overall Service.
	 *  <li> For period Q2 09, Overall Experience is 6.5 for Oslo Unit and Overall Service 
	 *       is 7.2 for Oslo Unit.
	 * </ul>
	 * 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Send a request with timeperiod 253 filtering by Unit Oslo
	 *  <li> Check that it contains t2o scores. Overall Service and Overall Experience.
	 *  <li> Check that it contains the right scores.
	 * </ul>
	 */
	public void testFilterByUnitWorks() throws Exception {	
		scoresRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment())
			.timePeriod(new Id(253)).unit(UnitFilter.UNIT_PREFIX + "oslo").buildScoresRequest();
		String s = getResponse(scoresRequest);
		SWPage response = getScoresResponse(scoresRequest);
		assertEquals(2, response.lines().size());
		assertEquals("6.5", response.lines().get(0).value());
		assertEquals("7.2", response.lines().get(1).value());
	}
	
	/**
	 * Tests that the lineStyles contains the right info.
	 * 
	 * <p> Preconditions:
	 * <ul>
	 * 	<li> Timeperiod 253 corresponds to Q2 Year 09.
	 * </ul>
	 * 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Send a request with timeperiod 253.
	 * </ul>
	 */
	public void testLineStylesContainsCorrectInfo() throws Exception {
		scoresRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment())
			.timePeriod(new Id(253)).buildScoresRequest();
		SWPage response = getScoresResponse(scoresRequest);
		assertEquals(3, response.lineStyles().size());
		
		assertEquals("1", response.lineStyles().get(0).featured());
		assertEquals("first", response.lineStyles().get(0).legendText());

		assertEquals(null, response.lineStyles().get(1).featured());
		assertEquals("second", response.lineStyles().get(1).legendText());

		assertEquals(null, response.lineStyles().get(2).featured());
		assertEquals("third", response.lineStyles().get(2).legendText());
		fail("what is this lineStyles????");
		//TODO Check Colors once it is implemented on the test framework.
	}
	
	/**
	 * Tests that the timeperiods are set correctly in the request.
	 * 
	 * <p> Preconditions:
	 * <ul>
	 * 	<li> Timeperiod 253 corresponds to Q2 Year 09.
	 * </ul>
	 * 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Send a request with timeperiod 253.
	 * </ul>
	 */
	public void testTimePeriodContainsCorrectInfo() throws Exception {
		scoresRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment())
			.timePeriod(new Id(253)).buildScoresRequest();
		SWPage response = getScoresResponse(scoresRequest);
		assertEquals(6, response.timePeriods().size());
		
		assertEquals("1w", response.timePeriods().get(0).title());
		assertEquals("1m", response.timePeriods().get(1).title());
		assertEquals("3m", response.timePeriods().get(2).title());
		assertEquals("6m", response.timePeriods().get(3).title());
		assertEquals("1y", response.timePeriods().get(4).title());
		assertEquals("2y", response.timePeriods().get(5).title());
		
		assertEquals(0, response.timePeriods().get(0).labelsSmallGraph().size());
		assertEquals(0, response.timePeriods().get(1).labelsSmallGraph().size());
		assertEquals(0, response.timePeriods().get(2).labelsSmallGraph().size());
		assertEquals(0, response.timePeriods().get(3).labelsSmallGraph().size());
		assertEquals(0, response.timePeriods().get(4).labelsSmallGraph().size());
		assertEquals(0, response.timePeriods().get(5).labelsSmallGraph().size());
		
		assertEquals(7, response.timePeriods().get(0).labelsLargeGraph().size());
		assertEquals(4, response.timePeriods().get(1).labelsLargeGraph().size());
		assertEquals(6, response.timePeriods().get(2).labelsLargeGraph().size());
		assertEquals(6, response.timePeriods().get(3).labelsLargeGraph().size());
		assertEquals(6, response.timePeriods().get(4).labelsLargeGraph().size());
		assertEquals(8, response.timePeriods().get(5).labelsLargeGraph().size());
		
		assertEquals("Mar 17", response.timePeriods().get(0).labelsLargeGraph().get(0).label());
		assertEquals(0.0, response.timePeriods().get(0).labelsLargeGraph().get(0).position());
		assertEquals("Mar 18", response.timePeriods().get(0).labelsLargeGraph().get(1).label());
		assertEquals(0.16666666666666666, response.timePeriods().get(0).labelsLargeGraph().get(1).position());
		assertEquals("Mar 19", response.timePeriods().get(0).labelsLargeGraph().get(2).label());
		assertEquals(0.33333333333333333, response.timePeriods().get(0).labelsLargeGraph().get(2).position());	
		fail("This seems to look into the actual date");
	}
	
	/**
	 * Checks that a request without timeperiod fails.
	 * 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Send a request without timeperiod.
	 *  <li> Check that it fails and shows some warning.
	 * </ul>
	 */
	public void testSendingARequestWithoutTimePeriodFails() throws Exception {
		scoresRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).
			timePeriod(null).buildScoresRequest();
		assertTextPresent(getResponse(scoresRequest), "Expected a parameter, but found");	
	}

	/**
	 * Checks that sending a request with a non existent time period fails.
	 * 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Send a request with a non existent timeperiod.
	 *  <li> Check that it fails and a proper message is shown.
	 * </ul>
	 */
	public void testSendingARequestWithNonExistentTimePeriodFails() throws Exception {
		scoresRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).
			timePeriod(new Id(312312312)).buildScoresRequest();
		assertTextPresent(getResponse(scoresRequest), "Cannot find timeperiod");			
	}

	/**
	 * Tests that it is not possible to get the request with an unknown company.
	 * 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Sends a request with an unknown company.
	 * 	<li> Checks that the request does not go through and shows a proper message.
	 * </ul>
	 */
	public void testScorecardRequestFailsIfCompanyUnknown() throws Exception {
		scoresRequest = MobileRequestBuilder.getBuilder(
				getExpressEnvironment()).company("unknownCompany").responseText("David")
				.scorecardName("test").timePeriod(new Id(180)).buildScoresRequest();
		assertTextPresent(getResponse(scoresRequest), "Unknown company");
	}

	/**
	 * Tests that it is not possible to get the request with a wrong password.
	 * 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Sends a request with wrong password.
	 * 	<li> Checks that the request fails and shows a proper message.
	 * </ul> 	
	 */
	public void testScorecardRequestFailsIfWrongPassword() throws Exception {
		scoresRequest = MobileRequestBuilder.getBuilder(
				getExpressEnvironment()).password("wrongPassword").responseText("David")
				.scorecardName("test").timePeriod(new Id(180)).buildScoresRequest();
		assertTextPresent(getResponse(scoresRequest), "Not authorized");
	}

	/**
	 * Tests that it is not possible to get the request with a non existent
	 * user.
	 * 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Sends a request with a user that does not exist.
	 * 	<li> Checks that the request fails with a proper message.
	 * </ul>
	 */
	public void testScorecardRequestFailsIfUserDoesNotExist() throws Exception {
		scoresRequest = MobileRequestBuilder.getBuilder(
				getExpressEnvironment()).user("nonExistent").responseText("David")
				.scorecardName("test").timePeriod(new Id(180)).buildScoresRequest();
		assertTextPresent(getResponse(scoresRequest), "Not authorized");
	}

	/**
	 * Tests that it is not possible to get the request with a null user.
	 * 
	 * <p> Steps:
	 * <ul> 
	 * 	<li> Sends a request with a null user.
	 * 	<li> Checks that the request does not go through and shows a proper message.
	 * </ul>
	 */
	public void testScorecardRequestFailsWithNullUser() throws Exception {
		scoresRequest = MobileRequestBuilder.getBuilder(
				getExpressEnvironment()).user(null).scorecardName("test").responseText("David")
				.timePeriod(new Id(180)).buildScoresRequest();
		assertTextPresent(getResponse(scoresRequest), "Not authorized");
	}

	/**
	 * Tests that it is not possible to get the request with a null password.
	 * 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Sends a request with null password.
	 * 	<li> Checks that the request fails and shows a proper message.
	 * </ul> 	
	 */	
	public void testScorecardRequestFailsWithNullPassword() throws Exception {
		scoresRequest = MobileRequestBuilder.getBuilder(
				getExpressEnvironment()).password(null).scorecardName("test").responseText("David")
				.timePeriod(new Id(180)).buildScoresRequest();
		assertTextPresent(getResponse(scoresRequest), "Not authorized");
	}

	/**
	 * Tests that it is not possible to get the request with both a null user
	 * and a null password.
	 * 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Sends a request with both null user and null password.
	 * 	<li> Checks that the request fails and shows a proper message.
	 * </ul>
	 */
	public void testScorecardRequestFailsWithNullPasswordAndNullUser()
			throws Exception {
		scoresRequest = MobileRequestBuilder.getBuilder(
				getExpressEnvironment()).user(null).password(null).responseText("David")
				.timePeriod(new Id(180)).scorecardName("test").buildScoresRequest();
		assertTextPresent(getResponse(scoresRequest), "Not authorized");
	}

	/**
	 * Tests that it is not possible to get the request with a null company
	 * 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Sends a request with a null company.
	 * 	<li> Checks that the request fails and shows a proper message.
	 * </ul>
	 */
	public void testScorecardRequestFailsWithNullCompany() throws Exception {
		scoresRequest = MobileRequestBuilder.getBuilder(
				getExpressEnvironment()).scorecardName("test").company(null).responseText("David")
				.timePeriod(new Id(180)).buildScoresRequest();
		assertTextPresent(getResponse(scoresRequest), "Unknown company");
	}
	
	/**
	 * Tests that it is not possible to get a request with an empty company.
	 *  
	 * <p> Steps:
	 * <ul>
	 * 	<li> Sends a request with an empty company.
	 * 	<li> Checks that the request fails and shows a proper message.
	 * </ul>
	 */
	public void testScorecardRequestFailsWithEmptyCompany() throws Exception {
		scoresRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).responseText("David")
			.company("").scorecardName("test").timePeriod(new Id(180)).buildScoresRequest();
		assertTextPresent(getResponse(scoresRequest), "Unknown company");		
	}

	/**
	 * Tests that it is not possible to get a request with an empty user.
	 *  
	 * <p> Steps:
	 * <ul>
	 * 	<li> Sends a request with an empty user.
	 * 	<li> Checks that the request fails and shows a proper message.
	 * </ul>
	 */
	public void testScorecardRequestFailsWithEmptyUser() throws Exception {
		scoresRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).responseText("David")
			.user("").scorecardName("test").timePeriod(new Id(180)).buildScoresRequest();
		assertTextPresent(getResponse(scoresRequest), "Not authorized");		
	}

	/**
	 * Tests that it is not possible to get a request with an empty password.
	 *  
	 * <p> Steps:
	 * <ul>
	 * 	<li> Sends a request with an empty password.
	 * 	<li> Checks that the request fails and shows a proper message.
	 * </ul>
	 */
	public void testScorecardRequestFailsWithEmptyPassword() throws Exception {
		scoresRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).responseText("David")
			.password("").scorecardName("test").timePeriod(new Id(180)).buildScoresRequest();
		assertTextPresent(getResponse(scoresRequest), "Not authorized");		
	}
}

