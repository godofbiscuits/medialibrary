package com.medallia.mobile.ranker;

import com.medallia.api.orm.Row;
import com.medallia.api.orm.Field.DoubleField;
import com.medallia.api.orm.Field.IntField;

class RankerUG implements Comparable<RankerUG> {
	private String id;
	private String name;
	private int count;
	private double sum;
	private int benchCount;
	private double benchSum;
	
	private final IntField countField;
	private final DoubleField avgField;
	
	public RankerUG(String id, String name, IntField countField, DoubleField avgField) {
		this.id = id;
		this.name = name;
		this.countField = countField;
		this.avgField = avgField;
		count = benchCount = 0;
		sum = benchSum = 0;
	}

	public void addResults(Row results, Row benchResults) {
		if (results != null) {
			int c = results.getInt(countField);
			count += c;
			sum += c*results.getDouble(avgField);
//			System.out.print("for ug "+id+" add results "+c+","+results.getDouble(avgField)+" --> tot = "+count+","+sum);
			
		}
		if (benchResults != null) {
			int c = benchResults.getInt(countField);
			benchCount += c;
			benchSum += c*benchResults.getDouble(avgField);
		}
//		System.out.println();
	}

	public String getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public int getCount() {
		return count;
	}
	
	public double getAvg() {
		return sum / count;
	}

	public double getBenchAvg() {
		return benchSum / benchCount;
	}

	@Override
	public int compareTo(RankerUG otherUG) {
		return Double.compare(otherUG.getAvg(), getAvg());
	}
}