package com.medallia.mobile.test.tests;

import java.util.LinkedHashMap;
import java.util.Map;

import com.medallia.api.datamodel.Timeperiod;
import com.medallia.mobile.dto.ResponsesEntry;
import com.medallia.mobile.dto.ResponsesLump;
import com.medallia.mobile.dto.builders.Build.Constants;
import com.medallia.mobile.filter.UnitFilter;
import com.medallia.mobile.test.framework.MobileTestCase;
import com.medallia.mobile.test.framework.beans.Id;
import com.medallia.mobile.test.framework.config.Environment;
import com.medallia.mobile.test.framework.config.ExpressEnvironment;
import com.medallia.mobile.test.framework.config.MobileEnvironment;
import com.medallia.mobile.test.framework.requests.MobileRequest;
import com.medallia.mobile.test.framework.requests.MobileRequestBuilder;

/**
 * Scenario Suite that contains severeal tests the exercises the responses request
 * 
 * @author fernando
 */
@Environment(express=ExpressEnvironment.EARTH, mobile=MobileEnvironment.LOCALHOST)
public class LocalResponsesRequestTest extends MobileTestCase {

	private MobileRequest responsesRequest;
	
	/**
	 * Checks that the number of surveys returned corresponds to the actual number of surveys that
	 * is present in the system.
	 * 
	 * <p> Preconditions
	 * <ul>
	 * 	<li> Timeperiod 180 corresponds to Year 09.
	 *  <li> For period 180, there are 154 surveys.
	 * </ul>
	 * 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Gets the number of Surveys for the period 180 (year 09).
	 * 	<li> Gets the first response, asserts that it has 30 and also that the fetchMoreURL
	 *       contains the right offset.
	 * 	<li> Gets the second response, asserts that it has 30 and also that the fetchMoreURL
	 * 		 contains the right offset.         
	 * 	<li> Gets the third response, asserts that it has 30 and also that the fetchMoreURL
	 *       contains the right offset.
	 * 	<li> Gets the fourth response, asserts that it has 30 and also that the fetchMoreURL
	 *       contains the right offset.
  	 * 	<li> Gets the last response, asserts that it has 34 and also that the fetchMoreURL is null
  	 *       since there are no more surveys.
  	 *  <li> Double checks that the total amount of surveys is 154.
	 * </ul>
	 */
	public void testNumberOfResponsesIsCorrect() throws Exception {
		
		int numberResponses = getQuerier().getNumberOfSurveys(
			new Timeperiod("180", "", null, null, 0));
		
		responsesRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment())
			.timePeriod(new Id(180)).buildResponsesRequest();
		ResponsesLump response = getResponsesResponse(responsesRequest);
		assertEquals(30, response.entries().size());
		assertTrue(response.fetchMoreURL().contains("offset=30"));
		numberResponses -= response.entries().size();
		
		responsesRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment())
			.timePeriod(new Id(180)).offset(30).buildResponsesRequest();
		response = getResponsesResponse(responsesRequest);
		assertEquals(30, response.entries().size());
		assertTrue(response.fetchMoreURL().contains("offset=60"));
		numberResponses -= response.entries().size();
		
		responsesRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment())
			.timePeriod(new Id(180)).offset(60).buildResponsesRequest();
		response = getResponsesResponse(responsesRequest);
		assertEquals(30, response.entries().size());
		assertTrue(response.fetchMoreURL().contains("offset=90"));
		numberResponses -= response.entries().size();
	
		responsesRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment())
			.timePeriod(new Id(180)).offset(90).buildResponsesRequest();
		response = getResponsesResponse(responsesRequest);
		assertEquals(30, response.entries().size());
		assertTrue(response.fetchMoreURL().contains("offset=120"));
		numberResponses -= response.entries().size();
		
		responsesRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment())
			.timePeriod(new Id(180)).offset(120).buildResponsesRequest();
		response = getResponsesResponse(responsesRequest);
		assertEquals(34, response.entries().size());
		assertEquals(response.fetchMoreURL(), null);
		numberResponses -= response.entries().size();
		assertEquals(0, numberResponses);
	}
	
	/**
	 * Checks that the Responses Entry contains the right info of the surveys.
	 * 
	 * <p> Preconditions:
	 * <ul>
	 * 	<li> Timeperiod 180 corresponds to Year 09.
	 *  <li> The first response is the response with id 68. It is from Oslo unit, made on the
	 *       2009-06-21, the customer is Kelly Wayne, it has two comments, and no alert.
	 *  <li> The second response is the response with id 18, and has an A1 alert.
	 * </ul>
	 * 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Gets the responses from the period Year 09.
	 *  <li> Gets the first response, and asserts all the fields, including name, score, header, subheader
	 *       comments and asserts that no alert is present.
	 *  <li> Gets the second response, which has an alert, and checks that the alert is present.
	 * </ul>
	 */
	public void testSurveyContainsRightInfo() throws Exception {
		responsesRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).timePeriod(new Id(180))
			.buildResponsesRequest();
		ResponsesLump response = getResponsesResponse(responsesRequest);
		
		ResponsesEntry survey = response.entries().get(0);
		assertEquals("Wayne Kelly", survey.name());
		assertEquals("10", survey.score());
		assertEquals("Jun 21", survey.sectionHeader());
		assertEquals("Oslo", survey.subheader());
		assertTrue(survey.text().contains("We have many meetings at this hotel"));
		assertTrue(survey.text().contains("Safe not working "));
		assertEquals(null,survey.alert());
		
		survey = response.entries().get(1);
		assertEquals("A1", survey.alert());	
	}
	
	/**
 	 * Filters by unit and asserts that only the correct entries are obtained.
	 * 
	 * <p> Preconditions:
	 * <ul>
	 * 	<li> Timeperiod 180 corresponds to Year 09.
	 *  <li> Oslo unit group has 49 surveys for that year (there are actually 50, but one is excluded).
	 * </ul>
	 * 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Send a request filtering by unit=oslo.
	 *  <li> Check that the first response has 30 entries, and all of them are from Oslo unit group.
	 *  <li> Send a second request filtering by unit=oslo, but with an offset of 30.
	 *  <li> Check that the second response contains the remaining 19 enries.
	 *  <li> Assert that the total number of surveys is 49.
	 * </ul>
	 */
	public void testFilterByUnitIdWorks() throws Exception {
		responsesRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).timePeriod(new Id(180)).unit(UnitFilter.UNIT_PREFIX + "oslo")
			.buildResponsesRequest();
		ResponsesLump response = getResponsesResponse(responsesRequest);
		Map<String,String> filters = new LinkedHashMap<String, String>();
		filters.put("unitid", "oslo");
		int numberSurveys = getQuerier().getNumberOfSurveys(new Timeperiod("180", "", null, null, 0), filters);
		assertEquals(30, response.entries().size());
		numberSurveys -= response.entries().size();
		assertTrue(response.fetchMoreURL().contains("offset=30"));
		for(ResponsesEntry entry : response.entries()) {
			assertEquals("Oslo", entry.subheader());
		}
		
		responsesRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).timePeriod(new Id(180))
			.offset(30).unit(UnitFilter.UNIT_PREFIX + "oslo").buildResponsesRequest();
		response = getResponsesResponse(responsesRequest);
		for(ResponsesEntry entry : response.entries()) {
			assertEquals("Oslo", entry.subheader());
		}
		assertEquals(19, response.entries().size());
		numberSurveys -= response.entries().size();
		assertEquals(0, numberSurveys);
	}
	
	/**
 	 * Filters by satisfaction range and asserts that only the correct entries are obtained.
	 * 
	 * <p> Preconditions:
	 * <ul>
	 * 	<li> Timeperiod 180 corresponds to Year 09.
	 *  <li> There are 46 surveys with a satisfaction range between 9 and 10 in that year.
	 * </ul>
	 * 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Send a request filtering by satrange between 9 and 10.
	 *  <li> Check that the first response has 30 entries, and all of them have that satisfaction range.
	 *  <li> Send a second request filtering by the same satrange, but with an offset of 30.
	 *  <li> Check that the second response contains the remaining 16 enries.
	 * </ul>
	 */
	public void testFilterBySatRangeWorks() throws Exception {
		responsesRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).timePeriod(new Id(180)).satRange("9.000000:10.000000")
			.buildResponsesRequest();
		ResponsesLump response = getResponsesResponse(responsesRequest);
		assertEquals(30, response.entries().size());
		assertTrue(response.fetchMoreURL().contains("offset=30"));
		for(ResponsesEntry entry : response.entries()) {
			assertTrue(Integer.valueOf(entry.score()) >= 9);
			assertTrue(Integer.valueOf(entry.score()) <= 10);
		}
		responsesRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).timePeriod(new Id(180)).satRange("9.000000:10.000000")
			.offset(30).buildResponsesRequest();
		response = getResponsesResponse(responsesRequest);
		assertEquals(16, response.entries().size());
		assertEquals(null, response.fetchMoreURL());
		for(ResponsesEntry entry : response.entries()) {
			assertTrue(Integer.valueOf(entry.score()) >= 9);
			assertTrue(Integer.valueOf(entry.score()) <= 10);
		}		
	}
	
	/**
 	 * Check that the alerts only parameter works.
	 * 
	 * <p> Preconditions:
	 * <ul>
	 * 	<li> Timeperiod 180 corresponds to Year 09.
	 *  <li> There are 4  surveys with an alert.
	 * </ul>
	 * 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Send a request filtering by alerts_only parameter
	 *  <li> Check that the first response has the 4 entries, and all of them have an alert.
	 * </ul>
	 */
	public void testFilterByAlertOnlyWorks() throws Exception {
		responsesRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).timePeriod(new Id(180)).alertsOnly(true)
			.buildResponsesRequest();
		ResponsesLump response = getResponsesResponse(responsesRequest);
		assertEquals(4, response.entries().size());
		for(ResponsesEntry entry : response.entries()) {
			assertTrue(entry.alert() != null);
			assertFalse(entry.alert().equals(""));
		}
	}
	
	/**
 	 * Check that the comments only parameter works
	 * 
	 * <p> Preconditions:
	 * <ul>
	 * 	<li> Timeperiod 180 corresponds to Year 09.
	 *  <li> There are 151  surveys with a comments.
	 * </ul>
	 * 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Send a request filtering by comments_only parameter
	 *  <li> Check that the first response has the 30 entries
	 *  <li> Sends a request with offset 120, and checks that the response contains 31.
	 * </ul>
	 */
	public void testFilterCommentsOnlyWorks() throws Exception {
		responsesRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).timePeriod(new Id(180)).commentsOnly(true)
			.buildResponsesRequest();
		ResponsesLump response = getResponsesResponse(responsesRequest);
		assertEquals(30, response.entries().size());
	
		responsesRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).timePeriod(new Id(180)).commentsOnly(true)
			.offset(120).buildResponsesRequest();
		response = getResponsesResponse(responsesRequest);
		assertEquals(31, response.entries().size());
	}
	
	/**
 	 * Check that the response text parameter works.
	 * 
	 * <p> Preconditions:
	 * <ul>
	 * 	<li> Timeperiod 180 corresponds to Year 09.
	 *  <li> There are 2 surveys containing 'David'.
	 *  <li> There are 2 surveys containing 'help'.
	 * </ul>
	 * 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Send a request filtering with response_text = 'David'
	 *  <li> Check that the response has only 2 entries and the 2 entries contains the text 'David'.
	 *  <li> Sends a request with response_text = 'help'.
	 *  <li> Checks that the response has 3 entries and those 2 contains the text 'help'.
	 * </ul>
	 */
	public void testFilterResponseTextWorks() throws Exception {
		responsesRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).timePeriod(new Id(180)).responseText("David")
			.buildResponsesRequest();
		ResponsesLump response = getResponsesResponse(responsesRequest);
		assertEquals(2, response.entries().size());
		assertTrue(response.entries().get(0).name().contains("David"));
		assertTrue(response.entries().get(1).name().contains("David"));
		
		responsesRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).timePeriod(new Id(180)).responseText("help")
			.buildResponsesRequest();
		response = getResponsesResponse(responsesRequest);
		assertEquals(3, response.entries().size());
		assertTrue(response.entries().get(0).text().toLowerCase().contains("help"));
		assertTrue(response.entries().get(1).text().toLowerCase().contains("help"));
		assertTrue(response.entries().get(2).text().toLowerCase().contains("help"));
	}
	
	/**
 	 * Sets several filters and verifies that the response is correct.
 	 * 
	 * <p> Preconditions:
	 * <ul>
	 * 	<li> Timeperiod 180 corresponds to Year 09.
	 *  <li> There is only 1 survey that matches all this criteria:
	 *       - Unit: Oslo.
	 *       - Range: 9-10.
	 *       - Response Text: 'help'.
	 *       - Contains a comment.
	 * </ul>
	 * 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Send a request filtering by: unit Oslo, range 9-10, contains 'help' and has a comment. 
	 *  <li> Check that the response contains only 1 survey.
	 *  <li> Check that the survey is the correct one, checking that the last name is Washington.
	 * </ul>
	 */
	public void testMixingFiltersWorks() throws Exception {
		responsesRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).timePeriod(new Id(180)).responseText("help")
			.satRange("9.000000:10.000000").unit(UnitFilter.UNIT_PREFIX + "oslo").commentsOnly(true).buildResponsesRequest();
		ResponsesLump response = getResponsesResponse(responsesRequest);
		assertEquals(1, response.entries().size());
		assertTrue(response.entries().get(0).name().contains("Washington"));
	}
	
	/**
 	 * Sets several filters until no survey matches the request and check that the response
 	 * does no contain surveys.
 	 * 
	 * <p> Preconditions:
	 * <ul>
	 * 	<li> Timeperiod 180 corresponds to Year 09.
	 *  <li> There is only 1 survey that matches all this criteria:
	 *       - Unit: Oslo.
	 *       - Range: 9-10.
	 *       - Response Text: 'NotPossibleToBeThere'.
	 *       - Contains a comment.
	 * </ul>
	 * 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Send a request filtering by: unit Oslo, range 9-10, contains 'NotPossibleToBeThere'
	 *       and has a comment. 
	 *  <li> Check that the response does not contain surveys.
	 * </ul>
	 */
	public void testFilteringUntilNoSurveyMatchesCriteriaWorks() throws Exception {
		responsesRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).timePeriod(new Id(180))
			.responseText("NotPossibleToBeThere").satRange("9.000000:10.000000")
			.unit(UnitFilter.UNIT_PREFIX + "oslo").commentsOnly(true).buildResponsesRequest();
		ResponsesLump response = getResponsesResponse(responsesRequest);
		assertEquals(0, response.entries().size());
	}
	
	/**
	 * Checks that is not possible to get a request without a time period.
	 * 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Sends a request without a timeperiod.
	 *  <li> Asserts thta a proper message is shown.
	 * </ul>
	 */
	public void testResponsesRequestWithoutATimePeriodFails() {
		responsesRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).buildResponsesRequest();
		String response = getResponse(responsesRequest);
		fail("this one shows not a nice message, would be better if says something like \"time period not set\" (it says:" + response + ")");
	}
	
	/**
	 * Checks that a request with a non existent time period fails.
	 * 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Sends a request with a non existent timeperiod.
	 *  <li> Asserts that it fails and a proper message is sent.
	 * </ul>
	 */
	public void testResponsesRequestWithWrongTimePeriodFails() {
		responsesRequest = MobileRequestBuilder.getBuilder(
				getExpressEnvironment()).timePeriod(new Id(43124)).buildResponsesRequest();
		assertTrue(getResponse(responsesRequest)
				.contains("Cannot find timeperiod for the identifier: 43124"));
	}
	
	/**
	 * Tests that it is not possible to login with an unknown company. 
	 * 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Sends a request with an unknown company.
	 * 	<li> Checks that the request does not go through and shows a proper message.
	 * </ul>
	 */
	public void testresponsesRequestFailsIfCompanyUnknown() throws Exception {
		responsesRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).company("unknownCompany")
			.version(Constants.FIRST_COMPATIBLE_CLIENT_VERSION).buildResponsesRequest();
		assertTextPresent(getResponse(responsesRequest), "Unknown company");
	}
	
	/**
	 * Tests that it is not possible to login with a wrong password.
	 * 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Sends a request with wrong password.
	 * 	<li> Checks that the request fails and shows a proper message.
	 * </ul> 	
	 */ 
	public void testresponsesRequestFailsIfWrongPassword() throws Exception {
		responsesRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).password("wrongPassword")
			.version(Constants.FIRST_COMPATIBLE_CLIENT_VERSION).buildResponsesRequest();
		assertTextPresent(getResponse(responsesRequest), "Not authorized");
	}
	
	/**
	 * Tests that it is not possible to login with a non existent user. 
	 * 	 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Sends a request with a user that does not exist.
	 * 	<li> Checks that the request fails with a proper message.
	 * </ul>
	 */
	public void testresponsesRequestFailsIfUserDoesNotExist() throws Exception {
		responsesRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).user("nonExistentUser")
		.version(Constants.FIRST_COMPATIBLE_CLIENT_VERSION).buildResponsesRequest();
		assertTextPresent(getResponse(responsesRequest), "Not authorized");
	}
	
	/**
	 * Tests that it is not possible to login with a null user. 
	 * 
	 * <p> Steps:
	 * <ul> 
	 * 	<li> Sends a request with a null user.
	 * 	<li> Checks that the request does not go through and shows a proper message.
	 * </ul>
	 */
	public void testresponsesRequestFailsWithNullUser() throws Exception {
		responsesRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).user(null)
			.version(Constants.FIRST_COMPATIBLE_CLIENT_VERSION).buildResponsesRequest();
		assertTextPresent(getResponse(responsesRequest), "Not authorized");
	}
	
	/**
	 * Tests that it is not possible to login with a null password. 
	 * 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Sends a request with null password.
	 * 	<li> Checks that the request fails and shows a proper message.
	 * </ul> 
	 */	
	public void testresponsesRequestFailsWithNullPassword() throws Exception {
		responsesRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).password(null)
			.version(Constants.FIRST_COMPATIBLE_CLIENT_VERSION).buildResponsesRequest();
		assertTextPresent(getResponse(responsesRequest), "Not authorized");
	}

	/**
	 * Tests that it is not possible to login with both a null user and a null password.
	 * 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Sends a request with both null user and null password.
	 * 	<li> Checks that the request fails and shows a proper message.
	 * </ul>
	 */
	public void testresponsesRequestFailsWithNullPasswordAndNullUser() throws Exception {
		responsesRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).password(null).user(null)
			.version(Constants.FIRST_COMPATIBLE_CLIENT_VERSION).buildResponsesRequest();
		assertTextPresent(getResponse(responsesRequest), "Not authorized");
	}
	
	/**
	 * Tests that it is not possible to login with a null company 
	 *
	 * <p> Steps:
	 * <ul>
	 * 	<li> Sends a request with a null company.
	 * 	<li> Checks that the request fails and shows a proper message.
	 * </ul>
	 */
	public void testresponsesRequestFailsWithNullCompany() throws Exception {
		responsesRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).company(null)
			.version(Constants.FIRST_COMPATIBLE_CLIENT_VERSION).buildResponsesRequest();
		assertTextPresent(getResponse(responsesRequest), "Unknown company");
	}
}
