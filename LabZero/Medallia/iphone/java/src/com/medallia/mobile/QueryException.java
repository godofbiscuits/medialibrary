package com.medallia.mobile;

public class QueryException extends RuntimeException {

	public final Number resCode;
	public final String error;
	
	public QueryException(Number resCode, String error) {
		super(error);
		this.resCode = resCode;
		this.error = error;
	}
}
