package com.medallia.mobile.test.framework.requests.impl;

import junit.framework.TestCase;

import com.medallia.mobile.test.framework.requests.MobileRequest;

/**
 * Test suite for the {@link MobileRequestImpl}.
 * 
 * @author fernando
 */
public class MobileRequestImplTest extends TestCase {

	/**
	 * Tests that a simple request is properly created.
	 */
	public void testSimpleMobileRequestIsProperlyCreated() throws Exception {
		MobileRequest mobileRequest = new MobileRequestImpl("user", "password", "company", "request");
		assertEquals("user", mobileRequest.getUser());
		assertEquals("password", mobileRequest.getPassword());
		assertEquals("company", mobileRequest.getCompany());
		assertEquals("req=request&c=company&u=user&p=password", mobileRequest.getRequest());
	}
	
	/**
	 * Tests that extra parameters are added properly.
	 */
	public void testExtraParametersAreAdded() throws Exception {
		MobileRequest mobileRequest = new MobileRequestImpl("user", "password", "company", "request");
		mobileRequest.addParameter("param1", "param1");
		mobileRequest.addParameter("param2", "param2");
		assertTrue(mobileRequest.getRequest().contains("&param1=param1"));		
		assertTrue(mobileRequest.getRequest().contains("&param2=param2"));		
	}
}
