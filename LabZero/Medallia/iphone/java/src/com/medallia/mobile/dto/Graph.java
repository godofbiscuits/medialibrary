package com.medallia.mobile.dto;
import java.util.*;
import com.medallia.mobile.*;
import com.medallia.mobile.dto.*;
import com.medallia.mobile.dto.builders.*;
import com.medallia.mobile.color.UIColor;

// These files are generated automatically from declarations in DataFormat.h; see convert.pl for details.
//
// Do *not* modify this file directly!


/**
 *  A graph. Contains data for a list of lines.
 */
@SuppressWarnings("unused")
public interface Graph extends Plist.Plistable {

	/**
	 *  Suggested graph y range minimum (will be overridden if data lower than this appear).
	 */
	String minY();

	/**
	 *  Suggested graph y range maximum (will be overridden if data higher than this appear).
	 */
	String maxY();

	/**
	 *  Data for a list of lines. There will be as many lines as line styles defined in the SWPage.
	 */
	@Required List<LineData> lineData();


}
