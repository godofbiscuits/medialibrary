package com.medallia.mobile.dto;
import java.util.*;
import com.medallia.mobile.*;
import com.medallia.mobile.dto.*;
import com.medallia.mobile.dto.builders.*;
import com.medallia.mobile.color.UIColor;

// These files are generated automatically from declarations in DataFormat.h; see convert.pl for details.
//
// Do *not* modify this file directly!


/**
 *  One table cell on the settings page. Can be a CECheckbox, CEDropdown or CERange (the double slider).
 */
@SuppressWarnings("unused")
public interface ConfigPart extends Plist.Plistable {

	/**
	 *  the type of control
	 */
	ConfigPartType type();

	/**
	 *  which clickable header element this configpart is associated with
	 */
	ClickableHeaderElement headerElement();

	/**
	 *  "ev_filter_foo" (the server's name for this field)
	 */
	@Required String key();

	/**
	 *  name shown ("Property")
	 */
	String name();

	/**
	 *  default value
	 */
	String def();

	/**
	 *  for CEDropdown, the name of the AltTable to use
	 */
	String table();

	/**
	 *  for CERange
	 */
	double minValue();

	/**
	 *  for CERange
	 */
	double maxValue();


}
