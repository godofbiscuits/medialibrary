package com.medallia.mobile.dto;
import java.util.*;
import com.medallia.mobile.*;
import com.medallia.mobile.dto.*;
import com.medallia.mobile.dto.builders.*;
import com.medallia.mobile.color.UIColor;

// These files are generated automatically from declarations in DataFormat.h; see convert.pl for details.
//
// Do *not* modify this file directly!


/**
 *  Line data. Contains a list of data points for a single line.
 */
@SuppressWarnings("unused")
public interface LineData extends Plist.Plistable {

	/**
	 *  List of pipe-separated x values for the line, ordered and ranged between 0 and 1 (inclusive). May be empty if there is no data. Leading zeros are omitted, e.g. '0.25' is written as '.25', but a single '0' is still written as '0'. Each x point has a corresponding y point.
	 */
	@Required String xData();

	/**
	 *  List of pipe-separated y values for the line. May be empty if there is no data. Need not conform to the range given by [minY, maxY]. Leading zeros are omitted, e.g. '0.25' is written as '.25', but a single '0' is still written as '0'. Each y point has a corresponding x point.
	 */
	@Required String yData();


}
