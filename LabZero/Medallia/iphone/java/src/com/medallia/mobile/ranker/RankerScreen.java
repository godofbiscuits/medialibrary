package com.medallia.mobile.ranker;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.medallia.api.datamodel.SurveyAggregate;
import com.medallia.api.datamodel.SurveyAggregateQuery;
import com.medallia.api.datamodel.Unit;
import com.medallia.api.datamodel.UnitGroup;
import com.medallia.api.datamodel.UnitGroupMembership;
import com.medallia.api.orm.Row;
import com.medallia.api.orm.SelectQuery;
import com.medallia.api.orm.Field.DoubleField;
import com.medallia.api.orm.Field.IntField;
import com.medallia.mobile.ApiUtil;
import com.medallia.mobile.BenchmarkTimeperiod;
import com.medallia.mobile.FieldWithColor;
import com.medallia.mobile.RequestParameterMap;
import com.medallia.mobile.ScoreFormatter;
import com.medallia.mobile.RequestParameterMap.Param;
import com.medallia.mobile.companysettings.CompanySettings;
import com.medallia.mobile.dto.RankerData;
import com.medallia.mobile.dto.RankerUnit;
import com.medallia.mobile.dto.builders.Build;
import com.medallia.mobile.dto.builders.Build.RankerUnitBuilder;
import com.medallia.mobile.filter.DashboardFilter;
import com.medallia.mobile.filter.UnitFilter;

public class RankerScreen {
	private final CompanySettings config;
	private final RequestParameterMap req;

	private final FieldWithColor rankerField;
	private final DoubleField rankerFieldAvg;
	private final IntField rankerFieldCount;
	
	public RankerScreen(CompanySettings config, RequestParameterMap req) {
		this.config = config;
		this.req = req;
		rankerField = config.getRankerFieldByKey(req.get(Param.rankerq));
		rankerFieldAvg = rankerField.avg();
		rankerFieldCount = rankerField.count();
	}

	/** @return Benchmark time period, or null of benchmarking is not requested. */
	private BenchmarkTimeperiod applyRankerQuery(SurveyAggregateQuery query, boolean benchmark) {
		BenchmarkTimeperiod benchPeriod = null;
		if (benchmark) {
			benchPeriod = ApiUtil.whereBenchTimePeriod(query, config, req);
			if (!benchPeriod.usesBenchmark())
				return benchPeriod; // no benchmark used -- no need to continue
		} else {
			ApiUtil.whereTimePeriod(query, req);
		}
		
		query.setExtended(true);
		query.addField(Unit.UNITID);
		query.addField(rankerFieldCount);
		query.addField(rankerFieldAvg);
		query.setOrderBy(rankerFieldAvg.key + " DESC");
		for (DashboardFilter filter : config.getRankerFilters())
			filter.addFilter(req, query);
		if (rankerField.filterUnitGroup != null)
			query.addCondition("group = '" + rankerField.filterUnitGroup.id + "'");
		
		return benchPeriod;
	}
	
	/**
	 * @return Map of unit names for unit ids for all ACTIVE units. So in addition to providing names, this map
	 * may be used to see if a unit id returned from another query represents an active unit.
	 */
	private Map<String, String> getUnitNameByIdMap() {
		// there is no filter support (WHERE) on the unit table, so we need to load all of them 
		// XXX this could be a performance issue if the number of units is large and the query is done frequently
		List<Unit> units = Unit.getUnits(config.getConnection(), true);
		
		Map<String, String> unitNameById = new HashMap<String, String>();
		for (Unit unit : units)
			unitNameById.put(unit.getId(), unit.getName());
		
		return unitNameById;
	}
	
	/** @return Set of units included in the filter unit group. */
	private Set<String> getIncludedUnitIds(Collection<UnitGroupMembership> memberships, String unitFilterId) {
		Set<String> includedUnitIds = new HashSet<String>();
		assert unitFilterId != null && unitFilterId.startsWith(UnitFilter.UNIT_GROUP_PREFIX) : unitFilterId;
		String filterUgId = unitFilterId.substring(UnitFilter.UNIT_GROUP_PREFIX.length());
		for (UnitGroupMembership membership : memberships)
			if (membership.unitgroupid.equals(filterUgId))
				includedUnitIds.add(membership.unitid);
		return includedUnitIds;
	}
	
	/**
	 * @param rankerBenchTable List of benchmark results.
	 * @param includedUnitIds Set of units to include. Null to include all.
	 * @return Table results mapped by unit id.
	 */
	private Map<String, Row> getResultsByUnitId(List<SurveyAggregate> rankerBenchTable, Set<String> includedUnitIds) {
		Map<String, Row> resultsByUnitId = new HashMap<String, Row>();
		for (Row resultRow : rankerBenchTable) {
			String unitId = resultRow.getString(Unit.UNITID);
			if (includedUnitIds == null || includedUnitIds.contains(unitId))
				resultsByUnitId.put(unitId, resultRow);
		}
		return resultsByUnitId;
	}

	private RankerUnit buildRankerUnit(String id, String name, ScoreFormatter sf, boolean fullAccess) {
		RankerUnitBuilder rub = Build.rankerUnit()
			.uniqueIdentifier(id)
			.name(name)
			.score(sf.getScoreText())
			.scoreNumeric(sf.getScore())
			.difference(sf.getDiffText())
			.differenceNumeric(sf.getDiff())
			.diffColor(sf.getDiffColor());
		if (fullAccess)
			rub.selected("1"); // in order to highlight units that the user has access to
		return rub.get();
	}
	
	/** @return Unit ids for units that the user has full access to. Include inactive units if so specified. */
	private Set<String> getFullAccessUnitIds(boolean includeInactive) {
		Set<String> fullAccessUnitIds = new HashSet<String>();
		for (Unit unit : Unit.getUnits(config.getConnection(), false, includeInactive))
			fullAccessUnitIds.add(unit.getId());
		return fullAccessUnitIds;
	}
	
	/** @return Final list of units to rank. */
	public List<RankerUnit> getRankerUnits(List<SurveyAggregate> rankerResultsTable, Set<String> includedUnitIds, Map<String, Row> benchResultsByUnitId, BenchmarkTimeperiod benchPeriod) {
		Set<String> fullAccessUnitIds = getFullAccessUnitIds(false);
		List<RankerUnit> rankerUnits = new ArrayList<RankerUnit>();
		Map<String, String> unitNameById = getUnitNameByIdMap();
		for (Row rankerRow : rankerResultsTable) {
			String rankerUnitId = rankerRow.getString(Unit.UNITID);
			if ((includedUnitIds == null || includedUnitIds.contains(rankerUnitId)) && unitNameById.containsKey(rankerUnitId)) {
				Row benchRow = benchResultsByUnitId.get(rankerUnitId);
				// the row may be null if there were no results in the benchmark period
				double benchScore = benchRow == null ? Double.NaN : benchRow.getDouble(rankerFieldAvg);
				ScoreFormatter sf = new ScoreFormatter(config, rankerRow.getDouble(rankerFieldAvg), benchScore, benchPeriod.usesBenchmark());
				rankerUnits.add(buildRankerUnit(UnitFilter.UNIT_PREFIX + rankerUnitId, unitNameById.get(rankerUnitId), sf, fullAccessUnitIds.contains(rankerUnitId)));
			}
		}
		return rankerUnits;
	}
	
	/** @return Final list of unit groups to rank. */
	public List<RankerUnit> getRankerUnitGroups(String scope, List<SurveyAggregate> rankerResultsTable, Collection<UnitGroupMembership> ugMemberships,
			Set<String> includedUnitIds, Map<String, Row> benchResultsByUnitId, BenchmarkTimeperiod benchPeriod) {
		// first find which unit groups are included in the scope
		assert scope != null && scope.startsWith(UnitFilter.UNIT_GROUP_PREFIX) : scope;
		String scopeUgId = scope.substring(UnitFilter.UNIT_GROUP_PREFIX.length());

		List<UnitGroup> unitGroups = config.getUnitGroupsWithFilteredName(true);
		
		Map<String, RankerUG> rankerUGById = new HashMap<String, RankerUG>();
		for (UnitGroup ugRow : unitGroups) {
			if (ugRow.parent_unitgroupid.equals(scopeUgId)) {
				String id = ugRow.unitgroupid;
				rankerUGById.put(id, new RankerUG(id, ugRow.name, rankerFieldCount, rankerFieldAvg));
			}
		}

		// map results by unit id, only including units matching the unit filter
		Map<String, Row> resultsByUnitId = getResultsByUnitId(rankerResultsTable, includedUnitIds);
		
		// now calculate the scores for the unit groups by aggregating scores from its units
		Map<String, List<String>> unitsByUG = new HashMap<String, List<String>>();
		if (ugMemberships != null) {
			for (UnitGroupMembership membership : ugMemberships) {
				String ugId = membership.unitgroupid;
				RankerUG ug = rankerUGById.get(ugId);
				if (ug != null) {
					String unitId = membership.unitid;
					ug.addResults(resultsByUnitId.get(unitId), benchResultsByUnitId.get(unitId));
					
					List<String> units = unitsByUG.get(ugId);
					if (units == null)
						unitsByUG.put(ugId, units = new ArrayList<String>());
					units.add(unitId);
				}
			}
		}
		
		// full access units; if the user has full access to ALL units of the ug, the ug is marked "own"
		// include inactive units here since these are included in the membership table
		Set<String> fullAccessUnitIds = getFullAccessUnitIds(true);
		
		// sort unit groups and build ranker unit objects out of them
		List<RankerUnit> rankerUGs = new ArrayList<RankerUnit>();
		List<RankerUG> sortedUGs = new ArrayList<RankerUG>(rankerUGById.values());
		Collections.sort(sortedUGs);
		for (RankerUG ug : sortedUGs) {
			if (ug.getCount() > 0) {
				boolean fullAccess = true;
				for (String unitId : unitsByUG.get(ug.getId()))
					if (!(fullAccess = fullAccessUnitIds.contains(unitId)))
						break;
				ScoreFormatter sf = new ScoreFormatter(config, ug.getAvg(), ug.getBenchAvg(), benchPeriod.usesBenchmark());
				rankerUGs.add(buildRankerUnit(UnitFilter.UNIT_GROUP_PREFIX + ug.getId(), ug.getName(), sf, fullAccess));
			}
		}
		
		return rankerUGs;
	}
	
	public RankerData get() {
		String scope = req.get(Param.scope);
		String unitFilterId = req.get(Param.unitid);

		boolean listUnits = UnitFilter.UNIT_PARENT_ID.equals(scope);
		boolean filter = unitFilterId != null && !"all".equals(unitFilterId);
		
		// note: no need to query membership info if listing units and there's no filter
		Collection<UnitGroupMembership> memberships = null;
		if (filter || !listUnits) {
			SelectQuery<UnitGroupMembership> query = UnitGroupMembership.getUnitGroupMembershipQuery();
			query.setExtended(true);
			memberships = query.run(config.getConnection());
		}
		
		// set of units included in the filter unit group (null if no filter -- include all units)
		Set<String> includedUnitIds = filter ? getIncludedUnitIds(memberships, unitFilterId) : null;
		
		// ranker results per unit
		SurveyAggregateQuery query = new SurveyAggregateQuery();
		applyRankerQuery(query, false);
		List<SurveyAggregate> rankerResultsTable = query.run(config.getConnection());
		
		// map bench results by unit id, only including units matching the unit filter -- don't run the bench query if benchmark is not used for the time period
		SurveyAggregateQuery benchQuery = new SurveyAggregateQuery();
		BenchmarkTimeperiod benchPeriod = applyRankerQuery(benchQuery, true);
		List<SurveyAggregate> rankerBenchTable = benchPeriod.usesBenchmark() ? benchQuery.run(config.getConnection()) : rankerResultsTable;
		Map<String, Row> benchResultsByUnitId = getResultsByUnitId(rankerBenchTable, includedUnitIds);
		
		List<RankerUnit> rankerUnits = listUnits ? getRankerUnits(rankerResultsTable, includedUnitIds, benchResultsByUnitId, benchPeriod) :
			getRankerUnitGroups(scope, rankerResultsTable, memberships, includedUnitIds, benchResultsByUnitId, benchPeriod);

		return Build.rankerData().benchTitle(benchPeriod.getShortName()).entries(rankerUnits).get();
	}
}
