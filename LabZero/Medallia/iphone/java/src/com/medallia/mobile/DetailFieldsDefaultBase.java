package com.medallia.mobile;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import tiny.Empty;

import com.medallia.api.datamodel.Survey;
import com.medallia.api.orm.Field;
import com.medallia.api.orm.Field.StringField;

/**
 * Overrides many of the abstract methods from DetailFields
 * returning default values that will work for most companies.
 */
public abstract class DetailFieldsDefaultBase extends DetailFields {

	@Override
	public StringField getCommentField() {
		return Survey.COMMENTS;
	}

	@Override
	public List<StringField> getCommentFields() {
		return Arrays.asList(getCommentField());
	}

	@Override
	public List<StringField> getNameFields() {
		return Arrays.asList(Survey.firstname, Survey.lastname);
	}

	@Override
	public List<Field> getParticipantFields() {
		List<Field> list = new ArrayList<Field>();
		list.add(Survey.fullname);
		list.add(Survey.RESPONSEDATE);
		StringField phoneField = getPhoneField();
		if (phoneField != null) list.add(phoneField);
		return list;
	}

	@Override
	public List<FieldAndAction> getActionFields() {
		return Empty.list();
	}

	@Override
	public StringField getPhoneField() {
		return Survey.phone;
	}

	@Override
	public boolean showAlert() {
		return true;
	}

}
