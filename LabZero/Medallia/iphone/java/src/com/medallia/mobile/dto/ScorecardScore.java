package com.medallia.mobile.dto;
import java.util.*;
import com.medallia.mobile.*;
import com.medallia.mobile.dto.*;
import com.medallia.mobile.dto.builders.*;
import com.medallia.mobile.color.UIColor;

// These files are generated automatically from declarations in DataFormat.h; see convert.pl for details.
//
// Do *not* modify this file directly!


/**
 *  An individual score shown on the scorecard.
 */
@SuppressWarnings("unused")
public interface ScorecardScore extends Plist.Plistable {

	/**
	 *  Score title, e.g. "Overall satisfaction".
	 */
	@Required String title();

	/**
	 *  Score, e.g. "8.4". It's a string so that it can be customized and formatted on the server side.
	 */
	@Required String value();

	/**
	 *  The color for the score background; "green"/"yellow"/"red" for now
	 */
	String scoreBackgroundColor();


}
