package com.medallia.mobile.responses;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.medallia.api.datamodel.Survey;
import com.medallia.api.datamodel.SurveyQuery;
import com.medallia.api.orm.Row;
import com.medallia.api.orm.Field.StringField;
import com.medallia.mobile.ApiUtil;
import com.medallia.mobile.Common;
import com.medallia.mobile.FieldWithColor;
import com.medallia.mobile.RequestParameterMap;
import com.medallia.mobile.RequestParameterMap.LogonInfo;
import com.medallia.mobile.RequestParameterMap.Param;
import com.medallia.mobile.color.BackgroundColorHelper;
import com.medallia.mobile.color.UIColor;
import com.medallia.mobile.companysettings.CompanySettings;
import com.medallia.mobile.dto.ResponsesEntry;
import com.medallia.mobile.dto.ResponsesLump;
import com.medallia.mobile.dto.builders.Build;
import com.medallia.mobile.dto.builders.Build.ResponsesLumpBuilder;
import com.medallia.mobile.filter.DashboardFilter;
import com.medallia.mobile.scorecard.ScorecardScreen;

public class ResponsesScreen {
	private final CompanySettings config;
	private final LogonInfo logonInfo;
	private final RequestParameterMap req;
	private final boolean isEmployeeScorecard;

	private static final int normalBatch = 30, minimumBatch = 10;
    private static final int MAX_BATCH_SIZE = normalBatch + minimumBatch - 1;
	
	public ResponsesScreen(CompanySettings config, LogonInfo logonInfo, RequestParameterMap req, boolean isEmployeeScorecard) {
		this.config = config;
		this.logonInfo = logonInfo;
		this.req = req;
		this.isEmployeeScorecard = isEmployeeScorecard;
	}

    /**
     * Contains a ResponsesEntry and the corresponding date. Used to not have to add the date to the ResponsesEntry class
     * since that class is used on the IPhone side, and the date is not needed there. The date is only needed here on
     * the server side to order the responses sent to the IPhone.
     */
    private static class ResponsesEntryWithDate {
    	ResponsesEntry re;
    	/** Response date in format '2009-07-14'. */
    	Date date;
    	ResponsesEntryWithDate(ResponsesEntry re, Date date) {
    		this.re = re;
    		this.date = date;
    	}
    	static List<ResponsesEntry> removeDate(List<ResponsesEntryWithDate> rewds) {
    		List<ResponsesEntry> res = new ArrayList<ResponsesEntry>();
    		for (ResponsesEntryWithDate rewd : rewds)
    			res.add(rewd.re);
    		return res;
    	}
    }
    
    /**
     * Comparator for the list of responses, used to sort on date. This can not be accomplished with
     * the query since the api sorts for time zone adjusted date, while the date in the database
     * is always server time zone.
     */
    Comparator<ResponsesEntryWithDate> responseComparator = new Comparator<ResponsesEntryWithDate>() {
    	@Override public int compare(ResponsesEntryWithDate o1, ResponsesEntryWithDate o2) {
    		return -o1.date.compareTo(o2.date);
    	}
    };
    
	public ResponsesLump get() {
    	List<ResponsesEntryWithDate> list = getResponses();
    	
        String offset = req.get(Param.offset);
        int ofs = offset == null ? 0 : Integer.parseInt(offset);

        ResponsesLumpBuilder responsesBuilder = Build.responsesLump();
        
        // we increase the batch size if the number of remaining messages is low
        // the maximum batch size is normalBatch + minimumBatch - 1
        int size = list.size();
        if (size > MAX_BATCH_SIZE) {
        	size = normalBatch;
            String url = logonInfo.getBase() + "?" + Param.offset + "=" + (ofs+size);
            Map<String,String> keepReq = req.getParameters();
            for (String s : keepReq.keySet()) {
            	if (Param.offset.equals(s)) continue;
                url += "&"+Common.urlencode(s)+"="+Common.urlencode(keepReq.get(s));
            }
            list = list.subList(0, size);
            Collections.sort(list, responseComparator);
            responsesBuilder.fetchMoreURL(url).fetchMoreText("Load More...");
        } else {
        	String fetchMoreText;
        	if (size == 0) {
        		if (isAlertRequest()) {
        			fetchMoreText = "No open alerts.";
        		} else {
        			fetchMoreText = "No Matches. Adjust Filter...";
        		}
        	} else {
        		fetchMoreText = "No More.";
        	}
        	Collections.sort(list, responseComparator);
	        responsesBuilder.fetchMoreText(fetchMoreText);
        }
        responsesBuilder.entries(ResponsesEntryWithDate.removeDate(list));
        
        // only query and update alert count if asking for alerts only
        if (req.get(Param.alerts_only) != null)
        	responsesBuilder.nrOpenAlerts(AlertNoteAction.getNrOpenAlerts(config));
        
        responsesBuilder.filterButtons(config.getResponsesFilterButtons());
        
       	return responsesBuilder.get();
    }

	private boolean isAlertRequest() {
		return req.get(Param.alerts_only) != null;
	}
        
    private void addFilters(SurveyQuery query) {
    	if (req.getAction() == RequestParameterMap.Action.scorecard) {
    		// big hack to get the right time period...
    		ScorecardScreen.addTimeperiodFilter(config, query);
    	} else {
    		ApiUtil.whereTimePeriod(query, req);
    	}

    	String range = req.get(Param.satrange);
        if (range != null) {
            String[] rs = range.split(":");
            int min = Double.valueOf(rs[0]).intValue();
            int max = Double.valueOf(rs[1]).intValue();
            StringBuffer rangeCondition = new StringBuffer(config.getMainScoreField().key + " IN (");
            for (int i = min; i <= max; ++i) {
            	if (i != min) rangeCondition.append(",");
            	rangeCondition.append(i);
            }
            rangeCondition.append(")");
            query.addCondition(rangeCondition.toString());
        }

		for (DashboardFilter sbf : config.getScoreboardFilters())
			sbf.addFilter(req, query);

        String searchTerm = req.get(Param.response_text);
        if (searchTerm != null && searchTerm.length() > 0)
        	query.addTextSearch(searchTerm);

        if (isAlertRequest())
        	query.addCondition("has_open_alert = 1");
        
        if (req.get(Param.comments_only) != null)
        	query.addCondition("has_comment = 1");
        	
        String respFilter = req.get(Param.resp_filter);
        if (respFilter != null)
        	config.applyResponsesFilter(query, respFilter);
        
        query.setOrderBy("responsedate DESC, surveyid DESC ");
        // We need to try to retrieve more responses than the maximum batch size,
        // to figure out how many responses to show and whether to display "load more"
        query.setLimit(MAX_BATCH_SIZE + 1);
    	
        String offset = req.get(Param.offset);
        if (offset != null)
        	query.setOffset(Integer.parseInt(offset));
        
        System.err.println("## built query = "+query.toString());
    }

	private List<ResponsesEntryWithDate> getResponses() {
		SurveyQuery query = new SurveyQuery();
    	query.addField(Survey.SURVEYID);
    	query.addField(Survey.ALERT_TYPE);
    	query.addField(Survey.RESPONSEDATE);
    	query.addField(Survey.COMMENTS);
    	query.addField(config.getMainScoreField());
    	query.addFields(config.getDetailFields().getNameFields());
    	
    	addFilters(query);
    	
    	StringField subheaderField = config.getResponsesSubheaderField();
    	if (subheaderField != null) query.addField(subheaderField);

    	List<Survey> table = query.run(config.getConnection());
		List<ResponsesEntryWithDate> entries = new ArrayList<ResponsesEntryWithDate>();
		for (Row row : table) {
			String id = String.valueOf(row.getInt(Survey.SURVEYID));
			FieldWithColor mainScoreField = config.getMainScoreField();
			Double score = row.getDouble(mainScoreField);
			String scoreStr = mainScoreField.getStringValue(row);
			UIColor color = UIColor.WHITE;
			if (score != null) {
				FieldWithColor f = config.getMainScoreField();
				color = BackgroundColorHelper.getInstance(config, f).uiColorForBackground(score, UIColor.WHITE);
			}

			String alertType = config.showAlertTypeInResponsesList() ?
					row.getString(Survey.ALERT_TYPE)
					: "";

			String comments = row.getString(Survey.COMMENTS);
			Date rawDate = row.getDate(Survey.RESPONSEDATE);
			String date = Common.PRETTY_DF.format(rawDate);
			String name = "";
			for (StringField nameField : config.getDetailFields().getNameFields()) {
				if (name.length() > 0)
					name += " ";
				name += row.getString(nameField);
			}
			String subheader = subheaderField == null ? "" : row.getString(subheaderField);
			String url = logonInfo.createURL("details") + "&" + Param.id + "=" + id;
			if (isEmployeeScorecard) {
				url += "&" + Param.employee + "=1";
			}
			
			entries.add(new ResponsesEntryWithDate(Build.responsesEntry().uniqueIdentifier(id).score(scoreStr).scoreColor(color).alert(
			        alertType.length() > 0 ? alertType : null).name(name).sectionHeader(date).text(!comments.isEmpty() ? comments : null)
			        .subheader(subheader)
			        .detailsURL(url).get(), rawDate));
		}
		return entries;
	}
}
