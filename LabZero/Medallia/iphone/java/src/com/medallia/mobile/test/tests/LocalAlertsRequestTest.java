package com.medallia.mobile.test.tests;

import java.util.List;

import org.joda.time.DateTime;

import com.medallia.mobile.dto.SingleResponse;
import com.medallia.mobile.test.framework.MobileTestCase;
import com.medallia.mobile.test.framework.beans.Id;
import com.medallia.mobile.test.framework.beans.LogNote;
import com.medallia.mobile.test.framework.config.Environment;
import com.medallia.mobile.test.framework.config.ExpressEnvironment;
import com.medallia.mobile.test.framework.config.MobileEnvironment;
import com.medallia.mobile.test.framework.querier.Querier;
import com.medallia.mobile.test.framework.requests.MobileRequest;
import com.medallia.mobile.test.framework.requests.MobileRequestBuilder;

/**
 * Contains the Test Suite for the Alerts and Notes Request of the Mobile Servlet.
 * 
 * @author fernando
 */
@Environment(express = ExpressEnvironment.EARTH, mobile = MobileEnvironment.LOCALHOST)
public class LocalAlertsRequestTest extends MobileTestCase {

	private MobileRequest alertsRequest;
	private MobileRequest logNotesRequest;

	/**
	 * Tests that Log notes are added successfully.
	 * 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Creates a survey.
	 * 	<li> Adds two notes.
	 * 	<li> Gets the log notes from the survey.
	 * 	<li> Asserts that there are only 2 notes.
	 * 	<li> Asserts that the 2 notes contains the proper message a content.
	 * </ul>
	 */
	public void testLogNotesAreAddedSucceessfully() throws Exception {
		Id<?> surveyId = getQuerier().createDummySurvey("earth", "Springfield");
		logNotesRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment())
			.surveyId(surveyId).logNote("test note").buildLogNotesRequest();
		SingleResponse response = getAlertNoteResponse(logNotesRequest);
		assertEquals(1, response.notes().size());
		assertEquals("test note", response.notes().get(0).content());
		
		logNotesRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment())
			.surveyId(surveyId).logNote("test note2").buildLogNotesRequest();
		response = getAlertNoteResponse(logNotesRequest);
		assertEquals(2, response.notes().size());
		assertEquals("test note2", response.notes().get(0).content());
		assertEquals("test note", response.notes().get(1).content());
		
		List<LogNote> apiResponse = getQuerier().getLogNotes(surveyId);
		assertEquals(2, apiResponse.size());
		assertEquals("test note2", apiResponse.get(0).getContent());
		assertEquals("Note Added", apiResponse.get(0).getTitle());
		assertEquals(getExpressEnvironment().getUser(), apiResponse.get(0).getUser());
		DateTime today = new DateTime();
		assertEquals(today.getDayOfMonth(), apiResponse.get(0).getDate().getDayOfMonth());
		assertEquals(today.getMonthOfYear(), apiResponse.get(0).getDate().getMonthOfYear());
		assertEquals(today.getYear(), apiResponse.get(0).getDate().getYear());
		
		assertEquals("test note", apiResponse.get(1).getContent());
		assertEquals("Note Added", apiResponse.get(1).getTitle());
		assertEquals(getExpressEnvironment().getUser(), apiResponse.get(1).getUser());
		assertEquals(today.getDayOfMonth(), apiResponse.get(1).getDate().getDayOfMonth());
		assertEquals(today.getMonthOfYear(), apiResponse.get(1).getDate().getMonthOfYear());
		assertEquals(today.getYear(), apiResponse.get(1).getDate().getYear());	
	}
	
	/**
	 * Checks that it is possible to close an active alert.
	 * 
	 * <p> Steps:
	 * 
	 * <ul>
	 * 	<li> Creates a survey.
	 * 	<li> Triggers an alert on that survey.
	 * 	<li> Closes the alert on that survey.
	 * 	<li> Asserts that the alert was closed for that survey.
	 * </ul>
	 */
	public void testClosingAnActiveAlertSucceeds() throws Exception {
		Querier querier = getQuerier();
		Id<?> survey = querier.createDummySurvey("earth", "Springfield");
		querier.triggerAlert("earth", "A1", survey);
		alertsRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment())
			.surveyId(survey).buildAlertsRequest();
		SingleResponse response = getAlertNoteResponse(alertsRequest);
		assertTextPresent(response.html(), "<span class=\"right\">New</span>");
		
		alertsRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment())
			.surveyId(survey).alert("closed").buildAlertsRequest();
		response = getAlertNoteResponse(alertsRequest);
		assertTextPresent(response.html(), "<span class=\"right\">Closed</span>");
	}

	/**
	 * Checks that it is possible to resolve an active alert.
	 * 
	 * <p> Steps:
	 * 
	 * <ul>
	 * 	<li> Creates a survey.
	 * 	<li> Triggers an alert on that survey.
	 * 	<li> Resolves the alert on that survey.
	 * 	<li> Asserts that the alert was resolved for that survey.
	 * </ul>
	 */
	public void testResolvingAnActiveAlertSucceeds() throws Exception {
		Querier querier = getQuerier();
		Id<?> survey = querier.createDummySurvey("earth", "Springfield");
		querier.triggerAlert("earth", "A1", survey);
		alertsRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment())
			.surveyId(survey).buildAlertsRequest();
		SingleResponse response = getAlertNoteResponse(alertsRequest);
		assertTextPresent(response.html(), "<span class=\"right\">New</span>");

		alertsRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment())
			.surveyId(survey).alert("resolved").buildAlertsRequest();
		response = getAlertNoteResponse(alertsRequest);
		assertTextPresent(response.html(), "<span class=\"right\">Resolved</span>");
	}
	
	/**
	 * Checks that it is possible to close an resolved alert.
	 * 
	 * <p> Steps:
	 * 
	 * <ul>
	 * 	<li> Creates a survey.
	 * 	<li> Triggers an alert on that survey.
	 * 	<li> Resolves the alert on that survey.
	 * 	<li> Closes that alert.
	 * 	<li> Asserts that the alert was closed for that survey.
	 * </ul>
	 */
	public void testClosingAResolvedAlertSucceeds() {
		Querier querier = getQuerier();
		Id<?> survey = querier.createDummySurvey("earth", "Springfield");
		querier.triggerAlert("earth", "A1", survey);
		alertsRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment())
			.surveyId(survey).buildAlertsRequest();
		SingleResponse response = getAlertNoteResponse(alertsRequest);
		assertTextPresent(response.html(), "<span class=\"right\">New</span>");
		
		alertsRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment())
			.surveyId(survey).alert("resolved").buildAlertsRequest();
		response = getAlertNoteResponse(alertsRequest);
		assertTextPresent(response.html(), "<span class=\"right\">Resolved</span>");
		
		alertsRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment())
			.surveyId(survey).alert("closed").buildAlertsRequest();
		response = getAlertNoteResponse(alertsRequest);
		assertTextPresent(response.html(), "<span class=\"right\">Closed</span>");
	}
	
	/**
	 * Checks that it is possible to reopen a closed alert.
	 * 
	 * <p> Steps:
	 * 
	 * <ul>
	 * 	<li> Creates a survey.
	 * 	<li> Triggers an alert on that survey.
	 * 	<li> Closes the alert on that survey.
	 * 	<li> Reopens that alert.
	 * 	<li> Asserts that the alert was reopened for that survey.
	 * </ul>
	 */
	public void testReopenAClosedAlertSucceeds() throws Exception {
		Querier querier = getQuerier();
		Id<?> survey = querier.createDummySurvey("earth", "Springfield");
		querier.triggerAlert("earth", "A1", survey);
		alertsRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment())
			.surveyId(survey).alert("closed").buildAlertsRequest();
		SingleResponse response = getAlertNoteResponse(alertsRequest);
		assertTextPresent(response.html(), "<span class=\"right\">Closed</span>");
		
		alertsRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment())
			.surveyId(survey).alert("in_progress").buildAlertsRequest();
		response = getAlertNoteResponse(alertsRequest);
		assertTextPresent(response.html(), "<span class=\"right\">In Progress</span>");
	}
	
	/**
	 * Checks that it is possible to reopen a resolved alert.
	 * (although not supported by the frontend, still supported by the backend)
	 * 
	 * <p> Steps:
	 * 
	 * <ul>
	 * 	<li> Creates a survey.
	 * 	<li> Triggers an alert on that survey.
	 * 	<li> Resolves the alert on that survey.
	 * 	<li> Reopens that alert.
	 * 	<li> Asserts that the request succeeded.
	 * </ul>
	 */
	public void testReopenAResolvedAlertSucceeds() throws Exception {
		Querier querier = getQuerier();
		Id<?> survey = querier.createDummySurvey("earth", "Springfield");
		querier.triggerAlert("earth", "A1", survey);
		alertsRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment())
			.surveyId(survey).alert("resolved").buildAlertsRequest();
		SingleResponse response = getAlertNoteResponse(alertsRequest);
		assertTextPresent(response.html(), "<span class=\"right\">Resolved</span>");
		
		alertsRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment())
			.surveyId(survey).alert("in_progress").buildAlertsRequest();
		response = getAlertNoteResponse(alertsRequest);
		assertTextPresent(response.html(), "<span class=\"right\">In Progress</span>");
	}
	
	/**
	 * Checks that it is possible to close an already closed alert.
	 * 
	 * <p> Steps:
	 * 
	 * <ul>
	 * 	<li> Creates a survey.
	 * 	<li> Triggers an alert on that survey.
	 * 	<li> Closes that alert.
	 *  <li> Closes the alert again.
	 * 	<li> Asserts that the alert still is closed for that survey.
	 * </ul>
	 */
	public void testClosingAClosedAlertSucceeds() {
		Querier querier = getQuerier();
		Id<?> survey = querier.createDummySurvey("earth", "Springfield");
		querier.triggerAlert("earth", "A1", survey);
		
		alertsRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment())
			.surveyId(survey).alert("closed").buildAlertsRequest();
		String res = getResponse(alertsRequest);
		SingleResponse response = getAlertNoteResponse(alertsRequest);
		assertTextPresent(response.html(), "<span class=\"right\">Closed</span>");
		
		alertsRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment())
			.surveyId(survey).alert("closed").buildAlertsRequest();
		response = getAlertNoteResponse(alertsRequest);
		assertTextPresent(response.html(), "<span class=\"right\">Closed</span>");
	}
	
	/**
	 * Checks that it is possible to resolve an already resolved alert.
	 * 
	 * <p> Steps:
	 * 
	 * <ul>
	 * 	<li> Creates a survey.
	 * 	<li> Triggers an alert on that survey.
	 * 	<li> Resolves that alert.
	 *  <li> Resolves the alert again.
	 * 	<li> Asserts that the alert still is Resolved for that survey.
	 * </ul>
	 */
	public void testResolvingAResolvedAlertSucceeds() {
		Querier querier = getQuerier();
		Id<?> survey = querier.createDummySurvey("earth", "Springfield");
		querier.triggerAlert("earth", "A1", survey);
		
		alertsRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment())
			.surveyId(survey).alert("resolved").buildAlertsRequest();
		SingleResponse response = getAlertNoteResponse(alertsRequest);
		assertTextPresent(response.html(), "<span class=\"right\">Resolved</span>");
		
		alertsRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment())
			.surveyId(survey).alert("resolved").buildAlertsRequest();
		response = getAlertNoteResponse(alertsRequest);
		assertTextPresent(response.html(), "<span class=\"right\">Resolved</span>");
	}
	
	/**
	 * Checks that it is possible to resolve an already closed alert.
	 * (although not supported by the frontend, still supported by the backend)
	 * 
	 * <p> Steps:
	 * 
	 * <ul>
	 * 	<li> Creates a survey.
	 * 	<li> Triggers an alert on that survey.
	 * 	<li> Closes the alert.
	 * 	<li> Resolves the alert.
	 * 	<li> Checks that the request succeeds.
	 * </ul>
	 */
	public void testResolvingAClosedAlertSucceeds() {
		Querier querier = getQuerier();
		Id<?> survey = querier.createDummySurvey("earth", "Springfield");
		querier.triggerAlert("earth", "A1", survey);
			
		alertsRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment())
			.surveyId(survey).alert("closed").buildAlertsRequest();
		SingleResponse response = getAlertNoteResponse(alertsRequest);
		assertTextPresent(response.html(), "<span class=\"right\">Closed</span>");
		
		
		alertsRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment())
			.surveyId(survey).alert("resolved").buildAlertsRequest();
		response = getAlertNoteResponse(alertsRequest);
		assertTextPresent(response.html(), "<span class=\"right\">Resolved</span>");
	}
	
	/**
	 * Checks that sending a request with an invalid survey fails.
	 * 
	 * <p> Steps:
	 * 
	 * <ul>
	 * 	<li> Creates a survey.
	 * 	<li> Sends an alert request with an invalid survey id.
	 * 	<li> Checks that the request fails.
	 * </ul>
	 */
	public void testGettingAlertOfAnInvalidSurveyFails() {		
		alertsRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment())
			.surveyId(new Id("233123")).alert("closed").buildAlertsRequest();
		assertTrue(getResponse(alertsRequest).contains("No survey found with id 233123"));		
	}

	/**
	 * Checks that sending a request with a null survey fails.
	 * 
	 * <p> Steps:
	 * 
	 * <ul>
	 * 	<li> Creates a survey.
	 * 	<li> Sends an alert request with a null survey id.
	 * 	<li> Checks that the request fails.
	 * </ul>
	 */	
	public void testGettingAlertOfNullSurveyFails() {
		alertsRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment())
			.surveyId(null).alert("closed").buildAlertsRequest();
		assertTextPresent(getResponse(alertsRequest), "Expected a number or string");
	}
	
	/**
	 * Checks that sending a request with survey that has not alert fails.
	 * 
	 * <p> Steps:
	 * 
	 * <ul>
	 * 	<li> Creates a survey.
	 * 	<li> Sends an alert request with a survey that has no alert.
	 * 	<li> Checks that the request fails.
	 * </ul>
	 */		
	public void testGettingAlertOfSurveyWithoutAlertFails() {
		Querier querier = getQuerier();
		Id<?> survey = querier.createDummySurvey("earth", "Springfield");
	
		alertsRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment())
			.surveyId(survey).alert("closed").buildAlertsRequest();
		assertTextPresent(getResponse(alertsRequest), "Survey does not have an alert");
	}
	
	/**
	 * Checks that sending a request with null action fails.
	 * 
	 * <p> Steps:
	 * 
	 * <ul>
	 * 	<li> Creates a survey.
	 * 	<li> Triggers an alert on that survey.
	 * 	<li> Sends an alert request with a null action.
	 * 	<li> Checks that the request fails.
	 * </ul>
	 */		
	public void testSettingAlertWithNullActionFails() {
		Querier querier = getQuerier();
		Id<?> survey = querier.createDummySurvey("earth", "Springfield");
		querier.triggerAlert("earth", "A1", survey);
				
		alertsRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment())
			.surveyId(survey).alert(null).buildAlertsRequest();
		assertTextPresent(getResponse(alertsRequest), "Unknown value for alert_status: null");
	}
	
	/**
	 * Checks that sending a request with an invalid action fails.
	 * 
	 * <p> Steps:
	 * 
	 * <ul>
	 * 	<li> Creates a survey.
	 * 	<li> Triggers an alert on that survey.
	 * 	<li> Sends an alert request with an invalid action.
	 * 	<li> Checks that the request fails.
	 * </ul>
	 */		
	public void testSettingAlertWithInvalidActionFails() {
		Querier querier = getQuerier();
		Id<?> survey = querier.createDummySurvey("earth", "Springfield");
		querier.triggerAlert("earth", "A1", survey);
				
		alertsRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment())
			.surveyId(survey).alert("invalid").buildAlertsRequest();
		assertTrue(getResponse(alertsRequest).contains("Unknown value for alert_status: invalid"));				
		
	}
	
	/**
	 * Tests that it is not possible to get the request with an unknown company.
	 * 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Sends a request with an unknown company.
	 * 	<li> Checks that the request does not go through and shows a proper message.
	 * </ul>
	 */
	public void testAlertsRequestFailsIfCompanyUnknown() throws Exception {
		alertsRequest = MobileRequestBuilder.getBuilder(
				getExpressEnvironment()).company("unknownCompany")
				.buildAlertsRequest();
		assertTextPresent(getResponse(alertsRequest), "Unknown company");
	}

	/**
	 * Tests that it is not possible to get the request with a wrong password.
	 * 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Sends a request with wrong password.
	 * 	<li> Checks that the request fails and shows a proper message.
	 * </ul> 	
	 */
	public void testAlertsRequestFailsIfWrongPassword() throws Exception {
		alertsRequest = MobileRequestBuilder.getBuilder(
				getExpressEnvironment()).password("wrongPassword")
				.buildAlertsRequest();
		assertTextPresent(getResponse(alertsRequest), "Not authorized");
	}

	/**
	 * Tests that it is not possible to get the request with a non existent
	 * user.
	 * 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Sends a request with a user that does not exist.
	 * 	<li> Checks that the request fails with a proper message.
	 * </ul>
	 */
	public void testAlertsRequestFailsIfUserDoesNotExist() throws Exception {
		alertsRequest = MobileRequestBuilder.getBuilder(
				getExpressEnvironment()).user("nonExistent")
				.buildAlertsRequest();
		assertTextPresent(getResponse(alertsRequest), "Not authorized");
	}

	/**
	 * Tests that it is not possible to get the request with a null user.
	 * 
	 * <p> Steps:
	 * <ul> 
	 * 	<li> Sends a request with a null user.
	 * 	<li> Checks that the request does not go through and shows a proper message.
	 * </ul>
	 */
	public void testAlertsRequestFailsWithNullUser() throws Exception {
		alertsRequest = MobileRequestBuilder.getBuilder(
				getExpressEnvironment()).user(null).buildAlertsRequest();
		assertTextPresent(getResponse(alertsRequest), "Not authorized");
	}

	/**
	 * Tests that it is not possible to get the request with a null password.
	 * 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Sends a request with null password.
	 * 	<li> Checks that the request fails and shows a proper message.
	 * </ul> 	
	 */	
	public void testAlertsRequestFailsWithNullPassword() throws Exception {
		alertsRequest = MobileRequestBuilder.getBuilder(
				getExpressEnvironment()).password(null).buildAlertsRequest();
		assertTextPresent(getResponse(alertsRequest), "Not authorized");
	}

	/**
	 * Tests that it is not possible to get the request with both a null user
	 * and a null password.
	 * 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Sends a request with both null user and null password.
	 * 	<li> Checks that the request fails and shows a proper message.
	 * </ul>
	 */
	public void testAlertsRequestFailsWithNullPasswordAndNullUser()
			throws Exception {
		alertsRequest = MobileRequestBuilder.getBuilder(
				getExpressEnvironment()).user(null).password(null)
				.buildAlertsRequest();
		assertTextPresent(getResponse(alertsRequest), "Not authorized");
	}

	/**
	 * Tests that it is not possible to get the request with a null company
	 * 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Sends a request with a null company.
	 * 	<li> Checks that the request fails and shows a proper message.
	 * </ul>
	 */
	public void testAlertsRequestFailsWithNullCompany() throws Exception {
		alertsRequest = MobileRequestBuilder.getBuilder(
				getExpressEnvironment()).company(null).buildAlertsRequest();
		assertTextPresent(getResponse(alertsRequest), "Unknown company");
	}
	
	/**
	 * Tests that it is not possible to get a request with an empty company.
	 *  
	 * <p> Steps:
	 * <ul>
	 * 	<li> Sends a request with an empty company.
	 * 	<li> Checks that the request fails and shows a proper message.
	 * </ul>
	 */
	public void testAlertsRequestFailsWithEmptyCompany() throws Exception {
		alertsRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).company("").buildAlertsRequest();
		assertTextPresent(getResponse(alertsRequest), "Unknown company");		
	}

	/**
	 * Tests that it is not possible to get a request with an empty user.
	 *  
	 * <p> Steps:
	 * <ul>
	 * 	<li> Sends a request with an empty user.
	 * 	<li> Checks that the request fails and shows a proper message.
	 * </ul>
	 */
	public void testAlertsRequestFailsWithEmptyUser() throws Exception {
		alertsRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).user("").buildAlertsRequest();
		assertTextPresent(getResponse(alertsRequest), "Not authorized");		
	}

	/**
	 * Tests that it is not possible to get a request with an empty password.
	 *  
	 * <p> Steps:
	 * <ul>
	 * 	<li> Sends a request with an empty password.
	 * 	<li> Checks that the request fails and shows a proper message.
	 * </ul>
	 */
	public void testAlertsRequestFailsWithEmptyPassword() throws Exception {
		alertsRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).password("").buildAlertsRequest();
		assertTextPresent(getResponse(alertsRequest), "Not authorized");		
	}
}
