package com.medallia.mobile;

import com.medallia.api.datamodel.Survey;
import com.medallia.api.orm.Field;

public class FieldAndAction {
	
	// Note: this constant must equal the SCORECARD define in ResponseDetails.m
	private final String SCORECARD = "scorecard";
	
	private final Field field;
	private final String luceneId;
	
	public FieldAndAction(Field field) {
		this(field, field.key);
	}

	public FieldAndAction(Field field, String luceneId) {
		this.field = field;
		this.luceneId = luceneId;
	}
	
	public String getName() {
		return field.name;
	}

	public String getTextSearch(Survey survey) {
		return SCORECARD + luceneId + ":\"" + getValue(survey) + "\"";
	}
	
	public String getValue(Survey survey) {
		return survey.getValueRaw(field);
	}

	public Field getField() {
		return field;
	}
	
}
