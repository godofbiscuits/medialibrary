package com.medallia.mobile.test.framework.querier.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.joda.time.DateTime;

import com.google.gson.Gson;
import com.medallia.api.datamodel.Timeperiod;
import com.medallia.mobile.Common;
import com.medallia.mobile.test.framework.beans.Id;
import com.medallia.mobile.test.framework.beans.LogNote;
import com.medallia.mobile.test.framework.config.ExpressEnvironment;
import com.medallia.mobile.test.framework.querier.AbstractQuerier;
import com.medallia.mobile.test.framework.querier.Querier;

/**
 * Implementation of a {@link Querier} that calls Production Apis.
 * 
 * <p>
 * 	Shouldn't be used directly, {@link ExpressQuerier} should be used instead.
 * 
 * @author fernando
 */
public class ProductionApiQuerier extends AbstractQuerier {

	private final ExpressEnvironment express;	

	@Override
	public int getNumberOfSurveys(Timeperiod timePeriod,
			Map<String, String> filters) {
		StringBuffer filterBuffer = new StringBuffer();
		for (Map.Entry<String, String> filter : filters.entrySet())
		{
			filterBuffer.append("AND (" + filter.getKey() + "='" + filter.getValue() +"') ");
		} 
		String response = getResponse(getQueryHeader(),
				String.format("query=SELECT count(*) FROM survey WHERE timeperiod(%s, responsedate) %s", timePeriod.id, filterBuffer.toString()));
		return Double.valueOf(response.substring(response.indexOf("[[\"") + 3, response.indexOf("\"]]"))).intValue();
	}
	
	@Override
	public int getNumberOfSurveys(Timeperiod timePeriod) {
		String response = getResponse(getQueryHeader(),
				String.format("query=SELECT count(*) FROM survey WHERE timeperiod(%s, responsedate)", timePeriod.id));
		return Double.valueOf(response.substring(response.indexOf("[[\"") + 3, response.indexOf("\"]]"))).intValue();
	}
	
	@Override
	public List<Timeperiod> getTimePeriods() {
		String response = getResponse(getQueryHeader(), "query=SELECT id,name,startdate,enddate,priority FROM timeperiod");
		Gson gson = new Gson();
		//Awful hack to get the table, ideally there would be a Table class to get the table with Gson.
		String tableString = response.substring(response.indexOf("table\":") + 7, response.indexOf("}"));
		String[][] table = gson.fromJson(tableString, String[][].class);
		List<Timeperiod> timePeriodList = new LinkedList<Timeperiod>();
		Calendar calendar = Calendar.getInstance();
		for(String[] item :table) {
			String date[] = item[2].split("-");
			calendar.set(Integer.valueOf(date[0]), Integer.valueOf(date[1]), Integer.valueOf(date[2]));
			Date startDate = calendar.getTime();
			date = item[3].split("-");
			calendar.set(Integer.valueOf(date[0]), Integer.valueOf(date[1]), Integer.valueOf(date[2]));
			Date endDate = calendar.getTime();
			timePeriodList.add(new Timeperiod(item[0], item[1], startDate, endDate, Integer.valueOf(item[4])));
		}
		return timePeriodList;
	}
	
	@Override
	public double getAverageFromSurvey(String field, String beginDate,
			String endDate) {
		String response = getResponse(getQueryHeader(),"query=" + 
				Common.urlencode(String.format("SELECT avg(%s) FROM survey " +
						"WHERE responsedate > '%s' AND responsedate < '%s'", field, beginDate, endDate)));
		return Double.valueOf(response.substring(response.indexOf("[[\"") + 3, response.indexOf("\"]]")));
	}

	@Override
	public double getAverageFromSurvey(String field, Id<?> timePeriod) {
		String response = getResponse(getQueryHeader(),"query=" + 
				Common.urlencode(String.format("SELECT avg(%s) FROM survey " +
						"WHERE timeperiod(%s, responsedate)", field, timePeriod.toString())));
		return Double.valueOf(response.substring(response.indexOf("[[\"") + 3, response.indexOf("\"]]")));
	}
	
	@Override
	public List<LogNote> getLogNotes(Id<?> surveyId) {
		String response = getResponse(getQueryHeader(),"query=" + 
				Common.urlencode("SELECT date, user, title, content FROM note WHERE surveyid = " + surveyId));
		Gson gson = new Gson();
		//Awful hack to get the table, ideally there would be a Table class to get the table with Gson.
		String tableString = response.substring(response.indexOf("table\":") + 7, response.indexOf("}"));
		String[][] table = gson.fromJson(tableString, String[][].class);
		List<LogNote> logNoteList = new LinkedList<LogNote>();
		LogNote logNote;
		for(String[] item :table) {
			logNote = new LogNote();
			String date[] = item[0].split("-");
			logNote.setDate(new DateTime(Integer.valueOf(date[0]), Integer.valueOf(date[1]), Integer.valueOf(date[2]), 0, 0, 0, 0));
			logNote.setUser(item[1]);
			logNote.setTitle(item[2]);
			logNote.setContent(item[3]);
			logNoteList.add(logNote);
		}
		return logNoteList;
	}
	
	/**
	 * Gets the header for querying the API.
	 */
	private String getQueryHeader() {
		return String.format("%s/api?output=json&company=%s&user=%s&pass=%s&version=1", express.getUrl(),
				Common.urlencode(express.getCompany()), Common.urlencode(express.getUser()),
				Common.urlencode(express.getPassword()));
		
	}
	
	private ProductionApiQuerier(ExpressEnvironment express) {
		this.express = express;
	}
	
	static Querier createQuerier(ExpressEnvironment express) {
		return new ProductionApiQuerier(express);
	}
}
