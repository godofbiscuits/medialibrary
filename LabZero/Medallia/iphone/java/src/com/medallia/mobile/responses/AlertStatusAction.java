package com.medallia.mobile.responses;

import com.medallia.mobile.dto.AlertAction;
import com.medallia.mobile.dto.builders.Build;

enum AlertStatusAction {
	/** the alert is being handled (and "reopen" is the action taken to make an alert "in progress", since it must come from closed status) */
	IN_PROGRESS("In Progress", "in_progress", "Reopen", "Alert reopened"),
	/** the alert is closed, no further action necessary */
	CLOSED("Closed", "closed", "Close", "Alert closed"),
	/** the alert is resolved, but not yet closed */
	RESOLVED("Resolved", "resolved", "Resolve", "Alert resolved"),
	;
	
	/** The status string as returned by the API for this alert status. */
	private String status;
	
	/** Alert action that is sent to the API ("closed", "resolved", etc). */
	private String action;
	
	/** Text on the iPhone button the user clicks to change the alert status. */
	private String buttonText;
	
	/** Text to add to the note log */
	private String noteLogText;
	
	private AlertStatusAction(String status, String action, String buttonText, String noteLogText) {
		this.status = status;
		this.action = action;
		this.buttonText = buttonText;
		this.noteLogText = noteLogText;
	}

	/** @return An AlertAction to change an alert to this AlertStatus. */
	AlertAction getAlertAction() {
		return Build.alertAction().action(action).text(buttonText).get();			
	}
	
	/** @return The status string as returned by the API for this alert status. */
	String getStatus() {
		return status;
	}
	
	public String getNoteLogText() { return noteLogText; }
	
	/** @return the AlertStatusAction with a given action value */
	public static AlertStatusAction getActionForStatus(String action) {
		for (AlertStatusAction asa : AlertStatusAction.values()) {
			if (asa.action.equals(action)) {
				return asa;
			}
		}
		return null;
	}
}