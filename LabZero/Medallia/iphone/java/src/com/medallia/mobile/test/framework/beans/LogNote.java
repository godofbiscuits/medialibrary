package com.medallia.mobile.test.framework.beans;

import org.joda.time.DateTime;

/**
 * Bean that represents a Log Note.
 * 
 * @author fernando
 */
public class LogNote {

	private DateTime date;
	private String title;
	private String content;
	private String user;

	public LogNote() {
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	
	public DateTime getDate() {
		return date;
	}

	public void setDate(DateTime date) {
		this.date = date;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}
