package com.medallia.mobile;

import com.medallia.api.orm.ApiConnection;
import com.medallia.api.orm.Statement;

public abstract class PasswordApiConnection implements ApiConnection {
	
	protected final String company;
	protected final String user;
	protected final String password;
	
	public PasswordApiConnection(String company, String user, String password) {
		this.company = company;
		this.user = user;
		this.password = password;
	}
	
	/**
	 * Return the base URI of the API, including the company name, e.g.:
	 * https://api.medallia.com/global
	 * @return the base URI of the API
	 */
	protected abstract String getBaseURI();

	protected String getQueryURI(Statement statement) {
		return getBaseURI() +
		"&user=" + Common.urlencode(user) + 
		"&pass=" + Common.urlencode(password) + 
		"&charSet=utf8" + 
		(statement.isExtendedScope() ? "&extended_scope=true" : "") +
		"&query=" + Common.urlencode(statement.getQueryString()) + 
		"&output=json" +
		"&version=1"
		;
	}
	
}
