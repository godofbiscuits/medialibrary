package com.medallia.mobile.color;


public class SimpleBackgroundColorRule implements BackgroundColorRule {

	private final double redMax;
	private final double greenMin;
	
	public SimpleBackgroundColorRule(double redMax, double greenMin) {
		this.redMax = redMax;
		this.greenMin = greenMin;
	}
	
	@Override
	public BackgroundColor getBackgroundColor(double score) {
		return score <= redMax ? BackgroundColor.RED : (score >= greenMin ? BackgroundColor.GREEN : BackgroundColor.YELLOW);
	}

}
