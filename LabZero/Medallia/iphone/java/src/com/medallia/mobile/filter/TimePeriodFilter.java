package com.medallia.mobile.filter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.medallia.mobile.TemporaryTimeperiod;
import com.medallia.mobile.companysettings.CompanySettings;
import com.medallia.mobile.dto.Alt;
import com.medallia.mobile.dto.AltSection;
import com.medallia.mobile.dto.builders.Build;

public class TimePeriodFilter {
	public static List<AltSection> getFilter(CompanySettings config) {
		List<Alt> alts = new ArrayList<Alt>();
    	for (TemporaryTimeperiod timeperiod : TemporaryTimeperiod.getTemporaryTimeperiods(config))
    		alts.add(Build.alt().key(timeperiod.id).value(timeperiod.name).headerValue(timeperiod.getShortName()).table(Build.altTable().key("").sections(new ArrayList<AltSection>()).get()).get());
		return Arrays.asList(Build.altSection().alternatives(alts).get());
	}
}
