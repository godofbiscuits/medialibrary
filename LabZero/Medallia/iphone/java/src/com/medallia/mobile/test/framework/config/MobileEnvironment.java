package com.medallia.mobile.test.framework.config;

/**
 * Defines the different Mobile Environments that can be used.
 * 
 * @author fernando
 */
public enum MobileEnvironment {
	LOCALHOST("http://localhost", 5009),
	PRODUCTION("", 0);

	private final String host;
	
	private final int port;
	
	private MobileEnvironment(String host, int port) {
		this.host = host;
		this.port = port;
	}
	
	public String getRequestHeader() {
		return String.format("%s:%s?", host, port);
	}
	
	public String getHost() {
		return host;
	}

	public int getPort() {
		return port;
	}
}
