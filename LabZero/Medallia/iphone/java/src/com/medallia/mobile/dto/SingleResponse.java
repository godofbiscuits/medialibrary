package com.medallia.mobile.dto;
import java.util.*;
import com.medallia.mobile.*;
import com.medallia.mobile.dto.*;
import com.medallia.mobile.dto.builders.*;
import com.medallia.mobile.color.UIColor;

// These files are generated automatically from declarations in DataFormat.h; see convert.pl for details.
//
// Do *not* modify this file directly!


/**
 *  A single survey. Contains HTML for rendering in WebKit, and information about actions like adding note, calling customer, forwarding email, etc.
 */
@SuppressWarnings("unused")
public interface SingleResponse extends Plist.Plistable {

	/**
	 *  HTML to be rendered by WebKit. This should ideally be self-contained; images are slow on a cell phone.
	 */
	@Required String html();

	/**
	 *  survey taker name (needed for notes/alerts screen)
	 */
	@Required String name();

	/**
	 *  if present, the user has permission to add notes
	 */
	String canAddNote();

	/**
	 *  if present, the user can call the customer
	 */
	String customerPhone();

	/**
	 *  the base URL to which parameters for the specific selected scoreboard should be appended in order to load it
	 */
	String scoreboardBaseURL();

	/**
	 *  log notes
	 */
	@Required List<LogNote> notes();

	/**
	 *  Name of alert, if there is an alert
	 */
	String alertName();

	/**
	 *  Status of alert, if there is an alert
	 */
	String alertStatus();

	/**
	 *  Alert actions (e.g. "close alert")
	 */
	@Required List<AlertAction> alertActions();

	/**
	 *  optional email action
	 */
	EmailAction emailAction();

	/**
	 *  Number of open alerts (optional -- only sent after an alert has been closed)
	 */
	String nrOpenAlerts();


}
