package com.medallia.mobile.color;

/** Replacement for java.awt.Color; that class pulls in all of AWT, which is no good for
 * a headless server */
public class UIColor {
	public static final UIColor LIGHTBLUE = new UIColor(0x86CDFE);
	public static final UIColor BLUE = new UIColor(0x007FFF);
	public static final UIColor GRAY = new UIColor(0xcccccc);
	public static final UIColor WHITE = new UIColor(0xffffff);
    public static final UIColor RED = new UIColor(0xbd0900);
    public static final UIColor GREEN = new UIColor(0x008209);
    public static final UIColor YELLOW = new UIColor(0xd4a500);
    public static final UIColor BLACK = new UIColor(0x000000);
    int color;
    public UIColor(int r) { this.color = r; }
    public int getRGB() { return color; }
}
