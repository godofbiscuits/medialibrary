package com.medallia.mobile.companysettings;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.medallia.api.datamodel.Survey;
import com.medallia.api.orm.Field;
import com.medallia.api.orm.Field.StringField;
import com.medallia.mobile.DetailFields;
import com.medallia.mobile.DetailFieldsDefaultBase;
import com.medallia.mobile.FieldWithColor;
import com.medallia.mobile.RequestParameterMap.LogonInfo;

public class CompanySettingsHHCQA extends CompanySettingsDefaultBase {

    public CompanySettingsHHCQA(LogonInfo req) {
        super(req);
    }

    @Override
    protected String getCompany() {
        return "hilton";
    }

    @Override
    protected String getBaseURI0() {
        return "https://hhcqa.medallia.com";
    }

    @Override
    public Date getStartTime() {
        return getDate(2009, 0, 1);
    }

    @Override
    public String getUnitTranslation() {
	    return "Property";
    }

    @Override
    public FieldWithColor getMainScoreField() {
        return new FieldWithColor("hhc_ov_experience", "Overall Experience");
    }

    @Override
    public List<FieldWithColor> getScoreFields(String widgetRequestAction) {
        List<FieldWithColor> list = new ArrayList<FieldWithColor>();
        list.add(new FieldWithColor("hhc_ov_experience", "Overall Experience"));
        list.add(new FieldWithColor("hhc_ov_return", "Return to property"));
        list.add(new FieldWithColor("hhc_ov_recommend", "Recommend"));

        return list;
    }

	@Override public List<FieldWithColor> getScorecardFields() {
		return getScoreFields("");
	}
	
    @Override
    public DetailFields getDetailFields() {
        return new DetailFieldsDefaultBase() {

            @Override
            public List<Field> getParticipantFields() {
                return Arrays.<Field>asList(Survey.firstname, Survey.lastname);
            }

            @Override
            public List<StringField> getCommentFields() {
                List<StringField> list = new ArrayList<StringField>();
                list.add(new StringField("hhc_comments", "Comments"));
                return list;
            }

            @Override
            public List<FieldWithColor> getScoreFields() {
                List<FieldWithColor> list = new ArrayList<FieldWithColor>();
                list.add(new FieldWithColor("hhc_ov_experience", "Overall Experience"));
                list.add(new FieldWithColor("hhc_ov_return", "Return to property"));
                list.add(new FieldWithColor("hhc_ov_recommend", "Recommend"));
                return list;
            }
        };
    }

}
