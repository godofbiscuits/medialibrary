package com.medallia.mobile.filter;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import tiny.Empty;
import tiny.Strings;

import com.medallia.mobile.RequestParameterMap;
import com.medallia.mobile.RequestParameterMap.Param;

/** Helper class for the unit parameter {@link Param#unitid}. */
public class UnitParam {
	private String unitid;
	
	public UnitParam(String unitid) {
		this.unitid = unitid;
	}

	public UnitParam(RequestParameterMap req) {
		this(req.get(Param.unitid));
	}

	public boolean isAll() { return unitid == null || "all".equals(unitid); }
	
	public boolean isUnit() { return unitid.startsWith(UnitFilter.UNIT_PREFIX); }
	
	public boolean isUnitGroup() { return unitid.startsWith(UnitFilter.UNIT_GROUP_PREFIX); }
	
	public String unitId() { return unitid.substring(UnitFilter.UNIT_PREFIX.length()); }
	
	public String unitGroupId() { return unitid.substring(UnitFilter.UNIT_GROUP_PREFIX.length()); }

	public List<String> getUnitIds() { return Strings.split(unitId(), "|"); }
	
	/**
	 * @return Set of all individual units included in the unit parameter (which can be either a single unit,
	 * multiple units or a unit group).
	 */
	public Set<String> getAllUnits(Map<String, Set<String>> unitsByUG) {
		if (isUnit()) {
			List<String> uids = getUnitIds();
	    	if (uids.size() > 1) {
	    		return Empty.hashSet(uids);
	    	} else {
	    		return Collections.singleton(unitId());
	    	}
		} else {
			return unitsByUG.get(unitGroupId());
		}
	}
}