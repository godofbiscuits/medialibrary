package com.medallia.mobile.test.framework.requests;

/**
 * Defines the basic structure of a Request;
 * 
 * @author fernando
 */
public interface MobileRequest {
	
	/**
	 * Returns the request that is ready to be sent to the mobile servlet.
	 */
	public String getRequest();
	
	/**
	 * Gets the company name of the request.
	 */
	public String getCompany();
	
	/**
	 * Gets the user's password.
	 */
	public String getPassword();
	
	/**
	 * Gets the user's login.
	 */
	public String getUser();

	/**
	 * Adds an extra parameter to the request.
	 */
	public MobileRequest addParameter(String name, String value);
}
