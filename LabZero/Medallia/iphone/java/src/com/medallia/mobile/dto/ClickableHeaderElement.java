package com.medallia.mobile.dto;

/**
 * 
 * Warning: These enum names are used on the Objective-C side,
 *          so do not modify without updating DataFormat.h as well.
 */
public enum ClickableHeaderElement {
	CHNone,
	CHUnitScope,
	CHCustomerSegment,
	CHTimePeriod,
	CHScoreSelector,
}
