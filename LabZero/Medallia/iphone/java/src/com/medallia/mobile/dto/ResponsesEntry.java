package com.medallia.mobile.dto;
import java.util.*;
import com.medallia.mobile.*;
import com.medallia.mobile.dto.*;
import com.medallia.mobile.dto.builders.*;
import com.medallia.mobile.color.UIColor;

// These files are generated automatically from declarations in DataFormat.h; see convert.pl for details.
//
// Do *not* modify this file directly!


/**
 *  One entry in the list of responses. Contains only the data needed for the list.
 */
@SuppressWarnings("unused")
public interface ResponsesEntry extends Plist.Plistable {

	/**
	 *  Some persistent identifier for this record
	 */
	@Required String uniqueIdentifier();

	/**
	 *  Adjacent records with the same header will be grouped together. At the moment we use the survey date here; that could change.
	 */
	@Required String sectionHeader();

	/**
	 *  The main score for this survey, e.g. "7"
	 */
	@Required String score();

	/**
	 *  The name of the alert, if there is an alert for this survey. E.g. "R1"
	 */
	String alert();

	/**
	 *  The header to show for each response, typically the customer name, e.g. "John Doe"
	 */
	@Required String name();

	/**
	 *  An optional sub-header to show under the main header, above the comment. E.g. "Palo Alto Store"
	 */
	String subheader();

	/**
	 *  The comment of the survey, e.g. "My iPod didn't have a Centronics connector. Boo!"
	 */
	String text();

	/**
	 *  URL of details page; that page should return an HTML page
	 */
	@Required String detailsURL();

	/**
	 *  Color for the score background; the text is white, so use a darkish shade.
	 */
	@Required UIColor scoreColor();


}
