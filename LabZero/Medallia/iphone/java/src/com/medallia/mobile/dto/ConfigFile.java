package com.medallia.mobile.dto;
import java.util.*;
import com.medallia.mobile.*;
import com.medallia.mobile.dto.*;
import com.medallia.mobile.dto.builders.*;
import com.medallia.mobile.color.UIColor;

// These files are generated automatically from declarations in DataFormat.h; see convert.pl for details.
//
// Do *not* modify this file directly!


/**
 *  This is the type of the main file sent to the client upon login.
 */
@SuppressWarnings("unused")
public interface ConfigFile extends Plist.Plistable {

	/**
	 *  The (shared) lists of values such as property names, age groups, etc (alternative sets)
	 */
	@Required List<AltTable> tables();

	/**
	 *  Some global user identifier -- used to keep different users' settings separate
	 */
	@Required String userKey();

	/**
	 *  All the possible widget types the user may install on the scores dashboard (scoreboard)
	 */
	@Required List<WidgetType> scoresWidgetTypes();

	/**
	 *  All the possible widget types the user may install on the ranker
	 */
	@Required List<WidgetType> rankerWidgetTypes();

	/**
	 *  The initial list of scores widgets (installed if the user has no widgets)
	 */
	@Required List<WidgetInstance> initialScoresWidgets();

	/**
	 *  The initial list of ranker widgets (installed if the user has no widgets)
	 */
	@Required List<WidgetInstance> initialRankerWidgets();

	/**
	 *  Config used to set filters for the Response Investigator
	 */
	@Required WidgetType responsesFilterConfig();

	/**
	 *  Default values for the RI filter
	 */
	@Required WidgetInstance initialResponseFilter();

	/**
	 *  Number of open alerts
	 */
	@Required String nrOpenAlerts();


}
