package com.medallia.mobile;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.FieldPosition;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/** Common static helper methods and constants. */ 
public class Common {
	/** MMM d */
	public static final DateFormat PRETTY_DF = synch(new SimpleDateFormat("MMM d"));
	
	/** UTC datetime with timezone, used by iPhone */
	public static final DateFormat UTC_DF = synch(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z"));
	
	/** Format to use in note log messages */
	public static final DateFormat LOG_DF = synch(new SimpleDateFormat("M/d/yy HH:mm"));
	
	/** Number of milliseconds in a day. */
	public final static long MS_PER_DAY = 24 * 60 * 60 * 1000;

	/** Wrap a DateFormat in a synchronizer, so it can be safely shared between threads. */
	public static DateFormat synch(final DateFormat d) {
		return new DateFormat() {
			@Override
			public synchronized Date parse(String source, ParsePosition pos) {
				return d.parse(source, pos);
			}
			@Override
			public synchronized StringBuffer format(Date date, StringBuffer toAppendTo,
					FieldPosition fieldPosition) {
				return d.format(date, toAppendTo, fieldPosition);
			}		
		};
	}
	
    /**
     * Important: this needs to replace space with %20, not +
     * The reason is that we make mailto: urls for the iphone,
     * and the iphone url decode function does not replace +
     * with space. (Similarly, the iphone encode function does not
     * replace space with + either...)
     */
    public static String urlencode(String url) {
        try {
            return URLEncoder.encode(url, "utf8").replace("+", "%20");
        } catch (UnsupportedEncodingException e) {
            throw new AssertionError();           
        }
    }
    
    /** This is XML, not HTML. Do *not* replace this with something that will insert 
     * entities like &aring; into the stream -- the client will explode. */
    private static final Pattern xmlBad = Pattern.compile("[<>&]");
    
    /** @return Escaped version of the given string that is XML compliant. */
    public static String escapeXML(String s) {
        StringBuffer sb = new StringBuffer();
        Matcher m = xmlBad.matcher(s);
        while (m.find()) {
            m.appendReplacement(sb, "&#");
            sb.append((int) m.group().charAt(0)).append(';');           
        }
        m.appendTail(sb);
        return sb.toString();
    }
}
