package com.medallia.mobile.test.tests;

import java.util.List;

import com.medallia.api.datamodel.Timeperiod;
import com.medallia.mobile.dto.Alt;
import com.medallia.mobile.dto.AltSection;
import com.medallia.mobile.dto.AltTable;
import com.medallia.mobile.dto.ConfigFile;
import com.medallia.mobile.dto.builders.Build.Constants;
import com.medallia.mobile.filter.UnitFilter;
import com.medallia.mobile.test.framework.MobileTestCase;
import com.medallia.mobile.test.framework.config.Environment;
import com.medallia.mobile.test.framework.config.ExpressEnvironment;
import com.medallia.mobile.test.framework.config.MobileEnvironment;
import com.medallia.mobile.test.framework.requests.MobileRequest;
import com.medallia.mobile.test.framework.requests.MobileRequestBuilder;

/**
 * Module that contains several tests that exercise the Login Request.
 * 
 * @author fernando
 */
@Environment(express=ExpressEnvironment.EARTH, mobile=MobileEnvironment.LOCALHOST)
public class LocalLoginRequestTest extends MobileTestCase {
			
	private MobileRequest loginRequest;
	
	/**
	 * Tests that all the Unit and UnitGroups are present and in the right section.
	 * 
	 * <p> Preconditions:
	 * <ul>
	 * 	<li> Earth has 3 Unit Groups: Countries, Hotels and Market.
	 *  <li> There are units associated with Small, Medium and Large market.
	 *  <li> There are units associated with Aruba, Argentina, Norway, Jamaica and USA.
	 * </ul> There are 6 units available: Albuquerque, Burgess, Burgess2, Cordoba, HQ, HQ2, 
	 *       Kingston, New New York, Oranjestad, Oslo, Oslo2, Quahog, South Park and Springfield.
	 * 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Send a valid login request and parse the response.
	 * 	<li> Asserts that the response contains the "UNIT" section.
	 * 	<li> Asserts that the UNIT section contains the "all" Alternative, plus one alternative for 
	 * 		 UnitGroup that is currently associated with at least a Unit.
	 *  <li> Asserts that the response contains the Country Alternative, with all the used
	 *  	 countries, ordered alphabetically.
	 *  <li> Asserts that the response contains the Market Alternative, with all the used markets,
	 *       ordered alphabetically.
	 *  <li> Asserts that the response contains the Hotel Alternative, with all the units, ordered
	 *       alphabetically.
	 * </ul>
	 */
	public void testUnitAndUnitGroupsAreShownInRightSection() throws Exception {
		loginRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).version(Constants.FIRST_COMPATIBLE_CLIENT_VERSION)
			.buildLoginRequest();
		
		ConfigFile loginResponse = getLoginResponse(loginRequest);

		List<AltTable> tables = loginResponse.tables();
			
		AltTable units = tables.get(3);
		assertEquals("UNIT", units.key());
		List<AltSection> unitSections = units.sections();
		assertEquals(4, unitSections.size());
		
		Alt all = unitSections.get(0).alternatives().get(0);
		assertEquals("All", all.value());
		
		Alt altCountry = unitSections.get(1).alternatives().get(0);
		assertEquals("Country", altCountry.value());
		assertEquals(4, altCountry.table().sections().size());
		
		checkSection(altCountry.table().sections().get(0), "A", 2, "Argentina", "Aruba");
		checkSection(altCountry.table().sections().get(1), "J", 1, "Jamaica");
		checkSection(altCountry.table().sections().get(2), "N", 1, "Norway");
		checkSection(altCountry.table().sections().get(3), "U", 6, "USA");
		
		Alt altMarket = unitSections.get(2).alternatives().get(0);
		assertEquals("Market", altMarket.value());
		assertEquals(3, altMarket.table().sections().size());
		
		checkSection(altMarket.table().sections().get(0), null, 1, "Large");
		checkSection(altMarket.table().sections().get(1), null, 1, "Medium");
		checkSection(altMarket.table().sections().get(2), null, 1, "Small");
		
		Alt altHotel = unitSections.get(3).alternatives().get(0);
		assertEquals("Hotel", altHotel.value());
		assertEquals(9, altHotel.table().sections().size());
		checkSection(altHotel.table().sections().get(0), "A", 1, "Albuquerque");
		checkSection(altHotel.table().sections().get(1), "B", 2, "Burgess", "Burgess2");
		checkSection(altHotel.table().sections().get(2), "C", 1, "Cordoba");
		checkSection(altHotel.table().sections().get(3), "H", 2, "HQ", "HQ2");
		checkSection(altHotel.table().sections().get(4), "K", 1, "Kingston");
		checkSection(altHotel.table().sections().get(5), "N", 1, "New New York");
		checkSection(altHotel.table().sections().get(6), "O", 3, "Oranjestad", "Oslo", "Oslo2");
		checkSection(altHotel.table().sections().get(7), "Q", 1, "Quahog");
		checkSection(altHotel.table().sections().get(8), "S", 2, "South Park", "Springfield");
	}
	
	/**
	 * Tests that all the ranker questions are being shown.
	 * 
	 * <p> Preconditions:
	 * <ul>
	 * 	<li> Earth only contains two ranker questions: Overall Experience and Overall Service.
	 * </ul>
	 * 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Send a valid login request and parse the response.
	 * 	<li> Asserts that the response contains the "RANKER_QUESTION" Section.
	 *  <li> Asserts that the correct questions are there.
	 * </ul>
	 */
	public void testRankerQuestionShowsCorrectInfo() throws Exception {
		loginRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).version(Constants.FIRST_COMPATIBLE_CLIENT_VERSION)
			.buildLoginRequest();
		ConfigFile loginResponse = getLoginResponse(loginRequest);
		List<AltTable> tables = loginResponse.tables();

		AltTable rankerQuestion = tables.get(2);
		assertEquals("RANKER_QUESTION", rankerQuestion.key());
		assertEquals(2, rankerQuestion.sections().get(0).alternatives().size());
		assertEquals("earth_ov_experience", rankerQuestion.sections().get(0).alternatives().get(0).key());
		assertEquals("Overall Experience", rankerQuestion.sections().get(0).alternatives().get(0).value());
		assertEquals("earth_ov_service", rankerQuestion.sections().get(0).alternatives().get(1).key());
		assertEquals("Overall Service", rankerQuestion.sections().get(0).alternatives().get(1).value());
	}
	
	/**
	 * Tests that all the Time Periods are present and in the correct section.
	 * 
	 * <p> Preconditions:
	 * <ul>
	 *  <li> Earth contains 19 Time Periods.
	 * </ul>
	 * 
 	 * <p> Steps:
	 * <ul>
	 * 	<li> Send a valid login request and parse the response.
	 * 	<li> Assert that the response contains the "TIME_PERIOD" section.
	 *  <li> Queries Express to get all the Time periods.
	 *  <li> Asserts that all the time periods are present in the response.
	 * </ul>
	 */
	public void testTimePeriodsSectionContainsCorrectInfo() throws Exception {
		loginRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).version(Constants.FIRST_COMPATIBLE_CLIENT_VERSION)
			.buildLoginRequest();
		ConfigFile loginResponse = getLoginResponse(loginRequest);
		List<AltTable> tables = loginResponse.tables();
		AltTable timePeriods = tables.get(1);
		assertEquals("TIME_PERIOD", timePeriods.key());
		AltSection section = timePeriods.sections().get(0);
		List<Timeperiod> timePeriodsList = getQuerier().getTimePeriods();
	
		assertEquals(timePeriodsList.size(), section.alternatives().size());
		int index = 0;
		for(Alt alt : section.alternatives()) {
			assertEquals(timePeriodsList.get(index).id, alt.key());
			assertEquals(timePeriodsList.get(index).name, alt.value());
			index++;
		}
	}
		
	/**
	 * Tests that the Ranker Unit section contains the right info.
	 * 
	 * <p> Preconditions:
	 * <ul>
	 * </ul>
	 * 
 	 * <p> Steps:
	 * <ul>
	 * </ul>
	 */
	public void testRankerUnitSectionsShowsCorrectInfo() throws Exception {
		loginRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).version(Constants.FIRST_COMPATIBLE_CLIENT_VERSION)
			.buildLoginRequest();
		ConfigFile loginResponse = getLoginResponse(loginRequest);
		List<AltTable> tables = loginResponse.tables();
		AltTable rankerUnit = tables.get(0);
		assertEquals("ranker." + UnitFilter.UNIT_PARENT_ID, rankerUnit.key());		
		assertEquals(3, rankerUnit.sections().size());
		assertEquals("All", rankerUnit.sections().get(0).alternatives().get(0).value());
		assertEquals("Country", rankerUnit.sections().get(1).alternatives().get(0).value());
		assertEquals("Market", rankerUnit.sections().get(2).alternatives().get(0).value());
		
		assertEquals(4, rankerUnit.sections().get(1).alternatives().get(0).table().sections().size());
		
		assertEquals(2, rankerUnit.sections().get(1).alternatives().get(0).table().sections().get(0).alternatives().size());
		assertEquals("A", rankerUnit.sections().get(1).alternatives().get(0).table().sections().get(0).header());
		assertEquals("Argentina", rankerUnit.sections().get(1).alternatives().get(0).table().sections().get(0).alternatives().get(0).value());
		assertEquals("Aruba", rankerUnit.sections().get(1).alternatives().get(0).table().sections().get(0).alternatives().get(1).value());
		
		assertEquals(1, rankerUnit.sections().get(1).alternatives().get(0).table().sections().get(1).alternatives().size());
		assertEquals("J", rankerUnit.sections().get(1).alternatives().get(0).table().sections().get(1).header());
		assertEquals("Jamaica", rankerUnit.sections().get(1).alternatives().get(0).table().sections().get(1).alternatives().get(0).value());

		assertEquals(1, rankerUnit.sections().get(1).alternatives().get(0).table().sections().get(2).alternatives().size());
		assertEquals("N", rankerUnit.sections().get(1).alternatives().get(0).table().sections().get(2).header());
		assertEquals("Norway", rankerUnit.sections().get(1).alternatives().get(0).table().sections().get(2).alternatives().get(0).value());

		assertEquals(6, rankerUnit.sections().get(1).alternatives().get(0).table().sections().get(3).alternatives().size());
		assertEquals("U", rankerUnit.sections().get(1).alternatives().get(0).table().sections().get(3).header());
		assertEquals("USA", rankerUnit.sections().get(1).alternatives().get(0).table().sections().get(3).alternatives().get(0).value());
		assertEquals("California", rankerUnit.sections().get(1).alternatives().get(0).table().sections().get(3).alternatives().get(1).value());
		assertEquals("Colorado", rankerUnit.sections().get(1).alternatives().get(0).table().sections().get(3).alternatives().get(2).value());
		assertEquals("Florida", rankerUnit.sections().get(1).alternatives().get(0).table().sections().get(3).alternatives().get(3).value());
		assertEquals("New York", rankerUnit.sections().get(1).alternatives().get(0).table().sections().get(3).alternatives().get(4).value());
		assertEquals("Pennsylvania", rankerUnit.sections().get(1).alternatives().get(0).table().sections().get(3).alternatives().get(5).value());
		fail("is the last part right????");
	}
	
	/**
	 * Tests that it is not possible to login with an unknown company. 
	 * 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Sends a request with an unknown company.
	 * 	<li> Checks that the request does not go through and shows a proper message.
	 * </ul>
	 */
	public void testLoginRequestFailsIfCompanyUnknown() throws Exception {
		loginRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).company("unknownCompany")
			.version(Constants.FIRST_COMPATIBLE_CLIENT_VERSION).buildLoginRequest();
		assertTextPresent(getResponse(loginRequest), "Unknown company");
	}
	
	/**
	 * Tests that it is not possible to login with a wrong password.
	 * 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Sends a request with wrong password.
	 * 	<li> Checks that the request fails and shows a proper message.
	 * </ul> 	
	 */ 
	public void testLoginRequestFailsIfWrongPassword() throws Exception {
		loginRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).password("wrongPassword")
			.version(Constants.FIRST_COMPATIBLE_CLIENT_VERSION).buildLoginRequest();
		assertTextPresent(getResponse(loginRequest), "Not authorized");
	}
	
	/**
	 * Tests that it is not possible to login with a non existent user. 
	 * 	 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Sends a request with a user that does not exist.
	 * 	<li> Checks that the request fails with a proper message.
	 * </ul>
	 */
	public void testLoginRequestFailsIfUserDoesNotExist() throws Exception {
		loginRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).user("nonExistentUser")
		.version(Constants.FIRST_COMPATIBLE_CLIENT_VERSION).buildLoginRequest();
		assertTextPresent(getResponse(loginRequest), "Not authorized");
	}
	
	/**
	 * Tests that it is not possible to login with a null user. 
	 * 
	 * <p> Steps:
	 * <ul> 
	 * 	<li> Sends a request with a null user.
	 * 	<li> Checks that the request does not go through and shows a proper message.
	 * </ul>
	 */
	public void testLoginRequestFailsWithNullUser() throws Exception {
		loginRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).user(null)
			.version(Constants.FIRST_COMPATIBLE_CLIENT_VERSION).buildLoginRequest();
		assertTextPresent(getResponse(loginRequest), "Not authorized");
	}
	
	/**
	 * Tests that it is not possible to login with a null password. 
	 * 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Sends a request with null password.
	 * 	<li> Checks that the request fails and shows a proper message.
	 * </ul> 
	 */	
	public void testLoginRequestFailsWithNullPassword() throws Exception {
		loginRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).password(null)
			.version(Constants.FIRST_COMPATIBLE_CLIENT_VERSION).buildLoginRequest();
		assertTextPresent(getResponse(loginRequest), "Not authorized");
	}

	/**
	 * Tests that it is not possible to login with both a null user and a null password.
	 * 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Sends a request with both null user and null password.
	 * 	<li> Checks that the request fails and shows a proper message.
	 * </ul>
	 */
	public void testLoginRequestFailsWithNullPasswordAndNullUser() throws Exception {
		loginRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).password(null).user(null)
			.version(Constants.FIRST_COMPATIBLE_CLIENT_VERSION).buildLoginRequest();
		assertTextPresent(getResponse(loginRequest), "Not authorized");
	}
	
	/**
	 * Tests that it is not possible to login with a null company 
	 *
	 * <p> Steps:
	 * <ul>
	 * 	<li> Sends a request with a null company.
	 * 	<li> Checks that the request fails and shows a proper message.
	 * </ul>
	 */
	public void testLoginRequestFailsWithNullCompany() throws Exception {
		loginRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).company(null)
			.version(Constants.FIRST_COMPATIBLE_CLIENT_VERSION).buildLoginRequest();
		assertTextPresent(getResponse(loginRequest), "Unknown company");
	}
	
	/**
	 * Tests that it is not possible to login with an old version. 
	 * 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Send a request with an old version.
	 * 	<li> Check that the request fails and shows a proper message.
	 * </ul>
	 */
	public void testLoginRequestFailsIfOldVersion() throws Exception {
		loginRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).version(1).buildLoginRequest();
		assertTextPresent(getResponse(loginRequest), "This version of the application is outdated; please upgrade.");
	}
	
	/**
	 * Checks that one section contains the right header, and the right number of alternatives,
	 * and also that each alternative has the correct value.
	 * 
	 * @param section the section to be used.
	 * @param header contains what letter the section should contain.
	 * @param numElements the number of alternatives that the section should contain.
	 * @param elementNames the names of all the alternatives.
	 */
	private void checkSection(AltSection section, String header, int numElements, String... elementNames) {
		assertEquals(header, section.header());
		assertEquals(numElements, section.alternatives().size());
		Alt altElement;
		int index = 0;
		for(String elementName : elementNames) {
			altElement = section.alternatives().get(index);
			assertEquals(elementName, altElement.value());
			index++;
		}
	}
}
