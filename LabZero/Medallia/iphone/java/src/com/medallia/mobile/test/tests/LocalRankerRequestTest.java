package com.medallia.mobile.test.tests;

import com.medallia.mobile.dto.RankerData;
import com.medallia.mobile.filter.UnitFilter;
import com.medallia.mobile.test.framework.MobileTestCase;
import com.medallia.mobile.test.framework.beans.Id;
import com.medallia.mobile.test.framework.config.Environment;
import com.medallia.mobile.test.framework.config.ExpressEnvironment;
import com.medallia.mobile.test.framework.config.MobileEnvironment;
import com.medallia.mobile.test.framework.requests.MobileRequest;
import com.medallia.mobile.test.framework.requests.MobileRequestBuilder;

/**
 * Scenario Suite that contains several tests the exercise the ranker request
 */
@Environment(express=ExpressEnvironment.EARTH, mobile=MobileEnvironment.LOCALHOST)
public class LocalRankerRequestTest extends MobileTestCase {

	private MobileRequest rankerRequest;
	
	/**
	 * Tests that the selected field is properly working depending on the user.
	 * 
	 * <p> Preconditions:
	 * <ul>
	 * 	<li> The user mobiletest can only select the units from Norway.
	 * 	<li> Kristian can see all.
	 * 	<li> Timeperiod 253 corresponds to Q2 2009.
	 * 	<li> There are 5 units with data on that period.
	 * </ul>
	 * 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Send a request with user "mobiletest".
	 *  <li> Check that the request contains the 5 units, but only Oslo and Oslo2 can be selected.
	 *  <li> Send a request with user "kristian".
	 *  <li> Check that the request contains the 5 units, and kristian can select all the 5.
	 * </ul>
	 */
	public void testSelectedFiltersDependingOnUser() throws Exception {
		rankerRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).scope(UnitFilter.UNIT_PARENT_ID)
			.user("mobiletest").password("test1234")
			.rankerRequest("earth_ov_service").unit("all").timePeriod(new Id(253)).buildRankerRequest();
		
		RankerData response = getRankerDataRepsonse(rankerRequest);
		assertEquals(5, response.entries().size());
		assertEquals(null, response.entries().get(0).selected());
		assertEquals("HQ", response.entries().get(0).name());
		assertEquals("1", response.entries().get(1).selected());
		assertEquals("Oslo2", response.entries().get(1).name());
		assertEquals(null, response.entries().get(2).selected());
		assertEquals("Burgess", response.entries().get(2).name());
		assertEquals("1", response.entries().get(3).selected());
		assertEquals("Oslo", response.entries().get(3).name());
		assertEquals(null, response.entries().get(4).selected());
		assertEquals("Burgess2", response.entries().get(4).name());

		rankerRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).scope(UnitFilter.UNIT_PARENT_ID)
			.rankerRequest("earth_ov_service").unit("all").timePeriod(new Id(253)).buildRankerRequest();
		
		response = getRankerDataRepsonse(rankerRequest);
		assertEquals(5, response.entries().size());
		assertEquals("1", response.entries().get(0).selected());
		assertEquals("HQ", response.entries().get(0).name());
		assertEquals("1", response.entries().get(1).selected());
		assertEquals("Oslo2", response.entries().get(1).name());
		assertEquals("1", response.entries().get(2).selected());
		assertEquals("Burgess", response.entries().get(2).name());
		assertEquals("1", response.entries().get(3).selected());
		assertEquals("Oslo", response.entries().get(3).name());
		assertEquals("1", response.entries().get(4).selected());
		assertEquals("Burgess2", response.entries().get(4).name());

	}
	
	/**
	 * Tests that the units are ordered by score
	 * 
	 * <p> Preconditions:
	 * <ul>
	 * 	<li> Timeperiod 253 corresponds to Q2 2009.
	 * 	<li> There are 5 units with data on that period.
	 * </ul>
	 * 
	 * <p> Steps:
	 * <ul>
	 *  <li> Send a request.
	 *  <li> Check that the request contains the 5 units, and all are ordered by score.
	 * </ul>
	 */
	public void testResultsAreOrderedByRank() throws Exception {
		rankerRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).scope(UnitFilter.UNIT_PARENT_ID)
			.user("mobiletest").password("test1234")
			.rankerRequest("earth_ov_service").unit("all").timePeriod(new Id(253)).buildRankerRequest();
		
		RankerData response = getRankerDataRepsonse(rankerRequest);
		assertEquals(5, response.entries().size());

		double first = response.entries().get(0).scoreNumeric();
		double second = response.entries().get(1).scoreNumeric();
		double third = response.entries().get(2).scoreNumeric();
		double fourth = response.entries().get(3).scoreNumeric();
		double fifth = response.entries().get(4).scoreNumeric();
		assertTrue(first >= second);
		assertTrue(second >= third);
		assertTrue(third >= fourth);
		assertTrue(fourth >= fifth);
	}
	
	/**
	 * Tests that it is possible to filter by unit.
	 * 
	 * <p> Preconditions:
	 * <ul>
	 * 	<li> Timeperiod 180 corresponds to Year 2009.
	 * 	<li> There are 5 units with data on that period, but only 2 are from Norway.
	 * </ul>
	 * 
	 * <p> Steps:
	 * <ul>
	 *  <li> Send a request.
	 *  <li> Check that the request contains only Oslo and Oslo2.
	 * </ul>
	 */
	public void testFilterByUnitWorks() throws Exception {
		rankerRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).scope(UnitFilter.UNIT_PARENT_ID)
			.user("mobiletest").password("test1234")
			.rankerRequest("earth_ov_service").unit(UnitFilter.UNIT_GROUP_PREFIX + "norway")
			.timePeriod(new Id(180)).buildRankerRequest();
		RankerData response = getRankerDataRepsonse(rankerRequest);
		assertEquals(2, response.entries().size());
		assertEquals("Oslo2", response.entries().get(0).name());
		assertEquals("Oslo", response.entries().get(1).name());
	}
	
	/**
	 * Tests that the score shows the correct value.
	 * 
	 * <p> Preconditions:
	 * <ul>
	 * 	<li> Timeperiod 253 corresponds to Q2 2009.
	 * 	<li> There are 5 units with data on that period.
	 * 	<li> Q2 2009 for Oslo has an overall service of 7.23.
	 * </ul>
	 * 
	 * <p> Steps:
	 * <ul>
	 *  <li> Send a request.
	 *  <li> Check that the score of Oslo is 7.23.
	 * </ul>
	 */
	public void testScoreShowsCorrectValue() throws Exception {
		rankerRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).scope(UnitFilter.UNIT_PARENT_ID)
			.rankerRequest("earth_ov_service").unit("all").timePeriod(new Id(253)).buildRankerRequest();

		RankerData response = getRankerDataRepsonse(rankerRequest);
		assertEquals(7.235294117647059, response.entries().get(3).scoreNumeric());
		assertEquals("7.2", response.entries().get(3).score());
	}
	
	/**
	 * Tests that the difference shows the correct value.
	 * 
	 * <p> Preconditions:
	 * <ul>
	 * 	<li> Timeperiod 253 corresponds to Q2 2009.
	 * 	<li> There are 5 units with data on that period.
	 * 	<li> Q2 2009 for Oslo has an overall service of 7.23 and the overall service
	 * 		 of Q1 is 4.1.
	 * </ul>
	 * 
	 * <p> Steps:
	 * <ul>
	 *  <li> Send a request.
	 *  <li> Check that the difference is 3.1.
	 * </ul>
	 */
	public void testDifferenceShowsCorrectValue() throws Exception {
		rankerRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).scope(UnitFilter.UNIT_PARENT_ID)
			.rankerRequest("earth_ov_service").unit("all").timePeriod(new Id(253)).buildRankerRequest();

		RankerData response = getRankerDataRepsonse(rankerRequest);
		assertEquals(3.110294117647059, response.entries().get(3).differenceNumeric());
		assertEquals("+3.1", response.entries().get(3).difference());
	}

	/**
	 * Tests that is possible to filter by a particular unit group.
	 * 
	 * <p> Preconditions:
	 * <ul>
	 * 	<li> Timeperiod 180 corresponds to Year 2009.
	 * 	<li> There are 5 units with data on that period.
	 * 	<li> Only 3 of them are "small", market-wise.
	 * </ul>
	 * 
	 * <p> Steps:
	 * <ul>
	 *  <li> Send a request, filtering by market, and requesting only the units
	 *       that are "small"
	 *  <li> Check that the request contains 3 units.
	 * </ul>
	 */
	public void testIsPossibleToFilterByAParticularUnitGroup() throws Exception {
  		rankerRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).scope(UnitFilter.UNIT_GROUP_PREFIX + "Market")
  			.rankerRequest("earth_ov_service").unit(UnitFilter.UNIT_GROUP_PREFIX + "Small")
  			.timePeriod(new Id(180)).buildRankerRequest();

  		RankerData response = getRankerDataRepsonse(rankerRequest);
  		assertEquals(1, response.entries().size());
		assertEquals("Small", response.entries().get(0).name());
	}
	
	/**
	 * Tests that is possible to filter by a unit group, and only show the entries
	 * that belong to one option of another unit group.
	 * 
	 * <p> Preconditions:
	 * <ul>
	 * 	<li> Timeperiod 180 corresponds to Year 2009.
	 * 	<li> There are 5 units with data on that period.
	 * 	<li> Only 1 of them are "small" and in Norway.
	 * </ul>
	 * 
	 * <p> Steps:
	 * <ul>
	 *  <li> Send a request, filtering by country Norway, and requesting only the units
	 *       that are "small"
	 *  <li> Check that the request contains 1 unit.
	 * </ul>
	 */	
	public void testIsPossibleToFilterByDifferentUnitGroups() throws Exception {
  		rankerRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).scope(UnitFilter.UNIT_GROUP_PREFIX + "Market")
			.rankerRequest("earth_ov_service").unit(UnitFilter.UNIT_GROUP_PREFIX + "norway")
			.timePeriod(new Id(180)).buildRankerRequest();
		
  		RankerData response = getRankerDataRepsonse(rankerRequest);
  		assertEquals(2, response.entries().size());
		assertEquals("Small", response.entries().get(0).name());
		assertEquals("Large", response.entries().get(1).name());
	}
	
	/**
	 * Tests that it is not possible to get the request with an unknown company.
	 * 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Sends a request with an unknown company.
	 * 	<li> Checks that the request does not go through and shows a proper message.
	 * </ul>
	 */
	public void testRankerRequestFailsIfCompanyUnknown() throws Exception {
		rankerRequest = MobileRequestBuilder.getBuilder(
				getExpressEnvironment()).company("unknownCompany")
				.rankerRequest("earth_ov_service").unit("all").timePeriod(new Id(253)).buildRankerRequest();
		assertTextPresent(getResponse(rankerRequest), "Unknown company");
	}

	/**
	 * Tests that it is not possible to get the request with a wrong password.
	 * 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Sends a request with wrong password.
	 * 	<li> Checks that the request fails and shows a proper message.
	 * </ul> 	
	 */
	public void testrankerRequestFailsIfWrongPassword() throws Exception {
		rankerRequest = MobileRequestBuilder.getBuilder(
				getExpressEnvironment()).password("wrongPassword")
			.rankerRequest("earth_ov_service").unit("all").timePeriod(new Id(253)).buildRankerRequest();
		assertTextPresent(getResponse(rankerRequest), "Not authorized");
	}

	/**
	 * Tests that it is not possible to get the request with a non existent
	 * user.
	 * 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Sends a request with a user that does not exist.
	 * 	<li> Checks that the request fails with a proper message.
	 * </ul>
	 */
	public void testRankerRequestFailsIfUserDoesNotExist() throws Exception {
		rankerRequest = MobileRequestBuilder.getBuilder(
				getExpressEnvironment()).user("nonExistent")
			.rankerRequest("earth_ov_service").unit("all").timePeriod(new Id(253)).buildRankerRequest();
		assertTextPresent(getResponse(rankerRequest), "Not authorized");
	}

	/**
	 * Tests that it is not possible to get the request with a null user.
	 * 
	 * <p> Steps:
	 * <ul> 
	 * 	<li> Sends a request with a null user.
	 * 	<li> Checks that the request does not go through and shows a proper message.
	 * </ul>
	 */
	public void testRankerRequestFailsWithNullUser() throws Exception {
		rankerRequest = MobileRequestBuilder.getBuilder(
				getExpressEnvironment()).user(null)
				.rankerRequest("earth_ov_service").unit("all").timePeriod(new Id(253)).buildRankerRequest();

		assertTextPresent(getResponse(rankerRequest), "Not authorized");
	}

	/**
	 * Tests that it is not possible to get the request with a null password.
	 * 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Sends a request with null password.
	 * 	<li> Checks that the request fails and shows a proper message.
	 * </ul> 	
	 */	
	public void testRankerRequestFailsWithNullPassword() throws Exception {
		rankerRequest = MobileRequestBuilder.getBuilder(
				getExpressEnvironment()).password(null)
				.rankerRequest("earth_ov_service").unit("all").timePeriod(new Id(253)).buildRankerRequest();
		assertTextPresent(getResponse(rankerRequest), "Not authorized");
	}

	/**
	 * Tests that it is not possible to get the request with both a null user
	 * and a null password.
	 * 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Sends a request with both null user and null password.
	 * 	<li> Checks that the request fails and shows a proper message.
	 * </ul>
	 */
	public void testRankerRequestFailsWithNullPasswordAndNullUser()
			throws Exception {
		rankerRequest = MobileRequestBuilder.getBuilder(
				getExpressEnvironment()).user(null).password(null)
			.rankerRequest("earth_ov_service").unit("all").timePeriod(new Id(253)).buildRankerRequest();
		assertTextPresent(getResponse(rankerRequest), "Not authorized");
	}

	/**
	 * Tests that it is not possible to get the request with a null company
	 * 
	 * <p> Steps:
	 * <ul>
	 * 	<li> Sends a request with a null company.
	 * 	<li> Checks that the request fails and shows a proper message.
	 * </ul>
	 */
	public void testRankerRequestFailsWithNullCompany() throws Exception {
		rankerRequest = MobileRequestBuilder.getBuilder(
				getExpressEnvironment()).company(null)
				.rankerRequest("earth_ov_service").unit("all").timePeriod(new Id(253))
				.buildRankerRequest();
		assertTextPresent(getResponse(rankerRequest), "Unknown company");
	}
	
	/**
	 * Tests that it is not possible to get a request with an empty company.
	 *  
	 * <p> Steps:
	 * <ul>
	 * 	<li> Sends a request with an empty company.
	 * 	<li> Checks that the request fails and shows a proper message.
	 * </ul>
	 */
	public void testRankerRequestFailsWithEmptyCompany() throws Exception {
		rankerRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).company("")
					.rankerRequest("earth_ov_service").unit("all").timePeriod(new Id(253)).buildRankerRequest();

		assertTextPresent(getResponse(rankerRequest), "Unknown company");		
	}

	/**
	 * Tests that it is not possible to get a request with an empty user.
	 *  
	 * <p> Steps:
	 * <ul>
	 * 	<li> Sends a request with an empty user.
	 * 	<li> Checks that the request fails and shows a proper message.
	 * </ul>
	 */
	public void testRankerRequestFailsWithEmptyUser() throws Exception {
		rankerRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).user("")
					.rankerRequest("earth_ov_service").unit("all").timePeriod(new Id(253)).buildRankerRequest();

		assertTextPresent(getResponse(rankerRequest), "Not authorized");		
	}

	/**
	 * Tests that it is not possible to get a request with an empty password.
	 *  
	 * <p> Steps:
	 * <ul>
	 * 	<li> Sends a request with an empty password.
	 * 	<li> Checks that the request fails and shows a proper message.
	 * </ul>
	 */
	public void testRankerRequestFailsWithEmptyPassword() throws Exception {
		rankerRequest = MobileRequestBuilder.getBuilder(getExpressEnvironment()).password("")
					.rankerRequest("earth_ov_service").unit("all").timePeriod(new Id(253)).buildRankerRequest();

		assertTextPresent(getResponse(rankerRequest), "Not authorized");		
	}
}
