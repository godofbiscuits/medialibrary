package com.medallia.mobile;

import com.medallia.mobile.Plist.Plistable;

public class ErrorMessage implements Plistable {
	
	public String error;
	
	public ErrorMessage(String error) {
		this.error = error;
	}
	
	@Override
	public String toString() {
		return error;
	}

}
