package com.medallia.mobile.test.framework.querier.impl;

import com.medallia.mobile.test.framework.config.ExpressEnvironment;
import com.medallia.mobile.test.framework.querier.AbstractQuerier;
import com.medallia.mobile.test.framework.querier.Querier;

/**
 * Implementation of a {@link Querier} that calls QA Apis..
 * 
 * <p>
 * 	Shouldn't be used directly, {@link ExpressQuerier} should be used instead.
 * 
 * <p> Not implemented yet.
 * 
 * @author fernando
 */
public class QAApiQuerier extends AbstractQuerier {
	
	private QAApiQuerier(ExpressEnvironment express) {
		this.express = express;
	}
	
	static Querier createQuerier(ExpressEnvironment express) {
		return new QAApiQuerier(express);
	}
}
