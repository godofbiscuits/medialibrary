package com.medallia.mobile.test.framework.config;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Simple Annotation that contains the environments for setting up a test.
 * 
 * @author fernando
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Environment {
	
	/**
	 * Returns the ExpressEnvironment used. 
	 */
	ExpressEnvironment express();
	
	/**
	 * Returns the MobileEnvironment used.
	 */
	MobileEnvironment mobile();
}
