package com.medallia.mobile.dto;
import java.util.*;
import com.medallia.mobile.*;
import com.medallia.mobile.dto.*;
import com.medallia.mobile.dto.builders.*;
import com.medallia.mobile.color.UIColor;

// These files are generated automatically from declarations in DataFormat.h; see convert.pl for details.
//
// Do *not* modify this file directly!


/**
 *  A list of units/unit groups
 */
@SuppressWarnings("unused")
public interface RankerData extends Plist.Plistable {

	/**
	 *  "vs 6m" - explains the benchmark period we are comparing to
	 */
	@Required String benchTitle();

	/**
	 *  The list of units/unit groups
	 */
	@Required List<RankerUnit> entries();


}
