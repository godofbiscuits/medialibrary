package com.medallia.mobile.test.tests;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.medallia.mobile.test.framework.requests.MobileRequestBuilderTest;

public class AllMobileTests {

	/**
	 * @return test suite with all of the mobile tests
	 */
	public static Test suite() {
		TestSuite suite = new TestSuite("Tests for com.medallia.mobile");
		//$JUnit-BEGIN$
		suite.addTestSuite(LocalAlertsRequestTest.class);
		suite.addTestSuite(LocalDetailsRequestTest.class);
		suite.addTestSuite(LocalLoginRequestTest.class);
		suite.addTestSuite(LocalRankerRequestTest.class);
		suite.addTestSuite(LocalResponsesRequestTest.class);
		suite.addTestSuite(LocalScorecardRequestTest.class);
		suite.addTestSuite(LocalScoresRequestTest.class);
		suite.addTestSuite(MobileRequestBuilderTest.class);
		//$JUnit-END$
		return suite;
	}

}
