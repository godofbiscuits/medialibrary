package com.medallia.mobile.color;


public enum BackgroundColor {
	GREEN(UIColor.GREEN, "green"),
	YELLOW(UIColor.YELLOW, "yellow"),
	RED(UIColor.RED, "red"),
	;

	private final UIColor uiColor;
	private final String colorName;
	
	private BackgroundColor(UIColor uiColor, String colorName) {
		this.uiColor = uiColor;
		this.colorName = colorName;
	}
	
	public String getColorName() {
		return colorName;
	}

	public UIColor getUIColor() {
		return uiColor;
	}
}
