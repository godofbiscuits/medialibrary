package com.medallia.mobile.dto;
import java.util.*;
import com.medallia.mobile.*;
import com.medallia.mobile.dto.*;
import com.medallia.mobile.dto.builders.*;
import com.medallia.mobile.color.UIColor;

// These files are generated automatically from declarations in DataFormat.h; see convert.pl for details.
//
// Do *not* modify this file directly!


/**
 *  An alert action (e.g. "close alert")
 */
@SuppressWarnings("unused")
public interface AlertAction extends Plist.Plistable {

	/**
	 *  Button text
	 */
	@Required String text();

	/**
	 *  Alert action that will be sent back to the servlet and then forwarded to the api ("closed", "resolved", "in_progress")
	 */
	@Required String action();


}
