package com.medallia.mobile.companysettings;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.medallia.api.datamodel.SelectQueryWithBooleanFilter;
import com.medallia.api.orm.Field.StringField;
import com.medallia.mobile.DetailFields;
import com.medallia.mobile.DetailFieldsDefaultBase;
import com.medallia.mobile.FieldWithColor;
import com.medallia.mobile.RequestParameterMap;
import com.medallia.mobile.RequestParameterMap.LogonInfo;
import com.medallia.mobile.dto.AltSection;
import com.medallia.mobile.dto.ClickableHeaderElement;
import com.medallia.mobile.filter.DashboardFilter;
import com.medallia.mobile.filter.Filters;

public class CompanySettingsGlobal extends CompanySettingsDefaultBase {

	private static final FieldWithColor OVERALL_SAT = new FieldWithColor("b_g_overall_satisfaction", "Overall Satisfaction");
	private static final FieldWithColor RECOMMEND = new FieldWithColor("b_q_likely_to_recommend", "Recommend");
	
	public CompanySettingsGlobal(LogonInfo req) {
		super(req);
	}
	
	@Override protected String getBaseURI0() {
		return "https://edemo.medallia.com";
	}
	
	@Override protected String getCompany() { return "global"; }

	@Override
    public String getUnitTranslation() {
	    return "Property";
    }

	@Override public Date getStartTime() { return getDate(2007, 0, 1); }
	@Override public Date getTodayInternal() { return getDate(2008, 0, 25); }
	
	@Override public List<FieldWithColor> getScoreFields(String widgetRequestAction) {
		return Arrays.asList(OVERALL_SAT, RECOMMEND);
	}
	
	@Override public List<FieldWithColor> getScorecardFields() {
		return getScoreFields("");
	}
	
	@Override public FieldWithColor getMainScoreField() {
		return OVERALL_SAT;
	}
	
	// detailed response
	@Override public DetailFields getDetailFields() {
		return new DetailFieldsDefaultBase() {
			@Override public StringField getCommentField() {
				return new StringField("comments", "What could be improved");
			}
			@Override public List<FieldWithColor> getScoreFields() {
				return Arrays.asList(OVERALL_SAT, new FieldWithColor("b_q_likely_to_recommend", "Likelihood to recommend"));
			}
			@Override public String getHardCodedPhoneNr() { return "650-796-9698"; } // mr bennedich
		};
	}
	
	@Override
	public List<DashboardFilter> getShownScoreboardFilters(String widgetKey) {
		List<DashboardFilter> filters = super.getShownScoreboardFilters(widgetKey);
		
		filters.add(new DashboardFilter() {
							@Override public String getDefault() { return "all"; }
							@Override public String getId() { return Filters.ALT_ROLE; }
							@Override public String getKey() { return "role"; }
							@Override public String getName() { return "Functional role"; }
							@Override public void addFilter(RequestParameterMap req, SelectQueryWithBooleanFilter query) {
								String prop = req.getRaw(getKey());
								if (prop != null && !prop.equals("all"))
									query.addCondition(prop + " = 'Yes'");
							}
							@Override public List<AltSection> getSections() {
								return Filters.altList(
									"all", "All",
									"b_g_managing", "Managing the budget",
									"b_g_influencing", "Influencing selection",
									"b_g_selecting_product", "Selecting product or service",
									"b_g_ensuring", "Ensuring correct installation");
							}
							@Override public ClickableHeaderElement getHeaderElement() { return ClickableHeaderElement.CHCustomerSegment; }
						});
		
		filters.add(new DashboardFilter() {
							@Override public String getDefault() { return "all"; }
							@Override public String getId() { return Filters.ALT_YEARS_WORKED; }
							@Override public String getKey() { return "yearsworked"; }
							@Override public String getName() { return "Years working with Global"; }
							@Override public void addFilter(RequestParameterMap req, SelectQueryWithBooleanFilter query) {
								String prop = req.getRaw(getKey());
								if (prop != null && !prop.equals("all"))
									query.addCondition("b_g_years = '" + prop + "'");
							}
							@Override public List<AltSection> getSections() {
								return Filters.altList(
										"all", "All",
										"Less than 1 year", "Less than 1 year",
										"1 to 2 years", "1 to 2 years",
										"3 to 5 years", "3 to 5 years",
										"6 to 9 years", "6 to 9 years",
										"10 or more years", "10 or more years");
							}
							@Override public ClickableHeaderElement getHeaderElement() { return ClickableHeaderElement.CHNone; }
						});
		
		filters.add(new DashboardFilter() {
							@Override public String getDefault() { return "all"; }
							@Override public String getId() { return Filters.ALT_EMPLOYEES; }
							@Override public String getKey() { return "employees"; }
							@Override public String getName() { return "Number of employees"; }
							@Override public void addFilter(RequestParameterMap req, SelectQueryWithBooleanFilter query) {
								String prop = req.getRaw(getKey());
								if (prop != null && !prop.equals("all"))
									query.addCondition("b_g_how_many_employees = '" + prop + "'");
							}
							@Override public List<AltSection> getSections() {
								return Filters.altList(
										"all", "All",
										"<50 employees", "<50 employees",
										"51 to 200 employees", "51 to 200 employees",
										"200 to 500 employees", "200 to 500 employees",
										"501 to 1000 employees", "501 to 1000 employees",
										"1001 to 10000 employees", "1001 to 10000 employees",
										"10000+ employees", "10000+ employees");
							}
							@Override public ClickableHeaderElement getHeaderElement() { return ClickableHeaderElement.CHNone; }
						});
		
		return filters;
	}

}
