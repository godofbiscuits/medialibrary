package com.medallia.mobile.dashboard;

class ScoreAndBench {
	double score;
	double benchScore;
	
	ScoreAndBench(double score, double benchScore) {
		this.score = score;
		this.benchScore = benchScore;
	}

	@Override
	public String toString() {
		return String.format("%f, %f", score, benchScore);
	}
}