package com.medallia.mobile.dto;
import java.util.*;
import com.medallia.mobile.*;
import com.medallia.mobile.dto.*;
import com.medallia.mobile.dto.builders.*;
import com.medallia.mobile.color.UIColor;

// These files are generated automatically from declarations in DataFormat.h; see convert.pl for details.
//
// Do *not* modify this file directly!


/**
 *  Scorecard. Contains a summary of scores for a set of surveys matching a given search string, e.g. the name of an employee.
 */
@SuppressWarnings("unused")
public interface Scorecard extends Plist.Plistable {

	/**
	 *  The name of the scorecard (who/what the scorecard is for).
	 */
	@Required String scorecardName();

	/**
	 *  The number of responses over the past 12 months.
	 */
	@Required double numberOfResponses();

	/**
	 *  The list of scores for the past 12 months.
	 */
	@Required List<ScorecardScore> scores();

	/**
	 *  The lump of responses for this scorecard.
	 */
	@Required ResponsesLump responsesLump();


}
