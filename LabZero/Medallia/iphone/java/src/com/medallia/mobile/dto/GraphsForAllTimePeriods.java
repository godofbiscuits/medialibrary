package com.medallia.mobile.dto;
import java.util.List;

import com.medallia.mobile.Plist;
import com.medallia.mobile.dto.builders.Required;

// These files are generated automatically from declarations in DataFormat.h; see convert.pl for details.
//
// Do *not* modify this file directly!


/**
 *  Contains a list of graphs, one for each time period.
 */
@SuppressWarnings("unused")
public interface GraphsForAllTimePeriods extends Plist.Plistable {

	/**
	 *  List of graphs, one for each time period.
	 */
	@Required List<Graph> graph();


}
