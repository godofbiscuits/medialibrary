package com.medallia.mobile;

/** Contains information about a benchmark time period. */
public class BenchmarkTimeperiod {
	private final boolean usesBenchmark;
	
	private final TemporaryTimeperiod benchPeriod;
	
	/** Create new benchmark time period. */
	public BenchmarkTimeperiod(boolean usesBenchmark, TemporaryTimeperiod benchPeriod) {
		this.usesBenchmark = usesBenchmark;
		this.benchPeriod = benchPeriod;
	}
	
	/** @return whether benchmarking is used at all */
	public boolean usesBenchmark() { return usesBenchmark; }
	
	/**
	 * @return the time period used for benchmarking, or null if there is no benchmark time period
	 * (but it might still use benchmarking by creating the benchmark period heuristically)
	 */
	public TemporaryTimeperiod getBenchPeriod() { return benchPeriod; }
	
	/** @return Whether a regular benchmark period exists. */
	public boolean hasBenchPeriod() {
		return usesBenchmark && benchPeriod != null;
	}
	
	/** @return Whether to use heuristic benchmark period when regular period is missing. */
	public boolean hasHeuristicBenchmark() {
		return usesBenchmark && benchPeriod == null;
	}
	
	public String getShortName() {
		if (!usesBenchmark())
			return "";
		if (hasHeuristicBenchmark())
			return "xx";
		else
			return benchPeriod.getShortName();
	}
}