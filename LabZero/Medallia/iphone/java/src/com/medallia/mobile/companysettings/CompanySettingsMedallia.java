package com.medallia.mobile.companysettings;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.medallia.mobile.DetailFields;
import com.medallia.mobile.DetailFieldsDefaultBase;
import com.medallia.mobile.FieldWithColor;
import com.medallia.mobile.RequestParameterMap.LogonInfo;

public class CompanySettingsMedallia extends CompanySettingsDefaultBase {

	public CompanySettingsMedallia(LogonInfo req) {
		super(req);
	}
	
	@Override protected String getBaseURI0() {
		return "http://localhost:8080";
	}
	
	@Override protected String getCompany() { return "medallia"; }

	@Override public Date getStartTime() { return getDate(2005, 0, 1); }
	@Override public Date getTodayInternal() { return getDate(2005, 2, 31); }
	
	@Override public FieldWithColor getMainScoreField() {
		return new FieldWithColor("ov_experience", "Overall Experience");
	}
	
	@Override public List<FieldWithColor> getScoreFields(String widgetRequestAction) {
		return Arrays.asList(getMainScoreField(), new FieldWithColor("ov_service", "Overall Service"));
	}
	
	@Override public List<FieldWithColor> getScorecardFields() {
		return getScoreFields("");
	}
	
	// detailed response
	@Override public DetailFields getDetailFields() {
		return new DetailFieldsDefaultBase() {
			@Override public List<FieldWithColor> getScoreFields() {
				return CompanySettingsMedallia.this.getScoreFields(null);
			}
		};
	}

	@Override
    public String getUnitTranslation() {
	    return "Property";
    }
}
