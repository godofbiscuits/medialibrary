package com.medallia.mobile.dto;
import java.util.*;
import com.medallia.mobile.*;
import com.medallia.mobile.dto.*;
import com.medallia.mobile.dto.builders.*;
import com.medallia.mobile.color.UIColor;

// These files are generated automatically from declarations in DataFormat.h; see convert.pl for details.
//
// Do *not* modify this file directly!


/**
 *  An instance of a Widget -- the user may have three instances of the scoreboard, for, uhm, instance.
 */
@SuppressWarnings("unused")
public interface WidgetInstance extends Plist.Plistable {

	/**
	 *  The type of widget; this should match the key() of a WidgetType
	 */
	@Required String type();

	/**
	 *  Arguments; this is a map from ConfigPart.key() to values
	 */
	@Required Map<String,String> arguments();


}
