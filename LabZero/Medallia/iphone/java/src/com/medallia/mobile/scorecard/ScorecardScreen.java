package com.medallia.mobile.scorecard;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import junit.framework.Assert;

import com.medallia.api.datamodel.SelectQueryWithBooleanFilter;
import com.medallia.api.datamodel.SurveyAggregate;
import com.medallia.api.datamodel.SurveyAggregateQuery;
import com.medallia.mobile.FieldWithColor;
import com.medallia.mobile.RequestParameterMap;
import com.medallia.mobile.ScoreFormatter;
import com.medallia.mobile.RequestParameterMap.LogonInfo;
import com.medallia.mobile.RequestParameterMap.Param;
import com.medallia.mobile.color.BackgroundColor;
import com.medallia.mobile.companysettings.CompanySettings;
import com.medallia.mobile.dto.Scorecard;
import com.medallia.mobile.dto.ScorecardScore;
import com.medallia.mobile.dto.builders.Build;
import com.medallia.mobile.dto.builders.Build.ScorecardScoreBuilder;
import com.medallia.mobile.filter.UnitFilter;
import com.medallia.mobile.filter.UnitParam;
import com.medallia.mobile.responses.ResponsesScreen;

public class ScorecardScreen {
	private final CompanySettings config;
	private final LogonInfo logonInfo;
	private final RequestParameterMap req;
	private final List<FieldWithColor> fields;
	
	public ScorecardScreen(CompanySettings config, LogonInfo logonInfo, RequestParameterMap req) {
		this.config = config;
		this.logonInfo = logonInfo;
		this.req = req;
		this.fields = config.getScorecardFields();
	}

	public static void addTimeperiodFilter(CompanySettings config, SelectQueryWithBooleanFilter query) {
		Date today = config.getToday();
		Calendar calendarToday = new GregorianCalendar();
		calendarToday.setTime(today);
		calendarToday.add(Calendar.YEAR, -1);
		Date oneYearAgo = calendarToday.getTime();
		query.whereTimePeriod(oneYearAgo, today);
	}
	
	/**
	 * Due to a bug in the API, we cannot run count(*) together with NPS fields,
	 * so we have to run two separate queries, unfortunately.
	 * @param isCountStar if true, add COUNT_STAR, otherwise add the score fields
	 * @return
	 */
	private SurveyAggregateQuery buildQuery(boolean isCountStar) {
		SurveyAggregateQuery query = new SurveyAggregateQuery();
		if (isCountStar) {
			query.addField(SurveyAggregate.COUNTSTAR);
		} else {
			for (FieldWithColor f : fields)
				query.addField(f.avg());
		}
		addTimeperiodFilter(config, query);
		UnitFilter.applyUnitFilter(new UnitParam(req), query);
        String respFilter = req.get(Param.resp_filter);
        if (respFilter != null)
        	config.applyResponsesFilter(query, respFilter);
        String searchTerm = req.get(Param.response_text);
        Assert.assertTrue(Param.response_text + " is a required parameter for the scorecard", searchTerm != null && !searchTerm.isEmpty());
    	query.addTextSearch(searchTerm);
		return query;
	}

	public Scorecard get() {
		String scorecardName = req.get(Param.scorecard_name);
		List<ScorecardScore> scores = new ArrayList<ScorecardScore>();
		SurveyAggregate row = buildQuery(false).runSingleRowQuery(config.getConnection());
		for (FieldWithColor field : fields) {
			Double score = row.getDouble(field.avg());
			if (score == null) continue;
			if (score.isNaN()) continue;
			ScoreFormatter formatter = new ScoreFormatter(config, score, 0, false); // benchmark value is not used
			ScorecardScoreBuilder scoreBuilder = Build.scorecardScore().title(field.name).value(formatter.getScoreText());
			BackgroundColor color = field.chooseBackgroundColor(config, score);
			if (color != null) {
				scoreBuilder.scoreBackgroundColor(color.getColorName());
			}
			scores.add(scoreBuilder.get());
		}
		
		row = buildQuery(true).runSingleRowQuery(config.getConnection());
		int numberOfResponses = row.getInt(SurveyAggregate.COUNTSTAR);
		ResponsesScreen responsesScreen = new ResponsesScreen(config, logonInfo, req, true);
		return Build.scorecard().scores(scores)
				.numberOfResponses(numberOfResponses)
				.responsesLump(responsesScreen.get())
				.scorecardName(scorecardName).get();
	}

}
