package com.medallia.mobile.filter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import tiny.Strings;

import com.medallia.api.datamodel.SelectQueryWithBooleanFilter;
import com.medallia.api.datamodel.Unit;
import com.medallia.api.datamodel.UnitGroup;
import com.medallia.mobile.AlphanumCompareTool;
import com.medallia.mobile.companysettings.CompanySettings;
import com.medallia.mobile.dto.Alt;
import com.medallia.mobile.dto.AltSection;
import com.medallia.mobile.dto.builders.Build;

public class UnitFilter {

    private static FilterUnit merge(FilterUnit a, FilterUnit b) {
    	return new FilterUnit(a.id + "|" + b.id, a.name);
    }

    // the unit parent id is used in objective c code -- don't change here without changing there too
    public static final String UNIT_PARENT_ID = "_unit";
    
    public static final String UNIT_PREFIX = "u_";
    
    public static final String UNIT_GROUP_PREFIX = "ug_";
    
    private static String getMQLFilter(UnitParam unitParam) {
    	if (unitParam.isAll()) {
    		// include all units -- no filter
    		return null;
    	} else if (unitParam.isUnitGroup()) {
    		// unit group filter (single unit group)
    		return "group = '" + unitParam.unitGroupId() + "'";
    	} else {
    		// unit filter, either multiple or single units
    		List<String> uids = unitParam.getUnitIds();
	    	if (uids.size() > 1) {
	    		return "unitid IN('" + Strings.join("','", uids) + "')";
	    	} else {
				return "unitid = '" + unitParam.unitId() + "'";
	    	}
    	}
    }

    /** Add unit/unitgroup filter to the given query. Does not add anything if the unit parameter denotes all properties. */
    public static void applyUnitFilter(UnitParam unitParam, SelectQueryWithBooleanFilter query) {
		String filter = getMQLFilter(unitParam);
		if (filter != null)
			query.addCondition(filter);
    }
    
    /**
     * Apple specific - merging all units with the same name, because they
     * represent the same store.
     * @return the merged list of units
     */
    private static List<FilterUnit> mergeEqualName(List<FilterUnit> units) {
    	List<FilterUnit> mergedUnits = new ArrayList<FilterUnit>();
    	FilterUnit current = null;
    	for (FilterUnit unit : units) {
    		if (current == null || current.compareTo(unit) != 0) {
    			if (current != null) mergedUnits.add(current);
    			current = unit;
    		} else {
    			current = merge(current, unit);
    		}
    	}
    	if (current != null) mergedUnits.add(current);
    	return mergedUnits;
    }
    
    public static List<FilterUnit> getUnits(CompanySettings config) {
        List<FilterUnit> units = new ArrayList<FilterUnit>();
    	for (Unit unit : Unit.getUnits(config.getConnection(), false)) {
    		units.add(new FilterUnit(unit.getId(), unit.getName()));
    	}
    	Collections.sort(units);
    	return units;
    }

	/** @return All unit groups with the given unit group parent id. */
    private static List<FilterUnitGroup> getUnitGroups(List<UnitGroup> ugTable, String ugParentId) {
    	// build ug hash map to speed things up
    	Map<String, List<UnitGroup>> ugsByParent = new HashMap<String, List<UnitGroup>>();
		for (UnitGroup ugRow : ugTable) {
			List<UnitGroup> ugs = ugsByParent.get(ugRow.parent_unitgroupid);
			if (ugs == null)
				ugsByParent.put(ugRow.parent_unitgroupid, ugs = new ArrayList<UnitGroup>());
			ugs.add(ugRow);
		}
    	
    	// first find and sort top level alternatives
        List<FilterUnitGroup> topLevelUGs = new ArrayList<FilterUnitGroup>();
		for (UnitGroup topLevelUG : ugsByParent.get(ugParentId))
			topLevelUGs.add(new FilterUnitGroup(topLevelUG.unitgroupid, topLevelUG.name));
    	Collections.sort(topLevelUGs);

    	// now find, sort and insert children
        List<FilterUnitGroup> allUGs = new ArrayList<FilterUnitGroup>();
        for (FilterUnitGroup topLevelUG : topLevelUGs) {
			allUGs.add(topLevelUG);
	        if (ugsByParent.containsKey(topLevelUG.id)) {
		        List<FilterUnitGroup> childrenUGs = new ArrayList<FilterUnitGroup>();
					for (UnitGroup childUG : ugsByParent.get(topLevelUG.id))
						childrenUGs.add(new FilterUnitGroup(childUG.unitgroupid, childUG.name, 1));
				Collections.sort(childrenUGs);
				allUGs.addAll(childrenUGs);
	        }
        }
		
    	return allUGs;
    }
    
    private static abstract class UnitObject implements Comparable<UnitObject> {
    	public final String id;
    	public final String name;
    	
    	/** indentation level, used for hierarchies */
    	public final int level;
    	
    	public UnitObject(String id, String name, int level) {
    		this.id = id;
    		this.name = name;
    		this.level = level;
    	}
    	@Override public int compareTo(UnitObject b) {
    		return AlphanumCompareTool.compare(name, b.name, true);
    	}
		public String getMangledId() {
			return getManglePrefix() + id;
		}
		protected abstract String getManglePrefix();
    }
    
    public static class FilterUnit extends UnitObject {
    	public FilterUnit(String id, String name) {
    		super(id, name, 0);
    	}
		@Override protected String getManglePrefix() {
			return UNIT_PREFIX;
		}
    }
    
    public static class FilterUnitGroup extends UnitObject {
    	/** create unit group at given indentation level */
    	public FilterUnitGroup(String id, String name, int level) {
    		super(id, name, level);
    	}
    	/** create unit group at indentation level 0 */
    	public FilterUnitGroup(String id, String name) {
    		super(id, name, 0);
    	}
    	/** create unit group that is parent of all units */
    	public FilterUnitGroup(String name) {
    		super(UNIT_PARENT_ID, name, 0);
    	}
    	public boolean isUnitParent() {
    		return UNIT_PARENT_ID.equals(id);
    	}
		@Override public String getMangledId() {
			return isUnitParent() ? id : super.getMangledId();
		}
		@Override protected String getManglePrefix() {
			return UNIT_GROUP_PREFIX;
		}
		@Override public String toString() {
			return "<"+name+" ("+id+")>";
		}
    }

    private static List<AltSection> getUnitObjectSections(List<? extends UnitObject> unitObjects) {
        List<AltSection> sects = new ArrayList<AltSection>();
        AltSection current = null;
        for (UnitObject uo : unitObjects) {
            String group = uo.name.substring(0,1).toUpperCase();
            if (current == null || (uo.level == 0 && !current.header().toUpperCase().startsWith(group))) {
                current = Build.altSection().header(group).alternatives(new ArrayList<Alt>()).get();
                sects.add(current);
            }
            current.alternatives().add(Build.alt().key(uo.getMangledId()).value(uo.name).level(uo.level).table(Build.altTable().key("").sections(new ArrayList<AltSection>()).get()).get());
        }
        // if only one section or less than 10 alternatives, remove headers
        if (sects.size() == 1 || (!sects.isEmpty() && unitObjects.size() < 10)) {
            List<AltSection> noHeaders = new ArrayList<AltSection>();
            for (AltSection as : sects)
            	noHeaders.add(Build.altSection().alternatives(as.alternatives()).get());
            sects = noHeaders;
        }
        return sects;
    }
    
    private static List<AltSection> getUnitSections(CompanySettings config) {
        List<FilterUnit> units = getUnits(config);
        if (config.mergeEqualUnits())
        	units = mergeEqualName(units);
        return getUnitObjectSections(units);
    }
    
    private static List<AltSection> getUnitGroupSections(List<UnitGroup> ugTable, FilterUnitGroup ug) {
        return getUnitObjectSections(getUnitGroups(ugTable, ug.id));
    }

    /** @return True if the list of alt sections contains multiple alternatives. */
    public static boolean hasMultipleAlternatives(List<AltSection> sections) {
    	// return true if > 1 section, false if no sections
    	if (sections.size() != 1)
    		return !sections.isEmpty();
    	
    	// return false if the alternatives are not just one alt in a hierarchy
    	List<Alt> alts = sections.get(0).alternatives();
    	for (int i = 0; i < alts.size()-1; ++i)
    		if (alts.get(i).level() >= alts.get(i+1).level())
    			return true;
    	return false;
    }
    
    /**
     * @return List of alternative sections for the unit/unit group selector for the given list of filter unit groups.
     * If the given list is empty, it'll return a list of all individual units.
     * Otherwise, it'll return one alternative for each unit group sent, each of which leads
     * to a new screen of alternatives with that unit group's children unit groups.
     */
	public static List<AltSection> getFilter(CompanySettings config, List<FilterUnitGroup> unitGroups) {
        List<AltSection> sections = new ArrayList<AltSection>();
        
        sections.add(Build.altSection().alternatives(Arrays.asList(
                Build.alt().key("all").value("All").table(Build.altTable().key("").sections(new ArrayList<AltSection>()).get()).get())).get());

        if (unitGroups.isEmpty()) {
        	// if there are no unit group filters, fall back to showing all individual units
        	sections.addAll(getUnitSections(config));
        } else {
        	// add an alternative for each specified unit group, each of which leads to a new screen of alternatives
    		List<UnitGroup> ugTable = config.getUnitGroupsWithFilteredName(false);
            List<AltSection> ugSections = new ArrayList<AltSection>();
        	int idx = 0;
	        for (FilterUnitGroup ug : unitGroups) {
		        List<AltSection> subsections = ug.isUnitParent() ? getUnitSections(config) : getUnitGroupSections(ugTable, ug);
		        if (hasMultipleAlternatives(subsections))
			        ugSections.add(Build.altSection().alternatives(Arrays.asList(
			                Build.alt().key("alt" + idx++).value(ug.name).table(Build.altTable().key("").sections(subsections).get()).get())).get());
	        }
	        
	        // if only one section (and hence one alternative), add its subsections directly to the first screen
	        if (ugSections.size() == 1) {
	        	sections.addAll(ugSections.get(0).alternatives().get(0).table().sections());
	        } else {
	        	sections.addAll(ugSections);
	        }
        }
        
        return sections;
	}
}
