package com.medallia.mobile.dto;
import java.util.*;
import com.medallia.mobile.*;
import com.medallia.mobile.dto.*;
import com.medallia.mobile.dto.builders.*;
import com.medallia.mobile.color.UIColor;

// These files are generated automatically from declarations in DataFormat.h; see convert.pl for details.
//
// Do *not* modify this file directly!


/**
 *  Alternatives may be shown in groups
 */
@SuppressWarnings("unused")
public interface AltSection extends Plist.Plistable {

	/**
	 *  Group header
	 */
	String header();

	/**
	 *  List of alternatives in the group
	 */
	@Required List<Alt> alternatives();


}
