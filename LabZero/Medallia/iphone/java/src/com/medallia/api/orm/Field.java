package com.medallia.api.orm;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import com.medallia.mobile.Common;

public abstract class Field {

	public final String key;
	public final String name;
	
	protected Field(String key, String name) {
		if (key == null) {
			throw new IllegalArgumentException("key cannot be null");
		}
		this.key = key;
		this.name = name;
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof Field) {
			Field f = (Field)o;
			return key.equals(f.key);
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return key.hashCode();
	}
	
	@Override
	public String toString() {
		return name;
	}
	
	public static class AggregatableField extends Field {
		public AggregatableField(String key, String name) {
			super(key, name);
		}

		public DoubleField avg() {
			return new DoubleField("avg(" + key + ")", name);
		}
		
		public IntField count() {
			return new IntField("count(" + key + ")", name);
		}
		
		public LongField sum() {
			return new LongField("sum(" + key + ")", name);
		}
		
	}

	public static class IntField extends AggregatableField {
		public IntField(String key, String name) {
			super(key, name);
		}
	}

	public static class DoubleField extends AggregatableField {
		public DoubleField(String key, String name) {
			super(key, name);
		}
	}

	public static class StringField extends Field {
		public StringField(String key, String name) {
			super(key, name);
		}
	}

	public static class LongField extends Field {
		public LongField(String key, String name) {
			super(key, name);
		}
	}

	public static class DateField extends Field {

		/** yyy-MM-dd */
		public static final DateFormat ISO_DF = Common.synch(new SimpleDateFormat("yyyy-MM-dd"));

		public DateField(String key, String name) {
			super(key, name);
		}
	}

}
