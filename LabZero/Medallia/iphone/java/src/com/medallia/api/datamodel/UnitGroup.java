package com.medallia.api.datamodel;

import java.util.List;

import com.medallia.api.orm.Row;
import com.medallia.api.orm.SelectQuery;
import com.medallia.api.orm.Field.StringField;

public class UnitGroup {
	
	private static final StringField UNITGROUPNAME = new StringField("name", "UnitGroup name");
	private static final StringField PARENT_UNITGROUPID = new StringField("parent_unitgroupid", "Parent UnitGroup identifier");
	static final StringField UNITGROUPID = new StringField("unitgroupid", "UnitGroup identifier");
	
	public final String unitgroupid;
	public final String name;
	public final String parent_unitgroupid;

	public UnitGroup(String unitgroupid, String name, String parent_unitgroupid) {
		this.unitgroupid = unitgroupid;
		this.name = name;
		this.parent_unitgroupid = parent_unitgroupid;
	}

	public static SelectQuery<UnitGroup> getUnitGroupQuery() {
		SelectQuery<UnitGroup> query = new SelectQuery<UnitGroup>("unitgroup") {
			@Override
			protected UnitGroup createFromRow(List<String> rowStr) {
				Row row = new Row(fields, rowStr);
				return new UnitGroup(row.getString(UNITGROUPID), row.getString(UNITGROUPNAME), row.getString(PARENT_UNITGROUPID));
			}
		};
		query.addField(UNITGROUPID);
		query.addField(UNITGROUPNAME);
		query.addField(PARENT_UNITGROUPID);
		return query;
	}
	
}
