package com.medallia.api.datamodel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.medallia.api.orm.ApiConnection;
import com.medallia.api.orm.QueryFieldList;
import com.medallia.api.orm.Row;
import com.medallia.api.orm.SelectQuery;
import com.medallia.api.orm.Field.StringField;

/**
 * 
 */
public class Unit extends Row implements Comparable<Unit> {

	public static final StringField UNITNAME = new StringField("name", "Unit name");
	public static final StringField UNITID = new StringField("unitid", "Unit identifier");
	public static final StringField ACTIVE = new StringField("active", "Active");

	public Unit(QueryFieldList fields, List<String> data) {
		super(fields, data);
	}
	
	private static QueryFieldList defaultFieldList() {
		QueryFieldList fields = new QueryFieldList();
		fields.addField(UNITID);
		fields.addField(UNITNAME);
		fields.addField(ACTIVE);
		return fields;
	}

	public Unit(String id, String name) {
		super(defaultFieldList(), Arrays.asList(id, name));
	}

	public String getId() {
		return getString(UNITID);
	}
	
	public String getName() {
		return getString(UNITNAME);
	}

	/** @return Whether the unit is active. */
	public boolean isActive() {
		return "yes".equals(getString(ACTIVE));
	}

	@Override public int compareTo(Unit b) {
		return getName().compareToIgnoreCase(b.getName());
	}
	
	@Override public String toString() {
		return getName() + " (" + getId() + ")";
	}
	
	private static SelectQuery<Unit> getUnitQuery() {
        SelectQuery<Unit> query = new SelectQuery<Unit>("unit") {
			@Override
			protected Unit createFromRow(List<String> row) {
				return new Unit(fields, row);
			}
        };
        query.addField(UNITID);
        query.addField(UNITNAME);
        query.addField(ACTIVE);
		return query;
	}

	/** @return List of all active units (with or without extended scope), sorted by name. */
    public static List<Unit> getUnits(ApiConnection connection, boolean extendedScope) {
    	return getUnits(connection, extendedScope, false);
    }
    
	/** @return List of all units (with or without extended scope), including inactive units if so specified, sorted by name. */
    public static List<Unit> getUnits(ApiConnection connection, boolean extendedScope, boolean includeInactive) {
        SelectQuery<Unit> query = getUnitQuery();
        query.setExtended(extendedScope);
        List<Unit> units = query.run(connection);
        if (!includeInactive) {
        	List<Unit> activeUnits = new ArrayList<Unit>();
        	for (Unit unit : units)
        		if (unit.isActive())
        			activeUnits.add(unit);
        	units = activeUnits;
        }
    	Collections.sort(units);
    	return units;
    }

}
