package com.medallia.api.datamodel;

import java.util.List;

import com.medallia.api.orm.QueryFieldList;
import com.medallia.api.orm.Row;
import com.medallia.api.orm.UpdateStatement;
import com.medallia.api.orm.Field.DateField;
import com.medallia.api.orm.Field.IntField;
import com.medallia.api.orm.Field.StringField;

public class Survey extends Row {

	public Survey(QueryFieldList fields, List<String> data) {
		super(fields, data);
	}

	// Common fields in survey table
	public static final IntField SURVEYID = new IntField("surveyid", "Surveyid");
	public static final DateField RESPONSEDATE = new DateField("responsedate", "Response Date");
	public static final StringField COMMENTS = new StringField("comments", "All surey comments concatenated");

	public static final StringField fullname = new StringField("fullname", "Name");
	public static final StringField firstname = new StringField("firstname", "First name");
	public static final StringField lastname = new StringField("lastname", "Last name");
	public static final StringField phone = new StringField("phone", "Phone");
	
	public static final IntField HAS_OPEN_ALERT = new IntField("has_open_alert", "Survey has open alert (1 or 0)");
	public static final StringField ALERT_TYPE = new StringField("alert_type", "Alert type of survey (if any)");
	public static final StringField ALERT_STATUS = new StringField("alert_status", "Alert status of survey (if any)");
	
	
	public static UpdateStatement updateAlertStatement(String surveyId, String alertAction) {
		UpdateStatement statement = new UpdateStatement("survey");
		statement.setValue(Survey.ALERT_STATUS, alertAction);
		statement.addCondition(Survey.SURVEYID + " = " + surveyId);
		return statement;
	}

}
