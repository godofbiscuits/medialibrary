package com.medallia.api.datamodel;

import java.util.List;

public class SurveyAggregateQuery extends SelectQueryWithBooleanFilter<SurveyAggregate> {

	/** Create a new query for the given query string and given extended unit scope setting (default is false). */
	public SurveyAggregateQuery(boolean extendedScope) {
		super("survey");
		setExtended(extendedScope);
	}
	
	/** Create a new query for the given query string and no extended unit scope (default setting). */
	public SurveyAggregateQuery() {
		this(false);
	}
	
	@Override
	protected SurveyAggregate createFromRow(List<String> row) {
		return new SurveyAggregate(fields, row);
	}
	
}
