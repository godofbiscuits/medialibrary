package com.medallia.api.datamodel;

import java.util.Date;
import java.util.List;

import com.medallia.api.orm.InsertStatement;
import com.medallia.api.orm.Row;
import com.medallia.api.orm.SelectQuery;
import com.medallia.api.orm.Field.DateField;
import com.medallia.api.orm.Field.StringField;

public class Note {
	
	public final String user, title, content;
	public final Date date;
	
	public Note(String user, Date date, String title, String content) {
		this.user = user;
		this.date = date;
		this.title = title;
		this.content = content;
	}
	
	private static final StringField USER = new StringField("user", "User");
	private static final DateField DATE = new DateField("date", "Date");
	private static final StringField TITLE = new StringField("title", "Title");
	private static final StringField CONTENT = new StringField("content", "Content");
	
	public static SelectQuery<Note> getNoteQuery(String surveyId) {
		SelectQuery<Note> query = new SelectQuery<Note>("note") {
			@Override
			protected Note createFromRow(List<String> rowStr) {
				Row row = new Row(fields, rowStr);
				return new Note(row.getString(USER), row.getDate(DATE), row.getString(TITLE), row.getString(CONTENT));
			}
		};
		query.addField(USER);
		query.addField(DATE);
		query.addField(TITLE);
		query.addField(CONTENT);
		query.addCondition("surveyid = " + surveyId);
		return query;
	}

	public static InsertStatement getNewNoteStatement(String surveyId, String content) {
		InsertStatement statement = new InsertStatement("note");
		statement.setValue(Survey.SURVEYID, surveyId);
		statement.setValue(CONTENT, content);
		return statement;
	}

}
