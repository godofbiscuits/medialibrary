package com.medallia.api.datamodel;

import java.util.List;

public class SurveyQuery extends SelectQueryWithBooleanFilter<Survey> {

	/** Create a new query for the given query string and given extended unit scope setting (default is false). */
	public SurveyQuery(boolean extendedScope) {
		super("survey");
		setExtended(extendedScope);
	}
	
	/** Create a new query for the given query string and no extended unit scope (default setting). */
	public SurveyQuery() {
		this(false);
	}
	
	@Override
	protected Survey createFromRow(List<String> row) {
		return new Survey(fields, row);
	}
	
}
