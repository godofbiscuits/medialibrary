package com.medallia.api.orm;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Build a list of fields to use in a select query to the API.
 * This class keeps track of the position of each field in the query
 * and thus extracts the value from a row returned by the API.
 */
public class QueryFieldList {
	
	private final StringBuffer fieldList = new StringBuffer();
	private final Map<Field, Integer> fieldToPositionMap = new HashMap<Field, Integer>();

	/** Add a new Field to be included in the query */
	public void addField(Field field) {
		if (field == null) throw new IllegalArgumentException("field is null");
		if (fieldToPositionMap.containsKey(field)) {
			// the Field has already been added to this query
			return;
		}
		int pos = fieldToPositionMap.size();
		fieldToPositionMap.put(field, pos);
		if (pos > 0) {
			fieldList.append(", ");
		}
		fieldList.append(field.key);
	}
	
	/** Add a new Field to be included in the query,
	 *  or just ignore if field is null */
	public void addFieldOrNull(Field field) {
		if (field == null) return;
		addField(field);
	}
	
	/** Add all of the fields in the given collection to the query */
	public void addFields(Collection<? extends Field> fields) {
		for (Field field : fields) {
			addField(field);
		}
	}

	/**
	 * Fetch the value of the given field from a row in the query result set.
	 * @param row one row from the result set returned from the API for the query
	 * @param field the field for which we want the value
	 * @return the value of the given field for the given row
	 */
	public String getValue(List<String> row, Field field) {
		if (field == null) throw new IllegalArgumentException("field is null");
		int pos = getPosition(field);
		if (row.size() <= pos) throw new RuntimeException("not enough entries in " + row);
		return row.get(pos);
	}
	
	private int getPosition(Field field) {
		if (field == null) throw new IllegalArgumentException("field is null");
		Integer pos = fieldToPositionMap.get(field);
		if (pos == null) throw new RuntimeException(field + " is not a key in " + fieldToPositionMap);
		return pos;
	}
	
	/**
	 * @return the field list as a string ready to use in a query, like:
	 * SELECT fieldlist FROM table WHERE condition
	 */
	public String getFieldList() {
		return fieldList.toString();
	}
	
}
