package com.medallia.api.orm;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public abstract class SelectQuery<T> extends StatementTakingWhere {

	private String orderBy;
	private Integer limit;
	private Integer offset;

	public SelectQuery(String table) {
		super(table);
	}

	/** Add a new Field to be included in the query */
	public void addField(Field field) {
		fields.addField(field);
	}

	/** Add a new Field to be included in the query,
	 *  or just ignore if field is null */
	public void addFieldOrNull(Field field) {
		fields.addFieldOrNull(field);
	}
	
	/** Add all of the fields in the given collection to the query */
	public void addFields(Collection<? extends Field> fields) {
		this.fields.addFields(fields);
	}
	
	@Override public String getQueryString() {
		StringBuilder sb = new StringBuilder("SELECT ");
		sb.append(fields.getFieldList());
		sb.append(" FROM " + table);
		addSqlWhereClause(sb);
		if (orderBy != null) {
			sb.append(" ORDER BY ");
			sb.append(orderBy);
		}
		if (limit != null) {
			sb.append(" LIMIT ");
			sb.append(limit);
		}
		if (offset != null) {
			sb.append(" OFFSET ");
			sb.append(offset);
		}
		return sb.toString();
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}
	
	public void setLimit(int limit) {
		this.limit = limit;
	}
	
	public void setOffset(int offset) {
		this.offset = offset;
	}
	
	public final List<T> run(ApiConnection connection) {
		List<T> results = new ArrayList<T>();
		List<List<String>> table = connection.runQuery(this);
		for (List<String> row : table) {
			results.add(createFromRow(row));
		}
		return results;
	}
	
	public final T runSingleRowQuery(ApiConnection connection) {
		List<T> rs = run(connection);
		if (rs.size() == 0) return null;
		if (rs.size() == 1) {
			return rs.iterator().next();
		}
		throw new RuntimeException("Several rows were returned when only one was expected; " + rs);
	}
	
	protected abstract T createFromRow(List<String> row);
	
}
