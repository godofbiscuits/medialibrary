package com.medallia.api.orm;

public class UpdateStatement extends StatementTakingWhere {

	private StringBuilder fieldValues = new StringBuilder();
	
	public UpdateStatement(String table) {
		super(table);
	}
	
	public void setValue(Field field, Object value) {
		if (fieldValues.length() > 0) fieldValues.append(", ");
		fieldValues.append(field.key);
		fieldValues.append(" = ");
		addArgument(fieldValues, value);
	}

	@Override
	public String getQueryString() {
		StringBuilder sb = new StringBuilder("UPDATE " + table + " SET ");
		sb.append(fieldValues);
		addSqlWhereClause(sb);
		return sb.toString();
	}

}
