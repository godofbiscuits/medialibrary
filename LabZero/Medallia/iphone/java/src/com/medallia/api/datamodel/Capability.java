package com.medallia.api.datamodel;

import java.util.List;

import com.medallia.api.orm.SelectQuery;
import com.medallia.api.orm.Field.StringField;

public class Capability {

	private static final StringField CAPABILITYID = new StringField("capabilityid", "Capability id");
	
	public final String id;
	
	public Capability(String id) {
		this.id = id;
	}
	
	public static SelectQuery<Capability> getCapabilityQuery() {
		SelectQuery<Capability> query = new SelectQuery<Capability>("capability") {
			@Override
			protected Capability createFromRow(List<String> row) {
				return new Capability(row.get(0));
			}
		};
		query.addField(CAPABILITYID);
		return query;
	}
	
}
