package com.medallia.api.orm;

import java.util.List;

import tiny.Empty;

public class InsertStatement extends Statement {

	private List<Field> fieldsToInsert = Empty.list();
	private List<Object> valuesToInsert = Empty.list();
	
	public InsertStatement(String table) {
		super(table);
	}

	/** add a pair of values to insert.
	 * 
	 * @param field name of the field the value should be inserted into
	 * @param value the value to insert
	 */
	public void setValue(Field field, Object value) {
		fieldsToInsert.add(field);
		valuesToInsert.add(value);
	}
	
	@Override
	public String getQueryString() {
		StringBuilder sb = new StringBuilder("INSERT INTO " + table + "(");
		boolean first = true;
		for (Field f : fieldsToInsert) {
			if (first) first = false;
			else sb.append(", ");
			sb.append(f.key);
		}
		sb.append(") VALUES (");
		first = true;
		for (Object o : valuesToInsert) {
			if (first) first = false;
			else sb.append(", ");
			addArgument(sb, o);
		}
		sb.append(")");
		return sb.toString();
	}

}
