package com.medallia.api.datamodel;

import java.util.Date;
import java.util.List;

import com.medallia.api.orm.Row;
import com.medallia.api.orm.SelectQuery;
import com.medallia.api.orm.Field.DateField;
import com.medallia.api.orm.Field.IntField;
import com.medallia.api.orm.Field.StringField;

public class Timeperiod {

	private static final StringField ID = new StringField("id", "Id");
	private static final StringField NAME = new StringField("name", "Name");
	private static final DateField STARTDATE = new DateField("startdate", "Start date");
	private static final DateField ENDDATE = new DateField("enddate", "End date");
	private static final IntField PRIORITY = new IntField("priority", "Priority");
	
	public final String id;
	public final String name;
	public final Date startDate;
	public final Date endDate;
	public final int priority;
	
	public Timeperiod(String id, String name, Date startDate, Date endDate, int priority) {
		this.id = id;
		this.name = name;
		this.startDate = startDate;
		this.endDate = endDate;
		this.priority = priority;
	}
	
	public static SelectQuery<Timeperiod> getTimeperiodQuery() {
		SelectQuery<Timeperiod> query = new SelectQuery<Timeperiod>("timeperiod") {
			@Override
			protected Timeperiod createFromRow(List<String> rowStr) {
				Row row = new Row(fields, rowStr);
				return new Timeperiod(row.getString(ID), row.getString(NAME), row.getDate(STARTDATE), row.getDate(ENDDATE), row.getInt(PRIORITY));
			}
		};
		query.addField(ID);
		query.addField(NAME);
		query.addField(STARTDATE);
		query.addField(ENDDATE);
		query.addField(PRIORITY);
		return query;
	}
	
}
