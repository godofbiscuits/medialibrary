package com.medallia.api.orm;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ResultSet implements Iterable<Row> {

	private final QueryFieldList fields;
	private final List<Row> rows = new ArrayList<Row>();
	
	public ResultSet(QueryFieldList fields, List<List<String>> data) {
		this.fields = fields;
		for (List<String> row : data) {
			rows.add(new Row(fields, row));
		}
	}
	
	@Override
	public Iterator<Row> iterator() {
		return rows.iterator();
	}
	
	public int getNumberOfRows() {
		return rows.size();
	}

}
