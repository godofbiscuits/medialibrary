package com.medallia.api.orm;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import com.medallia.api.orm.Field.DateField;
import com.medallia.api.orm.Field.DoubleField;
import com.medallia.api.orm.Field.IntField;
import com.medallia.api.orm.Field.LongField;
import com.medallia.api.orm.Field.StringField;


public class Row {

	private final QueryFieldList fields;
	private final List<String> data;
	
	public Row(QueryFieldList fields, List<String> data) {
		this.fields = fields;
		this.data = data;
	}
	
	public String getValueRaw(Field field) {
		return fields.getValue(data, field);
	}
	
	public String getString(StringField field) {
		return getValueRaw(field);
	}

	public Date getDate(DateField field) {
		try {
			String value = getValueRaw(field);
			if (value.isEmpty()) return null;
			return DateField.ISO_DF.parse(value);
		} catch (ParseException e) {
			throw new RuntimeException(e);
		}
	}
	
	public Double getDouble(DoubleField field) {
		String value = getValueRaw(field);
		if (value.isEmpty()) return null;
		return Double.parseDouble(value);
	}
	
	public Integer getInt(IntField field) {
		String value = getValueRaw(field);
		if (value.isEmpty()) return null;
		return (int)Double.parseDouble(value);
	}
	
	public Long getLong(LongField field) {
		String value = getValueRaw(field);
		if (value.isEmpty()) return null;
		return (long)Double.parseDouble(value);
	}
	
}
