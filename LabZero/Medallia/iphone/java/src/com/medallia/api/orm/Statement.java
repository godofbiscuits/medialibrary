package com.medallia.api.orm;

import java.util.List;

import org.apache.commons.lang.StringEscapeUtils;

import tiny.Strings;


/**
 * Contains an MQL query string to be sent to the Medallia API servlet.
 * Also contains any additional query data, such as if to query with an extended unit scope.
 */
public abstract class Statement {
	
	protected final String table;
	protected final QueryFieldList fields = new QueryFieldList();
	protected boolean extendedScope;
	
	protected Statement(String table) {
		this.table = table;
	}
	
	public void setExtended(boolean extendedScope) {
		this.extendedScope = extendedScope;
	}

	/**
	 * @return If true, the query will be performed with the user's extended unit scope. A user may have full access to a single
	 * unit, and extended access to perform aggregate queries on a wider set of units. This is useful for the ranker module.
	 */
	public boolean isExtendedScope() {
		return extendedScope;
	}
	
	/** @return The query string. E.g. "SELECT avg(ov_experience) FROM survey". */
	public abstract String getQueryString();

	
	/**
	 * Use for INSERT and UPDATE statements that return "OK" if it goes through
	 * @param connection the connection with which to run the statement
	 * @return null if the update went through without error, or the error message otherwise
	 */
	public String runAcceptQuery(ApiConnection connection) {
		List<List<String>> result = connection.runQuery(this);
		if (result.size() != 1) throw new RuntimeException("Expected 1 row, got " + result.size() + " rows");
		List<String> row = result.get(0);
		if (row.size() == 1 && "OK".equals(row.get(0))) {
			// OK
			return null;
		} else {
			return Strings.join(" | ", row);
		}
	}
	
	protected static void addArgument(StringBuilder sb, Object o) {
		if (o instanceof Number) {
			sb.append(o);
		} else {
			sb.append(quote(o.toString()));
		}
	}
	
	/** Escape and quote the string, and return it */
	public static String quote(String s) {
		return '\'' + StringEscapeUtils.escapeJavaScript(s) + '\'';
	}

	@Override public String toString() {
		if (extendedScope) return getQueryString() + " (extended)";
		else return getQueryString();
	}

}
