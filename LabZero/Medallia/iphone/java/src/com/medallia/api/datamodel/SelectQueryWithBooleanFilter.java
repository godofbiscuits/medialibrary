package com.medallia.api.datamodel;

import java.util.Date;

import com.medallia.api.orm.SelectQuery;
import com.medallia.api.orm.Statement;
import com.medallia.api.orm.Field.DateField;

public abstract class SelectQueryWithBooleanFilter<T> extends SelectQuery<T> {

	public SelectQueryWithBooleanFilter(String table) {
		super(table);
	}

	/** add condition "responsedate >= start AND responsedate <= end" to WHERE clause */
	public void whereTimePeriod(Date start, Date end) {
		addCondition("responsedate >= '" + DateField.ISO_DF.format(start) + "'");
		addCondition("responsedate <= '" + DateField.ISO_DF.format(end) + "'");
	}
	
	/** add condition "timeperiod(XX, responsedate)" to WHERE clause */
	public void whereTimePeriod(String timePeriodId) {
		addCondition("timeperiod(" + timePeriodId + ", responsedate)");
	}

	public void addTextSearch(String searchTerm) {
		addCondition(String.format("TEXT %s", Statement.quote(searchTerm)));
	}

}
