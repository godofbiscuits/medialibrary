package com.medallia.api.datamodel;

import java.util.List;

import com.medallia.api.orm.QueryFieldList;
import com.medallia.api.orm.Row;
import com.medallia.api.orm.Field.IntField;

public class SurveyAggregate extends Row {

	public static final IntField COUNTSTAR = new IntField("count(*)", "Number of surveys");
	
	public SurveyAggregate(QueryFieldList fields, List<String> data) {
		super(fields, data);
	}

}
