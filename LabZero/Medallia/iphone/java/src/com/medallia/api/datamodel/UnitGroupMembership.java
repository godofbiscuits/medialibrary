package com.medallia.api.datamodel;

import java.util.List;

import com.medallia.api.orm.Row;
import com.medallia.api.orm.SelectQuery;

public class UnitGroupMembership {
	
	public final String unitid;
	public final String unitgroupid;
	
	public UnitGroupMembership(String unitid, String unitgroupid) {
		this.unitid = unitid;
		this.unitgroupid = unitgroupid;
	}
	
	public static SelectQuery<UnitGroupMembership> getUnitGroupMembershipQuery() {
        SelectQuery<UnitGroupMembership> query = new SelectQuery<UnitGroupMembership>("unitgroup_membership") {
			@Override
			protected UnitGroupMembership createFromRow(List<String> rowStr) {
				Row row = new Row(fields, rowStr);
				return new UnitGroupMembership(row.getString(Unit.UNITID), row.getString(UnitGroup.UNITGROUPID));
			}
        };
        query.addField(Unit.UNITID);
        query.addField(UnitGroup.UNITGROUPID);
		return query;
	}

}
