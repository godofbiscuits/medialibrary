package com.medallia.api.orm;

import java.util.List;

public interface ApiConnection {

	List<List<String>> runQuery(Statement statement);
	
}
