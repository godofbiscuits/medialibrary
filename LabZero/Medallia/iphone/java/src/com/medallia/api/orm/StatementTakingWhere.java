package com.medallia.api.orm;

public abstract class StatementTakingWhere extends Statement {

	protected final StringBuffer filter = new StringBuffer();

	protected StatementTakingWhere(String table) {
		super(table);
	}

	public void addCondition(String condition) {
		if (filter.length() > 0) {
			filter.append(" AND ");
		}
		filter.append(condition);
	}
	
	protected void addSqlWhereClause(StringBuilder sb) {
		if (filter.length() > 0) {
			sb.append(" WHERE ");
			sb.append(filter);
		}
	}
	
}
