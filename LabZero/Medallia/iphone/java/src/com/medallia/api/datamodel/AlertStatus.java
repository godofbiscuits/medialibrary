package com.medallia.api.datamodel;


/**
 * The different statuses an alert can have.
 * A company will typically only use a subset of these statuses in their work flow,
 * thus it is possible to configure which ones are available on a per company basis.
 * Furthermore, exactly how each status is being used may also differ, as different
 * rules for transitioning between statuses and what actions to take are configurable.
 * The comments for each status indicate what it is typically being used for.
 */
public enum AlertStatus {
	NEW,         // a new alert that has not been handled yet
	IN_PROGRESS, // the alert is being handled
	OVERDUE,     // the alert is overdue to be closed
	CLOSED,      // the alert is closed, no further action necessary
	RESOLVED,    // the alert is resolved, but not yet closed
	ESCALATED,   // the alert is escalated to a higher level in the company hierarchy
	ESCALATED2;  // the alert is escalated to an even higher level

	public static AlertStatus parse(String value) {
		if (value == null || value.isEmpty()) return null;
		value = value.toUpperCase();
		value = value.replace(' ', '_');
		return AlertStatus.valueOf(value);
	}
	
}
