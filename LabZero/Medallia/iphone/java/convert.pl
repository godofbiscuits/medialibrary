use strict;

# interface package and builder package
my $pkg = "com.medallia.mobile";
my $idir = my $ipkg = "$pkg.dto";
my $bdir = my $bpkg = "$pkg.dto.builders";
y-.-/- for ($idir,$bdir);

my $unused = '@SuppressWarnings("unused")';

my %map = qw(NSString String  NSNumber double  NSMutableDictionary Map<String,String>);

open F,"../Classes/DataFormat.h";
open IMPL,">src/$bdir/Build.java" or die "Can't write in $bdir";

my $fileHeader = "
import java.util.*;
import $pkg.*;
import $ipkg.*;
import $bpkg.*;
import $pkg.color.UIColor;

// These files are generated automatically from declarations in DataFormat.h; see convert.pl for details.
//
// Do *not* modify this file directly!


";

print IMPL "package $bpkg;", $fileHeader, "

$unused
public class Build {
";

my %output;  # class name -> file contents
my $const;
my $claz;
my @z;
my @classes;
my $b;
my %classdoc;
while(<F>) {
    /\#define ([A-Z_]+) (\d+|(\").*\")(\s*\/\/(.*))?$/ and do {
	my $type = $3 ? "String" : "int";
	$const .= "/** $5 */\n\t" if $4;
	$const .= "public static final $type $1 = $2;\n\t";
    };
    /\@interface (\w+)/ and do {
	push @classes, $claz = $1;
	my $comment = m!//(.+)! && $1;
	$classdoc{$claz} = $comment;
	$output{$claz} = "package $ipkg;" . $fileHeader . "/**\n * $comment\n */\n$unused\npublic interface $claz extends Plist.Plistable {\n\n";
	print IMPL "\n\n/**\n * $comment\n */\npublic static class ${claz}Builder extends Builder<$claz> {\n\n";
	@z = ();
	$b = '';
    };
    /\@property/ and do {
	my @m = /\@property\s*(\(retain\))?(\(copy\))? (Plist(Opt|Req|Manual) )?(?:OnSet\((\w+)\) )?(FromInit )?((?:IBOutlet )?\w+|\w+<\w+>) (\*)?(\w+);/;
	die "Can't parse $_\n" if !@m;
	my ($retain, $copy, $plist, $req, $onSet, $init, $type, $pointer, $name) = @m;
	next if !$plist;

	$type = $map{$type} || $type;
	$type =~ s/(.+)_List/List<$1>/g;

	my $r = $req eq 'Req' ? "\@Required " : "";

	print IMPL "\t${r}$type $name;\n"; # declare variable

	if (m!//(.+)!) {  # javadoc
	    $output{$claz} .= "\t/**\n\t * $1\n\t */\n";
	    print IMPL "\n\t/**\n\t * $1\n\t */\n";
	}
	$output{$claz} .= "\t$r$type $name();\n\n";

	print IMPL "\tpublic ${claz}Builder $name($type x) {\n\t\t$name = x;\n\t\treturn this;\n\t}\n\n";
	$b .= "\t\t\tpublic $type $name() {\n\t\t\t\treturn $name;\n\t\t\t}\n";
    };
    /\@end/ and do {
	if ($claz) {
	    $output{$claz} .= "\n}\n";
	    print IMPL "\tpublic $claz get() {\n";
	    print IMPL "\t\tcheck();\n";
	    print IMPL "\t\treturn new $claz() {\n";
	    print IMPL $b;
	    print IMPL "\t\t};\n";
	    print IMPL "\t}\n";
	    print IMPL "}\n";
	    undef $claz;
	}
    };
    /\#define (ConfigPartType)Def\(def,and\).+/ and do {
	my $n = $1;
	my @x = /def\((\w+)\)/g;
	$output{$n} = "package $ipkg;" . $fileHeader . "$unused\npublic enum $n {\n\n" . join(",\n\t",@x) . "\n}\n";
    };
}

print IMPL "

public static class Constants {
	$const
}
", map("
	/**
	 * $classdoc{$_}
	 */
	public static ${_}Builder \l${_}() {
		return new ${_}Builder();
	}
", @classes), "
}
";


for my $f (keys %output) {
  my $fn = "src/$idir/$f.java";
  open F, ">$fn" or die "Can't write $fn";
  print F $output{$f};
  close F;
}
