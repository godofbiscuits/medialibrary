#import <UIKit/UIKit.h>
#import "Gen.h"
#import "DataFormat.h"
#import "WidgetConfig.h"
#import "Widget.h"
#import "SimpleRequest.h"
#import "GaugeWithBaseline.h"
#import "GraphDataset.h"

#import "CorePlot-CocoaTouch.h"

#import "ScoreWidgetNum.h"
#import "ClickableHeaderView.h"


extern NSString* MEDUserSelectedDifferentDataSetNotification;

#define TIME_INTERVAL_CHANGE_MESSAGE @"TimeIntervalChanged"


@class PlotSourceDataset;
@class GraphDataset;




 enum {
    GraphDisplayIntervalValueBaseValue = -1,
    GraphDisplayIntervalValue1W,
    GraphDisplayIntervalValue1M,
    GraphDisplayIntervalValue3M,
    GraphDisplayIntervalValue6M,
    GraphDisplayIntervalValue1Y,
    GraphDisplayIntervalValue2Y
} GraphDisplayIntervalValue;






// Generic Scoreboard-like widget. Shows a set of SWPage-s, one at a time, each consisting of a number of SWLine-s.

@interface /*NOGENERATE*/ ScoreWidget : Widget <SRDelegate, UITableViewDelegate, UITableViewDataSource, ClickableHeaderDelegate, UIScrollViewDelegate> 
{
	UILabel *titleL;   
	//UILabel *subtitleL;
    UILabel *noDataLabel;
    UILabel *landscapeNoDataLabel;
	UIView *contentContainer;   
	UIView *flipView;   
	UIView *graphView;
	UIView *portraitNavView;
	UIView *landscapeGraphView;
    UIView *legendView;
	UIView *graphPortraitContainerView;   
    
	NSArray *timeIntervalButtonArray;
	NSArray *landscapeTimeIntervalButtonArray;
    
    UITableView* swTableView;
    UILabel* swScoreLabel;
    UILabel* swBenchmarkLabel;
    NSUInteger selectedTableRow;
    CGRect swScoreFrame;
    CGRect swBenchmarkFrame;
    ClickableHeaderView* swHeaderView;
    
    CPLayerHostingView* layerHostView;
    CPLayerHostingView* landscapeLayerHostView;
    CPXYGraph* graph;
    CPXYGraph* landscapeGraph;
    NSInteger graphDisplayTimeInterval;
    UISegmentedControl* graphDisplayIntervalControl;
    UISegmentedControl* landscapeGraphDisplayIntervalControl;
    
    IBOutlet ScoreWidgetNum* holdCell;
    
    IBOutlet UIImageView* shadowSectionHeader;
    
    NSMutableArray* parsedPageData;
    GraphDataset* currentGraphDataset;
    
    SWPage* page;

    CGRect portraitGraphViewFrame;
    NSUInteger currentOrientationIndex;

    UIButton* button1w;
    UIButton* button1m;
    UIButton* button3m;
    UIButton* button6m;
    UIButton* button1y;
    UIButton* button2y;

    UIButton* landscapeButton1w;
    UIButton* landscapeButton1m;
    UIButton* landscapeButton3m;
    UIButton* landscapeButton6m;
    UIButton* landscapeButton1y;
    UIButton* landscapeButton2y;
}


@property (nonatomic, retain) IBOutlet UILabel* titleL; // "Scoreboard"
//@property (nonatomic, retain) IBOutlet UILabel* subtitleL; // "Palo Alto (....)"
@property (nonatomic, retain) IBOutlet UIView* contentContainer;
@property (nonatomic, retain) IBOutlet UIView* flipView;
@property (nonatomic, retain) IBOutlet UIView* graphView;
@property (nonatomic, retain) UIView* portraitNavView;
@property (nonatomic, retain) IBOutlet UIView* landscapeGraphView;
@property (nonatomic, retain) IBOutlet UIView* legendView;
@property (nonatomic, retain) IBOutlet UIView* graphPortraitContainerView;

// time interval buttons
@property (nonatomic, retain) IBOutlet UIButton* button1w;
@property (nonatomic, retain) IBOutlet UIButton* button1m;
@property (nonatomic, retain) IBOutlet UIButton* button3m;
@property (nonatomic, retain) IBOutlet UIButton* button6m;
@property (nonatomic, retain) IBOutlet UIButton* button1y;
@property (nonatomic, retain) IBOutlet UIButton* button2y;

@property (nonatomic, retain) IBOutlet UIButton* landscapeButton1w;
@property (nonatomic, retain) IBOutlet UIButton* landscapeButton1m;
@property (nonatomic, retain) IBOutlet UIButton* landscapeButton3m;
@property (nonatomic, retain) IBOutlet UIButton* landscapeButton6m;
@property (nonatomic, retain) IBOutlet UIButton* landscapeButton1y;
@property (nonatomic, retain) IBOutlet UIButton* landscapeButton2y;

// core plot
@property (nonatomic, retain) IBOutlet UILabel* noDataLabel;
@property (nonatomic, retain) IBOutlet UILabel* landscapeNoDataLabel;
@property (nonatomic, retain) IBOutlet UITableView* swTableView;
@property (nonatomic, assign) IBOutlet UILabel* swScoreLabel;
@property (nonatomic, assign) IBOutlet UILabel* swBenchmarkLabel;
@property (nonatomic) CGRect swScoreFrame;
@property (nonatomic) CGRect swBenchmarkFrame;
@property (nonatomic, retain, readonly, getter=clickableHeaderView) IBOutlet ClickableHeaderView* swHeaderView;

@property (nonatomic, readonly) NSUInteger selectedTableRow;
@property (nonatomic, retain) IBOutlet CPLayerHostingView* layerHostView;
@property (nonatomic, retain) IBOutlet CPLayerHostingView* landscapeLayerHostView;
@property (nonatomic, retain) CPXYGraph* graph;
@property (nonatomic, retain) CPXYGraph* landscapeGraph;
@property (nonatomic, assign) IBOutlet ScoreWidgetNum* holdCell;
@property (nonatomic) NSInteger graphDisplayTimeInterval;
@property (nonatomic, retain) IBOutlet UISegmentedControl* graphDisplayIntervalControl;
@property (nonatomic, retain) IBOutlet UISegmentedControl* landscapeGraphDisplayIntervalControl;

@property (nonatomic, retain) NSMutableArray* parsedPageData;
@property (nonatomic, retain) GraphDataset* currentGraphDataset;

@property (nonatomic, assign, readonly, getter=page) SWPage* page;




- (IBAction) timePeriodButtonPressed:(id)sender;



@end



