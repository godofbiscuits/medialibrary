
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "MedalliaAppDelegate.h"
#import "ScoreWidget.h"
#import "WidgetConfig.h"
#import "UIViewControllerShared.h"
#import "SimpleRequest.h"
#import "Gen.h"
#import "Map.h"
#import "DataFormat.h"

#import "CorePlot-CocoaTouch.h"
#import "PlotSourceDataset.h"

#import "GraphThemePortrait.h"
#import "GraphThemeLandscape.h"

#import "ClickableHeaderView.h"

#define S_GRAPH_INTERVAL @"M.dashboard.graphinterval"

#define NO_DATA_LABEL_VIEW_TAG 1
#define GRAPH_VIEW_TAG 888

#define SMILEY_BUTTON_BACKGROUND_IMAGE_TAG 99999



// "class continuation" interface
@interface ScoreWidget ()

@property (nonatomic, readwrite) NSUInteger selectedTableRow;

- (void) plotSourceChangedForGraph:(CPXYGraph*)changedGraph inView:(UIView*)parentView;
- (CPXYGraph*)configureGraphInView:(CPLayerHostingView*)view withTheme:(CPTheme*)theme;
- (void) parseDataset;
- (void) swLineChanged;
- (NSArray*) parseGraphs:(NSArray*)graphs;
- (ClickableHeaderView*) clickableHeaderView;
- (void) createLegend;

@end


@implementation ScoreWidget

@synthesize titleL;
//, subtitleL, 
@synthesize contentContainer, flipView;
@synthesize noDataLabel;
@synthesize landscapeNoDataLabel;
@synthesize graphView;
@synthesize landscapeGraphView;
@synthesize legendView;
@synthesize portraitNavView;
@synthesize graphPortraitContainerView;

@synthesize swTableView, selectedTableRow;
@synthesize swScoreLabel, swBenchmarkLabel, swScoreFrame, swBenchmarkFrame, swHeaderView;

@synthesize layerHostView, graph;
@synthesize landscapeLayerHostView;
@synthesize landscapeGraph;
@synthesize holdCell;
@synthesize graphDisplayTimeInterval, graphDisplayIntervalControl;
@synthesize landscapeGraphDisplayIntervalControl;
@synthesize parsedPageData;
@synthesize currentGraphDataset;
@synthesize page;

@synthesize button1w;
@synthesize button1m;
@synthesize button3m;
@synthesize button6m;
@synthesize button1y;
@synthesize button2y;

@synthesize landscapeButton1w;
@synthesize landscapeButton1m;
@synthesize landscapeButton3m;
@synthesize landscapeButton6m;
@synthesize landscapeButton1y;
@synthesize landscapeButton2y;


/*
// TODO (max) Remove time period support properly.
- (SWPage_List*)pages {
	return self.settings.currentData;
}
 */


- (SWPage*) page
{
    SWPage* obj = self.settings.currentData;
    
    if ( obj != nil )
    {
        page = obj;
    }
    
    return obj;
}


- (id)init 
{
    // ÎÎÎ jjb -- this is nasty and completely wrong, but may be depended upon elsewhere, so leave it for now
	// max -- it's called from [Dashboard makeControllerForPart]

    
    if ( self = [super initWithNibName:@"ScoreWidget" bundle:nil] )
    {
        parsedPageData = nil;
    
        page = nil;

        currentOrientationIndex = 0;
        
        swHeaderView = nil;

        return self;
    }
    else
    {
        return nil;
    }
}


- (void)dealloc 
{
    NSLog(@"in ScoreWidget Dealloc");
    [[NSNotificationCenter defaultCenter] removeObserver:self];

    page = nil;

    [button1m release];
    [button1w release];
    [button1y release];
    [button2y release];
    [button3m release];
    [button6m release];
    [contentContainer release];
    [flipView release];
    [graph release];
    [graphDisplayIntervalControl release];
    [graphPortraitContainerView release];
    [graphView release];
    [landscapeButton1m release];
    [landscapeButton1w release];
    [landscapeButton1y release];
    [landscapeButton2y release];
    [landscapeButton3m release];
    [landscapeButton6m release];
    [landscapeGraph release];
    [landscapeGraphDisplayIntervalControl release];
    [landscapeGraphView release];
    [landscapeLayerHostView release];
    [landscapeNoDataLabel release];
    [layerHostView release];
    [legendView release];
    [noDataLabel release];
    [parsedPageData release];
    [portraitNavView release];
//    [subtitleL release];
    [swTableView release];
    [titleL release];

    [super dealloc]; 
}


- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    if (toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft ||
        toInterfaceOrientation == UIInterfaceOrientationLandscapeRight) {
        NSLog(@"Switching to landscape mode");

        // Switch to large dataset (TODO make this a constant)
        currentOrientationIndex = 1;

        if (!self.landscapeGraph) {
            NSLog(@"We were in portrait mode");
            // We were in portrait mode
            self.portraitNavView = [MedalliaAppDelegate sharedAppDelegate].tabCtlr.view;
            [MedalliaAppDelegate sharedAppDelegate].tabCtlr.view = landscapeGraphView;
            self.landscapeGraph = [self configureGraphInView:landscapeLayerHostView withTheme:[[[GraphThemeLandscape alloc] init] autorelease]];
            self.landscapeGraph.paddingBottom = 20.0;
            [self plotSourceChangedForGraph:self.landscapeGraph inView:landscapeGraphView];
        }
    }
    else {
        NSLog(@"Switching to portrait mode");
        // Assume we are rotating to normal portrait from landscape as we don't support Portrait upside-down

        // Switch to small dataset (TODO make this a constant)
        currentOrientationIndex = 0;

		[MedalliaAppDelegate sharedAppDelegate].tabCtlr.view = self.portraitNavView;
		[self plotSourceChangedForGraph:self.graph inView:portraitNavView];
		self.landscapeGraph = nil;
		if (self.legendView) {
			[self.legendView removeFromSuperview];
			self.legendView = nil;
		}
    }
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
}

- (CPXYGraph*)configureGraphInView:(CPLayerHostingView*)view withTheme:(CPTheme*)theme {
    CPXYGraph* newGraph = [[[CPXYGraph alloc] initWithFrame:view.bounds] autorelease];
    
    view.hostedLayer = newGraph;
    newGraph.paddingLeft = 30.0;
    newGraph.paddingTop = 10.0;
    newGraph.paddingRight = 10.0;
    newGraph.paddingBottom = 10.0;
    
    [newGraph applyTheme:theme];
    
    return newGraph;
}

- (void) updateGraphs {
    [self plotSourceChangedForGraph:self.graph inView:graphView];
	
    if (self.landscapeGraph) {
        [self plotSourceChangedForGraph:self.landscapeGraph inView:landscapeGraphView];
    }
}

- (void)buildInitialView {
	
    self.selectedTableRow = 0;
    
	int p = 0;
	id x = [self.settings.arguments objectForKey:@VIEWSTATE_SUBPAGE];
	if ([x isKindOfClass:[NSNumber class]]) p = [x intValue];	
	[self parseDataset];
    self.selectedTableRow = 0;
    [self.swTableView reloadData];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(timePeriodChanged:) name:TIME_INTERVAL_CHANGE_MESSAGE object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:TIME_INTERVAL_CHANGE_MESSAGE object:self];
}

- (void)viewDidLoad 
{
    [super viewDidLoad];

	NSMutableArray* tempTimeIntervalButtonArray = [NSMutableArray arrayWithCapacity:6];
	[tempTimeIntervalButtonArray addObject:button1w];
	[tempTimeIntervalButtonArray addObject:button1m];
	[tempTimeIntervalButtonArray addObject:button3m];
	[tempTimeIntervalButtonArray addObject:button6m];
	[tempTimeIntervalButtonArray addObject:button1y];
	[tempTimeIntervalButtonArray addObject:button2y];

	timeIntervalButtonArray = [[NSArray arrayWithArray:tempTimeIntervalButtonArray] retain];

	tempTimeIntervalButtonArray = [NSMutableArray arrayWithCapacity:6];
	[tempTimeIntervalButtonArray addObject:landscapeButton1w];
	[tempTimeIntervalButtonArray addObject:landscapeButton1m];
	[tempTimeIntervalButtonArray addObject:landscapeButton3m];
	[tempTimeIntervalButtonArray addObject:landscapeButton6m];
	[tempTimeIntervalButtonArray addObject:landscapeButton1y];
	[tempTimeIntervalButtonArray addObject:landscapeButton2y];

	landscapeTimeIntervalButtonArray = [[NSArray arrayWithArray:tempTimeIntervalButtonArray] retain];

    self.button2y.selected = YES;

	self.graph = [self configureGraphInView:layerHostView withTheme:[[[GraphThemePortrait alloc] init] autorelease]];
    
	if (self.settings.currentData)
    {
		[self buildInitialView];
	} 
    else 
    {
		self.titleL.text = self.settings.widgetType.name;
		//self.subtitleL.text = @"Loading...";
		[self startLoadingData];
	}
    
    
    
    // set up 'clickable header'layout info we'll need later
    [[NSBundle mainBundle] loadNibNamed:@"ScoreWidgetNum" owner:self options:nil];
    
    self.swScoreFrame = self.swScoreLabel.frame;
    self.swBenchmarkFrame = self.swBenchmarkLabel.frame;
    
    self.swScoreLabel = nil;
    self.swBenchmarkLabel = nil;
    
    
    
    self.swTableView.scrollsToTop = YES;
    
        
    self.swTableView.tableHeaderView = self.clickableHeaderView;
    
    
    
}


- (void)requestSucceeded:(NSData*)data {	
	[self.spinner stopAnimating];
    
	self.settings.currentData = (id) [SWPage fromPlist:[data parsePlist]];
	[self buildInitialView];

	[swHeaderView setButtonLabels];
	[swHeaderView setBenchmarkTitle:page.benchTitle];
}

- (void)requestFailed:(NSError*)err {
	[self.spinner stopAnimating];
	self.titleL.text = @"Network error";
	//self.subtitleL.text = [err description];
}

- (ClickableHeaderView*) clickableHeaderView
{
    if ( swHeaderView == nil )
    {
        NSDictionary* proxies = [NSDictionary dictionaryWithObject:[UIImage imageNamed:@"widget_filter_bg_bordered.png"] forKey:@"ClickableHeaderBackgroundImage"];
        NSDictionary* options = [NSDictionary dictionaryWithObject:proxies forKey:UINibExternalObjects];
        
        NSArray* topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"ClickableHeaderView" owner:self options:options];

        ClickableHeaderView* header = nil;
        UIImage* img = nil;
        
        for ( NSObject* obj in topLevelObjects )
        {
            if ( [obj isMemberOfClass:[ClickableHeaderView class]] )
            {
                header = (ClickableHeaderView*)obj;
            }
            else if ( [obj isMemberOfClass:[UIImage class]] )
            {
                img = (UIImage*)obj;
            }
        }
        
        if ( header != nil )
        {
            swHeaderView = [header retain];
            
            [swHeaderView setBackgroundImage:img];
			
			swHeaderView.delegate = self;
			[swHeaderView setButtonLabels];
            
            
            //TODO: replace current background image on smiley with stretchable version of itself
            
            UIImageView* smileyBackgroundImageView = (UIImageView*)[swHeaderView.smileyElement viewWithTag:SMILEY_BUTTON_BACKGROUND_IMAGE_TAG];
            UIImage* stretchableImage = [[UIImage imageNamed:@"response_current_filter_bt.png"] stretchableImageWithLeftCapWidth:2.0 topCapHeight:0.0];
            smileyBackgroundImageView.image = stretchableImage;
            
            
            // view controller specific visual elements
            swHeaderView.haveCommentsElement.hidden = YES;
            //[swHeaderView.haveCommentsElement removeFromSuperview];
            //swHeaderView.haveCommentsElement = nil;
            
            swHeaderView.smileyElement.hidden = NO;
            swHeaderView.rankerQuestionLabel.hidden = YES;
            
            
            // grooves
            swHeaderView.topSideGrooveView.hidden = YES;
            swHeaderView.rightSideGrooveView.hidden = YES;
        }
    }
    
    return swHeaderView;
}


- (void)upperRightCornerPressedAction {
	MedalliaAppDelegate * medalliaApp = [MedalliaAppDelegate sharedAppDelegate];
	SharableTabBarController* tabCtlr = medalliaApp.tabCtlr;
	tabCtlr.selectedViewController = medalliaApp.responsesTab;
	[tabCtlr updateNavItems:YES];
	
	WidgetInstance * currentWidgetInstance = [self getWidgetInstance];
	WidgetInstance * responsesWidgetInstance = medalliaApp.responsesTab.settings;
	for (ConfigSection *sect in currentWidgetInstance.widgetType.configSections) {
		for (ConfigPart *cp in sect.parts) {
			[responsesWidgetInstance.arguments setValue:[currentWidgetInstance.arguments objectForKey:cp.key] forKey:cp.key];
		}
	}
	[medalliaApp.responsesTab setSelectedScopeButton:currentWidgetInstance.widgetType.responsesFilterButtonKey];
	[medalliaApp.responsesTab widgetSaved:responsesWidgetInstance position:-1];
}




#pragma -
#pragma === Graph Display Change Response Methods ===
#pragma -


- (IBAction) timePeriodButtonPressed:(id)sender {
	for (int i = 0; i < timeIntervalButtonArray.count; ++i) {
		UIButton *portraitButton = [timeIntervalButtonArray objectAtIndex:i];
		UIButton *landscapeButton = [landscapeTimeIntervalButtonArray objectAtIndex:i];

		if (sender == portraitButton || sender == landscapeButton) {
            [[NSUserDefaults standardUserDefaults] setInteger:i forKey:S_GRAPH_INTERVAL];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [[NSNotificationCenter defaultCenter] postNotificationName:TIME_INTERVAL_CHANGE_MESSAGE object:self];
            return;
		}
	}
}

- (void) timePeriodChanged:(NSNotification*)n {
    self.graphDisplayTimeInterval = [[NSUserDefaults standardUserDefaults] integerForKey:S_GRAPH_INTERVAL];

	for (int i = 0; i < timeIntervalButtonArray.count; ++i) {
		UIButton *portraitButton = [timeIntervalButtonArray objectAtIndex:i];
		UIButton *landscapeButton = [landscapeTimeIntervalButtonArray objectAtIndex:i];

		if (i == self.graphDisplayTimeInterval) {
			portraitButton.selected = YES;
			landscapeButton.selected = YES;
		}
		else {
			portraitButton.selected = NO;
			landscapeButton.selected = NO;
		}
	}
    [self updateGraphs];
}


- (void) swLineChanged
{
	[self plotSourceChangedForGraph:self.graph inView:graphView];
}




- (void) parseDataset
{
    NSLog (@"parseDataset: Page title:'%@'", self.page.title );

    if ( self.page == nil ) {
        return;
    }

	parsedPageData = [[NSMutableArray alloc] init];

	for ( NSUInteger k = 0; k < self.page.lines.count; k++ ) {
		SWLine* aLine = [self.page.lines objectAtIndex:k];
		
		NSMutableArray* newLineArray = [NSMutableArray array];
							
		[newLineArray addObject:[self parseGraphs:aLine.graphSmall]];
		[newLineArray addObject:[self parseGraphs:aLine.graphLarge]];
		
		// add a new array to the data model to represent *this* page's data
		[self.parsedPageData addObject:newLineArray];
	}
}

- (NSArray*) parseGraphs:(NSArray*)graphList {
    NSMutableArray* graphDatasetArray = [[[NSMutableArray alloc] init] autorelease];

    for (Graph* aGraph in graphList) {
        GraphDataset* graphDataset = [[[GraphDataset alloc] initWithServerGraph:aGraph lineStyles:page.lineStyles] autorelease];
        [graphDatasetArray addObject:graphDataset];
    }

    return graphDatasetArray;
}    


#pragma mark -
#pragma mark === UITableView Delegate & DataSource Methods ===
#pragma mark -


- (NSInteger)numberOfSectionsInTableView:(UITableView*)tableView
{
	return 1;
}

/*
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if ( section == 0 )
    {
        return shadowSectionHeader;
    }
    else
    {
        return nil;
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if ( section == 0 )
    {
        return shadowSectionHeader.frame.size.height;
    }
    else
    {
        return 0.0;
    }
}
 */


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return self.page.lines.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
	static NSString *GenericCellIdentifier = @"ScoreWidgetNumCellIdentifier";
	//static NSString *EditableCellIdentifier = @"EditableCellIdentifier";

    static UIImage* cellBackgroundImage = nil;
    
    
    ScoreWidgetNum* cell = (ScoreWidgetNum*)[tableView dequeueReusableCellWithIdentifier:GenericCellIdentifier];

    if ( cell == nil )
    {
        [[NSBundle mainBundle] loadNibNamed:@"ScoreWidgetNum" owner:self options:nil];
        
        cell = self.holdCell;
        self.holdCell = nil;
    }
    
    SWLine* dataLine = [self.page.lines objectAtIndex:indexPath.row];
    
    
    
    
    
    if ( cell.backgroundView == nil )
    {
        if ( cellBackgroundImage == nil )
        {
            UIImage* originalCellBackground = [UIImage imageNamed:@"score_widget_table_cell_bg.png"];
            cellBackgroundImage = [[originalCellBackground stretchableImageWithLeftCapWidth:4.0 topCapHeight:37.0] retain];
        }
        
        
        
        UIImageView* backgroundView = [[UIImageView alloc] initWithImage:cellBackgroundImage];
        backgroundView.contentMode = UIViewContentModeScaleToFill;
        cell.backgroundView = backgroundView;
        
        [backgroundView release];
    }
    
    
    
    
    if ( dataLine != nil )
    {
        cell.titleL.text = dataLine.title;
        cell.differenceL.text = dataLine.difference;
        cell.numberL.text = dataLine.value;
        cell.differenceL.textColor = dataLine.diffColor;	
        
        NSString* backgroundColor = dataLine.scoreBackgroundColor;
        
        if ( backgroundColor == nil ) {
            cell.backgroundImage.image = nil;
			cell.numberL.textColor = UIColor.blackColor;
			cell.numberL.shadowColor = nil;
        } else {
            cell.backgroundImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"db-score-bg-%@.png", backgroundColor]];
			cell.numberL.textColor = UIColor.whiteColor;
			cell.numberL.shadowColor = UIColor.blackColor;
        }
        
        cell.checkboxImageView.hidden = !( indexPath.row == self.selectedTableRow );
    }
    else
    {
        return nil;
    }
    
    return cell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
        
    NSIndexPath* oldIP = [NSIndexPath indexPathForRow:self.selectedTableRow inSection:indexPath.section];
    
    ScoreWidgetNum* oldCell = (ScoreWidgetNum*)[tableView cellForRowAtIndexPath:oldIP];
    oldCell.checkboxImageView.hidden = YES;
    
    ScoreWidgetNum* newCell = (ScoreWidgetNum*)[tableView cellForRowAtIndexPath:indexPath];
    newCell.checkboxImageView.hidden = NO;
    
    self.selectedTableRow = indexPath.row;
    
    [self swLineChanged];
}


- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return UITableViewCellEditingStyleNone;
}


- (void) plotSourceChangedForGraph:(CPXYGraph*)changedGraph inView:(UIView*)parentView
{
    self.graphView.hidden = NO;

    NSArray* arraysAtLineInTable = [self.parsedPageData objectAtIndex:self.selectedTableRow];
    NSArray* arrayOfGraphsForCurrentOrientation = [arraysAtLineInTable objectAtIndex:currentOrientationIndex];
    self.currentGraphDataset = [arrayOfGraphsForCurrentOrientation objectAtIndex:self.graphDisplayTimeInterval];

    [parentView viewWithTag:NO_DATA_LABEL_VIEW_TAG].hidden = self.currentGraphDataset.hasData;
    [parentView viewWithTag:GRAPH_VIEW_TAG].hidden = !self.currentGraphDataset.hasData;
    self.legendView.hidden = !self.currentGraphDataset.hasData;

    BOOL isLandscape = changedGraph.bounds.size.height > 150.0;
    if (isLandscape) {
        GraphLabel_List* labelList = [page.timePeriods objectAtIndex:self.graphDisplayTimeInterval].labelsLargeGraph;
        [self.currentGraphDataset applyToGraph:changedGraph labels:labelList];
        [self createLegend];
    }
    else {
        [self.currentGraphDataset applyToGraph:changedGraph labels:nil];
    }
}

- (void) createLegend {
    if (!self.legendView) {
		if (page.lineStyles.count <= 1) {
			// Do not display legend when there is only one line
			return;
		}
        self.legendView = [[[UIView alloc] init] autorelease];
        self.legendView.alpha = 0.7;
        self.legendView.backgroundColor = [UIColor blackColor];
        self.legendView.opaque = NO;

        NSUInteger maxWidth = 0;

        for (NSUInteger i = 0; i < page.lineStyles.count; ++i) {
            LineStyle* style = [page.lineStyles objectAtIndex:i];

            CGRect legendLabelFrame = CGRectMake(5, i * 20 + 5, self.legendView.bounds.size.width, 20);
            UILabel* legendLabel = [[UILabel alloc] initWithFrame:legendLabelFrame];

            legendLabel.textColor = style.color;
            legendLabel.backgroundColor = [UIColor clearColor];
            legendLabel.text = style.legendText;
            legendLabel.adjustsFontSizeToFitWidth = YES;
            CGSize textSize = [legendLabel.text sizeWithFont:legendLabel.font];
            maxWidth = MAX(maxWidth, textSize.width);

            [self.legendView addSubview:legendLabel];
            [legendLabel release];
        }

        CGFloat labelWidth = MIN(maxWidth, 150);
        CGFloat legendWidth = labelWidth + 10;
        CGFloat legendHeight = page.lineStyles.count * 20 + 10;
        CGFloat legendX = 480 - legendWidth - 20;
        CGFloat legendY = 320 - legendHeight - 40;

        self.legendView.frame = CGRectMake(legendX, legendY, legendWidth, legendHeight);

        for (UIView* labelView in self.legendView.subviews) {
            CGRect frame = labelView.frame;
            labelView.frame = CGRectMake(frame.origin.x, frame.origin.y, labelWidth, frame.size.height);
        }

        [self.landscapeGraphView addSubview:self.legendView];
    }
}



#pragma mark -
#pragma mark === UIScrollViewDelegate Methods ===
#pragma mark -



- (BOOL)scrollViewShouldScrollToTop:(UIScrollView *)scrollView
{
    return YES;
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if ( scrollView == self.swTableView )
    {
        //NSLog( @"[ScoreWidget.swTableview scrollViewDidScroll]" );
    }
    
    
}


@end
