//
//  RankerTableViewCell.h
//  Medallia
//
//  Created by Max Bennedich on 3/4/10.
//  Copyright 2010 Medallia, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface /*NOGENERATE*/ RankerTableViewCell : UITableViewCell {
	UILabel *index;
	UILabel *unitName;
	UILabel *score;
	UILabel *difference;
	UIImageView *selectedImage;
}

@property(nonatomic,retain) IBOutlet UILabel *index;
@property(nonatomic,retain) IBOutlet UILabel *unitName;
@property(nonatomic,retain) IBOutlet UILabel *score;
@property(nonatomic,retain) IBOutlet UILabel *difference;
@property(nonatomic,retain) IBOutlet UIImageView *selectedImage;

@end
