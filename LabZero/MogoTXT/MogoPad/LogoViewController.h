//
//  LogoViewController.h
//  MogoPad
//
//  Created by Shiun Hwang on 10/13/10.
//  Copyright 2010 HowLand Software. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface LogoViewController : UIViewController {
	NSTimer *splashTimer;
	IBOutlet UIActivityIndicatorView *activityIndicator;
}

@property (nonatomic, retain) NSTimer *splashTimer;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *activityIndicator;

@end
