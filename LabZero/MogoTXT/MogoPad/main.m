//
//  main.m
//  MogoPad
//
//  Created by Jeff Barbose on 9/13/10.
//  Copyright (c) 2010 HowLand Software. All rights reserved.
//


#import <UIKit/UIKit.h>

int main(int argc, char *argv[])
{

    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
    int retVal = UIApplicationMain(argc, argv, nil, nil);
    [pool release];
    return retVal;

}

