//
//  ImageScrollView.h
//  MogoPad
//
//  Created by Shiun Hwang on 10/3/10.
//  Copyright 2010 HowLand Software. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ImageScrollView : UIScrollView <UIScrollViewDelegate> {
    UIView        *imageView;
    NSUInteger     index;
	NSString	  *exifDescription;
	NSString	  *urlString;
}
@property (assign) NSUInteger index;
@property (nonatomic, retain) NSString *exifDescription;
@property (nonatomic, retain) NSString *urlString;

- (void)clearImage;
- (void)displayImage:(UIImage *)image;
- (void)configureForImageSize:(CGSize)imageSize;

@end
