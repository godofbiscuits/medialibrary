//
//  BasicWebViewController.m
//  MogoPad
//
//  Created by Jeff Barbose on 9/13/10.
//  Copyright (c) 2010 HowLand Software. All rights reserved.
//
#import <Foundation/Foundation.h>

#import "BasicWebViewController.h"
#import "LogoViewController.h"
#import "MogoPadAppDelegate.h"
#import "PhotoScrollerViewController.h"

//#define kMogoURL @"http://mogotxt.3jam.com/mogopad"
NSString *const kMogoURL = @"http://mogotxt.com/mogopad";
#define kUrlListDelimiter @"\n"
#define kJavaScriptFunction @"image_list();"
#define kJavaScriptTitleFunction @"page_title();"
#define kJavaScriptGameFunction @"game_title();"


NSString *const kMogoURLScheme = @"mogo";
NSString *const kMogoHTTPScheme = @"http";
NSString *const kMogoFTPScheme = @"ftp";



@interface BasicWebViewController (Private)

- (void) loadWebView;

- (NSArray *) urlListFromWebView:(UIWebView *)webView;
- (NSString *) getTitleFromWebView:(UIWebView *)webView;
- (NSString *) getGameTitleFromWebView:(UIWebView *)webView;

@end



@interface NSURL (Mogo)

- (BOOL) isMogo;

@end


@interface BasicWebViewController (URLUtilities)

- (MogoURLSchemeType) schemeTypeForURL:(NSURL *)url;
- (BOOL) shouldPushNewWebViewControllerForSchemeType:(MogoURLSchemeType)mogoSchemeType;
- (BOOL) shouldPushNewWebViewForURL:(NSURL *)tappedURL;
- (BOOL) isMogoURL:(NSURL *)url;

@end


@implementation BasicWebViewController
@synthesize myWebView;
@synthesize activityIndicator;
@synthesize shouldShowLogo;
@synthesize theUrl;

// The designated initializer. Override to perform setup that is required before the view is loaded.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil 
{
//	self.hidesBottomBarWhenPushed = YES;
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) 
    {
        interceptLinks = NO;
		shouldShowLogo = NO;
    }
    return self;
}

- (id) init 
{
    // self.hidesBottomBarWhenPushed = YES;
    if ( self = [super init] )
    {
		shouldShowLogo = NO;
        return self;
    }
    else
    {
        return nil;
    }
    
}
/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad 
 {
     // !!! -- jjb all this "appIsStartingUp" stuff should be moved up into the app delegate because it's obvious it's the stuff 
     // that only happens at app startup, which is the purview of the app delegate.
     
     
     [super viewDidLoad];
     
//     BOOL appIsStartingUp = ((MogoPadAppDelegate *)([UIApplication sharedApplication].delegate)).isStartingUp;
     
//     if ( appIsStartingUp == YES) {
	 if (self.shouldShowLogo) {
             LogoViewController *l = [[[LogoViewController alloc] initWithNibName:@"LogoViewController" bundle:nil] autorelease];
             [self presentModalViewController:l animated:NO];
		 self.shouldShowLogo = NO;
     }
     self.activityIndicator.hidesWhenStopped = YES;
     [self.activityIndicator startAnimating];
     self.hidesBottomBarWhenPushed = YES;
     
     interceptLinks = NO;

     [self loadWebView];
 }


// Ensure that the view controller supports rotation and that the split view can therefore show in both portrait and landscape.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return YES;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
	[super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
}


- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)dealloc {
    [super dealloc];
	[myWebView release];
	myWebView = nil;
	[activityIndicator release];
	activityIndicator = nil;
	[theUrl release];
	theUrl = nil;
}


#pragma mark -
#pragma mark Private
#pragma mark  

- (void) loadWebView {	
	
    self.myWebView.backgroundColor = [UIColor whiteColor];
    self.myWebView.scalesPageToFit = YES;
    self.myWebView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
    self.myWebView.delegate = self;
    
    [self.myWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.theUrl] cachePolicy:NSURLRequestReloadRevalidatingCacheData timeoutInterval:60.0]];
}


#pragma mark -
#pragma mark URLUtilities
#pragma mark  



- (MogoURLSchemeType) schemeTypeForURL:(NSURL *)url
{
    MogoURLSchemeType schemeType = kMogoWebViewURLSchemeUndefined;

    NSString *scheme = url.scheme;
    
    if ( [scheme isEqualToString:kMogoURLScheme] )
    {
        schemeType = kMogoWebViewURLSchemeMogo;
    }
    else if ( [scheme isEqualToString:kMogoHTTPScheme] )
    {
        schemeType = kMogoWebViewURLSchemeHTTP;
    }
    else if ( [scheme isEqualToString:kMogoFTPScheme] )
    {
        schemeType = kMogoWebViewURLSchemeFTP;
    }
    else
    {
        schemeType = kMogoWebViewURLSchemeUndefined;
    }
    
    return schemeType;
}



- (BOOL) shouldPushNewWebViewControllerForSchemeType:(MogoURLSchemeType)mogoSchemeType
{
    return ( mogoSchemeType == kMogoWebViewURLSchemeHTTP );
}



- (BOOL) shouldPushNewWebViewForURL:(NSURL *)url
{
    MogoURLSchemeType schemeType = [self schemeTypeForURL:url];
    
    return [self shouldPushNewWebViewControllerForSchemeType:schemeType];
}


- (BOOL) isMogoURL:(NSURL *)url
{
    return ( [self schemeTypeForURL:url] == kMogoWebViewURLSchemeMogo );
}




#pragma mark -

#define kMogoTagLength 5

-(BOOL) webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if ( interceptLinks == YES )
    {
        NSURL *url = request.URL;
        
        if ( [url isMogo] )
        {
			interceptLinks = NO;
			[self.myWebView reload];            
            PhotoScrollerViewController *p = [[PhotoScrollerViewController alloc] init];
            
            NSString *host = [url host];
            p.pageIndex = (NSUInteger)[host integerValue] - 1;
			p.title = [self getGameTitleFromWebView:self.myWebView];
			NSArray *urls = [self urlListFromWebView:self.myWebView];
            p.urlList = urls;
			p.hidesBottomBarWhenPushed = YES;
			
            [self.navigationController pushViewController:p animated:YES];
            [p release];
            return NO;
        }
        else
        {
			NSLog(@"Pushing Basic View Controller with url = %@", [request.URL absoluteString]);
			BasicWebViewController *b = [[BasicWebViewController alloc] init];
			b.theUrl = [request.URL absoluteString];
			[self.navigationController pushViewController:b animated:YES];
			[b release];
            return NO;
        }
    }
    else
    {
        interceptLinks = YES;  // don't intercept the load request the first time thru, since it's the load of the initial page
        
        return YES;
    }
}




- (NSArray *) urlListFromWebView:(UIWebView *)webView {
	NSString *string = [webView stringByEvaluatingJavaScriptFromString:kJavaScriptFunction];
	NSArray *urlList = [string componentsSeparatedByString:kUrlListDelimiter];
	NSLog(@"JAVASCRIPT RETURNED %i urls", [urlList count]);
	return urlList;
}

- (NSString *) getTitleFromWebView:(UIWebView *)webView {
	NSString *title = [webView stringByEvaluatingJavaScriptFromString:kJavaScriptTitleFunction];
	return title;
}


- (NSString *) getGameTitleFromWebView:(UIWebView *)webView {
	NSString *title = [webView stringByEvaluatingJavaScriptFromString:kJavaScriptGameFunction];
	return title;
}
/*
-(BOOL) webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{

	
    //You might need to set up a interceptLinks-Bool since you don't want to intercept the initial loading of the content
	if (self.interceptLinks) {
		NSURL *url = request.URL;
		NSLog(@"%@", url);
		NSArray *parts = [[url absoluteString] componentsSeparatedByString:@"/"];
		NSLog(@"LAST COMPONENT = %@", [parts objectAtIndex:([parts count] - 1)]);
 		PhotoScrollerViewController *p = [[PhotoScrollerViewController alloc] init];
		p.pageIndex = [[parts objectAtIndex:([parts count] - 1)] integerValue];
		p.hidesBottomBarWhenPushed = YES;
		[self.navigationController pushViewController:p animated:YES];
		
        return NO;
    }
    //No need to intercept the initial request to fill the WebView
    else {
		NSURL *url = request.URL;
		NSLog(@"WebView: %@", url);
		self.interceptLinks = YES;
        return YES;
    }
	
}
*/
- (void)webViewDidFinishLoad:(UIWebView *)webView {
	if (((MogoPadAppDelegate *)([UIApplication sharedApplication].delegate)).isStartingUp == YES) {
		[self dismissModalViewControllerAnimated:YES];
		((MogoPadAppDelegate *)([UIApplication sharedApplication].delegate)).isStartingUp = NO;
	}
	
	self.title = [self getTitleFromWebView:webView];
	NSLog(@"Setting new title = %@", self.title);
	[self.activityIndicator stopAnimating];
}

@end


@implementation NSURL (Mogo)

- (BOOL) isMogo
{
    return ( [self.scheme isEqualToString:kMogoURLScheme] );
}

@end



