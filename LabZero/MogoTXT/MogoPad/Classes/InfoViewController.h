//
//  InfoViewController.h
//  MogoPad
//
//  Created by Shiun Hwang on 10/18/10.
//  Copyright 2010 HowLand Software. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface InfoViewController : UIViewController {
	IBOutlet UITextView *textView;
}

@property (nonatomic, retain) UITextView *textView;

@end
