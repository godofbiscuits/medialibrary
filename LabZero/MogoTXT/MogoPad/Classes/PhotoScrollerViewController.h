//
//  SecondViewController.h
//  MogoPad
//
//  Created by Jeff Barbose on 9/13/10.
//  Copyright (c) 2010 HowLand Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImageScrollView.h"
#import "EXF.h"
#import "ASINetworkQueue.h"

@class InfoViewController;

@interface PhotoScrollerViewController : UIViewController <UIScrollViewDelegate> {
    UIScrollView *pagingScrollView;
    
    NSMutableSet *recycledPages;
    NSMutableSet *visiblePages;
	
	NSInteger pageIndex;
	
	NSArray *urlList;
	
	EXFJpeg *exfData;
	
	UIPopoverController *infoPopoverController;
	InfoViewController *infoViewController;
	
	ASINetworkQueue *networkRequestQueue;
}

@property NSInteger pageIndex;
@property (nonatomic, retain) NSArray *urlList;
@property (nonatomic, retain) EXFJpeg *exfData;

@property (nonatomic, retain) UIPopoverController *infoPopoverController;
@property (nonatomic, retain) InfoViewController *infoViewController;

@property (nonatomic, retain) ASINetworkQueue *networkRequestQueue;

- (void)configurePage:(ImageScrollView *)page forIndex:(NSUInteger)index;
- (BOOL)isDisplayingPageForIndex:(NSUInteger)index;
- (CGRect)frameForPagingScrollView;
- (CGRect)frameForPageAtIndex:(NSUInteger)index;

- (void)tilePages;
- (ImageScrollView *)dequeueRecycledPage;

@end
