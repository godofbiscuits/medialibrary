//
//  MogoPadAppDelegate.h
//  MogoPad
//
//  Created by Jeff Barbose on 9/13/10.
//  Copyright (c) 2010 HowLand Software. All rights reserved.
//


#import <UIKit/UIKit.h>

@interface MogoPadAppDelegate : NSObject <UIApplicationDelegate, UITabBarControllerDelegate> {
    UIWindow *window;

    UINavigationController *navigationController;
	BOOL isStartingUp;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet UINavigationController *navigationController;
@property (nonatomic) BOOL isStartingUp;

@end

