//
//  MogoPadAppDelegate.m
//  MogoPad
//
//  Created by Jeff Barbose on 9/13/10.
//  Copyright (c) 2010 HowLand Software. All rights reserved.
//


#import "MogoPadAppDelegate.h"
#import "BasicWebViewController.h"

BOOL gLogging = NO;

@implementation MogoPadAppDelegate

@synthesize navigationController;
@synthesize window;
@synthesize isStartingUp;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    // Override point for customization after application launch.
    // Add the tab bar controller's current view as a subview of the window
	
	isStartingUp = YES;
	BasicWebViewController *b = (BasicWebViewController *)navigationController.topViewController;
	b.shouldShowLogo = YES;
	b.theUrl = kMogoURL;
	b.title = @"MogoPad";
	
    [window addSubview:navigationController.view];
	
    [window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillTerminate:(UIApplication *)application {

    // Save data if appropriate.
}

- (void)dealloc {

    [window release];
    [navigationController release];
    [super dealloc];
}

/*
// Optional UITabBarControllerDelegate method.
- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController {
}
*/

/*
// Optional UITabBarControllerDelegate method.
- (void)tabBarController:(UITabBarController *)tabBarController didEndCustomizingViewControllers:(NSArray *)viewControllers changed:(BOOL)changed {
}
*/

@end

