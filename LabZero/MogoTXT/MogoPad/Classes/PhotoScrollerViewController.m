//
//  SecondViewController.m
//  MogoPad
//
//  Created by Jeff Barbose on 9/13/10.
//  Copyright (c) 2010 HowLand Software. All rights reserved.
//

#import "PhotoScrollerViewController.h"
#import "ASIHTTPRequest.h"
#import "InfoViewController.h"

// This buffer increased the number of pages to load onto app in the background.
// increasing this value will use more memory footprint but may reduce load time
// for new pages through pre-fetching.  The value is for the number of pages to fetch
// ahead as well as behind.  i.e. 4 = 4 pages ahead + 4 pages behind = 8~9 pages.
#define kPageBuffer 2

@interface PhotoScrollerViewController (private)
- (CGRect) adjustFrameForOrientation:(CGRect) original;
- (void) adjustPagingScrollView;
- (void) grabURLInBackground:(NSString *)urlString;
- (NSString *) extractImageEXIFDescriptionFromData:(NSData *)data;
- (void) initializeInfoPopover;
- (void) displayInfoPopover;
- (NSString *)getEXIFDescriptionForIndex:(NSUInteger)index;
- (ImageScrollView *)getVisiblePageForUrlString:(NSString *)urlString;
- (void) recycleAllVisiblePages;

@end

@implementation PhotoScrollerViewController

@synthesize pageIndex;
@synthesize urlList;
@synthesize exfData;
@synthesize infoPopoverController;
@synthesize infoViewController;
@synthesize networkRequestQueue;

- (id) init {
	self = [super init];
	if (self) {
		[self initializeInfoPopover];
		networkRequestQueue = [[ASINetworkQueue alloc] init];
		networkRequestQueue.maxConcurrentOperationCount = (kPageBuffer * 2) + 1;
		[networkRequestQueue setShouldCancelAllRequestsOnFailure:NO];
	}
	return self;
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Description"
																			  style:UIBarButtonItemStylePlain
																			 target:self 
																			 action:@selector(displayInfoPopover)];	
}


// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	return YES;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
	[super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
	[self recycleAllVisiblePages];
	[self tilePages];
	[self adjustPagingScrollView];
//	[pagingScrollView setNeedsDisplay];
	[super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void) viewWillDisappear:(BOOL)animated
{
	[self.infoPopoverController dismissPopoverAnimated:NO];
	[self.networkRequestQueue cancelAllOperations];
    [super viewWillDisappear:animated];
}

#pragma mark -
#pragma mark View loading and unloading

- (void)loadView 
{   
    // Step 1: make the outer paging scroll view
    CGRect pagingScrollViewFrame = [self frameForPagingScrollView];
    pagingScrollView = [[UIScrollView alloc] initWithFrame:pagingScrollViewFrame];
    pagingScrollView.pagingEnabled = YES;
    pagingScrollView.backgroundColor = [UIColor blackColor];
    pagingScrollView.showsVerticalScrollIndicator = NO;
    pagingScrollView.showsHorizontalScrollIndicator = NO;
	pagingScrollView.bounces = NO;
	
    pagingScrollView.contentSize = CGSizeMake(pagingScrollViewFrame.size.width * [self.urlList count],
                                              pagingScrollViewFrame.size.height / 2);
	
	CGPoint pageOffset = CGPointMake(pagingScrollViewFrame.size.width * (self.pageIndex), 0);
	[pagingScrollView setContentOffset:pageOffset animated:NO];
	pagingScrollView.delegate = self;
    self.view = pagingScrollView;
    
    // Step 2: prepare to tile content
    recycledPages = [[NSMutableSet alloc] init];
    visiblePages  = [[NSMutableSet alloc] init];
    [self tilePages];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    [pagingScrollView release];
    pagingScrollView = nil;
    [recycledPages release];
    recycledPages = nil;
    [visiblePages release];
    visiblePages = nil;
	[urlList release];
	urlList = nil;
	[networkRequestQueue release];
	networkRequestQueue = nil;
}

- (void)dealloc
{
    [pagingScrollView release];
	[exfData release];
	[infoPopoverController release];
	[infoViewController release];
	[recycledPages release];
	[visiblePages release];
	[urlList release];
	[networkRequestQueue release];
    [super dealloc];
}


#pragma mark -
#pragma mark Tiling and page configuration

- (void)tilePages 
{
    // Calculate which pages are visible
    CGRect visibleBounds = pagingScrollView.bounds;
	
    int firstNeededPageIndex = floorf(CGRectGetMinX(visibleBounds) / CGRectGetWidth(visibleBounds));
    int lastNeededPageIndex  = floorf((CGRectGetMaxX(visibleBounds)-1) / CGRectGetWidth(visibleBounds));
    firstNeededPageIndex = MAX(firstNeededPageIndex - kPageBuffer, 0);
    lastNeededPageIndex  = MIN(lastNeededPageIndex + kPageBuffer, [self.urlList count] - 1);
	
    // Recycle no-longer-visible pages 
    for (ImageScrollView *page in visiblePages) {
        if (page.index < firstNeededPageIndex || page.index > lastNeededPageIndex) {
            [recycledPages addObject:page];
            [page removeFromSuperview];
			[page clearImage];
        }
    }
    [visiblePages minusSet:recycledPages];
    
    // add missing pages
    for (int index = firstNeededPageIndex; index <= lastNeededPageIndex; index++) {
        if (![self isDisplayingPageForIndex:index]) {
            ImageScrollView *page = [self dequeueRecycledPage];
            if (page == nil) {
                page = [[[ImageScrollView alloc] init] autorelease];
            }
            [self configurePage:page forIndex:index];
            [pagingScrollView addSubview:page];
            [visiblePages addObject:page];
        }
    }
	[self.networkRequestQueue go];
}

- (ImageScrollView *)dequeueRecycledPage
{
    ImageScrollView *page = [recycledPages anyObject];
    if (page) {
        [[page retain] autorelease];
        [recycledPages removeObject:page];
    }
    return page;
}

- (BOOL)isDisplayingPageForIndex:(NSUInteger)index
{
    BOOL foundPage = NO;
    for (ImageScrollView *page in visiblePages) {
        if (page.index == index) {
            foundPage = YES;
            break;
        }
    }
    return foundPage;
}

- (void)configurePage:(ImageScrollView *)page forIndex:(NSUInteger)index
{
    page.index = index;
    page.frame = [self frameForPageAtIndex:index];
	page.urlString = [self.urlList objectAtIndex:index];
    
	// We should call grabURLInBackground
	// and on completion the completion will
	// call [page displayImage:[self imageAtIndex:index];
	if (index > ([self.urlList count] - 1)) {
		NSLog(@"SOMETHING's GONE WRONG: Url List size = %i, index = %i", [self.urlList count], index);
		return;
	}
	
    // !!! jjb - do this to get an imageView stuffed in there for now
    [page displayImage:nil];
    
    [self grabURLInBackground:[self.urlList objectAtIndex:index]];
}


#pragma mark -
#pragma mark ScrollView delegate methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self tilePages];
}


- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    CGRect visibleBounds = pagingScrollView.bounds;
	self.pageIndex = (floorf(CGRectGetMinX(visibleBounds)/ CGRectGetWidth(visibleBounds)));
	NSLog(@"scrollview did end decelerating:  self.pageIndex = %i", self.pageIndex);
}

#pragma mark -
#pragma mark  Frame calculations
#define PADDING  0

- (CGRect)frameForPagingScrollView {
    CGRect frame = [[UIScreen mainScreen] bounds];
	frame.origin.x -= PADDING;
    frame.size.width += (2 * PADDING);
	
	frame = [self adjustFrameForOrientation:frame];
    return frame;
}

- (CGRect)frameForPageAtIndex:(NSUInteger)index {
    CGRect pagingScrollViewFrame = [self frameForPagingScrollView];
	
    CGRect pageFrame = pagingScrollViewFrame;
    pageFrame.size.width -= (2 * PADDING);
    pageFrame.origin.x = (pagingScrollViewFrame.size.width * index) + PADDING;
    return pageFrame;
}


#pragma mark -
#pragma mark Image wrangling

- (CGRect) adjustFrameForOrientation:(CGRect) original  {
	
	if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation)) {
		return original;
	} else {
		NSLog(@"Adjusting");
		CGRect adjusted = CGRectMake(original.origin.x, original.origin.y, original.size.height, original.size.width);
		return adjusted;
	}
}

- (void) adjustPagingScrollView {
    // Step 1: make the outer paging scroll view
    CGRect pagingScrollViewFrame = [self frameForPagingScrollView];
	pagingScrollView.contentSize = CGSizeMake(pagingScrollViewFrame.size.width * [self.urlList count],
                                              pagingScrollViewFrame.size.height / 2);
	
	CGPoint pageOffset = CGPointMake(pagingScrollViewFrame.size.width * (self.pageIndex), 0);
	[pagingScrollView setContentOffset:pageOffset animated:NO];

	CGSize offsetSize;
	
	if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation)) {
		offsetSize = CGSizeMake(768, 1024);
	} else {
		offsetSize = CGSizeMake(1024, 768);
	}
	CGRect offsetRect = CGRectMake(pageOffset.x, pageOffset.y, offsetSize.width, offsetSize.height);
	[pagingScrollView scrollRectToVisible:offsetRect animated:YES];
 
	
}
- (ImageScrollView *)getVisiblePageForUrlString:(NSString *)urlString {
    for (ImageScrollView *page in visiblePages) {
        if ([page.urlString isEqualToString:urlString]) {
            return page;
        }
    }
	return nil;
}

- (void) recycleAllVisiblePages {
	for (ImageScrollView *page in visiblePages) {
		[recycledPages addObject:page];
		[page removeFromSuperview];
		[page clearImage];
	}
    [visiblePages minusSet:recycledPages];	
}
#pragma mark -
#pragma mark Asynchronous HTTP request methods
#pragma mark -

- (void)grabURLInBackground:(NSString *)urlString{
	NSLog(@"Grabbing url = %@", urlString);
	NSURL *url = [NSURL URLWithString:urlString];
	ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
		
	[request setDelegate:self];
	[self.networkRequestQueue addOperation:request];
}

- (void)requestFinished:(ASIHTTPRequest *)request {
	// Use when fetching binary data
	NSData *responseData = [request responseData];
	
	ImageScrollView *page = [self getVisiblePageForUrlString:[request.originalURL absoluteString]];
	
	if (page == nil) {
		NSLog(@"Couldn't find page, ignoring response! URL = %@", [request.originalURL absoluteString]);
		return;
	}
		
	// Create the image
	UIImage *image = [UIImage imageWithData:responseData];
	// Associate the exifDescription to the page.
	page.exifDescription = [self extractImageEXIFDescriptionFromData:responseData];
	if (page.index == self.pageIndex) {
		self.infoViewController.textView.text = page.exifDescription;
	}
	// Associate the image to the page.
	[page displayImage:image];
}

- (void)requestFailed:(ASIHTTPRequest *)request {
	NSError *error = [request error];
	NSLog(@"error = %@", error);
}

#pragma mark -
#pragma mark EXF iPhone methods
#pragma mark -

- (NSString *)extractImageEXIFDescriptionFromData:(NSData *)data {
	EXFJpeg* jpegScanner = [[EXFJpeg alloc] init];
	[jpegScanner scanImageData:data];
	self.exfData = jpegScanner;
	[jpegScanner release];
	
	id value = [ self.exfData.exifMetaData tagValue:[NSNumber numberWithInt:EXIF_ImageDescription]];
	
	
	NSString *textValue;
	if ([value isKindOfClass:[NSArray class]]){
		// formater for array
		textValue =[NSString stringWithFormat:@"[%@]",[value
													   componentsJoinedByString:@","]];
	}else if ([value isKindOfClass:[NSData class]]){
		textValue =@"Binary";
	}else{
		textValue = [NSString stringWithFormat:@"%@",value];
	}

	return textValue;
}


#pragma mark -
#pragma mark InfoPopoverViewController methods
#pragma mark -
- (void) initializeInfoPopover {
	infoViewController = [[InfoViewController alloc] initWithNibName:@"InfoViewController" bundle:nil];
	infoPopoverController = [[UIPopoverController alloc] initWithContentViewController:infoViewController];
	infoPopoverController.popoverContentSize = CGSizeMake(300, 300);
}

- (void)displayInfoPopover {
	
	// If already displayed, it'll dismiss it.
	if (self.infoPopoverController.popoverVisible) {
		[self.infoPopoverController dismissPopoverAnimated:YES];
		return;
	}
	
	// need to get hold of the current page's exifDescription and assign it to the 
	// infoViewController's textView value;
	self.infoViewController.textView.text = [self getEXIFDescriptionForIndex:self.pageIndex];	
	[self.infoPopoverController presentPopoverFromBarButtonItem:self.navigationItem.rightBarButtonItem
									   permittedArrowDirections:UIPopoverArrowDirectionAny 
													   animated:YES];
}

- (NSString *)getEXIFDescriptionForIndex:(NSUInteger)index
{
    for (ImageScrollView *page in visiblePages) {
        if (page.index == index) {
			return page.exifDescription;
        }
    }
    return nil;
}


@end

