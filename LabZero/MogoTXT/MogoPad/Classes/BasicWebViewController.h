//
//  BasicWebViewController.h
//  MogoPad
//
//  Created by Jeff Barbose on 9/13/10.
//  Copyright (c) 2010 HowLand Software. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString *const kMogoURL;

enum {
    kMogoWebViewURLSchemeUndefined = 0,
    kMogoWebViewURLSchemeHTTP = 1,
    kMogoWebViewURLSchemeFTP = 2,
    kMogoWebViewURLSchemeMogo = 4
};

typedef NSUInteger MogoURLSchemeType;




@interface BasicWebViewController : UIViewController <UIWebViewDelegate>
{
    IBOutlet UIWebView *myWebView;
    IBOutlet UIActivityIndicatorView *activityIndicator;
	NSString *theUrl;
    BOOL shouldShowLogo;
    BOOL interceptLinks;
}

@property (nonatomic, retain) IBOutlet UIWebView *myWebView;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (nonatomic, retain) NSString *theUrl;
@property (nonatomic) BOOL shouldShowLogo;

@end
