    //
//  LogoViewController.m
//  MogoPad
//
//  Created by Shiun Hwang on 10/13/10.
//  Copyright 2010 HowLand Software. All rights reserved.
//

#import "LogoViewController.h"

@interface LogoViewController (private)
- (void)showImage;
- (void)fadeImage;
- (void)imageDidFadeOut:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context;

@end


@implementation LogoViewController

@synthesize splashTimer;
@synthesize activityIndicator;

/*
- (id) init {
	self = [super init];
	
	if (self) {
//		UIInterfaceOrientation o = [[UIDevice currentDevice] orientation]];
//		if (orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight) {
//			view = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"SplashScreen_landscape"
//		}
//		else if (orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown) {
//			view =
//		}
	}
	return self;
}
*/


/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	self.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
	self.activityIndicator.hidesWhenStopped = YES;
	[self.activityIndicator startAnimating];
//	[self showImage];
}



- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Overriden to allow any orientation.
    return YES;
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}


- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
	[splashTimer release];
	[activityIndicator release];
    [super dealloc];
}



- (void)showImage
{	
	self.splashTimer = [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(fadeImage) userInfo:nil repeats:NO];	
}

- (void)fadeImage
{
	NSLog(@"Fade image");
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.45];
	[UIView setAnimationDelegate:self];
	[UIView setAnimationDidStopSelector:@selector(imageDidFadeOut:finished:context:)];
	self.view.alpha = 0.0;
	[UIView commitAnimations];	
}

- (void)imageDidFadeOut:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context
{
	[self.activityIndicator stopAnimating];
	[self.view removeFromSuperview];
	//NOTE: This controller will automatically be released sometime after its view is removed from it' superview...
}




@end
