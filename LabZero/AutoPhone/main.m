//
//  main.m
//  AutoPhone
//
//  Created by Jeff Barbose on 12/9/09.
//  Copyright HowLand Software 2009. All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[]) {
    
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
    int retVal = UIApplicationMain(argc, argv, nil, nil);
    [pool release];
    return retVal;
}
