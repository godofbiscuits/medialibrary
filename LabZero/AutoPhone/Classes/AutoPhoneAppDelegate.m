//
//  AutoPhoneAppDelegate.m
//  AutoPhone
//
//  Created by Jeff Barbose on 12/9/09.
//  Copyright HowLand Software 2009. All rights reserved.
//

#import "AutoPhoneAppDelegate.h"
#import "AutoPhoneViewController.h"

@implementation AutoPhoneAppDelegate

@synthesize window;
@synthesize viewController;


- (void)applicationDidFinishLaunching:(UIApplication *)application {    
    
    // Override point for customization after app launch    
    [window addSubview:viewController.view];
    [window makeKeyAndVisible];
}


- (void)dealloc {
    [viewController release];
    [window release];
    [super dealloc];
}


@end
