//
//  AutoPhoneAppDelegate.h
//  AutoPhone
//
//  Created by Jeff Barbose on 12/9/09.
//  Copyright HowLand Software 2009. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AutoPhoneViewController;

@interface AutoPhoneAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    AutoPhoneViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet AutoPhoneViewController *viewController;

@end

