//
//  AutoPhoneViewController.h
//  AutoPhone
//
//  Created by Jeff Barbose on 12/9/09.
//  Copyright HowLand Software 2009. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef enum {
    LZCameraStateRepublic,
    LZCameraStateAcrossTheRubicon,
    LZCameraStateRetreated
} LZCameraState;


@interface AutoPhoneViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate>
{
    UIImagePickerController* imagePicker;
    
    UIButton* shakeButton;
}


@property (nonatomic, retain) UIImagePickerController* imagePicker;



@property (nonatomic, retain) IBOutlet UIButton* shakeButton;


- (void) crossTheRubicon:(id)sender;


@end

