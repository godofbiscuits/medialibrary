//
//  GrannyCamAppDelegate.m
//  GrannyCam
//
//  Created by Jeff Barbose on 1/1/10.
//  Copyright HowLand Software 2010. All rights reserved.
//

#import "GrannyCamAppDelegate.h"
#import "GrannyCamViewController.h"

@implementation GrannyCamAppDelegate

@synthesize window;
@synthesize viewController;


- (void)applicationDidFinishLaunching:(UIApplication *)application {    
    
    // Override point for customization after app launch    
    [window addSubview:viewController.view];
    [window makeKeyAndVisible];
}


- (void)dealloc {
    [viewController release];
    [window release];
    [super dealloc];
}


@end
