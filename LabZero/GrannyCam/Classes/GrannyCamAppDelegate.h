//
//  GrannyCamAppDelegate.h
//  GrannyCam
//
//  Created by Jeff Barbose on 1/1/10.
//  Copyright HowLand Software 2010. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GrannyCamViewController;

@interface GrannyCamAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    GrannyCamViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet GrannyCamViewController *viewController;

@end

